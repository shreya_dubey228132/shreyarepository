
import json
import re as ser
import initialCall
import support_help1
import check2
tab = '    '
tab1 = '        '
tab2 = '            '
tab3 = '                '
tab4 = '                    '
tab4 = '                        '

true = True
false = False
null = None
error = {}
def write_campaign(campaign_data,Appsflyer_data,firebase_data,appkey,event_data,filename):
    error = ["app_size","retention","country","api_ver","tracker",'data']
    b = "campaign_data = "
    spykey =  appkey.get('dev_key')
    with open(filename, 'a',encoding="utf-8") as convert_file:
        convert_file.write(b)
        convert_file.write("{")
        convert_file.write("\n")
        li = campaign_data.items()
        for st in li:
            if st[0] == "appsflyer":
                # convert_file.write("\n")
                convert_file.write("    'appsflyer' : { \n")
                for k in st[1].items():
            
                    t1 = "      "+"'"+k[0]+"' : '"+k[1]+"',"

                    convert_file.write(t1)
                    convert_file.write("\n")
                    # except:
                    #     traceback.print_exc(file=sys.stdout)
                
                convert_file.write("       #'spi_key' : ")
                s = "'"+spykey+"',#devkey"
                convert_file.write(s)
                # convert_file.write("'")
                convert_file.write("\n")
                convert_file.write("    },")
                convert_file.write("\n")
            elif not st[0] in error:
                # try:
                # print(st[0])
                # print(st[1])
                # print('B'*100)
                t = "   "+"'"+st[0]+"' : '"+st[1]+"',"
                # print(t)
                convert_file.write(t.encode('utf-8').decode())
                convert_file.write("\n")
                # except:
                #     traceback.print_exc(file=sys.stdout)

            else:
                pass

        for key, value in campaign_data.items(): 

            if key in error:
                convert_file.write("    '%s' : %s,\n" % (key, value))

    
    
        convert_file.write("    }")
        convert_file.write("\n")
    Appsflyer_data21 = {}
    if Appsflyer_data.get('cdn'):
        Appsflyer_data21['cdn'] = Appsflyer_data.get('cdn')
    if Appsflyer_data.get('skadsdk'):
        Appsflyer_data21['skadsdk'] = Appsflyer_data.get('skadsdk')
    if Appsflyer_data.get('dlsdk'):
        Appsflyer_data21['dlsdk'] = Appsflyer_data.get('dlsdk')
    if Appsflyer_data.get('dynamic'):
        Appsflyer_data21['dynamic'] = Appsflyer_data.get('dynamic')
    if Appsflyer_data.get('attr'):
        Appsflyer_data21['attr'] = Appsflyer_data.get('attr')
    if Appsflyer_data.get('register'):
        Appsflyer_data21['register'] = Appsflyer_data.get('register')
    if firebase_data.get('run'):
        Appsflyer_data21['run'] = firebase_data.get('run')
    with open(filename, "a") as f:
            print('Appsflyer_data = ',file=f,end='')
            print(json.dumps(Appsflyer_data21),file=f)
            print('firebase_data = ',file=f,end='')
            print(json.dumps(firebase_data),file=f)
            # print('event = ',file=f,end='')
            # print(json.dumps(event_data.get('event')),file=f)
            # print('event_val = ',file=f,end='')
            # print(json.dumps(event_data.get('event_val')),file=f)















def launch(launch_d1,filename,appsflyer_data):

    launch32 = support_help1.launch_data
    launch321 = []
    for call in launch32:
        if "launches.appsflyer.com" in call:
            if appsflyer_data.get('la_url'):
                toapend = tab+"url = 'https://"+appsflyer_data.get('la_url')+"/api/'+campaign_data.get('appsflyer').get('version') +'/iosevent'"
            else:
                toapend = tab+"url = 'https://launches.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/iosevent'"
        else:
            toapend = call
        launch321.append(toapend)
    with open(filename, "a") as f:
        for x in launch321:
            print(x,file=f)
        
    er = check2.write_function(launch_d1,filename)

    with open(filename, "a") as f1:
        if launch_d1.get('cksm_v1'):
            print(tab+"update_ios_cksm_v1(data,campaign_data.get('appsflyer').get('buildnumber'))",file=f1)
        else:
            print(tab+"update_ios_cksm_v2(data,campaign_data.get('appsflyer').get('buildnumber'))",file=f1)
        print(tab+"return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':(af_cipher(campaign_data.get('appsflyer').get('spi_key') ,json.dumps(data, separators = (',',':')), device='ios'))}",file=f1)
        print('')
    error['launch'] = er


def attri(attri,filename,appsflyer_data):
    attri32 = support_help1.attri
    attri321 = []
    for call in attri32:
        if "attr.appsflyer.com" in call:
            if appsflyer_data.get('att_url'):
                toapend = tab+"url = 'https://"+appsflyer_data.get('att_url')+"/api/'+campaign_data.get('appsflyer').get('version') +'/iosevent'"
            else:
                toapend = tab+"url = 'https://attr.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/iosevent'"
        else:
            toapend = call
        attri321.append(toapend)
    with open(filename, "a") as f:
        for x in attri321:
            print(x,file=f)
        
    er = check2.write_function(attri,filename)

    with open(filename, "a") as f1:
        if attri.get('cksm_v1'):
            print(tab+"update_ios_cksm_v1(data,campaign_data.get('appsflyer').get('buildnumber'))",file=f1)
        else:
            print(tab+"update_ios_cksm_v2(data,campaign_data.get('appsflyer').get('buildnumber'))",file=f1)
        print(tab+"return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':(af_cipher(campaign_data.get('appsflyer').get('spi_key') ,json.dumps(data, separators = (',',':')), device='ios'))}",file=f1)
        print('')
    error['attri'] = er

def import_w(filename):
    with open(filename, "w") as f:
        for x in support_help1.import_data:
            print(x,file=f)

def conversion(conversion_data,filename,appsflyer_data):
    conversion32 =  support_help1.conversion_data
    conversion321 = []
    for call in conversion32:
        if "conversions.appsflyer.com" in call:
            if appsflyer_data.get('con_url'):
                toapend = tab+"url = 'https://"+appsflyer_data.get('con_url')+"/api/'+campaign_data.get('appsflyer').get('version') +'/iosevent'"
            else:
                toapend = tab+"url = 'https://conversions.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version') +'/iosevent'"
        else:
            toapend = call
        conversion321.append(toapend)
    
    
    with open(filename, "a") as f:
        for x in conversion321:
            print(x,file=f)
        
    er = check2.write_function(conversion_data,filename)

    with open(filename, "a") as f1:
        if conversion_data.get('cksm_v1'):
            print(tab+"update_ios_cksm_v1(data,campaign_data.get('appsflyer').get('buildnumber'))",file=f1)
        else:
            print(tab+"update_ios_cksm_v2(data,campaign_data.get('appsflyer').get('buildnumber'))",file=f1)
        print(tab+"return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':(af_cipher(campaign_data.get('appsflyer').get('spi_key') ,json.dumps(data, separators = (',',':')), device='ios'))}",file=f1)
        print('')
    error['con'] = er

def inapp_write(inapp_data,filename,appsflyer_data):
    
    inapp132 = support_help1.inapp_data
    inapp_data12 = []
    for call in inapp132:
        if "inapps.appsflyer.com" in call:
            if appsflyer_data.get('in_url'):
                toapend = tab+"url = 'https://"+appsflyer_data.get('in_url')+"/api/'+campaign_data.get('appsflyer').get('version')+'/iosevent'"
            else:
                toapend = tab+"url = 'https://inapps.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/iosevent'"
        else:
            toapend = call
        inapp_data12.append(toapend)

    with open(filename, "a") as f:
        for x in inapp_data12:
            print(x,file=f)
        
    er = check2.write_function(inapp_data,filename,call_name='inapp')

    with open(filename, "a") as f1:
        if inapp_data.get('cksm_v1'):
            print(tab+"update_ios_cksm_v1(data,campaign_data.get('appsflyer').get('buildnumber'))",file=f1)
        else:
            print(tab+"update_ios_cksm_v2(data,campaign_data.get('appsflyer').get('buildnumber'))",file=f1)
        print(tab+"if event__Value == None:",file=f1)
        print(tab1+'data["eventvalue"] = "null"',file=f1)
        print(tab+'else:',file=f1)
        print(tab1+'''data["eventvalue"] = quote(json.dumps(event__Value, separators = (',',':')))''',file=f1)

        print(tab+"return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':(af_cipher(campaign_data.get('appsflyer').get('spi_key') ,json.dumps(data, separators = (',',':')), device='ios'))}",file=f1)
        print()
    error['inapp'] = er

def register_write(inapp_data,filename,appsflyer_data):
    with open(filename, "a") as f:
        resister_data_temp = support_help1.register_data
        register_data = []
        for call in resister_data_temp:
            if "register.appsflyer.com" in call:
                if appsflyer_data.get('reg_url'):
                    toapend = tab+"url = 'https://"+appsflyer_data.get('reg_url')+"/api/'+campaign_data.get('appsflyer').get('version')+'/iosevent'"
                else:
                    toapend = tab+"url = 'https://register.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/iosevent'"
            else:
                toapend = call
            register_data.append(toapend)


        for x in register_data:
            print(x,file=f)
        
    er = check2.write_function(inapp_data,filename,call_name='register')

    with open(filename, "a") as f1:
        print(tab+"return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data': json.dumps(data, separators = (',',':'))}",file=f1)
        print('')
    error['register'] = er


def gcd_write(appsflyer_data,filename):
    if appsflyer_data.get('gcd_ver') == 'v4.0':
        gcd_data1 = support_help1.gcd_dataV4
        gcd_data = []
        for call in gcd_data1:
            if "gcdsdk.appsflyer.com" in call:
                if appsflyer_data.get('gcd_url'):
                    toapend = tab+"url = 'https://"+appsflyer_data.get('gcd_url')+"/install_data/v4.0/id' + campaign_data.get('app_id')"
                else:
                    toapend = tab+"url = 'https://gcdsdk.appsflyer.com/install_data/v4.0/id' + campaign_data.get('app_id')"
            else:
                toapend = call
            gcd_data.append(toapend)

    else:
        gcd_data12 = support_help1.gcd_dataV5
        gcd_data = []
        for call in gcd_data12:
            if "gcdsdk.appsflyer.com" in call:
                if appsflyer_data.get('gcd_url'):
                    toapend = tab+"url = 'https://"+appsflyer_data.get('gcd_url')+"/install_data/v5.0/id' + campaign_data.get('app_id')"
                else:
                    toapend = tab+"url = 'https://gcdsdk.appsflyersdk.com/install_data/v5.0/id' + campaign_data.get('app_id')"
            else:
                toapend = call
            gcd_data.append(toapend)

    with open(filename, "a") as f:
        for x in gcd_data:
            print(x,file=f)

def appsf(filename,Appsflyer_data):

    calls1 = support_help1.appsflyer_call
    calls = []
    for call in calls1:
        
        if "skadsdk.appsflyer.com" in call:
            if Appsflyer_data.get('skadsdk_url'):
                toapend = tab+"url = 'https://"+Appsflyer_data.get('skadsdk_url')+"/api/v1.0/rules'"
            else:
                toapend = tab+"url = 'https://skadsdk.appsflyer.com/api/v1.0/rules'"
        elif "dynamic-config-api.appsflyer.com" in call:
            if Appsflyer_data.get('dynamic-config-api_url'):
                toapend = tab+"url = 'https://"+Appsflyer_data.get('dynamic-config-api_url')+"/api/dynamic-config/1'"
            else:
                toapend = tab+"url = 'https://dynamic-config-api.appsflyer.com/api/dynamic-config/1'"
        elif "cdn-settings.appsflyersdk.com" in call:
            if Appsflyer_data.get('cdn_url'):
                toapend = tab+"url = 'https://"+Appsflyer_data.get('cdn_url')+"/ios/v1/'+str(app_data.get('cdn_token'))+'/settings'"
            else:
                toapend = tab+"url = 'https://cdn-settings.appsflyersdk.com/ios/v1/'+str(app_data.get('cdn_token'))+'/settings'"
        elif "dlsdk.appsflyer.com" in call:
            if Appsflyer_data.get('dlsdk_url'):
                toapend = tab+"url = 'https://"+Appsflyer_data.get('dlsdk_url')+"/v1.0/ios/id'+campaign_data.get('app_id')"
            else:
                toapend = tab+'''url = "https://dlsdk.appsflyer.com/v1.0/ios/id"+campaign_data.get('app_id')'''
        else:
            toapend = call
        calls.append(toapend)


    with open(filename, "a") as f:
        for x in calls:
            print(x,file=f)

def install_write(filename,event_data,Appsflyer_data,firebase_data):
    calls = support_help1.install_call

    with open(filename, "a",encoding="utf-8") as f:
        for x in calls['fisrt']:
            print(x,file=f)


        if Appsflyer_data.get('cdn'):
            for x in calls['cdn']:
                print(x,file=f)
        if Appsflyer_data.get('skadsdk'):
            for x in calls['skasdk']:
                print(x,file=f)
        if firebase_data.get('run'):
            for x in calls['firebase']:
                print(x,file=f)
        if Appsflyer_data.get('dlsdk'):
            for x in calls['dlsdk']:
                print(x,file=f)
        if Appsflyer_data.get('attr'):
            for x in calls['att']:
                print(x,file=f)
    
        for x in calls['conversion']:
            print(x,file=f)
        if Appsflyer_data.get('dynamic'):
            for x in calls['dynamic']:
                print(x,file=f)
        
        if Appsflyer_data.get('register'):
            for x in calls['register']:
                print(x,file=f)
    
        for x in calls['end']:
            print(x,file=f)
        for x in calls['launch']:
            print(x,file=f) 

    # event_data.get('event')),file=f)
    #             print('event_val = ',file=f,end='')
    #             print(json.dumps(event_data.get('event_val')
    # with open(filename, "a",encoding="utf-8") as f:
        if event_data.get('event_val'):
            for eventname in event_data.get('event_val'):
                name1 = list(eventname.keys())
                name = name1[0]
                val =   eventname.get(name)
                n = "name = '"+name+"'"
                if val:
                    w = "val = "+json.dumps(val)
                elif val == None:
                    w = 'val = "null"'
                elif val == "null":
                    w = 'val = "null"'
                else:
                    w ='val = {}'
                print(tab+n,file=f)
                print(tab+w,file=f)
                print(tab+"req = appsflyer_inapps(app_data, device_data, event__Name = name, event__Value = val)",file=f)
                print(tab+"util.execute_request(**req)",file=f)
                print('',file=f)
                print('',file=f)
        print(tab+"return {'status':True}",file=f)


def open_write(filename):
    calls = support_help1.open_data

    with open(filename, "a") as f:
        for x in calls:
            print(x,file=f)

def last_write(filename):
    calls = support_help1.support_call

    with open(filename, "a") as f:
        for x in calls:
            print(x,file=f)







def check(string,name12):
    regex = '^[A-Za-z][A-Za-z0-9_]*'
    # pass the regular expression
    # and the string in search() method
    if ser.search(regex, string) :
        name12['name'] = string
        print("Vlaid")
    else:
        print("Invalid Name Enter Valid Name")




def file_creation(campaign_data='',final_data='',Appsflyer_data='',Apps_key='',firebase_data='',event_data = ''):
    name12 = {} 
    while(not name12.get('name')):
        name = str(input("Enter File name of app name only string...............\n"))

        check(name,name12)
    file =  name12.get('name').replace(' ','')
    filename = 'my_file/'+file+'ios.py'
    import_w(filename)
    write_campaign(campaign_data,Appsflyer_data,firebase_data,Apps_key,event_data,filename)
    if final_data.get('conversion_data'):
        conversion(final_data.get('conversion_data'),filename,Appsflyer_data)
    else:
        print("Sorry we can create file need conversion call")
    if final_data.get('atri_data'):
        attri(final_data.get('atri_data'),filename,Appsflyer_data)
    if final_data.get('launch_data'):
        launch(final_data.get('launch_data'),filename,Appsflyer_data)
    else:
        if not Appsflyer_data.get('la_url'):
            tmp = Appsflyer_data.get('con_url').split('.')
            if tmp[0] != 'conversions':
                int12 = str(input("Enter before string conversion"))
                Appsflyer_data['la_url'] = int12+'launches.'+tmp[1]+'.'+tmp[2]
            else:
                Appsflyer_data['la_url'] = 'launches.'+tmp[1]+'.'+tmp[2]
        launch(initialCall.launch_initial,filename,Appsflyer_data)
    if final_data.get('inapp_data'):
        inapp_write(final_data.get('inapp_data'),filename,Appsflyer_data)
    else:
        if not Appsflyer_data.get('in_url'):
            tmp = Appsflyer_data.get('con_url').split('.')
            if tmp[0] != 'conversions':
                int12 = str(input("Enter before string conversion"))
                Appsflyer_data['in_url'] = int12+'inapps.'+tmp[1]+'.'+tmp[2]
            else:
                Appsflyer_data['in_url'] = 'inapps.'+tmp[1]+'.'+tmp[2]
        inapp_write(initialCall.inapp,filename,Appsflyer_data)
    if final_data.get('register'):
        register_write(final_data.get('register'),filename,Appsflyer_data)
    gcd_write(Appsflyer_data,filename)
    appsf(filename,Appsflyer_data)
    install_write(filename,event_data,Appsflyer_data,firebase_data)
    open_write(filename)
    last_write(filename)

    print(error)
    import os
    error_file_name = os.getlogin()
    with open('Error/'+error_file_name+".py", "a") as f:
        print("")
        print(json.dumps(error),file=f)
        print("")

