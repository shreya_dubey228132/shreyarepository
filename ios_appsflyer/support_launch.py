def appsflyer_launch(app_data, device_data):
    url = 'https://launches.appsflyer.com/api/'+campaign_data.get('appsflyer').get('version')+'/iosevent'
    httpmethod = 'post'
    headers = {
        'Content-Type': 'application/octet-stream',
        'Accept': '*/*',
        'User-Agent':  '{0}/{1} CFNetwork/{2} Darwin/{3}'.format(campaign_data.get('app_name'), campaign_data.get('app_version_code'), device_data.get('cfnetwork').split('_')[0], device_data.get('cfnetwork').split('_')[1]),
        'Accept-Language': device_data.get('locale').get('language')+"-"+device_data.get('locale').get('country'.lower()),
        'Accept-Encoding': 'gzip, deflate, br'
    }
    params = {
        'app_id':   campaign_data.get('app_id'),
        'buildnumber':  campaign_data.get('appsflyer').get('buildnumber')
    }
    if not app_data.get('counter'):
        app_data['counter'] = 1
    else:
        app_data['counter'] +=1

    t1 = time.time()
    t_in1 = str(int(t1*1000))
    t_in2 = "%.3f"%t1+str(random.randint(000,999))
    if not app_data.get('timepassedsincelastlaunch'):
        timeSinceLastCall='-1'
        app_data['timepassedsincelastlaunch']=int(time.time())
    else:
        timeSinceLastCall=int(time.time())-app_data.get('timepassedsincelastlaunch')
        app_data['timepassedsincelastlaunch']=int(time.time())
    data = {
        "advertiserId": app_data.get('idfa'),
        "advertiserIdEnabled": False,
        "af_events_api": "1",
        "af_iad_data": {
            "Version3.1": {
            "iad-attribution": "false"
            }
        },
        "af_timestamp": t_in1,
        "appUID": app_data.get('appUID'),
        "att_status": 3,#random.randint(0,3),
        "bundleIdentifier": campaign_data.get('package_name'),
        "bundlename": campaign_data.get('bundle_name'),
        "bundleversion": campaign_data.get('app_version_code'),
        "cell":{
            "mcc": device_data.get('mcc'),
            "mnc": device_data.get('mnc'),
            "ra": "af_restricted"
        },
        "counter": str(app_data.get('counter')),
        "currentCountrycode": device_data.get('locale').get('country'),
        "currentLanguage"   : "{en}-{IN}".format(en=device_data.get('locale').get('language'), IN = device_data.get('locale').get('country')),
        "customData": json.dumps({"ta_distinct_id":app_data.get('appUID')}, separators = (',',':')),
        "date1": app_data.get('date1'),
        "date1_2": app_data.get('date1'),
        "date2": app_data.get('firstLaunchDate'),
        "date3": app_data.get('firstLaunchDate'),
        "dev_key": campaign_data.get('appsflyer').get('key'),
        "fb_anon_id": app_data.get('fb_anon_id'),
        "deviceData": {
            "brightness": str(round(random.uniform(0,1),6)),
            "cpu_64bits": "true",
            "cpu_count": "2",
            "cpu_speed": "-1",
            "cpu_type": device_data.get('cpu_type'),
            "device_model": device_data.get('device_platform'),
            "osVersion": "{os} (Build {build})".format(os = device_data.get('version'), build = device_data.get('build')),
            "ram_size": device_data.get('ram'),
            "dim": {
                "y_px": int(device_data.get('resolution').split('x')[0]),
                "x_px": int(device_data.get('resolution').split('x')[1])},
        },
        # "cksm_v2": "32cf2ad94b9b11f8591b0c4f1a0d401a0c481b0d4e1b",
        "model": "iPhone",
        "meta": {
            "first_launch": {
                "from_fg": random.randint(400,500),#448,
                "init_to_fg": random.randint(900,1000),#912,
                "net": random.randint(500,600)#566
            },
            "gcd": {
                "net": random.randint(3000,4000),#3351,
                "retries": 0
            },
            "skad": {
                "s2s": {
                    "upcv": 0
                }
            }
        },
        "open_referrer": "",
        "originalAppsflyerId": app_data.get('uid'),
        "platform": device_data.get('device_platform'),
        "platformextension": "ios_flutter",
        "ref": campaign_data.get('ref'),###############______00000
        "reinstallCounter": "0",
        "sc_o": "fu",
        "sk_rules": {
            "sk_exp": app_data.get('sk_exp'),
            "sk_rules": app_data.get('sk_rules')
        },
        "serverApiVersion": campaign_data.get('appsflyer').get('server_api_version'),
        "sessioncounter": str(app_data.get('counter')),
        "systemname": "iOS",
        "systemversion": device_data.get('os_version'),
        "timepassedsincelastlaunch": str(timeSinceLastCall),
        "timestamp": t_in2,
        "disk": app_data.get('disk'),
        "event": "Launched",
        "eventName": "Launched",
        "eventvalue": "",
        "firstLaunchDate": app_data.get('firstLaunchDate'),
        "gcd_timing": 2753,###############______00000
        "iaecounter": str(app_data.get('iaecounter')),
        "ivc": false,
        "JBDevice": False,
        "localizedmodel": "iPhone",
        "shortbundleversion": campaign_data.get('app_version_name'),
        "uid": app_data.get('uid'),
        "vendorId": app_data.get('idfv'),
        "wifi": False
    }
    update_ios_cksm_v1(data,campaign_data.get('appsflyer').get('buildnumber'))

    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':(af_cipher(campaign_data.get('appsflyer').get('spi_key') ,json.dumps(data, separators = (',',':')), device='ios'))}
