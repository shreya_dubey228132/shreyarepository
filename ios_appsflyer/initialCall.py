true = True
false = False
null = None



launch_initial =  {
      "JBDevice": true, 
      "advertiserId": "00000000-0000-0000-0000-000000000000", 
      "advertiserIdEnabled": false, 
      "af_events_api": "1", 
      "af_iad_data": {
        "error": "ADClientErrorTrackingRestrictedOrDenied"
      }, 
      "af_timestamp": "1679893371615", 
      "att_status": 0, 
      "bundleIdentifier": "com.one97.paytmmoney", 
      "bundlename": "Paytm Money", 
      "bundleversion": "7", 
      "cell": {
        "mcc": "999", 
        "mnc": "99", 
        "ra": "af_restricted"
      }, 
      "cksm_v1": "00d72f609810096af15a0e75a9fe60a8", 
      "counter": "3", 
      "currentCountrycode": "US", 
      "currentLanguage": "", 
      "date1": "2023-03-27_102958+0530", 
      "date1_2": "2023-03-27_102958+0530", 
      "date2": "2023-03-27_103248+0530", 
      "date3": "2023-03-27_103132+0530", 
      "dev_key": "r2m3EcWYgRrbwnKEUHnqt", 
      "deviceData": {
        "brightness": "0.720183", 
        "cpu_64bits": "true", 
        "cpu_count": "9", 
        "cpu_speed": "-1", 
        "cpu_type": "ARM64", 
        "device_model": "iPhone8,4", 
        "dim": {
          "x_px": 640, 
          "y_px": 1136
        }, 
        "osVersion": "99.9 (Build 99999)", 
        "ram_size": "999"
      }, 
      "disk": "999/9999", 
      "event": "Launched", 
      "eventName": "Launched", 
      "eventvalue": "", 
      "firstLaunchDate": "2023-03-27_103133+0530", 
      "iaecounter": "0", 
      "ivc": false, 
      "localizedmodel": "iPhone", 
      "meta": {
        "skad": {
          "sk_rules": {
            "sk_exp": 1680152494.7502809, 
            "sk_rules": {
              "s2s": true, 
              "wi": 72
            }
          }, 
          "upcv": 0
        }
      }, 
      "model": "iPhone", 
      "open_referrer": "", 
      "originalAppsflyerId": "1679893198090-2137374", 
      "platform": "iPhone8,4", 
      "platformextension": "ios_native", 
      "prev_session_dur": 75, 
      "ref": "db7329eef4753e02f2f61ae54182f260365b4da62dc7cab86fc41cde012bd3f8;0;0;0;0;0", 
      "reinstallCounter": "0", 
      "sc_o": "p", 
      "serverApiVersion": "6.5", 
      "sessioncounter": "2", 
      "shortbundleversion": "2.63", 
      "sk_rules": {
        "sk_exp": 1680152494.7502809, 
        "sk_rules": {
          "s2s": true, 
          "wi": 72
        }
      }, 
      "systemname": "iOS", 
      "systemversion": "15.7.3", 
      "timepassedsincelastlaunch": "78", 
      "timestamp": "1679893371.615160", 
      "uid": "1679893198090-2137374", 
      "vendorId": "D213BAB7-3E74-4044-86A8-A0317EC70142", 
      "wifi": false
    }


inapp = {
    "JBDevice":false,
    "advertiserId": "00000000-0000-0000-0000-000000000000",
    "advertiserIdEnabled": false,
    "af_events_api": "1",
    "af_iad_data": {
    "error": "ADClientErrorTrackingRestrictedOrDenied"
    },
    "af_timestamp": "1679899957775",
    "att_status": 0,
    "bundleIdentifier": "com.dest.Dresslily",
    "bundlename": "Dresslily",
    "bundleversion": "1.0",
    "cell": {
    "mcc": "999",
    "mnc": "99",
    "ra": "af_restricted"
    },
    "cksm_v2": "56689afdb41141f3591b0c4f1a0d401a0c481b0d4e1b",
    "counter": "2",
    "currentCountrycode": "US",
    "currentLanguage": "",
    "date1": "2023-03-27_122053+0530",
    "date1_2": "2023-03-27_122053+0530",
    "date2": "2023-03-27_122226+0530",
    "date3": "2023-03-27_122226+0530",
    "dev_key": "PQmjs6dfikqatrWRQ4EEG",
    "deviceData": {
    "brightness": "0.615006",
    "cpu_64bits": "true",
    "cpu_count": "9",
    "cpu_speed": "-1",
    "cpu_type": "ARM64",
    "device_model": "iPhone8,4",
    "dim": {
    "x_px": 640,
    "y_px": 1136
    },
    "osVersion": "99.9 (Build 99999)",
    "ram_size": "999"
    },
    "disk": "999/9999",
    "event": "af_view_homepage",
    "eventName": "af_view_homepage",
    "eventvalue": "%7B%22af_bts%22:%5B%5D%2C%22af_user_type%22:%221%22%2C%22af_content_type%22:%22view%20homepage%22%2C%22af_country_code%22:%22US%22%2C%22af_channel_name%22:%22ALL%22%2C%22af_channel_id%22:%22416%22%2C%22af_lang%22:%22EN%22%7D",
    "firstLaunchDate": "2023-03-27_122226+0530",
    "iaecounter": "3",
    "ivc": false,
    "localizedmodel": "iPhone",
    "meta": {
    "skad": {
    "sk_rules": {
    "sk_exp": 1679986346.7321758,
    "sk_rules": {
    "di": 1,
    "ex": 1,
    "wi": 24
    }
    }
    }
    },
    "model": "iPhone",
    "onelink_id": "t6H2",
    "originalAppsflyerId": "1679899853174-7902468",
    "platform": "iPhone8,4",
    "platformextension": "ios_native",
    "ref": "db7329eef4753e02f2f61ae54182f260365b4da62dc7cab86fc41cde012bd3f8;0;0;0;0;0",
    "reinstallCounter": "0",
    "sc_o": "fu",
    "serverApiVersion": "6.9",
    "sessioncounter": "1",
    "shortbundleversion": "7.3.1",
    "systemname": "iOS",
    "systemversion": "15.7.3",
    "timestamp": "1679899957.775931",
    "uid": "1679899853174-7902468",
    "vendorId": "79024F6B-8C55-45E0-8676-27F5E744A214",
    "wifi": false
    }