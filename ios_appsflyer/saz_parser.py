# from email.errors import NonPrintableDefect
from glob import glob
from pickle import NONE
import shutil
import tempfile
from os import listdir, path
# from xdrlib import ConversionError
from zipfile import ZipFile
import random
from traceback import format_exc
import codecs
from bs4 import BeautifulSoup
from sys import platform
import base64,requests,json
from urllib import parse 
import traceback,sys
import os,gzip
import check1

global Conversion
Conversion = []
global launch
launch = []
global inapp_data
inapp_data = []
global gcdsdk
gcdsdk = []
global Appsflyer_data
Appsflyer_data = {}
global firebase_data
firebase_data = {}
global firebase_data_check
firebase_data_check = {}
global register_data
register_data = []
global tem_list
tem_list = []
global atri
atri = []

input_path = "logs"

file_name = listdir(input_path)[0]
# print(file_name.split('_')[0])
# exit()
input_file = input_path+"/"+file_name
# print(listdir(input_path))


# os.remove(input_file)

# exit()



# exit()
package_name = str(input("Enter Appid Name........ \n"))
# package_name = file_name.split('_')[0]
# print(package_name)
# input("press enter")
package_name= package_name.strip()
def main():
	tem_list = []
	# uncommnert below 5 lines E:\autocode\saz_parser
	temp_dir = tempfile.mkdtemp(prefix="C:/httplogs_",suffix="_"+str(random.randint(1000,9999)))
	print(temp_dir)
	zf = ZipFile(input_file,'r')
	zf.extractall(temp_dir)
	zf.close()
	# temp_dir = "E:/test/file/httplogs_c9hvb2ve_8824"
	# print(listdir(temp_dir))

	try:
		dir_list = listdir(temp_dir)
		for x in dir_list:
			if x.endswith(".htm"):
				file_link = path.join(temp_dir,x)
				# print(file_link)
				log_list_html = BeautifulSoup(codecs.open(file_link,'r'),features="html.parser")
				tc1 = 0
				column_numbers = {"#":-1,"Result":-1,"Protocol":-1,"Host":-1,"URL":-1,"Body":-1}
				for row in log_list_html('tr'):
					if len(row('td')) == 0 and len(row('th')) > 0:
						i=0
						for i in range(len(row('th'))):
							column_number_key = row('th')[i].text
							if column_number_key in column_numbers:
								column_numbers[column_number_key]=i
						for column_number_key in column_numbers:
							if column_numbers.get(column_number_key) == -1:
								del column_numbers[column_number_key]
					elif len(row('td')) > 0 and len(row('th')) == 0:
						log_host = row('td')[column_numbers.get('Host')].text
						log_no = row('td')[column_numbers.get('#')].text
						url_path = row('td')[column_numbers.get('URL')].text
						links_html = row('td')[0]
						links = {"C":None,"S":None,"M":None}
						for link in row('td')[0]('a',href=True):
							if "linux" in platform:
								links[link.text] = link['href'].replace('\\','/')
							else:
								links[link.text] = link['href']
						request_file = None
						response_file = None
						if links.get('C'):
							request_file = path.join(temp_dir,links.get('C'))
						if links.get('S'):
							response_file = path.join(temp_dir,links.get('S'))

						# print(log_host)
						# exit()
						parse_gcd_log(log_host, log_no, links, request_file, response_file,url_path)
						# print("Calling ___GCD")
							# print("# "+str(log_no)+", "+log_contentType,)
					else:
						print(row)
						print("*"*100)
					# print("~"*50)
					tc1 += 1
					# if tc1 == 100:
					# 	break
				# print(log_list_html.read())
				print(column_numbers)
		for x in dir_list:
			if not Appsflyer_data.get('Key'):
				print("Appsflyer Key Not Found Please Enter Mamually If u have...................")
				raw_Key = str(input())
				Appsflyer_data['Key'] = raw_Key.strip()
				Appsflyer_data['gcd_ver'] = 'v5.0'
				# print("Please enter package id ....................................")
				# raw_id = str(input())
				if not Apps_key.get('dev_key'):
					key_generation(raw_Key.strip())
				Appsflyer_data['id'] = package_name
				print("key done")
			if x.endswith(".htm"):
				file_link = path.join(temp_dir,x)
				# print(file_link)
				log_list_html = BeautifulSoup(codecs.open(file_link,'r'),features="html.parser")
				tc1 = 0
				column_numbers = {"#":-1,"Result":-1,"Protocol":-1,"Host":-1,"URL":-1,"Body":-1}
				for row in log_list_html('tr'):
					if len(row('td')) == 0 and len(row('th')) > 0:
						i=0
						for i in range(len(row('th'))):
							column_number_key = row('th')[i].text
							if column_number_key in column_numbers:
								column_numbers[column_number_key]=i
						for column_number_key in column_numbers:
							if column_numbers.get(column_number_key) == -1:
								del column_numbers[column_number_key]
					elif len(row('td')) > 0 and len(row('th')) == 0:
						log_host = row('td')[column_numbers.get('Host')].text
						log_no = row('td')[column_numbers.get('#')].text
						url_path = row('td')[column_numbers.get('URL')].text
						links_html = row('td')[0]
						links = {"C":None,"S":None,"M":None}
						for link in row('td')[0]('a',href=True):
							if "linux" in platform:
								links[link.text] = link['href'].replace('\\','/')
							else:
								links[link.text] = link['href']
						request_file = None
						response_file = None
						if links.get('C'):
							request_file = path.join(temp_dir,links.get('C'))
						if links.get('S'):
							response_file = path.join(temp_dir,links.get('S'))

						# print(log_host)
						# exit()
						parse_single_log(log_host, log_no, links, request_file, response_file,url_path)
						if log_host == "firebaseinstallations.googleapis.com":
							tem_list = [log_host, log_no, links, request_file, response_file,url_path]
						if len(tem_list) > 0:
							# print(tem_list)
							if not firebase_data.get('run'):
								if Appsflyer_data.get('bundle_id'):
									parse_single_log2(tem_list[0], tem_list[1], tem_list[2], tem_list[3], tem_list[4],tem_list[5])
							# print("# "+str(log_no)+", "+log_contentType,)
					else:
						print(row)
						print("*"*100)
					# print("~"*50)
					tc1 += 1
					# if tc1 == 100:
					# 	break
				# print(log_list_html.read())
				print(column_numbers)

	except:
		error_out = format_exc()
		print(error_out)


	# uncomment below 3 line
	print("#"*50+"\nPress Enter to Delet Temp file ...\n"+"#"*50)
	input("")
	
	shutil.rmtree(temp_dir)
	print("Temp file deleted........................................")




	# a = tempfile.shopeerandom1


def parse_single_log2(log_host, log_no, links, request_file, response_file,url_path):
	# print(filepath)
	# print("Second"*100)
	print(log_host)
	# d = str(input("enter enter for 2"))
	if "firebaseinstallations.googleapis.com" in log_host:
		print("F"*100)
		print(url_path)
		if not firebase_data_check.get('once'):
			parse_firebase_request(filepath=request_file,log_host = log_host,url_path=url_path)
			firebase_data_check['once'] = True

def parse_single_log(log_host, log_no, links, request_file, response_file,url_path):
	# print("First"*10)
	# print(log_host)
	# d = str(input("enter enter for 1"))
	print(log_host)
	if "appsflyer" in log_host:
		
		try: 
			if  log_host.split('.')[0].endswith('launches') or log_host.split('.')[0].endswith('conversions') or log_host.split('.')[0].endswith('inapps') or log_host.split('.')[0].endswith('attr') or log_host.split('.')[0].endswith('register'):
				print("in parser")
				# print(url_path)
				# input("break")
				if not  Appsflyer_data.get('Key'):
					pass
					# print("Appsflyer Key Not Found Please Enter Mamually If u have...................")
					# raw_Key = str(input())
					# Appsflyer_data['Key'] = raw_Key.strip()
					# Appsflyer_data['gcd_ver'] = 'v5.0'
					# # print("Please enter package id ....................................")
					# # raw_id = str(input())
					# if not Apps_key.get('dev_key'):
					# 	key_generation(raw_Key.strip())
					# Appsflyer_data['id'] = package_name


					# print((log_host, log_no, links, request_file, response_file,url_path))
					# input("break")
					# parse_appsflyer_request(filepath=request_file,log_host = log_host.split('.')[0],url=url_path,file_url ="."+log_host.split('.')[1]+"."+log_host.split('.')[2])
				else:
					print("S"*100)
					print(url_path)
					print("S"*100)
					if not Apps_key.get('dev_key'):
						print(Appsflyer_data.get('Key'))
						key_generation(Appsflyer_data.get('Key'))
					b =  parse.urlparse(url_path)
					b.query
					# print(b.query)
					if parse.parse_qs(b.query).get('app_id'):
						id =  parse.parse_qs(b.query).get('app_id')[0]
					
					if parse.parse_qs(b.query).get('buildnumber'):
						Appsflyer_data['build_number'] =  parse.parse_qs(b.query).get('buildnumber')[0]
					
					if Appsflyer_data.get('id') == id:
						# print("**"*50)
						# print((log_host, log_no, links, request_file, response_file,url_path))
						parse_appsflyer_request(filepath=request_file,log_host = log_host.split('.')[0],url=url_path,file_url ="."+log_host.split('.')[1]+"."+log_host.split('.')[2],main_url=log_host)
			elif  log_host.split('.')[1] == 'launches' or log_host.split('.')[1] == 'conversions' or log_host.split('.')[1] == 'inapps' or log_host.split('.')[1] == 'attr' or log_host.split('.')[1] == 'register':
				print("in parser")
				# print(url_path)
				# input("break")
				if not  Appsflyer_data.get('Key'):
					pass
					# print("Appsflyer Key Not Found Please Enter Mamually If u have...................")
					# raw_Key = str(input())
					# Appsflyer_data['Key'] = raw_Key.strip()
					# Appsflyer_data['gcd_ver'] = 'v5.0'
					# # print("Please enter package id ....................................")
					# # raw_id = str(input())
					# if not Apps_key.get('dev_key'):
					# 	key_generation(raw_Key.strip())
					# Appsflyer_data['id'] = package_name


					# print((log_host, log_no, links, request_file, response_file,url_path))
					# input("break")
					# parse_appsflyer_request(filepath=request_file,log_host = log_host.split('.')[0],url=url_path,file_url ="."+log_host.split('.')[1]+"."+log_host.split('.')[2])
				else:
					print("S"*100)
					print(url_path)
					print("S"*100)
					# if not Apps_key.get('dev_key'):
					# 	print(Appsflyer_data.get('Key'))
					# 	key_generation(Appsflyer_data.get('Key'))
					b =  parse.urlparse(url_path)
					b.query
					# print(b.query)
					if parse.parse_qs(b.query).get('app_id'):
						id =  parse.parse_qs(b.query).get('app_id')[0]
					
					if parse.parse_qs(b.query).get('buildnumber'):
						Appsflyer_data['build_number'] =  parse.parse_qs(b.query).get('buildnumber')[0]
					
					if Appsflyer_data.get('id') == id:
						# print("**"*50)
						# print((log_host, log_no, links, request_file, response_file,url_path))
						parse_appsflyer_request(filepath=request_file,log_host = log_host.split('.')[1],url=url_path,file_url ="."+log_host.split('.')[2]+"."+log_host.split('.')[3],main_url=log_host)

			elif log_host.split('.')[0].endswith('cdn-settings'):
				b = parse.urlparse(url_path)
				b.query
				# print()
				Appsflyer_data['cdn_version'] = b.path.split('/')[2]
				Appsflyer_data['cdn_url'] = log_host
				Appsflyer_data['cdn'] = True
				# Appsflyer_data['cdn_url'] = log_host.split('.')[1]

			elif log_host.split('.')[1] == 'cdn-settings':
				b = parse.urlparse(url_path)
				b.query
				# print()
				Appsflyer_data['cdn_version'] = b.path.split('/')[2]
				Appsflyer_data['cdn_url'] = log_host
				Appsflyer_data['cdn'] = True
				# Appsflyer_data['cdn_url'] = log_host.split('.')[1]
			elif log_host.split('.')[0].endswith('dlsdk'):
				
				# print()
				# Appsflyer_data['cdn_version'] = b.path.split('/')[2]
				Appsflyer_data['dlsdk_url'] = log_host
			elif log_host.split('.')[1] == 'dlsdk':
				
				# print()
				# Appsflyer_data['cdn_version'] = b.path.split('/')[2]
				Appsflyer_data['dlsdk_url'] = log_host
				Appsflyer_data['dlsdk'] = True
			elif log_host.split('.')[0].endswith('skadsdk'):
				Appsflyer_data['dlsdk'] = True
				# print()
				# Appsflyer_data['cdn_version'] = b.path.split('/')[2]
				Appsflyer_data['skadsdk_url'] = log_host
				Appsflyer_data['skadsdk'] = True
			elif log_host.split('.')[1] == 'skadsdk':
				Appsflyer_data['skadsdk'] = True
				
				Appsflyer_data['skadsdk_url'] = log_host
			elif log_host.split('.')[0] == 'dynamic-config-api':
				# print()
				Appsflyer_data['dynamic'] = True
				Appsflyer_data['dynamic-config-api_url'] = log_host
			elif log_host.split('.')[1].endswith('dynamic-config-api'):
				Appsflyer_data['dynamic'] = True
				Appsflyer_data['dynamic-config-api_url'] = log_host
			elif log_host.split('.')[0].endswith('skadsdkless'):
				# print()
				# Appsflyer_data['cdn_version'] = b.path.split('/')[2]
				Appsflyer_data['skadsdkless_url'] = log_host
			elif log_host.split('.')[1] == 'skadsdkless':
				# print()
				# Appsflyer_data['cdn_version'] = b.path.split('/')[2]
				Appsflyer_data['skadsdkless_url'] = log_host

			print(log_host.split('.'))
			# d = str(input(' '))
				# print(path.dirname(b.path))
		except:
			traceback.print_exc(file=sys.stdout)

			



def parse_gcd_log(log_host, log_no, links, request_file, response_file,url_path):

	if "appsflyer" in log_host:
		# print(log_host)
		if log_host.split('.')[0].endswith('gcdsdk'):
			print("G"*100)
			print((log_host, log_no, links, request_file, response_file,url_path))
			parse_gcdsdk_appsflyer_request(filepath=request_file,log_host = log_host,url=url_path,file_url ="."+log_host.split('.')[1]+"."+log_host.split('.')[2])
		if log_host.split('.')[0].endswith('register'):
			print("R"*100)
			print((log_host, log_no, links, request_file, response_file,url_path))
			parse_gcdsdk_appsflyer_request(filepath=request_file,log_host = log_host,url=url_path,file_url ="."+log_host.split('.')[1]+"."+log_host.split('.')[2])


def parse_gcdsdk_appsflyer_request(filepath=None,log_host= None,url=None,file_url=None):
	# print(filepath)
	# print(log_host)
	if filepath == None:
		print("Request parse appsflyer request function without passing request file path")
		return True
	if log_host.split('.')[0].endswith('register'):
		Appsflyer_data['reg_url'] = log_host
		Appsflyer_data['register'] = True
		print(filepath)
		b = parse.urlparse(url)
		b.query
		# print(b.query)
		if  parse.parse_qs(b.query).get('app_id'):
			id1 = parse.parse_qs(b.query).get('app_id')[0]
		else:
			id1 = ''
		# print(path.basename(b.path))
		if path.isfile(filepath):
			no = 0
			with open(filepath,'rb') as infile:
				# print(infile.read())
				# infile.seek(0)
				for line in infile:
					# print(line)
					no += 1
					try:
						line1 = line.decode('utf-8').replace("\n","").replace("\r","")
						print(line1)
						if line1 == "":
							print("~"*60+str(no))
							break

					except:
						pass
				# infile.seek(no-1)
				file_data = b""
				for line in infile:
					# print(line)
					file_data += line
				register_local_data = json.loads(file_data.decode())
				register_data.append(register_local_data)
				if package_name == id1:
					# print(register_data)
					Appsflyer_data['Key'] =  register_local_data.get('devkey')
					Appsflyer_data['id']  = id1
					Appsflyer_data['app_name']  = register_local_data.get('app_name')
					# print("Dta")
	else:
		Appsflyer_data['gcd_url'] = log_host
		b = parse.urlparse(url)
		b.query
		print(parse.parse_qs(b.query))
		# xyz = 
		Appsflyer_data['gcd_ver'] = b.path.split('/')[2]
		n = path.basename(b.path)[2:]
		# n = input('')
		if package_name == n:
			if parse.parse_qs(b.query).get('devkey'):
				Appsflyer_data['Key'] =  parse.parse_qs(b.query).get('devkey')[0]
				Appsflyer_data['id']  = n
			else:
				pass
				# Appsflyer_data['afsign']  = True
				# Appsflyer_data['af_request_epoch_ms']  = True





def parse_firebase_request(filepath=None,log_host= None,url_path=None):
	# print(filepath)
	# print("hjgahgh"*100)
	# print(log_host)
	# d = str(input("enter enter for fdir"))
	
	if filepath == None:
		print("Request parse appsflyer request function without passing request file path")
		return True
	if path.isfile(filepath):
		no = 0
		with open(filepath,'rb') as infile:
			# print("z"*100)
			# print(infile.read())
			# print("z"*100)
			for line in infile:
				# print(line)
				no += 1
				try:
					line1 = line.decode('utf-8').replace("\n","").replace("\r","")
					print("&"*100)
					print(line1)
					print(type(line1))
					# print('X-firebase-client')
					print("&"*100)

					# print("check"*100)
					if "X-firebase-client" in line1:
						print("*"*60+"x-firebase-client"+"*"*60)
						print(line1)
						b =  parse.urlparse(line1)
						print(b)
						b1 = b.path	
						print(b1)
						client = b1.strip()
					if "User-Agent" in line1:
						print("*"*60+"User-Agent"+"*"*60)
						print(line1)
						b =  parse.urlparse(line1)
						print(b)
						b1 = b.path	
						print(b1)
						agent = b1.strip()
					elif "X-Goog-Api-Key" in line1:
						print("A"*100)
						print("*"*60+"X-Google-Api-Key"+"*"*60)
						print(line1)
						b =  parse.urlparse(line1)
						print(b)
						b1 = b.path	
						print(b1)
						google_api_key = b1.strip()
						# print("="*200)
					# elif "x-goog-api-key" in line1:
					# 	print("A"*100)
					# 	print("*"*60+"x-goog-api-key"+"*"*60)
					# 	print(line1)
					# 	b =  parse.urlparse(line1)
					# 	print(b)
					# 	b1 = b.path	
					# 	print(b1)
					# 	google_api = b1.strip()
					# 	print("="*200)
					elif "X-Ios-Bundle-Identifier" in line1:
						print("A"*100)
						print("*"*60+"Bundle-Identifier"+"*"*60)
						print(line1)
						b =  parse.urlparse(line1)
						print(b)
						b1 = b.path	
						print(b1)
						x_bundle_identifer = b1.strip()
						# firebase_data['x-goog-api-key'] = google_api
						print("="*200)
					else:
						client = ''
						pass
					# print(line1)
					# print(8)
					if line1 == "":
						print("~"*60+str(no))
						break

				except:
					pass
			# infile.seek(no-1)
			file_data = b""
			for line in infile:
				# print(line)
				file_data += line
			# print(file_data)
				
			print("#"*50)
			# print("Final="*100)
			try:
				print(file_data)
				firebase_tmp_data= json.loads(file_data.decode())
			except:
				final_data = gzip.decompress(file_data)
				print(final_data)
				firebase_tmp_data = json.loads(final_data.decode())
				# print(file_data)
				# print(len(file_data))
			# print(client)
			# print("upper")
			# print(x_bundle_identifer)
			# print(Appsflyer_data.get('bundle_id'))
			# print("IN key Generatiion")
			# d = str(input(''))
			firebase_data['run'] = True
			if x_bundle_identifer == Appsflyer_data.get('bundle_id'):
				# firebase_data['x-goog-api-key'] = google_api
				if client:
					firebase_data['x-firebase-client'] = client
				firebase_data['X-Android-Cert'] = google_api_key
				firebase_data['data'] = firebase_tmp_data
				firebase_data['agent'] = agent
				firebase_data['url'] = url_path

				# print(firebase_data)
				# s = str(input(""))
		# print(codecs.open(filepath,'r').read())
	else:
		print("requested path is not a file")
		return True





def parse_appsflyer_request(filepath=None,log_host= None,url=None,file_url=None,main_url=''):
	# print("S"*10)
	# print(file_url)
	# print("&"*10)
	# print(log_host)
	# print("&"*10)
	if filepath == None:
		print("Request parse appsflyer request function without passing request file path")
		return True
	if path.isfile(filepath):
		no = 0
		with open(filepath,'rb') as infile:
			# print(infile.read())
			# infile.seek(0)
			for line in infile:
				# print(line)
				no += 1
				try:
					line1 = line.decode('utf-8').replace("\n","").replace("\r","")
					# print(line1)
					if "User-Agent" in line1:
						print("*"*60+"User-Agent"+"*"*60)
						print(line1)
						b =  parse.urlparse(line1)
						print(b)
						b1 = b.path	
						print(b1)
						agent = b1.strip()
						if not Appsflyer_data.get('main_agent'):
							Appsflyer_data['main_agent'] = agent
					if line1 == "":
						print("~"*60+str(no))
						break

				except:
					pass
			# infile.seek(no-1)
			file_data = b""
			for line in infile:
				# print(line)
				file_data += line

			# print(log_host)
			# print(url)
			# input("wait and see url")
			if log_host.endswith('launches'):
				da = base64.b64encode(file_data)
				if not Appsflyer_data.get('bundle_id'):
					dyc_con = decrypt(da,Apps_key)
					dyc__data = json.loads(dyc_con).get('Decrypted Data')[0]
					Appsflyer_data['bundle_id'] = dyc__data.get('bundleIdentifier')
				launch.append(da)
				# if not Appsflyer_data.get('main_url'):
				# 	Appsflyer_data['main_url'] = "."+main_url.split('.')[1]+"."+main_url.split('.')[2]
				# else:
				# 	if Appsflyer_data.get('main_url') == file_url:
				# 		pass
				# 	else:
				Appsflyer_data['la_url']  = main_url
				# print(len(file_data))
				print("E"*100)
			elif log_host.endswith('inapps'):
				print("in inapp")
				da = base64.b64encode(file_data)
				print(da)
				# input("")
				inapp_data.append(da)
				# if not Appsflyer_data.get('main_url'):
				# 	Appsflyer_data['main_url'] = "."+main_url.split('.')[1]+"."+main_url.split('.')[2]
				# else:
				# 	if Appsflyer_data.get('main_url') == file_url:
				# 		pass
				# 	else:
				Appsflyer_data['in_url']  = main_url
				# print(len(file_data))
			elif log_host.endswith('conversions'):
				# if not Appsflyer_data.get('main_url'):
				# 	Appsflyer_data['main_url'] = "."+main_url.split('.')[1]+"."+main_url.split('.')[2]
				# else:
				# 	if Appsflyer_data.get('main_url') == file_url:
				# 		pass
				# 	else:
				Appsflyer_data['con_url']  = main_url
				da = base64.b64encode(file_data)
				# print(da)
				# input("hello")
				Conversion.append(da)
				if not Appsflyer_data.get('bundle_id'):
					dyc_con = decrypt(da,Apps_key)
					dyc__data = json.loads(dyc_con).get('Decrypted Data')[0]
					Appsflyer_data['bundle_id'] = dyc__data.get('bundleIdentifier')
				# print(len(file_data))
			elif log_host.endswith('attr'):
				# if not Appsflyer_data.get('main_url'):
				# 	Appsflyer_data['main_url'] = "."+main_url.split('.')[1]+"."+main_url.split('.')[2]
				# else:
				# 	if Appsflyer_data.get('main_url') == file_url:
				# 		pass
				# 	else:
				Appsflyer_data['att_url']  = main_url
				da = base64.b64encode(file_data)
				atri.append(da)
				Appsflyer_data['attr'] = True

				# print("#"*50)
		# print(codecs.open(filepath,'r').read())
	else:
		print("requested path is not a file")
		return True




Apps_key = {}
launchs_data1 = {}
event_name = []
event_value = []
launch1 = []
inapp1 = []


def key_generation(key):
	url = "https://fuseclick.c2a.in/keyconvert"
	proxies = {"http": None, "https": None}

	data = {}
	data['af_key'] = key
	resp = requests.post(url, data=data, proxies=proxies, timeout=100)
	# print resp.text
	if not Apps_key.get('dev_key'):
		Apps_key['dev_key'] =  json.loads(resp.text).get('development_key')
		# print Apps_key.get('dev_key')
		print('Key Generated')

	if not Apps_key.get('production_key'):
		Apps_key['production_key'] =  json.loads(resp.text).get('production_key')
		print ("Sucessfully...................")




def decrypt(dyc_data,Apps_key):
	print(".",end='')
	print("........",end='.')
	# d = str(input("press enter and wait"))
	url = "https://fuseclick.c2a.in/af_decrypt"
	proxies = {"http": None, "https": None}

	data = {}
	# print conversin_data
	# print 
	data['appsflyer_key'] = Apps_key.get('dev_key')
	data['appsflyer_data'] = dyc_data
	
	if  Apps_key.get('dev_key'):
			count12 = 0
			for _ in range(2):
				try:
					resp = requests.post(url, data=data, proxies=proxies, timeout=100)
					print(".",end='')
					print("........",end='.')
					print(resp.status_code)
					if resp.status_code == 200:
						if json.loads(resp.text).get('Decrypted Data'):
							return resp.text
						else:
							print(resp.text)
							input('check your enternet and press Enter')
					# count += 1 
				except:
					# count += 1 
					traceback.print_exc(file=sys.stdout)
					input('check above erorr and  your enternet and press Enter')
				count12 += 1
	else:
		print("There Are some problem in ur Key or Internet")
		print("Thanks For using Autocode")
		exit()
	if count12 == 2:
		print("There Are some problem in ur Key or Internet")
		print("Thanks For using Autocode")
		exit()

	# else:
	# 	return True

	# resp = requests.post(url, data=data, proxies=proxies, timeout=100)
	# t = resp.text
	# print(resp.status_code)




	# root_path = os.getcwd()
	# fi = str(file_name.split('_')[0]).strip()
	# path12 = str(root_path)+'/decrypt_data/'+fi+'.py'
	# sys.stdout = open(path12, 'a')
	# print('')
	# print(t)
	# sys.stdout.close()

	return t


def event_list(data12):
	e = decrypt(data12,Apps_key)
	if e:
		eve =  json.loads(e).get('Decrypted Data')[0]
		if not launchs_data1.get('inapp'):
			# e = decrypt(data12,Apps_key)
			inapp1.append(json.loads(e).get('Decrypted Data')[0])
			# print launch
			launchs_data1['inapp'] = True  
		
	# print p
	# exit()

		event_name.append(eve.get('eventName'))
		if eve.get('eventName'):
			if eve.get('eventvalue') == None:
				tmp = {eve.get('eventName'):eve.get('eventvalue')}
			else:
				tmp = {eve.get('eventName'):json.loads(parse.unquote(eve.get('eventvalue')))}
			event_value.append(tmp)



def launch_dyc(data12):
	# print "hell+++++++++++++++++++++++++++++++++++++++++++++++"
	# print data12
	if not launchs_data1.get('launch'):
		e = decrypt(data12,Apps_key)
		launch1.append(json.loads(e).get('Decrypted Data')[0])
		# print launch
		# print launch
		launchs_data1['launch'] = True





def get_campaign_data(conversion_data,appsflyer_data):
	import ast
	# error = ["app_size","retention","country","api_ver","tracker"]

	# reading the data from the file
	with open('support/appstext.txt') as f:
		data = f.read()
	
	# reconstructing the data as a dictionary
	d = ast.literal_eval(data)
	d['package_name'] = conversion_data.get('bundleIdentifier')
	d['bundle_name'] = conversion_data.get('bundlename')
	d['app_id'] = appsflyer_data.get('id')
	d['app_version_name'] = conversion_data.get('shortbundleversion')
	d['app_version_code'] = conversion_data.get('bundleversion')
	d['app_name'] =appsflyer_data.get('main_agent').split('/')[0]
	d['supported_os'] = str(random.randint(13,15))
	d['platformextension'] = conversion_data.get('platformextension')
	d['appsflyer']['key'] = conversion_data.get('dev_key')
	d['appsflyer']['buildnumber'] = appsflyer_data.get('build_number')
	d['appsflyer']['version'] = 'v'+appsflyer_data.get('build_number')[:-2]
	d['appsflyer']['server_api_version'] = conversion_data.get('serverApiVersion')
	d['appsflyer']['spi_key'] =  Apps_key.get('production_key')
	return d

if __name__ == "__main__":
	# Appsflyer_data['Key'] = '5Hptn6QnC4DpXGMZpmcZD9'
	main()
	# print(Appsflyer_data)
	# exit()

	# print(len(Conversion))
	# print(len(launch))
	# print(len(inapp_data))
	# # print(len(register_data))
	# print(firebase_data)


	if not Apps_key.get('dev_key'):
		key_generation(Appsflyer_data.get('Key'))

	# print(Appsflyer_data)
	# print(len(Conversion))
	# print(len(launch))
	# print(len(inapp_data))
	# # print(len(register_data))
	# print(firebase_data)
	try:
		# print(Conversion)
		dyc_con = decrypt(Conversion,Apps_key)
		dyc_conversion_data = json.loads(dyc_con).get('Decrypted Data')[0]
	except:
		dyc_conversion_data = {}		
	# exit()

	try:
		dyc_lau = decrypt(launch[0],Apps_key)
		dyc_launch_data = json.loads(dyc_lau).get('Decrypted Data')[0]
	except:
		dyc_launch_data = []

	try:
		dyc_inapp = decrypt(inapp_data[0],Apps_key)
		dyc_inapp_data = json.loads(dyc_inapp).get('Decrypted Data')[0]
	except:
		dyc_inapp_data= {}
	
	for i in inapp_data:
		event_list(i)


	try:
		register = register_data[0]
	except:
		register = {} 

	try:
		dyc_atri= decrypt(atri[0],Apps_key)
		dyc_atri_data = json.loads(dyc_lau).get('Decrypted Data')[0]
	except:
		dyc_atri_data = []
	final_data = {}
	event_data = {}
	# print('Conversion')
	# print(dyc_conversion_data)
	if dyc_conversion_data:
		campaign_data =  get_campaign_data(dyc_conversion_data,Appsflyer_data)
	else:
		print("Fail")
		exit()
	if dyc_conversion_data:
		final_data['conversion_data'] = dyc_conversion_data
	if dyc_inapp_data:
		# print(dyc_inapp_data)
		final_data['inapp_data'] = dyc_inapp_data
	if dyc_launch_data:
		final_data['launch_data'] = dyc_launch_data
	if dyc_atri_data:
		final_data['atri_data'] = dyc_atri_data
	if register:
		final_data['register'] = register
	if event_name:
		event_data['event'] = event_name
	if event_value:
		event_data['event_val'] = event_value
	# print(campaign_data)
	# input("")
	check1.file_creation(campaign_data=campaign_data,final_data=final_data,Appsflyer_data=Appsflyer_data,Apps_key=Apps_key,firebase_data=firebase_data,event_data = event_data)
	os.remove(input_file)
	# print(dyc_launch_data)
	# print("Inapp")
	# print(dyc_inapp_data)
	# print(event_name)
	# print(event_value)
	# print(Appsflyer_data)
	# print(Apps_key)
	# print(firebase_data)
	# print(register)
	# print("atri")
	# print(atri)
	# appsflyernew.file_creation(dyc_conversion_data,dyc_launch_data,dyc_inapp_data,event_name,event_value,Appsflyer_data,Apps_key,firebase_data,register)
