false = False
true = True
null = None

launch_data = {
        "advertiserId": "app_data.get('idfa')",
        "advertiserIdEnabled": 'False',
        "af_events_api": "1",
        "af_iad_data": {
            "Version3.1": {
            "iad-attribution": "false"
            }
        },
        'fb_ddl': {
                'error': 'The operation couldn’t be completed. (com.facebook.sdk.core error 8.)', 
                'ttr': '3.31029'
            },
        "af_timestamp": 't_in1',
        "appUID": "app_data.get('appUID')",
        "att_status": 'random.randint(0,3)',
        "bundleIdentifier": "campaign_data.get('package_name')",
        "bundlename": "campaign_data.get('bundle_name')",
        "bundleversion": "campaign_data.get('app_version_code')",
        "cell":{
            "mcc": "device_data.get('mcc')",
            "mnc": "device_data.get('mnc')",
            "ra": "''"
        },
        "counter": "str(app_data.get('counter'))",
        "currentCountrycode": "device_data.get('locale').get('country')",
        "currentLanguage"   : '''"{en}-{IN}".format(en=device_data.get('locale').get('language'), IN = device_data.get('locale').get('country'))''',
        "date1": "app_data.get('date1')",
        "date1_2": "app_data.get('date1')",
        "date2": "app_data.get('firstLaunchDate')",
        "date3": "app_data.get('firstLaunchDate')",
        "dev_key": "campaign_data.get('appsflyer').get('key')",
        "fb_anon_id": "app_data.get('fb_anon_id')",
        "deviceData": {
            "brightness": 'str(round(random.uniform(0,1),6))',
            "cpu_64bits": "true",
            "cpu_count": "2",
            "cpu_speed": "-1",
            "cpu_type": "device_data.get('cpu_type')",
            "device_model": "device_data.get('device_platform')",
            "osVersion": '''"{os} (Build {build})".format(os = device_data.get('version'), build = device_data.get('build'))''',
            "ram_size": "device_data.get('ram')",
            "dim": {
                "y_px": "int(device_data.get('resolution').split('x')[0])",
                "x_px": "int(device_data.get('resolution').split('x')[1])"
                },
        },
        "cksm_v2": "32cf2ad94b9b11f8591b0c4f1a0d401a0c481b0d4e1b",
        "cksm_v1": "32cf2ad94b9b11f8591b0c4f1a0d401a0c481b0d4e1b",
        "model": 'iPhone',
        "meta": {
            "first_launch": {
                "from_fg": 'random.randint(400,500)',
                "init_to_fg": 'random.randint(900,1000)',
                "net": 'random.randint(500,600)'
            },
            "gcd": {
                "net": 'random.randint(3000,4000)',
                "retries": 0
            },
            "skad": {
                "s2s": {
                    "upcv": 0
                }
            }
        },
        "open_referrer": "''",
        'lang_code':  'en',
        "originalAppsflyerId": "app_data.get('uid')",
        "platform": "device_data.get('device_platform')",
        "platformextension": 'campaign_data.get("platformextension")',
        "ref": "campaign_data.get('ref')",
        "reinstallCounter": "0",
        "sc_o": "fu",
        "sk_rules": {
            "sk_exp": "app_data.get('sk_exp')",
            "sk_rules": "app_data.get('sk_rules')"
        },
        "serverApiVersion": "campaign_data.get('appsflyer').get('server_api_version')",
        "sessioncounter": "str(app_data.get('counter'))",
        "systemname": "iOS",
        "systemversion": "device_data.get('os_version')",
        "timepassedsincelastlaunch": 'str(timeSinceLastCall)',
        "timestamp": 't_in2',
        "disk": "app_data.get('disk')",
        "event": "Launched",
        "eventName": "Launched",
        "eventvalue": "''",
        "prev_session_dur":"prev_session_dur",
        "firstLaunchDate": "app_data.get('firstLaunchDate')",
        "gcd_timing": "random.randint(1200,1300)",
        "iaecounter" : "str(app_data.get('iaecounter'))",
        "ivc": 'False',
        "JBDevice": 'False',
        "localizedmodel": "iPhone",
        "shortbundleversion": "campaign_data.get('app_version_name')",
        "uid": "app_data.get('uid')",
        "vendorId": "app_data.get('idfv')",
        "wifi": 'False'
    }

conversion_data = {
        "advertiserId": "app_data.get('idfa')",
        "advertiserIdEnabled": 'False',
        "af_events_api": "1",
        "af_iad_data": {
            "Version3.1": {
            "iad-attribution": "false"
            }
        },
        'fb_ddl': {
                'error': 'The operation couldn’t be completed. (com.facebook.sdk.core error 8.)', 
                'ttr': '3.31029'
            },
        "asa_data": {
            "token":"app_data.get('asa_data')"
        },
        'af_deeplink':  'com.busykid.app.mobile://google/link/?request_ip_version=IP%5FV6&match_message=No%20pre%2Dinstall%20link%20matched%20for%20this%20device%2E',
        "af_timestamp": 't_in1',
        "appUID": "app_data.get('appUID')",
        "att_status": 'random.randint(0,3)',
        "bundleIdentifier": "campaign_data.get('package_name')",
        "bundlename": "campaign_data.get('bundle_name')",
        "bundleversion": "campaign_data.get('app_version_code')",
        "cell":{
            "mcc": "device_data.get('mcc')",
            "mnc": "device_data.get('mnc')",
            "ra": "''"
        },
        "counter": "str(app_data.get('counter'))",
        "currentCountrycode": "device_data.get('locale').get('country')",
        "currentLanguage"   : '''"{en}-{IN}".format(en=device_data.get('locale').get('language'), IN = device_data.get('locale').get('country'))''',
        "date1": "app_data.get('date1')",
        "date1_2": "app_data.get('date1')",
        "date2": "app_data.get('firstLaunchDate')",
        "date3": "app_data.get('firstLaunchDate')",
        "dev_key": "campaign_data.get('appsflyer').get('key')",
        "fb_anon_id": "app_data.get('fb_anon_id')",
        "deviceData": {
            "brightness": 'str(round(random.uniform(0,1),6))',
            "cpu_64bits": "true",
            "cpu_count": "2",
            "cpu_speed": "-1",
            "cpu_type": "device_data.get('cpu_type')",
            "device_model": "device_data.get('device_platform')",
            "osVersion": '''"{os} (Build {build})".format(os = device_data.get('version'), build = device_data.get('build'))''',
            "ram_size": "device_data.get('ram')",
            "dim": {
                "y_px": "int(device_data.get('resolution').split('x')[0])",
                "x_px": "int(device_data.get('resolution').split('x')[1])"
                },
        },
        "cksm_v2": "32cf2ad94b9b11f8591b0c4f1a0d401a0c481b0d4e1b",
        "cksm_v1": "32cf2ad94b9b11f8591b0c4f1a0d401a0c481b0d4e1b",
        "installReceiptData": '''json.dumps({"data":{"receipt-data":str(app_data.get('apple_reciept'))}})''',
        "model": 'iPhone',
        "meta": {
            "first_launch": {
                "from_fg": 'random.randint(400,500)',
                "init_to_fg": 'random.randint(900,1000)',
                "net": 'random.randint(500,600)'
            },
            "gcd": {
                "net": 'random.randint(3000,4000)',
                "retries": 0
            },
            "skad": {
                "s2s": {
                    "upcv": 0
                }
            },
            'att':{
                        'queued':  True ,
            },
            'rc':{
                'c_ver':      "app_data.get('cdn_ver')",
                'cdn_token':   "app_data.get('cdn_token')",
                'delay':      'delay',
                'latency':      'delay-random.randint(5,50)',
                'res_code':      200,
                'sig':      'success',
            },
            'ddl':{
                'from_fg':      'f_g',
                'net':      '[f_g+random.randint(10,20)]',
                'status':      'NOT_FOUND',
                'timeout_value':      'random.randint(1,4)*1000',
            },
        },
        "open_referrer": "''",
        'lang_code':  'en',
        "originalAppsflyerId": "app_data.get('uid')",
        "platform": "device_data.get('device_platform')",
        "platformextension": 'campaign_data.get("platformextension")',
        "ref": "campaign_data.get('ref')",
        "reinstallCounter": "0",
        "sc_o": "fu",
        "sk_rules": {
            "sk_exp": "app_data.get('sk_exp')",
            "sk_rules": "app_data.get('sk_rules')"
        },
        "serverApiVersion": "campaign_data.get('appsflyer').get('server_api_version')",
        "sessioncounter": "str(app_data.get('counter'))",
        "systemname": "iOS",
        "systemversion": "device_data.get('os_version')",
        "timepassedsincelastlaunch": 'str(timeSinceLastCall)',
        "timestamp": 't_in2',
        "disk": "app_data.get('disk')",
        "event": "Launched",
        "eventName": "Launched",
        # "eventvalue": "''",
        "prev_session_dur":"prev_session_dur",
        "firstLaunchDate": "app_data.get('firstLaunchDate')",
        "gcd_timing": "random.randint(1200,1300)",
        "iaecounter" : "str(app_data.get('iaecounter'))",
        "ivc": 'False',
        "JBDevice": 'False',
        "localizedmodel": "iPhone",
        "shortbundleversion": "campaign_data.get('app_version_name')",
        "uid": "app_data.get('uid')",
        "vendorId": "app_data.get('idfv')",
        "wifi": 'False'
    }
inapp_data = {
        "advertiserId": "app_data.get('idfa')",
        "advertiserIdEnabled": 'False',
        "af_events_api": "1",
        "af_iad_data": {
            "Version3.1": {
            "iad-attribution": "false"
            }
        },
        'fb_ddl': {
                'error': 'The operation couldn’t be completed. (com.facebook.sdk.core error 8.)', 
                'ttr': '3.31029'
        },
        "asa_data": {
            "token":"app_data.get('asa_data')"
        },
        "af_timestamp": 't_in1',
        "appUID": "app_data.get('appUID')",
        "att_status": 'random.randint(0,3)',
        "bundleIdentifier": "campaign_data.get('package_name')",
        "bundlename": "campaign_data.get('bundle_name')",
        "bundleversion": "campaign_data.get('app_version_code')",
        "cell":{
            "mcc": "device_data.get('mcc')",
            "mnc": "device_data.get('mnc')",
            "ra": "''"
        },
        "counter": "str(app_data.get('counter'))",
        "currentCountrycode": "device_data.get('locale').get('country')",
        "currentLanguage"   : '''"{en}-{IN}".format(en=device_data.get('locale').get('language'), IN = device_data.get('locale').get('country'))''',
        "date1": "app_data.get('date1')",
        "date1_2": "app_data.get('date1')",
        "date2": "app_data.get('firstLaunchDate')",
        "date3": "app_data.get('firstLaunchDate')",
        "dev_key": "campaign_data.get('appsflyer').get('key')",
        "fb_anon_id": "app_data.get('fb_anon_id')",
        "deviceData": {
            "brightness": 'str(round(random.uniform(0,1),6))',
            "cpu_64bits": "true",
            "cpu_count": "2",
            "cpu_speed": "-1",
            "cpu_type": "device_data.get('cpu_type')",
            "device_model": "device_data.get('device_platform')",
            "osVersion": '''"{os} (Build {build})".format(os = device_data.get('version'), build = device_data.get('build'))''',
            "ram_size": "device_data.get('ram')",
            "dim": {
                "y_px": "int(device_data.get('resolution').split('x')[0])",
                "x_px": "int(device_data.get('resolution').split('x')[1])"
                },
        },
        "cksm_v2": "32cf2ad94b9b11f8591b0c4f1a0d401a0c481b0d4e1b",
        "cksm_v1": "32cf2ad94b9b11f8591b0c4f1a0d401a0c481b0d4e1b",
        "installReceiptData": '''json.dumps({"data":{"receipt-data":str(app_data.get('apple_reciept'))}})''',
        "model": 'iPhone',
        "meta": {
            "first_launch": {
                "from_fg": 'random.randint(400,500)',
                "init_to_fg": 'random.randint(900,1000)',
                "net": 'random.randint(500,600)'
            },
            "gcd": {
                "net": 'random.randint(3000,4000)',
                "retries": 0
            },
            "skad": {
                "s2s": {
                    "upcv": 0
                }
            }
        },
        "open_referrer": "''",
        'lang_code':  'en',
        "originalAppsflyerId": "app_data.get('uid')",
        "platform": "device_data.get('device_platform')",
        "platformextension": 'campaign_data.get("platformextension")',
        "ref": "campaign_data.get('ref')",
        "reinstallCounter": "0",
        "sc_o": "fu",
        "sk_rules": {
            "sk_exp": "app_data.get('sk_exp')",
            "sk_rules": "app_data.get('sk_rules')"
        },
        "serverApiVersion": "campaign_data.get('appsflyer').get('server_api_version')",
        "sessioncounter": "str(app_data.get('counter'))",
        "systemname": "iOS",
        "systemversion": "device_data.get('os_version')",
        "timepassedsincelastlaunch": 'str(timeSinceLastCall)',
        "timestamp": 't_in2',
        "disk": "app_data.get('disk')",
        "event": "event__Name",
        "eventName": "event__Name",
        "eventvalue": "event__Value",
        "prev_session_dur":"prev_session_dur",
        "firstLaunchDate": "app_data.get('firstLaunchDate')",
        "gcd_timing": "random.randint(1200,1300)",
        "iaecounter" : "str(app_data.get('iaecounter'))",
        "ivc": 'False',
        "JBDevice": 'False',
        "localizedmodel": "iPhone",
        "shortbundleversion": "campaign_data.get('app_version_name')",
        "uid": "app_data.get('uid')",
        "vendorId": "app_data.get('idfv')",
        "wifi": 'False'
    }


register_data = {
        "useDevelopment": "False",
        "uid": "app_data.get('uid')",
        "advertiserId": "device_data.get('adid').upper()",
        "OriginalAppsFlyerId": "app_data.get('uid')",
        "att_status": "0",
        "sdk_version": "campaign_data.get('appsflyer').get('buildnumber')",
        "launch_counter": "0",
        "bundlename": "campaign_data.get('bundle_name')",
        'bundleversion':"campaign_data.get('app_version_name')",
        "devkey": "campaign_data.get('appsflyer').get('key')",
        "shortbundleversion": "campaign_data.get('app_version_name')",
        "topic": "campaign_data.get('package_name')",
        "wifi": "False",
        "reInstallCounter": '0',
        "devicetoken": "util.get_random_string('hex', 64)",
        "deviceData": {
            "cpu_type":"device_data.get('cpu_type')",
            "cpu_speed": "-1",
            "osVersion": "'{os} (Build {build})'.format(os = device_data.get('version'), build = device_data.get('build'))",
            "ram_size": "device_data.get('ram')",
            "cpu_64bits": "True",
            "dim": {
            "y_px": "device_data.get('resolution').split('x')[0]",
            "x_px": "device_data.get('resolution').split('x')[1]"
            },
            "brightness": "round(random.uniform(0,1),6)",
            "device_model": "device_data.get('device_type')",
            "cpu_count": "9"
        },
        "install_date": "app_data.get('date1')",
        "shortbundleversion": "campaign_data.get('app_version_name')",
        "vendorId": "app_data.get('idfv')"
    }


def update_launch(data,call_name=False):

    if call_name == "inapp":
        launch_data = inapp_data
        # input("")
    elif call_name == "register":
        launch_data = register_data
    else:
        launch_data = conversion_data
    # print(launch_data)

    
    # input("")
    # print(data)
    # input("")
    data2  = {}
    error = []
    if data.get('af_iad_data'):
        del data['af_iad_data']
    for x in data:
        tmp = data.get(x)
        if not type(tmp) == dict:
            if launch_data.get(x):
                data2[x] = launch_data.get(x)
            else:
                error.append(x)
                data2[x] = data.get(x)
        else:
            data2[x] = {}
            for z in tmp:
                # print(z)
                # tmp2 = x.get(z)
                # print(x)
                # input("")
                if launch_data.get(x).get(z):
                    # print(launch_data.get(x).get(z))
                    data2[x][z] = launch_data.get(x).get(z)
                else:
                    error.append(x+'_'+z)
                    data2[x][z] = data.get(x).get(z)
    if "af_iad_data_error"  in error:
        error.remove("af_iad_data_error")
    if "eventvalue"  in error:
        error.remove("eventvalue")
    if call_name == "inapp":
        if 'eventvalue' in data2:
            del data2['eventvalue']

    return data2,error

