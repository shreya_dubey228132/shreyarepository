# -*- coding: utf-8 -*-
#=========================#
#   A3: STILL ALIVE 
#   Author@Sandeep
#========================#
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from sdk import installtimenew
from sdk import util
from sdk import NameLists
import time
import random
import json
import urllib
import datetime
import urlparse
import uuid
import clicker
import Config
from Crypto.Cipher import AES
import base64
import string
import hashlib

#########################################################
# 			Campaign Data : dictionary					#
#														#
# 	Contains App's predefined strings like app versions,#
# 	package name, sdk and retention information, etc	#
#########################################################


campaign_data = {
    'package_name'		 : 'com.netmarble.survivalgb',
    'app_size'			 : 300,
    'app_name'			 : 'A3: STILL ALIVE',
    'app_version_name' 	 : '1.0.12',
    'app_version_code'	 : '1012',
    'app_id' 			 : '1491129341',
    'ctr' 				 : 6,
    'sdk' 		 		 : 'ios',
    'device_targeting'	 : True,
    'supported_countries': 'WW',
    'supported_os'		 : ['9.0'],
    'singular'		 	 : {
                            'version'		    : 'Singular/9.2.4-Unity/1.3.8',
                            'key'				: 'netmarblekr_9b38e90b',
                            'secret'		    : 'c52013bac93dfe2b0c9bf61a35d10452',
                            },
    'netmarble' 		:{
                            'GameCode': 'survivalgb',
                            'version': '4.6.3.1'
                        },
    # 'link'				:'https://track.adsfast.com/?aff_id=206861&offer_id=1265962&aff_sub={dp2}&aff_sub2={agent}',
    'country'	:[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
            'retention' :{			
                1:70,
                2:68,
                3:66,
                4:64,
                5:61,
                6:59,
                7:57,
                8:52,
                9:50,
                10:47,
                11:45,
                12:43,
                13:40,
                14:37,
                15:35,
                16:31,
                17:30,
                18:28,
                19:26,
                20:20,
                21:19,
                22:18,
                23:17,
                24:16,
                25:15,
                26:14,
                27:13,
                28:12,
                29:11,
                30:10,
                31:9,
                32:8,
                33:7,
                34:6,
                35:5,
            },
        }

#########################################################
# 				install() : method						#
# 				parameter : app_data,device_data 		#
# 														#
# 	Contains method calls to simulate App's behaviour	#
# 	when the App was openned for first time 			#
#########################################################

def install(app_data, device_data):	
    ###########		INITIALIZE		############	
    print "Please wait installing..."
    installtimenew.main(app_data,device_data,app_size=campaign_data.get('app_size'),os='ios'),

    make_sec(app_data,device_data)

    app_data['volume']=random.randint(0,1)

    ios_device_data = {
        'iPad mini 2':'16777228',
        'iPad mini Wi-Fi':'12',
        'iPad mini Wi-Fi + Cellular':'12',
        'iPad 4 Wi-Fi':'16777228',
        'iPad 4 Wi-Fi + Cellular':'16777228',
        'iPhone 7 Plus':'16777228',
        'iPhone 7':'16777228',
        'iPhone SE':'16777228',
        'iPhone 6s Plus':'16777228',
        'iPhone 6s':'16777228',
        'iPhone 6 Plus':'16777228',
        'iPhone 6':'16777228', 
        'iPhone 5s':'16777228', 
        'iPhone 5c':'12',
        'iPhone 5 CDMA':'12',
        'iPhone 5':'12',
        'iPhone 4s':'12',
        'iPhone 4 CDMA':'12',
        'iPhone 4':'12',
        'iPad Air':'16777228',
        'iPad mini 3':'16777228',
        'iPad Air 2':'16777228',
        'iPad mini 4':'16777228'
        }

    if not app_data.get('device_cpu_type'):
        for key in ios_device_data:
            if key == device_data.get('device'):
                app_data['device_cpu_type'] = ios_device_data.get(key)
                break
            else:
                app_data['device_cpu_type'] = '16777228'
    
    if not device_data.get('idfa_id'):
        app_data['idfa_id'] = str(uuid.uuid4()).upper()
    else:
        app_data['idfa_id'] = device_data.get('idfa_id')
        
    if not app_data.get('idfv_id'):
        app_data['idfv_id'] = str(uuid.uuid4()).upper()	

    app_data['call_run_time']=time.time()

    app_data['registration_flag']=False

    if not app_data.get('user_id'):
        app_data['user_id']=str(uuid.uuid4()).upper().replace('-','')

    if not app_data.get('session_id'):
        app_data['session_id']=str(uuid.uuid4())

    if not app_data.get('sytm'):
        app_data['sytm'] = time.time()

    if not app_data.get('singular_install_id'):
        app_data['singular_install_id']=str(uuid.uuid4()).upper()

    if not app_data.get('pid'):
        app_data['pid']=util.get_random_string('hex',32).upper()

    if not app_data.get('player_id'):
        app_data['player_id'] = str(uuid.uuid4()).upper().replace('-','')

    if not app_data.get('registration_id'):
        app_data['registration_id'] = util.get_random_string('hex',64)

    if not app_data.get('deviceKey'):
        app_data['deviceKey'] = str(uuid.uuid4()).upper().replace('-','')

    if not app_data.get('screen_size'):
        app_data['screen_size']=random.choice(['568.000000, 320.000000', '667.000000, 375.000000'])

    if not app_data.get('batteryLevel'):
        app_data['batteryLevel'] = random.uniform(55.5, 99.9)

    if not app_data.get('brightness'):
        app_data['brightness'] =  random.uniform(0,1)

    if not app_data.get('fileSystemSize'):
        app_data['fileSystemSize'] = int(random.randint(20501,40501))

    if not app_data.get('fileSystemFreeSize'):
        app_data['fileSystemFreeSize'] = int(random.randint(2222,9999))

    if not app_data.get('usedMemory'):
        app_data['usedMemory'] = int(random.randint(1111,9999))

    if not app_data.get('purgeableCount'):
        app_data['purgeableCount'] = int(random.randint(1,9))

    ################generating_realtime_differences##################

    ###########	 CALLS		 ############
    req = netmarble_versions(app_data, campaign_data, device_data)
    util.execute_request(**req)


    req = netmarble_mobileapi_getKey(app_data, campaign_data, device_data)
    resp = util.execute_request(**req)
    try:
        if resp:
            resData = json.loads(resp.get('data'))
            app_data['geoLocation']=resData.get('geoLocation')
            app_data['clientIp']=resData.get('clientIp')
            app_data['isoCity']=resData.get('geoIPs')[0].get('isoCity')
        else:
            app_data['clientIp']=device_data.get('private_ip')

    except:
        app_data['clientIp']=device_data.get('private_ip')

    time.sleep(random.randint(3,7))
    req=singularResolve(campaign_data, app_data, device_data,ifu='IDFA',retries=False)
    util.execute_request(**req)

    time.sleep(random.randint(3,7))
    req=singularResolve(campaign_data, app_data, device_data,ifu='IDFV',retries=False)
    util.execute_request(**req)

    time.sleep(random.randint(3,7))
    event={
            "event_value": {
                "install_receipt": install_receipt(),
                "scs": [
                        "nmsurvivalgb",
                        "fb758971181309801"
                    ]
            }
        }

    req=netmarble_launch(app_data, campaign_data, device_data)
    util.execute_request(**req)

    req=singularStart(campaign_data, app_data, device_data,event=event, ifu='IDFA')
    util.execute_request(**req)

    time.sleep(random.randint(3,7))
    req=singularResolve(campaign_data, app_data, device_data,ifu='IDFA',retries=False)
    util.execute_request(**req)

    time.sleep(random.randint(3,7))

    event={'event_name': '__iAd_Attribution__','event_value':{"e":json.dumps({"is_revenue_event":False,'Version3.1':{'iad-attribution': 'false'}})}}
    
    req=singular_event(campaign_data, app_data, device_data,event=event,custom_user_id=False,ifu='IDFA')
    util.execute_request(**req)

    req=netmarble_mobileauth_token(app_data, campaign_data, device_data)
    resp=util.execute_request(**req)
    try:
        if  resp: 
            resData = json.loads(resp.get('data'))
            app_data['gameToken'] = resData.get('resultData').get('accessToken')
        else:
            app_data['gameToken']=util.get_random_string(hex,426).upper()

    except:
        app_data['gameToken']=util.get_random_string(hex,426).upper()

    req=netmarble_list_select(app_data, campaign_data, device_data,kindtype='D')
    util.execute_request(**req)

    time.sleep(random.randint(1,2))

    req=netmarble_Pop(app_data, campaign_data, device_data)
    util.execute_request(**req)

    time.sleep(random.randint(1,2))

    req=netmarble_getNoticeCount(app_data, campaign_data, device_data)
    util.execute_request(**req)


    if not app_data.get('cdn_complete'):
        time.sleep(random.randint(60*2,60*3))
        cdn_complete(campaign_data, app_data, device_data, custom_user_id=False)

        req=netmarble_Push_devices(app_data, campaign_data, device_data)
        util.execute_request(**req)

    if not app_data.get('registration') and app_data.get('cdn_complete'):
        time.sleep(random.randint(30,60))
        registration(campaign_data, app_data, device_data, custom_user_id=False)

        req=netmarble_Push_Servicecode(app_data, campaign_data, device_data)
        util.execute_request(**req)

        time.sleep(random.randint(3,6))

        req=netmarble_Push_devices(app_data, campaign_data, device_data)
        util.execute_request(**req)

        time.sleep(random.randint(3,6))

        req=netmarble_mobileauth2(app_data, campaign_data, device_data)
        util.execute_request(**req)


    time.sleep(random.randint(3,8))
    heartbeat(campaign_data, app_data, device_data, custom_user_id=False)

    if not app_data.get('create_character') and app_data.get('registration') :
        time.sleep(random.randint(30,60))
        create_character(campaign_data, app_data, device_data, custom_user_id=False)

    if not app_data.get('login') and app_data.get('create_character') :
        time.sleep(random.randint(30,60))
        login(campaign_data, app_data, device_data, custom_user_id=False)
    
    time.sleep(random.randint(3,7))
    heartbeat(campaign_data, app_data, device_data, custom_user_id=False)


    start_lv,end_lv=1,random.randint(10,20)
    
    if app_data.get('level'):
        start_lv = app_data.get('level')
        end_lv = start_lv+1

    for lv in range(start_lv, end_lv):

        netmarble_netmarblelog(campaign_data, app_data, device_data,call_type='init')
        time.sleep(random.randint(1,3))

        if random.randint(1,4)<3:
            time.sleep(random.randint(10,15))
            heartbeat(campaign_data, app_data, device_data, custom_user_id=False)

        time.sleep(random.randint(30,60))
        level_achieved(campaign_data, app_data, device_data, custom_user_id=False, level=lv)

        if lv == 1:
            time.sleep(random.randint(15,30))
            user_new(campaign_data, app_data, device_data, level=None, custom_user_id=False)

            netmarble_netmarblelog(campaign_data, app_data, device_data,call_type='normal')
            time.sleep(random.randint(1,3))
    
    app_data['level'] = end_lv
    del app_data['popCount']
    app_data['installed']=True

    return {'status':'true'}



#########################################################
# 			 Open 	   : Methods 						#
# 			 parameter : app_data,device_data 			#
# 														#
# 	 Contains method calls to simulate App's behaviour	#
# 	   when the App was openned after first-open 		#
#########################################################

def open(app_data, device_data,day=1):

    if not app_data.get('installed'):
        install(app_data, device_data)

    else:
        time.sleep(random.randint(3,7))
        req=singularResolve(campaign_data, app_data, device_data,ifu='IDFA',retries=False)
        util.execute_request(**req)

        time.sleep(random.randint(3,7))
        req=singularResolve(campaign_data, app_data, device_data,ifu='IDFV',retries=False)
        util.execute_request(**req)

        time.sleep(random.randint(3,7))
        event={
                "event_value": {
                    "install_receipt": install_receipt(),
                    "scs": [
                            "nmsurvivalgb",
                            "fb758971181309801"
                        ]
                }
            }

        req=netmarble_launch(app_data, campaign_data, device_data)
        util.execute_request(**req)

        req=singularStart(campaign_data, app_data, device_data,event=event, ifu='IDFA')
        util.execute_request(**req)

        time.sleep(random.randint(3,7))
        req=singularResolve(campaign_data, app_data, device_data,ifu='IDFA',retries=False)
        util.execute_request(**req)

        time.sleep(random.randint(3,7))

        event={'event_name': '__iAd_Attribution__','event_value':{"e":json.dumps({"is_revenue_event":False,'Version3.1':{'iad-attribution': 'false'}})}}
        
        req=singular_event(campaign_data, app_data, device_data,event=event,custom_user_id=False,ifu='IDFA')
        util.execute_request(**req)

        req=netmarble_list_select(app_data, campaign_data, device_data,kindtype='D')
        util.execute_request(**req)

        time.sleep(random.randint(1,2))

        req=netmarble_Pop(app_data, campaign_data, device_data)
        util.execute_request(**req)

        time.sleep(random.randint(1,2))

        req=netmarble_getNoticeCount(app_data, campaign_data, device_data)
        util.execute_request(**req)


        if not app_data.get('cdn_complete'):
            time.sleep(random.randint(60*2,60*3))
            cdn_complete(campaign_data, app_data, device_data, custom_user_id=False)

            req=netmarble_Push_devices(app_data, campaign_data, device_data)
            util.execute_request(**req)

        if not app_data.get('registration') and app_data.get('cdn_complete'):
            time.sleep(random.randint(30,60))
            registration(campaign_data, app_data, device_data, custom_user_id=False)

            req=netmarble_Push_Servicecode(app_data, campaign_data, device_data)
            util.execute_request(**req)

            time.sleep(random.randint(3,6))

            req=netmarble_Push_devices(app_data, campaign_data, device_data)
            util.execute_request(**req)

            time.sleep(random.randint(3,6))

            req=netmarble_mobileauth2(app_data, campaign_data, device_data)
            util.execute_request(**req)


        time.sleep(random.randint(3,8))
        heartbeat(campaign_data, app_data, device_data, custom_user_id=False)

        if not app_data.get('create_character') and app_data.get('registration') :
            time.sleep(random.randint(30,60))
            create_character(campaign_data, app_data, device_data, custom_user_id=False)

        if not app_data.get('login') and app_data.get('create_character') :
            time.sleep(random.randint(30,60))
            login(campaign_data, app_data, device_data, custom_user_id=False)
        
        time.sleep(random.randint(3,7))
        heartbeat(campaign_data, app_data, device_data, custom_user_id=False)


        start_lv,end_lv=1,random.randint(10,20)
        
        if app_data.get('level'):
            start_lv = app_data.get('level')
            end_lv = start_lv+1

        for lv in range(start_lv, end_lv):

            netmarble_netmarblelog(campaign_data, app_data, device_data,call_type='init')
            time.sleep(random.randint(1,3))

            if random.randint(1,4)<3:
                time.sleep(random.randint(10,15))
                heartbeat(campaign_data, app_data, device_data, custom_user_id=False)

            time.sleep(random.randint(30,60))
            level_achieved(campaign_data, app_data, device_data, custom_user_id=False, level=lv)

            if lv == 1:
                time.sleep(random.randint(15,30))
                user_new(campaign_data, app_data, device_data, level=None, custom_user_id=False)

                netmarble_netmarblelog(campaign_data, app_data, device_data,call_type='normal')
                time.sleep(random.randint(1,3))
        
        app_data['level'] = end_lv

    return {'status':'true'}


################################################################
# EVENT DEFINITION
#
# Define all the event's call below
################################################################

def cdn_complete(campaign_data, app_data, device_data, custom_user_id=False):
    event = {'event_name': 'cdn_complete', 'event_value': ({"e":json.dumps({"is_revenue_event":False})})}
    print 'Apsalr : EVENT______________________________cdn_complete'
    request=singular_event(campaign_data, app_data, device_data,event=event)
    util.execute_request(**request)
    app_data['cdn_complete'] = True

def registration(campaign_data, app_data, device_data, custom_user_id=False):
    event = {'event_name': 'registration', 'event_value': ({"e":json.dumps({"is_revenue_event":False})})}
    print 'Apsalr : EVENT______________________________registration'
    request=singular_event(campaign_data, app_data, device_data,event=event)
    util.execute_request(**request)
    app_data['registration'] = True


def create_character(campaign_data, app_data, device_data, custom_user_id=False):
    event = {'event_name': 'create_character', 'event_value':({"e":json.dumps({"is_revenue_event":False})})}
    print 'Apsalr : EVENT______________________________create_character'
    request=singular_event(campaign_data, app_data, device_data,event=event)
    util.execute_request(**request)
    app_data['create_character'] = True

def login(campaign_data, app_data, device_data, custom_user_id=False):
    event = {'event_name': 'login', 'event_value':({"e":json.dumps({"is_revenue_event":False})})}
    print 'Apsalr : EVENT______________________________login'
    request=singular_event(campaign_data, app_data, device_data,event=event)
    util.execute_request(**request)
    app_data['login'] = True

def heartbeat(campaign_data, app_data, device_data, custom_user_id=False):
    event = {'event_name': 'heartbeat', 'event_value': ({"e":json.dumps({})})}
    print 'Apsalr : EVENT______________________________heartbeat'
    request=singular_event(campaign_data, app_data, device_data,event=event)
    util.execute_request(**request)
    app_data['heartbeat'] = True


def level_achieved(campaign_data, app_data, device_data, custom_user_id=False, level=1):
    event = {'event_name': 'level_achieved', 'event_value': ({"e":json.dumps({"Level": level,"is_revenue_event": False})})}
    print 'Apsalr : EVENT______________________________level_achieved'
    request=singular_event(campaign_data, app_data, device_data,event=event, custom_user_id=False)
    util.execute_request(**request)
    app_data['level_achieved'] = True


def user_new(campaign_data, app_data, device_data, level=None, custom_user_id=False):
    event = {'event_name': 'user_new', 'event_value': ({"e":json.dumps({"is_revenue_event":False})})}
    print 'Apsalr : EVENT______________________________user_new'
    request=singular_event(campaign_data, app_data, device_data,event=event)
    util.execute_request(**request)
    app_data['user_new'] = True


def generate_pushRegistrationID(app_data):
    
    if not (app_data.get('pushRegistrationID')):
        app_data['pushRegistrationID']='fNAxX23-6F4:APA9' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(136))

    return app_data.get('pushRegistrationID') 

#===============================================#
#				netmarble Calls					#
#===============================================#
def netmarble_mobileapi_getKey(app_data, campaign_data, device_data):
    method = 'get'
    url = 'https://mobileapi.netmarble.com/v2/commonCs/getKey'

    headers={
            'GameCode': campaign_data.get('netmarble').get('GameCode'),
            'User-Agent':  urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
            'Accept-Encoding': 'gzip, deflate, br',
            'Content-Type': 'application/x-www-form-urlencoded'

        }
    params={
            'countryCode':'JP',
            'gameCode':campaign_data.get('netmarble').get('GameCode'),#survivalgb
            'localeCode':'ko_kr',
            'serviceCode':'crashReport',
            'version':'iOS_v1.3.3',
            'zone':	'real'
        }

    data=None

    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':data}


def netmarble_versions(app_data, campaign_data, device_data):
    method = 'get'
    url = 'https://apis.netmarble.com/gmc2/v4/constants/netmarbles/versions/4.6'
    headers={
            'GameCode': campaign_data.get('netmarble').get('GameCode'),
            # 'Accept': 'application/json',
            'User-Agent':  urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
            'Accept-Encoding': 'gzip, deflate, br'
        }
    params={
            'countryCode': 'JP',
            'localeCode': 'ko_kr',
            'timezone':	device_data.get('local_tz_name')
        }

    data={}

    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':data}

def netmarble_launch(app_data, campaign_data, device_data):
    if not app_data.get('deviceKey'):
        app_data['deviceKey'] = str(uuid.uuid4()).upper().replace('-','')

    method = 'get'
    url = 'http://api.at.netmarble.net/ver1/launch'
    headers={
            'NMLocalizedLevel': '1',
            'NetworkStatus': device_data.get('network').upper(),#WIFI
            'NMPlayerID': app_data.get('user_id'),#D5959B8E15EA449381FA5923B584B0B8
            'NMGameCode': campaign_data.get('netmarble').get('GameCode'),#survivalgb
             'locale': device_data.get('locale').get('language'),#+'_'+device_data.get('locale').get('country').upper(),
            'platform': 'iOS',
            'NMSDKVersion': campaign_data.get('netmarble').get('version'),#4.6.3.1
            'NMLanguage': device_data.get('locale').get('language'),#+'_'+device_data.get('locale').get('country').upper(),
            'NMManufacturer':"Apple" ,
            'countryCode': app_data.get('geoLocation'),
            'NMModel': device_data.get('model'),
            'NMWorld': '211',
            'NMRegion':'',
            'ScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMDeviceKey': app_data.get('deviceKey'),
            'NMCountryCode': app_data.get('geoLocation'),
            'NMPlatform': 'iOS',
            'DeviceModel': device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'NMMarketType': 'AppStore',
            'NMChannelUserID': urllib.quote('{}'),
            'NMDeviceLanguage': device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country').upper() or 'en-JP',
            'module': 'sdk',
            'NMOSVersion': device_data.get('os_version'),
            'version': campaign_data.get('netmarble').get('version'),#4.6.3.1
            'NS_Lang': device_data.get('locale').get('language'), #+'_'+device_data.get('locale').get('country').upper(),
            'TimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NS_Region':'',
            'NMScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMNetworkStatus': device_data.get('network').upper(),#WIFI,
            'NMTimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NMDeviceModel': device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'GeoLocation': app_data.get('geoLocation'),#IN
            'NMCity':app_data.get('isoCity'),
            'NMJoinedCountryCode':'',
            'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
            'Accept-Encoding': 'gzip, deflate'
        }
    params={
        'deviceBrand':'Apple',
        'deviceKey': app_data.get('deviceKey'),#'D5959B8E15EA449381FA5923B584B0B8',
        'deviceModel': 'iPhone',
        'deviceOsVer': device_data.get('os_version'),#14.0,
        'pId': app_data.get('user_id'),#'D5959B8E15EA449381FA5923B584B0B8',
        'platformAdId':	app_data.get('idfa_id'),#'8DE9D78A-BC2A-4C96-BDC3-B092775291AC',
        'targetGameCode': campaign_data.get('netmarble').get('GameCode'),
        'userId': app_data.get('user_id'),#'D5959B8E15EA449381FA5923B584B0B8',

        }

    data=None

    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':data}


def netmarble_mobileauth_token(app_data, campaign_data, device_data):
    method = 'get'
    url = 'https://apis.netmarble.com/mobileauth/v2/players/'+str(app_data.get('user_id'))+'/deviceKeys/'+'9F72D14A33BB4D78BD0278624734234E/accessToken'
    headers={
            'GameCode':campaign_data.get('netmarble').get('GameCode'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
            'Accept-Encoding': 'gzip, deflate, br',
        }
    params={
            'nmDeviceKey':app_data.get('user_id'),#D5959B8E15EA449381FA5923B584B0B8
            'countryCode': app_data.get('geoLocation'),#IN
            'adId':app_data.get('idfa_id'),#8DE9D78A-BC2A-4C96-BDC3-B092775291AC
        }

    data={}

    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':data}

def netmarble_list_select(app_data, campaign_data, device_data, kindtype=None):
    method = 'post'
    url = 'http://marbleproduct.netmarble.com/product/product_list_select'
    headers={
            'NMLocalizedLevel': '',
            'NetworkStatus': device_data.get('network').upper(),#WIFI
            'NMPlayerID': app_data.get('user_id'),#D5959B8E15EA449381FA5923B584B0B8
            'NMGameCode': campaign_data.get('netmarble').get('GameCode'),#survivalgb
            'locale': device_data.get('locale').get('language'),#+'_'+device_data.get('locale').get('country').upper(),
            'platform': 'iOS',
            'NMSDKVersion': campaign_data.get('netmarble').get('version'),
            'NMCharacterID': app_data.get('I_NMCharacterID'),#21120111900006288
            'NMLanguage': device_data.get('locale').get('language'),#+'_'+device_data.get('locale').get('country').upper(),
            'NMManufacturer': "Apple",
            'countryCode': app_data.get('geoLocation'),
            'NMModel': device_data.get('model'),
            'NMWorld': '211',
            'NMRegion': 'AS2',
            'ScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMDeviceKey': app_data.get('deviceKey'),
            'NMCountryCode': app_data.get('geoLocation'),
            'NMPlatform': 'iOS',
            'DeviceModel': device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'Cookie': '_ga=GA1.2.895125332.1605762456; _gid=GA1.2.676603507.1605762456; NMLanguage={0}; NS_Lang={1}; NMJoinedCountryCode={2}; NMRegion=AS2; NS_Region=AS2'.format(device_data.get('locale').get('language'), device_data.get('locale').get('language'), device_data.get('locale').get('country')),
            'NMMarketType': 'AppStore',
            'NMChannelUserID': urllib.quote('{}'),
            'NMDeviceLanguage': device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country').upper() or 'en-JP',
            'module': 'sdk',
            'NMOSVersion': device_data.get('os_version'),
            'version': campaign_data.get('netmarble').get('version'),
            'NS_Lang': device_data.get('locale').get('language'), #+'_'+device_data.get('locale').get('country').upper(),
            'TimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NS_Region': 'AS2',
            'NMScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMNetworkStatus': device_data.get('network').upper(),
            'NMTimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NMDeviceModel': device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'GeoLocation': app_data.get('geoLocation'),
            'NMCity':app_data.get('isoCity'),
            'NMJoinedCountryCode':'',
            'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
            'Accept-Encoding': 'gzip, deflate',
        }
    params={
        }

    data={
        'param':json.dumps({
                "gameCode":campaign_data.get('netmarble').get('GameCode'),
                "source":"store",
                "kindType":kindtype,#"D"
                "storeCd":"appstore",
                "userKey":app_data.get('user_id')
                })
        }

    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':data}

def netmarble_Pop(app_data, campaign_data, device_data):
    if not app_data.get('popCount'):
        app_data['popCount'] = 1

    method = 'get'
    url = 'http://popup.netmarble.com/v4/marblePop'
    headers={
            'NMLocalizedLevel': '',
            'NetworkStatus': device_data.get('network').upper(),#WIFI
            'NMPlayerID': app_data.get('user_id'),#D5959B8E15EA449381FA5923B584B0B8
            'NMGameCode': campaign_data.get('netmarble').get('GameCode'),#survivalgb
            'locale': device_data.get('locale').get('language'),#+'_'+device_data.get('locale').get('country').upper(),
            'platform': 'iOS',
            'NMSDKVersion': campaign_data.get('netmarble').get('version'),#4.6.3.1
            'NMLanguage': device_data.get('locale').get('language'),#+'_'+device_data.get('locale').get('country').upper(),
            'NMManufacturer': "Apple",
            'countryCode': app_data.get('geoLocation'),
            'NMModel': device_data.get('model'),
            'NMWorld': '211',
            'NMRegion': 'AS2',
            'C': 'sdk',
            'ScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMDeviceKey': app_data.get('deviceKey'),
            'NMCountryCode': app_data.get('geoLocation'),
            'NMPlatform': 'iOS',
            'DeviceModel': device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'Cookie': 'NMJoinedCountryCode={0}; NMLanguage={1}; NMRegion=AS2; NS_Lang={2}; NS_Region=AS2'.format(device_data.get('locale').get('country'), device_data.get('locale').get('language'), device_data.get('locale').get('language')),
            'NMMarketType': 'AppStore',
            'NMChannelUserID': urllib.quote('{}'),
            'NMDeviceLanguage': device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country').upper() or 'en-JP',
            'module': 'sdk',
            'NMOSVersion': device_data.get('os_version'),
            'version': campaign_data.get('netmarble').get('version'),#4.6.3.1
            'NS_Lang': device_data.get('locale').get('language'), #+'_'+device_data.get('locale').get('country').upper(),
            'TimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NS_Region': 'AS2',
            'NMScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMNetworkStatus': device_data.get('network').upper(),#WIFI,
            'NMTimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NMDeviceModel': device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'GeoLocation': app_data.get('geoLocation'),#IN
            'NMCity':app_data.get('isoCity'),
            'NMJoinedCountryCode':app_data.get('geoLocation') or '',#IN'',
            'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
            'Accept-Encoding': 'gzip, deflate',
        }
    params = None
    if app_data.get('popCount') %2 != 0:
        params = {
                'clientId':app_data.get('user_id'),#	4474F39D29D54424B2BC00C33F1537F9
                'gameCode':	campaign_data.get('netmarble').get('GameCode'),#survivalgb
                'inReview':	'false',
                'market': 'ios',
                'osType': 'ios',
                'platform': 'netmarbles',
                'version': campaign_data.get('app_version_name'),#1.0.12
            }
    else:
        params={}

    data = None

    if app_data.get('popCount') %2 == 0: 
        data = json.dumps({
                "clientId": app_data.get('user_id'),#"4474F39D29D54424B2BC00C33F1537F9",
                "exposureData": [
                    {
                        "exposures": [],
                        "playerId": app_data.get('user_id'),#"4474F39D29D54424B2BC00C33F1537F9"
                    }
                ],
                "gameCode": campaign_data.get('netmarble').get('GameCode'),#"survivalgb",
                "inReview": "false",
                "location": 14 if app_data.get('popCount')==1 else 22 ,
                "market": "ios",
                "osType": "ios",
                "platform": "netmarbles",
                "version": campaign_data.get('app_version_name'),#"1.0.12"
            })
    else:
        data={}

    app_data['popCount'] += 1

    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':data}

def netmarble_getNoticeCount(app_data, campaign_data, device_data):
    method = 'post'
    url = 'http://noticeboard.netmarble.com/mobileNotice/survivalgb/getNoticeCount'
    headers={
            'NMLocalizedLevel': '',
            'NetworkStatus': device_data.get('network').upper(),#WIFI
            'NMPlayerID': app_data.get('user_id'),#D5959B8E15EA449381FA5923B584B0B8
            'NMGameCode': campaign_data.get('netmarble').get('GameCode'),#survivalgb
            'locale': device_data.get('locale').get('language'), #+'_'+device_data.get('locale').get('country').upper(),
            'platform': 'iOS',
            'NMSDKVersion': campaign_data.get('netmarble').get('version'),#4.6.3.1
            'NMLanguage': device_data.get('locale').get('language'),#+'_'+device_data.get('locale').get('country').upper(),
            'NMManufacturer': "Apple",
            'countryCode': app_data.get('geoLocation'),
            'NMModel': device_data.get('model'),
            'NMWorld': '211',
            'NMRegion': 'AS2',
            'ScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMDeviceKey': app_data.get('deviceKey'),
            'NMCountryCode': app_data.get('geoLocation'),
            'NMPlatform': 'iOS',
            'DeviceModel': device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'Cookie': 'NMLanguage={0}; NS_Lang={1}; NMJoinedCountryCode={2}; NMRegion=AS2; NS_Region=AS2'.format(device_data.get('locale').get('language'), device_data.get('locale').get('language'),device_data.get('locale').get('country')),
            'NMMarketType': 'AppStore',
            'NMChannelUserID': urllib.quote('{}'),
            'NMDeviceLanguage': device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country').upper() or 'en-JP',
            'module': 'sdk',
            'NMOSVersion': device_data.get('os_version'),
            'version': campaign_data.get('netmarble').get('version'),#4.6.3.1
            'NS_Lang': device_data.get('locale').get('language'), #+'_'+device_data.get('locale').get('country').upper(),
            'TimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NS_Region': 'AS2',
            'NMScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMNetworkStatus': device_data.get('network').upper(),#WIFI,
            'NMTimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NMDeviceModel': device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'GeoLocation': app_data.get('geoLocation'),#IN
            'NMCity':app_data.get('isoCity'),
            'NMJoinedCountryCode':app_data.get('geoLocation'),#IN'',
            'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
            'Accept-Encoding': 'gzip, deflate'
        }
    params={}

    data={
            'marketType':'AppStore'
        }

    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':data}

def netmarble_Push_devices(app_data, campaign_data, device_data):
    method = 'post'
    url = 'http://push.netmarble.com/v3.1/devices'
    headers={
            'NMLocalizedLevel': '',
            'NetworkStatus': device_data.get('network').upper(),#WIFI
            'NMPlayerID': app_data.get('user_id'),#D5959B8E15EA449381FA5923B584B0B8
            'NMGameCode': campaign_data.get('netmarble').get('GameCode'),#survivalgb
             'locale': device_data.get('locale').get('language'),#+'_'+device_data.get('locale').get('country').upper(),
            'platform': 'iOS',
            'NMSDKVersion': campaign_data.get('netmarble').get('version'),#4.6.3.1
            'NMLanguage': device_data.get('locale').get('language'),#+'_'+device_data.get('locale').get('country').upper(),
            'NMManufacturer': "Apple",
            'countryCode': app_data.get('geoLocation'),
            'NMModel': device_data.get('model'),
            'NMWorld': '211',
            'NMRegion': 'AS2',
            'ScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMDeviceKey': app_data.get('deviceKey'),
            'NMCountryCode': app_data.get('geoLocation'),
            'NMPlatform': 'iOS',
            'DeviceModel': device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'Cookie': 'NMJoinedCountryCode={0}; NMLanguage={1}; NMRegion=AS2; NS_Lang={2}; NS_Region=AS2'.format(device_data.get('locale').get('country'), device_data.get('locale').get('language'), device_data.get('locale').get('language')),
            'NMMarketType': 'AppStore',
            'NMChannelUserID': urllib.quote('{}'),
            'NMDeviceLanguage': device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country').upper() or 'en-JP',
            'module': 'sdk',
            'NMOSVersion': device_data.get('os_version'),
            'gameVersion': campaign_data.get('app_version_name'),
            'gameToken': app_data.get('gameToken'),
            'version': campaign_data.get('netmarble').get('version'),#4.6.3.1
            'NS_Lang': device_data.get('locale').get('language'), #+'_'+device_data.get('locale').get('country').upper(),
            'TimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NS_Region': 'AS2',
            'NMScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMNetworkStatus': device_data.get('network').upper(),#WIFI,
            'NMTimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NMDeviceModel': device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'GeoLocation': app_data.get('geoLocation'),#IN
            'NMCity':app_data.get('isoCity'),
            'NMJoinedCountryCode':app_data.get('geoLocation'),#IN,
            'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
            'Accept-Encoding': 'gzip, deflate',
            'Content-Type': 'application/json'
        }
    params={
        }

    data={
    "pushInformation": {
        "channelCode": 100,
        "deviceKey": device_data.get('user_id'),#"D5959B8E15EA449381FA5923B584B0B8",
        "operateType": "U",
        "pushAllowFlag": 1000111,
        "pushRegistrationID": generate_pushRegistrationID(app_data),#"fNAxX23-6F4:APA91bHNUWn_7cq6SjIvdoQogGXmu5ezwCiLxvxZdh7bO4kVZJMyG6ff_85c6kY_2T6IkLG30WIG12pvd9b0QQPq-JB0mESremc2EkMoqnMt-vspz9sIzTZGAsRbxF7TIk5jKqwBdPKJ",
        "pushServiceType": 0,
        "serviceCode": campaign_data.get('netmarble').get('GameCode'),#"survivalgb",
        "userKey": app_data.get('user_id'),#"4474F39D29D54424B2BC00C33F1537F9"
    }
}
    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':json.dumps(data)}


def netmarble_Push_Servicecode(app_data, campaign_data, device_data):
    method = 'get'
    url = 'http://push.netmarble.com/v1/push/allow'
    headers={
            'NMLocalizedLevel': '',
            'NetworkStatus': device_data.get('network').upper(),#WIFI
            'NMPlayerID': app_data.get('user_id'),#D5959B8E15EA449381FA5923B584B0B8
            'NMGameCode': campaign_data.get('netmarble').get('GameCode'),#survivalgb
             'locale': device_data.get('locale').get('language'),#+'_'+device_data.get('locale').get('country').upper(),
            'platform': 'iOS',
            'NMSDKVersion': campaign_data.get('netmarble').get('version'),#4.6.3.1
            'NMLanguage': device_data.get('locale').get('language'),#+'_'+device_data.get('locale').get('country').upper(),
            'NMManufacturer': "Apple",
            'countryCode': app_data.get('geoLocation'),
            'NMModel': device_data.get('model'),
            'NMWorld': '211',
            'NMRegion': 'AS2',
            'ScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMDeviceKey': app_data.get('deviceKey'),
            'NMCountryCode': app_data.get('geoLocation'),
            'NMPlatform': 'iOS',
            'DeviceModel': device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'Cookie': 'NMLanguage={0}; NS_Lang={1}; NMJoinedCountryCode={2}; NMRegion=AS2; NS_Region=AS2'.format(device_data.get('locale').get('language'), device_data.get('locale').get('language'), device_data.get('locale').get('country')),
            'NMMarketType': 'AppStore',
            'NMChannelUserID': urllib.quote('{}'),
            'NMDeviceLanguage':device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country').upper() or 'en-JP', #'ko_KR' ,
            'module': 'sdk',
            'NMOSVersion': device_data.get('os_version'),
            'gameVersion': campaign_data.get('app_version_name'),
            'gameToken': app_data.get('gameToken'),#'0AB0015C72AC4FD353DFB3D47090C87E1E4E8190597C96324BA9AE6B2ED784AA40CA915DA295CA51093979281A1ECD6A3EB5540595E3A31CFA2C91C5A4512082E0EB66F4F6AA1908F0DC988CB73D4885367855BEF89ADD9D36D842B2DF7874BF8845320F777C5219910AAE757CB24E78F696A459B9C6F1E30FAD8D91218D4B06B5CC8CD835B649E4A34A4F9ED02F2B5DA19DED71406843B8E819FDFDC891CE66F75046C34630236B7E0B500C15EA7F49A4297612209BF088BCD16ACB33EEC0A6882ADFE1AE2E86E9605341D96A79214820378D412A',
            'version': campaign_data.get('netmarble').get('version'),#4.6.3.1
            'NS_Lang': device_data.get('locale').get('language'), #+'_'+device_data.get('locale').get('country').upper(),
            'TimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NS_Region': 'AS2',
            'NMScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMNetworkStatus': device_data.get('network').upper(),#WIFI,
            'NMTimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NMDeviceModel':device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'GeoLocation': app_data.get('geoLocation'),#IN
            'NMCity':app_data.get('isoCity'),
            'NMJoinedCountryCode':app_data.get('geoLocation'),#IN
            # 'Content-Type': 'application/json; charset=utf-8',
            'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
            'Accept-Encoding': 'gzip, deflate'
        }

    params={

            "serviceCode":campaign_data.get('netmarble').get('GameCode'),
            'registrationId':app_data.get('registration_id'),
            "userKey":app_data.get('user_id')
        }

    data={}
    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':data}

def netmarble_mobileauth2(app_data, campaign_data, device_data):
    method = 'put'
    url = 'https://apis.netmarble.com/mobileauth/v2/players/'+str(app_data.get('user_id'))+'/deviceKeys/9F72D14A33BB4D78BD0278624734234E/accessToken'
    headers={
    'GameCode': campaign_data.get('netmarble').get('GameCode'),#survivalgb
    'Accept': 'application/json',
    'AccessToken': app_data.get('gameToken'),#0AB0019A5206C0836CC26473F55FEF9BAFE70FA2532D0AC273FF638D9A8D051660FF1C9713220F94698D336AF770B03677F769BEF72E009930F972F8883D82DB0F26C138992C3CC63D7ACE19B30E49418B6C0A52654B32F96D4BCFEA56A07C6DFC2BB1E99AEBC17ABA61A968B285AAF913F3694C9E11C3B0EFDC2EA4E0C5759A88575CF5AE12A765BCEBDF5CD239F5305C2A88B1DC92BE4E94A31B10E610F5917DBCE1379E7A7B8A82E4EE1D3BEA00B762709F1220150828B8A3FFC2DF2435F0E949B810B62FBF7B45A0EFD3D375ADE595F27534C5
    'Content-Type': 'application/json',
    'Cookie': '_ga=GA1.2.895125332.1605762456; _gid=GA1.2.676603507.1605762456; NMLanguage={0}; NS_Lang={1}; NMJoinedCountryCode={2}; NMRegion=AS2; NS_Region=AS2'.format(device_data.get('locale').get('language'), device_data.get('locale').get('language'), device_data.get('locale').get('country')),
    'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
    'Accept-Encoding': ' gzip, deflate, br',
        }

    params={}

    data={}

    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':data}


def netmarble_Push_devices2(app_data, campaign_data, device_data):
    method = 'post'
    url = 'http://push.netmarble.com/v3.1/devices'
    headers={
            'NMLocalizedLevel': '',
            'NetworkStatus': device_data.get('network').upper(),#WIFI
            'NMPlayerID': app_data.get('user_id'),#D5959B8E15EA449381FA5923B584B0B8
            'NMGameCode': campaign_data.get('netmarble').get('GameCode'),#survivalgb
            'locale': device_data.get('locale').get('language'),#+'_'+device_data.get('locale').get('country').upper(),
            'platform': 'iOS',
            'NMSDKVersion': campaign_data.get('netmarble').get('version'),#4.6.3.1
            'NMLanguage': device_data.get('locale').get('language'),#+'_'+device_data.get('locale').get('country').upper(),
            'NMManufacturer': "Apple",
            'countryCode': app_data.get('geoLocation'),
            'NMModel': device_data.get('model'),
            'NMWorld': '211',
            'NMRegion': 'AS2',
            'ScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMDeviceKey': app_data.get('deviceKey'),
            'NMCountryCode': app_data.get('geoLocation'),
            'NMPlatform': 'iOS',
            'DeviceModel': device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'Cookie': 'NMJoinedCountryCode={0}; NMLanguage={1}; NMRegion=AS2; NS_Lang={2}; NS_Region=AS2'.format(device_data.get('locale').get('country'), device_data.get('locale').get('language'), device_data.get('locale').get('language')),
            'NMMarketType': 'AppStore',
            'NMChannelUserID': urllib.quote('{}'),
            'NMDeviceLanguage': device_data.get('locale').get('language')+'_'+device_data.get('locale').get('country').upper() or 'en-JP',
            'module': 'sdk',
            'NMOSVersion': device_data.get('os_version'),
            'gameVersion': campaign_data.get('app_version_name'),
            'gameToken': app_data.get('gameToken'),#'0AB0015C72AC4FD353DFB3D47090C87E1E4E8190597C96324BA9AE6B2ED784AA40CA915DA295CA51093979281A1ECD6A3EB5540595E3A31CFA2C91C5A4512082E0EB66F4F6AA1908F0DC988CB73D4885367855BEF89ADD9D36D842B2DF7874BF8845320F777C5219910AAE757CB24E78F696A459B9C6F1E30FAD8D91218D4B06B5CC8CD835B649E4A34A4F9ED02F2B5DA19DED71406843B8E819FDFDC891CE66F75046C34630236B7E0B500C15EA7F49A4297612209BF088BCD16ACB33EEC0A6882ADFE1AE2E86E9605341D96A79214820378D412A',
            'version': campaign_data.get('netmarble').get('version'),#4.6.3.1
            'NS_Lang': device_data.get('locale').get('language'), #+'_'+device_data.get('locale').get('country').upper(),
            'TimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NS_Region': 'AS2',
            'NMScreenSize': app_data.get('screen_size'),#568.000000, 320.000000
            'NMNetworkStatus': device_data.get('network').upper(),#WIFI,
            'NMTimeZone': device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],
            'NMDeviceModel': device_data.get('device_platform','iPhone8,4'),#iPhone8,4
            'GeoLocation': app_data.get('geoLocation'),#IN
            'NMCity':app_data.get('isoCity'),
            'NMJoinedCountryCode':app_data.get('geoLocation'),#IN,
            'Content-Type': 'application/json; charset=utf-8',
            'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
            'Accept-Encoding': 'gzip, deflate',
        }
    params={
        }

    data={
        "pushInformation":{
            "serviceCode":campaign_data.get('netmarble').get('GameCode'),#"survivalgb",
            "channelCode":100,
            "deviceKey": app_data.get('user_id'),#"D5959B8E15EA449381FA5923B584B0B8",
            "pushServiceType":1,
            "pushAllowFlag":1000111,
            "pushRegistrationID":app_data.get('pushRegistrationID'),#"fNAxX23-6F4:APA91bHNUWn_7cq6SjIvdoQogGXmu5ezwCiLxvxZdh7bO4kVZJMyG6ff_85c6kY_2T6IkLG30WIG12pvd9b0QQPq-JB0mESremc2EkMoqnMt-vspz9sIzTZGAsRbxF7TIk5jKqwBdPKJ",
            "userKey": app_data.get('deviceKey'),#"4474F39D29D54424B2BC00C33F1537F9",
            "operateType":"U"
            }
        }

    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':json.dumps(data)}

def netmarble_netmarblelog(campaign_data, app_data, device_data,marketProductCode='',payAmount='',payOrderNo='',call_type=''):
    app_data['keep_time1']=time.time()

    if not app_data.get('sytm'):
        app_data['sytm'] = time.time()

    url = 'https://netmarbleslog.netmarble.com/'
    method = 'post'
    headers={
        'Charset':'UTF-8',
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        'Accept-Encoding':'gzip',
        'User-Agent':urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],
        }

    if not app_data.get('batteryLevel'):
        app_data['batteryLevel'] = random.uniform(55.5, 99.9)

    if not app_data.get('brightness'):
        app_data['brightness'] =  random.uniform(0,1)

    if not app_data.get('fileSystemSize'):
        app_data['fileSystemSize'] = int(random.randint(20501,40501))

    if not app_data.get('fileSystemFreeSize'):
        app_data['fileSystemFreeSize'] = int(random.randint(2222,9999))

    if not app_data.get('usedMemory'):
        app_data['usedMemory'] = int(random.randint(1111,9999))

    if not app_data.get('purgeableCount'):
        app_data['purgeableCount'] = int(random.randint(1,9))

    if not app_data.get('I_NMSessionID'):
        app_data['I_NMSessionID']=str(uuid.uuid4()).lower().replace('-','')

    if not app_data.get('I_NMCharacterID'):
        app_data['I_NMCharacterID']='2112011'+str(random.randint(1111111111,9999999999)) #21120111900006288

    if not app_data.get('I_LogKey'):
        app_data['I_LogKey']=str(uuid.uuid4()).lower().replace('-','')

    if not app_data.get('I_TID'):
        app_data['I_TID']=str(uuid.uuid4()).upper().replace('-','')
    
        if not app_data.get('I_LogId'):
            app_data['I_LogId']=0

        elif random.randint(1,4)<2:
            app_data['I_LogId']=app_data['I_LogId']+1

        if not app_data.get('I_LogDetailId'):
            app_data['I_LogDetailId']=0

        elif random.randint(1,4)<2:
            app_data['I_LogDetailId']=app_data['I_LogDetailId']+1

    if not app_data.get('systemUptime'):
        app_data['systemUptime'] = int(time.time()-app_data.get('sytm'))


    datenow=datetime.datetime.fromtimestamp(app_data.get('keep_time1')+1).strftime("%Y-%m-%d %H:%M:%S:%f")[:-3]
    params=None


    #==call_type= normal, payInit, paySuccess
    if call_type=='paySuccess':
         I_LogDes={
                "GameCode": campaign_data.get('netmarble').get('GameCode'),#"survivalgb",
                "marketAmountMicros": str(int(payAmount)*3939),#"39000000",
                "marketCurrency": "USD",
                "marketName": "AppStore",
                "marketOrderNo": "90360286420"+str(random.randint(11111111,99999999))+".34023285"+str(random.randint(11111111,99999999)),
                "marketProductCode": marketProductCode,#"survivalgb_user_daily_21",
                "payOrderNo": payOrderNo,#"2200318055443007995",
                "resultCode": 0,
                "resultMessage": "Success"
            }

    if call_type=='payInit':
        I_LogDes = {
            "productType": "NMP",
            "marketProductCode": marketProductCode,#"survivalgb_user_daily_21",
            "payAmount": payAmount,#9900,
            "payOrderNo": payOrderNo,#"2200318055443007995",
            "GameCode": campaign_data.get('netmarble').get('GameCode'),#"survivalgb",
            "payCurrency": "KRW"
        }

    if call_type=='init':
        I_LogDes = {
                "GameCode": campaign_data.get('netmarble').get('GameCode'),#"survivalgb",
                "SDKType": "Default",
                "zone": "real"
        }

    if call_type=='normal':
        I_LogDes={
                "activeCount": 604,
                "activeProcessorCount": 2,
                "batteryLevel": app_data.get('batteryLevel'),#60.000003814697266,
                "batteryState": 2,
                "brightness": app_data.get('brightness'),#0.40650951862335205,
                "currentRadioAccessTechnology": "",
                "fileSystemFreeSize": app_data.get('fileSystemFreeSize'),#2374,
                "fileSystemSize": app_data.get('fileSystemSize'),#	#30501,
                "freeCount": 36,
                "GameCode": campaign_data.get('netmarble').get('GameCode'),#"survivalgb",
                "inactiveCount": 580,
                "kSCNetworkReachabilityFlags": 2,
                "outputVolume": 0,
                "processorCount": device_data.get('cpu').get('core'),#2,
                "purgeableCount": app_data.get('purgeableCount'),#8,
                "systemUptime": app_data.get('systemUptime'),#6997,
                "thermalState": 1,
                "usedMemory": app_data.get('usedMemory'),#1537,
                "wireCount": 352
            }

        # print"========== I_LogDes ===========\n"
        # print I_LogDes

    data ={
            "I_ConnectIP": "0",
            "I_GameCode": "netmarbles",
            "I_LogDes": [
                {
                    "I_ChannelType": "100",
                    "I_City": app_data.get('isoCity') or "",#"UP",
                    "I_ConnectIP":app_data.get('clientIp') or "",# "223.233.106.188",
                    "I_CountryCD": app_data.get('geocountry') or "",#"IN",
                    "I_DeviceModel": device_data.get('device_platform','iPhone8,4'),#iPhone8,4
                    "I_DeviceOSVersion": device_data.get('os_version'),#"14.0",
                    "I_GameCode": "netmarbles",
                    "I_GameVersion": campaign_data.get('app_version_name'),#"1.0.12",
                    "I_JoinedCountryCode":app_data.get('geocountry') or "",# "IN",
                    "I_LanguageCD": "en-JP",
                    "I_LogDes": I_LogDes,
                    "I_LogDetailId": app_data.get('I_LogDetailId'),#11
                    "I_LogId":app_data.get('I_LogId'),# 7,
                    "I_LogKey": app_data.get('I_LogKey'),#"39d4db0f31c04640bc55e10f00c09cbe",
                    "I_NMCharacterID": app_data.get('I_NMCharacterID'),#"21120111900006288",
                    "I_NMEventTime": str(int(time.time()*1000)),#1584512209014,
                    "I_NMIDFV": app_data.get('idfv_id'),#"F2BCF692-F370-4692-AE67-53DD6BB27697",
                    "I_NMManufacturer": "Apple",
                    "I_NMMarket": "appstore",
                    "I_NMMobileIP": app_data.get('clientIp'),#"223.233.106.188",
                    "I_NMModel": device_data.get('device_platform','iPhone8,4'),#iPhone8,4
                    "I_NMSessionID": app_data.get('I_NMSessionID'),#"b4f5b2281b9d436180aff9c92d84aebc",
                    "I_NMTimestamp": str(int(time.time()*1000)),#1584512209014,
                    "I_Now": datenow,#"2020-03-18 11:46:49:013",
                    "I_OS": "0",
                    "I_PCSeq": "0",
                    "I_PID": app_data.get('user_id'),#"9DB10E04106541D8B8B89C6F60D3BED4",
                    "I_PlatformADID": app_data.get('idfa_id'),#"8DE9D78A-BC2A-4C96-BDC3-B092775291AC",
                    "I_Region": "AS2",
                    "I_SDKVersion": campaign_data.get('netmarble').get('version'),#"4.6.3.1",
                    "I_TID": "2498747888684A1E9644CB1DF49A9F50",
                    "I_TimeZone": device_data.get('timezone')[:3]+':'+device_data.get('timezone')[3:],#"+05:30",
                    "I_UDID": app_data.get('user_id'),#"D5959B8E15EA449381FA5923B584B0B8",
                    "I_World": ""
                }
            ],
            "I_LogDetailId": 0,
            "I_LogId": 0,
            "I_NMRequestTime":str(int(time.time()*1000)),# 1584532009015,
            "I_PCSeq": 0,
            "I_PID": app_data.get('user_id'),#"9DB10E04106541D8B8B89C6F60D3BED4",
            "I_RequestTime": datenow,#"2020-03-18 11:46:49:015"
        }

    return {'url':url, 'httpmethod':method, 'headers':headers, 'params':params, 'data':'data='+json.dumps(data)}



#################################################
#												#
# 			Singular Resolve funcation 			#
#												#
#################################################

def singularResolve(campaign_data, app_data, device_data, ifu='IDFA', retries=False):
    custom_user_id_create(app_data)
    #====method===
    method = "post"

    #===url===
    url = "https://sdk-api-v1.singular.net/api/v1/resolve"

    #=====header====
    headers = {
        "Accept-Encoding" : "gzip",
        "Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "User-Agent" : urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

        }

    #====event index==========
    # ec=0
    if not app_data.get('event_index'):
        app_data['event_index'] =0

    #====params====
    params = {
        "a" : campaign_data.get('singular').get('key'),
        "event_index":app_data.get('event_index'),
        "idfa" : app_data.get('idfa_id'),
        "idfv" : app_data.get('idfv_id'),
        "k" : "IDFA",
        "p" : "iOS",
        "rt" : "plist",
        "singular_install_id": app_data.get('singular_install_id'),
        "u" : app_data.get('idfa_id'),
        "v" : device_data.get('os_version')
        }

    if app_data.get('event_index')<1:
        del params['singular_install_id']

    if ifu=='APID':
        params['u']=str(uuid.uuid4()).upper()
    elif ifu=='IDFV':
        params['u']=app_data.get('idfv_id')
    elif ifu=='IDFA':
        params['u']=app_data.get('idfa_id')

    if retries==True:	
        params["tries"] = "."

    params = get_params(params)
    # params = params.replace(' ','+')
    h = getHash('?'+params,campaign_data.get('singular').get('secret'))
    params = params+'&h='+h

    app_data['event_index'] +=1

    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':None}


#################################################
#												#
# 			Singular Start funcation 			#
#												#
#################################################

def singularStart(campaign_data, app_data, device_data,event=None, ifu='IDFA'):
    custom_user_id_create(app_data)

    # if not app_data.get('s'):
    # 	app_data['s'] = str(int(time.time()*1000)-random.randint(5,10))

    #===method===
    method = "post"

    #===url===
    url = "https://sdk-api-v1.singular.net/api/v1/start"

    #===header===
    headers = {
        "Accept-Encoding" : "gzip",
        "Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "User-Agent" : urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

        }

    #====event index==========
    if not app_data.get('event_index'):
        app_data['event_index'] =0

    if not app_data.get('s'):
        app_data['s'] = str(uuid.uuid4()).upper()

    params = {
        "a" : campaign_data.get('singular').get('key'),
        "av" : campaign_data.get('app_version_name'),
        "c" : "wifi",
        "cr" : 1,
        "ddl enabled":"true",
        "ddl_to":60,
        "d" : device_data.get('device_platform','iPhone8,4'),
        "device_type" : device_data.get('device_type'),
        "dnt" : 0,
        "event_index":app_data.get('event_index'),
        "i" : campaign_data.get('package_name'),
        "idfa" : app_data.get('idfa_id'),
        "idfv" : app_data.get('idfv_id'),
        "install_time" : str(int(app_data.get('times').get('install_complete_time')*1000)),
        "is" : "true",
        "k" : "IDFA",
        "lag" : str(generateLag()),
        "n" : campaign_data.get("app_name"),
        "p" : "iOS",
        "rt" : "plist",
        "s" : app_data.get('s'),
        "sc" : 'fb758971181309801',
        "sdk" : campaign_data.get('singular').get('version'),
        "singular_install_id":app_data.get('singular_install_id'),
        "u" : app_data.get('idfa_id'),
        "update_time" : 0,
        "v" : device_data.get('os_version')
        }
    
    if app_data.get('custom_user_id'):
        params['custom_user_id'] = app_data.get('custom_user_id')

    if params.get('scs'):
        params['scs'] = str(params.get('scs'))

    params = get_params(params)
    # params = params.replace(' ','+')
    h = getHash('?'+params,campaign_data.get('singular').get('secret'))
    params = params+'&h='+h

    #===data===

    payload = json.dumps(event.get('event_value')).replace(" ","").strip()
    sign = payloadsignature(payload, campaign_data.get('singular').get('secret'))
    data = {
        "payload":payload,
        "signature": sign
    }
    app_data['singular_time']=time.time()

    app_data['event_index'] +=1

    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data, separators=(',', ':'))}


#################################################
#												#
# 			Singular Event funcation 			#
#												#
#################################################

def singular_event(campaign_data, app_data, device_data,event=None,custom_user_id=False,ifu='IDFA'):
    custom_user_id_create(app_data)

    if not app_data.get('s'):
        app_data['s'] = str(uuid.uuid4()).upper()

    if not app_data.get('seq'):
        app_data['seq'] = 1
    else:
        app_data['seq'] +=1


    t = str(time.time()-app_data.get('singular_time'))[:-8]

    #===method===
    method = "post"

    #===url===
    url = "https://sdk-api-v1.singular.net/api/v1/event"

    #===header===
    headers = {
        "Accept-Encoding" : "br, gzip, deflate",
        "Accept-Language" : device_data.get('locale').get('language')+'-'+device_data.get('locale').get('country').lower(),
        "User-Agent" : urllib.quote(campaign_data.get('app_name'))+'/'+campaign_data.get('app_version_code')+' CFNetwork/'+device_data.get('cfnetwork').split('_')[0]+' Darwin/'+device_data.get('cfnetwork').split('_')[1],

        }

    #====event index==========
    if not app_data.get('event_index'):
        app_data['event_index'] =0

    #===params===
    params = {
        "a" : campaign_data.get('singular').get('key'),
        "av" : campaign_data.get('app_version_name'),
        "device_type" : device_data.get('device_type'),
        'event_index': app_data.get('event_index'),
        "i" : campaign_data.get('package_name'),
        "idfa" : app_data.get('idfa_id'),
        "idfv" : app_data.get('idfv_id'),
        "k" : "IDFA",
        "lag" : str(generateLag()),
        "n" : event.get('event_name'),
        "p" : "iOS",
        "rt" : "plist",
        "s" : app_data.get('s'),
        "sdk" : campaign_data.get('singular').get('version'),
        "seq" : app_data.get('seq'),
        "singular_install_id":app_data.get('singular_install_id'),
        "t" : t,
        "u" : app_data.get('idfa_id')

        }

    if app_data.get('custom_user_id'):
            params['custom_user_id'] = app_data.get('custom_user_id')

    if event.get('event_name')=="heartbeat":
        params['seq']=0

    params = get_params(params)

    # params = params.replace(' ','+')
    h = getHash('?'+params,campaign_data.get('singular').get('secret'))
    params = params+'&h='+h

    #===data===

    payload = json.dumps(event.get('event_value')).replace(" ","").strip()
    sign = payloadsignature(payload, campaign_data.get('singular').get('secret'))
    data = {
        "payload":payload,
        "signature": sign
    }

    if custom_user_id:
        data['custom_user_id'] = 	app_data.get('custom_user_id')

    #====event index==========

    app_data['event_index'] +=1

    app_data['singular_time']=time.time()

    return { 'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data, separators=(',', ':')) }


#===========payload function =============
def payloadsignature(payload, secret_key, radix=16):
    sha_obj = hashlib.sha1()
    sha_obj.update(secret_key.encode('utf-8'))
    sha_obj.update(payload.encode('utf-8'))
    if radix == 16:		
        return sha_obj.hexdigest()
    elif radix == 64:		
        return base64.b64encode(sha_obj.digest())


###########################################################################
#
#               Manadatry Methods
#
############################################################################
            
def click(device_data=None, camp_type='market', camp_plat = 'ios'):
    package_name = campaign_data.get('app_id')
    serial = device_data.get('serial')
    agent_id = Config.AGENTID
    random_number = random.randint(1,10)
    gaid = device_data.get('idfa_id')
    source_id = ""
    if random_number < 5:
        source_id = "728"
    elif random_number < 8:
        source_id = "517"
    elif random_number < 9:
        source_id = "604"
    else:
        source_id = "386"

    st = device_data.get("device_id", str(int(time.time()*1000)))

    link = campaign_data['link']
    link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=gaid)
    return clicker.click(link, device_data, camp_type, camp_plat, 'url=', package_name)
    
def get_country():
    weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
    country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
    return random.choice(country_list)

def get_retention(day):
    return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0
    
#####################################################
#													#
# 			singular Utility function 				#
#													#
#####################################################

def get_params(data_value):
    key = data_value.keys()
    a = ''
    for value in sorted(key):
        a += str(urllib.quote_plus(str(value)).encode('utf-8'))+'='+str(urllib.quote_plus(str(data_value.get(value))).encode('utf-8'))+'&'
    a = a.rsplit("&",1)[0]

    return a

            
def generateLag():
    return (random.randint(40,300)*0.001)

def custom_user_id_create(app_data):
    if not app_data.get('custom_user_id'):
        app_data['custom_user_id']= app_data.get('user_id')
        # raise Exception('please enter custom_user_id')

def getHash(url,secret):
    h = hashlib.sha1()
    h.update(secret)
    h.update(url)
    return h.hexdigest()

def sha1(data, radix=16):
    import hashlib
    sha_obj = hashlib.sha1()
    sha_obj.update(data)
    if radix == 16:
        return sha_obj.hexdigest()
    elif radix == 64:
        return util.base64(sha_obj.digest())



###########################################################
# Utility methods : DEVICE
#
# Methods to get device related parameters
###########################################################	

def catch(response,app_data,paramName):
    try:
        if paramName=="PHPSESSID":
            header = response.get('res').headers
            return header.get('Set-Cookie').split("PHPSESSID=")[1].split(";")[0]
        if paramName=='COOKIE':
            header = response.get('res').headers
            return header.get('Set-Cookie')
        if paramName=="user_id":
            jsonData = json.loads(response.get('data'))
            app_data['user_id'] = jsonData.get('result').get('Achievements_getUserAchievementsAndProgress')[0].get('user_achievement').get('user_id')
        if paramName=="mat":
            jsonData = json.loads(response.get('data'))
            app_data['last_open_log_id'] = jsonData.get('log_id')

    except:
        print "Exception:: Couldn't fetch "+paramName+" from response"

BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)


def encrypt_AES(key, data):
    data=pad(data.encode('hex').upper())
    return AES.new(key, AES.MODE_ECB).encrypt(data).encode('hex').upper()


def return_userinfo(app_data):
    
    gender_n = app_data.get('gender')
    if not gender_n:
        app_data['gender'] = random.choice(['male', 'female'])
        gender_n = app_data['gender']
        
    user_info = app_data.get('user_info')
    if not user_info:
        user_data = util.generate_name(gender=gender_n)
        user_info = {}
        user_info['username'] = user_data.get('username').lower()
        user_info['email'] = user_data.get('username').lower() + '@gmail.com'
        user_info['f_name'] = user_data.get('firstname')
        user_info['l_name'] = user_data.get('lastname')
        user_info['sex'] = gender_n
        user_info['gender'] = str(1) if gender_n == 'female' else str(2)
        user_info['interestedin'] = str(2) if gender_n == 'female' else str(1)
        user_info['dob'] = util.get_random_date('1975-01-01', '1997-01-01', random.random())
        user_info['password'] = util.get_random_string(type='all',size=16)
        app_data['user_info'] = user_info

def ios_device_data(app_data):
    if not app_data.get('ios_device_data'):
        app_data['ios_device_data'] = {
        'iPad mini 2':'16777228',
        'iPad mini Wi-Fi':'12',
        'iPad mini Wi-Fi + Cellular':'12',
        'iPad 4 Wi-Fi':'16777228',
        'iPad 4 Wi-Fi + Cellular':'16777228',
        'iPhone 7 Plus':'16777228',
        'iPhone 7':'16777228',
        'iPhone SE':'16777228',
        'iPhone 6s Plus':'16777228',
        'iPhone 6s':'16777228',
        'iPhone 6 Plus':'16777228',
        'iPhone 6':'16777228', 
        'iPhone 5s':'16777228', 
        'iPhone 5c':'12',
        'iPhone 5 CDMA':'12',
        'iPhone 5':'12',
        'iPhone 4s':'12',
        'iPhone 4 CDMA':'12',
        'iPhone 4':'12',
        'iPad Air':'16777228',
        'iPad mini 3':'16777228',
        'iPad Air 2':'16777228',
        'iPad mini 4':'16777228'
        }
    return app_data.get('ios_device_data')


# ###################################################
# 													#
# 				Extra Funcation 					#
# 													#
# ###################################################

def make_sec(app_data,device_data):	
    timez = device_data.get('timezone')
    sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
    if int(timez) < 0:
        sec = (-1) * sec
    if not app_data.get('sec'):
        app_data['sec'] = sec
        
    return app_data.get('sec')

def install_receipt():
        return ''.join(random.choice(string.digits + string.ascii_letters+ '+\/' ) for _ in range(random.randint(6000,7000)))
