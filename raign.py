# -*- coding: utf8 -*-
# created by p@nkaj
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from sdk import installtimenew,NameLists
from sdk import util
import urllib,urlparse
import time
import uuid
import random
import json
import string
from datetime import datetime, timedelta
import clicker
import Config
import hashlib

#------------------------------------------------- app_data ---------------------------------------------------------------------------------
# 'device_fingerprint_id', 'install_begin_ts', 'user_id', 'installDate', 'referrer', 'session_id', 'times', 'close_count', 'user', 'install', 'identity_id'

#----------------------------- campaign_data---------------------------------------------------
campaign_data = {
    'package_name'       :'ng.com.fairmoney.fairmoney',
    'app_size'           : 0,#17,
    'app_name'           :'FairMoney: Instant loan app, bill payment and more',
    'app_version_name'   : '8.72.0',
    'app_version_code'   : '320',
    'ctr'                : 6,
    'no_referrer'        : False,
    'device_targeting'   : True,
    'supported_countries': 'WW',
    'supported_os'       : '5.0',
    'tracker'            : 'branch',
    'branch':{
        'key'  :'key_live_oaRhO0hQ2lk29uKfKF6nOijfFxijN0Ob',
        'sdk':'android5.0.15',
        'sdk_version':'5.0.15' 
    },
    # 'link':'https://track.iontap.io/?aff_id=946784&offer_id=1143713&aff_sub={dp2}&aff_sub2={agent}',
    'country'   :[('USA',50),('India', 3),('Malaysia', 4),('Indonesia', 1),('Thailand', 2), ('Egypt',1), ('Russia',6), ('USA',10), ('SaudiArabia',1), ('SouthAfrica',1), ('Israel',2), ('Kenya',1), ('Nigeria',1), ('Pakistan',2), ('Qatar',1), ('Brazil',3), ('Mexico',4), ('Canada',5),("UK", 30), ("HongKong",5), ("Spain", 4), ("France",4), ("Australia", 5)],
    'retention':{
        1:85, 
        2:82, 
        3:78, 
        4:75, 
        5:70, 
        6:66, 
        7:62, 
        8:58, 
        9:55, 
        10:50, 
        11:48, 
        12:45, 
        13:40, 
        14:39, 
        15:38, 
        16:37, 
        17:35, 
        18:35, 
        19:34, 
        20:33, 
        21:32, 
        22:31, 
        23:30, 
        24:29, 
        25:29, 
        26:28, 
        27:27, 
        28:26, 
        29:25, 
        30:25,
        31:24,
        32:23,
        33:23,
        34:22,
        35:22,
        36:21,
        37:20,
        38:19,
        39:19,
        40:18,
        41:16,
        42:16,
        43:16,
        44:15,
        45:15,
        46:14,
        47:13,
        48:13,
        49:12,
        50:11,
        51:11,
        52:10,
        53:9,
        54:9,
        55:8,
        56:7,
        57:6,
        58:5,
        59:4,
        60:3
    }
}


def branch_install(app_data, device_data):
    url = 'https://api2.branch.io/v1/install'
    httpmethod = 'post'
    headers ={
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
        'Accept-Encoding': 'gzip'
    }

    data ={
        "hardware_id": device_data.get('android_id'),
        "is_hardware_id_real": True,
        "brand": device_data.get('brand'),
        "model": device_data.get('model'),
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "wifi": True,
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "cpu_type": device_data.get('cpu'),
        "cd": {
            "mv": "-1",
            "pn": campaign_data.get('package_name')
        },
        "build": device_data.get('build'),
        "locale": "{0}_{1}".format(device_data.get('locale').get('language'), device_data.get('locale').get('country')),
        "connection_type": "wifi",
        "os_version_android": device_data.get('os_version'),
        "country": device_data.get('locale').get('country'),
        "language": device_data.get('locale').get('language'),
        "local_ip": device_data.get('private_ip'),
        "app_version": campaign_data.get('app_version_name'),
        "facebook_app_link_checked": False,
        "is_referrable": 0,
        "debug": False,
        "update": 0,
        "latest_install_time": app_data.get('install'),
        "latest_update_time": app_data.get('install'),
        "first_install_time": app_data.get('install'),
        "previous_update_time": 0,
        "environment": "FULL_APP",
        # "clicked_referrer_ts": app_data.get('install_begin_ts') - random.randint(30, 60),
        # "install_begin_ts": app_data.get('install_begin_ts'),
        "metadata": {},
        "lat_val": 0,
        "google_advertising_id": device_data.get('adid'),
        "advertising_ids": {
            "aaid": device_data.get('adid')
        },
        "instrumentation": {
            "v1/install-qwt": "0"
        },
        "sdk": campaign_data.get('branch').get('sdk'),
        "branch_key": campaign_data.get('branch').get('key'),
        "retryNumber": 0
    }

    params =None

    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':json.dumps(data).replace(': ', ':').replace(', ', ',')}
#----------------------------------------------------------------------------------------
def branch_profile(app_data, device_data, identity_id ='', identity =''):
    url = 'https://api2.branch.io/v1/profile'
    httpmethod = 'post'
    headers ={
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
        'Accept-Encoding': 'gzip'
    }
    params =None
    data ={
        'identity_id':identity_id,
        'identity': identity,
        "hardware_id": device_data.get('android_id'),
        "is_hardware_id_real": True,
        "brand": device_data.get('brand'),
        "model": device_data.get('model'),
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "wifi": True,
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "cpu_type": device_data.get('cpu'),
        "build": device_data.get('build'),
        "locale": "{0}_{1}".format(device_data.get('locale').get('language'), device_data.get('locale').get('country')),
        "connection_type": "wifi",
        "os_version_android": device_data.get('os_version'),
        "country": device_data.get('locale').get('country'),
        "language": device_data.get('locale').get('language'),
        "local_ip": device_data.get('private_ip'),
        "app_version": campaign_data.get('app_version_name'),
        "facebook_app_link_checked": False,
        "debug": False,
        "environment": "FULL_APP",
        "metadata": {},
        "lat_val": 0,
        "google_advertising_id": device_data.get('adid'),
        "advertising_ids": {
            "aaid": device_data.get('adid')
        },
        "instrumentation": {
            "v1/profile-qwt": '2',
            "v1/install-brtt": str(random.randint(400, 800))
        
        },
        "sdk": campaign_data.get('branch').get('sdk'),
        "branch_key": campaign_data.get('branch').get('key'),
        "retryNumber": 0
    }

    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':json.dumps(data).replace(': ', ':').replace(', ', ',')}
# -------------------------------------- branch close --------------------------------------------------

def branch_close(app_data, device_data, device_fingerprint_id = None, identity_id =None, session_id =None):
    url = 'https://api2.branch.io/v1/close'
    httpmethod = 'post'
    headers ={
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
        'Accept-Encoding': 'gzip'
    }
    if not app_data.get('close_count'):
        app_data['close_count'] =1
    else:
        app_data['close_count'] +=1
    params = None
    data ={
        "device_fingerprint_id": device_fingerprint_id,
        "identity_id": identity_id,
        "session_id": session_id,
        "app_version": campaign_data.get('app_version_name'),
        "hardware_id": device_data.get('android_id'),
        "is_hardware_id_real": True,
        "brand": device_data.get('brand'),
        "model": device_data.get('model').upper(),
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "wifi": True,
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "country": device_data.get('locale').get('country'),
        "language": device_data.get('locale').get('language'),
        "local_ip": device_data.get('private_ip'),
        "metadata": {},
        "lat_val": 0,
        "google_advertising_id": device_data.get('adid'),
        "advertising_ids": {
            "aaid": device_data.get('adid')
        },
        "instrumentation": {
            "v1/close-qwt": str(random.randint(0, 2)),
            "v2/event-brtt": str(random.randint(400, 500))
        },
        "sdk": campaign_data.get('branch').get('sdk'),
        "branch_key": campaign_data.get('branch').get('key'),
        "retryNumber": 0
    }
    

    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':json.dumps(data).replace(': ', ':').replace(', ', ',')}

#---------------------------------- branch open ---------------------------------------------------
def branch_open(app_data, device_data, device_fingerprint_id = None, identity_id =None):
    url = 'https://api2.branch.io/v1/open'
    httpmethod = 'post'
    headers ={
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
        'Accept-Encoding': 'gzip'
    }
    params = None
    data ={
        "device_fingerprint_id": device_fingerprint_id,
        "identity_id": identity_id,
        "hardware_id": device_data.get('android_id'),
        "is_hardware_id_real": True,
        "brand": device_data.get('brand'),
        "model": device_data.get('model'),
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "wifi": True,
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "cpu_type": device_data.get('cpu'),
          "cd": {
            "mv": "-1",
            "pn": campaign_data.get('package_name')
        },
        "build": device_data.get('build'),
        "locale": "{0}_".format(device_data.get('locale').get('language')),
        "connection_type": "wifi",
        "os_version_android": device_data.get('os_version'),
        # "country": device_data.get('locale').get('country'),
        "language": device_data.get('locale').get('language'),
        "local_ip": device_data.get('private_ip'),
        "app_version": campaign_data.get('app_version_name'),
        "facebook_app_link_checked": False,
        "is_referrable": 0,
        "debug": False,
        "update": 1,
        "latest_install_time": app_data.get('install'),
        "latest_update_time": app_data.get('install'),
        "first_install_time": app_data.get('install'),
        "previous_update_time": app_data.get('install'),
        "environment": "FULL_APP",
        "metadata": {},
        "lat_val": 0,
        "google_advertising_id": device_data.get('adid'),
        "advertising_ids": {
            "aaid": device_data.get('adid')
        },
        "instrumentation": {
            "v1/open-brtt": str(random.randint(389, 1897)),
            "v1/open-qwt": "0"
        },
        "sdk": campaign_data.get('branch').get('sdk'),
        "branch_key": campaign_data.get('branch').get('key'),
        "retryNumber": 0
    }

    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':json.dumps(data).replace(': ', ':').replace(', ', ',')}
#------------------------------------------------------------------------

def branch_cdn(app_data, device_data, v):
    url = 'https://cdn.branch.io/sdk/uriskiplist_'+v+'.json'
    httpmethod = 'get'
    headers = {
        'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
        'Accept-Encoding': 'gzip'
    }
    params = None
    data =None

    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':data}

#----------------------------------------
def branch_event(app_data, device_data, event_name=None, device_fingerprint_id =None, custom_data ='',session_id=None):
    url = 'https://api2.branch.io/v1/event'
    httpmethod = 'post'
    headers ={
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
        'Accept-Encoding': 'gzip'
    }
    params = None

    data ={
        "advertising_ids": {
            "aaid": device_data.get('adid')
        },
        "branch_key": campaign_data.get('branch').get('key'),
        "brand": device_data.get('brand'),
        "build": device_data.get('build'),
        "connection_type": "wifi",
        "country": device_data.get('locale').get('country'),
        "cpu_type": device_data.get('cpu'),
        "device_fingerprint_id": device_fingerprint_id,
        "environment": "FULL_APP",
        "event": event_name,
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {
            "v1/event-qwt":str(random.randint(400, 500)),
            "v1/profile-brtt": str(random.randint(400, 500))
        },
        "is_hardware_id_real": True,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "local_ip": device_data.get('private_ip'),
        "locale": "{0}_{1}".format(device_data.get('locale').get('language'), device_data.get('locale').get('country')),
        "metadata": {
            "application_id": "13"+str(random.randint(300000,999999))
        },
        "model": device_data.get('model'),
        "os": "Android",
        "os_version": int(device_data.get('sdk')),
        "os_version_android": device_data.get('os_version'),
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "sdk": campaign_data.get('branch').get('sdk'),
        "session_id": session_id,
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True
        }


    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':json.dumps(data).replace(': ', ':').replace(', ', ',')}

def branch_url(app_data, device_data,session_id=None):
    url = 'https://api2.branch.io/v1/url'
    httpmethod = 'post'
    headers ={
        'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
        'Accept-Encoding': 'gzip'

    }
    params =None
    data = {
        "advertising_ids": {
            "aaid": device_data.get('adid')
        },
        "alias":{},
        "branch_key": campaign_data.get('branch').get('key'),
        "brand": device_data.get('brand'),
        "campaign":'',
        "channel":'',
        "country": device_data.get('locale').get('country'),
        "data":{
            "$og_title": "prateek tomar invites you to try Hello Play",
            "$canonical_identifier": "HelloPlayInviteId",
            "$og_description": "Play Ludo, Carrom and other games with friends",
            "$og_image_url": "https://helloplay.flixcart.com/cdn/HCContent/hp-logo-12-01.jpg",
            "$publicly_indexable": "true",
            "player_id": app_data.get('player_id'),
            "source": "android"
        },
        "device_fingerprint_id": app_data.get('device_fingerprint_id'),
        "feature":"sharing",
        "google_advertising_id": device_data.get('adid'),
        "hardware_id": device_data.get('android_id'),
        "identity_id": app_data.get('identity_id'),
        "instrumentation": {
            "v2/event/custom-brtt":str(random.randint(890,950)),
            "v1/url-qwt": "4"
        },
        "is_hardware_id_real": False,
        "language": device_data.get('locale').get('language'),
        "lat_val": 0,
        "local_ip": device_data.get('private_ip'),
        "metadata": {},
        "model": device_data.get('model'),
        "os_version": int(device_data.get('sdk')),
        "os": "Android",
        "retryNumber": 0,
        "screen_dpi": int(device_data.get('dpi')),
        "screen_height": int(device_data.get('resolution').split('x')[0]),
        "screen_width": int(device_data.get('resolution').split('x')[1]),
        "session_id": session_id,
        "sdk": campaign_data.get('branch').get('sdk'),
        "stage":'',
        "tags":[],
        "ui_mode": "UI_MODE_TYPE_NORMAL",
        "wifi": True,
    }

    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':json.dumps(data)}

def signup(app_data, device_data,campaign_data):
    url = 'https://app.fairmoney.com.ng/v2/android/signup/phone'
    httpmethod = 'post'
    headers = {
        'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
        'Accept-Encoding': 'gzip'
    }
    params =None

    data ={

            "android": {
            "version_name": campaign_data.get('app_version_name')
            },
            "auth": {
            "phone": app_data.get("phone_number")
            },
            "version_code": campaign_data.get('app_version_code')
            }

    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':json.dumps(data).replace(': ', ':').replace(', ', ',')}


# {
#   "data": {
#     "android": {
#       "version_name": "8.72.0"
#     },
#     "auth": {
#       "android_app_instance_guid": "8d32a79d-1eeb-495e-8639-946081b428f3",
#       "password": 1997,
#       "phone": "+919359226290"
#     }
#   },
#   "version_code": 320

def login(app_data, device_data,campaign_data):
    url = 'https://app.fairmoney.com.ng/v2/android/login/phone'
    httpmethod = 'post'
    headers = {
        'User-Agent': 'Dalvik/2.1.0'+' (Linux; U; Android '+ device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+  device_data.get('build')+')',
        'Accept-Encoding': 'gzip'
    }
    params =None

    data ={
        "android": {
        "version_name": campaign_data.get('app_version_name')
        },
        "auth": {
        "android_app_instance_guid": device_data.get('adid'),
        "password": 1997,
        "phone": "+919359226290"
        },
        "version_code": 320
        }
    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':json.dumps(data).replace(': ', ':').replace(', ', ',')}




    #-----------------------------------------------------------------------------------
def install(app_data, device_data):
    if not app_data.get('times'):
        print "Please wait installing..."
        register_user(app_data,country='united states')
        installtimenew.main(app_data, device_data, app_size=campaign_data.get('app_size'), os='android')
        app_data['installDate'] = datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S') + '+0000'
        app_data['install'] =int(time.time()*1000)
        app_data['install_begin_ts'] =int(time.time() - random.randint(120, 180))

    if not app_data.get('user_id'):
        app_data['user_id'] = str(random.randint(1000689432, 1000778421))

    if not app_data.get("phone_number"):
        app_data["phone_number"]='91'+str(random.randint(7000000000,9999999999))




    #----------------- Branch Install -----------------------------------------------------------

    print 'Branch install ----------------------'
    count=1
    while not app_data.get('device_fingerprint_id') and count<5:
        req=branch_install(app_data,device_data)
        result=util.execute_request(**req)
        try:
            json_result=json.loads(result.get('data'))
            app_data['device_fingerprint_id']=json_result.get('device_fingerprint_id')
            app_data['identity_id']=json_result.get('identity_id')
            app_data['session_id']=json_result.get('session_id')
        except:
            pass
        count +=1

    print '---------------Branch_cdn__v1-------------------'
    req = branch_cdn(app_data, device_data, v = 'v1')
    util.execute_request(**req)

    req=signup(app_data, device_data,campaign_data)
    util.execute_request(**req)


    req=login(app_data, device_data,campaign_data)
    util.execute_request(**req)


    if app_data.get('device_fingerprint_id'):
        #-------------------------- Branch profile -----------------------------------
        res=branch_profile(app_data, device_data, identity_id =app_data.get('identity_id'), identity =app_data.get('user_id'))
        util.execute_request(**req)
        time.sleep(random.randint(5, 10))

        #-------------------- Branch Event ----------------------------
        req = branch_event(app_data, device_data, event_name='link_application_id', device_fingerprint_id=app_data.get('device_fingerprint_id'),session_id=app_data.get('session_id'))
        util.execute_request(**req)
        time.sleep(random.randint(5, 10))



        #--------------------------------- Branch Close ---------------------------------------
        req=branch_close(app_data, device_data, device_fingerprint_id = app_data.get('device_fingerprint_id'), identity_id =app_data.get('identity_id'), session_id =app_data.get('session_id'))
        util.execute_request(**req)



    return {'status':True}
#-------------------------------------------------------------
def open(app_data, device_data, day =1):
    if not app_data.get('times'):
        print "Please wait installing..."
        register_user(app_data,country='united states')
        installtimenew.main(app_data, device_data, app_size=campaign_data.get('app_size'), os='android')
        app_data['installDate'] = datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S') + '+0000'
        app_data['install'] =int(time.time()*1000)
        app_data['install_begin_ts'] =int(time.time() - random.randint(120, 180))

    if not app_data.get('user_id'):
        app_data['user_id'] = str(random.randint(1000689432, 1000778421))

    if not app_data.get("phone_number"):
        app_data["phone_number"]='91'+str(random.randint(7000000000,9999999999))


    print "------------open__branch---------------------------"
    count=1
    while not app_data.get('device_fingerprint_id') and count<5:
        req = branch_open(app_data, device_data, device_fingerprint_id =app_data.get('device_fingerprint_id'), identity_id = app_data.get('identity_id'))
        result=util.execute_request(**req)
        try:
            json_result=json.loads(result.get('data'))
            app_data['device_fingerprint_id']=json_result.get('device_fingerprint_id')
            app_data['identity_id']=json_result.get('identity_id')
            app_data['session_id']=json_result.get('session_id')
        except:
            pass
        count +=1


    if app_data.get('device_fingerprint_id'):
        print 'close___branch------------------'
        req = branch_close(app_data, device_data, device_fingerprint_id =app_data.get('device_fingerprint_id'), identity_id = app_data.get('identity_id'), session_id = app_data.get('session_id'))
        util.execute_request(**req)   
        
    print '---------------Branch_cdn__v2-------------------'
    req = branch_cdn(app_data, device_data, v = 'v2')
    util.execute_request(**req)


    return {'status':True}
# ================== Clicker Functions ==========================

def click(device_data=None, camp_type='market', camp_plat = 'android'):
    serial = device_data.get('serial')
    agent_id = Config.AGENTID
    random_number = random.randint(1,10)
    source_id = ""
    if random_number < 5:
        source_id = "728"
    elif random_number < 8:
        source_id = "517"
    elif random_number < 9:
        source_id = "604"
    else:
        source_id = "386"

    st = str(int(time.time()*1000))

    link = campaign_data['link']
    link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
    return clicker.click(link, device_data, camp_type, camp_plat, 'url=')

def get_country():
    weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
    country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
    return random.choice(country_list)

def get_retention(day):
    return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0


def register_user(app_data,country='united states'):
    if not app_data.get('user'):
        gender      = random.choice(['male','female'])
        first_names = NameLists.NAMES[country][gender]
        first_name  = random.choice(first_names)
        last_names  = NameLists.NAMES[country]['lastnames']
        last_name   = random.choice(last_names)
        number      = str(random.randint(100, 9000))
        symbol      = random.choice(['', '.', '_'])
        usernamelst = [first_name, last_name, number, symbol]
        while usernamelst[-1] in [symbol] or usernamelst[0] in [symbol, number]:
            random.shuffle(usernamelst)
        username    = ''.join(usernamelst)
        app_data['user']                ={}
        app_data['user']['sex']         = gender
        app_data['user']['dob']         = util.get_random_date('1975-01-01', '2000-01-01', random.random())
        app_data['user']['firstname']   = first_name
        app_data['user']['lastname']    = last_name
        app_data['user']['username']    = username
        app_data['user']['email']       = (first_name+last_name+str(random.randint(1,9999))).lower()+"@" +random.choice(['yahoo.com',"gmail.com"])
        app_data['user']['pwd']         = util.get_random_string('all',15)