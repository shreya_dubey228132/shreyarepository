# from email.errors import NonPrintableDefect
from glob import glob
from pickle import NONE
import shutil
import tempfile
from os import listdir, path
# from xdrlib import ConversionError
from zipfile import ZipFile
import random
from traceback import format_exc
import codecs
from bs4 import BeautifulSoup
from sys import platform
import base64,requests,json
from urllib import parse
import traceback,sys
import os
import appsflyernew

global Conversion
Conversion = []
global launch
launch = []
global inapp
inapp = []
global gcdsdk
gcdsdk = []
global Appsflyer_data
Appsflyer_data = {}


input_path = "logs"

file_name = listdir(input_path)[0]
input_file = input_path+"/"+file_name
# print(listdir(input_path))


# os.remove(input_file)

# exit()



# exit()
package_name = str(input("Enter Package Name........ \n"))

package_name= package_name.strip()
def main():
	# uncommnert below 5 lines E:\autocode\saz_parser
	temp_dir = tempfile.mkdtemp(prefix="C:/httplogs_",suffix="_"+str(random.randint(1000,9999)))
	print(temp_dir)
	zf = ZipFile(input_file,'r')
	zf.extractall(temp_dir)
	zf.close()
	# temp_dir = "E:/test/file/httplogs_c9hvb2ve_8824"
	# print(listdir(temp_dir))

	try:
		dir_list = listdir(temp_dir)
		for x in dir_list:
			if x.endswith(".htm"):
				file_link = path.join(temp_dir,x)
				# print(file_link)
				log_list_html = BeautifulSoup(codecs.open(file_link,'r'),features="html.parser")
				tc1 = 0
				column_numbers = {"#":-1,"Result":-1,"Protocol":-1,"Host":-1,"URL":-1,"Body":-1}
				for row in log_list_html('tr'):
					if len(row('td')) == 0 and len(row('th')) > 0:
						i=0
						for i in range(len(row('th'))):
							column_number_key = row('th')[i].text
							if column_number_key in column_numbers:
								column_numbers[column_number_key]=i
						for column_number_key in column_numbers:
							if column_numbers.get(column_number_key) == -1:
								del column_numbers[column_number_key]
					elif len(row('td')) > 0 and len(row('th')) == 0:
						log_host = row('td')[column_numbers.get('Host')].text
						log_no = row('td')[column_numbers.get('#')].text
						url_path = row('td')[column_numbers.get('URL')].text
						links_html = row('td')[0]
						links = {"C":None,"S":None,"M":None}
						for link in row('td')[0]('a',href=True):
							if "linux" in platform:
								links[link.text] = link['href'].replace('\\','/')
							else:
								links[link.text] = link['href']
						request_file = None
						response_file = None
						if links.get('C'):
							request_file = path.join(temp_dir,links.get('C'))
						if links.get('S'):
							response_file = path.join(temp_dir,links.get('S'))

						# print(log_host)
						# exit()
						parse_gcd_log(log_host, log_no, links, request_file, response_file,url_path)
						# print("Calling ___GCD")
							# print("# "+str(log_no)+", "+log_contentType,)
					else:
						print(row)
						print("*"*100)
					# print("~"*50)
					tc1 += 1
					# if tc1 == 100:
					# 	break
				# print(log_list_html.read())
				print(column_numbers)
		for x in dir_list:
			if x.endswith(".htm"):
				file_link = path.join(temp_dir,x)
				# print(file_link)
				log_list_html = BeautifulSoup(codecs.open(file_link,'r'),features="html.parser")
				tc1 = 0
				column_numbers = {"#":-1,"Result":-1,"Protocol":-1,"Host":-1,"URL":-1,"Body":-1}
				for row in log_list_html('tr'):
					if len(row('td')) == 0 and len(row('th')) > 0:
						i=0
						for i in range(len(row('th'))):
							column_number_key = row('th')[i].text
							if column_number_key in column_numbers:
								column_numbers[column_number_key]=i
						for column_number_key in column_numbers:
							if column_numbers.get(column_number_key) == -1:
								del column_numbers[column_number_key]
					elif len(row('td')) > 0 and len(row('th')) == 0:
						log_host = row('td')[column_numbers.get('Host')].text
						log_no = row('td')[column_numbers.get('#')].text
						url_path = row('td')[column_numbers.get('URL')].text
						links_html = row('td')[0]
						links = {"C":None,"S":None,"M":None}
						for link in row('td')[0]('a',href=True):
							if "linux" in platform:
								links[link.text] = link['href'].replace('\\','/')
							else:
								links[link.text] = link['href']
						request_file = None
						response_file = None
						if links.get('C'):
							request_file = path.join(temp_dir,links.get('C'))
						if links.get('S'):
							response_file = path.join(temp_dir,links.get('S'))

						# print(log_host)
						# exit()
						parse_single_log(log_host, log_no, links, request_file, response_file,url_path)
							# print("# "+str(log_no)+", "+log_contentType,)
					else:
						print(row)
						print("*"*100)
					# print("~"*50)
					tc1 += 1
					# if tc1 == 100:
					# 	break
				# print(log_list_html.read())
				print(column_numbers)

	except:
		error_out = format_exc()
		print(error_out)


	# uncomment below 3 line
	print("#"*50+"\nPress Enter to Delet Temp file ...\n"+"#"*50)
	input("")
	# os.remove(input_file)
	shutil.rmtree(temp_dir)




	# a = tempfile.shopeerandom1


def parse_single_log(log_host, log_no, links, request_file, response_file,url_path):
	if "appsflyer" in log_host:
		print(log_host)
		try: 
			if  log_host.split('.')[0] == 'launches' or log_host.split('.')[0] == 'conversions' or log_host.split('.')[0] == 'inapps' or log_host.split('.')[0] == 'attr' or log_host.split('.')[0] == 'register' :
				# print("in parser")
				# print(url_path)
				if not  Appsflyer_data.get('Key'):
					print("Appsflyer Key Not Found Please Enter Mamually If u have...................")
					raw_Key = str(input())
					Appsflyer_data['Key'] = raw_Key.strip()
					# print("Please enter package id ....................................")
					# raw_id = str(input())
					Appsflyer_data['id'] = package_name

					print((log_host, log_no, links, request_file, response_file,url_path))
					parse_appsflyer_request(filepath=request_file,log_host = log_host.split('.')[0],url=url_path)
				else:
					# print("S"*100)
					# print(url_path)
					# print("S"*100)
					b =  parse.urlparse(url_path)
					b.query
					# print(b.query)
					if parse.parse_qs(b.query).get('app_id'):
						id =  parse.parse_qs(b.query).get('app_id')[0]
					
					if parse.parse_qs(b.query).get('buildnumber'):
						Appsflyer_data['build_number'] =  parse.parse_qs(b.query).get('buildnumber')[0]
					
					if Appsflyer_data.get('id') == id:
						# print("**"*50)
						# print((log_host, log_no, links, request_file, response_file,url_path))
						parse_appsflyer_request(filepath=request_file,log_host = log_host.split('.')[0],url=url_path)
		except:
			traceback.print_exc(file=sys.stdout)
				
def parse_gcd_log(log_host, log_no, links, request_file, response_file,url_path):
	
	if "appsflyer" in log_host:
		# print(log_host)
		if log_host.split('.')[0] == 'gcdsdk':
			print((log_host, log_no, links, request_file, response_file,url_path))
			parse_gcdsdk_appsflyer_request(filepath=request_file,log_host = log_host.split('.')[0],url=url_path)



def parse_gcdsdk_appsflyer_request(filepath=None,log_host= None,url=None):
	# print(filepath)
	# print(log_host)
	if filepath == None:
		print("Request parse appsflyer request function without passing request file path")
		return True
	b = parse.urlparse(url)
	# b.query
	if package_name == path.basename(b.path):
		Appsflyer_data['Key'] =  parse.parse_qs(b.query).get('devkey')[0]
		Appsflyer_data['id']  = path.basename(b.path)


def parse_appsflyer_request(filepath=None,log_host= None,url=None):
	# print(filepath)
	# print(log_host)
	if filepath == None:
		print("Request parse appsflyer request function without passing request file path")
		return True
	if path.isfile(filepath):
		no = 0
		with open(filepath,'rb') as infile:
			# print(infile.read())
			# infile.seek(0)
			for line in infile:
				# print(line)
				no += 1
				try:
					line1 = line.decode('utf-8').replace("\n","").replace("\r","")
					# print(line1)
					if line1 == "":
						print("~"*60+str(no))
						break

				except:
					pass
			# infile.seek(no-1)
			file_data = b""
			for line in infile:
				# print(line)
				file_data += line


			if log_host == 'launches':
				da = base64.b64encode(file_data)
				launch.append(da)
				# print(len(file_data))
				print("E"*100)
			elif log_host == 'inapps':
				da = base64.b64encode(file_data)
				inapp.append(da)
				# print(len(file_data))
			elif log_host == 'conversions':
				da = base64.b64encode(file_data)
				Conversion.append(da)
				# print(len(file_data))
			elif log_host == 'attr':
				Appsflyer_data['attr'] = True
			elif log_host == 'register':
				Appsflyer_data['register'] =True
			else:
				pass
				# print("#"*50)
		# print(codecs.open(filepath,'r').read())
	else:
		print("requested path is not a file")
		return True




Apps_key = {}
launchs_data1 = {}
event_name = []
event_value = []
launch1 = []
inapp1 = []


def key_generation(key):
	
	url = "https://fuseclick.c2a.in/keyconvert"
	proxies = {"http": None, "https": None}

	data = {}
	data['af_key'] = key
	print(key)
	resp = requests.post(url, data=data, proxies=proxies, timeout=100)
	# print resp.text
	if not Apps_key.get('dev_key'):
		Apps_key['dev_key'] =  json.loads(resp.text).get('development_key')
		# print Apps_key.get('dev_key')
		print('Key Generated')

	if not Apps_key.get('production_key'):
		Apps_key['production_key'] =  json.loads(resp.text).get('production_key')
		print ("Sucessfully...................")



def decrypt(dyc_data,Apps_key):
	print("====================")
	url = "https://fuseclick.c2a.in/af_decrypt"
	proxies = {"http": None, "https": None}

	data = {}
	# print conversin_data
	# print 
	data['appsflyer_key'] = Apps_key.get('dev_key')
	data['appsflyer_data'] = dyc_data
	resp = requests.post(url, data=data, proxies=proxies, timeout=100)
	t = resp.text
	# print(t)

	return t


def event_list(data12):
	e = decrypt(data12,Apps_key)
	if e:
		eve =  json.loads(e).get('Decrypted Data')[0]
		if not launchs_data1.get('inapp'):
			# e = decrypt(data12,Apps_key)
			inapp1.append(json.loads(e).get('Decrypted Data')[0])
			# print launch
			launchs_data1['inapp'] = True  
		
	# print p
	# exit()
		event_name.append(eve.get('eventName'))
	if eve.get('eventName'):
		tmp = {eve.get('eventName'):json.loads(eve.get('eventValue'))}
		event_value.append(tmp)



def launch_dyc(data12):
	# print "hell+++++++++++++++++++++++++++++++++++++++++++++++"
	# print data12
	if not launchs_data1.get('launch'):
		e = decrypt(data12,Apps_key)
		launch1.append(json.loads(e).get('Decrypted Data')[0])
		# print launch
		# print launch
		launchs_data1['launch'] = True














if __name__ == "__main__":
	main()

	# print(Appsflyer_data)
	print(Appsflyer_data.get('Key'))
	
	key_generation(Appsflyer_data.get('Key'))

	# print(Apps_key)

	# print(Conversion)
	try:
		dyc_con = decrypt(Conversion,Apps_key)
		dyc_conversion_data = json.loads(dyc_con).get('Decrypted Data')[0]
	except:
		dyc_conversion_data = []		

	try:
		dyc_lau = decrypt(launch[0],Apps_key)
		dyc_launch_data = json.loads(dyc_lau).get('Decrypted Data')[0]
	except:
		dyc_launch_data = []

	try:
		dyc_inapp = decrypt(inapp[0],Apps_key)
		dyc_inapp_data = json.loads(dyc_inapp).get('Decrypted Data')[0]
	except:
		dyc_inapp_data= {}
	
	for i in inapp:
		event_list(i)


	# print(event_name)
	# print(dyc_conversion_data)
	# print(dyc_launch_data)
	# print(event_value)

	appsflyernew.file_creation(dyc_conversion_data,dyc_launch_data,dyc_inapp_data,event_name,event_value,Appsflyer_data,Apps_key)


	os.remove(input_file)


	# print("I"*100)
	# print(inapp)
	# print(len(inapp))
	# print("C"*100)
	# print(Conversion)
	# print(len(Conversion))
