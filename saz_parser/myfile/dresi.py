#-*- coding: utf-8 -*-
# created by him@nshu
import sys
if int(sys.version_info.major) == 2:
    py_version = 2
else:
    py_version = 3
    import urllib.parse as urlparse
from sdk import installtimenew, util
import time, random, json, string, urllib, uuid, hashlib
from spicipher import af_cipher,get_dlsdk_af_sig,get_cdn_token,get_gcd_sdk_af_sig
false = False
from datetime import datetimetrue = True
null = None


campaign_data = {
   'package_name' : 'com.globalegrow.app.dresslily',
   'app_name' : 'drsily',
   'app_version_name' : '7.3.3',
   'app_version_code' : '139',
   'supported_countries' : 'WW',
   'supported_os' : '10',
   'api_ver_name' : '30.5.16-21 [0] [PR] 447062287',
   'sig' : 'B262ECBAF629B2D7A9958EBB894E60A27E86A251D5036B53C37624D0D7102628',
   'platformextension' : 'android_native',
    'appsflyer' : { 
      'key' : 'PQmjs6dfikqatrWRQ4EEG',
      'buildnumber' : '6.9.0',
      'version' : 'v6.9',
      'spiKey' : 'kOPDIpF52digNXVyVDrHsyKo7xnuNmq6zg09usqILuu8doQQFD1sD6/DkZSTsW33',
       #'spiKey' : 'e/mCkfJLeJaZF7kL02tNFbQub6V55k6ixczKFAWq+dvC15fmjk3ifIIcpGULPbwt',#devkey
    },
   'link' : '',
    'app_size' : 20,
    'api_ver' : 83051610,
    'tracker' : ['appsflyer'],
    'country' : [('USA', 50), ('India', 3), ('Malaysia', 4), ('Indonesia', 1), ('Thailand', 2), ('Egypt', 1), ('Russia', 6), ('USA', 10), ('SaudiArabia', 1), ('SouthAfrica', 1), ('Israel', 2), ('Kenya', 1), ('Nigeria', 1), ('Pakistan', 2), ('Qatar', 1), ('Brazil', 3), ('Mexico', 4), ('Canada', 5), ('UK', 30), ('HongKong', 5), ('Spain', 4), ('France', 4), ('Australia', 5)],
    'retention' : {1: 85, 2: 82, 3: 80, 4: 78, 5: 75, 6: 72, 7: 70, 8: 68, 9: 65, 10: 60, 11: 55, 12: 50, 13: 45, 14: 40, 15: 35, 16: 33, 17: 30, 18: 28, 19: 26, 20: 20, 21: 19, 22: 18, 23: 17, 24: 16, 25: 15, 26: 14, 27: 13, 28: 12, 29: 11, 30: 10, 31: 9, 32: 8, 33: 7, 34: 6, 35: 5},
    }


Appsflyer_data = {"gcd_url": "gcdsdk.appsflyer.com", "gcd_ver": "v4.0", "Key": "PQmjs6dfikqatrWRQ4EEG", "id": "com.globalegrow.app.dresslily", "cdn_version": "v1", "cdn_url": "cdn-settings.appsflyersdk.com", "build_number": "6.9.0", "con_url": "conversions.appsflyer.com", "in_url": "inapps.appsflyer.com", "att_url": "attr.appsflyer.com", "la_url": "launches.appsflyer.com", "res_url": null}

register_data = {}

firebase_data = {"x-goog-api-key": "AIzaSyBYYHOt6C_4-RMKNM4Njgk-I9QoYzoSze4", "x-firebase-client": "H4sIAAAAAAAAAD2QzY7DIAyEX6XiXCCQSu32VZYeaHCyVglUYEW7qvru6-bvyMd4xp6X-AFf6A6eqrh-v4QfIJG4ih4LyC4XcNo2qlXNwadQMgZJvgxAsoaH0605LMJYnTYX1p0Oj0wREz_VWZlm-ffJxz_CjlXWKKtW3Hcjg1YZZTbdnMHwsudhquRjhOJ0l0e1YjVBCpiGZW7VeMKcPpucZ8sAE3Yg74VnnP5Fn0fcbUdMyw3W7OzJDn0uvNQ2O-YA0ekJcwTaYPIj7GzOJ46oz1xoboEvF0cRPMGnU2Eb28rmJM2XuL1vRzFBqbwnl2zF-x9kl7z2gQEAAA", "X-Android-Cert": "B3DCD21171AC2CD62BEDEADA3816098B5B2CCF93", "data": {"fid": "ewK6s8aXR2ig4wb0v9oLAq", "appId": "1:780583622417:android:4445ecf7ffba514d", "authVersion": "FIS_v2", "sdkVersion": "a:17.1.1"}, "url": "/v1/projects/lateral-academy-101501/installations"}

Event_name = {"event_name": ["af_enter_app", "af_sign_in", "af_guide_app", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_click", "af_Catentrance_impression", "af_Catentrance_click", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_impression", "af_view_homepage", "af_view_homepage", "af_banner_impression", "af_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_click", "af_view_product", "af_impression", "af_impression", "af_enter_app", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_click", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_click", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_view_homepage", "af_banner_impression", "af_impression", "af_view_homepage", "af_view_homepage", "af_view_homepage", "af_view_homepage", "af_view_homepage", "af_impression", "af_view_homepage", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_click", "af_impression"]}

event_data = [{"af_enter_app": {"af_lang": null, "af_country_code": null, "af_user_type": 0, "af_content_type": "enter app"}}, {"af_sign_in": {"af_inner_mediasource": "lauch_signin", "af_lang": "en", "af_country_code": "US", "af_user_type": 0, "af_content_type": "Email"}}, {"af_guide_app": {"af_lang": "en", "af_country_code": "US", "af_user_type": 0, "af_content_type": "creat an accoun"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "WOMEN", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "DRESSES", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "SWIMWEAR", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "BOTTOMS ", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "PLUS SIZE", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "TOPS", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "OUTFITS", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "ACCESSORIES", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "MEN", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_click": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 2, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "-2", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "Vitural_Catentrance_impression", "af_cat_name": "NEW"}}, {"af_Catentrance_click": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 2, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "-2", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "Vitural_Catentrance_impression", "af_cat_name": "NEW"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Dress"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Tops"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Bottoms"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Outfits"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Men"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Plus Size"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "galaxy", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Accessories"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "galaxy swimwear", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Swimwear"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "galaxy dress", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Dress"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "galaxy outfit", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Outfits"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "galaxy top", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Tops"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "galaxy plus size", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Plus Size"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "moon accessory", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Acessories"}}, {"af_impression": {"af_inner_mediasource": "recommend_personal", "af_lang": "en", "af_price": "34.99,34.99,24.99,16.99,26.99,34.99,16.99,19.99,36.99,36.99,16.99,36.99,16.99,24.99,36.99,28.99,16.99,34.99,36.99,24.99,36.99,19.99,22.99,22.99,19.99,24.99,36.99,42.99,34.99,24.99,24.99,26.99,22.99,22.99,19.99,36.99,25.99,22.99,36.99,26.99,22.99,28.99,22.99,18.99,19.99,24.99,19.99,38.99,19.99,38.99,16.99,24.99,41.99,35.99,20.99,36.99,37.99,24.99,24.99,22.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "509168801,509168501,508696603,499613101,499287227,509247301,507930302,499704546,499763801,498667901,507661814,504392301,508510415,508399108,503432601,507361007,507608605,504707401,509197901,448672008,509251301,502007901,475490616,476888602,508976504,469239301,508752701,505863501,499241301,508663205,470963604,495494806,509244203,507579805,505968804,501961801,504303801,466931402,509274801,498468808,495480605,506247326,507551006,476710904,508928907,498498503,326778587,498539701,499825107,509267701,498639203,503921704,507321815,508491101,508446804,509244401,509174401,508740704,480888301,469738309"}}, {"af_view_homepage": {"af_lang": "en", "af_channel_id": "416", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_content_type": "view homepage"}}, {"af_view_homepage": {"af_lang": "en", "af_channel_id": "416", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_content_type": "view homepage"}}, {"af_banner_impression": {"af_col_id": null, "af_lang": "en", "af_ad_id": "24", "af_component_id": "14", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "Homepage", "af_banner_name": "Hey New Friend!", "af_content_type": "banner impression"}}, {"af_impression": {"af_inner_mediasource": "recommend_personal", "af_lang": "en", "af_price": "34.99,34.99,24.99,16.99,26.99,34.99,16.99,19.99,36.99,36.99,16.99,36.99,16.99,24.99,36.99,28.99,16.99,34.99,36.99,24.99,36.99,19.99,22.99,22.99,19.99,24.99,36.99,42.99,34.99,24.99,24.99,26.99,22.99,22.99,19.99,36.99,25.99,22.99,36.99,26.99,22.99,28.99,22.99,18.99,19.99,24.99,19.99,38.99,19.99,38.99,16.99,24.99,41.99,35.99,20.99,36.99,37.99,24.99,24.99,22.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "509168801,509168501,508696603,499613101,499287227,509247301,507930302,499704546,499763801,498667901,507661814,504392301,508510415,508399108,503432601,507361007,507608605,504707401,509197901,448672008,509251301,502007901,475490616,476888602,508976504,469239301,508752701,505863501,499241301,508663205,470963604,495494806,509244203,507579805,505968804,501961801,504303801,466931402,509274801,498468808,495480605,506247326,507551006,476710904,508928907,498498503,326778587,498539701,499825107,509267701,498639203,503921704,507321815,508491101,508446804,509244401,509174401,508740704,480888301,469738309"}}, {"af_banner_impression": {"af_col_id": "03fadd10-6e71", "af_lang": "en", "af_ad_id": "66cecc51-3d51", "af_channel_id": "416", "af_component_id": "md-37defe76-2c00", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "\u6807\u7b7e", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "a07e8a91-ae09", "af_lang": "en", "af_ad_id": "7559bf24-f70c", "af_channel_id": "416", "af_component_id": "md-abhn1681888595524", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "New Arrivals", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "a07e8a91-ae09", "af_lang": "en", "af_ad_id": "3b79ab5f-3c4e", "af_channel_id": "416", "af_component_id": "md-abhn1681888595524", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Great Deals", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "a07e8a91-ae09", "af_lang": "en", "af_ad_id": "524f8e4c-c260", "af_channel_id": "416", "af_component_id": "md-abhn1681888595524", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Tie Dye Collection", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "0f0fe0e4-c735", "af_lang": "en", "af_ad_id": "73dbad2d-a129", "af_channel_id": "416", "af_component_id": "md-32324602-e671", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "572407c4-c9dc", "af_lang": "en", "af_ad_id": "b2509a40-8ad2", "af_channel_id": "416", "af_component_id": "md-ee43c6e6-dfd9", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "3736afa9-5f78", "af_lang": "en", "af_ad_id": "29472f72-6c74", "af_channel_id": "416", "af_component_id": "md-3fbad2a1-0138", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "PLUS SIZE", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "30fc2875-4933", "af_lang": "en", "af_ad_id": "1583ff67-f230", "af_channel_id": "416", "af_component_id": "md-3fbad2a1-0138", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "WOMEN", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "da3cb2b6-b165", "af_lang": "en", "af_ad_id": "48df86bc-8a89", "af_channel_id": "416", "af_component_id": "md-7d033e38-15b4", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "WOMEN", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "4dd2fec0-33d2", "af_lang": "en", "af_ad_id": "21fc90e9-aa82", "af_channel_id": "416", "af_component_id": "md-7d033e38-15b4", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "MEN", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "4af37586-1cd7", "af_lang": "en", "af_ad_id": "3c1a0f72-a0dc", "af_channel_id": "416", "af_component_id": "md-byuu1675165213567", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "ACCESSORIES", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "8c16bc3e-0567", "af_lang": "en", "af_ad_id": "5c7712fe-4b05", "af_channel_id": "416", "af_component_id": "md-byuu1675165213567", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "SWIMWEAR", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "62fdf365-1d8c", "af_lang": "en", "af_ad_id": "e4b8f195-8b42", "af_channel_id": "416", "af_component_id": "md-byuu1675165213567", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "OUTFITS", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "804cc241-77e0", "af_lang": "en", "af_ad_id": "402982d4-b558", "af_channel_id": "416", "af_component_id": "md-byuu1675165213567", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "DRESS", "af_content_type": "banner impression"}}, {"af_impression": {"af_inner_mediasource": "recommend_personal", "af_lang": "en", "af_price": "34.99,34.99,24.99,16.99,26.99,34.99,16.99,19.99,36.99,36.99,16.99,36.99,16.99,24.99,36.99,28.99,16.99,34.99,36.99,24.99,36.99,19.99,22.99,22.99,19.99,24.99,36.99,42.99,34.99,24.99,24.99,26.99,22.99,22.99,19.99,36.99,25.99,22.99,36.99,26.99,22.99,28.99,22.99,18.99,19.99,24.99,19.99,38.99,19.99,38.99,16.99,24.99,41.99,35.99,20.99,36.99,37.99,24.99,24.99,22.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "509168801,509168501,508696603,499613101,499287227,509247301,507930302,499704546,499763801,498667901,507661814,504392301,508510415,508399108,503432601,507361007,507608605,504707401,509197901,448672008,509251301,502007901,475490616,476888602,508976504,469239301,508752701,505863501,499241301,508663205,470963604,495494806,509244203,507579805,505968804,501961801,504303801,466931402,509274801,498468808,495480605,506247326,507551006,476710904,508928907,498498503,326778587,498539701,499825107,509267701,498639203,503921704,507321815,508491101,508446804,509244401,509174401,508740704,480888301,469738309"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "509247301", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Celestial Sun Moon Print Asymmetric Tank Top And Tie Dye Print Cinched Foldover Wide Leg Pants Casual Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "750ed021-1d30", "af_lang": "en", "af_ad_id": "148a4d59-54d2", "af_channel_id": "416", "af_component_id": "md-qhdw1675165723816", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "ALL", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "509244203", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Tie Dye Print Dress Mock Button Crossover High Waisted Sleeveless A Line Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "509153302", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Star Striped Print Dress Colorblock V Neck Sleeveless High Waisted A Line Midi Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508976203", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Gothic Dress Contrast Colorblock Wing Print V Neck High Waisted A Line Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "509363501", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Ribbon Lace Up Metal Buckle Tank Top And Tie Dye Print Asymmetric Handkerchief Mini Skirt Casual Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508822401", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Star Print Pointed Hem Camisole Adjustable Strap Ruched Casual Tank Top", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508976504", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Gothic Dress Contrast Colorblock Skeleton Star Print High Waisted V Neck A Line Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "509364101", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Tie Dye Print Colorblock Cinched Crossover Faux Twinset Tank Top And Contrast Color Asymmetric Mini Skirt Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "509363401", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Tie Dye Print Cold Shoulder Asymmetric Cinched T Shirt And Lace Up Skinny Crop Leggings Casual Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508557205", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Solid Color Multi-way Bandage Dress Plunging Neck Backless Party Midi Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "506711105", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Plain Color Dress Lace Up O Ring Sleeveless A Line Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508567905", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Sparkly Print Asymmetric Tank Dress Scoop Neck Casual Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508548601", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Sunflower Print Vacation Dress Bowknot Shoulder Strap O Ring Self-belt Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508673301", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Tie Dye Daisy Butterfly Print High Low Dress Lace Up Spaghetti Strap Cami Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508164601", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Colorblock Faux Twinset Dress Lace Twisted Scoop Neck High Waisted A Line Midi Twofer Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "501921307", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Cold Shoulder T Shirt O Ring Ruched T-shirt Long V Neck Casual Tee", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "507659003", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Large Pockets Low Cut Casual Mini Dress And Solid Color Tank Top Two Piece Set", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "484958703", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Hooded Contrast Two Tone Cinched Belted Bodycon Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "507719403", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Cold Shoulder Crossover Short Sleeve T-shirt Cinched Ruched Bust V Neck Tee", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "507558001", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Heather Plaid Print Panel T Shirt Cowl Neck Draped Mock Button Short Sleeve Tee", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "449076705", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Tummy Control Tankini Swimwear Vintage Swimsuit Sun Moon Print Lace Up Cut Out Summer Beach Bathing Suit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "476314101", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Gothic Bikini Swimsuit Bat Crescent Star Print Bathing Suit Cinched Mesh Lace Up Cut Out Swimwear", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "988d5ae2-4fac", "af_lang": "en", "af_ad_id": "76a292d5-a752", "af_channel_id": "416", "af_component_id": "md-vcne1679021558390", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "ae313ee2-c216", "af_lang": "en", "af_ad_id": "58f2f465-bbd7", "af_channel_id": "416", "af_component_id": "md-pgxz1679021416453", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "NEW", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "508446801", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Plain Color Tank Top Draped Cowl Neck Chain Embellishment Casual Tank Top", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "507551006", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Short Sleeve Solid Color T-shirt Ruffles Bowknot Ruched Empire Waist Tee", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "508808502", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Sun Moon American Flag Panel Tank Top Cinched Scoop Neck Casual Tank Top", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "509153305", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Star Striped Print Dress Colorblock V Neck Sleeveless High Waisted A Line Midi Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "508070203", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Hooded Bandage Wrap Tankini Swimsuit Handkerchief Skorts Tankini Swimwear Padded Bathing Suit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "501962001", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "American Flag Star Striped Print O Ring Cut Out Handkerchief Tank Top and Lace Up Skinny Crop Leggings Summer Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "508976203", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Gothic Dress Contrast Colorblock Wing Print V Neck High Waisted A Line Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "508730005", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Plus Size Flower Leaf Print A Line Midi Cami Dress And Heather Crossover Tied Cropped Top Two Piece Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "509275401", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Lattice Strappy Tank Top And Ombre Print Cinched Foldover Wide Leg Pants Casual Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "509275001", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Cold Shoulder Ruched Crisscross Adjustable Strap T Shirt And Tie Dye Print Cinched Foldover Wide Leg Pants Casual Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "509275501", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Lace Up Lace Panel Chain Belted Tank Top And High Slit Flare Pants Celestial Sun Moon Cinched Pants Casual Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "509096401", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Paisley Floral Print Top Cold Shoulder Ruffle Top And Plaid Print Cuffed Hem Capri Pants Ethnic Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "508116904", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Tropical Leaf Print Mesh Cover-ups Underwire Bikini Swimsuit Crossover Beach Bikini Three Piece Swimwear Set", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "507787205", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Skull Flower Spider Web Print Gothic Tank Dress O-ring Straps Sleevless A Line Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "509003504", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Plus Size Vacation Dress Allover Leaf Flower Print Surplice High Waisted Belted A Line Maxi Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "508778306", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Plus Size Dress Plain Color Off The Shoulder Ruffle Lace Up Lace Hem Tied A Line Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "508681905", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Colorblock Lace Panel Gothic Mini Dress Lace Up Puff Sleeve Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "507828605", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Allover Floral Leaf Print One-piece Swimsuit Front Twist Cut Out Cinched Monokini Swimwear", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "c6458850-c59c", "af_lang": "en", "af_ad_id": "23db0cc2-0c71", "af_channel_id": "416", "af_component_id": "md-qwqi1675165854048", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "ALL", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "508613405", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Solid Color Short Sleeve Beads Belt Tee Dress V Neck Ruched Casual Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "0990180a-316a", "af_lang": "en", "af_ad_id": "378bda74-d85f", "af_channel_id": "416", "af_component_id": "md-sckq1681296286885", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "code", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "a354dd1b-5933", "af_lang": "en", "af_ad_id": "6caf097f-cc0d", "af_channel_id": "416", "af_component_id": "md-yves1671191514126", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "MYSTERY BOX", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "1b73b0f3-2644", "af_lang": "en", "af_ad_id": "492f6c34-d019", "af_channel_id": "416", "af_component_id": "md-yves1671191514126", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "SALE", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "8be0c387-808f", "af_lang": "en", "af_ad_id": "2eb48848-a008", "af_channel_id": "416", "af_component_id": "md-yves1671191514126", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "APP ONLY", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "a2fc2362-c16d", "af_lang": "en", "af_ad_id": "0a368579-07ed", "af_channel_id": "416", "af_component_id": "md-48c61bba-3cd4", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_click": {"af_col_id": "7ec0ff31-0ed9", "af_lang": "en", "af_ad_id": "507551006", "af_channel_id": "416", "af_component_id": "md-ukmz1679570223703", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Short Sleeve Solid Color T-shirt Ruffles Bowknot Ruched Empire Waist Tee", "af_content_type": "banner impression"}}, {"af_view_product": {"af_inner_mediasource": " recommend_ALL", "af_sort": null, "af_price": "22.99", "af_changed_size_or_color": 0, "af_content_id": "507551006", "af_content_type": "product", "af_currency": "USD", "af_lang": "en", "af_promote_discount": "", "af_country_code": "US", "af_user_type": 1, "af_rank": 0, "af_price_type": "shop_price", "af_promote_type": ""}}, {"af_impression": {"af_inner_mediasource": "recommend_buytogether", "af_lang": "en", "af_price": "6.99,29.99,20.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "508297701,508345309,509300002"}}, {"af_impression": {"af_inner_mediasource": "recommend_productdetail", "af_lang": "en", "af_price": "22.99,12.99,19.99,22.99,22.99,19.99,24.99,24.99,18.99,22.99,16.99,26.99,22.99,16.99,16.99,19.99,24.99,18.99,22.99,18.99,24.99,18.99,16.99,24.99,16.99,22.99,16.99,22.99,16.99,18.99,16.99,26.99,19.99,19.99,19.99,22.99,22.99,22.99,16.99,22.99,22.99,26.99,24.99,19.99,22.99,22.99,16.99,19.99,22.99,19.99,20.99,22.99,26.99,22.99,22.99,20.99,20.99,20.99,22.99,24.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "483896203,475340202,495007403,476807301,477028101,476924202,498493003,498498503,498224509,476888611,481000502,498468808,476714407,495533501,499309501,499253803,495046112,502274303,499679402,480962902,503698106,499328401,503997203,504026802,499657402,504610102,499774002,502989003,507661502,501921307,507497405,495562323,507795604,507558202,499704545,507641905,507696503,495279714,507719403,508528002,508961703,508800802,508801003,508928907,508844409,508214003,508613102,508651204,509099302,508040304,508333702,508214204,508535905,509000905,508084503,508446603,508199702,509244805,508831504,495505206"}}, {"af_enter_app": {"af_lang": null, "af_country_code": null, "af_user_type": 0, "af_content_type": "enter app"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "DRESSES", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "WOMEN", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "SWIMWEAR", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "OUTFITS", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "TOPS", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "PLUS SIZE", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "BOTTOMS ", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "MEN", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_click": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 2, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "-2", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "Vitural_Catentrance_impression", "af_cat_name": "NEW"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "ACCESSORIES", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_click": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Dress"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Bottoms"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 2, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "-2", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "Vitural_Catentrance_impression", "af_cat_name": "NEW"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Outfits"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Tops"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Men"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Plus Size"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "galaxy dress", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Dress"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Accessories"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "galaxy top", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Tops"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "galaxy", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "galaxy plus size", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Plus Size"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "galaxy swimwear", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Swimwear"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "galaxy outfit", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Outfits"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 1, "af_cat_value": "moon accessory", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Acessories"}}, {"af_view_homepage": {"af_lang": "en", "af_channel_id": "416", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_content_type": "view homepage"}}, {"af_banner_impression": {"af_col_id": null, "af_lang": "en", "af_ad_id": "24", "af_component_id": "14", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "Homepage", "af_banner_name": "Hey New Friend!", "af_content_type": "banner impression"}}, {"af_impression": {"af_inner_mediasource": "recommend_personal", "af_lang": "en", "af_price": "34.99,34.99,24.99,16.99,26.99,34.99,16.99,19.99,36.99,36.99,16.99,36.99,16.99,24.99,36.99,28.99,16.99,34.99,36.99,24.99,36.99,19.99,22.99,22.99,19.99,24.99,36.99,42.99,34.99,24.99,24.99,26.99,22.99,22.99,19.99,36.99,25.99,22.99,36.99,26.99,22.99,28.99,22.99,18.99,19.99,24.99,19.99,38.99,19.99,38.99,16.99,24.99,41.99,35.99,20.99,36.99,37.99,24.99,24.99,22.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "509168801,509168501,508696603,499613101,499287227,509247301,507930302,499704546,499763801,498667901,507661814,504392301,508510415,508399108,503432601,507361007,507608605,504707401,509197901,448672008,509251301,502007901,475490616,476888602,508976504,469239301,508752701,505863501,499241301,508663205,470963604,495494806,509244203,507579805,505968804,501961801,504303801,466931402,509274801,498468808,495480605,506247326,507551006,476710904,508928907,498498503,326778587,498539701,499825107,509267701,498639203,503921704,507321815,508491101,508446804,509244401,509174401,508740704,480888301,469738309"}}, {"af_view_homepage": {"af_lang": "en", "af_channel_id": "416", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_content_type": "view homepage"}}, {"af_view_homepage": {"af_lang": "en", "af_channel_id": "416", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_content_type": "view homepage"}}, {"af_view_homepage": {"af_lang": "en", "af_channel_id": "389", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_content_type": "view homepage"}}, {"af_view_homepage": {"af_lang": "en", "af_channel_id": "394", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_content_type": "view homepage"}}, {"af_view_homepage": {"af_lang": "en", "af_channel_id": "392", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_content_type": "view homepage"}}, {"af_impression": {"af_inner_mediasource": "recommend_personal", "af_lang": "en", "af_price": "34.99,34.99,24.99,16.99,26.99,34.99,16.99,19.99,36.99,36.99,16.99,36.99,16.99,24.99,36.99,28.99,16.99,34.99,36.99,24.99,36.99,19.99,22.99,22.99,19.99,24.99,36.99,42.99,34.99,24.99,24.99,26.99,22.99,22.99,19.99,36.99,25.99,22.99,36.99,26.99,22.99,28.99,22.99,18.99,19.99,24.99,19.99,38.99,19.99,38.99,16.99,24.99,41.99,35.99,20.99,36.99,37.99,24.99,24.99,22.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "509168801,509168501,508696603,499613101,499287227,509247301,507930302,499704546,499763801,498667901,507661814,504392301,508510415,508399108,503432601,507361007,507608605,504707401,509197901,448672008,509251301,502007901,475490616,476888602,508976504,469239301,508752701,505863501,499241301,508663205,470963604,495494806,509244203,507579805,505968804,501961801,504303801,466931402,509274801,498468808,495480605,506247326,507551006,476710904,508928907,498498503,326778587,498539701,499825107,509267701,498639203,503921704,507321815,508491101,508446804,509244401,509174401,508740704,480888301,469738309"}}, {"af_view_homepage": {"af_lang": "en", "af_channel_id": "426", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ACCESSORIES", "af_content_type": "view homepage"}}, {"af_banner_impression": {"af_col_id": "a07e8a91-ae09", "af_lang": "en", "af_ad_id": "3b79ab5f-3c4e", "af_channel_id": "416", "af_component_id": "md-abhn1681888595524", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Great Deals", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "a07e8a91-ae09", "af_lang": "en", "af_ad_id": "524f8e4c-c260", "af_channel_id": "416", "af_component_id": "md-abhn1681888595524", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Tie Dye Collection", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "03fadd10-6e71", "af_lang": "en", "af_ad_id": "66cecc51-3d51", "af_channel_id": "416", "af_component_id": "md-37defe76-2c00", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "\u6807\u7b7e", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "a07e8a91-ae09", "af_lang": "en", "af_ad_id": "7559bf24-f70c", "af_channel_id": "416", "af_component_id": "md-abhn1681888595524", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "New Arrivals", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "0f0fe0e4-c735", "af_lang": "en", "af_ad_id": "73dbad2d-a129", "af_channel_id": "416", "af_component_id": "md-32324602-e671", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "572407c4-c9dc", "af_lang": "en", "af_ad_id": "b2509a40-8ad2", "af_channel_id": "416", "af_component_id": "md-ee43c6e6-dfd9", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "30fc2875-4933", "af_lang": "en", "af_ad_id": "1583ff67-f230", "af_channel_id": "416", "af_component_id": "md-3fbad2a1-0138", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "WOMEN", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "3736afa9-5f78", "af_lang": "en", "af_ad_id": "29472f72-6c74", "af_channel_id": "416", "af_component_id": "md-3fbad2a1-0138", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "PLUS SIZE", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "4dd2fec0-33d2", "af_lang": "en", "af_ad_id": "21fc90e9-aa82", "af_channel_id": "416", "af_component_id": "md-7d033e38-15b4", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "MEN", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "62fdf365-1d8c", "af_lang": "en", "af_ad_id": "e4b8f195-8b42", "af_channel_id": "416", "af_component_id": "md-byuu1675165213567", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "OUTFITS", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "da3cb2b6-b165", "af_lang": "en", "af_ad_id": "48df86bc-8a89", "af_channel_id": "416", "af_component_id": "md-7d033e38-15b4", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "WOMEN", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "4af37586-1cd7", "af_lang": "en", "af_ad_id": "3c1a0f72-a0dc", "af_channel_id": "416", "af_component_id": "md-byuu1675165213567", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "ACCESSORIES", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "8c16bc3e-0567", "af_lang": "en", "af_ad_id": "5c7712fe-4b05", "af_channel_id": "416", "af_component_id": "md-byuu1675165213567", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "SWIMWEAR", "af_content_type": "banner impression"}}, {"af_impression": {"af_inner_mediasource": "recommend_personal", "af_lang": "en", "af_price": "34.99,34.99,24.99,16.99,26.99,34.99,16.99,19.99,36.99,36.99,16.99,36.99,16.99,24.99,36.99,28.99,16.99,34.99,36.99,24.99,36.99,19.99,22.99,22.99,19.99,24.99,36.99,42.99,34.99,24.99,24.99,26.99,22.99,22.99,19.99,36.99,25.99,22.99,36.99,26.99,22.99,28.99,22.99,18.99,19.99,24.99,19.99,38.99,19.99,38.99,16.99,24.99,41.99,35.99,20.99,36.99,37.99,24.99,24.99,22.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "509168801,509168501,508696603,499613101,499287227,509247301,507930302,499704546,499763801,498667901,507661814,504392301,508510415,508399108,503432601,507361007,507608605,504707401,509197901,448672008,509251301,502007901,475490616,476888602,508976504,469239301,508752701,505863501,499241301,508663205,470963604,495494806,509244203,507579805,505968804,501961801,504303801,466931402,509274801,498468808,495480605,506247326,507551006,476710904,508928907,498498503,326778587,498539701,499825107,509267701,498639203,503921704,507321815,508491101,508446804,509244401,509174401,508740704,480888301,469738309"}}, {"af_banner_impression": {"af_col_id": "804cc241-77e0", "af_lang": "en", "af_ad_id": "402982d4-b558", "af_channel_id": "416", "af_component_id": "md-byuu1675165213567", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "DRESS", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "5cb0f342-3c7d", "af_lang": "en", "af_ad_id": "35c48273-d0d7", "af_channel_id": "392", "af_component_id": "md-ab6c4b67-5f32", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "11f53813-cd0d", "af_lang": "en", "af_ad_id": "fb0bf51c-050b", "af_channel_id": "392", "af_component_id": "md-015b603c-ec68", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "Great Deals", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "11f53813-cd0d", "af_lang": "en", "af_ad_id": "bdddc988-ea69", "af_channel_id": "392", "af_component_id": "md-015b603c-ec68", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "Plus Size", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "11f53813-cd0d", "af_lang": "en", "af_ad_id": "4e5328dd-ce7c", "af_channel_id": "392", "af_component_id": "md-015b603c-ec68", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "New Arrivals", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "11f53813-cd0d", "af_lang": "en", "af_ad_id": "27a7452c-45ea", "af_channel_id": "392", "af_component_id": "md-015b603c-ec68", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "Tie Dye Collection", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "2a7fb97a-fb7b", "af_lang": "en", "af_ad_id": "ffad17e2-db95", "af_channel_id": "392", "af_component_id": "md-95f43d08-5288", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "3336266d-aff9", "af_lang": "en", "af_ad_id": "d786dd57-d2a4", "af_channel_id": "392", "af_component_id": "md-9a6a04b7-a6ca", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "c5707842-e91f", "af_lang": "en", "af_ad_id": "e8483f52-ba5e", "af_channel_id": "392", "af_component_id": "md-93cd62ed-f55b", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "DRESSES", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "95805eb9-ec70", "af_lang": "en", "af_ad_id": "3ee58b42-669e", "af_channel_id": "392", "af_component_id": "md-93cd62ed-f55b", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "Tops", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "6dd6dcd9-50cc", "af_lang": "en", "af_ad_id": "9a4a31a8-4c9a", "af_channel_id": "392", "af_component_id": "md-93cd62ed-f55b", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "Bottoms", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7987622e-1419", "af_lang": "en", "af_ad_id": "a15e1f45-bd8c", "af_channel_id": "392", "af_component_id": "md-f03d6711-edd1", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "Outfits", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "aaef55c4-4ad6", "af_lang": "en", "af_ad_id": "91b2f133-e5b4", "af_channel_id": "392", "af_component_id": "md-93cd62ed-f55b", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "Swimwear", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "d1cf8285-5d05", "af_lang": "en", "af_ad_id": "bc01383d-82df", "af_channel_id": "392", "af_component_id": "md-f03d6711-edd1", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "Lingreries", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "412066e3-3c05", "af_lang": "en", "af_ad_id": "040ba04a-2dab", "af_channel_id": "392", "af_component_id": "md-f03d6711-edd1", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "PLUS SIZE", "af_banner_name": "TOPS", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "4915344e-6792", "af_lang": "en", "af_ad_id": "494f1563-f0fd", "af_channel_id": "426", "af_component_id": "md-7830a7ec-c4c7", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ACCESSORIES", "af_banner_name": "New Arrivals", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "2a0bdcd9-a9a6", "af_lang": "en", "af_ad_id": "c4634da8-2a7a", "af_channel_id": "426", "af_component_id": "md-c5867002-7011", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ACCESSORIES", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "61023f2e-d6e1", "af_lang": "en", "af_ad_id": "dd189732-857e", "af_channel_id": "426", "af_component_id": "md-17c5febd-3930", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ACCESSORIES", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "f8fd53e1-c25d", "af_lang": "en", "af_ad_id": "20f73103-6de2", "af_channel_id": "389", "af_component_id": "md-a2d11a9e-6fb6", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "Great Deals", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "4915344e-6792", "af_lang": "en", "af_ad_id": "82576c30-749c", "af_channel_id": "426", "af_component_id": "md-7830a7ec-c4c7", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ACCESSORIES", "af_banner_name": "ACCESSORIES", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "3eae361c-74ba", "af_lang": "en", "af_ad_id": "4daa4e3a-aa17", "af_channel_id": "389", "af_component_id": "md-0c96cf63-56bc", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "f8fd53e1-c25d", "af_lang": "en", "af_ad_id": "9a3daed0-1b3b", "af_channel_id": "389", "af_component_id": "md-a2d11a9e-6fb6", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "New Arrivals", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "1264a00c-5dd6", "af_lang": "en", "af_ad_id": "4fa7ab42-3535", "af_channel_id": "394", "af_component_id": "md-c7d370e3-ede9", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "Men's Clothing", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "71cc62ea-67d2", "af_lang": "en", "af_ad_id": "c90e992d-48c2", "af_channel_id": "426", "af_component_id": "md-829eaf44-f629", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ACCESSORIES", "af_banner_name": "CHOKER", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "f8fd53e1-c25d", "af_lang": "en", "af_ad_id": "7076f0a6-aafa", "af_channel_id": "389", "af_component_id": "md-a2d11a9e-6fb6", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "Tie Dye Collection", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "c2f7a12b-1206", "af_lang": "en", "af_ad_id": "b68e4ec5-608a", "af_channel_id": "394", "af_component_id": "md-fe2a959a-9c97", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "CODE", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "1264a00c-5dd6", "af_lang": "en", "af_ad_id": "6105356b-3f35", "af_channel_id": "394", "af_component_id": "md-c7d370e3-ede9", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "Great Deals", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "3c19f065-0077", "af_lang": "en", "af_ad_id": "eb97b7d1-96f8", "af_channel_id": "426", "af_component_id": "md-829eaf44-f629", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ACCESSORIES", "af_banner_name": "NECKLACE", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "2b33aad0-5626", "af_lang": "en", "af_ad_id": "6e795f88-694b", "af_channel_id": "426", "af_component_id": "md-829eaf44-f629", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ACCESSORIES", "af_banner_name": "RINGS", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "1264a00c-5dd6", "af_lang": "en", "af_ad_id": "51c84415-8eba", "af_channel_id": "394", "af_component_id": "md-c7d370e3-ede9", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "New Arrivals", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "8a095e55-a3c0", "af_lang": "en", "af_ad_id": "3820fdc1-4f46", "af_channel_id": "426", "af_component_id": "md-829eaf44-f629", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ACCESSORIES", "af_banner_name": "EARRINGS", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "6a1e9339-5df5", "af_lang": "en", "af_ad_id": "6ac67bc8-1c7a", "af_channel_id": "389", "af_component_id": "md-31168c97-0d8c", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "bd3bfc7f-3f0e", "af_lang": "en", "af_ad_id": "f04b95c8-2db9", "af_channel_id": "394", "af_component_id": "md-54370547-c4ce", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "5fd7c032-9c94", "af_lang": "en", "af_ad_id": "cd1017c7-a214", "af_channel_id": "426", "af_component_id": "md-7c8cb3b5-8c04", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ACCESSORIES", "af_banner_name": "HAIR ACCESSORIES", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "58d58f8e-80c0", "af_lang": "en", "af_ad_id": "827c66cb-0c48", "af_channel_id": "394", "af_component_id": "md-d760e0b8-fe49", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "Tops", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "802dadfc-1ebd", "af_lang": "en", "af_ad_id": "45afda84-a3ba", "af_channel_id": "394", "af_component_id": "md-240d154c-c7cf", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "14404651-5c62", "af_lang": "en", "af_ad_id": "eac71761-7984", "af_channel_id": "389", "af_component_id": "md-97478e82-50f3", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "83c6a872-f939", "af_lang": "en", "af_ad_id": "17811be2-7e2b", "af_channel_id": "389", "af_component_id": "md-2277f3f3-8582", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "bc9d583e-4447", "af_lang": "en", "af_ad_id": "9daf2beb-8892", "af_channel_id": "426", "af_component_id": "md-7c8cb3b5-8c04", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ACCESSORIES", "af_banner_name": "WIGS", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "01df6f59-053e", "af_lang": "en", "af_ad_id": "835ad439-c7e3", "af_channel_id": "426", "af_component_id": "md-7c8cb3b5-8c04", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ACCESSORIES", "af_banner_name": "SHOES", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "fc08ffa7-12ba", "af_lang": "en", "af_ad_id": "9e5c36e1-8c76", "af_channel_id": "389", "af_component_id": "md-97478e82-50f3", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "Tops", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "c860fdbd-4535", "af_lang": "en", "af_ad_id": "c04da1d0-7cdc", "af_channel_id": "389", "af_component_id": "md-97478e82-50f3", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "Bottoms", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "7765753a-244a", "af_lang": "en", "af_ad_id": "8867b8b0-1a20", "af_channel_id": "426", "af_component_id": "md-7c8cb3b5-8c04", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ACCESSORIES", "af_banner_name": "HEADBAND", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "827a2ff1-bcd6", "af_lang": "en", "af_ad_id": "c8b12044-3768", "af_channel_id": "389", "af_component_id": "md-97478e82-50f3", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "Swimwear", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "6200d5c6-50be", "af_lang": "en", "af_ad_id": "bcec4548-fa5e", "af_channel_id": "394", "af_component_id": "md-d760e0b8-fe49", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "T-shirts", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "9cf84285-2ea7", "af_lang": "en", "af_ad_id": "021bf04a-21d6", "af_channel_id": "394", "af_component_id": "md-d760e0b8-fe49", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "Hoodies", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "c61c15a5-7c4e", "af_lang": "en", "af_ad_id": "1d90bfbc-ee60", "af_channel_id": "394", "af_component_id": "md-d760e0b8-fe49", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "Jacket", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "ff76cf6f-9332", "af_lang": "en", "af_ad_id": "210e00c1-ff88", "af_channel_id": "389", "af_component_id": "md-a23ae37f-0eb3", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "Outfits", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "01532dde-1a7f", "af_lang": "en", "af_ad_id": "d6b0b434-7b24", "af_channel_id": "394", "af_component_id": "md-f49415cb-32b7", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "Bottoms", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "ae74289b-3052", "af_lang": "en", "af_ad_id": "a8403d64-dc53", "af_channel_id": "389", "af_component_id": "md-a23ae37f-0eb3", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "Sweaters", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "3e375b67-59c8", "af_lang": "en", "af_ad_id": "7f0f667d-6283", "af_channel_id": "394", "af_component_id": "md-f49415cb-32b7", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "Jeans", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "83e116f1-8029", "af_lang": "en", "af_ad_id": "71cf0255-92e5", "af_channel_id": "394", "af_component_id": "md-f49415cb-32b7", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "Pants", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "e0c441c6-4384", "af_lang": "en", "af_ad_id": "efb6a3c1-bd9e", "af_channel_id": "394", "af_component_id": "md-f49415cb-32b7", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "Shorts", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "e5cedbb1-32f0", "af_lang": "en", "af_ad_id": "e876dd8e-5bfa", "af_channel_id": "389", "af_component_id": "md-a23ae37f-0eb3", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "Hoodies", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "e5afa2b6-c958", "af_lang": "en", "af_ad_id": "ece0cda0-c511", "af_channel_id": "394", "af_component_id": "md-a199855b-c9e6", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "MEN", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "53ad8718-2855", "af_lang": "en", "af_ad_id": "134b404e-7ed6", "af_channel_id": "389", "af_component_id": "md-a23ae37f-0eb3", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "WOMEN", "af_banner_name": "Coats", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "750ed021-1d30", "af_lang": "en", "af_ad_id": "148a4d59-54d2", "af_channel_id": "416", "af_component_id": "md-qhdw1675165723816", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "ALL", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "509247301", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Celestial Sun Moon Print Asymmetric Tank Top And Tie Dye Print Cinched Foldover Wide Leg Pants Casual Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "509244203", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Tie Dye Print Dress Mock Button Crossover High Waisted Sleeveless A Line Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "509153302", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Star Striped Print Dress Colorblock V Neck Sleeveless High Waisted A Line Midi Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508822401", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Star Print Pointed Hem Camisole Adjustable Strap Ruched Casual Tank Top", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508976504", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Gothic Dress Contrast Colorblock Skeleton Star Print High Waisted V Neck A Line Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "509364101", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Tie Dye Print Colorblock Cinched Crossover Faux Twinset Tank Top And Contrast Color Asymmetric Mini Skirt Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508557205", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Solid Color Multi-way Bandage Dress Plunging Neck Backless Party Midi Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508976203", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Gothic Dress Contrast Colorblock Wing Print V Neck High Waisted A Line Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "509363501", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Ribbon Lace Up Metal Buckle Tank Top And Tie Dye Print Asymmetric Handkerchief Mini Skirt Casual Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508673301", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Tie Dye Daisy Butterfly Print High Low Dress Lace Up Spaghetti Strap Cami Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "509363401", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Tie Dye Print Cold Shoulder Asymmetric Cinched T Shirt And Lace Up Skinny Crop Leggings Casual Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "501921307", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Cold Shoulder T Shirt O Ring Ruched T-shirt Long V Neck Casual Tee", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508548601", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Sunflower Print Vacation Dress Bowknot Shoulder Strap O Ring Self-belt Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "506711105", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Plain Color Dress Lace Up O Ring Sleeveless A Line Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508164601", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Colorblock Faux Twinset Dress Lace Twisted Scoop Neck High Waisted A Line Midi Twofer Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "508567905", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Sparkly Print Asymmetric Tank Dress Scoop Neck Casual Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "507659003", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Large Pockets Low Cut Casual Mini Dress And Solid Color Tank Top Two Piece Set", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "507719403", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Cold Shoulder Crossover Short Sleeve T-shirt Cinched Ruched Bust V Neck Tee", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "484958703", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Hooded Contrast Two Tone Cinched Belted Bodycon Mini Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "476314101", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Gothic Bikini Swimsuit Bat Crescent Star Print Bathing Suit Cinched Mesh Lace Up Cut Out Swimwear", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "449076705", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Tummy Control Tankini Swimwear Vintage Swimsuit Sun Moon Print Lace Up Cut Out Summer Beach Bathing Suit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "507558001", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Heather Plaid Print Panel T Shirt Cowl Neck Draped Mock Button Short Sleeve Tee", "af_content_type": "banner impression"}}, {"af_banner_click": {"af_col_id": "30fc2875-4933", "af_lang": "en", "af_ad_id": "1583ff67-f230", "af_channel_id": "416", "af_component_id": "md-3fbad2a1-0138", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "WOMEN", "af_content_type": "banner impression"}}, {"af_impression": {"af_inner_mediasource": "category_1", "af_lang": "en", "af_sort": "Hot", "af_price": "34.99,24.99,34.99,34.99,26.99,16.99,19.99,36.99,16.99,16.99,36.99,36.99,22.99,36.99,36.99,16.99,24.99,36.99,34.99,22.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "509168801,508696603,509247301,509168501,499287227,499613101,499704512,509197901,507930303,507661815,499763801,498667901,509244203,509274801,509251301,507608602,448672008,504392301,504707401,507551006"}}]

conversion_data = {"advertiserId": "cba689e9-1f16-4c06-a066-c4d29483d803", "advertiserIdEnabled": "true", "af_events_api": "1", "af_preinstalled": "false", "af_timestamp": "1681896590098", "af_v": "bbd7fd73abd77b1b82aaf34a21d14f7ef83919d2", "af_v2": "2cf02931827d470493c5c220417693e2470291b7", "app_version_code": "139", "app_version_name": "7.3.3", "appsflyerKey": "PQmjs6dfikqatrWRQ4EEG", "brand": "xiaomi", "carrier": "", "cell": {"mcc": 0, "mnc": 0}, "cksm_v1": "9aa9dcc466ff6d18afead78049568fccb0", "counter": "3", "country": "GB", "date1": "2023-04-19_145531+0530", "date2": "2023-04-19_145531+0530", "device": "violet", "deviceData": {"arch": "", "btch": "ac", "btl": "6.0", "build_display_id": "PKQ1.181203.001", "cpu_abi": "arm64-v8a", "cpu_abi2": "", "dim": {"d_dpi": "440", "size": "2", "x_px": "1080", "xdp": "409.432", "y_px": "2130", "ydp": "409.903"}}, "deviceType": "user", "disk": "15778/51701", "exception_number": 1, "firstLaunchDate": "2023-04-19_092703+0000", "iaecounter": "1", "installDate": "2023-04-19_092531+0000", "installer_package": "com.android.vending", "isFirstCall": "false", "isGaidWithGps": "true", "ivc": false, "kef7268": "d7c0677efc41e0dc4818121b0c531a0d591f180d5a1e1a", "lang": "English", "lang_code": "en", "last_boot_time": 1681896136339, "meta": {"gcd": {"net": 2438, "retries": 0}}, "model": "Redmi Note 7 Pro", "network": "WIFI", "onelink_id": "t6H2", "onelink_ver": "6", "open_referrer": "android-app://com.globalegrow.app.dresslily", "operator": "", "platformextension": "android_native", "prev_session_dur": 145, "product": "violet", "registeredUninstall": false, "sc_o": "p", "sdk": "28", "sig": "B262ECBAF629B2D7A9958EBB894E60A27E86A251D5036B53C37624D0D7102628", "timepassedsincelastlaunch": "166", "tokenRefreshConfigured": false, "uid": "1681896422826-5557922205123249938"}

inapp_data = {"advertiserId": "cba689e9-1f16-4c06-a066-c4d29483d803", "advertiserIdEnabled": "true", "af_events_api": "1", "af_preinstalled": "false", "af_timestamp": "1681896422843", "af_v": "8dae94faac141a37046234adfe9658bde876d713", "af_v2": "52e659537081b96abb750b2a559df8aef897ecec", "app_version_code": "139", "app_version_name": "7.3.3", "appsflyerKey": "PQmjs6dfikqatrWRQ4EEG", "brand": "xiaomi", "carrier": "", "cell": {"mcc": 0, "mnc": 0}, "cksm_v1": "b04f52e3e5bc4961cf87b8969a5f7f937c", "counter": "2", "country": "GB", "date1": "2023-04-19_145531+0530", "date2": "2023-04-19_145531+0530", "device": "violet", "deviceData": {"arch": "", "build_display_id": "PKQ1.181203.001", "cpu_abi": "arm64-v8a", "cpu_abi2": "", "dim": {"d_dpi": "440", "size": "2", "x_px": "1080", "xdp": "409.432", "y_px": "2130", "ydp": "409.903"}}, "deviceType": "user", "disk": "15821/51701", "eventName": "af_enter_app", "eventValue": "{\"af_lang\":null,\"af_country_code\":null,\"af_user_type\":0,\"af_content_type\":\"enter app\"}", "firstLaunchDate": "2023-04-19_092703+0000", "iaecounter": "1", "installDate": "2023-04-19_092531+0000", "installer_package": "com.android.vending", "isFirstCall": "true", "isGaidWithGps": "true", "ivc": false, "kef7965": "cbd1f81b7c84f3ad48181d1e0c531a0d591f180d5a1f1d", "lang": "English", "lang_code": "en", "last_boot_time": 1681896136339, "model": "Redmi Note 7 Pro", "network": "WIFI", "operator": "", "p_receipt": {"an": {"dbg": false, "hk": ";"}, "pr": {"ac": "0", "ah": "zygote64_32", "ai": "0", "ak": "default", "al": "cortex-a9", "am": "default", "an": "generic", "ap": "builder", "ar": "qcom", "as": "arm64-v8a", "at": "arm64-v8a,armeabi-v7a,armeabi", "au": "armeabi-v7a,armeabi", "av": "arm64-v8a"}}, "platformextension": "android_native", "product": "violet", "registeredUninstall": false, "sc_o": "p", "sdk": "28", "sensors": {"am": {"n": "lsm6ds3c Accelerometer Non-wakeup", "v": [[-0.4, 5.8, 7.5], [0, 0, 0.1]]}, "gs": {"n": "lsm6ds3c Gyroscope Non-wakeup", "v": [[0, 0, 0], [0, 0, 0]]}, "mm": {"n": "ak0991x Magnetometer Non-wakeup", "v": [[26.2, -36.7], [-3.4, 2.3]]}}, "sig": "B262ECBAF629B2D7A9958EBB894E60A27E86A251D5036B53C37624D0D7102628", "tokenRefreshConfigured": false, "uid": "1681896422826-5557922205123249938"}



# ==================== Appsflyer Conversion ====================
def appsflyer_conversions( app_data, device_data ):

    inc_(app_data,'counter')
    def_(app_data,'iaecounter')
    set_batteryLevel(app_data)

    if not app_data.get('last_launch'):
        timeSinceLastCall = "-1"
    else:
        timeSinceLastCall = int(time.time())-app_data.get('last_launch')

    app_data['last_launch'] = int(time.time())

    method = "post"

    url = 'https://'+Appsflyer_data.get('con_url')+'/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'

    headers = {
        "Accept-Encoding"   : "gzip",
        "Content-Type"      : "application/octet-stream",
        "User-Agent"        : get_ua(device_data)
    }

    params = {
        "app_id"        : campaign_data.get('package_name'),
        "buildnumber"   : campaign_data.get('appsflyer').get('buildnumber'),
    }
    
    if not app_data.get('disk'):
        app_data['disk'] = "{available}/{total}".format(available =str(random.randint(8000, 16000)), total = str(random.randint(23000, 32000)))
    else:
        temp_disk = map(int, app_data.get('disk').split('/'))
        temp_disk[0] += random.randint(-150, 300)

        app_data['disk'] = "{available}/{total}".format(available = str(temp_disk[0]), total = str(temp_disk[1]))
    app_data['session_time'] = time.time()

    data = {
        "advertiserId"          : device_data.get('adid'), 
        "advertiserIdEnabled"   : "true", 
        "af_events_api"         : "1", 
        "af_preinstalled"       : "false", 
        "af_timestamp"          : str(int(time.time()*1000)), 
        "app_version_code"      : campaign_data.get('app_version_code'),
        "app_version_name"      : campaign_data.get('app_version_name'),
        "appsflyerKey"          : campaign_data.get('appsflyer').get('key'),
        "batteryLevel"          : app_data.get('btl'), 
        "brand"                 : device_data.get('brand'), 
        "carrier"               : device_data.get('carrier'), 
        "cell": {
            "mcc"               : int(device_data.get('mcc')), 
            "mnc"               : int(device_data.get('mnc'))
        },  
        "counter"               : str(app_data.get('counter')), 
        "country"               : device_data.get('locale').get('country'), 
        "date1"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "date2"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "device"                : device_data.get('device'), 
        "deviceData"            :
         {
            "arch"              : "", 
            "btch"              : app_data.get('btch'), 
            "btl"               : app_data.get('btl'), 
            "build_display_id"  : device_data.get('build'), 
            "cpu_abi"           : device_data.get('cpu_abi'), 
            "cpu_abi2"          : device_data.get('cpu_abi2'), 
            "dim"               :
             {
                "d_dpi"         : device_data.get('dpi'), 
                "size"          : app_data.get('dim_size'), 
                "x_px"          : device_data.get('resolution').split('x')[1],
                "xdp"           : app_data.get('xdp'), 
                "y_px"          : device_data.get('resolution').split('x')[0], 
                "ydp"           : app_data.get('ydp')
            },
            "sensors":[{
                "sN": "ICM20607 Accelerometer",
                "sT": 1,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,1),10),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(8,10),6)
                ],
                "sVS": [
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(8,10),6)
                ]
            },
            {
                "sN": "ICM20607 Gyroscope",
                "sT": 4,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,-1),10),
                    round(random.uniform(0,-1),10),
                    round(random.uniform(0,1),10)
                ],
                "sVS": [
                    round(random.uniform(0,1),10),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,1),10)
                ]
            },
            {
                "sN": "ICM20607 Gyroscope -Wakeup Secondary",
                "sT": 4,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,1),11),
                    round(random.uniform(0,-1),11),
                    round(random.uniform(0,1),10)
                ],
                "sVS": [
                    round(random.uniform(0,1),11),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,1),10)
                ]
            },
            {
                "sN": "AK09918 Magnetometer -Wakeup Secondary",
                "sT": 2,
                "sV": "AKM",
                "sVE": [
                    round(random.uniform(-13,-11),6),
                    round(random.uniform(16,19),6),
                    round(random.uniform(-45,-42),5)
                ],
                "sVS": [
                    round(random.uniform(-13,-11),6),
                    round(random.uniform(16,19),6),
                    round(random.uniform(-45,-42),5)
                ]
            },
            {
                "sN": "ICM20607 Accelerometer -Wakeup Secondary",
                "sT": 1,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,1),11),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(8,10),6)
                ],
                "sVS": [
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,-1),8),
                    round(random.uniform(8,10),6)
                ]
            },
            {
                "sN": "AK09918 Magnetometer",
                "sT": 2,
                "sV": "AKM",
                "sVE": [
                    round(random.uniform(-14,-12),6),
                    round(random.uniform(18,21),6),
                    round(random.uniform(-45,-42),6)
                ],
                "sVS": [
                    round(random.uniform(-14,-12),6),
                    round(random.uniform(18,21),6),
                    round(random.uniform(-45,-42),6)
                ]
            }
        ]
        },
        "deviceType"        : "user", 
        "disk"              : app_data.get('disk'),
        "exception_number"  : 0,
        "iaecounter"        : str(app_data.get('iaecounter')), 
        "installDate"       : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000', 
        "installer_package" : "com.android.vending", 
        "isFirstCall"       : "false" if app_data.get('counter') > 1 else "true", 
        "isGaidWithGps"     : "true", 
        "ivc"               : False, 
        "lang"              : "English", 
        "lang_code"         : "en", 
        "last_boot_time"    : app_data.get('last_boot_time'),
        "meta": {
            "first_launch": {
                "init_to_fg": random.randint(5,20)
            }
        },
        "model"             : device_data.get('model'), 
        "network"           : device_data.get('network').upper(), 
        "open_referrer"     : "android-app://"+campaign_data.get("package_name"), 
        "operator"          : device_data.get('carrier'), 
        "p_receipt"         :
             {
            "an": {
                "dbg"       : False, 
                "hk"        : ";"
            }, 
            "as"            :
                {
                "cav": random.randint(5,40),
                "null": random.randint(5,35),
                "other": random.randint(5,15)
            },
            "pr":{
                "ac": "0",
                "ah": "zygote64_32",
                "ai": "0",
                "ak": "default",
                "al": "cortex-a53",
                "am": "default",
                "an": "generic",
                "ap": "builder",
                "ar": "qcom",
                "as": device_data.get('cpu_abi'),
                "at": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "au": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "av": device_data.get('cpu_abi')
            }
        }, 
        "platformextension"         : campaign_data.get("platformextension"), 
        "product"                   : device_data.get('product'), 
        "referrers": [
        {
            "api_ver": campaign_data.get('api_ver'), 
            "api_ver_name": campaign_data.get('api_ver_name'), 
            "click_ts": int(app_data.get('times').get('click_time')),  
            "google_custom": { 
                "click_server_ts": int(app_data.get('times').get('click_time'))-1, 
                "install_begin_server_ts": int(app_data.get('times').get('download_begin_time'))-2, 
                "install_version": campaign_data.get('app_version_name'), 
                "instant": False
                },
            "install_begin_ts"  : int(app_data.get('times').get('download_begin_time')), 
            "latency"           : 92,
            "referrer"          : app_data.get("referrer", "utm_source=(not%20set)&utm_medium=(not%20set)"), 
            "response"          : "OK", 
            "source"            : "google", 
            "type"              : "store"
        }
        ], 
        "registeredUninstall"       : False, 
        "rfr"                       :
         {  
            "clk"                   : str(int(app_data.get('times').get('click_time'))), 
            "code"                  : "0", 
            "install"               : str(int(app_data.get('times').get('download_begin_time'))),
            "instant"               : False,
            "val"                   : app_data.get("referrer", "utm_source=(not%20set)&utm_medium=(not%20set)")
        },  
        "sc_o"                      : "l", 
        "sdk"                       : device_data.get('sdk'), 
        "timepassedsincelastlaunch" : str(timeSinceLastCall), 
        "tokenRefreshConfigured"    : False, 
    }

    if campaign_data.get('sig'):
        data["sig"]  = campaign_data.get("sig")


    if conversion_data.get('android_id'):
        data['android_id'] = device_data.get('android_id')
    if conversion_data.get('appUserId'):
        data['appUserId'] = app_data.get('appUserId')
    if conversion_data.get('customData'):
        data['customData'] = app_data.get('customData')
    if conversion_data.get('fb'): 
        data['fb'] =  app_data.get('fb')
    if conversion_data.get('user_emails'): 
        data['user_emails'] = app_data.get('user_emails')
    if conversion_data.get('sdkExtension'): 
        data['sdkExtension'] = conversion_data.get('sdkExtension')
    


    data['uid']                 = app_data.get('uid')
    data['firstLaunchDate']     = app_data.get('firstLaunchDate')
    data['cksm_v1']             = cksm_v1(data['date1'],data['af_timestamp'])
    data['af_v']                = util.sha1(data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13])
    data['af_v2']               = util.sha1(util.md5(data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')))

    

    if app_data.get('counter') > 2:
        del data['deviceData']['sensors']
        if data.get('rfr'):
            del data['rfr']
        if data.get('p_receipt'):
            del data['p_receipt']

    if data.get('isFirstCall')=="false":
        del data['batteryLevel']
        data['open_referrer']="android-app://"+campaign_data.get("package_name")            

    kefdata = get_kef_data(app_data, data)
    data[kefdata[0]] = kefdata[1]

    if Appsflyer_data.get('cdn'):
        data['meta'] = conversion_meta(app_data)



    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':af_cipher(campaign_data.get('appsflyer').get('spiKey'), json.dumps(data,separators = (',',':')))}



# ==================== Appsflyer Launches ====================
def appsflyer_launches( app_data, device_data ):

    inc_(app_data,'counter')
    def_(app_data,'iaecounter')
    set_batteryLevel(app_data)

    if not app_data.get('last_launch'):
        timeSinceLastCall = "-1"
    else:
        timeSinceLastCall = int(time.time())-app_data.get('last_launch')

    app_data['last_launch'] = int(time.time())

    method = "post"

    url = 'https://'+Appsflyer_data.get('la_url')+'/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'

    headers = {
        "Accept-Encoding"  : "gzip",
        "Content-Type"     : "application/octet-stream",
        "User-Agent"       : get_ua(device_data)
    }

    params = {
        "app_id"        : campaign_data.get('package_name'),
        "buildnumber"   : campaign_data.get('appsflyer').get('buildnumber'),
    } 
    if not app_data.get('disk'):
        app_data['disk'] = "{available}/{total}".format(available =str(random.randint(8000, 16000)), total = str(random.randint(23000, 32000)))
    else:
        temp_disk = map(int, app_data.get('disk').split('/'))
        temp_disk[0] += random.randint(-150, 300)

        app_data['disk'] = "{available}/{total}".format(available = str(temp_disk[0]), total = str(temp_disk[1]))


    data = {
        "advertiserId"          : device_data.get('adid'), 
        "advertiserIdEnabled"   : "true", 
        "af_events_api"         : "1", 
        "af_preinstalled"       : "false", 
        "af_timestamp"          : str(int(time.time()*1000)), 
        "app_version_code"      : campaign_data.get('app_version_code'),
        "app_version_name"      : campaign_data.get('app_version_name'),
        "appsflyerKey"          : campaign_data.get('appsflyer').get('key'),
        "brand"                 : device_data.get('brand'), 
        "carrier"               : device_data.get('carrier'), 
        "cell"                  :
         {
            "mcc"               : int(device_data.get('mcc')), 
            "mnc"               : int(device_data.get('mnc'))
        },  
        "counter"               : str(app_data.get('counter')), 
        "country"               : device_data.get('locale').get('country'), 
        "date1"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "date2"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "device"                : device_data.get('device'), 
        "deviceData"            :
         {
            "arch"              : "", 
            "btch"              : app_data.get('btch'), 
            "btl"               : app_data.get('btl'), 
            "build_display_id"  : device_data.get('build'), 
            "cpu_abi"           : device_data.get('cpu_abi'), 
            "cpu_abi2"          : device_data.get('cpu_abi2'), 
            "dim"               :
             {
                "d_dpi"         : device_data.get('dpi'), 
                "size"          : app_data.get('dim_size'), 
                "x_px"          : device_data.get('resolution').split('x')[1],
                "xdp"           : app_data.get('xdp'), 
                "y_px"          : device_data.get('resolution').split('x')[0], 
                "ydp"           : app_data.get('ydp')
            },
            "sensors":  [{
                "sN": "ICM20607 Accelerometer",
                "sT": 1,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,1),10),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(8,10),6)
                ],
                "sVS": [
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(8,10),6)
                ]
            },
            {
                "sN": "ICM20607 Gyroscope",
                "sT": 4,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,-1),10),
                    round(random.uniform(0,-1),10),
                    round(random.uniform(0,1),10)
                ],
                "sVS": [
                    round(random.uniform(0,1),10),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,1),10)
                ]
            },
            {
                "sN": "ICM20607 Gyroscope -Wakeup Secondary",
                "sT": 4,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,1),11),
                    round(random.uniform(0,-1),11),
                    round(random.uniform(0,1),10)
                ],
                "sVS": [
                    round(random.uniform(0,1),11),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,1),10)
                ]
            },
            {
                "sN": "AK09918 Magnetometer -Wakeup Secondary",
                "sT": 2,
                "sV": "AKM",
                "sVE": [
                    round(random.uniform(-13,-11),6),
                    round(random.uniform(16,19),6),
                    round(random.uniform(-45,-42),5)
                ],
                "sVS": [
                    round(random.uniform(-13,-11),6),
                    round(random.uniform(16,19),6),
                    round(random.uniform(-45,-42),5)
                ]
            },
            {
                "sN": "ICM20607 Accelerometer -Wakeup Secondary",
                "sT": 1,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,1),11),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(8,10),6)
                ],
                "sVS": [
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,-1),8),
                    round(random.uniform(8,10),6)
                ]
            },
            {
                "sN": "AK09918 Magnetometer",
                "sT": 2,
                "sV": "AKM",
                "sVE": [
                    round(random.uniform(-14,-12),6),
                    round(random.uniform(18,21),6),
                    round(random.uniform(-45,-42),6)
                ],
                "sVS": [
                    round(random.uniform(-14,-12),6),
                    round(random.uniform(18,21),6),
                    round(random.uniform(-45,-42),6)
                ]
            }
        ]
        },
        "deviceType"        : "user", 
        "disk"              : app_data.get('disk'),
        "exception_number"  : 0,
        "iaecounter"        : str(app_data.get('iaecounter')),
        "installDate"       : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000', 
        "installer_package" : "com.android.vending", 
        "isFirstCall"       : "false" if app_data.get('counter') > 1 else "true", 
        "isGaidWithGps"     : "true", 
        "ivc"               : False, 
        "lang"              : "English", 
        "lang_code"         : "en", 
        "last_boot_time"    : app_data.get('last_boot_time'),
        "meta": {
            "first_launch": {
            "from_fg": 960, 
            "init_to_fg": 9, 
            "net": 12605
            }
        }, 
        "model"             : device_data.get('model'), 
        "network"           : device_data.get('network').upper(), 
        "open_referrer"     : "android-app://"+campaign_data.get("package_name"), 
        "operator"          : device_data.get('carrier'), 
        "p_receipt"         :
         {
            "an": {
                "dbg"       : False, 
                "hk"        : ";"
            }, 
            "as"            :
                {
                "cav": random.randint(5,40),
                "null": random.randint(5,35),
                "other": random.randint(5,15)
            },
            "pr":{
                "ac": "0",
                "ah": "zygote64_32",
                "ai": "0",
                "ak": "default",
                "al": "cortex-a53",
                "am": "default",
                "an": "generic",
                "ap": "builder",
                "ar": "qcom",
                "as": device_data.get('cpu_abi'),
                "at": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "au": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "av": device_data.get('cpu_abi')
            }
        }, 
        "platformextension"    : campaign_data.get("platformextension"), 
        "product"              : device_data.get('product'), 
        "referrers": [
        {
            "api_ver": campaign_data.get('api_ver'), 
            "api_ver_name": campaign_data.get('api_ver_name'), 
            "click_ts": int(app_data.get('times').get('click_time')),  
            "google_custom": { 
                "click_server_ts": int(app_data.get('times').get('click_time'))-1, 
                "install_begin_server_ts": int(app_data.get('times').get('download_begin_time'))-2, 
                "install_version": campaign_data.get('app_version_name'), 
                "instant": False
                },
            "install_begin_ts": int(app_data.get('times').get('download_begin_time')), 
            "latency": 92,
            "referrer":app_data.get("referrer", "utm_source=(not%20set)&utm_medium=(not%20set)"), 
            "response": "OK", 
            "source": "google", 
            "type": "store"
        }
        ], 
        "registeredUninstall"       : False, 
        "rfr"                       :
         {
            "clk"                   : str(int(app_data.get('times').get('click_time'))), 
            "code"                  : "0", 
            "install"               : str(int(app_data.get('times').get('download_begin_time'))), 
            "instant"               : False,
            "val"                   : app_data.get("referrer", "utm_source=(not%20set)&utm_medium=(not%20set)")
        }, 
        "sc_o"                      : "l", 
        "sdk"                       : device_data.get('sdk'), 
        # "sig"                       : campaign_data.get("sig"), 
        "timepassedsincelastlaunch" : str(timeSinceLastCall), 
        "tokenRefreshConfigured"    : False, 
    }

    if campaign_data.get('sig'):
        data["sig"]  = campaign_data.get("sig")

    if conversion_data.get('android_id'):
        data['android_id'] = device_data.get('android_id')
    if conversion_data.get('appUserId'):
        data['appUserId'] = app_data.get('appUserId')
    if conversion_data.get('customData'):
        data['customData'] = app_data.get('customData')
    if conversion_data.get('fb'): 
        data['fb'] =  app_data.get('fb')
    if conversion_data.get('user_emails'): 
        data['user_emails'] = app_data.get('user_emails')
    if conversion_data.get('sdkExtension'): 
        data['sdkExtension'] = conversion_data.get('sdkExtension')
    

    data['uid']                = app_data.get('uid')
    data['firstLaunchDate']    = app_data.get('firstLaunchDate')
    data['cksm_v1']            = cksm_v1(data['date1'],data['af_timestamp'])
    data['af_v']               = util.sha1(data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13])
    data['af_v2']              = util.sha1(util.md5(data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')))

    if int(app_data.get('counter')) > 2:
        del data['deviceData']['sensors']
        if data.get("referrers"):
            del data["referrers"]
        if data.get('rfr'):
            del data['rfr']
        if data.get('p_receipt'):
            del data['p_receipt']

    kefdata = get_kef_data(app_data, data)
    data[kefdata[0]] = kefdata[1]

    if app_data.get('session_time'):
        data['prev_session_dur'] = int(time.time()- app_data.get('session_time'))

    app_data['session_time'] = time.time()

    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':af_cipher(campaign_data.get('appsflyer').get('spiKey'), json.dumps(data,separators = (',',':')))}

# ==================== Appsflyer Inapps ====================
def appsflyer_inapps( app_data, device_data, eventname= "", eventvalue= "" ):

    def_(app_data,'counter')
    inc_(app_data,'iaecounter')

    method = "post"

    url = 'https://'+Appsflyer_data.get('in_url')+'/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'

    headers = {
        "Accept-Encoding":"gzip",
        "Content-Type":"application/octet-stream",
        "User-Agent":get_ua(device_data)
    }

    params = {
        "app_id":campaign_data.get('package_name'),
        "buildnumber":campaign_data.get('appsflyer').get('buildnumber'),
    }
    if not app_data.get('disk'):
        app_data['disk'] = "{available}/{total}".format(available =str(random.randint(8000, 16000)), total = str(random.randint(23000, 32000)))
    else:
        temp_disk = map(int, app_data.get('disk').split('/'))
        temp_disk[0] += random.randint(-150, 300)

        app_data['disk'] = "{available}/{total}".format(available = str(temp_disk[0]), total = str(temp_disk[1]))


    data = {
        "advertiserId"          : device_data.get('adid'), 
        "advertiserIdEnabled"   : "true", 
        "af_events_api"         : "1", 
        "af_preinstalled"       : "false", 
        "af_timestamp"          : str(int(time.time()*1000)), 
        "app_version_code"      : campaign_data.get('app_version_code'),
        "app_version_name"      : campaign_data.get('app_version_name'),
        "appsflyerKey"          : campaign_data.get('appsflyer').get('key'),
        "appUserId"             : app_data.get('appUserId'),
        "brand"                 : device_data.get('brand'), 
        "carrier"               : device_data.get('carrier'),
        "cell"                  :
            {
            "mcc"               : int(device_data.get('mcc')), 
            "mnc"               : int(device_data.get('mnc'))
        },  
        "counter"               : str(app_data.get('counter')), 
        "country"               : device_data.get('locale').get('country'), 
        "date1"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "date2"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "device"                : device_data.get('device'), 
        "deviceData"            :
            {
            "arch"              : "", 
            "build_display_id"  : device_data.get('build'), 
            "cpu_abi"           : device_data.get('cpu_abi'), 
            "cpu_abi2"          : device_data.get('cpu_abi2'), 
            "dim"               :
                {
                "d_dpi"         : device_data.get('dpi'), 
                "size"          : app_data.get('dim_size'), 
                "x_px"          : device_data.get('resolution').split('x')[1],
                "xdp"           : app_data.get('xdp'), 
                "y_px"          : device_data.get('resolution').split('x')[0], 
                "ydp"           : app_data.get('ydp')
            }
        },
        "deviceType"            : "user", 
        "disk"                  : app_data.get('disk'),
        "eventName"             : eventname, 
        "eventValue"            : json.dumps(eventvalue,separators = (',',':')), 
        "iaecounter"            : str(app_data.get('iaecounter')), 
        "installDate"           : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000', 
        "installer_package"     : "com.android.vending", 
        "isFirstCall"           : "false" if app_data.get('counter') > 1 else "true", 
        "isGaidWithGps"         : "true", 
        "ivc"                   : False, 
        "lang"                  : "English", 
        "lang_code"             : "en", 
        "last_boot_time"        : app_data.get('last_boot_time'),
        "model"                 : device_data.get('model'), 
        "network"               : device_data.get('network').upper(), 
        "operator"              : device_data.get('carrier'), 
        "p_receipt"             :
            {
            "an": {
                "dbg"           : False, 
                "hk"            : ";"
            }, 
            "as"            :
                {
                    "cav": random.randint(5,40),
                    "null": random.randint(5,35),
                    "other": random.randint(5,15)
                },
            "pr":{
                "ac": "0",
                "ah": "zygote64_32",
                "ai": "0",
                "ak": "default",
                "al": "cortex-a53",
                "am": "default",
                "an": "generic",
                "ap": "builder",
                "ar": "qcom",
                "as": device_data.get('cpu_abi'),
                "at": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "au": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "av": device_data.get('cpu_abi')
            }
        }, 
        "platformextension"     : campaign_data.get("platformextension"), 
        "prev_event"            : "",
        "product"               : device_data.get('product'),
        "registeredUninstall"   : False, 
        # "sdkExtension"          : "unity_android_6.1.4",
        "sc_o"                  : "l", 
        "sdk"                   : device_data.get('sdk'), 
        "sensors"               :
            {
            "am": {
                "n": "ICM20607 Accelerometer -Wakeup Secondary",
                "v": [
                    [
                        0,
                        0,
                        9.6
                    ],
                    [
                        0,
                        0,
                        0
                    ]
                ]
            },
            "er": "no_svs",
            "gs": {
                "n": "ICM20607 Gyroscope -Wakeup Secondary",
                "v": [
                    [
                        0,
                        0,
                        0
                    ],
                    [
                        0,
                        0,
                        0
                    ]
                ]
            },
            "mm": {
                "n": "AK09918 Magnetometer",
                "v": [
                    [
                        -7,
                        -389.3
                    ],
                    []
                ]
            }
        },
        # "sig"                    : campaign_data.get("sig"), 
        "tokenRefreshConfigured" : False, 
    }

    if campaign_data.get('sig'):
        data["sig"]  = campaign_data.get("sig")

    if inapp_data.get('android_id'):
        data['android_id'] = device_data.get('android_id')
    if inapp_data.get('appUserId'):
        data['appUserId'] = app_data.get('appUserId')
    if inapp_data.get('customData'):
        data['customData'] = app_data.get('customData')
    if inapp_data.get('fb'): 
        data['fb'] =  app_data.get('fb')
    if inapp_data.get('user_emails'): 
        data['user_emails'] = app_data.get('user_emails')
    if inapp_data.get('sdkExtension'): 
        data['sdkExtension'] = inapp_data.get('sdkExtension')
    

    data['uid']             = app_data.get('uid')
    data['firstLaunchDate'] = app_data.get('firstLaunchDate')
    data['cksm_v1']         = cksm_v1(data['date1'],data['af_timestamp'])
    data['af_v']            = util.sha1(data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13])
    data['af_v2']           = util.sha1(util.md5(data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')))


    if not app_data.get('prev_event'):
        del data['prev_event']
    else:
        data['prev_event'] = json.dumps(app_data.get('prev_event'),separators = (',',':'))

    if app_data.get('counter') > 2:
        if data.get('p_receipt'):
            del data['p_receipt']



    # if app_data.get('iaecounter')==1:
    #     data["sensors"]= {"er": "na"}







    update_event_records( app_data, data.get('eventName'), data.get('eventValue'), data.get('af_timestamp'))

    kefdata = get_kef_data(app_data, data)
    data[kefdata[0]] = kefdata[1]

    # if app_data.get('session_time'):
    #     data['prev_session_dur'] = int(time.time()- app_data.get('session_time'))


    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':af_cipher(campaign_data.get('appsflyer').get('spiKey'), json.dumps(data,separators = (',',':')))}

# ==================== Appsflyer Register ====================
def appsflyer_register( app_data, device_data ):

    method = 'post'

    url = 'https://'+Appsflyer_data.get('res_url')+'/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'

    headers = {
        'Accept-Encoding'   : 'gzip',
        'Content-Type'      : 'application/json',
        'User-Agent'        : get_ua(device_data)
    }

    params = {
        'app_id'        : campaign_data.get('package_name'),
        'buildnumber'   : campaign_data.get('appsflyer').get('buildnumber')
    }

    data = {
        'advertiserId'     : device_data.get('adid'),
        'af_gcm_token'     : app_data.get('af_gcm_token'),
        'app_name'         : campaign_data.get('app_name'),
        'app_version_code' : campaign_data.get('app_version_code'),
        'app_version_name' : campaign_data.get('app_version_name'),
        'brand'            : device_data.get('brand'),
        'carrier'          : device_data.get('carrier'),
        'devkey'           : campaign_data.get('appsflyer').get('key'),
        'installDate'      : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
        'launch_counter'   : str(app_data.get('counter')),
        'model'            : device_data.get('model'),
        'network'          : device_data.get('network').upper(),
        'operator'         : device_data.get('carrier'),
        'sdk'              : device_data.get('sdk'),

    }
    if register_data.get('kef9999'):
        data['kef9999'] = "babeae0540d91f201848191d1b0c531a0d5918190d5a1b"
    
    if register_data.get('appUserId'):
        data['appUserId'] = app_data.get('appUserId')



    # if not app_data.get('af_gcm_token'):
    #     app_data['af_gcm_token'] = util.get_random_string(type='all', size=22)+ ':APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

    data['uid'] = app_data.get('uid')
    data['af_gcm_token'] = app_data.get('af_gcm_token')

    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data,separators = (',',':'))}

# ==================== Appsflyer GCDSDK ====================
def appsflyer_gcdsdk( app_data, device_data ):

    method = "get"

    url = 'https://'+Appsflyer_data.get('gcd_url')+'/install_data/'+Appsflyer_data.get('gcd_ver')+'/'+campaign_data.get('package_name')

    headers = {
        "Accept-Encoding" : "gzip",
        "User-Agent" : get_ua(device_data)
    }
    
    params = {
        "device_id" : app_data.get('uid'),
        "devkey" : campaign_data.get('appsflyer').get('key')
    }


    params['device_id'] = app_data.get('uid')

    if Appsflyer_data.get('gcd_ver') == "v5.0":
        af_request_epoch_ms = str(int(time.time()*1000))
        headers['af_request_epoch_ms'] = af_request_epoch_ms
        headers['af_sig'] = get_gcd_sdk_af_sig(package_name=campaign_data.get('package_name'),af_key=campaign_data.get('appsflyer').get('key'),uid=app_data.get('uid'),af_request_epoch_ms=af_request_epoch_ms)
        del params['devkey']
    data = {}

    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}





def attr_appsflyer( app_data, device_data ):
    
    inc_(app_data,'counter')
    def_(app_data,'iaecounter')
    set_batteryLevel(app_data)

    if not app_data.get('last_launch'):
        timeSinceLastCall = "-1"
    else:
        timeSinceLastCall = int(time.time())-app_data.get('last_launch')

    app_data['last_launch'] = int(time.time())

    method = "post"

    url = 'https://'+Appsflyer_data.get('att_url')+'/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'

    headers = {
        "Accept-Encoding"  : "gzip",
        "Content-Type"     : "application/octet-stream",
        "User-Agent"       : get_ua(device_data)
    }

    params = {
        "app_id"        : campaign_data.get('package_name'),
        "buildnumber"   : campaign_data.get('appsflyer').get('buildnumber'),
    } 
    if not app_data.get('disk'):
        app_data['disk'] = "{available}/{total}".format(available =str(random.randint(8000, 16000)), total = str(random.randint(23000, 32000)))
    else:
        temp_disk = map(int, app_data.get('disk').split('/'))
        temp_disk[0] += random.randint(-150, 300)

        app_data['disk'] = "{available}/{total}".format(available = str(temp_disk[0]), total = str(temp_disk[1]))


    data = {
        "advertiserId"          : device_data.get('adid'), 
        "advertiserIdEnabled"   : "true", 
        "af_events_api"         : "1", 
        "af_preinstalled"       : "false", 
        "af_timestamp"          : str(int(time.time()*1000)),
        "app_version_code"      : campaign_data.get('app_version_code'),
        "app_version_name"      : campaign_data.get('app_version_name'),
        "appsflyerKey"          : campaign_data.get('appsflyer').get('key'),
        "brand"                 : device_data.get('brand'), 
        "carrier"               : device_data.get('carrier'), 
        "cell"                  :
            {
                "mcc"           : int(device_data.get('mcc')), 
                "mnc"           : int(device_data.get('mnc'))
            }, 
        "counter"               : str(app_data.get('counter')), 
        "country"               : device_data.get('locale').get('country'), 
        "date1"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "date2"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "device"                : device_data.get('device'), 
        "deviceData"            :
         {
            "arch"              : "", 
            "btch"              : app_data.get('btch'), 
            "btl"               : app_data.get('btl'), 
            "build_display_id"  : device_data.get('build'), 
            "cpu_abi"           : device_data.get('cpu_abi'), 
            "cpu_abi2"          : device_data.get('cpu_abi2'), 
            "dim"               :
             {
                "d_dpi"         : device_data.get('dpi'), 
                "size"          : app_data.get('dim_size'), 
                "x_px"          : device_data.get('resolution').split('x')[1],
                "xdp"           : app_data.get('xdp'), 
                "y_px"          : device_data.get('resolution').split('x')[0], 
                "ydp"           : app_data.get('ydp')
            },
            "sensors":  [{
                "sN": "ICM20607 Accelerometer",
                "sT": 1,
                "sV": "InvenSense"
            },
            {
                "sN": "ICM20607 Gyroscope",
                "sT": 4,
                "sV": "InvenSense"
            },
            {
                "sN": "ICM20607 Gyroscope -Wakeup Secondary",
                "sT": 4,
                "sV": "InvenSense"
            },
            {
                "sN": "AK09918 Magnetometer -Wakeup Secondary",
                "sT": 2,
                "sV": "AKM"
            },
            {
                "sN": "ICM20607 Accelerometer -Wakeup Secondary",
                "sT": 1,
                "sV": "InvenSense"
            },
            {
                "sN": "AK09918 Magnetometer",
                "sT": 2,
                "sV": "AKM",
                "sVS": [
                    201.45416,
                    60.188293,
                    -428.685
                ]
            }
        ]
        },
        "deviceType"        : "user", 
        "disk"              : app_data.get('disk'),
        "exception_number"  : 0,
        "iaecounter"        : str(app_data.get('iaecounter')),
        "installDate"       : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000', 
        "installer_package" : "com.android.vending", 
        "isFirstCall"       : "false" if app_data.get('counter') > 1 else "true", 
        "isGaidWithGps"     : "true", 
        "ivc"               : False, 
        "lang"              : "English", 
        "lang_code"         : "en", 
        "last_boot_time"    : app_data.get('last_boot_time'),
        "meta"              : {},
        "model"             : device_data.get('model'), 
        "network"           : device_data.get('network').upper(), 
        "open_referrer"     : "android-app://"+campaign_data.get("package_name"), 
        "operator"          : device_data.get('carrier'), 
        "p_receipt"         :
         {
            "an": {
                "dbg"       : False, 
                "hk"        : ";"
            }, 
            "as": {
                "cav": 1,
                "null": 270,
                "other": 43
            },
            "pr": {
                "ac": "0",
                "ah": "zygote64_32",
                "ai": "0",
                "ak": "default",
                "al": "cortex-a53",
                "am": "default",
                "an": "generic",
                "ap": "builder",
                "ar": "qcom",
                "as": device_data.get('cpu_abi'),
                "at": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "au": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "av": device_data.get('cpu_abi')
            }
        }, 
        "platformextension"         : campaign_data.get("platformextension"), 
        "product"                   : device_data.get('product'),
        "referrers": [
        {
            "api_ver"       : campaign_data.get('api_ver'), 
            "api_ver_name"  : campaign_data.get('api_ver_name'), 
            "click_ts"      : int(app_data.get('times').get('click_time')),  
            "google_custom" :
                {
                "click_server_ts"           : int(app_data.get('times').get('click_time'))-1, 
                "install_begin_server_ts"   : int(app_data.get('times').get('download_begin_time'))-2, 
                "install_version"           : campaign_data.get('app_version_name'), 
                "instant"                   : False
                },
            "install_begin_ts"  : int(app_data.get('times').get('download_begin_time')), 
            "latency"           : 92,
            "referrer"          : app_data.get("referrer", "utm_source=(not%20set)&utm_medium=(not%20set)"), 
            "response"          : "OK", 
            "source"            : "google", 
            "type"              : "store"
        }
        ],
        "registeredUninstall"       : False, 
        "rfr"                       :
         {
            "clk"                   : str(int(app_data.get('times').get('click_time'))), 
            "code"                  : "0", 
            "install"               : str(int(app_data.get('times').get('download_begin_time'))), 
            "instant"               : False,
            "val"                   : app_data.get("referrer", "utm_source=(not%20set)&utm_medium=(not%20set)")
        }, 
        "sc_o"                      : "l", 
        "sdk"                       : device_data.get('sdk'), 
        "sig"                       : campaign_data.get("sig"), 
        "timepassedsincelastlaunch" : "0", 
        "tokenRefreshConfigured"    : False, 
    }



    data['uid']                = app_data.get('uid')
    data['firstLaunchDate']    = app_data.get('firstLaunchDate')
    data['cksm_v1']            = cksm_v1(data['date1'],data['af_timestamp'])
    data['af_v']               = util.sha1(data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13])
    data['af_v2']              = util.sha1(util.md5(data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')))

    if int(app_data.get('counter')) > 2:
        del data['deviceData']['sensors']
        if data.get("referrers"):
            del data["referrers"]
        if data.get('rfr'):
            del data['rfr']
        if data.get('p_receipt'):
            del data['p_receipt']

    kefdata = get_kef_data(app_data, data)
    data[kefdata[0]] = kefdata[1]

    if app_data.get('session_time'):
        data['prev_session_dur'] = int(time.time()- app_data.get('session_time'))

    app_data['session_time'] = time.time()

    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':af_cipher(campaign_data.get('appsflyer').get('spiKey'), json.dumps(data,separators = (',',':')))}



def appsflyer_dlsdk(app_data,device_data):

    url = "https://"+Appsflyer_data.get('dlsdk_url')+"/v1.0/android/"+campaign_data.get('package_name')
    method = "post"
    headers = {
    "Content-Type":"application/json",
    "User-Agent":get_ua(device_data),
    "Accept-Encoding":"gzip",
    }

    params = {
        "af_sig":'06aa74825006f9a1874fc637751d66e033954f764a1d303b50791bc5514525fa',
        'sdk_version': campaign_data.get('appsflyer').get('version').replace('v','')
    }

    data_dict = {
                "os":device_data.get('os_version'),
                "gaid": {
                            "type":"unhashed",
                            "value":str(uuid.uuid4())
                        },
                "is_first":True,
                "lang":"en-IN",
                "type":device_data.get('model'),
                "request_id":app_data.get('uid'),
                "timestamp":app_data.get('dlsdk_timestamp'),
                "request_count":1
            }
    if app_data.get('referrer'):
        data_dict["referrers"] = [{
                        "source": "google",
                        "value": app_data.get('referrer')
                    }]
    params['af_sig'] = get_dlsdk_af_sig(package_name=campaign_data.get('package_name'),af_key=campaign_data.get('appsflyer').get('key'),data_dict=data_dict)
    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':json.dumps(data_dict)}


def appsflyer_cdn( app_data, device_data ):
    if Appsflyer_data.get('cdn_version') != "v1":
        print("change cdn version")
        exit()

    method = 'get'
    app_data['cdn_token']=get_cdn_token(campaign_data.get('package_name'),campaign_data.get('appsflyer').get('key'))
    url = 'https://'+Appsflyer_data.get('cdn_url')+'/android/v1/'+str(app_data.get('cdn_token'))+'/settings'

    headers = {
    "Content-Type": "application/json",
    'User-Agent':get_ua(device_data),
    'Accept-Encoding': "gzip"
    }


    params ={}

    data = {}


    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


def firebase_installations( app_data, device_data ):
	method = "post"
	url = 'https://firebaseinstallations.googleapis.com'+firebase_data.get('url')

	headers = {
		'Content-Type':"application/json",
		'Accept':"application/json",
		'Cache-Control':"no-cache",
		'X-Android-Package':campaign_data.get('package_name'),
		'x-firebase-client':firebase_data.get('x-firebase-client'),
		'X-Android-Cert':firebase_data.get('X-Android-Cert'),
		'x-goog-api-key':firebase_data.get('x-goog-api-key'),
		'User-Agent':get_ua(device_data),
		'Connection':"Keep-Alive",
		'Accept-Encoding':"gzip"
	}

	data ={
		"fid": app_data.get('fid'),
		"appId":firebase_data.get('data').get('appId') ,
		"authVersion": firebase_data.get('data').get('authVersion'),
		"sdkVersion": firebase_data.get('data').get('sdkVersion')
	}
	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}







#==============================================================================
def install(app_data, device_data): 

    if inapp_data.get('appUserId') or conversion_data.get('appUserId') or register_data.get('appUserId') : 
        print("Please create appUserId")
        exit()
    if inapp_data.get('customData') or conversion_data.get('customData'):
        print("Please create customdata")
        exit()
    if inapp_data.get('user_emails') or conversion_data.get('user_emails'):
        print("Please create User_email")
        exit()

    if not app_data.get('times'):
        # print "Please wait installing..."
        installtimenew.main(app_data, device_data, app_size=campaign_data.get('app_size'),os='android')
        def_sec(app_data,device_data)
        app_data['uid'] = '{0}-{1}'.format(int(time.time()*1000), random.getrandbits(64))
        set_batteryLevel(app_data)
        set_deviceData(app_data)
        app_data['last_boot_time'] = int(time.time()*1000 - random.randint(20000, 30000))
        app_data['app_start_time'] =time.time()
        app_data['appUserId']= str(uuid.uuid4())
    app_data['xdp']=str(float(device_data.get('dpi'))-round(random.uniform(0, 2),3))
    app_data['ydp']=str(float(device_data.get('dpi'))-round(random.uniform(0, 2),3))
    app_data['dlsdk_timestamp'] = datetime.utcfromtimestamp(int(time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
    app_data['fid']=random.choice(['c','d','e','f'])+''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(21))
    if firebase_data.get('url'):
        num = 5
        while(num >0):
            req = firebase_installations(app_data,device_data)
            res= util.execute_request(**req)
            try:
                tmp = json.loads(res.get('data'))
                app_data['fid2'] = tmp.get('fid')
                break
            except:
                pass
            num -= 1
        if not app_data.get('fid2'):
            raise Exception ("Erorr in Firebase call  for country"+device_data.get('locale').get('country'))
            return {'status':True}


    if Appsflyer_data.get('cdn_url'):
        try:
            req = appsflyer_cdn( app_data, device_data )
            res = util.execute_request(**req)
            tem = json.loads(res.get('dara'))
            app_data['cdn_ver']  = tem.get('ver')
        except:
            pass


    if Appsflyer_data.get('dlsdk_url'):
        req = appsflyer_dlsdk(app_data,device_data)
        util.execute_request(**req)


    if not app_data.get('firstLaunchDate'):

        app_data['firstLaunchDate'] = datetime.utcfromtimestamp(int(time.time())).strftime("%Y-%m-%d_%H%M%S")+"+0000"

    if not app_data.get('af_gcm_token'):
        app_data['af_gcm_token'] = app_data.get('fid2') + ':APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

    if conversion_data.get('fb'): 
        app_data['fb'] = str(uuid.uuid4())


    #print 'conversion'
    req  = appsflyer_conversions( app_data, device_data )
    util.execute_request(**req)

    if Appsflyer_data.get('att_url'):
        req = attr_appsflyer( app_data, device_data )
        util.execute_request(**req)

    if Appsflyer_data.get('register'):
        req = appsflyer_register( app_data, device_data )
        util.execute_request(**req)

    #print 'Gcd_sdk'
    req  = appsflyer_gcdsdk( app_data, device_data )
    util.execute_request(**req)


    if random.randint(1,10)>6:
        #print 'launches'
        req  = appsflyer_launches( app_data, device_data )
        util.execute_request(**req)
        

        
    
    # please comment below 3 line for update

    for (i,j) in zip(Event_name.get('event_name'),event_data):

        req=appsflyer_inapps( app_data, device_data, eventname= i, eventvalue= j.get(i))
        util.execute_request(**req)

    # eve = ''
    # eve_val = {}
    # req=appsflyer_inapps( app_data, device_data, eventname= eve, eventvalue= eve_val)
    # util.execute_request(**req)








    if random.randint(1,10)>6:
        #print 'launches'
        req  = appsflyer_launches( app_data, device_data )
        util.execute_request(**req)
        
   

    return {'status':True}


# ================== Open Begins ======================
def open( app_data, device_data, day=1 ):
    if app_data.get('times'):
        # print 'launches'
        req  = appsflyer_launches( app_data, device_data )
        util.execute_request(**req)








    return {'status':True}





# ================= Supportive Functions ===================



def get_ua(device_data):
    if int(device_data.get("sdk")) >=19:
        return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
    else:
        return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data, paramName):
    if not app_data.get(paramName):
        app_data[paramName] = 0

def inc_(app_data, paramName):
    def_(app_data, paramName)
    app_data[paramName] = int(app_data.get(paramName)) + 1

def update_event_records( app_data, eventName, eventValue, af_timestamp):
    app_data['prev_event'] = {"prev_event_timestamp":af_timestamp, "prev_event_value":eventValue,"prev_event_name":eventName}

def sleep_interval(t1=0, t2=10):
    ''
    time.sleep(random.randint(t1, t2))

def set_batteryLevel(app_data):
    app_data['btch'] = random.choice(["no","ac","usb"])
    if app_data.get('btch') in ["ac", "usb"]:
        if app_data.get('batterylevel') > 10 and app_data.get('batterylevel') < 100:
            app_data['batterylevel'] += random.randint(1, 5)
        else:
            app_data['batterylevel'] = random.randint(10,100)
            app_data['btl']=str(app_data.get('batterylevel'))+".0"
    elif not app_data.get('batterylevel'):
        app_data['batterylevel'] = random.randint(10,100)
        app_data['btl']=str(app_data.get('batterylevel'))+".0"

def set_deviceData(app_data):
    if not app_data.get('deviceData'):
        app_data['dim_size']=str(random.randint(1,2))
        app_data['deviceData'] = True

def get_kef_data(app_data, data):
    if data.get('deviceData').get('sensors'):
        app_data["sensorCount"] = str(len(data.get('deviceData').get('sensors')))
    elif not app_data.get("sensorCount"):
        app_data["sensorCount"] = str(random.randint(3, 10))


    batteryTemp = random.choice(["300", "290", "280", "270"])

    kefKey = get_key_half(data.get('brand'), data.get('sdk'), data["lang"], data["af_timestamp"], buildnumber=campaign_data.get("appsflyer").get("buildnumber"))

    kefVal = generateValue(b=batteryTemp, x="0", s=app_data.get("sensorCount"), p=str(len(data.keys())), ts=data["af_timestamp"], fl=data["firstLaunchDate"], buildnumber=campaign_data.get("appsflyer").get("buildnumber"))

    return "kef" + kefKey, kefVal




def conversion_meta(app_data):
    data_l = {}
    if conversion_data.get('meta').get('first_launch'):
        data_l['first_launch'] = {}
    if conversion_data.get('meta').get('first_launch').get('start_with'):
        data_l['first_launch']['start_with'] = "application"
        
    if conversion_data.get('meta').get('first_launch').get('from_fg'):
        data_l['first_launch']['from_fg'] = random.randint(100,700)
    if conversion_data.get('meta').get('first_launch').get('init_to_fg'):
        data_l['first_launch']['init_to_fg'] = random.randint(100,600)

    fg = random.randint(100,700)
    delay = random.randint(100,700)
    if Appsflyer_data.get('dlsdk'):
        data_l['ddl'] =  {
                "from_fg": fg,
                "net": [
               fg+random.randint(10,20) ,
                0
                ],
                "rfr_wait": random.randint(2,10),
                "status": "NOT_FOUND",
                "timeout_value": random.randint(1,4)*1000
            }
    if Appsflyer_data.get('cdn'):
        data_l["rc"] = {
            "c_ver": app_data.get('cdn_ver'),
            "cdn_cache_status": "HIT",
            "cdn_token": app_data.get('cdn_token'),
            "delay": delay,
            "latency": delay-random.randint(5,50),
            "res_code": 200,
            "sig": "success"
        }

    return data_l




def def_sec(app_data,device_data):  
    if not app_data.get('sec'):
        timez = device_data.get('timezone')
        sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
        if int(timez) < 0:
            sec = (-1) * sec
        app_data['sec'] = sec
        
    return app_data.get('sec')

# ==================== CKSM Genrator ====================
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
        
def md5(data, radix=16):
    import hashlib
    md5_obj = hashlib.md5()
    md5_obj.update(data)
    if radix == 16:
        return md5_obj.hexdigest()
    elif radix == 64:
        return util.base64(md5_obj.digest())

def sha256(data):
    sha_obj = hashlib.sha256()
    sha_obj.update(data)
    return sha_obj.hexdigest()

def cksm_v1( ins_date, ins_time ):
    pk = campaign_data.get("package_name").split('.')
    pk[0], pk[-1] = pk[-1], pk[0]
    pkn='.'.join(pk)
    string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
    # print "========cksm_v1========"
    # print string
    # print "======================="
    sha256_result = sha256(string)  
    md5_result = md5(sha256_result) 
        
    n = ins_time
    sb = md5_result
    n4 = 0

    sb = change_char(sb,17,'f')
    sb = change_char(sb,27,'f')

    for i in range(0,len(str(n))):
        n4 += int(str(n)[i])
        

    insert1 = list('{:02x}'.format(n4))
    sb = change_char(sb,7,insert1[0])
    sb = change_char(sb,8,insert1[1])
            
    j = 0
    n6 = 77
    n3 = 0
    for i in range(0,len(str(sb))):
        n3 += int(str(sb)[i],36)
        
    if n3>100:
        n8 = 90
        n3%=100

    if n3<10:
        n3 = '0'+str(n3)
        
    sb = insert_char(sb,23,str(n3))
    return sb

# ==================== KEF Genrator ====================
global temp, counter
temp = 0
counter = 1

def get_key_half(brand, sdk, lang, af_ts, buildnumber):
    if buildnumber=="4.8.18":
        return getKeyHalf_4_8_18(sdk, lang.decode('utf-8'), af_ts)
    else:
        c = 'u0000'
        obj2 = af_ts[::-1]
        out = calculate(sdk, obj2, brand)
        i = len(out)
        if i > 4:
            i2 = globals()['counter']+65
            globals()['temp'] = i2 % 128
            if (1 if i2 % 2 != 0 else None) != 1:
                out.delete(4, i)
            else:
                out.delete(5, i)
        else:
            while i < 4:
                i3 = globals()['counter'] + 119
                globals()['temp'] = i3 % 128
                if (16 if i3 % 2 != 0 else 93) != 16:
                    i += 1
                    out+='1'
                else:
                    i += 66
                    out+='H'
                i3 = globals()['counter'] + 109
                globals()['temp'] = i3 % 128
                i3 %= 2
        return out.__str__()

def getKeyHalf_4_8_18(sdk, lang, ts):
    ts = ts[::-1]
    appends = [sdk,lang,ts]
    lengths = [len(sdk),len(lang),len(ts)]
    l = sorted(lengths)
    least = l[0]
    out = ""
    for x in range(least):
        numb = None
        for y in range(len(appends)):
            charAt = ord(appends[y][x])
            if numb:
                charAt = int((charAt ^ int(numb)))

            numb = charAt
            
        out+=str(hex(numb))[2:]

    if len(out)>4:
        out = out[:4]
    elif len(out)<4:
        while len(out)<4:
            out+="1"

    return out

def generateValue(b,x,s,p,ts,fl,buildnumber):
    part1=generateValue1get(ts,fl,buildnumber)
    str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p, "utf-8")
    for i in range(len(str_)):
        str_[i] = int((str_[i] ^ ((i % 2) + 42)))

    stringBuilder = ""
    for toHexString in str_:
        toHexString2 = str(hex(int(toHexString)))[2:]
        if 1 == len(toHexString2):
            toHexString2 = "0"+str(toHexString2)
        stringBuilder+=toHexString2
    part2 = stringBuilder.__str__()
    return part1+part2

def generateValue1get(ts, firstLaunch, buildnumber):
    gethash = sha256(ts+firstLaunch+buildnumber)
    return gethash[:16]

def calculate(sdk, obj, brand):
    allList = [sdk, obj, brand]
    arrayList = []
    i = 0
    while True:
        flag = 1
        if i >= 3:
            break
        i2 = globals()['temp'] + 63
        globals()['counter'] = i2 % 128
        if i2 % 2 != 0:
            flag = None
        if flag != None:
            arrayList.append(len(allList[i]))
            i += 31
        else:
            arrayList.append(len(allList[i]))
            i += 1
    sorted(arrayList)
    intValue = int(arrayList[0])
    out = ""
    i3 = 0
    while i3 < intValue:
        i4 = globals()['counter']+99
        globals()['temp'] = i4 % 128
        i5 =  globals()['temp']+67
        globals()['counter'] = i5 % 128
        i5 %= 2
        number = None
        if i4 % 2 != 0:
            a = 57
        else:
            a = 83
        if a != 83:
            i4 = 1
        else:
            i4 = 0
        while i4 < 3:
            i5 = globals()['temp'] + 87
            globals()['counter'] = i5 % 128
            i5 %= 2
            i5 = ord(allList[i4][i3])
            if (92 if number == None else 39) != 39:
                i6 = globals()['temp'] + 55
                globals()['counter'] = i6 % 128
                i6 %= 2
                i6 = globals()['counter'] + 39
                globals()['temp'] = i6 % 128
                i6 %= 2
            else:
                i5 ^= int(number)
            number = i5
            i4 += 1
        out+=str(hex(int(number)))[2:]
        i3 += 1
    return out

def get_country():
    weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
    country_list     = [val for val, cnt in weighted_choices for i in range(cnt)]
    return random.choice(country_list)

def get_retention(day):
    return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0


