#-*- coding: utf-8 -*-
# created by him@nshu
import sys
if int(sys.version_info.major) == 2:
    py_version = 2
else:
    py_version = 3
    import urllib.parse as urlparse
from sdk import installtimenew, util
import time, random, json, string, urllib, uuid, hashlib
from spicipher import af_cipher,get_dlsdk_af_sig,get_cdn_token,get_gcd_sdk_af_sig
false = False
from datetime import datetimetrue = True
null = None


campaign_data = {
   'package_name' : 'com.betvictor.bettingapp.uk',
   'app_name' : 'BetVictor Casino & Sports Bets',
   'app_version_name' : '5.21.0.25094',
   'app_version_code' : '25094',
   'supported_countries' : 'WW',
   'supported_os' : '11',
