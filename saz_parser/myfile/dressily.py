#-*- coding: utf-8 -*-
# created by him@nshu
import sys
if int(sys.version_info.major) == 2:
    py_version = 2
else:
    py_version = 3
    import urllib.parse as urlparse
from sdk import installtimenew, util
import time, random, json, string, urllib, uuid, hashlib
from spicipher import af_cipher,get_dlsdk_af_sig,get_cdn_token,get_gcd_sdk_af_sig
false = False
from datetime import datetimetrue = True
null = None


campaign_data = {
   'package_name' : 'com.globalegrow.app.dresslily',
   'app_name' : 'DressLily',
   'app_version_name' : '7.3.1',
   'app_version_code' : '137',
   'supported_countries' : 'WW',
   'supported_os' : '10',
   'api_ver_name' : '34.9.16-21 [0] [PR] 518042857',
   'sig' : 'B262ECBAF629B2D7A9958EBB894E60A27E86A251D5036B53C37624D0D7102628',
   'platformextension' : 'android_native',
    'appsflyer' : { 
      'key' : 'PQmjs6dfikqatrWRQ4EEG',
      'buildnumber' : '6.9.0',
      'version' : 'v6.9',
      'spiKey' : '6ffUCx0AICw1tKuLP/V5eesYS6l9y6nVK1W04CLMbtz5d3JVH2vniUBBmJxlJFC0',
       #'spiKey' : 'mzyCdjtcBmC9CHWHHKFZj+WNL2YbszYV+xvyxD4yQidp/EcMN8Bw2QB5TnW/nELq',#devkey
    },
   'link' : '',
    'app_size' : 17,
    'api_ver' : 83491610,
    'tracker' : ['appsflyer'],
    'country' : [('USA', 50), ('India', 3), ('Malaysia', 4), ('Indonesia', 1), ('Thailand', 2), ('Egypt', 1), ('Russia', 6), ('USA', 10), ('SaudiArabia', 1), ('SouthAfrica', 1), ('Israel', 2), ('Kenya', 1), ('Nigeria', 1), ('Pakistan', 2), ('Qatar', 1), ('Brazil', 3), ('Mexico', 4), ('Canada', 5), ('UK', 30), ('HongKong', 5), ('Spain', 4), ('France', 4), ('Australia', 5)],
    'retention' : {1: 85, 2: 82, 3: 80, 4: 78, 5: 75, 6: 72, 7: 70, 8: 68, 9: 65, 10: 60, 11: 55, 12: 50, 13: 45, 14: 40, 15: 35, 16: 33, 17: 30, 18: 28, 19: 26, 20: 20, 21: 19, 22: 18, 23: 17, 24: 16, 25: 15, 26: 14, 27: 13, 28: 12, 29: 11, 30: 10, 31: 9, 32: 8, 33: 7, 34: 6, 35: 5},
    }


Appsflyer_data = {"Key": "PQmjs6dfikqatrWRQ4EEG", "id": "com.globalegrow.app.dresslily", "app_name": "DressLily", "gcd_ver": "v4.0", "cdn_version": "v1", "cdn_url": ".appsflyersdk.com", "cdn": true, "build_number": "6.9.0", "register": true, "con_url": ".appsflyer.com", "att_url": ".appsflyer.com", "res_url": ".appsflyer.com", "in_url": ".appsflyer.com", "la_url": ".appsflyer.com", "gcd_url": ".appsflyer.com"}

register_data = {"kef9999": "babeae0540d91f20184819131b0c531a0d5918190d5a1b", "devkey": "PQmjs6dfikqatrWRQ4EEG", "channel": "googleplay", "operator": "", "network": "WIFI", "advertiserId": "6bc33da9-f93a-49b0-a7e2-715eb311139b", "app_name": "DressLily", "uid": "1679719236975-8783082296212626384", "carrier": "", "app_version_code": "137", "installDate": "2023-03-25_040831+0000", "af_gcm_token": "c7vr2giHSEq3uPxM4rYjIx:APA91bGK5_mf0HBtzhAyvT4uXuRr_m7aTBEdauvlQPxxSsBhUEaSlJQWDFNMP89uE3s4azqvzYDhSUcUI7G0lwHKwH0P-8qv5J-pD0O9_RyPmqGkNs-PMitm2Yl7OZEL7wtJbnMpWSct", "launch_counter": "1", "model": "Redmi Note 5 Pro", "sdk": "28", "app_version_name": "7.3.1", "brand": "xiaomi"}

firebase_data = {"x-goog-api-key": "AIzaSyBYYHOt6C_4-RMKNM4Njgk-I9QoYzoSze4", "x-firebase-client": "H4sIAAAAAAAAAKtWykhNLCpJSk0sKVayio7VUSpLLSrOzM9TslIyUqoFAFyivEQfAAAA", "X-Android-Cert": "B3DCD21171AC2CD62BEDEADA3816098B5B2CCF93", "data": {"fid": "c7vr2giHSEq3uPxM4rYjIx", "appId": "1:780583622417:android:4445ecf7ffba514d", "authVersion": "FIS_v2", "sdkVersion": "a:17.1.1"}, "url": "/v1/projects/lateral-academy-101501/installations"}

Event_name = {"event_name": ["af_enter_app", "af_sign_in", "af_guide_app", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_click", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_click", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_Catentrance_impression", "af_impression", "af_view_homepage", "af_banner_impression", "af_view_homepage", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_impression", "af_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_impression", "af_banner_click", "af_impression", "af_impression", "af_view_product", "af_impression", "af_add_to_bag", "af_bag_click", "af_view_cartpage", "af_checkout_btn_click", "af_view_checkout_page", "af_placeorder_btn_click", "af_create_order_success", "af_process_to_pay"]}

event_data = [{"af_enter_app": {"af_lang": null, "af_country_code": null, "af_user_type": 0, "af_content_type": "enter app"}}, {"af_sign_in": {"af_inner_mediasource": "lauch_signin", "af_lang": "en", "af_country_code": "US", "af_user_type": 0, "af_content_type": "Email"}}, {"af_guide_app": {"af_lang": "en", "af_country_code": "US", "af_user_type": 0, "af_content_type": "creat an accoun"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "WOMEN", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "PLUS SIZE", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "SWIMWEAR", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "DRESSES", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "TOPS", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "BOTTOMS ", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "OUTFITS", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_click": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "MEN", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "ACCESSORIES", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_click": {"af_lang": "en", "af_catentrance_type": 0, "af_cat_level": 1, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 2, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "-2", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "Vitural_Catentrance_impression", "af_cat_name": "NEW"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 2, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "-2", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "Vitural_Catentrance_impression", "af_cat_name": "NEW"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Dress"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Tops"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Bottoms"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Men"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Plus Size"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Outfits"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "galaxy", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": ""}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "galaxy swimwear", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Swimwear"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": -3, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "other_impression", "af_cat_name": "Accessories"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "galaxy dress", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Dress"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "galaxy outfit", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Outfits"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "galaxy plus size", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Plus Size"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "galaxy top", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Tops"}}, {"af_Catentrance_impression": {"af_lang": "en", "af_catentrance_type": 4, "af_cat_level": 2, "af_country_code": "US", "af_user_type": 0, "af_cat_value": "moon accessory", "af_text": "NEW", "af_first_entrance": "category", "af_content_type": "search_impression", "af_cat_name": "Galaxy Acessories"}}, {"af_impression": {"af_inner_mediasource": "recommend_personal", "af_lang": "en", "af_price": "24.99,24.99,19.99,14.99,22.99,16.99,22.99,16.99,16.99,21.99,34.99,29.99,16.99,22.99,34.99,26.99,19.99,22.99,24.99,16.99,26.99,32.99,26.99,23.99,22.99,27.99,26.99,16.99,33.99,24.99,22.99,18.99,33.99,29.99,22.99,39.99,20.99,36.99,20.99,24.99,23.99,16.99,22.99,24.99,16.99,16.99,22.99,34.99,29.99,22.99,19.99,26.99,24.99,26.99,22.99,22.99,19.99,26.99,19.99,14.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "499287201,507361003,499704546,508510415,508399105,499613103,508663204,507579802,507608608,476888602,499763801,469239301,507661815,466931424,503432601,495494806,476710904,508508205,470963601,469738356,498297905,485034701,503921703,496488501,502007903,487194901,503892101,495480610,507321815,498639202,506247328,507427412,477527607,504707401,475490616,505863501,508399405,504392301,466165502,465450528,506021406,326778588,487019807,506690904,496411203,496418504,504606911,498211102,484958705,502949802,448672012,498468809,507581205,505387805,498789902,507707504,499255104,495562322,485012710,503747105"}}, {"af_view_homepage": {"af_lang": "en", "af_channel_id": "416", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_content_type": "view homepage"}}, {"af_banner_impression": {"af_col_id": "6b366297-9872", "af_lang": "en", "af_ad_id": "cb9770bd-2773", "af_channel_id": "416", "af_component_id": "md-xchx1677028823880", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "spring 2", "af_content_type": "banner impression"}}, {"af_view_homepage": {"af_lang": "en", "af_channel_id": "416", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_content_type": "view homepage"}}, {"af_banner_impression": {"af_col_id": "03fadd10-6e71", "af_lang": "en", "af_ad_id": "66cecc51-3d51", "af_channel_id": "416", "af_component_id": "md-37defe76-2c00", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "\u6807\u7b7e", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "0f0fe0e4-c735", "af_lang": "en", "af_ad_id": "73dbad2d-a129", "af_channel_id": "416", "af_component_id": "md-32324602-e671", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "6b366297-9872", "af_lang": "en", "af_ad_id": "c6660b9a-7daa", "af_channel_id": "416", "af_component_id": "md-xchx1677028823880", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "spring ", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "3736afa9-5f78", "af_lang": "en", "af_ad_id": "29472f72-6c74", "af_channel_id": "416", "af_component_id": "md-3fbad2a1-0138", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "PLUS SIZE", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "572407c4-c9dc", "af_lang": "en", "af_ad_id": "b2509a40-8ad2", "af_channel_id": "416", "af_component_id": "md-ee43c6e6-dfd9", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "TAG", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "6b366297-9872", "af_lang": "en", "af_ad_id": "5077a7c1-5d7b", "af_channel_id": "416", "af_component_id": "md-xchx1677028823880", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "swimwear for $16.99", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "30fc2875-4933", "af_lang": "en", "af_ad_id": "1583ff67-f230", "af_channel_id": "416", "af_component_id": "md-3fbad2a1-0138", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "WOMEN", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "4dd2fec0-33d2", "af_lang": "en", "af_ad_id": "21fc90e9-aa82", "af_channel_id": "416", "af_component_id": "md-7d033e38-15b4", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "MEN", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "da3cb2b6-b165", "af_lang": "en", "af_ad_id": "48df86bc-8a89", "af_channel_id": "416", "af_component_id": "md-7d033e38-15b4", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "WOMEN", "af_content_type": "banner impression"}}, {"af_impression": {"af_inner_mediasource": "recommend_personal", "af_lang": "en", "af_price": "24.99,24.99,19.99,14.99,22.99,16.99,22.99,16.99,16.99,21.99,34.99,29.99,16.99,22.99,34.99,26.99,19.99,22.99,24.99,16.99,26.99,32.99,26.99,23.99,22.99,27.99,26.99,16.99,33.99,24.99,22.99,18.99,33.99,29.99,22.99,39.99,20.99,36.99,20.99,24.99,23.99,16.99,22.99,24.99,16.99,16.99,22.99,34.99,29.99,22.99,19.99,26.99,24.99,26.99,22.99,22.99,19.99,26.99,19.99,14.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "499287201,507361003,499704546,508510415,508399105,499613103,508663204,507579802,507608608,476888602,499763801,469239301,507661815,466931424,503432601,495494806,476710904,508508205,470963601,469738356,498297905,485034701,503921703,496488501,502007903,487194901,503892101,495480610,507321815,498639202,506247328,507427412,477527607,504707401,475490616,505863501,508399405,504392301,466165502,465450528,506021406,326778588,487019807,506690904,496411203,496418504,504606911,498211102,484958705,502949802,448672012,498468809,507581205,505387805,498789902,507707504,499255104,495562322,485012710,503747105"}}, {"af_impression": {"af_inner_mediasource": "recommend_personal", "af_lang": "en", "af_price": "24.99,24.99,19.99,14.99,22.99,16.99,22.99,16.99,16.99,21.99,34.99,29.99,16.99,22.99,34.99,26.99,19.99,22.99,24.99,16.99,26.99,32.99,26.99,23.99,22.99,27.99,26.99,16.99,33.99,24.99,22.99,18.99,33.99,29.99,22.99,39.99,20.99,36.99,20.99,24.99,23.99,16.99,22.99,24.99,16.99,16.99,22.99,34.99,29.99,22.99,19.99,26.99,24.99,26.99,22.99,22.99,19.99,26.99,19.99,14.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "499287201,507361003,499704546,508510415,508399105,499613103,508663204,507579802,507608608,476888602,499763801,469239301,507661815,466931424,503432601,495494806,476710904,508508205,470963601,469738356,498297905,485034701,503921703,496488501,502007903,487194901,503892101,495480610,507321815,498639202,506247328,507427412,477527607,504707401,475490616,505863501,508399405,504392301,466165502,465450528,506021406,326778588,487019807,506690904,496411203,496418504,504606911,498211102,484958705,502949802,448672012,498468809,507581205,505387805,498789902,507707504,499255104,495562322,485012710,503747105"}}, {"af_banner_impression": {"af_col_id": null, "af_lang": "en", "af_ad_id": "24", "af_component_id": "14", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "Homepage", "af_banner_name": "Hey New Friend!", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "4af37586-1cd7", "af_lang": "en", "af_ad_id": "3c1a0f72-a0dc", "af_channel_id": "416", "af_component_id": "md-byuu1675165213567", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "ACCESSORIES", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "62fdf365-1d8c", "af_lang": "en", "af_ad_id": "e4b8f195-8b42", "af_channel_id": "416", "af_component_id": "md-byuu1675165213567", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "OUTFITS", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "8c16bc3e-0567", "af_lang": "en", "af_ad_id": "5c7712fe-4b05", "af_channel_id": "416", "af_component_id": "md-byuu1675165213567", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "SWIMWEAR", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "804cc241-77e0", "af_lang": "en", "af_ad_id": "402982d4-b558", "af_channel_id": "416", "af_component_id": "md-byuu1675165213567", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "DRESS", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "750ed021-1d30", "af_lang": "en", "af_ad_id": "148a4d59-54d2", "af_channel_id": "416", "af_component_id": "md-qhdw1675165723816", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "ALL", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "498681801", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Moon Sun Cinched Tie Tank Top and Lace Up Leggings Outfit", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "507811304", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Space Dye Plaid Print Panel Faux Twinset Dress Belted High Waisted A Line Mini Twofer Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "507994305", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Contrast Colorblock Faux Twinset T Shirt Space Dye Mock Button Casual Twofer Tee", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "507227105", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Space Dye Cami Dress Bowknot O Ring Surplice Spaghetti Strap High Low Midi Dress", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "477638605", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Cold Shoulder Ruffle Detail Heathered T-shirt", "af_content_type": "banner impression"}}, {"af_banner_impression": {"af_col_id": "b3c42a66-f460", "af_lang": "en", "af_ad_id": "498667901", "af_channel_id": "416", "af_component_id": "md-cec57a51-db07", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "Cinched Cross Contrast Faux Twinset Tee And Lace-up Capri Pants", "af_content_type": "banner impression"}}, {"af_banner_click": {"af_col_id": "da3cb2b6-b165", "af_lang": "en", "af_ad_id": "48df86bc-8a89", "af_channel_id": "416", "af_component_id": "md-7d033e38-15b4", "af_country_code": "US", "af_user_type": 1, "af_channel_name": "ALL", "af_banner_name": "WOMEN", "af_content_type": "banner impression"}}, {"af_impression": {"af_inner_mediasource": "category_1", "af_lang": "en", "af_sort": "Hot", "af_price": "24.99,19.99,14.99,24.99,22.99,22.99,16.99,16.99,16.99,29.99,34.99,16.99,22.99,22.99,21.99,19.99,26.99,26.99,16.99,26.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "499287201,499704544,508510415,507361002,508663204,508399105,507608610,507579803,499613103,469239301,499763801,507661815,508508204,466931425,476888604,476710903,503921703,498297905,496411201,495494806"}}, {"af_impression": {"af_inner_mediasource": "recommend_buytogether", "af_lang": "en", "af_price": "19.99,20.99,12.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "448672009,508373906,508420201"}}, {"af_view_product": {"af_inner_mediasource": "category_1", "af_sort": "Hot", "af_price": "24.99", "af_changed_size_or_color": 0, "af_content_id": "499287201", "af_content_type": "product", "af_currency": "USD", "af_lang": "en", "af_promote_discount": "-50%", "af_country_code": "US", "af_user_type": 1, "af_rank": 0, "af_price_type": "sec_price", "af_promote_type": "Buy 2 Get 15% Off  | Buy 3 Get 25% Off "}}, {"af_impression": {"af_inner_mediasource": "recommend_productdetail", "af_lang": "en", "af_price": "39.99,29.99,22.99,24.99,20.99,18.99,29.99,24.99,29.99,29.99,24.99,22.99,32.99,27.99,22.99,19.99,26.99,23.99,32.99,16.99,24.99,18.99,19.99,34.99,26.99,24.99,24.99,19.99,33.99,22.99,28.99,24.99,18.99,19.99,24.99,32.99,26.99,14.99,32.99,32.99,28.99,22.99,32.99,32.99,26.99,24.99,32.99,16.99,18.99,26.99,20.99,24.99,22.99,22.99,22.99,24.99,32.99,22.99,19.99,24.99", "af_country_code": "US", "af_user_type": 1, "af_content_id": "455245613,469239304,470490104,470963601,466165502,475896607,475666005,485399502,484958705,486990504,486990904,487019807,487027303,487194901,466931424,487028307,495494806,496488501,498338809,496411201,477552306,487027002,497464401,498211102,498297905,499355704,498639202,499255104,477527607,502968604,499673004,499373505,499748204,504226303,504187804,505604209,503145804,503747105,505509005,485034701,503973502,505387604,504696605,506075307,484108511,507361002,506526504,507579802,507427412,503921703,508399405,506690903,508508204,508399105,508663204,499287227,508456304,508676702,507587208,508510103"}}, {"af_add_to_bag": {"af_currency": "USD", "af_inner_mediasource": "category_1", "af_lang": "en", "af_content_category": "Women/Dresses/Two Piece Dresses", "af_price": "24.99", "af_changed_size_or_color": 0, "af_country_code": "US", "af_user_type": 1, "af_content_id": "499287201", "af_quantity": 1, "af_content_type": "product"}}, {"af_bag_click": {"af_lang": "en", "af_country_code": "US", "af_user_type": 1, "af_page_name": "goods_page"}}, {"af_view_cartpage": {"af_lang": "en", "af_country_code": "US", "af_user_type": 1, "af_content_id": "499287201,", "af_content_type": "view cartpage"}}, {"af_checkout_btn_click": {"af_lang": "en", "af_country_code": "US", "af_user_type": 1, "af_content_id": "499287201,", "af_content_type": "checkout_btn"}}, {"af_view_checkout_page": {"af_lang": "en", "af_country_code": "US", "af_user_type": 1, "af_content_id": "499287201", "af_content_type": "view checkoutpage"}}, {"af_placeorder_btn_click": {"af_lang": "en", "af_country_code": "US", "af_user_type": 1, "af_content_id": "499287201", "af_content_type": "checkout_btn"}}, {"af_create_order_success": {"af_lang": "en", "af_price": "24.99", "af_reciept_id": "12857395", "af_country_code": "US", "af_user_type": 1, "af_content_id": "499287201", "af_quantity": "1", "af_content_type": "product", "af_payment": "PAY_ONLINE"}}, {"af_process_to_pay": {"af_lang": "en", "af_country_code": "US", "af_user_type": 1}}]

conversion_data = {"advertiserId": "6bc33da9-f93a-49b0-a7e2-715eb311139b", "advertiserIdEnabled": "true", "af_events_api": "1", "af_preinstalled": "false", "af_timestamp": "1679719237073", "af_v": "3ba64828dbe6f8c318169a987a75f3ae81227d5a", "af_v2": "ab7a7ec6c9e64cb1a7cc4a2a5381d196adfa62b1", "app_version_code": "137", "app_version_name": "7.3.1", "appsflyerKey": "PQmjs6dfikqatrWRQ4EEG", "batteryLevel": "17.0", "brand": "xiaomi", "carrier": "", "cell": {"mcc": 0, "mnc": 0}, "cksm_v1": "08210693e17883787ff5f1006ab78f1731", "counter": "1", "country": "IN", "date1": "2023-03-25_093831+0530", "date2": "2023-03-25_093831+0530", "device": "whyred", "deviceData": {"arch": "", "btch": "no", "btl": "17.0", "build_display_id": "PKQ1.180904.001", "cpu_abi": "arm64-v8a", "cpu_abi2": "", "dim": {"d_dpi": "440", "size": "2", "x_px": "1080", "xdp": "403.411", "y_px": "2030", "ydp": "403.411"}, "sensors": [{"sN": "ICM20607 Accelerometer", "sT": 1, "sV": "InvenSense", "sVE": [-4.205078, 3.9065552, 7.8418274], "sVS": [-4.147629, 3.913742, 7.9256287]}, {"sN": "ICM20607 Gyroscope", "sT": 4, "sV": "InvenSense", "sVE": [-0.016433716, -0.007888794, -0.005432129], "sVS": [-0.04626465, -0.028137207, -0.025665283]}, {"sN": "AK09918 Magnetometer", "sT": 2, "sV": "AKM", "sVE": [193.43414, -57.141113, -333.97217], "sVS": [194.50378, -57.67212, -334.4574]}]}, "deviceType": "user", "disk": "40436/51517", "exception_number": 0, "firstLaunchDate": "2023-03-25_044037+0000", "iaecounter": "1", "installDate": "2023-03-25_040831+0000", "installer_package": "com.android.vending", "isFirstCall": "true", "isGaidWithGps": "true", "ivc": false, "kef7966": "62a4c1066d87c34e4819131b0c531a0d5918190d5a1e18", "lang": "English", "lang_code": "en", "last_boot_time": 1679639038999, "meta": {"first_launch": {"from_fg": 1221, "init_to_fg": 275, "start_with": "application"}, "rc": {"c_ver": "default.v1.1637149529", "cdn_token": "ff4064ba14e26f38c0e40a7e1aada070efa5b2f52b407233b23ca3736178fdf1", "delay": 564, "latency": 555, "res_code": 200, "sig": "success"}}, "model": "Redmi Note 5 Pro", "network": "WIFI", "onelink_id": "t6H2", "open_referrer": "android-app://com.android.vending", "operator": "", "p_receipt": {"an": {"dbg": false, "hk": ";"}, "pr": {"ac": "0", "ah": "zygote64_32", "ai": "0", "ak": "default", "al": "cortex-a73", "am": "default", "an": "generic", "ap": "builder", "ar": "qcom", "as": "arm64-v8a", "at": "arm64-v8a,armeabi-v7a,armeabi", "au": "armeabi-v7a,armeabi", "av": "arm64-v8a"}}, "platformextension": "android_native", "product": "whyred", "referrers": [{"api_ver": 83491610, "api_ver_name": "34.9.16-21 [0] [PR] 518042857", "click_ts": 1679717212, "google_custom": {"click_server_ts": 1679717207, "install_begin_server_ts": 1679717222, "install_version": "7.3.1", "instant": false}, "install_begin_ts": 1679717228, "latency": 324, "referrer": "af_tranid=jXBoc9fOTMTLDhJdVs03_Q&af_sub1=BDalcscAAAGHFi7OZQACr3wAAAAPAAAAAAAAAAAG&af_installpostback=false&af_ua=Mozilla/5.0 (Linux; Android 9.0.0; Honor 10 Lite Build/QP1A.191105.003) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.97 Mobile Safari/537.36&af_ip=37.228.201.124&pid=appspark_int&af_click_lookback=7d&af_prt=greengrass&idfa=f866e3fe-c4da-4d3f-84be-e269d0929dd0f866e3fe-c4da-4d3f-84be-e269d0929dd0&clickid=641e4224a3b1190001e3064d&af_lang=en-US&af_siteid=551&af_sub_siteid=15_735&advertising_id=f866e3fe-c4da-4d3f-84be-e269d0929dd0f866e3fe-c4da-4d3f-84be-e269d0929dd0", "response": "OK", "source": "google", "type": "store"}], "registeredUninstall": false, "rfr": {"clk": "1679717212", "code": "0", "install": "1679717228", "instant": false, "val": "af_tranid=jXBoc9fOTMTLDhJdVs03_Q&af_sub1=BDalcscAAAGHFi7OZQACr3wAAAAPAAAAAAAAAAAG&af_installpostback=false&af_ua=Mozilla/5.0 (Linux; Android 9.0.0; Honor 10 Lite Build/QP1A.191105.003) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.97 Mobile Safari/537.36&af_ip=37.228.201.124&pid=appspark_int&af_click_lookback=7d&af_prt=greengrass&idfa=f866e3fe-c4da-4d3f-84be-e269d0929dd0f866e3fe-c4da-4d3f-84be-e269d0929dd0&clickid=641e4224a3b1190001e3064d&af_lang=en-US&af_siteid=551&af_sub_siteid=15_735&advertising_id=f866e3fe-c4da-4d3f-84be-e269d0929dd0f866e3fe-c4da-4d3f-84be-e269d0929dd0"}, "sc_o": "p", "sdk": "28", "sig": "B262ECBAF629B2D7A9958EBB894E60A27E86A251D5036B53C37624D0D7102628", "timepassedsincelastlaunch": "-1", "tokenRefreshConfigured": false, "uid": "1679719236975-8783082296212626384"}

inapp_data = {"advertiserId": "6bc33da9-f93a-49b0-a7e2-715eb311139b", "advertiserIdEnabled": "true", "af_events_api": "1", "af_preinstalled": "false", "af_timestamp": "1679719237023", "af_v": "5a46db160996f8827c091ec3a26bdc56b2b02295", "af_v2": "e8cd08b20545e7820bfed5698506d46fb384a862", "app_version_code": "137", "app_version_name": "7.3.1", "appsflyerKey": "PQmjs6dfikqatrWRQ4EEG", "brand": "xiaomi", "carrier": "", "cell": {"mcc": 0, "mnc": 0}, "cksm_v1": "14012bf395e6b4c9af33a9434f08af706c", "counter": "1", "country": "IN", "date1": "2023-03-25_093831+0530", "date2": "2023-03-25_093831+0530", "device": "whyred", "deviceData": {"arch": "", "build_display_id": "PKQ1.180904.001", "cpu_abi": "arm64-v8a", "cpu_abi2": "", "dim": {"d_dpi": "440", "size": "2", "x_px": "1080", "xdp": "403.411", "y_px": "2030", "ydp": "403.411"}}, "deviceType": "user", "disk": "40436/51517", "eventName": "af_enter_app", "eventValue": "{\"af_lang\":null,\"af_country_code\":null,\"af_user_type\":0,\"af_content_type\":\"enter app\"}", "firstLaunchDate": "2023-03-25_044037+0000", "iaecounter": "1", "installDate": "2023-03-25_040831+0000", "installer_package": "com.android.vending", "isFirstCall": "true", "isGaidWithGps": "true", "ivc": false, "kef7963": "38a482155beac6fd4819131b0c531a0d5918190d5a1f1d", "lang": "English", "lang_code": "en", "last_boot_time": 1679639038999, "model": "Redmi Note 5 Pro", "network": "WIFI", "operator": "", "p_receipt": {"an": {"dbg": false, "hk": ";"}, "pr": {"ac": "0", "ah": "zygote64_32", "ai": "0", "ak": "default", "al": "cortex-a73", "am": "default", "an": "generic", "ap": "builder", "ar": "qcom", "as": "arm64-v8a", "at": "arm64-v8a,armeabi-v7a,armeabi", "au": "armeabi-v7a,armeabi", "av": "arm64-v8a"}}, "platformextension": "android_native", "product": "whyred", "registeredUninstall": false, "sc_o": "p", "sdk": "28", "sensors": {"am": {"n": "ICM20607 Accelerometer", "v": [[-4.1, 3.9, 7.8], [0, 0, -0.1]]}, "gs": {"n": "ICM20607 Gyroscope", "v": [[0, 0, 0], [0, 0, 0]]}, "mm": {"n": "AK09918 Magnetometer", "v": [[-16.5, -334.6], [0.3, 0.4]]}}, "sig": "B262ECBAF629B2D7A9958EBB894E60A27E86A251D5036B53C37624D0D7102628", "tokenRefreshConfigured": false, "uid": "1679719236975-8783082296212626384"}



# ==================== Appsflyer Conversion ====================
def appsflyer_conversions( app_data, device_data ):

    inc_(app_data,'counter')
    def_(app_data,'iaecounter')
    set_batteryLevel(app_data)

    if not app_data.get('last_launch'):
        timeSinceLastCall = "-1"
    else:
        timeSinceLastCall = int(time.time())-app_data.get('last_launch')

    app_data['last_launch'] = int(time.time())

    method = "post"

    url = 'https://conversions'+Appsflyer_data.get('con_url')+'/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'

    headers = {
        "Accept-Encoding"   : "gzip",
        "Content-Type"      : "application/octet-stream",
        "User-Agent"        : get_ua(device_data)
    }

    params = {
        "app_id"        : campaign_data.get('package_name'),
        "buildnumber"   : campaign_data.get('appsflyer').get('buildnumber'),
    }
    
    if not app_data.get('disk'):
        app_data['disk'] = "{available}/{total}".format(available =str(random.randint(8000, 16000)), total = str(random.randint(23000, 32000)))
    else:
        temp_disk = map(int, app_data.get('disk').split('/'))
        temp_disk[0] += random.randint(-150, 300)

        app_data['disk'] = "{available}/{total}".format(available = str(temp_disk[0]), total = str(temp_disk[1]))
    app_data['session_time'] = time.time()

    data = {
        "advertiserId"          : device_data.get('adid'), 
        "advertiserIdEnabled"   : "true", 
        "af_events_api"         : "1", 
        "af_preinstalled"       : "false", 
        "af_timestamp"          : str(int(time.time()*1000)), 
        "app_version_code"      : campaign_data.get('app_version_code'),
        "app_version_name"      : campaign_data.get('app_version_name'),
        "appsflyerKey"          : campaign_data.get('appsflyer').get('key'),
        "batteryLevel"          : app_data.get('btl'), 
        "brand"                 : device_data.get('brand'), 
        "carrier"               : device_data.get('carrier'), 
        "cell": {
            "mcc"               : int(device_data.get('mcc')), 
            "mnc"               : int(device_data.get('mnc'))
        },  
        "counter"               : str(app_data.get('counter')), 
        "country"               : device_data.get('locale').get('country'), 
        "date1"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "date2"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "device"                : device_data.get('device'), 
        "deviceData"            :
         {
            "arch"              : "", 
            "btch"              : app_data.get('btch'), 
            "btl"               : app_data.get('btl'), 
            "build_display_id"  : device_data.get('build'), 
            "cpu_abi"           : device_data.get('cpu_abi'), 
            "cpu_abi2"          : device_data.get('cpu_abi2'), 
            "dim"               :
             {
                "d_dpi"         : device_data.get('dpi'), 
                "size"          : app_data.get('dim_size'), 
                "x_px"          : device_data.get('resolution').split('x')[1],
                "xdp"           : app_data.get('xdp'), 
                "y_px"          : device_data.get('resolution').split('x')[0], 
                "ydp"           : app_data.get('ydp')
            },
            "sensors":[{
                "sN": "ICM20607 Accelerometer",
                "sT": 1,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,1),10),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(8,10),6)
                ],
                "sVS": [
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(8,10),6)
                ]
            },
            {
                "sN": "ICM20607 Gyroscope",
                "sT": 4,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,-1),10),
                    round(random.uniform(0,-1),10),
                    round(random.uniform(0,1),10)
                ],
                "sVS": [
                    round(random.uniform(0,1),10),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,1),10)
                ]
            },
            {
                "sN": "ICM20607 Gyroscope -Wakeup Secondary",
                "sT": 4,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,1),11),
                    round(random.uniform(0,-1),11),
                    round(random.uniform(0,1),10)
                ],
                "sVS": [
                    round(random.uniform(0,1),11),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,1),10)
                ]
            },
            {
                "sN": "AK09918 Magnetometer -Wakeup Secondary",
                "sT": 2,
                "sV": "AKM",
                "sVE": [
                    round(random.uniform(-13,-11),6),
                    round(random.uniform(16,19),6),
                    round(random.uniform(-45,-42),5)
                ],
                "sVS": [
                    round(random.uniform(-13,-11),6),
                    round(random.uniform(16,19),6),
                    round(random.uniform(-45,-42),5)
                ]
            },
            {
                "sN": "ICM20607 Accelerometer -Wakeup Secondary",
                "sT": 1,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,1),11),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(8,10),6)
                ],
                "sVS": [
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,-1),8),
                    round(random.uniform(8,10),6)
                ]
            },
            {
                "sN": "AK09918 Magnetometer",
                "sT": 2,
                "sV": "AKM",
                "sVE": [
                    round(random.uniform(-14,-12),6),
                    round(random.uniform(18,21),6),
                    round(random.uniform(-45,-42),6)
                ],
                "sVS": [
                    round(random.uniform(-14,-12),6),
                    round(random.uniform(18,21),6),
                    round(random.uniform(-45,-42),6)
                ]
            }
        ]
        },
        "deviceType"        : "user", 
        "disk"              : app_data.get('disk'),
        "exception_number"  : 0,
        "iaecounter"        : str(app_data.get('iaecounter')), 
        "installDate"       : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000', 
        "installer_package" : "com.android.vending", 
        "isFirstCall"       : "false" if app_data.get('counter') > 1 else "true", 
        "isGaidWithGps"     : "true", 
        "ivc"               : False, 
        "lang"              : "English", 
        "lang_code"         : "en", 
        "last_boot_time"    : app_data.get('last_boot_time'),
        "meta": {
            "first_launch": {
                "init_to_fg": random.randint(5,20)
            }
        },
        "model"             : device_data.get('model'), 
        "network"           : device_data.get('network').upper(), 
        "open_referrer"     : "android-app://"+campaign_data.get("package_name"), 
        "operator"          : device_data.get('carrier'), 
        "p_receipt"         :
             {
            "an": {
                "dbg"       : False, 
                "hk"        : ";"
            }, 
            "as"            :
                {
                "cav": random.randint(5,40),
                "null": random.randint(5,35),
                "other": random.randint(5,15)
            },
            "pr":{
                "ac": "0",
                "ah": "zygote64_32",
                "ai": "0",
                "ak": "default",
                "al": "cortex-a53",
                "am": "default",
                "an": "generic",
                "ap": "builder",
                "ar": "qcom",
                "as": device_data.get('cpu_abi'),
                "at": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "au": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "av": device_data.get('cpu_abi')
            }
        }, 
        "platformextension"         : campaign_data.get("platformextension"), 
        "product"                   : device_data.get('product'), 
        "referrers": [
        {
            "api_ver": campaign_data.get('api_ver'), 
            "api_ver_name": campaign_data.get('api_ver_name'), 
            "click_ts": int(app_data.get('times').get('click_time')),  
            "google_custom": { 
                "click_server_ts": int(app_data.get('times').get('click_time'))-1, 
                "install_begin_server_ts": int(app_data.get('times').get('download_begin_time'))-2, 
                "install_version": campaign_data.get('app_version_name'), 
                "instant": False
                },
            "install_begin_ts"  : int(app_data.get('times').get('download_begin_time')), 
            "latency"           : 92,
            "referrer"          : app_data.get("referrer", "utm_source=(not%20set)&utm_medium=(not%20set)"), 
            "response"          : "OK", 
            "source"            : "google", 
            "type"              : "store"
        }
        ], 
        "registeredUninstall"       : False, 
        "rfr"                       :
         {  
            "clk"                   : str(int(app_data.get('times').get('click_time'))), 
            "code"                  : "0", 
            "install"               : str(int(app_data.get('times').get('download_begin_time'))),
            "instant"               : False,
            "val"                   : app_data.get("referrer", "utm_source=(not%20set)&utm_medium=(not%20set)")
        },  
        "sc_o"                      : "l", 
        "sdk"                       : device_data.get('sdk'), 
        "timepassedsincelastlaunch" : str(timeSinceLastCall), 
        "tokenRefreshConfigured"    : False, 
    }

    if campaign_data.get('sig'):
        data["sig"]  = campaign_data.get("sig")


    if conversion_data.get('android_id'):
        data['android_id'] = device_data.get('android_id')
    if conversion_data.get('appUserId'):
        data['appUserId'] = app_data.get('appUserId')
    if conversion_data.get('customData'):
        data['customData'] = app_data.get('customData')
    if conversion_data.get('fb'): 
        data['fb'] =  app_data.get('fb')
    if conversion_data.get('user_emails'): 
        data['user_emails'] = app_data.get('user_emails')
    if conversion_data.get('sdkExtension'): 
        data['sdkExtension'] = conversion_data.get('sdkExtension')
    


    data['uid']                 = app_data.get('uid')
    data['firstLaunchDate']     = app_data.get('firstLaunchDate')
    data['cksm_v1']             = cksm_v1(data['date1'],data['af_timestamp'])
    data['af_v']                = util.sha1(data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13])
    data['af_v2']               = util.sha1(util.md5(data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')))

    

    if app_data.get('counter') > 2:
        del data['deviceData']['sensors']
        if data.get('rfr'):
            del data['rfr']
        if data.get('p_receipt'):
            del data['p_receipt']

    if data.get('isFirstCall')=="false":
        del data['batteryLevel']
        data['open_referrer']="android-app://"+campaign_data.get("package_name")            

    kefdata = get_kef_data(app_data, data)
    data[kefdata[0]] = kefdata[1]

    if Appsflyer_data.get('cdn'):
        data['meta'] = conversion_meta(app_data)



    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':af_cipher(campaign_data.get('appsflyer').get('spiKey'), json.dumps(data,separators = (',',':')))}



# ==================== Appsflyer Launches ====================
def appsflyer_launches( app_data, device_data ):

    inc_(app_data,'counter')
    def_(app_data,'iaecounter')
    set_batteryLevel(app_data)

    if not app_data.get('last_launch'):
        timeSinceLastCall = "-1"
    else:
        timeSinceLastCall = int(time.time())-app_data.get('last_launch')

    app_data['last_launch'] = int(time.time())

    method = "post"

    url = 'https://launches'+Appsflyer_data.get('la_url')+'/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'

    headers = {
        "Accept-Encoding"  : "gzip",
        "Content-Type"     : "application/octet-stream",
        "User-Agent"       : get_ua(device_data)
    }

    params = {
        "app_id"        : campaign_data.get('package_name'),
        "buildnumber"   : campaign_data.get('appsflyer').get('buildnumber'),
    } 
    if not app_data.get('disk'):
        app_data['disk'] = "{available}/{total}".format(available =str(random.randint(8000, 16000)), total = str(random.randint(23000, 32000)))
    else:
        temp_disk = map(int, app_data.get('disk').split('/'))
        temp_disk[0] += random.randint(-150, 300)

        app_data['disk'] = "{available}/{total}".format(available = str(temp_disk[0]), total = str(temp_disk[1]))


    data = {
        "advertiserId"          : device_data.get('adid'), 
        "advertiserIdEnabled"   : "true", 
        "af_events_api"         : "1", 
        "af_preinstalled"       : "false", 
        "af_timestamp"          : str(int(time.time()*1000)), 
        "app_version_code"      : campaign_data.get('app_version_code'),
        "app_version_name"      : campaign_data.get('app_version_name'),
        "appsflyerKey"          : campaign_data.get('appsflyer').get('key'),
        "brand"                 : device_data.get('brand'), 
        "carrier"               : device_data.get('carrier'), 
        "cell"                  :
         {
            "mcc"               : int(device_data.get('mcc')), 
            "mnc"               : int(device_data.get('mnc'))
        },  
        "counter"               : str(app_data.get('counter')), 
        "country"               : device_data.get('locale').get('country'), 
        "date1"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "date2"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "device"                : device_data.get('device'), 
        "deviceData"            :
         {
            "arch"              : "", 
            "btch"              : app_data.get('btch'), 
            "btl"               : app_data.get('btl'), 
            "build_display_id"  : device_data.get('build'), 
            "cpu_abi"           : device_data.get('cpu_abi'), 
            "cpu_abi2"          : device_data.get('cpu_abi2'), 
            "dim"               :
             {
                "d_dpi"         : device_data.get('dpi'), 
                "size"          : app_data.get('dim_size'), 
                "x_px"          : device_data.get('resolution').split('x')[1],
                "xdp"           : app_data.get('xdp'), 
                "y_px"          : device_data.get('resolution').split('x')[0], 
                "ydp"           : app_data.get('ydp')
            },
            "sensors":  [{
                "sN": "ICM20607 Accelerometer",
                "sT": 1,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,1),10),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(8,10),6)
                ],
                "sVS": [
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(8,10),6)
                ]
            },
            {
                "sN": "ICM20607 Gyroscope",
                "sT": 4,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,-1),10),
                    round(random.uniform(0,-1),10),
                    round(random.uniform(0,1),10)
                ],
                "sVS": [
                    round(random.uniform(0,1),10),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,1),10)
                ]
            },
            {
                "sN": "ICM20607 Gyroscope -Wakeup Secondary",
                "sT": 4,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,1),11),
                    round(random.uniform(0,-1),11),
                    round(random.uniform(0,1),10)
                ],
                "sVS": [
                    round(random.uniform(0,1),11),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,1),10)
                ]
            },
            {
                "sN": "AK09918 Magnetometer -Wakeup Secondary",
                "sT": 2,
                "sV": "AKM",
                "sVE": [
                    round(random.uniform(-13,-11),6),
                    round(random.uniform(16,19),6),
                    round(random.uniform(-45,-42),5)
                ],
                "sVS": [
                    round(random.uniform(-13,-11),6),
                    round(random.uniform(16,19),6),
                    round(random.uniform(-45,-42),5)
                ]
            },
            {
                "sN": "ICM20607 Accelerometer -Wakeup Secondary",
                "sT": 1,
                "sV": "InvenSense",
                "sVE": [
                    round(random.uniform(0,1),11),
                    round(random.uniform(0,-1),9),
                    round(random.uniform(8,10),6)
                ],
                "sVS": [
                    round(random.uniform(0,-1),9),
                    round(random.uniform(0,-1),8),
                    round(random.uniform(8,10),6)
                ]
            },
            {
                "sN": "AK09918 Magnetometer",
                "sT": 2,
                "sV": "AKM",
                "sVE": [
                    round(random.uniform(-14,-12),6),
                    round(random.uniform(18,21),6),
                    round(random.uniform(-45,-42),6)
                ],
                "sVS": [
                    round(random.uniform(-14,-12),6),
                    round(random.uniform(18,21),6),
                    round(random.uniform(-45,-42),6)
                ]
            }
        ]
        },
        "deviceType"        : "user", 
        "disk"              : app_data.get('disk'),
        "exception_number"  : 0,
        "iaecounter"        : str(app_data.get('iaecounter')),
        "installDate"       : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000', 
        "installer_package" : "com.android.vending", 
        "isFirstCall"       : "false" if app_data.get('counter') > 1 else "true", 
        "isGaidWithGps"     : "true", 
        "ivc"               : False, 
        "lang"              : "English", 
        "lang_code"         : "en", 
        "last_boot_time"    : app_data.get('last_boot_time'),
        "meta": {
            "first_launch": {
            "from_fg": 960, 
            "init_to_fg": 9, 
            "net": 12605
            }
        }, 
        "model"             : device_data.get('model'), 
        "network"           : device_data.get('network').upper(), 
        "open_referrer"     : "android-app://"+campaign_data.get("package_name"), 
        "operator"          : device_data.get('carrier'), 
        "p_receipt"         :
         {
            "an": {
                "dbg"       : False, 
                "hk"        : ";"
            }, 
            "as"            :
                {
                "cav": random.randint(5,40),
                "null": random.randint(5,35),
                "other": random.randint(5,15)
            },
            "pr":{
                "ac": "0",
                "ah": "zygote64_32",
                "ai": "0",
                "ak": "default",
                "al": "cortex-a53",
                "am": "default",
                "an": "generic",
                "ap": "builder",
                "ar": "qcom",
                "as": device_data.get('cpu_abi'),
                "at": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "au": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "av": device_data.get('cpu_abi')
            }
        }, 
        "platformextension"    : campaign_data.get("platformextension"), 
        "product"              : device_data.get('product'), 
        "referrers": [
        {
            "api_ver": campaign_data.get('api_ver'), 
            "api_ver_name": campaign_data.get('api_ver_name'), 
            "click_ts": int(app_data.get('times').get('click_time')),  
            "google_custom": { 
                "click_server_ts": int(app_data.get('times').get('click_time'))-1, 
                "install_begin_server_ts": int(app_data.get('times').get('download_begin_time'))-2, 
                "install_version": campaign_data.get('app_version_name'), 
                "instant": False
                },
            "install_begin_ts": int(app_data.get('times').get('download_begin_time')), 
            "latency": 92,
            "referrer":app_data.get("referrer", "utm_source=(not%20set)&utm_medium=(not%20set)"), 
            "response": "OK", 
            "source": "google", 
            "type": "store"
        }
        ], 
        "registeredUninstall"       : False, 
        "rfr"                       :
         {
            "clk"                   : str(int(app_data.get('times').get('click_time'))), 
            "code"                  : "0", 
            "install"               : str(int(app_data.get('times').get('download_begin_time'))), 
            "instant"               : False,
            "val"                   : app_data.get("referrer", "utm_source=(not%20set)&utm_medium=(not%20set)")
        }, 
        "sc_o"                      : "l", 
        "sdk"                       : device_data.get('sdk'), 
        # "sig"                       : campaign_data.get("sig"), 
        "timepassedsincelastlaunch" : str(timeSinceLastCall), 
        "tokenRefreshConfigured"    : False, 
    }

    if campaign_data.get('sig'):
        data["sig"]  = campaign_data.get("sig")

    if conversion_data.get('android_id'):
        data['android_id'] = device_data.get('android_id')
    if conversion_data.get('appUserId'):
        data['appUserId'] = app_data.get('appUserId')
    if conversion_data.get('customData'):
        data['customData'] = app_data.get('customData')
    if conversion_data.get('fb'): 
        data['fb'] =  app_data.get('fb')
    if conversion_data.get('user_emails'): 
        data['user_emails'] = app_data.get('user_emails')
    if conversion_data.get('sdkExtension'): 
        data['sdkExtension'] = conversion_data.get('sdkExtension')
    

    data['uid']                = app_data.get('uid')
    data['firstLaunchDate']    = app_data.get('firstLaunchDate')
    data['cksm_v1']            = cksm_v1(data['date1'],data['af_timestamp'])
    data['af_v']               = util.sha1(data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13])
    data['af_v2']              = util.sha1(util.md5(data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')))

    if int(app_data.get('counter')) > 2:
        del data['deviceData']['sensors']
        if data.get("referrers"):
            del data["referrers"]
        if data.get('rfr'):
            del data['rfr']
        if data.get('p_receipt'):
            del data['p_receipt']

    kefdata = get_kef_data(app_data, data)
    data[kefdata[0]] = kefdata[1]

    if app_data.get('session_time'):
        data['prev_session_dur'] = int(time.time()- app_data.get('session_time'))

    app_data['session_time'] = time.time()

    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':af_cipher(campaign_data.get('appsflyer').get('spiKey'), json.dumps(data,separators = (',',':')))}

# ==================== Appsflyer Inapps ====================
def appsflyer_inapps( app_data, device_data, eventname= "", eventvalue= "" ):

    def_(app_data,'counter')
    inc_(app_data,'iaecounter')

    method = "post"

    url = 'https://inapps'+Appsflyer_data.get('in_url')+'/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'

    headers = {
        "Accept-Encoding":"gzip",
        "Content-Type":"application/octet-stream",
        "User-Agent":get_ua(device_data)
    }

    params = {
        "app_id":campaign_data.get('package_name'),
        "buildnumber":campaign_data.get('appsflyer').get('buildnumber'),
    }
    if not app_data.get('disk'):
        app_data['disk'] = "{available}/{total}".format(available =str(random.randint(8000, 16000)), total = str(random.randint(23000, 32000)))
    else:
        temp_disk = map(int, app_data.get('disk').split('/'))
        temp_disk[0] += random.randint(-150, 300)

        app_data['disk'] = "{available}/{total}".format(available = str(temp_disk[0]), total = str(temp_disk[1]))


    data = {
        "advertiserId"          : device_data.get('adid'), 
        "advertiserIdEnabled"   : "true", 
        "af_events_api"         : "1", 
        "af_preinstalled"       : "false", 
        "af_timestamp"          : str(int(time.time()*1000)), 
        "app_version_code"      : campaign_data.get('app_version_code'),
        "app_version_name"      : campaign_data.get('app_version_name'),
        "appsflyerKey"          : campaign_data.get('appsflyer').get('key'),
        "appUserId"             : app_data.get('appUserId'),
        "brand"                 : device_data.get('brand'), 
        "carrier"               : device_data.get('carrier'),
        "cell"                  :
            {
            "mcc"               : int(device_data.get('mcc')), 
            "mnc"               : int(device_data.get('mnc'))
        },  
        "counter"               : str(app_data.get('counter')), 
        "country"               : device_data.get('locale').get('country'), 
        "date1"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "date2"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "device"                : device_data.get('device'), 
        "deviceData"            :
            {
            "arch"              : "", 
            "build_display_id"  : device_data.get('build'), 
            "cpu_abi"           : device_data.get('cpu_abi'), 
            "cpu_abi2"          : device_data.get('cpu_abi2'), 
            "dim"               :
                {
                "d_dpi"         : device_data.get('dpi'), 
                "size"          : app_data.get('dim_size'), 
                "x_px"          : device_data.get('resolution').split('x')[1],
                "xdp"           : app_data.get('xdp'), 
                "y_px"          : device_data.get('resolution').split('x')[0], 
                "ydp"           : app_data.get('ydp')
            }
        },
        "deviceType"            : "user", 
        "disk"                  : app_data.get('disk'),
        "eventName"             : eventname, 
        "eventValue"            : json.dumps(eventvalue,separators = (',',':')), 
        "iaecounter"            : str(app_data.get('iaecounter')), 
        "installDate"           : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000', 
        "installer_package"     : "com.android.vending", 
        "isFirstCall"           : "false" if app_data.get('counter') > 1 else "true", 
        "isGaidWithGps"         : "true", 
        "ivc"                   : False, 
        "lang"                  : "English", 
        "lang_code"             : "en", 
        "last_boot_time"        : app_data.get('last_boot_time'),
        "model"                 : device_data.get('model'), 
        "network"               : device_data.get('network').upper(), 
        "operator"              : device_data.get('carrier'), 
        "p_receipt"             :
            {
            "an": {
                "dbg"           : False, 
                "hk"            : ";"
            }, 
            "as"            :
                {
                    "cav": random.randint(5,40),
                    "null": random.randint(5,35),
                    "other": random.randint(5,15)
                },
            "pr":{
                "ac": "0",
                "ah": "zygote64_32",
                "ai": "0",
                "ak": "default",
                "al": "cortex-a53",
                "am": "default",
                "an": "generic",
                "ap": "builder",
                "ar": "qcom",
                "as": device_data.get('cpu_abi'),
                "at": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "au": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "av": device_data.get('cpu_abi')
            }
        }, 
        "platformextension"     : campaign_data.get("platformextension"), 
        "prev_event"            : "",
        "product"               : device_data.get('product'),
        "registeredUninstall"   : False, 
        # "sdkExtension"          : "unity_android_6.1.4",
        "sc_o"                  : "l", 
        "sdk"                   : device_data.get('sdk'), 
        "sensors"               :
            {
            "am": {
                "n": "ICM20607 Accelerometer -Wakeup Secondary",
                "v": [
                    [
                        0,
                        0,
                        9.6
                    ],
                    [
                        0,
                        0,
                        0
                    ]
                ]
            },
            "er": "no_svs",
            "gs": {
                "n": "ICM20607 Gyroscope -Wakeup Secondary",
                "v": [
                    [
                        0,
                        0,
                        0
                    ],
                    [
                        0,
                        0,
                        0
                    ]
                ]
            },
            "mm": {
                "n": "AK09918 Magnetometer",
                "v": [
                    [
                        -7,
                        -389.3
                    ],
                    []
                ]
            }
        },
        # "sig"                    : campaign_data.get("sig"), 
        "tokenRefreshConfigured" : False, 
    }

    if campaign_data.get('sig'):
        data["sig"]  = campaign_data.get("sig")

    if inapp_data.get('android_id'):
        data['android_id'] = device_data.get('android_id')
    if inapp_data.get('appUserId'):
        data['appUserId'] = app_data.get('appUserId')
    if inapp_data.get('customData'):
        data['customData'] = app_data.get('customData')
    if inapp_data.get('fb'): 
        data['fb'] =  app_data.get('fb')
    if inapp_data.get('user_emails'): 
        data['user_emails'] = app_data.get('user_emails')
    if inapp_data.get('sdkExtension'): 
        data['sdkExtension'] = inapp_data.get('sdkExtension')
    

    data['uid']             = app_data.get('uid')
    data['firstLaunchDate'] = app_data.get('firstLaunchDate')
    data['cksm_v1']         = cksm_v1(data['date1'],data['af_timestamp'])
    data['af_v']            = util.sha1(data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13])
    data['af_v2']           = util.sha1(util.md5(data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')))


    if not app_data.get('prev_event'):
        del data['prev_event']
    else:
        data['prev_event'] = json.dumps(app_data.get('prev_event'),separators = (',',':'))

    if app_data.get('counter') > 2:
        if data.get('p_receipt'):
            del data['p_receipt']



    # if app_data.get('iaecounter')==1:
    #     data["sensors"]= {"er": "na"}







    update_event_records( app_data, data.get('eventName'), data.get('eventValue'), data.get('af_timestamp'))

    kefdata = get_kef_data(app_data, data)
    data[kefdata[0]] = kefdata[1]

    # if app_data.get('session_time'):
    #     data['prev_session_dur'] = int(time.time()- app_data.get('session_time'))


    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':af_cipher(campaign_data.get('appsflyer').get('spiKey'), json.dumps(data,separators = (',',':')))}

# ==================== Appsflyer Register ====================
def appsflyer_register( app_data, device_data ):

    method = 'post'

    url = 'https://register'+Appsflyer_data.get('res_url')+'/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'

    headers = {
        'Accept-Encoding'   : 'gzip',
        'Content-Type'      : 'application/json',
        'User-Agent'        : get_ua(device_data)
    }

    params = {
        'app_id'        : campaign_data.get('package_name'),
        'buildnumber'   : campaign_data.get('appsflyer').get('buildnumber')
    }

    data = {
        'advertiserId'     : device_data.get('adid'),
        'af_gcm_token'     : app_data.get('af_gcm_token'),
        'app_name'         : campaign_data.get('app_name'),
        'app_version_code' : campaign_data.get('app_version_code'),
        'app_version_name' : campaign_data.get('app_version_name'),
        'brand'            : device_data.get('brand'),
        'carrier'          : device_data.get('carrier'),
        'devkey'           : campaign_data.get('appsflyer').get('key'),
        'installDate'      : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000',
        'launch_counter'   : str(app_data.get('counter')),
        'model'            : device_data.get('model'),
        'network'          : device_data.get('network').upper(),
        'operator'         : device_data.get('carrier'),
        'sdk'              : device_data.get('sdk'),

    }
    if register_data.get('kef9999'):
        data['kef9999'] = "babeae0540d91f201848191d1b0c531a0d5918190d5a1b"
    
    if register_data.get('appUserId'):
        data['appUserId'] = app_data.get('appUserId')



    # if not app_data.get('af_gcm_token'):
    #     app_data['af_gcm_token'] = util.get_random_string(type='all', size=22)+ ':APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

    data['uid'] = app_data.get('uid')
    data['af_gcm_token'] = app_data.get('af_gcm_token')

    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': json.dumps(data,separators = (',',':'))}

# ==================== Appsflyer GCDSDK ====================
def appsflyer_gcdsdk( app_data, device_data ):

    method = "get"

    url = 'https://gcdsdk'+Appsflyer_data.get('gcd_url')+'/install_data/'+Appsflyer_data.get('gcd_ver')+'/'+campaign_data.get('package_name')

    headers = {
        "Accept-Encoding" : "gzip",
        "User-Agent" : get_ua(device_data)
    }
    
    params = {
        "device_id" : app_data.get('uid'),
        "devkey" : campaign_data.get('appsflyer').get('key')
    }


    params['device_id'] = app_data.get('uid')

    if Appsflyer_data.get('af_request_epoch_ms'):
        af_request_epoch_ms = str(int(time.time()*1000))
        headers['af_request_epoch_ms'] = af_request_epoch_ms
    if Appsflyer_data.get('afsign'):
        headers['af_sig'] = get_gcd_sdk_af_sig(package_name=campaign_data.get('package_name'),af_key=campaign_data.get('appsflyer').get('key'),uid=app_data.get('uid'),af_request_epoch_ms=af_request_epoch_ms)
        del params['devkey']
    data = {}

    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}





def attr_appsflyer( app_data, device_data ):
    
    inc_(app_data,'counter')
    def_(app_data,'iaecounter')
    set_batteryLevel(app_data)

    if not app_data.get('last_launch'):
        timeSinceLastCall = "-1"
    else:
        timeSinceLastCall = int(time.time())-app_data.get('last_launch')

    app_data['last_launch'] = int(time.time())

    method = "post"

    url = 'https://attr'+Appsflyer_data.get('att_url')+'/api/'+campaign_data.get('appsflyer').get('version')+'/androidevent'

    headers = {
        "Accept-Encoding"  : "gzip",
        "Content-Type"     : "application/octet-stream",
        "User-Agent"       : get_ua(device_data)
    }

    params = {
        "app_id"        : campaign_data.get('package_name'),
        "buildnumber"   : campaign_data.get('appsflyer').get('buildnumber'),
    } 
    if not app_data.get('disk'):
        app_data['disk'] = "{available}/{total}".format(available =str(random.randint(8000, 16000)), total = str(random.randint(23000, 32000)))
    else:
        temp_disk = map(int, app_data.get('disk').split('/'))
        temp_disk[0] += random.randint(-150, 300)

        app_data['disk'] = "{available}/{total}".format(available = str(temp_disk[0]), total = str(temp_disk[1]))


    data = {
        "advertiserId"          : device_data.get('adid'), 
        "advertiserIdEnabled"   : "true", 
        "af_events_api"         : "1", 
        "af_preinstalled"       : "false", 
        "af_timestamp"          : str(int(time.time()*1000)),
        "app_version_code"      : campaign_data.get('app_version_code'),
        "app_version_name"      : campaign_data.get('app_version_name'),
        "appsflyerKey"          : campaign_data.get('appsflyer').get('key'),
        "brand"                 : device_data.get('brand'), 
        "carrier"               : device_data.get('carrier'), 
        "cell"                  :
            {
                "mcc"           : int(device_data.get('mcc')), 
                "mnc"           : int(device_data.get('mnc'))
            }, 
        "counter"               : str(app_data.get('counter')), 
        "country"               : device_data.get('locale').get('country'), 
        "date1"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "date2"                 : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')+app_data.get('sec')).strftime('%Y-%m-%d_%H%M%S')+device_data.get('timezone'), 
        "device"                : device_data.get('device'), 
        "deviceData"            :
         {
            "arch"              : "", 
            "btch"              : app_data.get('btch'), 
            "btl"               : app_data.get('btl'), 
            "build_display_id"  : device_data.get('build'), 
            "cpu_abi"           : device_data.get('cpu_abi'), 
            "cpu_abi2"          : device_data.get('cpu_abi2'), 
            "dim"               :
             {
                "d_dpi"         : device_data.get('dpi'), 
                "size"          : app_data.get('dim_size'), 
                "x_px"          : device_data.get('resolution').split('x')[1],
                "xdp"           : app_data.get('xdp'), 
                "y_px"          : device_data.get('resolution').split('x')[0], 
                "ydp"           : app_data.get('ydp')
            },
            "sensors":  [{
                "sN": "ICM20607 Accelerometer",
                "sT": 1,
                "sV": "InvenSense"
            },
            {
                "sN": "ICM20607 Gyroscope",
                "sT": 4,
                "sV": "InvenSense"
            },
            {
                "sN": "ICM20607 Gyroscope -Wakeup Secondary",
                "sT": 4,
                "sV": "InvenSense"
            },
            {
                "sN": "AK09918 Magnetometer -Wakeup Secondary",
                "sT": 2,
                "sV": "AKM"
            },
            {
                "sN": "ICM20607 Accelerometer -Wakeup Secondary",
                "sT": 1,
                "sV": "InvenSense"
            },
            {
                "sN": "AK09918 Magnetometer",
                "sT": 2,
                "sV": "AKM",
                "sVS": [
                    201.45416,
                    60.188293,
                    -428.685
                ]
            }
        ]
        },
        "deviceType"        : "user", 
        "disk"              : app_data.get('disk'),
        "exception_number"  : 0,
        "iaecounter"        : str(app_data.get('iaecounter')),
        "installDate"       : datetime.utcfromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%d_%H%M%S')+'+0000', 
        "installer_package" : "com.android.vending", 
        "isFirstCall"       : "false" if app_data.get('counter') > 1 else "true", 
        "isGaidWithGps"     : "true", 
        "ivc"               : False, 
        "lang"              : "English", 
        "lang_code"         : "en", 
        "last_boot_time"    : app_data.get('last_boot_time'),
        "meta"              : {},
        "model"             : device_data.get('model'), 
        "network"           : device_data.get('network').upper(), 
        "open_referrer"     : "android-app://"+campaign_data.get("package_name"), 
        "operator"          : device_data.get('carrier'), 
        "p_receipt"         :
         {
            "an": {
                "dbg"       : False, 
                "hk"        : ";"
            }, 
            "as": {
                "cav": 1,
                "null": 270,
                "other": 43
            },
            "pr": {
                "ac": "0",
                "ah": "zygote64_32",
                "ai": "0",
                "ak": "default",
                "al": "cortex-a53",
                "am": "default",
                "an": "generic",
                "ap": "builder",
                "ar": "qcom",
                "as": device_data.get('cpu_abi'),
                "at": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "au": device_data.get('cpu_abi')+ ',' +device_data.get('cpu_abi2'),
                "av": device_data.get('cpu_abi')
            }
        }, 
        "platformextension"         : campaign_data.get("platformextension"), 
        "product"                   : device_data.get('product'),
        "referrers": [
        {
            "api_ver"       : campaign_data.get('api_ver'), 
            "api_ver_name"  : campaign_data.get('api_ver_name'), 
            "click_ts"      : int(app_data.get('times').get('click_time')),  
            "google_custom" :
                {
                "click_server_ts"           : int(app_data.get('times').get('click_time'))-1, 
                "install_begin_server_ts"   : int(app_data.get('times').get('download_begin_time'))-2, 
                "install_version"           : campaign_data.get('app_version_name'), 
                "instant"                   : False
                },
            "install_begin_ts"  : int(app_data.get('times').get('download_begin_time')), 
            "latency"           : 92,
            "referrer"          : app_data.get("referrer", "utm_source=(not%20set)&utm_medium=(not%20set)"), 
            "response"          : "OK", 
            "source"            : "google", 
            "type"              : "store"
        }
        ],
        "registeredUninstall"       : False, 
        "rfr"                       :
         {
            "clk"                   : str(int(app_data.get('times').get('click_time'))), 
            "code"                  : "0", 
            "install"               : str(int(app_data.get('times').get('download_begin_time'))), 
            "instant"               : False,
            "val"                   : app_data.get("referrer", "utm_source=(not%20set)&utm_medium=(not%20set)")
        }, 
        "sc_o"                      : "l", 
        "sdk"                       : device_data.get('sdk'), 
        "sig"                       : campaign_data.get("sig"), 
        "timepassedsincelastlaunch" : "0", 
        "tokenRefreshConfigured"    : False, 
    }



    data['uid']                = app_data.get('uid')
    data['firstLaunchDate']    = app_data.get('firstLaunchDate')
    data['cksm_v1']            = cksm_v1(data['date1'],data['af_timestamp'])
    data['af_v']               = util.sha1(data.get('appsflyerKey')[0:7]+data.get('uid')[0:7]+data.get('af_timestamp')[-7:13])
    data['af_v2']              = util.sha1(util.md5(data.get('appsflyerKey')+data.get('af_timestamp')+data.get('uid')+data.get('installDate')+data.get('counter')+data.get('iaecounter')))

    if int(app_data.get('counter')) > 2:
        del data['deviceData']['sensors']
        if data.get("referrers"):
            del data["referrers"]
        if data.get('rfr'):
            del data['rfr']
        if data.get('p_receipt'):
            del data['p_receipt']

    kefdata = get_kef_data(app_data, data)
    data[kefdata[0]] = kefdata[1]

    if app_data.get('session_time'):
        data['prev_session_dur'] = int(time.time()- app_data.get('session_time'))

    app_data['session_time'] = time.time()

    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':af_cipher(campaign_data.get('appsflyer').get('spiKey'), json.dumps(data,separators = (',',':')))}



def appsflyer_dlsdk(app_data,device_data):

    url = "https://dlsdk"+Appsflyer_data.get('dlsdk_url')+"/v1.0/android/"+campaign_data.get('package_name')
    method = "post"
    headers = {
    "Content-Type":"application/json",
    "User-Agent":get_ua(device_data),
    "Accept-Encoding":"gzip",
    }

    params = {
        "af_sig":'06aa74825006f9a1874fc637751d66e033954f764a1d303b50791bc5514525fa',
        'sdk_version': campaign_data.get('appsflyer').get('version').replace('v','')
    }

    data_dict = {
                "os":device_data.get('os_version'),
                "gaid": {
                            "type":"unhashed",
                            "value":str(uuid.uuid4())
                        },
                "is_first":True,
                "lang":"en-IN",
                "type":device_data.get('model'),
                "request_id":app_data.get('uid'),
                "timestamp":app_data.get('dlsdk_timestamp'),
                "request_count":1
            }
    if app_data.get('referrer'):
        data_dict["referrers"] = [{
                        "source": "google",
                        "value": app_data.get('referrer')
                    }]
    params['af_sig'] = get_dlsdk_af_sig(package_name=campaign_data.get('package_name'),af_key=campaign_data.get('appsflyer').get('key'),data_dict=data_dict)
    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data':json.dumps(data_dict)}


def appsflyer_cdn( app_data, device_data ):
    if Appsflyer_data.get('cdn_version') != "v1":
        print("change cdn version")
        exit()

    method = 'get'
    app_data['cdn_token']=get_cdn_token(campaign_data.get('package_name'),campaign_data.get('appsflyer').get('key'))
    url = 'https://cdn-settings'+Appsflyer_data.get('cdn_url')+'/android/v1/'+str(app_data.get('cdn_token'))+'/settings'

    headers = {
    "Content-Type": "application/json",
    'User-Agent':get_ua(device_data),
    'Accept-Encoding': "gzip"
    }


    params ={}

    data = {}


    return {'url': url, 'httpmethod': method, 'headers': headers, 'params': params, 'data': data}


def firebase_installations( app_data, device_data ):
	method = "post"
	url = 'https://firebaseinstallations.googleapis.com'+firebase_data.get('url')

	headers = {
		'Content-Type':"application/json",
		'Accept':"application/json",
		'Cache-Control':"no-cache",
		'X-Android-Package':campaign_data.get('package_name'),
		'x-firebase-client':firebase_data.get('x-firebase-client'),
		'X-Android-Cert':firebase_data.get('X-Android-Cert'),
		'x-goog-api-key':firebase_data.get('x-goog-api-key'),
		'User-Agent':get_ua(device_data),
		'Connection':"Keep-Alive",
		'Accept-Encoding':"gzip"
	}

	data ={
		"fid": app_data.get('fid'),
		"appId":firebase_data.get('data').get('appId') ,
		"authVersion": firebase_data.get('data').get('authVersion'),
		"sdkVersion": firebase_data.get('data').get('sdkVersion')
	}
	
	return {'url': url, 'httpmethod': method, 'headers': headers, 'params': None, 'data': json.dumps(data)}







#==============================================================================
def install(app_data, device_data): 

    if inapp_data.get('appUserId') or conversion_data.get('appUserId') or register_data.get('appUserId') : 
        print("Please create appUserId")
        exit()
    if inapp_data.get('customData') or conversion_data.get('customData'):
        print("Please create customdata")
        exit()
    if inapp_data.get('user_emails') or conversion_data.get('user_emails'):
        print("Please create User_email")
        exit()

    if not app_data.get('times'):
        # print "Please wait installing..."
        installtimenew.main(app_data, device_data, app_size=campaign_data.get('app_size'),os='android')
        def_sec(app_data,device_data)
        app_data['uid'] = '{0}-{1}'.format(int(time.time()*1000), random.getrandbits(64))
        set_batteryLevel(app_data)
        set_deviceData(app_data)
        app_data['last_boot_time'] = int(time.time()*1000 - random.randint(20000, 30000))
        app_data['app_start_time'] =time.time()
        app_data['appUserId']= str(uuid.uuid4())
    app_data['xdp']=str(float(device_data.get('dpi'))-round(random.uniform(0, 2),3))
    app_data['ydp']=str(float(device_data.get('dpi'))-round(random.uniform(0, 2),3))
    app_data['dlsdk_timestamp'] = datetime.utcfromtimestamp(int(time.time())).strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
    app_data['fid']=random.choice(['c','d','e','f'])+''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(21))
    if firebase_data.get('url'):
        num = 5
        while(num >0):
            req = firebase_installations(app_data,device_data)
            res= util.execute_request(**req)
            try:
                tmp = json.loads(res.get('data'))
                app_data['fid2'] = tmp.get('fid')
                break
            except:
                pass
            num -= 1
        if not app_data.get('fid2'):
            raise Exception ("Erorr in Firebase call  for country"+device_data.get('locale').get('country'))
            return {'status':True}


    if Appsflyer_data.get('cdn'):
        try:
            req = appsflyer_cdn( app_data, device_data )
            res = util.execute_request(**req)
            tem = json.loads(res.get('dara'))
            app_data['cdn_ver']  = tem.get('ver')
        except:
            pass


    if Appsflyer_data.get('dlsdk'):
        req = appsflyer_dlsdk(app_data,device_data)
        util.execute_request(**req)


    if not app_data.get('firstLaunchDate'):

        app_data['firstLaunchDate'] = datetime.utcfromtimestamp(int(time.time())).strftime("%Y-%m-%d_%H%M%S")+"+0000"

    if not app_data.get('af_gcm_token'):
        app_data['af_gcm_token'] = app_data.get('fid2') + ':APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))

    if conversion_data.get('fb'): 
        app_data['fb'] = str(uuid.uuid4())


    #print 'conversion'
    req  = appsflyer_conversions( app_data, device_data )
    util.execute_request(**req)

    if Appsflyer_data.get('attr'):
        req = attr_appsflyer( app_data, device_data )
        util.execute_request(**req)

    if Appsflyer_data.get('register'):
        req = appsflyer_register( app_data, device_data )
        util.execute_request(**req)

    #print 'Gcd_sdk'
    req  = appsflyer_gcdsdk( app_data, device_data )
    util.execute_request(**req)


    if random.randint(1,10)>6:
        #print 'launches'
        req  = appsflyer_launches( app_data, device_data )
        util.execute_request(**req)
        

        
    
    # please comment below 3 line for update

    for (i,j) in zip(Event_name.get('event_name'),event_data):

        req=appsflyer_inapps( app_data, device_data, eventname= i, eventvalue= j.get(i))
        util.execute_request(**req)

    # eve = ''
    # eve_val = {}
    # req=appsflyer_inapps( app_data, device_data, eventname= eve, eventvalue= eve_val)
    # util.execute_request(**req)








    if random.randint(1,10)>6:
        #print 'launches'
        req  = appsflyer_launches( app_data, device_data )
        util.execute_request(**req)
        
   

    return {'status':True}


# ================== Open Begins ======================
def open( app_data, device_data, day=1 ):
    if app_data.get('times'):
        # print 'launches'
        req  = appsflyer_launches( app_data, device_data )
        util.execute_request(**req)








    return {'status':True}





# ================= Supportive Functions ===================



def get_ua(device_data):
    if int(device_data.get("sdk")) >=19:
        return 'Dalvik/2.1.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'
    else:
        return 'Dalvik/1.6.0 (Linux; U; Android '+device_data.get('os_version')+'; '+device_data.get('model')+' Build/'+device_data.get('build')+')'

def def_(app_data, paramName):
    if not app_data.get(paramName):
        app_data[paramName] = 0

def inc_(app_data, paramName):
    def_(app_data, paramName)
    app_data[paramName] = int(app_data.get(paramName)) + 1

def update_event_records( app_data, eventName, eventValue, af_timestamp):
    app_data['prev_event'] = {"prev_event_timestamp":af_timestamp, "prev_event_value":eventValue,"prev_event_name":eventName}

def sleep_interval(t1=0, t2=10):
    ''
    time.sleep(random.randint(t1, t2))

def set_batteryLevel(app_data):
    app_data['btch'] = random.choice(["no","ac","usb"])
    if app_data.get('btch') in ["ac", "usb"]:
        if app_data.get('batterylevel') > 10 and app_data.get('batterylevel') < 100:
            app_data['batterylevel'] += random.randint(1, 5)
        else:
            app_data['batterylevel'] = random.randint(10,100)
            app_data['btl']=str(app_data.get('batterylevel'))+".0"
    elif not app_data.get('batterylevel'):
        app_data['batterylevel'] = random.randint(10,100)
        app_data['btl']=str(app_data.get('batterylevel'))+".0"

def set_deviceData(app_data):
    if not app_data.get('deviceData'):
        app_data['dim_size']=str(random.randint(1,2))
        app_data['deviceData'] = True

def get_kef_data(app_data, data):
    if data.get('deviceData').get('sensors'):
        app_data["sensorCount"] = str(len(data.get('deviceData').get('sensors')))
    elif not app_data.get("sensorCount"):
        app_data["sensorCount"] = str(random.randint(3, 10))


    batteryTemp = random.choice(["300", "290", "280", "270"])

    kefKey = get_key_half(data.get('brand'), data.get('sdk'), data["lang"], data["af_timestamp"], buildnumber=campaign_data.get("appsflyer").get("buildnumber"))

    kefVal = generateValue(b=batteryTemp, x="0", s=app_data.get("sensorCount"), p=str(len(data.keys())), ts=data["af_timestamp"], fl=data["firstLaunchDate"], buildnumber=campaign_data.get("appsflyer").get("buildnumber"))

    return "kef" + kefKey, kefVal




def conversion_meta(app_data):
    data_l = {}
    if conversion_data.get('meta').get('first_launch'):
        data_l['first_launch'] = {}
    if conversion_data.get('meta').get('first_launch').get('start_with'):
        data_l['first_launch']['start_with'] = "application"
        
    if conversion_data.get('meta').get('first_launch').get('from_fg'):
        data_l['first_launch']['from_fg'] = random.randint(100,700)
    if conversion_data.get('meta').get('first_launch').get('init_to_fg'):
        data_l['first_launch']['init_to_fg'] = random.randint(100,600)

    fg = random.randint(100,700)
    delay = random.randint(100,700)
    if Appsflyer_data.get('dlsdk'):
        data_l['ddl'] =  {
                "from_fg": fg,
                "net": [
               fg+random.randint(10,20) ,
                0
                ],
                "rfr_wait": random.randint(2,10),
                "status": "NOT_FOUND",
                "timeout_value": random.randint(1,4)*1000
            }
    if Appsflyer_data.get('cdn'):
        data_l["rc"] = {
            "c_ver": app_data.get('cdn_ver'),
            "cdn_cache_status": "HIT",
            "cdn_token": app_data.get('cdn_token'),
            "delay": delay,
            "latency": delay-random.randint(5,50),
            "res_code": 200,
            "sig": "success"
        }

    return data_l




def def_sec(app_data,device_data):  
    if not app_data.get('sec'):
        timez = device_data.get('timezone')
        sec = (abs(int(timez))/100)*3600+(abs(int(timez))%100)*60
        if int(timez) < 0:
            sec = (-1) * sec
        app_data['sec'] = sec
        
    return app_data.get('sec')

# ==================== CKSM Genrator ====================
def insert_char(s, p, r):
    return s[:p]+r+s[p:]

def change_char(s, p, r):
    return s[:p]+r+s[p+1:]
        
def md5(data, radix=16):
    import hashlib
    md5_obj = hashlib.md5()
    md5_obj.update(data)
    if radix == 16:
        return md5_obj.hexdigest()
    elif radix == 64:
        return util.base64(md5_obj.digest())

def sha256(data):
    sha_obj = hashlib.sha256()
    sha_obj.update(data)
    return sha_obj.hexdigest()

def cksm_v1( ins_date, ins_time ):
    pk = campaign_data.get("package_name").split('.')
    pk[0], pk[-1] = pk[-1], pk[0]
    pkn='.'.join(pk)
    string = pkn+(campaign_data.get("package_name"))*2+"true"+ins_date+ins_time
    # print "========cksm_v1========"
    # print string
    # print "======================="
    sha256_result = sha256(string)  
    md5_result = md5(sha256_result) 
        
    n = ins_time
    sb = md5_result
    n4 = 0

    sb = change_char(sb,17,'f')
    sb = change_char(sb,27,'f')

    for i in range(0,len(str(n))):
        n4 += int(str(n)[i])
        

    insert1 = list('{:02x}'.format(n4))
    sb = change_char(sb,7,insert1[0])
    sb = change_char(sb,8,insert1[1])
            
    j = 0
    n6 = 77
    n3 = 0
    for i in range(0,len(str(sb))):
        n3 += int(str(sb)[i],36)
        
    if n3>100:
        n8 = 90
        n3%=100

    if n3<10:
        n3 = '0'+str(n3)
        
    sb = insert_char(sb,23,str(n3))
    return sb

# ==================== KEF Genrator ====================
global temp, counter
temp = 0
counter = 1

def get_key_half(brand, sdk, lang, af_ts, buildnumber):
    if buildnumber=="4.8.18":
        return getKeyHalf_4_8_18(sdk, lang.decode('utf-8'), af_ts)
    else:
        c = 'u0000'
        obj2 = af_ts[::-1]
        out = calculate(sdk, obj2, brand)
        i = len(out)
        if i > 4:
            i2 = globals()['counter']+65
            globals()['temp'] = i2 % 128
            if (1 if i2 % 2 != 0 else None) != 1:
                out.delete(4, i)
            else:
                out.delete(5, i)
        else:
            while i < 4:
                i3 = globals()['counter'] + 119
                globals()['temp'] = i3 % 128
                if (16 if i3 % 2 != 0 else 93) != 16:
                    i += 1
                    out+='1'
                else:
                    i += 66
                    out+='H'
                i3 = globals()['counter'] + 109
                globals()['temp'] = i3 % 128
                i3 %= 2
        return out.__str__()

def getKeyHalf_4_8_18(sdk, lang, ts):
    ts = ts[::-1]
    appends = [sdk,lang,ts]
    lengths = [len(sdk),len(lang),len(ts)]
    l = sorted(lengths)
    least = l[0]
    out = ""
    for x in range(least):
        numb = None
        for y in range(len(appends)):
            charAt = ord(appends[y][x])
            if numb:
                charAt = int((charAt ^ int(numb)))

            numb = charAt
            
        out+=str(hex(numb))[2:]

    if len(out)>4:
        out = out[:4]
    elif len(out)<4:
        while len(out)<4:
            out+="1"

    return out

def generateValue(b,x,s,p,ts,fl,buildnumber):
    part1=generateValue1get(ts,fl,buildnumber)
    str_ = bytearray("b"+b+"&x"+x+"&s"+s+"&p"+p, "utf-8")
    for i in range(len(str_)):
        str_[i] = int((str_[i] ^ ((i % 2) + 42)))

    stringBuilder = ""
    for toHexString in str_:
        toHexString2 = str(hex(int(toHexString)))[2:]
        if 1 == len(toHexString2):
            toHexString2 = "0"+str(toHexString2)
        stringBuilder+=toHexString2
    part2 = stringBuilder.__str__()
    return part1+part2

def generateValue1get(ts, firstLaunch, buildnumber):
    gethash = sha256(ts+firstLaunch+buildnumber)
    return gethash[:16]

def calculate(sdk, obj, brand):
    allList = [sdk, obj, brand]
    arrayList = []
    i = 0
    while True:
        flag = 1
        if i >= 3:
            break
        i2 = globals()['temp'] + 63
        globals()['counter'] = i2 % 128
        if i2 % 2 != 0:
            flag = None
        if flag != None:
            arrayList.append(len(allList[i]))
            i += 31
        else:
            arrayList.append(len(allList[i]))
            i += 1
    sorted(arrayList)
    intValue = int(arrayList[0])
    out = ""
    i3 = 0
    while i3 < intValue:
        i4 = globals()['counter']+99
        globals()['temp'] = i4 % 128
        i5 =  globals()['temp']+67
        globals()['counter'] = i5 % 128
        i5 %= 2
        number = None
        if i4 % 2 != 0:
            a = 57
        else:
            a = 83
        if a != 83:
            i4 = 1
        else:
            i4 = 0
        while i4 < 3:
            i5 = globals()['temp'] + 87
            globals()['counter'] = i5 % 128
            i5 %= 2
            i5 = ord(allList[i4][i3])
            if (92 if number == None else 39) != 39:
                i6 = globals()['temp'] + 55
                globals()['counter'] = i6 % 128
                i6 %= 2
                i6 = globals()['counter'] + 39
                globals()['temp'] = i6 % 128
                i6 %= 2
            else:
                i5 ^= int(number)
            number = i5
            i4 += 1
        out+=str(hex(int(number)))[2:]
        i3 += 1
    return out

def get_country():
    weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
    country_list     = [val for val, cnt in weighted_choices for i in range(cnt)]
    return random.choice(country_list)

def get_retention(day):
    return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0


