#-*- coding: utf-8 -*-
# created by him@nshu
import sys
if int(sys.version_info.major) == 2:
    py_version = 2
else:
    py_version = 3
    import urllib.parse as urlparse
from sdk import installtimenew, util
import time, random, json, string, urllib, uuid, hashlib
from spicipher import af_cipher,get_dlsdk_af_sig,get_cdn_token,get_gcd_sdk_af_sig
false = False
from datetime import datetimetrue = True
null = None


campaign_data = {
   'package_name' : 'com.julofinance.juloapp',
   'app_name' : 'JULO Dana Online & Pinjaman',
   'app_version_name' : '7.6.2',
   'app_version_code' : '2308',
   'supported_countries' : 'WW',
   'supported_os' : '8',
