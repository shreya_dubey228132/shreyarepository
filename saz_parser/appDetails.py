App_data = {
    'com.acciona.mobility.app' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "08 Jun 2022",
        'App_name' : "ACCIONA Mobility - Motosharing",
        'Package_name' : "com.acciona.mobility.app",
        'installs' : "500,000+",
        'version' : "1.31.0",
    },
    'com.aktifbank.nkolay' :
    {
        'category' : "Finance",
        'updated' : "21 May 2022",
        'App_name' : "N Kolay",
        'Package_name' : "com.aktifbank.nkolay",
        'installs' : "500,000+",
        'version' : "Varies with device",
    },
    'com.scopely.yux' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "YAHTZEE With Buddies Dice Game",
        'Package_name' : "com.scopely.yux",
        'installs' : "10,000,000+",
        'version' : "8.15.1",
    },
    'com.dreamgames.royalmatch' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Royal Match",
        'Package_name' : "com.dreamgames.royalmatch",
        'installs' : "10,000,000+",
        'version' : "9106",
    },
    '1442764361' :
    {
        'category' : "Finance",
        'updated' : "2022-06-09T19:39:49Z",
        'App_name' : "Koinal: Buy Bitcoin instantly",
        'Package_name' : "com.koinal.ios",
        'version' : "1.3.6",
        'minimumOsVersion' : "11.1",
    },
    'com.armada.riva' :
    {
        'category' : "Shopping",
        'updated' : "26 May 2022",
        'App_name' : "Riva Fashion",
        'Package_name' : "com.armada.riva",
        'installs' : "100,000+",
        'version' : "5.8",
    },
    'com.btcturk.pro' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "BtcTurk | PRO: USDT|SHIB|AVAX",
        'Package_name' : "com.btcturk.pro",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.softtech.parakod' :
    {
        'category' : "Finance",
        'updated' : "05 Jun 2022",
        'App_name' : "Maximum Mobil",
        'Package_name' : "com.softtech.parakod",
        'installs' : "5,000,000+",
        'version' : "4.2.6",
    },
    'jp.konami.duellinks' :
    {
        'category' : "Games",
        'updated' : "11 May 2022",
        'App_name' : "Yu-Gi-Oh! Duel Links",
        'Package_name' : "jp.konami.duellinks",
        'installs' : "50,000,000+",
        'version' : "6.7.0",
    },
    'io.gosats' :
    {
        'category' : "Lifestyle",
        'updated' : "17 Feb 2022",
        'App_name' : "GoSats - Bitcoin Rewards App",
        'Package_name' : "io.gosats",
        'installs' : "100,000+",
        'version' : "2.1.4",
    },
    'com.ada.ADAEXPRESS' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "01 Jun 2022",
        'App_name' : "Ada Mobilit\u00e9s Location voiture, utilitaire, camion",
        'Package_name' : "com.ada.ADAEXPRESS",
        'installs' : "100,000+",
        'version' : "12.1.0",
    },
    'com.growthunit.palace' :
    {
        'category' : "Social and Communication",
        'updated' : "23 Nov 2021",
        'App_name' : "\ud330\ub9ac\uc2a4 - \uacbd\uc81c\ub825 \uc778\uc99d \uc18c\uac1c\ud305\uc571",
        'Package_name' : "com.growthunit.palace",
        'installs' : "10,000+",
        'version' : "1.5.35",
    },
    'com.akinon.dsdamat' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "Ds Damat",
        'Package_name' : "com.akinon.dsdamat",
        'installs' : "50,000+",
        'version' : "2.2.0",
    },
    'br.com.bancobmg.bancodigital.atletico' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Galo Bmg : banco digital",
        'Package_name' : "br.com.bancobmg.bancodigital.atletico",
        'installs' : "500,000+",
        'version' : "3.37.0-atletico",
    },
    'com.gopaysense.android.boost' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "PaySense: Personal Loan App",
        'Package_name' : "com.gopaysense.android.boost",
        'installs' : "5,000,000+",
        'version' : "3.9.4",
    },
    'com.mobiletoong.travelwallet' :
    {
        'category' : "Finance",
        'updated' : "27 May 2022",
        'App_name' : "\ud2b8\ub798\ube14\uc6d4\ub81b \u2013 TravelPay",
        'Package_name' : "com.mobiletoong.travelwallet",
        'installs' : "100,000+",
        'version' : "2.3.6",
    },
    'com.kreditbee.android' :
    {
        'category' : "Finance",
        'updated' : "26 May 2022",
        'App_name' : "KreditBee: Instant Flexi Loan",
        'Package_name' : "com.kreditbee.android",
        'installs' : "10,000,000+",
        'version' : "1.6.7",
    },
    'com.flowwow' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "Flowwow \u2013 Reliable delivery",
        'Package_name' : "com.flowwow",
        'installs' : "500,000+",
        'version' : "3.4.7",
    },
    'chat.meme.inke' :
    {
        'category' : "Social and Communication",
        'updated' : "17 Jun 2022",
        'App_name' : "MeMe Live -Live, Chat, Stream",
        'Package_name' : "chat.meme.inke",
        'installs' : "10,000,000+",
        'version' : "4.6.4",
    },
    'com.crazylabs.soap.cutting' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Soap Cutting - Satisfying ASMR",
        'Package_name' : "com.crazylabs.soap.cutting",
        'installs' : "50,000,000+",
        'version' : "3.8.6.0",
    },
    'com.zynga.farmville3' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "FarmVille 3 \u2013 Farm Animals",
        'Package_name' : "com.zynga.farmville3",
        'installs' : "5,000,000+",
        'version' : "1.16.26778",
    },
    'com.Gameberry.ColorCity' :
    {
        'category' : "Games",
        'updated' : "22 Sep 2020",
        'App_name' : "Wild Driver",
        'Package_name' : "com.Gameberry.ColorCity",
        'installs' : "5,000+",
        'version' : "2.1",
    },
    'com.cle.dy.soulseeker6' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "Soul Seeker: Six Knights",
        'Package_name' : "com.cle.dy.soulseeker6",
        'installs' : "500,000+",
        'version' : "1.4.804",
    },
    'com.eyougame.msn' :
    {
        'category' : "Games",
        'updated' : "24 Mar 2022",
        'App_name' : "Lost in Paradise:Waifu Connect",
        'Package_name' : "com.eyougame.msn",
        'installs' : "100,000+",
        'version' : "1.1.0.00710005",
    },
    'ru.lenta.lentochka' :
    {
        'category' : "Food & Drink",
        'updated' : "31 May 2022",
        'App_name' : "\u041b\u0435\u043d\u0442\u0430 \u041e\u043d\u043b\u0430\u0439\u043d \u2013 \u0417\u0430\u043a\u0430\u0437 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u043e\u0432",
        'Package_name' : "ru.lenta.lentochka",
        'installs' : "1,000,000+",
        'version' : "5.19.1",
    },
    'com.nexon.axe' :
    {
        'category' : "Games",
        'updated' : "25 Feb 2022",
        'App_name' : "\uc561\uc2a4(AxE)",
        'Package_name' : "com.nexon.axe",
        'installs' : "1,000,000+",
        'version' : "4.2.5",
    },
    'com.deemedyainc.duels' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "Duels: Epic Fighting PVP Game",
        'Package_name' : "com.deemedyainc.duels",
        'installs' : "1,000,000+",
        'version' : "1.12.1",
    },
    '390946943' :
    {
        'category' : "Entertainment",
        'updated' : "2022-05-23T09:12:43Z",
        'App_name' : "LOTTO & Eurojackpot spielen",
        'Package_name' : "lotto6aus49",
        'version' : "5.13.6",
        'minimumOsVersion' : "13.0",
    },
    'com.gravity.rod' :
    {
        'category' : "Games",
        'updated' : "17 Dec 2021",
        'App_name' : "RO\u4ed9\u5883\u50b3\u8aaa\uff1a\u6211\u7684\u6230\u8853",
        'Package_name' : "com.gravity.rod",
        'installs' : "100,000+",
        'version' : "5.6.0",
    },
    'com.eni.enilive' :
    {
        'category' : "Finance",
        'updated' : "31 May 2022",
        'App_name' : "Eni Live",
        'Package_name' : "com.eni.enilive",
        'installs' : "500,000+",
        'version' : "1.65",
    },
    'com.obilet.androidside' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "10 Jun 2022",
        'App_name' : "obilet U\u00e7ak, Otob\u00fcs Bileti",
        'Package_name' : "com.obilet.androidside",
        'installs' : "1,000,000+",
        'version' : "15.0.14",
    },
    'jp.colopl.dpuzzle' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "\u30c7\u30a3\u30ba\u30cb\u30fc \u30c4\u30e0\u30c4\u30e0\u30e9\u30f3\u30c9",
        'Package_name' : "jp.colopl.dpuzzle",
        'installs' : "1,000,000+",
        'version' : "1.4.102",
    },
    'com.Level5.FantasyLifeOnline' :
    {
        'category' : "Games",
        'updated' : "06 Dec 2021",
        'App_name' : "\u30d5\u30a1\u30f3\u30bf\u30b8\u30fc\u30e9\u30a4\u30d5 \u30aa\u30f3\u30e9\u30a4\u30f3",
        'Package_name' : "com.Level5.FantasyLifeOnline",
        'installs' : "500,000+",
        'version' : "1.9.81",
    },
    'com.lightinthebox.android' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "LightInTheBox Online Shopping",
        'Package_name' : "com.lightinthebox.android",
        'installs' : "10,000,000+",
        'version' : "8.9.1",
    },
    'sa.gov.apps.sauditourism' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "13 May 2022",
        'App_name' : "Visit Saudi - \u0631\u0648\u062d \u0627\u0644\u0633\u0639\u0648\u062f\u064a\u0629",
        'Package_name' : "sa.gov.apps.sauditourism",
        'installs' : "100,000+",
        'version' : "5.1.8",
    },
    'com.erosnow' :
    {
        'category' : "Entertainment",
        'updated' : "22 Jul 2021",
        'App_name' : "Eros Now - Movies, Originals, Music & TV Shows",
        'Package_name' : "com.erosnow",
        'installs' : "10,000,000+",
        'version' : "4.6.2",
    },
    'droidhang.twgame.restaurant' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "Animal Restaurant",
        'Package_name' : "droidhang.twgame.restaurant",
        'installs' : "10,000,000+",
        'version' : "9.8",
    },
    '336698281' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-10T16:56:28Z",
        'App_name' : "Realtor.com Real Estate",
        'Package_name' : "com.move.Realtor",
        'version' : "15.14.0",
        'minimumOsVersion' : "14.0",
    },
    'in.hirect' :
    {
        'category' : "Finance",
        'updated' : "28 May 2022",
        'App_name' : "Hirect: Chat Based Job Search",
        'Package_name' : "in.hirect",
        'installs' : "5,000,000+",
        'version' : "3.1.5",
    },
    '1483475992' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-05-30T02:32:19Z",
        'App_name' : "by.U ID",
        'Package_name' : "com.byu.id",
        'version' : "1.35.2",
        'minimumOsVersion' : "11.0",
    },
    'com.appatomic.vpnhub' :
    {
        'category' : "Tools",
        'updated' : "03 Jun 2022",
        'App_name' : "VPNhub: Unlimited & Secure",
        'Package_name' : "com.appatomic.vpnhub",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.douglas.main' :
    {
        'category' : "Lifestyle",
        'updated' : "31 May 2022",
        'App_name' : "Douglas \u2013 Parf\u00fcm & Kosmetik",
        'Package_name' : "com.douglas.main",
        'installs' : "1,000,000+",
        'version' : "10.6.0",
    },
    'id.extramarks.learningapp' :
    {
        'category' : "Education and Books",
        'updated' : "09 Jun 2022",
        'App_name' : "Kelas Pintar - Solusi Belajar Online",
        'Package_name' : "id.extramarks.learningapp",
        'installs' : "1,000,000+",
        'version' : "3.3.6",
    },
    'com.finwizard.myway' :
    {
        'category' : "Finance",
        'updated' : "06 Jun 2022",
        'App_name' : "Finity: Stocks, Direct MF, ETF",
        'Package_name' : "com.finwizard.myway",
        'installs' : "500,000+",
        'version' : "15.4",
    },
    'jp.sukedachi.android' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "\u52a9\u592a\u5200",
        'Package_name' : "jp.sukedachi.android",
        'installs' : "50,000+",
        'version' : "6.7.6",
    },
    '1448993473' :
    {
        'category' : "Games",
        'updated' : "2022-03-08T09:35:29Z",
        'App_name' : "Zero21 Solitaire",
        'Package_name' : "com.copilotgames.Zero21",
        'version' : "2.37",
        'minimumOsVersion' : "10.0",
    },
    'jp.mizzi.akbw' :
    {
        'category' : "Games",
        'updated' : "16 May 2022",
        'App_name' : "[AKB48\u516c\u5f0f] AKB48 World",
        'Package_name' : "jp.mizzi.akbw",
        'installs' : "50,000+",
        'version' : "1.09.004",
    },
    'com.appkarma.app' :
    {
        'category' : "Entertainment",
        'updated' : "21 Apr 2022",
        'App_name' : "appKarma Rewards & Gift Cards",
        'Package_name' : "com.appkarma.app",
        'installs' : "5,000,000+",
        'version' : "4.0.12",
    },
    'com.pinelabs.epos' :
    {
        'category' : "Finance",
        'updated' : "19 May 2022",
        'App_name' : "Pine Labs AllTap",
        'Package_name' : "com.pinelabs.epos",
        'installs' : "1,000,000+",
        'version' : "2.02.02",
    },
    'com.pocketgems.android.dragon' :
    {
        'category' : "Games",
        'updated' : "18 May 2022",
        'App_name' : "War Dragons",
        'Package_name' : "com.pocketgems.android.dragon",
        'installs' : "10,000,000+",
        'version' : "7.00+gn",
    },
    'com.valofe.icarusm.na' :
    {
        'category' : "Games",
        'updated' : "21 Apr 2022",
        'App_name' : "Icarus M: Riders of Icarus",
        'Package_name' : "com.valofe.icarusm.na",
        'installs' : "100,000+",
        'version' : "Varies with device",
    },
    'com.inv.idleprincess.re' :
    {
        'category' : "Games",
        'updated' : "29 Apr 2021",
        'App_name' : "\uc544\uc774\ub4e4\ud504\ub9b0\uc138\uc2a4(Idle Princess)",
        'Package_name' : "com.inv.idleprincess.re",
        'installs' : "10,000+",
        'version' : "1.4.41",
    },
    '909302093' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-12T20:07:24Z",
        'App_name' : "Modanisa: Online Fashion Shop",
        'Package_name' : "com.modanisa.iPhone",
        'version' : "2.8.167",
        'minimumOsVersion' : "12.1",
    },
    'com.axlebolt.standoff2' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "Standoff 2",
        'Package_name' : "com.axlebolt.standoff2",
        'installs' : "50,000,000+",
        'version' : "0.19.2",
    },
    '1473570977' :
    {
        'category' : "Games",
        'updated' : "2020-03-22T03:11:37Z",
        'App_name' : "Repair - \u6687\u3064\u3076\u3057 \u30d1\u30ba\u30eb \u30b2\u30fc\u30e0",
        'Package_name' : "com.repair.games",
        'version' : "1.0.7",
        'minimumOsVersion' : "9.0",
    },
    'com.yy.hiyo' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "Hago- Party, Chat & Games",
        'Package_name' : "com.yy.hiyo",
        'installs' : "100,000,000+",
        'version' : "5.1.2",
    },
    '329472759' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-03T07:52:23Z",
        'App_name' : "Thuisbezorgd.nl",
        'Package_name' : "citymealprofile",
        'version' : "34.30.1",
        'minimumOsVersion' : "14.0",
    },
    'com.appstailors.berain' :
    {
        'category' : "Food & Drink",
        'updated' : "06 Apr 2022",
        'App_name' : "Berain Water Delivery",
        'Package_name' : "com.appstailors.berain",
        'installs' : "500,000+",
        'version' : "3.7.0077",
    },
    'jp.co.bandainamcogames.NBGI0197' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "\u30c6\u30a4\u30eb\u30ba \u30aa\u30d6 \u30a2\u30b9\u30bf\u30ea\u30a2",
        'Package_name' : "jp.co.bandainamcogames.NBGI0197",
        'installs' : "1,000,000+",
        'version' : "6.14.0",
    },
    'br.com.bradesco.next' :
    {
        'category' : "Finance",
        'updated' : "18 Jun 2022",
        'App_name' : "Banco next: Conta e Cart\u00e3o",
        'Package_name' : "br.com.bradesco.next",
        'installs' : "10,000,000+",
        'version' : "30.29.1",
    },
    'com.disgaearpg.forwardworks' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "\u9b54\u754c\u6226\u8a18\u30c7\u30a3\u30b9\u30ac\u30a4\u30a2RPG \uff5e\u6700\u51f6\u9b54\u738b\u6c7a\u5b9a\u6226\uff01\uff5e",
        'Package_name' : "com.disgaearpg.forwardworks",
        'installs' : "500,000+",
        'version' : "3.4.2",
    },
    '1483025097' :
    {
        'category' : "Games",
        'updated' : "2022-06-18T06:31:15Z",
        'App_name' : "Rise of Kingdoms \u2015\u4e07\u56fd\u899a\u9192\u2015",
        'Package_name' : "com.lilithgames.rok.ios.jp",
        'version' : "1.0.59.21",
        'minimumOsVersion' : "10.0",
    },
    'com.kobobooks.android' :
    {
        'category' : "Education and Books",
        'updated' : "09 Jun 2022",
        'App_name' : "Kobo Books - eBooks & Audiobooks",
        'Package_name' : "com.kobobooks.android",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.itau.aco' :
    {
        'category' : "Finance",
        'updated' : "12 Mar 2021",
        'App_name' : "Banco Ita\u00fa Abreconta",
        'Package_name' : "com.itau.aco",
        'installs' : "5,000,000+",
        'version' : "3.0.0",
    },
    '551367321' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-16T07:37:41Z",
        'App_name' : "eDreams: Cheap Flight Tickets",
        'Package_name' : "com.edreams.flights",
        'version' : "4.272",
        'minimumOsVersion' : "14.0",
    },
    'com.shiprocket.shiprocket' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Shiprocket - Courier Delivery",
        'Package_name' : "com.shiprocket.shiprocket",
        'installs' : "1,000,000+",
        'version' : "5.1.8",
    },
    'com.digging.qtk.gp' :
    {
        'category' : "Games",
        'updated' : "30 Sep 2021",
        'App_name' : "\u4e09\u570b\u840c\u5c07\u50b3",
        'Package_name' : "com.digging.qtk.gp",
        'installs' : "10,000+",
        'version' : "2.8.2",
    },
    'com.crazyfruitcrush.gp' :
    {
        'category' : "Games",
        'updated' : "13 Apr 2022",
        'App_name' : "Crazy Fruit Crush",
        'Package_name' : "com.crazyfruitcrush.gp",
        'installs' : "500,000+",
        'version' : "Varies with device",
    },
    'com.fecredit.RoboLending' :
    {
        'category' : "Finance",
        'updated' : "11 May 2022",
        'App_name' : "FE $NAP: CashLoan & CreditCard",
        'Package_name' : "com.fecredit.RoboLending",
        'installs' : "1,000,000+",
        'version' : "2.8.3",
    },
    'net.whow.lounge777' :
    {
        'category' : "Casino",
        'updated' : "10 Jun 2022",
        'App_name' : "Lounge777 - Online Casino",
        'Package_name' : "net.whow.lounge777",
        'installs' : "100,000+",
        'version' : "4.13.36",
    },
    'com.customer.laan' :
    {
        'category' : "Health and Fitness",
        'updated' : "08 Jun 2022",
        'App_name' : "\u0644\u0627\u0646 \u0643\u064a\u0631 | Laan Care",
        'Package_name' : "com.customer.laan",
        'installs' : "10,000+",
        'version' : "1.5.6",
    },
    'com.nike.omega' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Nike",
        'Package_name' : "com.nike.omega",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.garena.game.kgvn' :
    {
        'category' : "Games",
        'updated' : "25 Apr 2022",
        'App_name' : "Garena Li\u00ean Qu\u00e2n Mobile",
        'Package_name' : "com.garena.game.kgvn",
        'installs' : "50,000,000+",
        'version' : "1.45.1.7",
    },
    'dev.song.kiem.loan.vu' :
    {
        'category' : "Games",
        'updated' : "24 May 2022",
        'App_name' : "Song Ki\u1ebfm Lo\u1ea1n V\u0169",
        'Package_name' : "dev.song.kiem.loan.vu",
        'installs' : "500,000+",
        'version' : "1.1.2",
    },
    '1568079688' :
    {
        'category' : "Finance",
        'updated' : "2022-05-28T09:17:43Z",
        'App_name' : "Poda",
        'Package_name' : "com.magna.poda",
        'version' : "4.3",
        'minimumOsVersion' : "12.0",
    },
    'com.odamax' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "06 Jun 2022",
        'App_name' : "Odamax - Hotel Reservation",
        'Package_name' : "com.odamax",
        'installs' : "500,000+",
        'version' : "2.1.9",
    },
    'rocketsingh.app.fintech' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "Rocket Singh App : Kamate raho",
        'Package_name' : "rocketsingh.app.fintech",
        'installs' : "50,000+",
        'version' : "1.14",
    },
    'com.nrxws.google.tw' :
    {
        'category' : "Games",
        'updated' : "13 Sep 2020",
        'App_name' : "\u653e\u7f6e\u4e09\u570b\u5fd7-\u795e\u9b54\u5a01\u5c07",
        'Package_name' : "com.nrxws.google.tw",
        'installs' : "50,000+",
        'version' : "1.0.6",
    },
    'burgerking.id.android' :
    {
        'category' : "Food & Drink",
        'updated' : "08 Jun 2022",
        'App_name' : "Burger King Indonesia",
        'Package_name' : "burgerking.id.android",
        'installs' : "1,000,000+",
        'version' : "2.7.2",
    },
    'com.clearchannel.iheartradio.controller' :
    {
        'category' : "Entertainment",
        'updated' : "18 May 2022",
        'App_name' : "iHeart: Music, Radio, Podcasts",
        'Package_name' : "com.clearchannel.iheartradio.controller",
        'installs' : "50,000,000+",
        'version' : "Varies with device",
    },
    'com.puding.deathknight' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "\ub370\uc2a4\ub098\uc774\ud2b8 \ud0a4\uc6b0\uae30 : \ubc29\uce58\ud615 RPG \ud0a4\uc6b0\uae30 \uac8c\uc784",
        'Package_name' : "com.puding.deathknight",
        'installs' : "500,000+",
        'version' : "1.3.15341",
    },
    'jp.co.matchingagent.cocotsure' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "\u30bf\u30c3\u30d7\u30eb-\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea\u3067\u604b\u6d3b\u30fb\u5a5a\u6d3b\u30fb\u51fa\u4f1a\u3044\u63a2\u3057",
        'Package_name' : "jp.co.matchingagent.cocotsure",
        'installs' : "1,000,000+",
        'version' : "8.1.13",
    },
    'com.netease.mrzhna' :
    {
        'category' : "Games",
        'updated' : "29 Apr 2022",
        'App_name' : "LifeAfter",
        'Package_name' : "com.netease.mrzhna",
        'installs' : "10,000,000+",
        'version' : "1.0.214",
    },
    '1192730343' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-17T07:09:39Z",
        'App_name' : "Le Club Leader Price",
        'Package_name' : "fr.leaderprice.leaderprice",
        'version' : "5.6.5",
        'minimumOsVersion' : "12.0",
    },
    'com.lhy.ssjj' :
    {
        'category' : "Games",
        'updated' : "27 Oct 2020",
        'App_name' : "\u4e00\u528d\u50be\u5fc3-\u5168\u65b0\u8077\u696d",
        'Package_name' : "com.lhy.ssjj",
        'installs' : "100,000+",
        'version' : "1.1.8.0",
    },
    'com.gamebeans.tdj' :
    {
        'category' : "Games",
        'updated' : "11 May 2022",
        'App_name' : "\u5929\u5730\u52ab",
        'Package_name' : "com.gamebeans.tdj",
        'installs' : "100,000+",
        'version' : "Varies with device",
    },
    'com.ncsoft.lineage2mtw' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "\u5929\u58022M",
        'Package_name' : "com.ncsoft.lineage2mtw",
        'installs' : "100,000+",
        'version' : "2.0.42",
    },
    'com.ksrgland.goat' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "King's Throne: Royal Delights",
        'Package_name' : "com.ksrgland.goat",
        'installs' : "1,000,000+",
        'version' : "1.3.149",
    },
    'gas.dung.com.gas24h' :
    {
        'category' : "Shopping",
        'updated' : "27 May 2022",
        'App_name' : "Gas24h",
        'Package_name' : "gas.dung.com.gas24h",
        'installs' : "500,000+",
        'version' : "22.5.25",
    },
    'com.gmt.doccocuukiemkimdung' :
    {
        'category' : "Games",
        'updated' : "26 Jan 2022",
        'App_name' : "\u0110\u1ed9c C\u00f4 K\u1ef3 Hi\u1ec7p: Thi\u1ebfu L\u00e2m Ma",
        'Package_name' : "com.gmt.doccocuukiemkimdung",
        'installs' : "100,000+",
        'version' : "7.0",
    },
    'com.acfantastic.moreinlive' :
    {
        'category' : "Social and Communication",
        'updated' : "16 Jun 2022",
        'App_name' : "MoreinLive",
        'Package_name' : "com.acfantastic.moreinlive",
        'installs' : "1,000,000+",
        'version' : "5.8.6",
    },
    '1249825694' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-04-05T07:25:01Z",
        'App_name' : "Numero: Second Phone Number",
        'Package_name' : "com.kalam.numero",
        'version' : "5.7.1",
        'minimumOsVersion' : "11.0",
    },
    'com.shawarmer' :
    {
        'category' : "Food & Drink",
        'updated' : "06 Dec 2021",
        'App_name' : "Shawarmer",
        'Package_name' : "com.shawarmer",
        'installs' : "100,000+",
        'version' : "2.9",
    },
    'com.gaea.g.sgjzcq' :
    {
        'category' : "Games",
        'updated' : "02 Jul 2020",
        'App_name' : "The Elder Scrolls: Legends Asia",
        'Package_name' : "com.gaea.g.sgjzcq",
        'installs' : "10,000+",
        'version' : "1.2.1",
    },
    'jp.co.amutus.mechacomic.android.mangaapp' :
    {
        'category' : "Education and Books",
        'updated' : "30 May 2022",
        'App_name' : "\u3081\u3061\u3083\u30b3\u30df\u30c3\u30af\u306e\u6bce\u65e5\u9023\u8f09\u30de\u30f3\u30ac\u30a2\u30d7\u30ea",
        'Package_name' : "jp.co.amutus.mechacomic.android.mangaapp",
        'installs' : "1,000,000+",
        'version' : "2.18.0",
    },
    'com.egypt.hardeesburger.fastfood' :
    {
        'category' : "Food & Drink",
        'updated' : "21 Feb 2022",
        'App_name' : "Hardee's Egypt - Burger & Sandwich Meals!",
        'Package_name' : "com.egypt.hardeesburger.fastfood",
        'installs' : "100,000+",
        'version' : "5.8.5",
    },
    'com.restar.senkistrike.android' :
    {
        'category' : "Games",
        'updated' : "15 May 2020",
        'App_name' : "\u6226\u59eb\u30b9\u30c8\u30e9\u30a4\u30af",
        'Package_name' : "com.restar.senkistrike.android",
        'installs' : "100,000+",
        'version' : "1.0.37",
    },
    'com.ziplab.mayhem' :
    {
        'category' : "Games",
        'updated' : "09 Mar 2020",
        'App_name' : "\uba54\uc774\ud5f4\uc758 \uc720\uc0b0 : \ubc29\uce58\ud615 RPG",
        'Package_name' : "com.ziplab.mayhem",
        'installs' : "10,000+",
        'version' : "1.0.22",
    },
    'com.hitmanrebornkr.android' :
    {
        'category' : "Games",
        'updated' : "17 Mar 2022",
        'App_name' : "\uac00\uc815\uad50\uc0ac \ud788\ud2b8\ub9e8 \ub9ac\ubcf8",
        'Package_name' : "com.hitmanrebornkr.android",
        'installs' : "100,000+",
        'version' : "1.1.0",
    },
    'com.MelsoftGames.FamilyIslandFarm' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Family Island\u2122 \u2014 Farming game",
        'Package_name' : "com.MelsoftGames.FamilyIslandFarm",
        'installs' : "10,000,000+",
        'version' : "2022164.0.18084",
    },
    'hemengetir.client.tr' :
    {
        'category' : "Finance",
        'updated' : "26 May 2022",
        'App_name' : "Banabikurye: Courier Delivery App",
        'Package_name' : "hemengetir.client.tr",
        'installs' : "100,000+",
        'version' : "1.62.0",
    },
    'com.mask.minutes3' :
    {
        'category' : "Games",
        'updated' : "02 Mar 2022",
        'App_name' : "3\u5206\u9593\u30df\u30b9\u30c6\u30ea\u30fc - \u6687\u3064\u3076\u3057\u63a8\u7406\u30b2\u30fc\u30e0",
        'Package_name' : "com.mask.minutes3",
        'installs' : "1,000,000+",
        'version' : "1.2.4",
    },
    'ru.getpharma.eapteka' :
    {
        'category' : "Health and Fitness",
        'updated' : "27 May 2022",
        'App_name' : "\u0415\u0410\u041f\u0422\u0415\u041a\u0410 \u2014 \u043e\u043d\u043b\u0430\u0439\u043d \u0430\u043f\u0442\u0435\u043a\u0430",
        'Package_name' : "ru.getpharma.eapteka",
        'installs' : "5,000,000+",
        'version' : "22.5.2",
    },
    'com.rootbridge.ula' :
    {
        'category' : "Shopping",
        'updated' : "09 Jun 2022",
        'App_name' : "Ula - Untung Lancar Aman",
        'Package_name' : "com.rootbridge.ula",
        'installs' : "100,000+",
        'version' : "1.2.24",
    },
    'com.blizzard.wtcg.hearthstone' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "Hearthstone",
        'Package_name' : "com.blizzard.wtcg.hearthstone",
        'installs' : "50,000,000+",
        'version' : "23.4.139963",
    },
    'com.playwith.rohanm.playstore' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "\ub85c\ud55cM",
        'Package_name' : "com.playwith.rohanm.playstore",
        'installs' : "100,000+",
        'version' : "1.4.96",
    },
    'de.dasoertliche.android' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "16 May 2022",
        'App_name' : "Das \u00d6rtliche Telephone Book & Directory Assistance",
        'Package_name' : "de.dasoertliche.android",
        'installs' : "1,000,000+",
        'version' : "8.0.4",
    },
    'com.fungames.sniper3d' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Sniper 3D\uff1aGun Shooting Games",
        'Package_name' : "com.fungames.sniper3d",
        'installs' : "500,000,000+",
        'version' : "3.47.5",
    },
    'com.flyin.bookings' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "22 Mar 2022",
        'App_name' : "Flyin.com - Flights, Hotels & Travel Deals Booking",
        'Package_name' : "com.flyin.bookings",
        'installs' : "1,000,000+",
        'version' : "4.5.6",
    },
    'com.igs.fafafa' :
    {
        'category' : "Casino",
        'updated' : "11 Jun 2022",
        'App_name' : "Golden HoYeah- Casino Slots",
        'Package_name' : "com.igs.fafafa",
        'installs' : "10,000,000+",
        'version' : "3.3.5",
    },
    'jp.co.recruit.majibu' :
    {
        'category' : "Lifestyle",
        'updated' : "24 May 2022",
        'App_name' : "\u82e5\u8005\u9650\u5b9a\u30fc\u611f\u52d5\u4f53\u9a13\u30a2\u30d7\u30ea\u3001\u30de\u30b8\u90e8",
        'Package_name' : "jp.co.recruit.majibu",
        'installs' : "500,000+",
        'version' : "9.1.0",
    },
    'com.funplus.teamup' :
    {
        'category' : "Social and Communication",
        'updated' : "31 May 2021",
        'App_name' : "nicee - \u5b85\u5bb6\u793e\u4ea4\u5fc5\u5099\u795e\u5668",
        'Package_name' : "com.funplus.teamup",
        'installs' : "50,000+",
        'version' : "1.49.1",
    },
    'com.balancehero.truebalance' :
    {
        'category' : "Finance",
        'updated' : "09 Jun 2022",
        'App_name' : "TrueBalance- Personal Loan App",
        'Package_name' : "com.balancehero.truebalance",
        'installs' : "50,000,000+",
        'version' : "6.08.00",
    },
    'com.clovergames.lordofheroes' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "Lord of Heroes: anime games",
        'Package_name' : "com.clovergames.lordofheroes",
        'installs' : "5,000,000+",
        'version' : "1.2.052405",
    },
    '1369317521' :
    {
        'category' : "Games",
        'updated' : "2022-06-13T16:10:24Z",
        'App_name' : "Game of Thrones Slots Casino",
        'Package_name' : "com.zynga.gotslots",
        'version' : "1.1.4824",
        'minimumOsVersion' : "11.0",
    },
    'com.forevernine.texaspoker.android' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Holdem or Foldem - Texas Poker",
        'Package_name' : "com.forevernine.texaspoker.android",
        'installs' : "1,000,000+",
        'version' : "1.5.10",
    },
    'tr.com.littlecaesars' :
    {
        'category' : "Food & Drink",
        'updated' : "28 May 2021",
        'App_name' : "Little Caesars Pizza - Pizza Sipari\u015fi",
        'Package_name' : "tr.com.littlecaesars",
        'installs' : "100,000+",
        'version' : "5.0.5",
    },
    'com.mpl.poolchamps' :
    {
        'category' : "Games",
        'updated' : "17 Mar 2022",
        'App_name' : "Pool Champs by MPL 8 Ball Pool",
        'Package_name' : "com.mpl.poolchamps",
        'installs' : "1,000,000+",
        'version' : "2.2",
    },
    '1205758388' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-05-30T10:47:34Z",
        'App_name' : "Embargo - Loyalty & Rewards",
        'Package_name' : "com.embargoapp.embargo.ios",
        'version' : "7.5.5",
        'minimumOsVersion' : "12.0",
    },
    'br.com.claro.flex' :
    {
        'category' : "Social and Communication",
        'updated' : "17 Jun 2022",
        'App_name' : "Claro flex",
        'Package_name' : "br.com.claro.flex",
        'installs' : "5,000,000+",
        'version' : "1.94.13",
    },
    'com.astrotalk' :
    {
        'category' : "Lifestyle",
        'updated' : "08 Jun 2022",
        'App_name' : "Astrotalk - Talk to Astrologer",
        'Package_name' : "com.astrotalk",
        'installs' : "5,000,000+",
        'version' : "1.1.203",
    },
    '1479696509' :
    {
        'category' : "Finance",
        'updated' : "2022-06-10T03:15:14Z",
        'App_name' : "Tamam",
        'Package_name' : "com.zain.pay",
        'version' : "2.7",
        'minimumOsVersion' : "11.0",
    },
    'com.boyaa.androidmarkettaiyu' :
    {
        'category' : "Casino",
        'updated' : "06 May 2022",
        'App_name' : "\u0e44\u0e1e\u0e48\u0e40\u0e17\u0e47\u0e01\u0e0b\u0e31\u0e2a\u0e42\u0e1a\u0e22\u0e48\u0e32-Texas Poker",
        'Package_name' : "com.boyaa.androidmarkettaiyu",
        'installs' : "5,000,000+",
        'version' : "6.7.0",
    },
    'com.akbank.android.apps.akbank_direkt' :
    {
        'category' : "Finance",
        'updated' : "05 May 2022",
        'App_name' : "Akbank",
        'Package_name' : "com.akbank.android.apps.akbank_direkt",
        'installs' : "10,000,000+",
        'version' : "4.22.1",
    },
    'com.mgs.petner' :
    {
        'category' : "Social and Communication",
        'updated' : "24 Feb 2022",
        'App_name' : "Petner - Evcil Hayvan Sahiplen",
        'Package_name' : "com.mgs.petner",
        'installs' : "1,000+",
        'version' : "2.1.6",
    },
    'com.ulugames.euros.google' :
    {
        'category' : "Games",
        'updated' : "10 Sep 2021",
        'App_name' : "\ud48d\uc2e0",
        'Package_name' : "com.ulugames.euros.google",
        'installs' : "500,000+",
        'version' : "1.0.51",
    },
    'kr.getcha' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "01 Jun 2022",
        'App_name' : "\uac9f\ucc28 - \ub354 \ub611\ub611\ud55c \uc2e0\ucc28 \uad6c\ub9e4 \uc571, GETCHA!",
        'Package_name' : "kr.getcha",
        'installs' : "500,000+",
        'version' : "8.1.0",
    },
    '575980917' :
    {
        'category' : "Games",
        'updated' : "2022-06-08T18:00:56Z",
        'App_name' : "Jackpot Party - Casino Slots",
        'Package_name' : "com.williamsinteractive.jackpotparty",
        'version' : "5032.00",
        'minimumOsVersion' : "9.0",
    },
    'com.indiaBulls' :
    {
        'category' : "Shopping",
        'updated' : "17 Jun 2022",
        'App_name' : "Dhani: Online Shopping App",
        'Package_name' : "com.indiaBulls",
        'installs' : "50,000,000+",
        'version' : "2022.06.16",
    },
    'com.gameone.mj' :
    {
        'category' : "Casino",
        'updated' : "29 Apr 2022",
        'App_name' : "\u4eca\u665a\u6253\u724c3\u7f3a1\u2013\u9ebb\u96c0\u3001\u9b25\u5730\u4e3b\u3001\u8dd1\u99ac\u4ed4\u3001\u8001\u864e\u6a5f",
        'Package_name' : "com.gameone.mj",
        'installs' : "50,000+",
        'version' : "11.9",
    },
    'com.papegames.nn4.jp' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "\u30b7\u30e3\u30a4\u30cb\u30f3\u30b0\u30cb\u30ad",
        'Package_name' : "com.papegames.nn4.jp",
        'installs' : "100,000+",
        'version' : "2.0.1039186",
    },
    'com.reddoorz.app' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "08 Jun 2022",
        'App_name' : "RedDoorz : Hotel Booking App",
        'Package_name' : "com.reddoorz.app",
        'installs' : "5,000,000+",
        'version' : "1.9.6",
    },
    '1457795581' :
    {
        'category' : "Games",
        'updated' : "2021-02-09T15:18:48Z",
        'App_name' : "\u30d0\u30ec\u3066\u307e\u3059\u3088\uff01\u3053\u3063\u305d\u308a\u3007\u3007\u3057\u3066\u308b\u4eba",
        'Package_name' : "com.barete.masuyo",
        'version' : "1.0.4",
        'minimumOsVersion' : "10.0",
    },
    'search.find.lorries.invite.transporter.add.lorries.deliveries.codenicely.search.vahak' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "07 Jun 2022",
        'App_name' : "Vahak: Book Online Truck, Load",
        'Package_name' : "search.find.lorries.invite.transporter.add.lorries.deliveries.codenicely.search.vahak",
        'installs' : "1,000,000+",
        'version' : "2.85",
    },
    'zero.finance.instantcash' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "X Wallet eXpress Loan APP",
        'Package_name' : "zero.finance.instantcash",
        'installs' : "100,000+",
        'version' : "6.6",
    },
    'kr.co.waug.waug' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "24 May 2022",
        'App_name' : "WAUG - EXPLORE MORE!",
        'Package_name' : "kr.co.waug.waug",
        'installs' : "1,000,000+",
        'version' : "2.26.4",
    },
    'com.mover.twswy' :
    {
        'category' : "Games",
        'updated' : "26 Apr 2022",
        'App_name' : "\u98df\u7269\u8a9e-\u6cbb\u6108\u7cfb\u7f88\u7d46\u990a\u6210RPG",
        'Package_name' : "com.mover.twswy",
        'installs' : "500,000+",
        'version' : "1.0.21",
    },
    '1194050611' :
    {
        'category' : "Games",
        'updated' : "2022-06-15T14:27:44Z",
        'App_name' : "Portal Quest",
        'Package_name' : "com.perblue.portalquest.ios",
        'version' : "5.16",
        'minimumOsVersion' : "9.0",
    },
    'com.goibibo' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "30 May 2022",
        'App_name' : "Flight Booking & Hotel Booking",
        'Package_name' : "com.goibibo",
        'installs' : "50,000,000+",
        'version' : "15.4.3",
    },
    '987860898' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-05-12T15:44:03Z",
        'App_name' : "Loisirs Ench\u00e8res",
        'Package_name' : "com.loisirsencheres.iosapp",
        'version' : "3.1.7",
        'minimumOsVersion' : "13.0",
    },
    'com.com2us.ninepb3d.normal.freefull.google.global.android.common' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "MLB 9 Innings 22",
        'Package_name' : "com.com2us.ninepb3d.normal.freefull.google.global.android.common",
        'installs' : "10,000,000+",
        'version' : "7.0.7",
    },
    'company.coinpop.coinpop' :
    {
        'category' : "Lifestyle",
        'updated' : "14 Jun 2022",
        'App_name' : "Coin Pop- Win Gift Cards",
        'Package_name' : "company.coinpop.coinpop",
        'installs' : "10,000,000+",
        'version' : "4.3.5-CoinPop",
    },
    'com.activision.callofduty.shooter' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Call of Duty Mobile Season 5",
        'Package_name' : "com.activision.callofduty.shooter",
        'installs' : "100,000,000+",
        'version' : "1.0.33",
    },
    'jp.co.cybird.appli.android.sgk' :
    {
        'category' : "Games",
        'updated' : "16 Dec 2021",
        'App_name' : "\u30a4\u30b1\u30e1\u30f3\u6226\u56fd\u3000\u6642\u3092\u304b\u3051\u308b\u604b\u3000\u604b\u611b\u30b2\u30fc\u30e0",
        'Package_name' : "jp.co.cybird.appli.android.sgk",
        'installs' : "1,000,000+",
        'version' : "1.7.1",
    },
    'app.skratchy.appsuccess' :
    {
        'category' : "Games",
        'updated' : "15 Apr 2022",
        'App_name' : "Skratchy",
        'Package_name' : "app.skratchy.appsuccess",
        'installs' : "500,000+",
        'version' : "3.0",
    },
    'kr.co.iparking.android' :
    {
        'category' : "Utilities",
        'updated' : "06 Jun 2022",
        'App_name' : "\uc544\uc774\ud30c\ud0b9(i PARKING)-\uc8fc\ucc28 \ud560\uc778/\uc815\ubcf4/\uac00\uaca9\ube44\uad50",
        'Package_name' : "kr.co.iparking.android",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.supergene.ting' :
    {
        'category' : "Social and Communication",
        'updated' : "26 May 2022",
        'App_name' : "doowit : Way to make BFF",
        'Package_name' : "com.supergene.ting",
        'installs' : "50,000+",
        'version' : "2.3.1",
    },
    'de.freenet.flex' :
    {
        'category' : "Social and Communication",
        'updated' : "25 Nov 2021",
        'App_name' : "freenet FLEX: Dein Handytarif",
        'Package_name' : "de.freenet.flex",
        'installs' : "50,000+",
        'version' : "1.7.0",
    },
    'com.meetville.dating' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "Online Dating App - Meetville",
        'Package_name' : "com.meetville.dating",
        'installs' : "5,000,000+",
        'version' : "6.32.6",
    },
    'jp.co.gu3.alchemist' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "\u8ab0\u30ac\u70ba\u306e\u30a2\u30eb\u30b1\u30df\u30b9\u30c8",
        'Package_name' : "jp.co.gu3.alchemist",
        'installs' : "1,000,000+",
        'version' : "11.2.4",
    },
    '1482717995' :
    {
        'category' : "Games",
        'updated' : "2020-10-06T12:57:28Z",
        'App_name' : "\u3044\u3058\u3081\u3089\u308c\u5973-\u30a4\u30b8\u30e1 \u306e \u8a3c\u62e0 \u3092\u63a2\u305b",
        'Package_name' : "com.mask.rareijime",
        'version' : "1.0.3",
        'minimumOsVersion' : "10.0",
    },
    'jp.co.playmotion.crossme' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "\u30af\u30ed\u30b9\u30df\u30fc - \u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea\u3067\u604b\u6d3b\u30fb\u5a5a\u6d3b\u30fb\u51fa\u4f1a\u3044",
        'Package_name' : "jp.co.playmotion.crossme",
        'installs' : "100,000+",
        'version' : "3.6.0",
    },
    'com.dhan.live' :
    {
        'category' : "Finance",
        'updated' : "09 Jun 2022",
        'App_name' : "Dhan: Share Market Trading App",
        'Package_name' : "com.dhan.live",
        'installs' : "500,000+",
        'version' : "1.0.21",
    },
    'com.NineGames.SwipeBrickBreaker2' :
    {
        'category' : "Games",
        'updated' : "02 Jul 2021",
        'App_name' : "Swipe Brick Breaker 2",
        'Package_name' : "com.NineGames.SwipeBrickBreaker2",
        'installs' : "10,000+",
        'version' : "1.0.107",
    },
    'com.jumia.android' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "JUMIA Online Shopping",
        'Package_name' : "com.jumia.android",
        'installs' : "50,000,000+",
        'version' : "11.5.0",
    },
    'com.kiloo.subwaysurf' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Subway Surfers",
        'Package_name' : "com.kiloo.subwaysurf",
        'installs' : "1,000,000,000+",
        'version' : "2.35.0",
    },
    'com.nexon.konosuba' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "KonoSuba: Fantastic Days",
        'Package_name' : "com.nexon.konosuba",
        'installs' : "1,000,000+",
        'version' : "2.6.5",
    },
    'com.bandainamcoent.gundambattle.tw' :
    {
        'category' : "Games",
        'updated' : "26 Apr 2022",
        'App_name' : "\u92fc\u5f48 \u722d\u92d2\u5c0d\u6c7a",
        'Package_name' : "com.bandainamcoent.gundambattle.tw",
        'installs' : "100,000+",
        'version' : "1.5.0",
    },
    'jp.co.happyelements.mirror' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "\u30e9\u30b9\u30c8\u30d4\u30ea\u30aa\u30c9 - \u5de1\u308a\u3042\u3046\u87ba\u65cb\u306e\u7269\u8a9e -",
        'Package_name' : "jp.co.happyelements.mirror",
        'installs' : "500,000+",
        'version' : "2.10.7",
    },
    'com.lumber.inc' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Idle Lumber Empire",
        'Package_name' : "com.lumber.inc",
        'installs' : "10,000,000+",
        'version' : "1.4.5",
    },
    '1167883783' :
    {
        'category' : "Games",
        'updated' : "2022-05-30T03:07:14Z",
        'App_name' : "\u795e\u59ebPROJECT A-\u7f8e\u5c11\u5973\u30ad\u30e3\u30e9\u00d7\u30d0\u30c8\u30ebRPG",
        'Package_name' : "com.dmm.games.kamihime",
        'version' : "2.5.1",
        'minimumOsVersion' : "11.0",
    },
    'com.userjoy.sgc.vinn' :
    {
        'category' : "Games",
        'updated' : "22 Nov 2021",
        'App_name' : "Anh H\u00f9ng Tam Qu\u1ed1c",
        'Package_name' : "com.userjoy.sgc.vinn",
        'installs' : "10,000+",
        'version' : "0.5.8",
    },
    'com.livescore' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "LiveScore: Live Sports Scores",
        'Package_name' : "com.livescore",
        'installs' : "50,000,000+",
        'version' : "5.15.1",
    },
    'com.gamehunt.boomz' :
    {
        'category' : "Games",
        'updated' : "19 Feb 2019",
        'App_name' : "BOOMZ",
        'Package_name' : "com.gamehunt.boomz",
        'installs' : "50,000+",
        'version' : "3.4.1.0",
    },
    'com.nexowallet' :
    {
        'category' : "Finance",
        'updated' : "31 May 2022",
        'App_name' : "Nexo: Buy BTC, ETH, SOL & AVAX",
        'Package_name' : "com.nexowallet",
        'installs' : "1,000,000+",
        'version' : "2.2.30",
    },
    'com.nirafinance.customer' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "NIRA Instant Personal Loan App",
        'Package_name' : "com.nirafinance.customer",
        'installs' : "5,000,000+",
        'version' : "6.9.1 ",
    },
    'com.mxtech.videoplayer.ad' :
    {
        'category' : "Entertainment",
        'updated' : "15 Jun 2022",
        'App_name' : "MX Player",
        'Package_name' : "com.mxtech.videoplayer.ad",
        'installs' : "1,000,000,000+",
        'version' : "Varies with device",
    },
    'kr.co.sugarhill.nemoapp' :
    {
        'category' : "House & Home",
        'updated' : "15 Jun 2022",
        'App_name' : "\ub124\ubaa8 - \uc0c1\uac00&\uc0ac\ubb34\uc2e4 \ub0b4\ub193\uae30, \uad6c\ud558\uae30, \uc2e4\uac70\ub798\uac00 \ud544\uc218\uc571",
        'Package_name' : "kr.co.sugarhill.nemoapp",
        'installs' : "1,000,000+",
        'version' : "5.0.97",
    },
    'com.longtukorea.ancientgod' :
    {
        'category' : "Games",
        'updated' : "16 May 2022",
        'App_name' : "\ud0dc\uc655: \uac00\ub78c\uc758 \uae38",
        'Package_name' : "com.longtukorea.ancientgod",
        'installs' : "100,000+",
        'version' : "1.0.30",
    },
    '1530127806' :
    {
        'category' : "Games",
        'updated' : "2022-06-16T05:16:26Z",
        'App_name' : "\u9b54\u8853\u7cbe\u54c1\u5e97",
        'Package_name' : "com.smilegate.magicshop.stove.ios",
        'version' : "2.5.18",
        'minimumOsVersion' : "12.0",
    },
    'com.sabiamedia.riseup' :
    {
        'category' : "Games",
        'updated' : "13 Sep 2021",
        'App_name' : "Dawn To Rise",
        'Package_name' : "com.sabiamedia.riseup",
        'installs' : "1,000+",
        'version' : "4.0",
    },
    'com.nexon.mdnf' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\ub358\uc804\uc564\ud30c\uc774\ud130 \ubaa8\ubc14\uc77c",
        'Package_name' : "com.nexon.mdnf",
        'installs' : "1,000,000+",
        'version' : "3.5.1",
    },
    '540236748' :
    {
        'category' : "Finance",
        'updated' : "2022-06-17T03:45:40Z",
        'App_name' : "Invoice2go: Easy Invoice Maker",
        'Package_name' : "com.invoice2go.plus",
        'version' : "11.51.2",
        'minimumOsVersion' : "13.0",
    },
    'com.playmgm.nj.sports' :
    {
        'category' : "Games",
        'updated' : "19 Apr 2022",
        'App_name' : "BetMGM Sportsbook",
        'Package_name' : "com.playmgm.nj.sports",
        'installs' : "500,000+",
        'version' : "22.04.16",
    },
    '1470463729' :
    {
        'category' : "Games",
        'updated' : "2021-01-09T08:29:50Z",
        'App_name' : "Water Shooty",
        'Package_name' : "com.waterShooty.tiplaystudio",
        'version' : "4.6",
        'minimumOsVersion' : "10.0",
    },
    'br.com.doghero.astro' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "10 Jun 2022",
        'App_name' : "DogHero - Dog Sitters & Walkers",
        'Package_name' : "br.com.doghero.astro",
        'installs' : "1,000,000+",
        'version' : "4.72.4.20220527",
    },
    'com.square_enix.android_googleplay.lastidea' :
    {
        'category' : "Games",
        'updated' : "09 Apr 2020",
        'App_name' : "\u30e9\u30b9\u30c8\u30a4\u30c7\u30a2",
        'Package_name' : "com.square_enix.android_googleplay.lastidea",
        'installs' : "100,000+",
        'version' : "1.10.0",
    },
    'com.etoro.openbook' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "eToro: Crypto. Stocks. Social.",
        'Package_name' : "com.etoro.openbook",
        'installs' : "10,000,000+",
        'version' : "440.0.0",
    },
    'com.imaginbank.app' :
    {
        'category' : "Finance",
        'updated' : "02 Jun 2022",
        'App_name' : "imagin: We are what we do",
        'Package_name' : "com.imaginbank.app",
        'installs' : "1,000,000+",
        'version' : "4.18.2",
    },
    '972002786' :
    {
        'category' : "Finance",
        'updated' : "2022-04-11T16:05:36Z",
        'App_name' : "\u8ee2\u8077\u306a\u3089\u30d3\u30ba\u30ea\u30fc\u30c1 - \u8ee2\u8077\u30a2\u30d7\u30ea",
        'Package_name' : "jp.bizreach.BizreachApp.candidate",
        'version' : "6.0.0",
        'minimumOsVersion' : "12.0",
    },
    'com.moaigames.ti' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "\ud2b8\ub77c\ud558 \uc778\ud53c\ub2c8\ud2f0",
        'Package_name' : "com.moaigames.ti",
        'installs' : "100,000+",
        'version' : "1.1.37",
    },
    'com.gentley' :
    {
        'category' : "Social and Communication",
        'updated' : "10 Jun 2022",
        'App_name' : "Gentley: Modern Dating",
        'Package_name' : "com.gentley",
        'installs' : "5,000+",
        'version' : "1.5.1",
    },
    'com.joycity.potc' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Pirates of the Caribbean: ToW",
        'Package_name' : "com.joycity.potc",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.solagame.zhvn.an' :
    {
        'category' : "Games",
        'updated' : "10 May 2022",
        'App_name' : "Mu\u00f4n \u0110\u1eddi Anh H\u00f9ng L\u1ee5c",
        'Package_name' : "com.solagame.zhvn.an",
        'installs' : "100,000+",
        'version' : "1.7",
    },
    'com.maila.buylive' :
    {
        'category' : "Shopping",
        'updated' : "01 Jul 2021",
        'App_name' : "BuyliveHK-\u65b0\u4eba\u514d\u904b\u8cbb",
        'Package_name' : "com.maila.buylive",
        'installs' : "10,000+",
        'version' : "3.9.9",
    },
    'com.animocabrands.google.CrazyDefenseHeroes' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "Crazy Defense Heroes - TD Game",
        'Package_name' : "com.animocabrands.google.CrazyDefenseHeroes",
        'installs' : "1,000,000+",
        'version' : "3.6.5",
    },
    'com.apalon.weatherradar.free' :
    {
        'category' : "Utilities",
        'updated' : "13 Jun 2022",
        'App_name' : "Clime: NOAA Weather Radar Live",
        'Package_name' : "com.apalon.weatherradar.free",
        'installs' : "10,000,000+",
        'version' : "1.53.0",
    },
    'com.tg.ysczgm.gp' :
    {
        'category' : "Games",
        'updated' : "22 Mar 2022",
        'App_name' : "\u96f2\u4e0a\u57ce\u4e4b\u6b4c",
        'Package_name' : "com.tg.ysczgm.gp",
        'installs' : "500,000+",
        'version' : "Varies with device",
    },
    'com.cupshe.cupshe' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "Cupshe - Swimsuit & Dress Shop",
        'Package_name' : "com.cupshe.cupshe",
        'installs' : "500,000+",
        'version' : "4.5.5",
    },
    'com.linecorp.LGYDSG' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "JUMPUTI HEROES \u82f1\u96c4\u6c23\u6ce1\u3000\u6b61\u6176\u4e09\u9031\u5e74\u5f8c\u534a\u6230",
        'Package_name' : "com.linecorp.LGYDSG",
        'installs' : "500,000+",
        'version' : "6.1.1",
    },
    'com.gsretail.android.smapp' :
    {
        'category' : "Shopping",
        'updated' : "29 Mar 2022",
        'App_name' : "GS Fresh Mall \ub2f9\uc2e0\uc744 \uc544\ub07c\ub294 \uc7a5\ubcf4\uae30",
        'Package_name' : "com.gsretail.android.smapp",
        'installs' : "1,000,000+",
        'version' : "3.0.26",
    },
    'com.bstar.intl' :
    {
        'category' : "Entertainment",
        'updated' : "10 Jun 2022",
        'App_name' : "Bilibili",
        'Package_name' : "com.bstar.intl",
        'installs' : "10,000,000+",
        'version' : "1.33.0",
    },
    'com.gamepanda.ST' :
    {
        'category' : "Games",
        'updated' : "03 Dec 2021",
        'App_name' : "\u84bc\u5929\u82f1\u96c4\u8a8c",
        'Package_name' : "com.gamepanda.ST",
        'installs' : "500,000+",
        'version' : "1.0.58",
    },
    'com.callapp.contacts' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "CallApp: Caller ID & Recording",
        'Package_name' : "com.callapp.contacts",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.snapdeal.main' :
    {
        'category' : "Shopping",
        'updated' : "16 Jun 2022",
        'App_name' : "Snapdeal: Online Shopping App",
        'Package_name' : "com.snapdeal.main",
        'installs' : "100,000,000+",
        'version' : "7.6.2",
    },
    'com.supr.suprdaily' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "Supr Daily, Grocery Delivery",
        'Package_name' : "com.supr.suprdaily",
        'installs' : "1,000,000+",
        'version' : "3.2.51",
    },
    '554578419' :
    {
        'category' : "Health and Fitness",
        'updated' : "2022-06-15T14:40:53Z",
        'App_name' : "Tata 1mg - Healthcare App",
        'Package_name' : "com.aranoah.healthkart",
        'version' : "14.2.0",
        'minimumOsVersion' : "12.1",
    },
    'com.thumbage.dekaron.google' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "\ub370\uce74\ub860M",
        'Package_name' : "com.thumbage.dekaron.google",
        'installs' : "100,000+",
        'version' : "1.0.237",
    },
    'com.webzen.mua.google' :
    {
        'category' : "Games",
        'updated' : "03 Mar 2022",
        'App_name' : "\ubba4 \uc544\ud06c\uc5d4\uc824",
        'Package_name' : "com.webzen.mua.google",
        'installs' : "500,000+",
        'version' : "1.43.01",
    },
    'de.tchibo.app' :
    {
        'category' : "Shopping",
        'updated' : "17 Jun 2022",
        'App_name' : "Tchibo - Mode, Wohnen & Kaffee",
        'Package_name' : "de.tchibo.app",
        'installs' : "1,000,000+",
        'version' : "8.2.5",
    },
    'com.srgamekr.damo.android' :
    {
        'category' : "Games",
        'updated' : "02 Nov 2021",
        'App_name' : "\ube14\ub9ac\uce58: \ub9cc\ud574\uc758 \uae38",
        'Package_name' : "com.srgamekr.damo.android",
        'installs' : "500,000+",
        'version' : "1.8.11",
    },
    'tech.fplabs.score' :
    {
        'category' : "Finance",
        'updated' : "18 Jun 2022",
        'App_name' : "OneScore: Credit Score Insight",
        'Package_name' : "tech.fplabs.score",
        'installs' : "10,000,000+",
        'version' : "3.14.21",
    },
    '1214486024' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-08T12:46:20Z",
        'App_name' : "Swvl - Daily Bus Rides",
        'Package_name' : "io.swvl.swvlapp",
        'version' : "5.15.0",
        'minimumOsVersion' : "12.0",
    },
    '654456967' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-08T07:08:16Z",
        'App_name' : "\u0627\u0644\u0633\u0648\u0642 \u0627\u0644\u0645\u0641\u062a\u0648\u062d - OpenSooq",
        'Package_name' : "com.opensooq.App",
        'version' : "8.9.00",
        'minimumOsVersion' : "10.0",
    },
    'jp.hikaruyamamoto.pato' :
    {
        'category' : "Entertainment",
        'updated' : "23 Jul 2021",
        'App_name' : "pato-\u98f2\u307f\u4f1a\u3084\u63a5\u5f85\u306b\u30aa\u30b9\u30b9\u30e1!\u65b0\u3057\u3044\u30a8\u30f3\u30bf\u30e1\u3068\u306e\u51fa\u4f1a\u3044!\u30a8\u30f3\u30bf\u30e1\u30de\u30c3\u30c1\u30f3\u30b0",
        'Package_name' : "jp.hikaruyamamoto.pato",
        'installs' : "50,000+",
        'version' : "5.0.10",
    },
    'com.kotakcherry.app' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "Cherry: MF, Stocks, NPS & more",
        'Package_name' : "com.kotakcherry.app",
        'installs' : "1,000,000+",
        'version' : "3.0.6",
    },
    '1494139203' :
    {
        'category' : "Utilities",
        'updated' : "2022-05-22T07:53:51Z",
        'App_name' : "Hot VPN: Fast Secure & Private",
        'Package_name' : "com.algoritmico.ios.securevpn",
        'version' : "1.4.2",
        'minimumOsVersion' : "11.0",
    },
    'com.jamcity.blackforest_goo' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Disney Frozen Adventures",
        'Package_name' : "com.jamcity.blackforest_goo",
        'installs' : "10,000,000+",
        'version' : "25.1.1",
    },
    'com.rapido.passenger' :
    {
        'category' : "Utilities",
        'updated' : "05 Jun 2022",
        'App_name' : "Rapido Bike Taxi & Auto",
        'Package_name' : "com.rapido.passenger",
        'installs' : "10,000,000+",
        'version' : "6.2.14",
    },
    '719972451' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-13T05:00:56Z",
        'App_name' : "DoorDash - Food Delivery",
        'Package_name' : "doordash.DoorDashConsumer",
        'version' : "4.87.0",
        'minimumOsVersion' : "14.0",
    },
    'com.kfcaus.ordering' :
    {
        'category' : "Food & Drink",
        'updated' : "29 Mar 2022",
        'App_name' : "KFC - Order On The Go",
        'Package_name' : "com.kfcaus.ordering",
        'installs' : "1,000,000+",
        'version' : "21.7.3",
    },
    'com.yy.biu' :
    {
        'category' : "Tools",
        'updated' : "06 Jun 2022",
        'App_name' : "Noizz: video editor with music",
        'Package_name' : "com.yy.biu",
        'installs' : "100,000,000+",
        'version' : "5.3.6",
    },
    'in.clovertech.bumblebee.nucleus' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "Deep Rooted",
        'Package_name' : "in.clovertech.bumblebee.nucleus",
        'installs' : "500,000+",
        'version' : "1.110.0",
    },
    'com.unionbankph.online' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "UnionBank Online",
        'Package_name' : "com.unionbankph.online",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.sjenm.detective' :
    {
        'category' : "Games",
        'updated' : "24 Mar 2020",
        'App_name' : "\ubb34\uc801\uc18c\ub144\uc9c0",
        'Package_name' : "com.sjenm.detective",
        'installs' : "100,000+",
        'version' : "1.0.16",
    },
    '1156964115' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-05-20T07:24:14Z",
        'App_name' : "Toosla - locations de voitures",
        'Package_name' : "com.toosla.flagship",
        'version' : "2.5.7",
        'minimumOsVersion' : "14.0",
    },
    'com.marathimatrimony' :
    {
        'category' : "Social and Communication",
        'updated' : "23 May 2022",
        'App_name' : "Marathi Matrimony\u00ae -Shaadi App",
        'Package_name' : "com.marathimatrimony",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.zoodfood.android.play' :
    {
        'category' : "Food & Drink",
        'updated' : "15 Jan 2022",
        'App_name' : "SnappFood \u0633\u0641\u0627\u0631\u0634 \u0627\u0646\u0644\u0627\u06cc\u0646 \u063a\u0630\u0627 \u0648 \u0633\u0648\u067e\u0631\u0645\u0627\u0631\u06a9\u062a",
        'Package_name' : "com.zoodfood.android.play",
        'installs' : "1,000,000+",
        'version' : "5.1.1.4",
    },
    'com.kog.grandchaseglobal' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "GrandChase",
        'Package_name' : "com.kog.grandchaseglobal",
        'installs' : "1,000,000+",
        'version' : "1.54.2",
    },
    'com.goatgames.ksr.kr.gp' :
    {
        'category' : "Games",
        'updated' : "18 Mar 2022",
        'App_name' : "\ud0b9\uc988 \uc2a4\ub860: \uc655\uc758 \uc5ec\uc790",
        'Package_name' : "com.goatgames.ksr.kr.gp",
        'installs' : "10,000+",
        'version' : "1.0.11",
    },
    'com.qustodio.qustodioapp' :
    {
        'category' : "Lifestyle",
        'updated' : "26 May 2022",
        'App_name' : "Kids App Qustodio",
        'Package_name' : "com.qustodio.qustodioapp",
        'installs' : "1,000,000+",
        'version' : "180.59.1.2-family",
    },
    'fr.telemaque.voyance' :
    {
        'category' : "Lifestyle",
        'updated' : "09 Jun 2022",
        'App_name' : "Psychic - Your tarot, astrology & numerology app",
        'Package_name' : "fr.telemaque.voyance",
        'installs' : "500,000+",
        'version' : "4.2",
    },
    'com.nhnent.Qpoker' :
    {
        'category' : "Casino",
        'updated' : "02 Jun 2022",
        'App_name' : "\ud55c\uac8c\uc784 \ud3ec\ucee4",
        'Package_name' : "com.nhnent.Qpoker",
        'installs' : "1,000,000+",
        'version' : "4.13.0",
    },
    'com.pinarsu.siparis' :
    {
        'category' : "Shopping",
        'updated' : "06 May 2022",
        'App_name' : "Ya\u015fam P\u0131nar\u0131m - P\u0131nar i\u00e7ecek ailesi kap\u0131n\u0131zda!",
        'Package_name' : "com.pinarsu.siparis",
        'installs' : "100,000+",
        'version' : "1.2.2",
    },
    'com.softstargames.mswdj' :
    {
        'category' : "Games",
        'updated' : "28 Sep 2021",
        'App_name' : "\u8ed2\u8f45\u528d - \u528d\u4e4b\u6e90",
        'Package_name' : "com.softstargames.mswdj",
        'installs' : "100,000+",
        'version' : "9.0.51",
    },
    'jp.co.asobism_castleanddragon' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "\u57ce\u3068\u30c9\u30e9\u30b4\u30f3",
        'Package_name' : "jp.co.asobism_castleanddragon",
        'installs' : "1,000,000+",
        'version' : "8.5.0.0",
    },
    'jp.dawn.mirrorcakes' :
    {
        'category' : "Games",
        'updated' : "17 Sep 2021",
        'App_name' : "Mirror cakes",
        'Package_name' : "jp.dawn.mirrorcakes",
        'installs' : "10,000,000+",
        'version' : "2.7.3",
    },
    '1453341551' :
    {
        'category' : "Games",
        'updated' : "2022-06-16T04:00:32Z",
        'App_name' : "JUMPUTI HEROES \u82f1\u96c4\u6c23\u6ce1",
        'Package_name' : "com.linecorp.LGYDSG",
        'version' : "6.1.1",
        'minimumOsVersion' : "8.0",
    },
    'br.com.guiabolso' :
    {
        'category' : "Finance",
        'updated' : "09 May 2022",
        'App_name' : "Guiabolso: Produtos e Gest\u00e3o Financeira",
        'Package_name' : "br.com.guiabolso",
        'installs' : "10,000,000+",
        'version' : "14.3.0.5",
    },
    '1396381549' :
    {
        'category' : "Games",
        'updated' : "2022-06-20T01:00:03Z",
        'App_name' : "2022 \uac8c\uc784\ube4c\ud504\ub85c\uc57c\uad6c \uc288\ud37c\uc2a4\ud0c0\uc988",
        'Package_name' : "com.gamevil.basebss.ios.apple.global.normal",
        'version' : "31.0.3",
        'minimumOsVersion' : "11.0",
    },
    'com.smartsecurityxzt' :
    {
        'category' : "Tools",
        'updated' : "07 Jun 2022",
        'App_name' : "Smart Security",
        'Package_name' : "com.smartsecurityxzt",
        'installs' : "50,000,000+",
        'version' : "192",
    },
    'com.nexon.v4tw' :
    {
        'category' : "Games",
        'updated' : "06 May 2021",
        'App_name' : "V4\uff1a\u8de8\u754c\u6230",
        'Package_name' : "com.nexon.v4tw",
        'installs' : "100,000+",
        'version' : "1.23.319873",
    },
    'jp.co.taito.onlinecrane' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "\u30bf\u30a4\u30c8\u30fc\u30aa\u30f3\u30e9\u30a4\u30f3\u30af\u30ec\u30fc\u30f3",
        'Package_name' : "jp.co.taito.onlinecrane",
        'installs' : "500,000+",
        'version' : "1.1.12",
    },
    'app.video.endlessclips' :
    {
        'category' : "Entertainment",
        'updated' : "29 May 2020",
        'App_name' : "endless clips",
        'Package_name' : "app.video.endlessclips",
        'installs' : "1,000+",
        'version' : "0.0.2",
    },
    'com.finger.hsgame' :
    {
        'category' : "Casino",
        'updated' : "26 May 2022",
        'App_name' : "\u8c6a\u795e\u5a1b\u6a02\u57ce-\u9ebb\u5c07\u3001\u6355\u9b5a\u3001\u8cd3\u679c\u3001\u6c34\u679c\u76e4\u3001\u9ab0\u5bf6\u3001\u62c9\u9738\u6a5f\u3001\u8001\u864e\u6a5f",
        'Package_name' : "com.finger.hsgame",
        'installs' : "500,000+",
        'version' : "5.00",
    },
    'md.your' :
    {
        'category' : "Health and Fitness",
        'updated' : "16 Jun 2022",
        'App_name' : "Healthily: Self-Care & Tracker",
        'Package_name' : "md.your",
        'installs' : "1,000,000+",
        'version' : "4.03.14",
    },
    'com.stickpoolclub' :
    {
        'category' : "Games",
        'updated' : "01 Apr 2022",
        'App_name' : "Stick Pool Club: 8 Ball Pool, 3D Poker, Callbreak",
        'Package_name' : "com.stickpoolclub",
        'installs' : "1,000,000+",
        'version' : "9.12",
    },
    'com.skytecgames.survival' :
    {
        'category' : "Games",
        'updated' : "18 May 2022",
        'App_name' : "Horrorfield Multiplayer horror",
        'Package_name' : "com.skytecgames.survival",
        'installs' : "10,000,000+",
        'version' : "1.4.6",
    },
    'com.tinder' :
    {
        'category' : "Lifestyle",
        'updated' : "11 Jun 2022",
        'App_name' : "Tinder - Dating & Make Friends",
        'Package_name' : "com.tinder",
        'installs' : "100,000,000+",
        'version' : "13.10.1",
    },
    'com.sephora.mobileapp' :
    {
        'category' : "Shopping",
        'updated' : "07 Jun 2022",
        'App_name' : "Sephora - \u043c\u0430\u0433\u0430\u0437\u0438\u043d \u043a\u043e\u0441\u043c\u0435\u0442\u0438\u043a\u0438",
        'Package_name' : "com.sephora.mobileapp",
        'installs' : "100,000+",
        'version' : "1.10.0",
    },
    'com.azadea.azadea' :
    {
        'category' : "Shopping",
        'updated' : "02 Jun 2022",
        'App_name' : "Azadea: Fashion & more",
        'Package_name' : "com.azadea.azadea",
        'installs' : "50,000+",
        'version' : "2.1.5",
    },
    'com.elearnmarkets.application' :
    {
        'category' : "Education and Books",
        'updated' : "13 May 2022",
        'App_name' : "Elearnmarkets- Learn to Invest",
        'Package_name' : "com.elearnmarkets.application",
        'installs' : "500,000+",
        'version' : "3.0.15",
    },
    'com.koovs.fashion' :
    {
        'category' : "Shopping",
        'updated' : "12 May 2022",
        'App_name' : "Koovs Online Shopping App",
        'Package_name' : "com.koovs.fashion",
        'installs' : "5,000,000+",
        'version' : "8.0",
    },
    'com.application.zomato' :
    {
        'category' : "Food & Drink",
        'updated' : "11 Jun 2022",
        'App_name' : "Zomato: Food Delivery & Dining",
        'Package_name' : "com.application.zomato",
        'installs' : "100,000,000+",
        'version' : "16.5.5",
    },
    'com.tatacapital.moneyfy' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "Moneyfy:Mutual Funds SIP, Loan",
        'Package_name' : "com.tatacapital.moneyfy",
        'installs' : "1,000,000+",
        'version' : "1.0.79",
    },
    'com.srpc.MangaArabiaYouth' :
    {
        'category' : "Education and Books",
        'updated' : "31 Dec 2021",
        'App_name' : "\u0645\u0627\u0646\u062c\u0627 \u0644\u0644\u0634\u0628\u0627\u0628",
        'Package_name' : "com.srpc.MangaArabiaYouth",
        'installs' : "100,000+",
        'version' : "1.0.6",
    },
    'com.valofe.fish.google.kr' :
    {
        'category' : "Games",
        'updated' : "27 Apr 2022",
        'App_name' : "\ud53c\uc26c\uc544\uc77c\ub79c\ub4dc : \uc815\ub839\uc758\ud56d\ub85c",
        'Package_name' : "com.valofe.fish.google.kr",
        'installs' : "10,000+",
        'version' : "1.0.15",
    },
    '1483014262' :
    {
        'category' : "Games",
        'updated' : "2020-03-17T21:42:20Z",
        'App_name' : "\u7247\u601d\u3044\u304b\u3089\u306e\u8131\u51fa",
        'Package_name' : "com.mask.kataomoi",
        'version' : "1.0.2",
        'minimumOsVersion' : "10.0",
    },
    'com.vindhyainfotech.classicrummyfree' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Classic Rummy",
        'Package_name' : "com.vindhyainfotech.classicrummyfree",
        'installs' : "500,000+",
        'version' : "2.5.4",
    },
    'ru.beeline.services' :
    {
        'category' : "Social and Communication",
        'updated' : "02 Jun 2022",
        'App_name' : "My Beeline",
        'Package_name' : "ru.beeline.services",
        'installs' : "10,000,000+",
        'version' : "4.63.2",
    },
    'com.ourpalm.kof.jp' :
    {
        'category' : "Games",
        'updated' : "21 Apr 2022",
        'App_name' : "The King of Fighters '98UM OL",
        'Package_name' : "com.ourpalm.kof.jp",
        'installs' : "1,000,000+",
        'version' : "1.3.9",
    },
    '873069066' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-03T22:26:06Z",
        'App_name' : "Biletall \u2013 Bus & Plane Tickets",
        'Package_name' : "com.ipektr.biletall",
        'version' : "11.4",
        'minimumOsVersion' : "10.0",
    },
    'com.itoxi.girlsbattle' :
    {
        'category' : "Games",
        'updated' : "21 Jan 2022",
        'App_name' : "\uc18c\ub140\ubc30\ud2c0 : \uc2a4\uc704\ud2b8 \ud558\ud2b8",
        'Package_name' : "com.itoxi.girlsbattle",
        'installs' : "100,000+",
        'version' : "1.1.5",
    },
    '1476397106' :
    {
        'category' : "Health and Fitness",
        'updated' : "2022-05-23T06:46:59Z",
        'App_name' : "PeriOsfer by Orkid",
        'Package_name' : "com.pg.orkid",
        'version' : "1.0.15",
        'minimumOsVersion' : "11.0",
    },
    '1439565347' :
    {
        'category' : "Games",
        'updated' : "2022-04-27T00:30:00Z",
        'App_name' : "ANOTHER EDEN \u7a7f\u8d8a\u6642\u7a7a\u7684\u8c93",
        'Package_name' : "games.wfs.anothereden",
        'version' : "2.11.700",
        'minimumOsVersion' : "10.0",
    },
    'com.kamagames.videopoker' :
    {
        'category' : "Games",
        'updated' : "05 May 2022",
        'App_name' : "Video Poker by Pokerist",
        'Package_name' : "com.kamagames.videopoker",
        'installs' : "500,000+",
        'version' : "46.3.0",
    },
    'com.smartspends' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "ET Money: Mutual Fund & Genius",
        'Package_name' : "com.smartspends",
        'installs' : "5,000,000+",
        'version' : "4.1.0.3",
    },
    'com.eyougame.kyokuya' :
    {
        'category' : "Games",
        'updated' : "12 Jan 2022",
        'App_name' : "\u6975\u591c\u5927\u9678\uff1a\u30e1\u30c6\u30aa\u306e\u5f7c\u65b9",
        'Package_name' : "com.eyougame.kyokuya",
        'installs' : "10,000+",
        'version' : "21.0",
    },
    'com.inkl.app' :
    {
        'category' : "Education and Books",
        'updated' : "14 May 2022",
        'App_name' : "inkl: Read news without ads, clickbait or paywalls",
        'Package_name' : "com.inkl.app",
        'installs' : "100,000+",
        'version' : "6.19",
    },
    'com.postpay' :
    {
        'category' : "Shopping",
        'updated' : "10 May 2022",
        'App_name' : "Postpay | Shop Now. Pay Later.",
        'Package_name' : "com.postpay",
        'installs' : "100,000+",
        'version' : "1.1.9",
    },
    'co.spoonme' :
    {
        'category' : "Social and Communication",
        'updated' : "03 Jun 2022",
        'App_name' : "Spoon: Talk & Music Livestream",
        'Package_name' : "co.spoonme",
        'installs' : "10,000,000+",
        'version' : "7.7.1",
    },
    '1018175041' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-08T08:02:00Z",
        'App_name' : "Picnic Online-Supermarkt",
        'Package_name' : "nl.picnic.picnic-supermarkt",
        'version' : "1.15.153",
        'minimumOsVersion' : "12.0",
    },
    'com.ei.robmaster3d' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "Rob Master 3D",
        'Package_name' : "com.ei.robmaster3d",
        'installs' : "10,000,000+",
        'version' : "1.10.60",
    },
    'com.inomera.sm' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "Migros Sanal Market",
        'Package_name' : "com.inomera.sm",
        'installs' : "5,000,000+",
        'version' : "7.0.0",
    },
    '830062153' :
    {
        'category' : "Games",
        'updated' : "2022-06-13T06:16:35Z",
        'App_name' : "\u307c\u304f\u3089\u306e\u7532\u5b50\u5712\uff01\u30dd\u30b1\u30c3\u30c8 \u9ad8\u6821\u91ce\u7403\u30b2\u30fc\u30e0",
        'Package_name' : "com.kayac.koshien-pocket",
        'version' : "8.11.0",
        'minimumOsVersion' : "10.0",
    },
    'ae.admedia.adtv' :
    {
        'category' : "Entertainment",
        'updated' : "06 Jun 2022",
        'App_name' : "ADtv",
        'Package_name' : "ae.admedia.adtv",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.app.kc.koscom' :
    {
        'category' : "Finance",
        'updated' : "03 May 2022",
        'App_name' : "\ud540\uc14bN - \ub300\ucd9c \ud655\uc815\uae08\ub9ac\uc640 \ud55c\ub3c4 \ud55c\ubc88\uc5d0 \ube44\uad50\ud558\uae30",
        'Package_name' : "com.app.kc.koscom",
        'installs' : "500,000+",
        'version' : "1.3.9",
    },
    'com.ulink.agrostar' :
    {
        'category' : "Lifestyle",
        'updated' : "16 May 2022",
        'App_name' : "Agrostar: Kisan Agridoctor App",
        'Package_name' : "com.ulink.agrostar",
        'installs' : "5,000,000+",
        'version' : "5.20.0",
    },
    'com.harmonika' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "Hawaya: Meet Muslim Singles",
        'Package_name' : "com.harmonika",
        'installs' : "5,000,000+",
        'version' : "4.21.1",
    },
    'ae.brandsforless.android' :
    {
        'category' : "Shopping",
        'updated' : "30 May 2022",
        'App_name' : "Brands For Less",
        'Package_name' : "ae.brandsforless.android",
        'installs' : "500,000+",
        'version' : "4.2.8",
    },
    'rebar.com.yh' :
    {
        'category' : "Lifestyle",
        'updated' : "24 May 2022",
        'App_name' : "rebar",
        'Package_name' : "rebar.com.yh",
        'installs' : "100,000+",
        'version' : "8.1.1",
    },
    'com.huanliao.tiya' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "TIYA - Live Group",
        'Package_name' : "com.huanliao.tiya",
        'installs' : "10,000,000+",
        'version' : "4.41.0",
    },
    'com.ddgames.geling' :
    {
        'category' : "Games",
        'updated' : "23 Oct 2020",
        'App_name' : "\ubc31\uadc0\uc57c\ud589 : \ubc29\uce58\ud615RPG",
        'Package_name' : "com.ddgames.geling",
        'installs' : "100,000+",
        'version' : "6.0",
    },
    'com.gm99.mus' :
    {
        'category' : "Games",
        'updated' : "21 Mar 2022",
        'App_name' : "\u5947\u8e5fMU\uff1a\u8de8\u6642\u4ee3",
        'Package_name' : "com.gm99.mus",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'jp.co.gu3.punk' :
    {
        'category' : "Games",
        'updated' : "19 May 2022",
        'App_name' : "\u30d5\u30a1\u30f3\u30c8\u30e0 \u30aa\u30d6 \u30ad\u30eb",
        'Package_name' : "jp.co.gu3.punk",
        'installs' : "1,000,000+",
        'version' : "11.5.2",
    },
    'com.kakaogames.gdtskr' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "\uac00\ub514\uc5b8 \ud14c\uc77c\uc988",
        'Package_name' : "com.kakaogames.gdtskr",
        'installs' : "1,000,000+",
        'version' : "2.45.0",
    },
    '302584613' :
    {
        'category' : "Education and Books",
        'updated' : "2022-06-01T14:27:41Z",
        'App_name' : "Kindle",
        'Package_name' : "com.amazon.Lassen",
        'version' : "6.59",
        'minimumOsVersion' : "14.0",
    },
    'com.vespainteractive.KingsRaid' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "KING`s RAID",
        'Package_name' : "com.vespainteractive.KingsRaid",
        'installs' : "5,000,000+",
        'version' : "4.80.2",
    },
    'hk.easyvan.app.client' :
    {
        'category' : "Utilities",
        'updated' : "17 Jun 2022",
        'App_name' : "Lalamove Asia - 24/7 Delivery",
        'Package_name' : "hk.easyvan.app.client",
        'installs' : "10,000,000+",
        'version' : "106.9.0",
    },
    '1195621598' :
    {
        'category' : "Games",
        'updated' : "2022-06-03T23:24:07Z",
        'App_name' : "Homescapes",
        'Package_name' : "com.playrix.gardenscapes-sweethome",
        'version' : "5.4.3",
        'minimumOsVersion' : "9.0",
    },
    'com.mobigame.afkgirl' :
    {
        'category' : "Games",
        'updated' : "14 Apr 2022",
        'App_name' : "AFK Girls: Idle Action",
        'Package_name' : "com.mobigame.afkgirl",
        'installs' : "100,000+",
        'version' : "33.0",
    },
    'com.webzen.muasea.google' :
    {
        'category' : "Games",
        'updated' : "11 Apr 2022",
        'App_name' : "MU Archangel",
        'Package_name' : "com.webzen.muasea.google",
        'installs' : "1,000,000+",
        'version' : "1.27.02",
    },
    'vivino.web.app' :
    {
        'category' : "Food & Drink",
        'updated' : "17 Jun 2022",
        'App_name' : "Vivino: Buy the Right Wine",
        'Package_name' : "vivino.web.app",
        'installs' : "10,000,000+",
        'version' : "2022.22.0",
    },
    '1535521299' :
    {
        'category' : "Games",
        'updated' : "2022-06-16T17:05:04Z",
        'App_name' : "Smart Brain: \u8133\u306e\u30b2\u30fc\u30e0",
        'Package_name' : "game.mind.teaser.smartbrain",
        'version' : "3.2.1",
        'minimumOsVersion' : "12.0",
    },
    'com.yjmg.lc.aos' :
    {
        'category' : "Games",
        'updated' : "24 Aug 2020",
        'App_name' : "\ub77c\uc2a4\ud2b8 \ucee4\ub9e8\ub354",
        'Package_name' : "com.yjmg.lc.aos",
        'installs' : "10,000+",
        'version' : "2.2",
    },
    '1496943893' :
    {
        'category' : "Games",
        'updated' : "2021-06-21T01:40:29Z",
        'App_name' : "\u30cd\u30c3\u30c8\u708e\u4e0a\u307f\u3063\u3051\uff01 - \u6687\u3064\u3076\u3057\u30b2\u30fc\u30e0\u30a2\u30d7\u30ea",
        'Package_name' : "com.mask.netburn",
        'version' : "1.0.9",
        'minimumOsVersion' : "10.0",
    },
    '1325457827' :
    {
        'category' : "Games",
        'updated' : "2022-06-09T02:51:25Z",
        'App_name' : "\u30a6\u30de\u5a18 \u30d7\u30ea\u30c6\u30a3\u30fc\u30c0\u30fc\u30d3\u30fc",
        'Package_name' : "jp.co.cygames.umamusume",
        'version' : "1.19.1",
        'minimumOsVersion' : "11.0",
    },
    'com.access_company.android.sh_jumpstore' :
    {
        'category' : "Education and Books",
        'updated' : "06 Jun 2022",
        'App_name' : "\u30b8\u30e3\u30f3\u30d7BOOK\u30b9\u30c8\u30a2\uff01\u4eba\u6c17\u30de\u30f3\u30ac/\u96d1\u8a8c\u591a\u6570\u306e\u96fb\u5b50\u66f8\u7c4d\u30a2\u30d7\u30ea",
        'Package_name' : "com.access_company.android.sh_jumpstore",
        'installs' : "1,000,000+",
        'version' : "3.5.2",
    },
    'com.fq.oversea.sg' :
    {
        'category' : "Games",
        'updated' : "02 Feb 2021",
        'App_name' : "\u5c71\u6d77\u9748\uff1a\u4e0a\u53e4\u5c01\u5370",
        'Package_name' : "com.fq.oversea.sg",
        'installs' : "100,000+",
        'version' : "1020214(187581.184081)",
    },
    'com.zoomcar' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "08 Jun 2022",
        'App_name' : "Zoomcar - Car sharing platform",
        'Package_name' : "com.zoomcar",
        'installs' : "10,000,000+",
        'version' : "6.10.1",
    },
    '1488557973' :
    {
        'category' : "Finance",
        'updated' : "2022-05-29T11:24:15Z",
        'App_name' : "Matrixport: Buy & Earn Crypto",
        'Package_name' : "com.matrixport.mark",
        'version' : "3.1.80",
        'minimumOsVersion' : "11.0",
    },
    'puzzle.blockpuzzle.cube.relax' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "Wood Block Puzzle - Block Game",
        'Package_name' : "puzzle.blockpuzzle.cube.relax",
        'installs' : "50,000,000+",
        'version' : "2.7.3",
    },
    '1549449518' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-08T07:14:10Z",
        'App_name' : "Mazadak",
        'Package_name' : "com.agac.Mazadak",
        'version' : "1.20.13",
        'minimumOsVersion' : "13.0",
    },
    'ru.x5.perekrestok.darkstore' :
    {
        'category' : "Food & Drink",
        'updated' : "08 Jun 2022",
        'App_name' : "\u041f\u0435\u0440\u0435\u043a\u0440\u0451\u0441\u0442\u043e\u043a \u0412\u043f\u0440\u043e\u043a \u2014 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u044b",
        'Package_name' : "ru.x5.perekrestok.darkstore",
        'installs' : "1,000,000+",
        'version' : "2.0.2",
    },
    'com.showtime.standalone' :
    {
        'category' : "Entertainment",
        'updated' : "29 Mar 2022",
        'App_name' : "SHOWTIME",
        'Package_name' : "com.showtime.standalone",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.netmarble.ma9bkr' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "\ub9c8\uad6c\ub9c8\uad6c 2022",
        'Package_name' : "com.netmarble.ma9bkr",
        'installs' : "500,000+",
        'version' : "3.2.0",
    },
    'com.phonepe.app' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "PhonePe UPI, Payment, Recharge",
        'Package_name' : "com.phonepe.app",
        'installs' : "100,000,000+",
        'version' : "4.1.35",
    },
    'com.tap4fun.kissofwar.googleplay' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Kiss of War",
        'Package_name' : "com.tap4fun.kissofwar.googleplay",
        'installs' : "5,000,000+",
        'version' : "1.83.0",
    },
    'com.apartmentlist.mobile' :
    {
        'category' : "House & Home",
        'updated' : "07 Feb 2022",
        'App_name' : "Apartment List: Housing, Apt, and Property Rentals",
        'Package_name' : "com.apartmentlist.mobile",
        'installs' : "1,000,000+",
        'version' : "2.37.3",
    },
    'com.banggood.client' :
    {
        'category' : "Shopping",
        'updated' : "16 Jun 2022",
        'App_name' : "Banggood - Online Shopping",
        'Package_name' : "com.banggood.client",
        'installs' : "10,000,000+",
        'version' : "7.42.2",
    },
    '1554831556' :
    {
        'category' : "Games",
        'updated' : "2022-06-09T01:12:19Z",
        'App_name' : "CPBL\u8077\u696d\u68d2\u74032022",
        'Package_name' : "com.gravity.probb.ios",
        'version' : "3.6",
        'minimumOsVersion' : "12.0",
    },
    'vng.game.sky.fantasy.song.vn' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "Cloud Song: V\u00e2n Th\u00e0nh Chi Ca",
        'Package_name' : "vng.game.sky.fantasy.song.vn",
        'installs' : "500,000+",
        'version' : "1.1.0",
    },
    'com.kfc.egypt' :
    {
        'category' : "Food & Drink",
        'updated' : "10 Oct 2021",
        'App_name' : "KFC Egypt - Order Food Online",
        'Package_name' : "com.kfc.egypt",
        'installs' : "1,000,000+",
        'version' : "5.14.5",
    },
    'com.gamevil.basebss.android.google.global.normal1' :
    {
        'category' : "Games",
        'updated' : "11 Jun 2022",
        'App_name' : "Baseball Superstars 2022",
        'Package_name' : "com.gamevil.basebss.android.google.global.normal1",
        'installs' : "1,000,000+",
        'version' : "31.0.1",
    },
    'com.yottagames.mafiawar' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Mafia City",
        'Package_name' : "com.yottagames.mafiawar",
        'installs' : "100,000,000+",
        'version' : "1.6.251",
    },
    'in.mohalla.video.lite' :
    {
        'category' : "Social and Communication",
        'updated' : "27 Apr 2022",
        'App_name' : "Moj Lite",
        'Package_name' : "in.mohalla.video.lite",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.pozitron.iscep' :
    {
        'category' : "Finance",
        'updated' : "03 Jun 2022",
        'App_name' : "\u0130\u015fCep - Mobile Banking",
        'Package_name' : "com.pozitron.iscep",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.bybit.app' :
    {
        'category' : "Finance",
        'updated' : "09 Jun 2022",
        'App_name' : "Bybit: Buy & Trade Crypto",
        'Package_name' : "com.bybit.app",
        'installs' : "5,000,000+",
        'version' : "3.22.5",
    },
    '1195233335' :
    {
        'category' : "Education and Books",
        'updated' : "2021-05-28T22:06:12Z",
        'App_name' : "Yarn - Chat & Text Stories",
        'Package_name' : "com.science-inc.Yarn",
        'version' : "10.0.1",
        'minimumOsVersion' : "11.0",
    },
    'com.gameinsight.solitaire' :
    {
        'category' : "Games",
        'updated' : "15 Feb 2022",
        'App_name' : "Solitaire: Treasure of Time",
        'Package_name' : "com.gameinsight.solitaire",
        'installs' : "1,000,000+",
        'version' : "2.4.0",
    },
    'dk.loplus.medlemskort' :
    {
        'category' : "Lifestyle",
        'updated' : "22 Mar 2022",
        'App_name' : "PlusKort app\u2019en",
        'Package_name' : "dk.loplus.medlemskort",
        'installs' : "100,000+",
        'version' : "2.0.14",
    },
    'com.saucey' :
    {
        'category' : "Food & Drink",
        'updated' : "21 Jan 2022",
        'App_name' : "Saucey: Alcohol Delivery",
        'Package_name' : "com.saucey",
        'installs' : "100,000+",
        'version' : "2.5.12",
    },
    'com.fingerfun.gp.touratairiku.jp' :
    {
        'category' : "Games",
        'updated' : "19 Nov 2021",
        'App_name' : "\u30bd\u30a6\u30eb7\uff1a\u95d8\u7f85\u5927\u9678-\u7570\u4e16\u754c\u738b\u9053RPG\u00d7\u8fce\u6483\u578b\u30a2\u30af\u30b7\u30e7\u30f3\u30b2\u30fc\u30e0",
        'Package_name' : "com.fingerfun.gp.touratairiku.jp",
        'installs' : "50,000+",
        'version' : "1.1.3",
    },
    'com.bandagames.mpuzzle.gp' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Magic Jigsaw Puzzles - Game HD",
        'Package_name' : "com.bandagames.mpuzzle.gp",
        'installs' : "50,000,000+",
        'version' : "6.9.1",
    },
    'com.zepetto.theoutlaw.turkey' :
    {
        'category' : "Games",
        'updated' : "08 Jan 2020",
        'App_name' : "OutLaw",
        'Package_name' : "com.zepetto.theoutlaw.turkey",
        'installs' : "50,000+",
        'version' : "4.0.4",
    },
    'com.solitaire.tripeaks.grand.free.journey' :
    {
        'category' : "Games",
        'updated' : "22 Apr 2022",
        'App_name' : "Solitaire TriPeaks: Grand Tour",
        'Package_name' : "com.solitaire.tripeaks.grand.free.journey",
        'installs' : "10,000+",
        'version' : "1.2.83",
    },
    'com.melodong.rush.run.magic' :
    {
        'category' : "Games",
        'updated' : "24 Nov 2021",
        'App_name' : "Magic Run - Mana Master",
        'Package_name' : "com.melodong.rush.run.magic",
        'installs' : "500,000+",
        'version' : "1.1.8",
    },
    'com.roposo.android' :
    {
        'category' : "Social and Communication",
        'updated' : "11 Jun 2022",
        'App_name' : "Roposo Live Online Shopping",
        'Package_name' : "com.roposo.android",
        'installs' : "100,000,000+",
        'version' : "9.15.5",
    },
    '1448979486' :
    {
        'category' : "Games",
        'updated' : "2022-06-08T19:49:20Z",
        'App_name' : "\ub358\uc804\uc564\ud30c\uc774\ud130 \ubaa8\ubc14\uc77c",
        'Package_name' : "com.nexon.mdnf",
        'version' : "3.5.1",
        'minimumOsVersion' : "12.0",
    },
    'sa.virginmobile.vm' :
    {
        'category' : "Tools",
        'updated' : "08 May 2022",
        'App_name' : "Virgin Mobile KSA",
        'Package_name' : "sa.virginmobile.vm",
        'installs' : "1,000,000+",
        'version' : "2.33.1",
    },
    'com.ncsoft.baseball.h3' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "\ud504\ub85c\uc57c\uad6c H3",
        'Package_name' : "com.ncsoft.baseball.h3",
        'installs' : "500,000+",
        'version' : "1.40.0",
    },
    '1413032341' :
    {
        'category' : "Games",
        'updated' : "2022-04-22T01:28:12Z",
        'App_name' : "Slots-dummy 2V2 \u0e44\u0e1e\u0e48\u0e41\u0e04\u0e07 \u0e14\u0e31\u0e21\u0e21\u0e35\u0e48",
        'Package_name' : "com.cynking.th.fk",
        'version' : "2.22.0",
        'minimumOsVersion' : "9.0",
    },
    'com.whitehatjr' :
    {
        'category' : "Education and Books",
        'updated' : "07 Jun 2022",
        'App_name' : "WhiteHat Jr: Book Live Classes",
        'Package_name' : "com.whitehatjr",
        'installs' : "5,000,000+",
        'version' : "5.1.4",
    },
    'com.fintonic' :
    {
        'category' : "Finance",
        'updated' : "09 Jun 2022",
        'App_name' : "Fintonic | Ahorra y fin\u00e1nciate",
        'Package_name' : "com.fintonic",
        'installs' : "1,000,000+",
        'version' : "7.2.8.2",
    },
    'com.fluffyfairygames.idleminertycoon' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Idle Miner Tycoon: Gold & Cash",
        'Package_name' : "com.fluffyfairygames.idleminertycoon",
        'installs' : "100,000,000+",
        'version' : "3.88.0",
    },
    '631927169' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-08T06:05:52Z",
        'App_name' : "Goibibo Hotel Flight Train Bus",
        'Package_name' : "com.goibibo.Goibibo",
        'version' : "12.4.2",
        'minimumOsVersion' : "11.0",
    },
    'com.luckycoco.earncash.gift' :
    {
        'category' : "Entertainment",
        'updated' : "28 Apr 2022",
        'App_name' : "Lucky Coco: Make money, Reward",
        'Package_name' : "com.luckycoco.earncash.gift",
        'installs' : "500,000+",
        'version' : "1.00.00.016",
    },
    'com.gamesofa.android.luxytexasholdem' :
    {
        'category' : "Casino",
        'updated' : "26 May 2022",
        'App_name' : "Luxy Poker-Online Texas Poker",
        'Package_name' : "com.gamesofa.android.luxytexasholdem",
        'installs' : "5,000,000+",
        'version' : "5.3.9.0.1",
    },
    'com.finaccel.android' :
    {
        'category' : "Finance",
        'updated' : "01 Jun 2022",
        'App_name' : "Kredivo - Cicilan s/d 12 Bulan",
        'Package_name' : "com.finaccel.android",
        'installs' : "10,000,000+",
        'version' : "3.6.1",
    },
    'com.tatemgames.dreamgym' :
    {
        'category' : "Games",
        'updated' : "12 Aug 2021",
        'App_name' : "My Gym: Fitness Studio Manager",
        'Package_name' : "com.tatemgames.dreamgym",
        'installs' : "1,000,000+",
        'version' : "4.7.2924",
    },
    'com.sixwaves.animal' :
    {
        'category' : "Games",
        'updated' : "17 Jul 2020",
        'App_name' : "\u30a2\u30cb\u30de\u30eb \u30d5\u30ed\u30f3\u30c6\u30a3\u30a2",
        'Package_name' : "com.sixwaves.animal",
        'installs' : "10,000+",
        'version' : "1.0.4",
    },
    'com.vn.ttxt.gp' :
    {
        'category' : "Games",
        'updated' : "24 Nov 2021",
        'App_name' : "Truy\u1ec1n Thuy\u1ebft X\u1ea1 Th\u1ee7",
        'Package_name' : "com.vn.ttxt.gp",
        'installs' : "100,000+",
        'version' : "2.18",
    },
    'com.telemundo.awe' :
    {
        'category' : "Entertainment",
        'updated' : "25 May 2022",
        'App_name' : "Telemundo: Series y TV en vivo",
        'Package_name' : "com.telemundo.awe",
        'installs' : "1,000,000+",
        'version' : "7.31.0",
    },
    'jd.cdyjy.overseas.market.indonesia' :
    {
        'category' : "Shopping",
        'updated' : "02 Jun 2022",
        'App_name' : "JD.ID Online Shopping",
        'Package_name' : "jd.cdyjy.overseas.market.indonesia",
        'installs' : "10,000,000+",
        'version' : "6.8.7",
    },
    'kr.co.forc' :
    {
        'category' : "Shopping",
        'updated' : "20 May 2022",
        'App_name' : "forc \ucf55! \ucc0d\uc5b4 \ucc3e\uc544\uc8fc\ub294 \ud3ec\ud06c\uc1fc\ud551",
        'Package_name' : "kr.co.forc",
        'installs' : "10,000+",
        'version' : "3.0.2",
    },
    'kr.co.kbc.cha.android' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "14 Jun 2022",
        'App_name' : "KB\ucc28\ucc28\ucc28 \uc911\uace0\ucc28\ub9e4\ub9e4,\ub0b4\ucc28\ud314\uae30,\ub0b4\ucc28\uc2dc\uc138",
        'Package_name' : "kr.co.kbc.cha.android",
        'installs' : "1,000,000+",
        'version' : "4.3.5",
    },
    'com.piggyride.customer' :
    {
        'category' : "Health and Fitness",
        'updated' : "23 May 2022",
        'App_name' : "PiggyRide - Online Live Classes App",
        'Package_name' : "com.piggyride.customer",
        'installs' : "10,000+",
        'version' : "7.2.0",
    },
    'com.culturacervecera.loscervecistas' :
    {
        'category' : "Lifestyle",
        'updated' : "13 Apr 2022",
        'App_name' : "Cervecistas",
        'Package_name' : "com.culturacervecera.loscervecistas",
        'installs' : "10,000+",
        'version' : "1.5.7",
    },
    'br.com.petz' :
    {
        'category' : "Shopping",
        'updated' : "16 Jun 2022",
        'App_name' : "Petz: pet shop com ofertas e delivery r\u00e1pido",
        'Package_name' : "br.com.petz",
        'installs' : "5,000,000+",
        'version' : "4.10.1",
    },
    'com.dsfgland.goat' :
    {
        'category' : "Games",
        'updated' : "25 Apr 2022",
        'App_name' : "Dragon Storm Fantasy",
        'Package_name' : "com.dsfgland.goat",
        'installs' : "1,000,000+",
        'version' : "3.1.5",
    },
    '1461434144' :
    {
        'category' : "Games",
        'updated' : "2021-07-26T20:47:38Z",
        'App_name' : "Neon N Balls",
        'Package_name' : "com.vgames.neonnballs",
        'version' : "5.0",
        'minimumOsVersion' : "10.0",
    },
    'in.ovenstory' :
    {
        'category' : "Food & Drink",
        'updated' : "24 May 2022",
        'App_name' : "Oven Story Pizza- Delivery App",
        'Package_name' : "in.ovenstory",
        'installs' : "1,000,000+",
        'version' : "1.3.0",
    },
    'mm.com.wavemoney.wavepay' :
    {
        'category' : "Finance",
        'updated' : "10 Nov 2021",
        'App_name' : "WavePay APP by Wave Money",
        'Package_name' : "mm.com.wavemoney.wavepay",
        'installs' : "5,000,000+",
        'version' : "1.4.0.1",
    },
    'com.loongcheer.ellrlandtales.deckheroes' :
    {
        'category' : "Games",
        'updated' : "28 Jan 2022",
        'App_name' : "Ellrland Tales: Deck Heroes",
        'Package_name' : "com.loongcheer.ellrlandtales.deckheroes",
        'installs' : "50,000+",
        'version' : "1.2.1",
    },
    'com.r2.sgmnz.kr' :
    {
        'category' : "Games",
        'updated' : "28 Sep 2021",
        'App_name' : "\uc0bc\uad6d\uc9c0 \uc624\ub9ac\uc9c4",
        'Package_name' : "com.r2.sgmnz.kr",
        'installs' : "500,000+",
        'version' : "1.17",
    },
    'com.babbel.mobile.android.en' :
    {
        'category' : "Education and Books",
        'updated' : "10 Jun 2022",
        'App_name' : "Babbel - Learn Languages",
        'Package_name' : "com.babbel.mobile.android.en",
        'installs' : "10,000,000+",
        'version' : "21.3.1",
    },
    'com.diinapp' :
    {
        'category' : "Finance",
        'updated' : "09 Jun 2022",
        'App_name' : "Gr\u00e3o - Your Digital Savings",
        'Package_name' : "com.diinapp",
        'installs' : "500,000+",
        'version' : "1.6.14",
    },
    '1400683584' :
    {
        'category' : "Games",
        'updated' : "2022-03-22T22:59:00Z",
        'App_name' : "\uc2a4\ud1a4\uc5d0\uc774\uc9c0 \uc6d4\ub4dc",
        'Package_name' : "com.netmarble.stonemmogb",
        'version' : "13.1.0",
        'minimumOsVersion' : "10.0",
    },
    '445338486' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-15T07:49:50Z",
        'App_name' : "LOVOO - Dating App & Live Chat",
        'Package_name' : "com.kiss2go.iphone",
        'version' : "127.2",
        'minimumOsVersion' : "13.0",
    },
    'in.stockedge.app' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "StockEdge - Stock Market India",
        'Package_name' : "in.stockedge.app",
        'installs' : "1,000,000+",
        'version' : "8.1.3",
    },
    '1401955754' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-04-28T15:19:11Z",
        'App_name' : "Odamax - Hotel Reservation",
        'Package_name' : "com.etstur.odamax",
        'version' : "v2.2.0",
        'minimumOsVersion' : "11.0",
    },
    'jp.eveeve.app' :
    {
        'category' : "Social and Communication",
        'updated' : "07 Sep 2021",
        'App_name' : "\u30a4\u30f4\u30a4\u30f4-\u65e5\u672c\u6700\u5927\u5be9\u67fb\u5236\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea \u771f\u5263\u604b\u6d3b\u30fb\u5a5a\u6d3b\u30fb\u51fa\u4f1a\u3044(\u767b\u9332\u7121\u6599) \u3067\u604b\u4eba\u63a2\u3057",
        'Package_name' : "jp.eveeve.app",
        'installs' : "100,000+",
        'version' : "2.34.0",
    },
    '1437308917' :
    {
        'category' : "Games",
        'updated' : "2022-04-26T14:27:55Z",
        'App_name' : "Skip school -escape game",
        'Package_name' : "com.EurekaStudio.school",
        'version' : "3.4.4",
        'minimumOsVersion' : "11.0",
    },
    'com.lightricks.facetune.free' :
    {
        'category' : "Entertainment",
        'updated' : "01 Jun 2022",
        'App_name' : "Facetune2 Editor by Lightricks",
        'Package_name' : "com.lightricks.facetune.free",
        'installs' : "50,000,000+",
        'version' : "2.9.1-free",
    },
    'com.zoosk.zoosk' :
    {
        'category' : "Social and Communication",
        'updated' : "10 May 2022",
        'App_name' : "Zoosk - Social Dating App",
        'Package_name' : "com.zoosk.zoosk",
        'installs' : "10,000,000+",
        'version' : "5.21.0",
    },
    'com.maxstream' :
    {
        'category' : "Entertainment",
        'updated' : "21 Apr 2022",
        'App_name' : "MAXstream",
        'Package_name' : "com.maxstream",
        'installs' : "10,000,000+",
        'version' : "3.1.0",
    },
    'com.freecharge.android' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Freecharge - Pay Later, UPI",
        'Package_name' : "com.freecharge.android",
        'installs' : "10,000,000+",
        'version' : "10.13.1",
    },
    'com.adjoy.standalone' :
    {
        'category' : "Lifestyle",
        'updated' : "01 Feb 2022",
        'App_name' : "Dabbl - Earn in your downtime",
        'Package_name' : "com.adjoy.standalone",
        'installs' : "100,000+",
        'version' : "2.06",
    },
    'com.axis.net' :
    {
        'category' : "Tools",
        'updated' : "03 Jun 2022",
        'App_name' : "AXISnet Cek & Beli Kuota Data",
        'Package_name' : "com.axis.net",
        'installs' : "10,000,000+",
        'version' : "8.2.1",
    },
    'io.thatisnomoon.ambidexter' :
    {
        'category' : "Shopping",
        'updated' : "24 Nov 2020",
        'App_name' : "Yunar \u2013 Your loyalty cards wallet",
        'Package_name' : "io.thatisnomoon.ambidexter",
        'installs' : "1,000,000+",
        'version' : "1.38.0",
    },
    '964498250' :
    {
        'category' : "Not available",
        'updated' : "2021-12-03T22:09:02Z",
        'App_name' : "Tonki - Design Fotos drucken",
        'Package_name' : "it.tonki.tonki",
        'version' : "3.1.14",
        'minimumOsVersion' : "12.0",
    },
    'com.flick.jycs.google' :
    {
        'category' : "Games",
        'updated' : "15 Dec 2021",
        'App_name' : "\uac80\uc0dd\uac80\uc0ac",
        'Package_name' : "com.flick.jycs.google",
        'installs' : "50,000+",
        'version' : "1.0.9",
    },
    'zombie.survival.craft.z' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Last Day on Earth: Survival",
        'Package_name' : "zombie.survival.craft.z",
        'installs' : "100,000,000+",
        'version' : "1.19.1",
    },
    '1107705982' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-15T05:24:14Z",
        'App_name' : "Justlife (Justmop)",
        'Package_name' : "com.mobile.justmop",
        'version' : "6.27.0",
        'minimumOsVersion' : "11.0",
    },
    'kr.co.sincetimes.mnbg' :
    {
        'category' : "Games",
        'updated' : "14 Apr 2020",
        'App_name' : "\ud574\uc804M",
        'Package_name' : "kr.co.sincetimes.mnbg",
        'installs' : "100,000+",
        'version' : "1.0.28",
    },
    'mx.com.procesar.aforemovil.nuevosura' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Afore SURA",
        'Package_name' : "mx.com.procesar.aforemovil.nuevosura",
        'installs' : "1,000,000+",
        'version' : "2.4.6",
    },
    'com.app.docdoc' :
    {
        'category' : "Health and Fitness",
        'updated' : "06 Jun 2022",
        'App_name' : "doc-doc | Citas m\u00e9dicas online con los mejores",
        'Package_name' : "com.app.docdoc",
        'installs' : "10,000+",
        'version' : "1.6.27",
    },
    'com.yinshan.program.banda' :
    {
        'category' : "Finance",
        'updated' : "20 Apr 2022",
        'App_name' : "AdaPundi- Pinjaman Uang Online",
        'Package_name' : "com.yinshan.program.banda",
        'installs' : "5,000,000+",
        'version' : "3.6.9",
    },
    'fi.ThreeDBear.ThreeDBearAR' :
    {
        'category' : "Education and Books",
        'updated' : "25 May 2022",
        'App_name' : "3DBear \u2013 Visualize your creative thinking",
        'Package_name' : "fi.ThreeDBear.ThreeDBearAR",
        'installs' : "500,000+",
        'version' : "3.12.0",
    },
    'com.vtcmobile.phongvanchivtc' :
    {
        'category' : "Games",
        'updated' : "06 Nov 2021",
        'App_name' : "Phong V\u00e2n Ch\u00ed \u2013 Chu\u1ea9n V\u00f5 L\u00e2m",
        'Package_name' : "com.vtcmobile.phongvanchivtc",
        'installs' : "500,000+",
        'version' : "1.01.079.3",
    },
    '1546719969' :
    {
        'category' : "Lifestyle",
        'updated' : "2021-10-12T19:11:28Z",
        'App_name' : "Petner",
        'Package_name' : "net.mgssoftware.petmate",
        'version' : "2.1.3",
        'minimumOsVersion' : "13.0",
    },
    'com.softtech.imece' :
    {
        'category' : "Tools",
        'updated' : "29 Mar 2022",
        'App_name' : "\u0130meceMobil | Dijital Tar\u0131m",
        'Package_name' : "com.softtech.imece",
        'installs' : "100,000+",
        'version' : "1.4.4",
    },
    'com.longe.ysq2' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\u6709\u6bba\u6c23\u7ae5\u8a712",
        'Package_name' : "com.longe.ysq2",
        'installs' : "100,000+",
        'version' : "1.0.30",
    },
    'jp.co.peanutsclub.gla' :
    {
        'category' : "Games",
        'updated' : "20 Apr 2022",
        'App_name' : "Getlive(Online Crane Game)",
        'Package_name' : "jp.co.peanutsclub.gla",
        'installs' : "100,000+",
        'version' : "3.2.1",
    },
    'com.winelivery1.oos' :
    {
        'category' : "Food & Drink",
        'updated' : "30 May 2022",
        'App_name' : "Winelivery: L'App per bere!",
        'Package_name' : "com.winelivery1.oos",
        'installs' : "100,000+",
        'version' : "7.4.0",
    },
    'mx.app.baz.superapp' :
    {
        'category' : "Entertainment",
        'updated' : "07 Jun 2022",
        'App_name' : "baz: superapp with everything",
        'Package_name' : "mx.app.baz.superapp",
        'installs' : "5,000,000+",
        'version' : "1.10.0",
    },
    'com.octafx' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "OctaFX Trading App",
        'Package_name' : "com.octafx",
        'installs' : "10,000,000+",
        'version' : "2.5.61",
    },
    'com.arctheladr.forwardworks' :
    {
        'category' : "Games",
        'updated' : "25 Jun 2021",
        'App_name' : "\u5149\u3068\u97f3\u306eRPG \u30a2\u30fc\u30af\u30b6\u30e9\u30c3\u30c9 R",
        'Package_name' : "com.arctheladr.forwardworks",
        'installs' : "100,000+",
        'version' : "4.0.0",
    },
    'com.ldsanguo.shark' :
    {
        'category' : "Games",
        'updated' : "11 Feb 2022",
        'App_name' : "\ub09c\ud22c\uc0bc\uad6d:10vs10",
        'Package_name' : "com.ldsanguo.shark",
        'installs' : "50,000+",
        'version' : "2.4.8",
    },
    'de.funke.tvdigital' :
    {
        'category' : "Entertainment",
        'updated' : "10 Mar 2022",
        'App_name' : "TV-Programm TV DIGITAL",
        'Package_name' : "de.funke.tvdigital",
        'installs' : "1,000,000+",
        'version' : "1.0.35",
    },
    'com.valorem.flobooks' :
    {
        'category' : "Finance",
        'updated' : "18 Jun 2022",
        'App_name' : "myBillBook Invoice Billing App",
        'Package_name' : "com.valorem.flobooks",
        'installs' : "5,000,000+",
        'version' : "7.8.0h2",
    },
    'com.elogames.app' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "elo - board games for two",
        'Package_name' : "com.elogames.app",
        'installs' : "100,000+",
        'version' : "1.9.17",
    },
    'com.grability.rappi' :
    {
        'category' : "Food & Drink",
        'updated' : "11 Jun 2022",
        'App_name' : "Rappi",
        'Package_name' : "com.grability.rappi",
        'installs' : "50,000,000+",
        'version' : "Varies with device",
    },
    'com.linecorp.LGPKV' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "LINE \u30dd\u30b3\u30d1\u30f3\u30bf\u30a6\u30f3-\u30ef\u30f3\u30bf\u30c3\u30d7\u30d1\u30ba\u30eb\u3067\u30dd\u30b3\u30bf\u3068\u753a\u3065\u304f\u308a",
        'Package_name' : "com.linecorp.LGPKV",
        'installs' : "1,000,000+",
        'version' : "5.1.0",
    },
    'air.com.ace2three.mobile' :
    {
        'category' : "Games",
        'updated' : "12 May 2022",
        'App_name' : "A23 Games - Rummy | Fantasy",
        'Package_name' : "air.com.ace2three.mobile",
        'installs' : "10,000,000+",
        'version' : "7.0.7",
    },
    'com.libertex.mobile' :
    {
        'category' : "Finance",
        'updated' : "02 Mar 2022",
        'App_name' : "Libertex: Stocks & CFD Trading",
        'Package_name' : "com.libertex.mobile",
        'installs' : "5,000,000+",
        'version' : "2.30.0",
    },
    'com.getplus.application' :
    {
        'category' : "Lifestyle",
        'updated' : "20 May 2022",
        'App_name' : "GetPlus",
        'Package_name' : "com.getplus.application",
        'installs' : "100,000+",
        'version' : "3.4.6",
    },
    'com.fugo.wowsearch' :
    {
        'category' : "Games",
        'updated' : "29 May 2022",
        'App_name' : "Words of Wonders: Search",
        'Package_name' : "com.fugo.wowsearch",
        'installs' : "10,000,000+",
        'version' : "2.5.5",
    },
    'kr.neogames.realfarm' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "\ub808\uc54c\ud31c : \ud604\uc2e4\uacfc \ub611\uac19\uc740 \ub18d\uc0ac\uac8c\uc784",
        'Package_name' : "kr.neogames.realfarm",
        'installs' : "5,000,000+",
        'version' : "7.49",
    },
    'de.wetteronline.wetterapp' :
    {
        'category' : "Utilities",
        'updated' : "15 Jun 2022",
        'App_name' : "Weather & Radar - Storm radar",
        'Package_name' : "de.wetteronline.wetterapp",
        'installs' : "50,000,000+",
        'version' : "2022.12",
    },
    'com.youngplatform.young' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "Young Platform Step",
        'Package_name' : "com.youngplatform.young",
        'installs' : "500,000+",
        'version' : "4.8.4",
    },
    '1498000244' :
    {
        'category' : "Games",
        'updated' : "2022-06-02T05:05:32Z",
        'App_name' : "\ud53c\uad6c\uc655\ud1b5\ud0a4M",
        'Package_name' : "net.snowpipe.tongkeymobile",
        'version' : "1.9.49",
        'minimumOsVersion' : "9.0",
    },
    'com.mediawill.findalljob' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "\ubcbc\ub8e9\uc2dc\uc7a5 \u2013 AI \uae30\ubc18 \uc0dd\ud65c\ubc00\ucc29\ud615 \uc77c\uc790\ub9ac \ud50c\ub7ab\ud3fc",
        'Package_name' : "com.mediawill.findalljob",
        'installs' : "1,000,000+",
        'version' : "2.9.2",
    },
    'com.freeplay.runandfight' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "Join Clash 3D",
        'Package_name' : "com.freeplay.runandfight",
        'installs' : "100,000,000+",
        'version' : "2.34.2",
    },
    'com.appz.dukkuba' :
    {
        'category' : "House & Home",
        'updated' : "02 Jun 2022",
        'App_name' : "\ud53c\ud130\ud32c\uc758 \uc88b\uc740\ubc29 \uad6c\ud558\uae30",
        'Package_name' : "com.appz.dukkuba",
        'installs' : "1,000,000+",
        'version' : "2.15.17",
    },
    'kr.co.angames.astrokings.google.android' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "ASTROKINGS: Space War Strategy",
        'Package_name' : "kr.co.angames.astrokings.google.android",
        'installs' : "1,000,000+",
        'version' : "1.40-1284",
    },
    'com.vuclip.viu' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "Viu: Dramas, TV Shows & Movies",
        'Package_name' : "com.vuclip.viu",
        'installs' : "100,000,000+",
        'version' : "1.1.14",
    },
    'com.square_enix.android_googleplay.RSRSWW' :
    {
        'category' : "Games",
        'updated' : "06 May 2022",
        'App_name' : "Romancing SaGa Re;univerSe",
        'Package_name' : "com.square_enix.android_googleplay.RSRSWW",
        'installs' : "500,000+",
        'version' : "1.26.1",
    },
    '1501523707' :
    {
        'category' : "Health and Fitness",
        'updated' : "2022-06-16T14:51:27Z",
        'App_name' : "Saudi Sports for All",
        'Package_name' : "com.uxbert.sfa",
        'version' : "1.0.38",
        'minimumOsVersion' : "11.0",
    },
    'dating.dine.dine' :
    {
        'category' : "Social and Communication",
        'updated' : "28 Apr 2022",
        'App_name' : "Dine Dating App",
        'Package_name' : "dating.dine.dine",
        'installs' : "100,000+",
        'version' : "1.1.22",
    },
    'com.moveinsync.gettowork.officecommute' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "22 Apr 2020",
        'App_name' : "GetToWork - Reliable office commute cab service",
        'Package_name' : "com.moveinsync.gettowork.officecommute",
        'installs' : "10,000+",
        'version' : "2020.04.01",
    },
    '1474128649' :
    {
        'category' : "Finance",
        'updated' : "2022-06-15T05:29:14Z",
        'App_name' : "Turkiye Petrolleri",
        'Package_name' : "com.turkiyepetroller.TP",
        'version' : "2.0.22",
        'minimumOsVersion' : "10.0",
    },
    'com.qiyee.recruit' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "KUPU: Find Jobs, Hire Staff",
        'Package_name' : "com.qiyee.recruit",
        'installs' : "100,000+",
        'version' : "1.4.1",
    },
    'com.netacom.netalo' :
    {
        'category' : "Social and Communication",
        'updated' : "11 Jun 2022",
        'App_name' : "NetAlo",
        'Package_name' : "com.netacom.netalo",
        'installs' : "100,000+",
        'version' : "2.3.50",
    },
    'jp.Marvelous.ikkitousen' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "\u4e00\u9a0e\u5f53\u5343\u30a8\u30af\u30b9\u30c8\u30e9\u30d0\u30fc\u30b9\u30c8",
        'Package_name' : "jp.Marvelous.ikkitousen",
        'installs' : "100,000+",
        'version' : "1.4.115",
    },
    '1247451758' :
    {
        'category' : "Finance",
        'updated' : "2022-06-14T08:28:05Z",
        'App_name' : "Mashreq Neo - Bank easy",
        'Package_name' : "com.mashreq.neo",
        'version' : "3.3.31",
        'minimumOsVersion' : "11.0",
    },
    'com.sgzs.jp' :
    {
        'category' : "Games",
        'updated' : "25 Apr 2022",
        'App_name' : "\u4e09\u56fd\u5fd7\u30b0\u30ed\u30fc\u30d0\u30eb",
        'Package_name' : "com.sgzs.jp",
        'installs' : "10,000+",
        'version' : "1.18.6",
    },
    'com.namshi.android' :
    {
        'category' : "Shopping",
        'updated' : "17 Jun 2022",
        'App_name' : "Namshi - Shop Fashion & Beauty",
        'Package_name' : "com.namshi.android",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.tencent.longzu.jp' :
    {
        'category' : "Games",
        'updated' : "07 Apr 2022",
        'App_name' : "\u30b3\u30fc\u30c9\uff1a\u30c9\u30e9\u30b4\u30f3\u30d6\u30e9\u30c3\u30c9",
        'Package_name' : "com.tencent.longzu.jp",
        'installs' : "500,000+",
        'version' : "0.1.185",
    },
    'com.worldremit.android' :
    {
        'category' : "Finance",
        'updated' : "30 May 2022",
        'App_name' : "WorldRemit: Money Transfer App",
        'Package_name' : "com.worldremit.android",
        'installs' : "5,000,000+",
        'version' : "3.107.0.37137",
    },
    'com.tachyonbnt.ticker' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Feb 2022",
        'App_name' : "TICKER \ud2f0\ucee4 -  \ubdf0\ud2f0 \ub79c\uc120 \ub77c\uc774\ud504",
        'Package_name' : "com.tachyonbnt.ticker",
        'installs' : "100,000+",
        'version' : "1.3.1",
    },
    '1217507712' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-07T11:12:52Z",
        'App_name' : "McDonald's",
        'Package_name' : "com.mcdonalds.mobileapp",
        'version' : "2.43.0",
        'minimumOsVersion' : "13.0",
    },
    'com.amvg.hemlak' :
    {
        'category' : "House & Home",
        'updated' : "05 Jun 2022",
        'App_name' : "Hepsiemlak\u2013Ev & Emlak \u0130lanlar\u0131",
        'Package_name' : "com.amvg.hemlak",
        'installs' : "1,000,000+",
        'version' : "22.06.03.12.27.4001912",
    },
    'com.klab.bleach' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "Bleach: Brave Souls Anime Game",
        'Package_name' : "com.klab.bleach",
        'installs' : "10,000,000+",
        'version' : "13.11.0",
    },
    'com.trycaviar.customer' :
    {
        'category' : "Food & Drink",
        'updated' : "13 Jun 2022",
        'App_name' : "Caviar - Order Food Delivery",
        'Package_name' : "com.trycaviar.customer",
        'installs' : "1,000,000+",
        'version' : "15.58.13",
    },
    'jp.co.cybird.appli.android.gen.ja' :
    {
        'category' : "Games",
        'updated' : "18 May 2022",
        'App_name' : "\u30a4\u30b1\u30e1\u30f3\u6e90\u6c0f\u4f1d \u3042\u3084\u304b\u3057\u604b\u3048\u306b\u3057\u3000\u604b\u611b\u30b2\u30fc\u30e0",
        'Package_name' : "jp.co.cybird.appli.android.gen.ja",
        'installs' : "100,000+",
        'version' : "3.6.0",
    },
    'com.dealshare' :
    {
        'category' : "Shopping",
        'updated' : "12 Jun 2022",
        'App_name' : "DealShare - Made in India",
        'Package_name' : "com.dealshare",
        'installs' : "10,000,000+",
        'version' : "0.6.5",
    },
    'net.ilius.android.meetic' :
    {
        'category' : "Social and Communication",
        'updated' : "08 Jun 2022",
        'App_name' : "Meetic - Amour et Rencontre",
        'Package_name' : "net.ilius.android.meetic",
        'installs' : "10,000,000+",
        'version' : "5.74.0",
    },
    'com.takhfifan.takhfifan' :
    {
        'category' : "Lifestyle",
        'updated' : "10 Apr 2022",
        'App_name' : "Takhfifan",
        'Package_name' : "com.takhfifan.takhfifan",
        'installs' : "1,000,000+",
        'version' : "3.8.2",
    },
    'jp.dimedia.ftchat' :
    {
        'category' : "Lifestyle",
        'updated' : "25 May 2022",
        'App_name' : "\u5360\u3044 \u30a6\u30e9\u30fc\u30e9 - \u30c1\u30e3\u30c3\u30c8\u5360\u3044\u30fb\u604b\u611b\u76f8\u8ac7\u3084\u604b\u611b\u5360\u3044",
        'Package_name' : "jp.dimedia.ftchat",
        'installs' : "50,000+",
        'version' : "4.0.5",
    },
    'live.citymall.customer.prod' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "CityMall: Online Shopping App",
        'Package_name' : "live.citymall.customer.prod",
        'installs' : "5,000,000+",
        'version' : "1.31.4",
    },
    'br.com.fshero' :
    {
        'category' : "Tools",
        'updated' : "25 May 2022",
        'App_name' : "HERO: Antiv\u00edrus e Otimizador",
        'Package_name' : "br.com.fshero",
        'installs' : "500,000+",
        'version' : "3.36.0",
    },
    'com.liligo.android' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "16 Jun 2022",
        'App_name' : "Liligo - Flights & Cars",
        'Package_name' : "com.liligo.android",
        'installs' : "1,000,000+",
        'version' : "5.2.1",
    },
    '1023721594' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-16T22:35:15Z",
        'App_name' : "BiSU",
        'Package_name' : "tr.tektus.bisu",
        'version' : "8.0.4",
        'minimumOsVersion' : "11.0",
    },
    'com.viewlift.hoichoi' :
    {
        'category' : "Entertainment",
        'updated' : "14 Apr 2022",
        'App_name' : "hoichoi - Movies & Web Series",
        'Package_name' : "com.viewlift.hoichoi",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.naturalmotion.customstreetracer2' :
    {
        'category' : "Games",
        'updated' : "10 May 2022",
        'App_name' : "CSR 2 - Drag Racing Car Games",
        'Package_name' : "com.naturalmotion.customstreetracer2",
        'installs' : "50,000,000+",
        'version' : "3.9.0",
    },
    '1279682900' :
    {
        'category' : "Games",
        'updated' : "2022-03-16T05:13:03Z",
        'App_name' : "\ud310\ud0c0\uc9c0\ud0c0\uc6b4",
        'Package_name' : "com.arumgames.fantasytown",
        'version' : "1.5.3",
        'minimumOsVersion' : "10.0",
    },
    'ru.migcredit.ma' :
    {
        'category' : "Finance",
        'updated' : "02 Jun 2022",
        'App_name' : "\u041c\u0438\u0433\u041a\u0440\u0435\u0434\u0438\u0442 \u0417\u0430\u0439\u043c\u044b \u043e\u043d\u043b\u0430\u0439\u043d",
        'Package_name' : "ru.migcredit.ma",
        'installs' : "1,000,000+",
        'version' : "2.1.65",
    },
    'com.hermes.chunqiu' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "\u6625\u79cbM - \u6625\u79cb\u6230\u570b\u7b56\u7565\u9769\u65b0\u4e4b\u4f5c",
        'Package_name' : "com.hermes.chunqiu",
        'installs' : "100,000+",
        'version' : "1.5.0",
    },
    'com.bigbluebubble.msm2' :
    {
        'category' : "Games",
        'updated' : "17 Dec 2021",
        'App_name' : "Singing Monsters: Dawn of Fire",
        'Package_name' : "com.bigbluebubble.msm2",
        'installs' : "5,000,000+",
        'version' : "2.8.0",
    },
    '1090814275' :
    {
        'category' : "Games",
        'updated' : "2022-04-28T01:04:07Z",
        'App_name' : "\ub9ac\ub2c8\uc9c02 \ub808\ubcfc\ub8e8\uc158",
        'Package_name' : "com.netmarble.lineageII",
        'version' : "0.82.12",
        'minimumOsVersion' : "10.0",
    },
    'com.kpm.gardenblast' :
    {
        'category' : "Games",
        'updated' : "08 Jul 2021",
        'App_name' : "Garden Blast",
        'Package_name' : "com.kpm.gardenblast",
        'installs' : "50+",
        'version' : "1.8",
    },
    'in.android.vyapar' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "Invoice Billing GST Accounting",
        'Package_name' : "in.android.vyapar",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.luckyyes.fun' :
    {
        'category' : "Lifestyle",
        'updated' : "24 Feb 2020",
        'App_name' : "Lucky Yes",
        'Package_name' : "com.luckyyes.fun",
        'installs' : "1,000+",
        'version' : "0.2020.222",
    },
    'com.applemoncash' :
    {
        'category' : "Finance",
        'updated' : "11 Jun 2022",
        'App_name' : "Lemon Cash: Compr\u00e1 Crypto",
        'Package_name' : "com.applemoncash",
        'installs' : "1,000,000+",
        'version' : "2.6.35",
    },
    'com.ifit.wolf' :
    {
        'category' : "Health and Fitness",
        'updated' : "15 Jun 2022",
        'App_name' : "iFIT: At Home Fitness Coach",
        'Package_name' : "com.ifit.wolf",
        'installs' : "1,000,000+",
        'version' : "2.6.78",
    },
    '964118383' :
    {
        'category' : "Entertainment",
        'updated' : "2022-05-24T05:01:42Z",
        'App_name' : "360 VUZ: Immersive Video Views",
        'Package_name' : "com.creativeinnovations.360mea",
        'version' : "4.17.8",
        'minimumOsVersion' : "11.0",
    },
    'com.mvp.rich' :
    {
        'category' : "Entertainment",
        'updated' : "17 Mar 2022",
        'App_name' : "Rich",
        'Package_name' : "com.mvp.rich",
        'installs' : "1,000+",
        'version' : "1.5.5",
    },
    'yesmadamservices.app.com.yesmadamservices' :
    {
        'category' : "Social and Communication",
        'updated' : "15 Jun 2022",
        'App_name' : "Yes Madam -Salon & Spa At Home",
        'Package_name' : "yesmadamservices.app.com.yesmadamservices",
        'installs' : "500,000+",
        'version' : "4.4.6",
    },
    'br.com.tecnonutri.app' :
    {
        'category' : "Health and Fitness",
        'updated' : "01 Jun 2022",
        'App_name' : "Tecnonutri: Encontre sua dieta",
        'Package_name' : "br.com.tecnonutri.app",
        'installs' : "5,000,000+",
        'version' : "4.13.2",
    },
    'com.xiaojukeji.didi.global.customer' :
    {
        'category' : "Food & Drink",
        'updated' : "15 Jun 2022",
        'App_name' : "DiDi Food: Express Delivery",
        'Package_name' : "com.xiaojukeji.didi.global.customer",
        'installs' : "10,000,000+",
        'version' : "1.3.34",
    },
    'com.rxsj.an.en4399' :
    {
        'category' : "Games",
        'updated' : "05 May 2022",
        'App_name' : "Yong Heroes",
        'Package_name' : "com.rxsj.an.en4399",
        'installs' : "1,000,000+",
        'version' : "1.6.6.001",
    },
    '1359124769' :
    {
        'category' : "Games",
        'updated' : "2022-06-20T02:42:20Z",
        'App_name' : "Days of Empire",
        'Package_name' : "com.onemt.boe.tr",
        'version' : "2.44.001",
        'minimumOsVersion' : "9.0",
    },
    'kr.co.lunasoft.showa' :
    {
        'category' : "Shopping",
        'updated' : "21 Apr 2022",
        'App_name' : "SHOWA",
        'Package_name' : "kr.co.lunasoft.showa",
        'installs' : "100,000+",
        'version' : "1.4.0",
    },
    'com.telugumatrimony' :
    {
        'category' : "Social and Communication",
        'updated' : "21 May 2022",
        'App_name' : "Telugu Matrimony\u00ae-Marriage App",
        'Package_name' : "com.telugumatrimony",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.movefastcompany.bora' :
    {
        'category' : "Entertainment",
        'updated' : "10 Jun 2022",
        'App_name' : "Hakuna: Live Streams and Chat",
        'Package_name' : "com.movefastcompany.bora",
        'installs' : "10,000,000+",
        'version' : "1.70.3",
    },
    'com.pokerstars.cebo' :
    {
        'category' : "Casino",
        'updated' : "14 Jun 2022",
        'App_name' : "Jackpot Poker by PokerStars\u2122",
        'Package_name' : "com.pokerstars.cebo",
        'installs' : "1,000,000+",
        'version' : "6.2.18",
    },
    'com.gamess.and' :
    {
        'category' : "Games",
        'updated' : "25 Nov 2020",
        'App_name' : "\u6700\u5f8c\u7684\u4ed9\u4fe0-\u6ec5\u4e16\u9304",
        'Package_name' : "com.gamess.and",
        'installs' : "100,000+",
        'version' : "1.0.19",
    },
    'com.voocle.now_remaster' :
    {
        'category' : "Games",
        'updated' : "15 Apr 2022",
        'App_name' : "\ub098\uc6b0 \ub9ac\ub9c8\uc2a4\ud130",
        'Package_name' : "com.voocle.now_remaster",
        'installs' : "10,000+",
        'version' : "1.2.1.162",
    },
    'com.funtapgame.kart.rushplus.kartm' :
    {
        'category' : "Games",
        'updated' : "08 Jan 2022",
        'App_name' : "KartRider Rush+ Funtap",
        'Package_name' : "com.funtapgame.kart.rushplus.kartm",
        'installs' : "100,000+",
        'version' : "1.11.14",
    },
    'trivia.site.qqq' :
    {
        'category' : "Entertainment",
        'updated' : "17 Feb 2021",
        'App_name' : "\u30c8\u30ea\u30c8\u30e2 - SNS\u3067\u3059\u3050\u76db\u308a\u4e0a\u304c\u308b\u7537\u5973\u306e\u604b\u611b\u30c8\u30ea\u30d3\u30a2",
        'Package_name' : "trivia.site.qqq",
        'installs' : "100,000+",
        'version' : "1.1.0.0",
    },
    'com.kisankonnect.in' :
    {
        'category' : "Shopping",
        'updated' : "02 Jun 2022",
        'App_name' : "KisanKonnect Safeplace For Fresh Fruits & Veggies",
        'Package_name' : "com.kisankonnect.in",
        'installs' : "500,000+",
        'version' : "0.0.62",
    },
    'com.inventia.app' :
    {
        'category' : "Education and Books",
        'updated' : "14 Jun 2022",
        'App_name' : "Current Affairs & GK 2022",
        'Package_name' : "com.inventia.app",
        'installs' : "1,000,000+",
        'version' : "2.8.21",
    },
    'qureka.live.game.show' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "Qureka: Play Quizzes & Learn",
        'Package_name' : "qureka.live.game.show",
        'installs' : "10,000,000+",
        'version' : "3.1.78",
    },
    'com.actionfit.athena' :
    {
        'category' : "Games",
        'updated' : "16 May 2022",
        'App_name' : "Jewel Athena: Match 3 blast",
        'Package_name' : "com.actionfit.athena",
        'installs' : "1,000,000+",
        'version' : "1.7.2",
    },
    'br.com.timbrasil.timmaisvantagens' :
    {
        'category' : "Tools",
        'updated' : "01 Jun 2022",
        'App_name' : "TIM + Vantagens",
        'Package_name' : "br.com.timbrasil.timmaisvantagens",
        'installs' : "1,000,000+",
        'version' : "1.1.97",
    },
    'com.btg.pactual.digital.mobile' :
    {
        'category' : "Finance",
        'updated' : "11 Jun 2022",
        'App_name' : "BTG Pactual Investimentos",
        'Package_name' : "com.btg.pactual.digital.mobile",
        'installs' : "1,000,000+",
        'version' : "9.00.30",
    },
    'com.yalla.yallagames' :
    {
        'category' : "Games",
        'updated' : "09 May 2022",
        'App_name' : "Yalla Ludo - Ludo&Domino",
        'Package_name' : "com.yalla.yallagames",
        'installs' : "100,000,000+",
        'version' : "1.3.1.0",
    },
    'com.nexon.axe.global' :
    {
        'category' : "Games",
        'updated' : "25 Feb 2022",
        'App_name' : "AxE: Alliance vs Empire",
        'Package_name' : "com.nexon.axe.global",
        'installs' : "1,000,000+",
        'version' : "4.05.00",
    },
    'com.gamamobi.iktw' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "\u7121\u76e1\u57ce\u6230-Infinity Kingdom",
        'Package_name' : "com.gamamobi.iktw",
        'installs' : "50,000+",
        'version' : "2.1.1",
    },
    'com.skylinedynamics.bk' :
    {
        'category' : "Food & Drink",
        'updated' : "13 Jun 2022",
        'App_name' : "Burger King Arabia",
        'Package_name' : "com.skylinedynamics.bk",
        'installs' : "500,000+",
        'version' : "5.0.5",
    },
    'com.withyotta.yotta' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "Yotta",
        'Package_name' : "com.withyotta.yotta",
        'installs' : "100,000+",
        'version' : "5.4.6",
    },
    '1261770407' :
    {
        'category' : "Social and Communication",
        'updated' : "2020-02-05T16:11:10Z",
        'App_name' : "\u51fa\u4f1a\u3044\u7cfb\u30a2\u30d7\u30ea\u300c\u3042\u3044\u307e\u3059\u300d\u53cb\u9054\u4f5c\u308a\u30c8\u30fc\u30af",
        'Package_name' : "jp.aimasu.chat",
        'version' : "2.0.0",
        'minimumOsVersion' : "10.0",
    },
    'com.cultsotry.yanolja.nativeapp' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "07 Jun 2022",
        'App_name' : "\uc57c\ub180\uc790 - \ub204\uad6c\ub098 \ub9c8\uc74c \ud3b8\ud788 \ub180 \uc218 \uc788\uac8c",
        'Package_name' : "com.cultsotry.yanolja.nativeapp",
        'installs' : "10,000,000+",
        'version' : "8.16.5",
    },
    'fr.leaderprice.leaderprice' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "Le Club Leader Price",
        'Package_name' : "fr.leaderprice.leaderprice",
        'installs' : "500,000+",
        'version' : "5.6.3",
    },
    'com.zodiactouch' :
    {
        'category' : "Lifestyle",
        'updated' : "10 Jun 2022",
        'App_name' : "Zodiac Touch - Psychic Tarot",
        'Package_name' : "com.zodiactouch",
        'installs' : "500,000+",
        'version' : "5.3.0",
    },
    'com.gravity.gtlor.aos' :
    {
        'category' : "Games",
        'updated' : "03 May 2022",
        'App_name' : "Ragnarok: Labyrinth",
        'Package_name' : "com.gravity.gtlor.aos",
        'installs' : "500,000+",
        'version' : "45.2425.0",
    },
    'vng.games.omg.sanguobawang.xm.threekingdoms' :
    {
        'category' : "Games",
        'updated' : "01 Dec 2021",
        'App_name' : "OMG!\u4e09\u56fd\u9738\u738b \u65b0\u9a6c\u7248 - Gods of Three Kingdoms",
        'Package_name' : "vng.games.omg.sanguobawang.xm.threekingdoms",
        'installs' : "10,000+",
        'version' : "0.40.61",
    },
    'com.earlysalary.android' :
    {
        'category' : "Finance",
        'updated' : "19 Apr 2022",
        'App_name' : "Instant Personal Loan App",
        'Package_name' : "com.earlysalary.android",
        'installs' : "5,000,000+",
        'version' : "2.9.0",
    },
    'com.axmor.spottytime' :
    {
        'category' : "Health and Fitness",
        'updated' : "10 Mar 2021",
        'App_name' : "Spotty Time - Limit Screen Time and Block Apps",
        'Package_name' : "com.axmor.spottytime",
        'installs' : "5,000+",
        'version' : "1.1.0",
    },
    'com.gameeapp.android.app' :
    {
        'category' : "Casino",
        'updated' : "02 Jun 2022",
        'App_name' : "GAMEE Prizes: Real Cash Games",
        'Package_name' : "com.gameeapp.android.app",
        'installs' : "10,000,000+",
        'version' : "4.14.1",
    },
    'com.dbs.in.digitalbank' :
    {
        'category' : "Finance",
        'updated' : "09 Jun 2022",
        'App_name' : "digibank by DBS India",
        'Package_name' : "com.dbs.in.digitalbank",
        'installs' : "5,000,000+",
        'version' : "4.19.2",
    },
    'paint.by.number.pixel.art.coloring.drawing.puzzle' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Paint by Number Coloring Games",
        'Package_name' : "paint.by.number.pixel.art.coloring.drawing.puzzle",
        'installs' : "100,000,000+",
        'version' : "2.77.7",
    },
    '1500564080' :
    {
        'category' : "Games",
        'updated' : "2022-06-16T02:06:13Z",
        'App_name' : "Home Restoration",
        'Package_name' : "com.panteon.homesweethome",
        'version' : "2.12",
        'minimumOsVersion' : "11.0",
    },
    '1493956050' :
    {
        'category' : "Finance",
        'updated' : "2022-06-10T02:02:41Z",
        'App_name' : "WeLab Bank \u2013 \u9999\u6e2f\u865b\u64ec\u9280\u884c",
        'Package_name' : "welab.bank",
        'version' : "1.5.25",
        'minimumOsVersion' : "10.3",
    },
    'com.webbyte.mytokri' :
    {
        'category' : "Shopping",
        'updated' : "04 Jan 2020",
        'App_name' : "MyTokri - Best Deals, Coupons",
        'Package_name' : "com.webbyte.mytokri",
        'installs' : "100,000+",
        'version' : "2.8.7",
    },
    'and.onemt.war.ar' :
    {
        'category' : "Games",
        'updated' : "21 May 2022",
        'App_name' : "\u0627\u0646\u062a\u0642\u0627\u0645 \u0627\u0644\u0633\u0644\u0627\u0637\u064a\u0646",
        'Package_name' : "and.onemt.war.ar",
        'installs' : "10,000,000+",
        'version' : "1.16.18",
    },
    'com.u2opia.woo' :
    {
        'category' : "Lifestyle",
        'updated' : "17 Jun 2022",
        'App_name' : "Woo - 30+ USA Dating and Love",
        'Package_name' : "com.u2opia.woo",
        'installs' : "10,000,000+",
        'version' : "3.10.36",
    },
    'au.com.aussiecommerce.luxuryescapes' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "16 Dec 2021",
        'App_name' : "Luxury Escapes - Travel Deals",
        'Package_name' : "au.com.aussiecommerce.luxuryescapes",
        'installs' : "500,000+",
        'version' : "4.28.0",
    },
    'com.disrapp.coinkeeper3' :
    {
        'category' : "Finance",
        'updated' : "11 Apr 2022",
        'App_name' : "CoinKeeper \u2014 expense tracker",
        'Package_name' : "com.disrapp.coinkeeper3",
        'installs' : "1,000,000+",
        'version' : "0.9.8.247",
    },
    'com.healthifyme.basic' :
    {
        'category' : "Health and Fitness",
        'updated' : "09 Jun 2022",
        'App_name' : "HealthifyMe \u2013 Calorie Counter",
        'Package_name' : "com.healthifyme.basic",
        'installs' : "10,000,000+",
        'version' : "v18.1",
    },
    'com.vgm.muh5' :
    {
        'category' : "Games",
        'updated' : "20 Dec 2021",
        'App_name' : "MU \u0110\u1ea1i Thi\u00ean S\u1ee9 H5",
        'Package_name' : "com.vgm.muh5",
        'installs' : "5,000,000+",
        'version' : "1.5",
    },
    'com.cyou.illusionc.gp' :
    {
        'category' : "Games",
        'updated' : "04 Mar 2022",
        'App_name' : "\uc77c\ub8e8\uc804 \ucee4\ub125\ud2b8",
        'Package_name' : "com.cyou.illusionc.gp",
        'installs' : "100,000+",
        'version' : "1.0.31",
    },
    'com.healthpass.production.ocbc' :
    {
        'category' : "Health and Fitness",
        'updated' : "29 Apr 2022",
        'App_name' : "HealthPass by OCBC",
        'Package_name' : "com.healthpass.production.ocbc",
        'installs' : "10,000+",
        'version' : "1.0.750",
    },
    'com.lottehomeshopping.fc' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "iTOO (\uc544\uc774\ud22c) - \ub098\uc758 \uccb4\ud615\uacfc \ucde8\ud5a5\uc5d0 \ub531 \ub9de\ub294 \ud328\uc158 \ud050\ub808\uc774\uc158 \ud50c\ub7ab\ud3fc",
        'Package_name' : "com.lottehomeshopping.fc",
        'installs' : "100,000+",
        'version' : "1.4.1",
    },
    'vn.vplay.cuumong.t002' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "C\u1eedu M\u1ed9ng Ti\u00ean V\u1ef1c",
        'Package_name' : "vn.vplay.cuumong.t002",
        'installs' : "100,000+",
        'version' : "1.4",
    },
    'com.plop.chat.stories' :
    {
        'category' : "Education and Books",
        'updated' : "25 Jun 2021",
        'App_name' : "Plop Chat - Text Stories",
        'Package_name' : "com.plop.chat.stories",
        'installs' : "100,000+",
        'version' : "3.44",
    },
    'com.prudential.pulse.onepulse' :
    {
        'category' : "Health and Fitness",
        'updated' : "15 Jun 2022",
        'App_name' : "We Do Pulse - Health & Fitness Solutions",
        'Package_name' : "com.prudential.pulse.onepulse",
        'installs' : "10,000,000+",
        'version' : "1.2.28",
    },
    'com.studyworld.sb.customer' :
    {
        'category' : "Education and Books",
        'updated' : "29 Apr 2022",
        'App_name' : "Studybay Expert Homework Help",
        'Package_name' : "com.studyworld.sb.customer",
        'installs' : "10,000+",
        'version' : "2.3.0",
    },
    'com.makemytrip' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "11 May 2022",
        'App_name' : "MakeMyTrip Hotels, Flight, Bus",
        'Package_name' : "com.makemytrip",
        'installs' : "50,000,000+",
        'version' : "8.7.6",
    },
    'com.mavshack' :
    {
        'category' : "Entertainment",
        'updated' : "15 Mar 2022",
        'App_name' : "Mavshack",
        'Package_name' : "com.mavshack",
        'installs' : "100,000+",
        'version' : "4.1.5.3",
    },
    'm.ntonsGP.ds' :
    {
        'category' : "Games",
        'updated' : "06 Jul 2021",
        'App_name' : "Dice Saber - Turn-based Strategy RPG",
        'Package_name' : "m.ntonsGP.ds",
        'installs' : "10,000+",
        'version' : "1.4.5",
    },
    'goova.sofisa.client.v2' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Sofisa Direto: CDB, LCI e mais",
        'Package_name' : "goova.sofisa.client.v2",
        'installs' : "1,000,000+",
        'version' : "3.0.80",
    },
    'com.tcm.supergoalon' :
    {
        'category' : "Games",
        'updated' : "13 Sep 2020",
        'App_name' : "Goalon - Live Sports Euro 2020",
        'Package_name' : "com.tcm.supergoalon",
        'installs' : "100,000+",
        'version' : "2.6.1",
    },
    'com.avex.pine.towerdefense' :
    {
        'category' : "Games",
        'updated' : "22 Apr 2022",
        'App_name' : "\u5143\u7956\uff01\u3000\u304a\u305d\u677e\u3055\u3093\u306e\u3078\u305d\u304f\u308a\u30a6\u30a9\u30fc\u30ba\uff5e\u30cb\u30fc\u30c8\u306e\u653b\u9632\uff5e",
        'Package_name' : "com.avex.pine.towerdefense",
        'installs' : "500,000+",
        'version' : "5.6.1",
    },
    'com.quok.blobHero' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Blob Hero",
        'Package_name' : "com.quok.blobHero",
        'installs' : "1,000,000+",
        'version' : "0.6.5",
    },
    'com.blinkslabs.blinkist.android' :
    {
        'category' : "Education and Books",
        'updated' : "14 Jun 2022",
        'App_name' : "Blinkist: Big Ideas in 15 Min",
        'Package_name' : "com.blinkslabs.blinkist.android",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'kr.co.sks.finlab.wealth' :
    {
        'category' : "Finance",
        'updated' : "14 Apr 2022",
        'App_name' : "SK\uc99d\uad8c \uc8fc\ud30c\uc218 \uc6f0\uc2a4(\uae08\uc735\uc0c1\ud488, \uc790\uc0b0\uad00\ub9ac, \uacc4\uc88c\uac1c\uc124 \ub4f1)",
        'Package_name' : "kr.co.sks.finlab.wealth",
        'installs' : "10,000+",
        'version' : "1.00.05",
    },
    'com.gworld.crazyspin' :
    {
        'category' : "Games",
        'updated' : "09 May 2022",
        'App_name' : "Crazy Fox - Big win",
        'Package_name' : "com.gworld.crazyspin",
        'installs' : "1,000,000+",
        'version' : "2.1.10.0",
    },
    'com.rubarb.app' :
    {
        'category' : "Finance",
        'updated' : "28 Feb 2022",
        'App_name' : "rubarb",
        'Package_name' : "com.rubarb.app",
        'installs' : "50,000+",
        'version' : "2.5.0",
    },
    'com.huya.nimo' :
    {
        'category' : "Entertainment",
        'updated' : "15 Jun 2022",
        'App_name' : "Nimo TV - Live Game Streaming",
        'Package_name' : "com.huya.nimo",
        'installs' : "50,000,000+",
        'version' : "1.10.40",
    },
    'com.jp.lzjh' :
    {
        'category' : "Games",
        'updated' : "11 Nov 2021",
        'App_name' : "\u6b32\u671b\u90fd\u5e02",
        'Package_name' : "com.jp.lzjh",
        'installs' : "500,000+",
        'version' : "17",
    },
    'com.yoozoogames.fwgame' :
    {
        'category' : "Games",
        'updated' : "24 Apr 2022",
        'App_name' : "Forsaken World: Gods&Demons",
        'Package_name' : "com.yoozoogames.fwgame",
        'installs' : "1,000,000+",
        'version' : "5.0.0",
    },
    'net.lovoo.android' :
    {
        'category' : "Social and Communication",
        'updated' : "07 Jun 2022",
        'App_name' : "LOVOO - Chat, date & find love",
        'Package_name' : "net.lovoo.android",
        'installs' : "50,000,000+",
        'version' : "127.0",
    },
    '357852748' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-08T11:41:16Z",
        'App_name' : "FREENOW (rejoint par Kapten)",
        'Package_name' : "de.intelligentapps.ott.client",
        'version' : "11.38.0",
        'minimumOsVersion' : "13.0",
    },
    'com.jmvas.jagmee' :
    {
        'category' : "Social and Communication",
        'updated' : "13 May 2022",
        'App_name' : "Jagmee Short Video",
        'Package_name' : "com.jmvas.jagmee",
        'installs' : "100,000+",
        'version' : "1.0.24",
    },
    'com.mobisystems.office' :
    {
        'category' : "Finance",
        'updated' : "31 May 2022",
        'App_name' : "OfficeSuite: Word, Sheets, PDF",
        'Package_name' : "com.mobisystems.office",
        'installs' : "100,000,000+",
        'version' : "12.5.42057",
    },
    'id.flip' :
    {
        'category' : "Finance",
        'updated' : "31 May 2022",
        'App_name' : "Flip: Bebas Biaya Transfer",
        'Package_name' : "id.flip",
        'installs' : "10,000,000+",
        'version' : "2.5.0",
    },
    'com.niyo.equitassavingsaccount' :
    {
        'category' : "Finance",
        'updated' : "17 Jun 2022",
        'App_name' : "NiyoX - Digital Banking",
        'Package_name' : "com.niyo.equitassavingsaccount",
        'installs' : "1,000,000+",
        'version' : "1.1.4",
    },
    'com.global.tmslg' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "Puzzles & Conquest",
        'Package_name' : "com.global.tmslg",
        'installs' : "1,000,000+",
        'version' : "5.0.70",
    },
    'com.linecorp.LGBB2' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "LINE Bubble 2",
        'Package_name' : "com.linecorp.LGBB2",
        'installs' : "10,000,000+",
        'version' : "3.9.1.22",
    },
    'com.kingsgroup.ss.kr' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "S.O.S: \uc2a4\ud14c\uc774\ud2b8 \uc624\ube0c \uc11c\ubc14\uc774\ubc8c",
        'Package_name' : "com.kingsgroup.ss.kr",
        'installs' : "1,000,000+",
        'version' : "1.16.10",
    },
    'de.payback.client.android' :
    {
        'category' : "Shopping",
        'updated' : "02 Jun 2022",
        'App_name' : "PAYBACK - Karte und Coupons",
        'Package_name' : "de.payback.client.android",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'jp.colopl.wcat' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\u767d\u732b\u30d7\u30ed\u30b8\u30a7\u30af\u30c8",
        'Package_name' : "jp.colopl.wcat",
        'installs' : "5,000,000+",
        'version' : "4.13.0",
    },
    'com.korea.datealive.gp' :
    {
        'category' : "Games",
        'updated' : "05 Feb 2021",
        'App_name' : "\ub370\uc774\ud2b8 \uc5b4 \ub77c\uc774\ube0c: \ub2e4\uc2dc \ub9cc\ub09c \uc815\ub839",
        'Package_name' : "com.korea.datealive.gp",
        'installs' : "100,000+",
        'version' : "1.09",
    },
    'com.tap4fun.odin.kingdomguard' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "Kingdom Guard",
        'Package_name' : "com.tap4fun.odin.kingdomguard",
        'installs' : "5,000,000+",
        'version' : "1.0.258",
    },
    'com.meetai.dating.android' :
    {
        'category' : "Social and Communication",
        'updated' : "31 May 2022",
        'App_name' : "Fika \u2013 Dating & Make friends",
        'Package_name' : "com.meetai.dating.android",
        'installs' : "500,000+",
        'version' : "2.11.0",
    },
    'my.com.goshop.app' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "Go Shop - Online Shopping App\u200b",
        'Package_name' : "my.com.goshop.app",
        'installs' : "1,000,000+",
        'version' : "6.0.1",
    },
    'ru.sbcs.store' :
    {
        'category' : "Food & Drink",
        'updated' : "16 Jun 2022",
        'App_name' : "\u0421\u0430\u043c\u043e\u043a\u0430\u0442\u30fb\u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u043e\u0432\u30fb\u0435\u0434\u044b",
        'Package_name' : "ru.sbcs.store",
        'installs' : "5,000,000+",
        'version' : "3.52.0",
    },
    'com.efun.kingdom.gp.sea' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "Kingdom: The Blood Pledge",
        'Package_name' : "com.efun.kingdom.gp.sea",
        'installs' : "100,000+",
        'version' : "1.00.20",
    },
    'com.planetart.fpuk' :
    {
        'category' : "Entertainment",
        'updated' : "09 Jun 2022",
        'App_name' : "FreePrints",
        'Package_name' : "com.planetart.fpuk",
        'installs' : "5,000,000+",
        'version' : "3.55.0",
    },
    'com.gamevil.psrforkakao' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "\ubcc4\uc774\ub418\uc5b4\ub77c!",
        'Package_name' : "com.gamevil.psrforkakao",
        'installs' : "5,000,000+",
        'version' : "8.3.0",
    },
    '288113403' :
    {
        'category' : "Tools",
        'updated' : "2022-06-13T08:35:04Z",
        'App_name' : "iTranslate Translator",
        'Package_name' : "com.outerspaceapps.itranslate",
        'version' : "14.2.5",
        'minimumOsVersion' : "10.0",
    },
    'ru.perekrestok.app' :
    {
        'category' : "Food & Drink",
        'updated' : "07 Jun 2022",
        'App_name' : "\u041f\u0435\u0440\u0435\u043a\u0440\u0435\u0441\u0442\u043e\u043a \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u043e\u0432",
        'Package_name' : "ru.perekrestok.app",
        'installs' : "5,000,000+",
        'version' : "3.31.0",
    },
    'com.feelingtouch.bn' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Battle Night: Cyberpunk RPG",
        'Package_name' : "com.feelingtouch.bn",
        'installs' : "1,000,000+",
        'version' : "1.5.26",
    },
    'com.jp.rings' :
    {
        'category' : "Games",
        'updated' : "18 Jan 2022",
        'App_name' : "\u30ec\u30b8\u30a7\u30f3\u30c9\u30aa\u30d6\u30ea\u30f3\u30b0",
        'Package_name' : "com.jp.rings",
        'installs' : "100,000+",
        'version' : "3.81.1",
    },
    'mmv.mmtravel' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "10 Jun 2022",
        'App_name' : "mmtravel | City | Wanderlust",
        'Package_name' : "mmv.mmtravel",
        'installs' : "10,000+",
        'version' : "2022.06-c",
    },
    'com.km.karaoke' :
    {
        'category' : "Entertainment",
        'updated' : "23 May 2022",
        'App_name' : "Sing Now, H\u00e1t kara livestream",
        'Package_name' : "com.km.karaoke",
        'installs' : "1,000,000+",
        'version' : "7.23.0.990",
    },
    'com.espritgames.rpg.dark.genesis' :
    {
        'category' : "Games",
        'updated' : "21 Jun 2021",
        'App_name' : "Dark Genesis: RPG IDLE MOBA",
        'Package_name' : "com.espritgames.rpg.dark.genesis",
        'installs' : "100,000+",
        'version' : "1.0.8",
    },
    'com.xlegend.grandfantasia.global' :
    {
        'category' : "Games",
        'updated' : "11 May 2022",
        'App_name' : "Sprite Fantasia - MMORPG",
        'Package_name' : "com.xlegend.grandfantasia.global",
        'installs' : "500,000+",
        'version' : "8.6.902",
    },
    'com.rastargames.pnlyjp' :
    {
        'category' : "Games",
        'updated' : "28 Apr 2022",
        'App_name' : "\u30d1\u30cb\u30ea\u30e4\u30fb\u30b6\u30fb\u30ea\u30d0\u30a4\u30d0\u30eb",
        'Package_name' : "com.rastargames.pnlyjp",
        'installs' : "50,000+",
        'version' : "2.8.402",
    },
    'com.fundoshiparade.majinmansion' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "Mystic Mansion",
        'Package_name' : "com.fundoshiparade.majinmansion",
        'installs' : "1,000,000+",
        'version' : "3.3.1",
    },
    'com.ncsoft.universeapp' :
    {
        'category' : "Entertainment",
        'updated' : "17 Jun 2022",
        'App_name' : "UNIVERSE",
        'Package_name' : "com.ncsoft.universeapp",
        'installs' : "10,000,000+",
        'version' : "1.0.2",
    },
    '1215933788' :
    {
        'category' : "Games",
        'updated' : "2022-06-08T12:49:38Z",
        'App_name' : "Scrabble\u00ae GO - New Word Game",
        'Package_name' : "com.pieyel.scrabble",
        'version' : "1.47.5",
        'minimumOsVersion' : "11.0",
    },
    'vn.kredivo.android' :
    {
        'category' : "Finance",
        'updated' : "21 May 2022",
        'App_name' : "Kredivo Vietnam",
        'Package_name' : "vn.kredivo.android",
        'installs' : "500,000+",
        'version' : "0.0.9",
    },
    'com.blinctrip.android' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "08 Jun 2022",
        'App_name' : "Blinctrip - Flight Ticket Booking & Trip Planner",
        'Package_name' : "com.blinctrip.android",
        'installs' : "500,000+",
        'version' : "0.0.83",
    },
    'deezer.android.app' :
    {
        'category' : "Entertainment",
        'updated' : "13 Jun 2022",
        'App_name' : "Deezer: Music & Podcast Player",
        'Package_name' : "deezer.android.app",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.generalmills.btfe' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "Box Tops for Education\u2122",
        'Package_name' : "com.generalmills.btfe",
        'installs' : "1,000,000+",
        'version' : "4.68.0 (2022060303)",
    },
    'jp.co.cyber_z.openrecviewapp' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "OPENREC.tv -Gaming Videos&Live",
        'Package_name' : "jp.co.cyber_z.openrecviewapp",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    '1548849305' :
    {
        'category' : "Games",
        'updated' : "2022-06-09T02:01:34Z",
        'App_name' : "Princess Tale",
        'Package_name' : "com.gamepub.pt",
        'version' : "2.2.04",
        'minimumOsVersion' : "10.0",
    },
    'jp.co.yoozoo.sengokulove' :
    {
        'category' : "Games",
        'updated' : "23 Feb 2022",
        'App_name' : "\u6210\u308a\u4e0a\u304c\u308a\u3000\u83ef\u3068\u6b66\u306e\u6226\u56fd",
        'Package_name' : "jp.co.yoozoo.sengokulove",
        'installs' : "1,000,000+",
        'version' : "1.0.57",
    },
    '589328270' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-07T13:29:17Z",
        'App_name' : "QuickThoughts - Earn Rewards",
        'Package_name' : "com.ssi.PanelistApplication",
        'version' : "2.33.0",
        'minimumOsVersion' : "13.0",
    },
    '1485501260' :
    {
        'category' : "Games",
        'updated' : "2020-10-17T00:49:28Z",
        'App_name' : "Homer City",
        'Package_name' : "com.dubtgames.homercity",
        'version' : "2.16.9",
        'minimumOsVersion' : "10.0",
    },
    'com.playbest.sgsmjz' :
    {
        'category' : "Games",
        'updated' : "11 Mar 2022",
        'App_name' : "\u4e09\u570b\u6bba\u540d\u5c07\u50b3-\u5a01\u529b\u52a0\u5f37\u7248",
        'Package_name' : "com.playbest.sgsmjz",
        'installs' : "100,000+",
        'version' : "0.3.22",
    },
    'com.khelplay.rummy' :
    {
        'category' : "Games",
        'updated' : "14 May 2022",
        'App_name' : "KhelPlay Rummy - Online Rummy, Indian Rummy App",
        'Package_name' : "com.khelplay.rummy",
        'installs' : "1,000,000+",
        'version' : "1.8.1",
    },
    '1491127905' :
    {
        'category' : "Games",
        'updated' : "2021-01-22T12:09:44Z",
        'App_name' : "Color Wall",
        'Package_name' : "com.Gameberry.ColorWall",
        'version' : "2.2",
        'minimumOsVersion' : "9.0",
    },
    'com.bluelionmobile.qeep.client.android' :
    {
        'category' : "Social and Communication",
        'updated' : "19 Apr 2022",
        'App_name' : "Qeep\u00ae Dating App, Singles Chat",
        'Package_name' : "com.bluelionmobile.qeep.client.android",
        'installs' : "10,000,000+",
        'version' : "4.5.2",
    },
    'com.practo.fabric' :
    {
        'category' : "Health and Fitness",
        'updated' : "09 Jun 2022",
        'App_name' : "Practo: Online Doctor Consultations & Appointments",
        'Package_name' : "com.practo.fabric",
        'installs' : "5,000,000+",
        'version' : "5.41",
    },
    'com.naver.vibe' :
    {
        'category' : "Entertainment",
        'updated' : "13 Apr 2022",
        'App_name' : "\ub124\uc774\ubc84 VIBE (\ubc14\uc774\ube0c)",
        'Package_name' : "com.naver.vibe",
        'installs' : "1,000,000+",
        'version' : "3.3.3",
    },
    'jp.kakao.piccoma' :
    {
        'category' : "Education and Books",
        'updated' : "06 Jun 2022",
        'App_name' : "\u30d4\u30c3\u30b3\u30de - \u72ec\u5360\u5148\u884c\u914d\u4fe1\u306e\u30de\u30f3\u30ac\u304c\u5f85\u3066\u3070\u8aad\u3081\u308b\u6f2b\u753b\u30a2\u30d7\u30ea",
        'Package_name' : "jp.kakao.piccoma",
        'installs' : "5,000,000+",
        'version' : "6.0.15",
    },
    'com.socialapps.homeplus' :
    {
        'category' : "Shopping",
        'updated' : "09 Jun 2022",
        'App_name' : "\ud648\ud50c\ub7ec\uc2a4 - \uc2e0\uc120\ud55c \uc0dd\uac01, \ud648\ud50c\ub7ec\uc2a4",
        'Package_name' : "com.socialapps.homeplus",
        'installs' : "5,000,000+",
        'version' : "6.0.23",
    },
    'com.phocket' :
    {
        'category' : "Finance",
        'updated' : "05 Jun 2022",
        'App_name' : "Instant Personal Salary Loan",
        'Package_name' : "com.phocket",
        'installs' : "1,000,000+",
        'version' : "8.9.6",
    },
    'com.Dominos' :
    {
        'category' : "Food & Drink",
        'updated' : "10 Jun 2022",
        'App_name' : "Domino's Pizza - Online Food Delivery App",
        'Package_name' : "com.Dominos",
        'installs' : "50,000,000+",
        'version' : "9.8.18",
    },
    'co.indiagold.gold.buy.loan' :
    {
        'category' : "Finance",
        'updated' : "31 May 2022",
        'App_name' : "Gold loan at home in 30 mins",
        'Package_name' : "co.indiagold.gold.buy.loan",
        'installs' : "1,000,000+",
        'version' : "2.34",
    },
    'com.dramafever.shudder' :
    {
        'category' : "Entertainment",
        'updated' : "06 Jun 2022",
        'App_name' : "Shudder: Horror & Thrillers",
        'Package_name' : "com.dramafever.shudder",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.kakaogames.moonlight' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "\ub2ec\ube5b\uc870\uac01\uc0ac",
        'Package_name' : "com.kakaogames.moonlight",
        'installs' : "1,000,000+",
        'version' : "1.0.493",
    },
    'jp.klab.captain283' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "\u30ad\u30e3\u30d7\u30c6\u30f3\u7ffc \uff5e\u305f\u305f\u304b\u3048\u30c9\u30ea\u30fc\u30e0\u30c1\u30fc\u30e0\uff5e \u30b5\u30c3\u30ab\u30fc\u30b2\u30fc\u30e0",
        'Package_name' : "jp.klab.captain283",
        'installs' : "500,000+",
        'version' : "6.2.1",
    },
    'com.pizzahut.phd' :
    {
        'category' : "Food & Drink",
        'updated' : "16 Jun 2022",
        'App_name' : "Pizza Hut Indonesia",
        'Package_name' : "com.pizzahut.phd",
        'installs' : "1,000,000+",
        'version' : "3.1.2",
    },
    'com.mobillium.papara' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Papara",
        'Package_name' : "com.mobillium.papara",
        'installs' : "5,000,000+",
        'version' : "3.8.9",
    },
    'vn.funtap.thienngoai.giangho' :
    {
        'category' : "Games",
        'updated' : "11 May 2022",
        'App_name' : "Thi\u00ean Ngo\u1ea1i Giang H\u1ed3 - Thien Ngoai Giang Ho",
        'Package_name' : "vn.funtap.thienngoai.giangho",
        'installs' : "500,000+",
        'version' : "1.69",
    },
    'com.bahrigames.monkeybubble' :
    {
        'category' : "Games",
        'updated' : "27 Sep 2021",
        'App_name' : "Monkey Bubble",
        'Package_name' : "com.bahrigames.monkeybubble",
        'installs' : "100,000+",
        'version' : "2.9",
    },
    'com.modanisa' :
    {
        'category' : "Shopping",
        'updated' : "01 Jun 2022",
        'App_name' : "Modanisa: Modest Hijab Fashion",
        'Package_name' : "com.modanisa",
        'installs' : "10,000,000+",
        'version' : "2.7.1148",
    },
    '799238595' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-05-16T00:09:49Z",
        'App_name' : "\u30de\u30b8\u90e8",
        'Package_name' : "jp.co.recruit.majibu.majibu.iPhone.1",
        'version' : "9.1.0",
        'minimumOsVersion' : "13.0",
    },
    '1582390510' :
    {
        'category' : "Games",
        'updated' : "2022-05-28T00:57:53Z",
        'App_name' : "\ud55c\uac8c\uc784\ud3ec\ucee4 \ud074\ub798\uc2dd with PC",
        'Package_name' : "com.nhnent.pokerclassic",
        'version' : "1.3.44",
        'minimumOsVersion' : "10.0",
    },
    '977958854' :
    {
        'category' : "Games",
        'updated' : "2022-06-03T23:48:51Z",
        'App_name' : "Harvest Land",
        'Package_name' : "com.mysterytag.slavsforvk",
        'version' : "1.12.5",
        'minimumOsVersion' : "10.0",
    },
    '1117270703' :
    {
        'category' : "Games",
        'updated' : "2022-06-16T05:25:40Z",
        'App_name' : "eFootball\u2122 2022",
        'Package_name' : "jp.konami.pesactionmobile",
        'version' : "6.1.1",
        'minimumOsVersion' : "12.0",
    },
    'io.chingari.app' :
    {
        'category' : "Social and Communication",
        'updated' : "11 Jun 2022",
        'App_name' : "Chingari - Watch and Earn Gari",
        'Package_name' : "io.chingari.app",
        'installs' : "50,000,000+",
        'version' : "3.2.2",
    },
    'com.mumzworld.android' :
    {
        'category' : "Shopping",
        'updated' : "02 Mar 2022",
        'App_name' : "Mumzworld",
        'Package_name' : "com.mumzworld.android",
        'installs' : "500,000+",
        'version' : "5.0.4",
    },
    'com.playbest.sgs' :
    {
        'category' : "Games",
        'updated' : "19 Apr 2022",
        'App_name' : "Game of Heroes: Three Kingdoms",
        'Package_name' : "com.playbest.sgs",
        'installs' : "100,000+",
        'version' : "2.5.6",
    },
    'jp.konami.pawapuroapp' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "\u5b9f\u6cc1\u30d1\u30ef\u30d5\u30eb\u30d7\u30ed\u91ce\u7403",
        'Package_name' : "jp.konami.pawapuroapp",
        'installs' : "5,000,000+",
        'version' : "7.5.0",
    },
    'dk.minreklame.tilbudsavis' :
    {
        'category' : "Shopping",
        'updated' : "21 Mar 2022",
        'App_name' : "minetilbud - tilbudsaviser, reklamer, indk\u00f8bsliste",
        'Package_name' : "dk.minreklame.tilbudsavis",
        'installs' : "500,000+",
        'version' : "7.16.0",
    },
    '1569102341' :
    {
        'category' : "Games",
        'updated' : "2022-06-17T00:05:38Z",
        'App_name' : "Brain Test 3: Knifflige Quests",
        'Package_name' : "com.unicostudio.braintest3",
        'version' : "1.54.01",
        'minimumOsVersion' : "11.0",
    },
    'com.u8.knightclash' :
    {
        'category' : "Games",
        'updated' : "08 Jan 2020",
        'App_name' : "Knight Clash 2019",
        'Package_name' : "com.u8.knightclash",
        'installs' : "10,000+",
        'version' : "2.9.0",
    },
    'com.westernunion.moneytransferr3app.ae' :
    {
        'category' : "Finance",
        'updated' : "17 Mar 2022",
        'App_name' : "WesternUnion AE Money Transfer",
        'Package_name' : "com.westernunion.moneytransferr3app.ae",
        'installs' : "500,000+",
        'version' : "1.215.1",
    },
    'com.casual.survival.game' :
    {
        'category' : "Games",
        'updated' : "13 Jan 2022",
        'App_name' : "Octopus 456 Survival Challenge",
        'Package_name' : "com.casual.survival.game",
        'installs' : "5,000,000+",
        'version' : "1.0.24",
    },
    'com.netmarble.nanakr' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\uc77c\uacf1 \uac1c\uc758 \ub300\uc8c4: GRAND CROSS",
        'Package_name' : "com.netmarble.nanakr",
        'installs' : "1,000,000+",
        'version' : "2.4.3",
    },
    '1512938135' :
    {
        'category' : "Games",
        'updated' : "2021-09-17T22:37:58Z",
        'App_name' : "Mirror cakes",
        'Package_name' : "jp.dawn.mirrorcakes",
        'version' : "2.7.3",
        'minimumOsVersion' : "11.0",
    },
    'com.aniplex.fategrandorder.en' :
    {
        'category' : "Games",
        'updated' : "24 May 2022",
        'App_name' : "Fate/Grand Order (English)",
        'Package_name' : "com.aniplex.fategrandorder.en",
        'installs' : "1,000,000+",
        'version' : "2.31.1",
    },
    'com.krbn.jinsanguo' :
    {
        'category' : "Games",
        'updated' : "17 Jan 2022",
        'App_name' : "\ucc10\uc0bc\uad6d",
        'Package_name' : "com.krbn.jinsanguo",
        'installs' : "500,000+",
        'version' : "1.1.6",
    },
    'com.trenbe.trenbehybrid' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "\ud2b8\ub80c\ube44 - \uc804\uc138\uacc4 \uba85\ud488 \uc1fc\ud551 \ud50c\ub7ab\ud3fc",
        'Package_name' : "com.trenbe.trenbehybrid",
        'installs' : "1,000,000+",
        'version' : "1.3.69",
    },
    'com.sungame.xjh.vn' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "T\u00e2n Giang H\u1ed3 Truy\u1ec1n K\u1ef3-Sungame",
        'Package_name' : "com.sungame.xjh.vn",
        'installs' : "1,000,000+",
        'version' : "1.3.7",
    },
    'eka.care' :
    {
        'category' : "Health and Fitness",
        'updated' : "08 Jun 2022",
        'App_name' : "Eka Care: Healthcare & Records",
        'Package_name' : "eka.care",
        'installs' : "1,000,000+",
        'version' : "2.4.7",
    },
    'com.robokiller.app' :
    {
        'category' : "Social and Communication",
        'updated' : "12 May 2022",
        'App_name' : "RoboKiller - Robocall Blocker",
        'Package_name' : "com.robokiller.app",
        'installs' : "5,000,000+",
        'version' : "22.05.09",
    },
    'com.matrixport.mark' :
    {
        'category' : "Finance",
        'updated' : "26 May 2022",
        'App_name' : "Matrixport: Buy & Earn Crypto",
        'Package_name' : "com.matrixport.mark",
        'installs' : "100,000+",
        'version' : "1.4.8",
    },
    'com.kfc.ksa' :
    {
        'category' : "Food & Drink",
        'updated' : "01 Jun 2022",
        'App_name' : "KFC Saudi Arabia",
        'Package_name' : "com.kfc.ksa",
        'installs' : "1,000,000+",
        'version' : "5.20.2",
    },
    'vng.game.sky.fantasy.song.sea' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "Cloud Song: Saga of Skywalkers",
        'Package_name' : "vng.game.sky.fantasy.song.sea",
        'installs' : "500,000+",
        'version' : "1.1.0",
    },
    'jp.co.yahoo.android.ebookjapan' :
    {
        'category' : "Education and Books",
        'updated' : "13 Jun 2022",
        'App_name' : "\u6f2b\u753b ebookjapan \u6f2b\u753b\u304c\u96fb\u5b50\u66f8\u7c4d\u3067\u8aad\u3081\u308b\u6f2b\u753b\u30a2\u30d7\u30ea",
        'Package_name' : "jp.co.yahoo.android.ebookjapan",
        'installs' : "1,000,000+",
        'version' : "1.11.23",
    },
    'kr.co.ssg.earlymorning' :
    {
        'category' : "Shopping",
        'updated' : "02 Jun 2022",
        'App_name' : "SSG \uc0c8\ubcbd\ubc30\uc1a1",
        'Package_name' : "kr.co.ssg.earlymorning",
        'installs' : "100,000+",
        'version' : "1.4.4",
    },
    '1479140024' :
    {
        'category' : "Games",
        'updated' : "2022-06-02T05:37:48Z",
        'App_name' : "\u30d1\u30ba\u306b\u3083\u3093",
        'Package_name' : "com.skymoons.kittenmatch",
        'version' : "0.32.0",
        'minimumOsVersion' : "10.0",
    },
    'com.saberespoder.poder.card' :
    {
        'category' : "Finance",
        'updated' : "09 Dec 2021",
        'App_name' : "PODERcard - Mobile Banking",
        'Package_name' : "com.saberespoder.poder.card",
        'installs' : "500,000+",
        'version' : "3.301",
    },
    'fr.leocare.app' :
    {
        'category' : "Finance",
        'updated' : "07 Jun 2022",
        'App_name' : "Leocare, Car & Home Insurance",
        'Package_name' : "fr.leocare.app",
        'installs' : "100,000+",
        'version' : "3.23.0",
    },
    'com.mrt.ducati' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "07 Jun 2022",
        'App_name' : "\ub9c8\uc774\ub9ac\uc5bc\ud2b8\ub9bd - \ub098\ub2e4\uc6b4 \uc9c4\uc9dc \uc5ec\ud589",
        'Package_name' : "com.mrt.ducati",
        'installs' : "1,000,000+",
        'version' : "6.27.0",
    },
    'fr.playsoft.lefigarov3' :
    {
        'category' : "Education and Books",
        'updated' : "31 May 2022",
        'App_name' : "Le Figaro.fr: Actu en direct",
        'Package_name' : "fr.playsoft.lefigarov3",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.chucksports.app.prod' :
    {
        'category' : "Games",
        'updated' : "11 Jun 2022",
        'App_name' : "Chuck",
        'Package_name' : "com.chucksports.app.prod",
        'installs' : "1,000+",
        'version' : "1.4.0",
    },
    'com.jnj.acuvue.consumer' :
    {
        'category' : "Lifestyle",
        'updated' : "15 Jun 2022",
        'App_name' : "MyACUVUE\u00ae Russia",
        'Package_name' : "com.jnj.acuvue.consumer",
        'installs' : "1,000,000+",
        'version' : "2.8.2",
    },
    'com.conceptivapps.blossom' :
    {
        'category' : "Education and Books",
        'updated' : "15 Jun 2022",
        'App_name' : "Blossom - Plant Identification",
        'Package_name' : "com.conceptivapps.blossom",
        'installs' : "1,000,000+",
        'version' : "1.29.0",
    },
    'com.kuncie.app' :
    {
        'category' : "Education and Books",
        'updated' : "14 Jun 2022",
        'App_name' : "Kuncie - Learn Business Online",
        'Package_name' : "com.kuncie.app",
        'installs' : "1,000,000+",
        'version' : "1.3.19",
    },
    'com.kingsgroup.ss.tw' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "\u5168\u9762\u5c4d\u63a7",
        'Package_name' : "com.kingsgroup.ss.tw",
        'installs' : "100,000+",
        'version' : "1.16.10",
    },
    'com.bimo.bimo' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "Bimo: tu billetera m\u00f3vil",
        'Package_name' : "com.bimo.bimo",
        'installs' : "100,000+",
        'version' : "1.69.0",
    },
    '592074348' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-05-10T06:26:48Z",
        'App_name' : "Ya\u015fam P\u0131nar\u0131m",
        'Package_name' : "com.pinarsu.PinarSu",
        'version' : "4.1.7",
        'minimumOsVersion' : "11.4",
    },
    'com.kakaogames.moonlighttw' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "\u6708\u5149\u96d5\u523b\u5e2b",
        'Package_name' : "com.kakaogames.moonlighttw",
        'installs' : "100,000+",
        'version' : "1.0.222",
    },
    'eu.formigas.cleverlotto.noOnlineGambling' :
    {
        'category' : "Entertainment",
        'updated' : "19 May 2022",
        'App_name' : "LOTTO 6aus49 & Eurojackpot App",
        'Package_name' : "eu.formigas.cleverlotto.noOnlineGambling",
        'installs' : "100,000+",
        'version' : "5.9.6",
    },
    'com.dashlane' :
    {
        'category' : "Tools",
        'updated' : "16 Jun 2022",
        'App_name' : "Dashlane Password Manager",
        'Package_name' : "com.dashlane",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.zoodel.kz' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "ZoodPay & ZoodMall",
        'Package_name' : "com.zoodel.kz",
        'installs' : "5,000,000+",
        'version' : "3.5.4",
    },
    'com.rbrx.ssjj' :
    {
        'category' : "Games",
        'updated' : "09 May 2022",
        'App_name' : "\u5263\u9b42\uff5e\u65b0\u30b8\u30e7\u30d6\u300c\u4e5d\u5c3e\u300d\u767b\u5834",
        'Package_name' : "com.rbrx.ssjj",
        'installs' : "100,000+",
        'version' : "1.6.6.002",
    },
    'co.windyapp.android' :
    {
        'category' : "Utilities",
        'updated' : "16 Jun 2022",
        'App_name' : "Windy.app: wind & weather live",
        'Package_name' : "co.windyapp.android",
        'installs' : "5,000,000+",
        'version' : "25.0.4",
    },
    'tv.fubo.mobile' :
    {
        'category' : "Entertainment",
        'updated' : "04 May 2022",
        'App_name' : "fuboTV: Watch Live Sports & TV",
        'Package_name' : "tv.fubo.mobile",
        'installs' : "5,000,000+",
        'version' : "4.64.0",
    },
    'com.mambet.tv' :
    {
        'category' : "Entertainment",
        'updated' : "16 Jun 2022",
        'App_name' : "BOOYAH!",
        'Package_name' : "com.mambet.tv",
        'installs' : "50,000,000+",
        'version' : "1.49.10",
    },
    'com.candy.cube' :
    {
        'category' : "Games",
        'updated' : "06 Jan 2022",
        'App_name' : "Candy Cube",
        'Package_name' : "com.candy.cube",
        'installs' : "1,000,000+",
        'version' : "0.4.8",
    },
    '481067676' :
    {
        'category' : "Education and Books",
        'updated' : "2021-11-08T18:16:32Z",
        'App_name' : "LetterSchool - Learn to Write!",
        'Package_name' : "com.boreaal.LetterSchool.lite.en-US",
        'version' : "2.3.0",
        'minimumOsVersion' : "9.0",
    },
    'com.dtg.officefever' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Office Fever",
        'Package_name' : "com.dtg.officefever",
        'installs' : "10,000,000+",
        'version' : "3.1.1",
    },
    'com.vaibhavkalpe.android.khatabook' :
    {
        'category' : "Finance",
        'updated' : "17 Jun 2022",
        'App_name' : "Khatabook Credit Account Book",
        'Package_name' : "com.vaibhavkalpe.android.khatabook",
        'installs' : "50,000,000+",
        'version' : "6.48.3",
    },
    'com.fifun.zhsea.an' :
    {
        'category' : "Games",
        'updated' : "11 Jun 2021",
        'App_name' : "Idle Goddess",
        'Package_name' : "com.fifun.zhsea.an",
        'installs' : "500,000+",
        'version' : "20.0.0",
    },
    'com.sega.Kotodaman' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "\u30b3\u30c8\u30c0\u30de\u30f3 \u2010 \u5171\u95d8\u3053\u3068\u3070RPG",
        'Package_name' : "com.sega.Kotodaman",
        'installs' : "1,000,000+",
        'version' : "5.3.0",
    },
    'com.xlegend.dragonsoul.tw' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "\u6230\u9b25\u5427\u9f8d\u9b42",
        'Package_name' : "com.xlegend.dragonsoul.tw",
        'installs' : "100,000+",
        'version' : "12.0.5",
    },
    'com.homingos.ar' :
    {
        'category' : "Entertainment",
        'updated' : "03 Jun 2022",
        'App_name' : "Flam - Camera to Scan FlamCard",
        'Package_name' : "com.homingos.ar",
        'installs' : "500,000+",
        'version' : "72",
    },
    'com.regz.quananh3q' :
    {
        'category' : "Games",
        'updated' : "13 Jan 2022",
        'App_name' : "Qu\u1ea7n Anh Tam Qu\u1ed1c",
        'Package_name' : "com.regz.quananh3q",
        'installs' : "100,000+",
        'version' : "1.2.3",
    },
    'com.kayac.ball_run' :
    {
        'category' : "Games",
        'updated' : "12 May 2022",
        'App_name' : "Ball Run 2048",
        'Package_name' : "com.kayac.ball_run",
        'installs' : "50,000,000+",
        'version' : "0.3.8",
    },
    'com.il.mcdelivery' :
    {
        'category' : "Food & Drink",
        'updated' : "14 Jun 2022",
        'App_name' : "McDonald\u2019s India Food Delivery",
        'Package_name' : "com.il.mcdelivery",
        'installs' : "5,000,000+",
        'version' : "10.60",
    },
    'br.com.userede' :
    {
        'category' : "Finance",
        'updated' : "09 Jun 2022",
        'App_name' : "Rede: maquininha de cart\u00e3o",
        'Package_name' : "br.com.userede",
        'installs' : "1,000,000+",
        'version' : "6.1.0",
    },
    'com.icantw.zle' :
    {
        'category' : "Games",
        'updated' : "03 Dec 2021",
        'App_name' : "\u5c11\u5973\u672b\u4e16\u9304",
        'Package_name' : "com.icantw.zle",
        'installs' : "50,000+",
        'version' : "1.300.302",
    },
    '432750508' :
    {
        'category' : "Games",
        'updated' : "2022-06-09T13:52:23Z",
        'App_name' : "Dice With Buddies: Social Game",
        'Package_name' : "com.stofledesigns.dicewithbuddiesfree",
        'version' : "8.15.1",
        'minimumOsVersion' : "11.0",
    },
    'com.ncanvas.daytalk' :
    {
        'category' : "Social and Communication",
        'updated' : "31 May 2022",
        'App_name' : "DayTalk - Talk Daily, Weekly",
        'Package_name' : "com.ncanvas.daytalk",
        'installs' : "100,000+",
        'version' : "2.1.7",
    },
    'com.saranyu.shemarooworld' :
    {
        'category' : "Entertainment",
        'updated' : "03 Jun 2022",
        'App_name' : "ShemarooMe",
        'Package_name' : "com.saranyu.shemarooworld",
        'installs' : "1,000,000+",
        'version' : "1.0.17 (114)",
    },
    'com.nexon.maplem.global' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "MapleStory M - Fantasy MMORPG",
        'Package_name' : "com.nexon.maplem.global",
        'installs' : "5,000,000+",
        'version' : "1.7800.3188",
    },
    'com.arriva.glimble' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "01 Jun 2022",
        'App_name' : "glimble reisplanner ov en meer",
        'Package_name' : "com.arriva.glimble",
        'installs' : "50,000+",
        'version' : "5.94.1.536",
    },
    'com.estgames.cm.th' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Cabal M",
        'Package_name' : "com.estgames.cm.th",
        'installs' : "500,000+",
        'version' : "1.1.81",
    },
    'net.ilius.android.match' :
    {
        'category' : "Social and Communication",
        'updated' : "08 Jun 2022",
        'App_name' : "Match: Dating App for singles",
        'Package_name' : "net.ilius.android.match",
        'installs' : "1,000,000+",
        'version' : "5.74.0",
    },
    'com.primebitstudio.cavemanchuck' :
    {
        'category' : "Games",
        'updated' : "01 Dec 2021",
        'App_name' : "Caveman Chuck",
        'Package_name' : "com.primebitstudio.cavemanchuck",
        'installs' : "100,000+",
        'version' : "1.14.5",
    },
    'com.aladinfun.piggytravel.android' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Piggy GO - Clash of Coin",
        'Package_name' : "com.aladinfun.piggytravel.android",
        'installs' : "10,000,000+",
        'version' : "4.0.0",
    },
    'com.zalora.android' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "ZALORA - Fashion Shopping",
        'Package_name' : "com.zalora.android",
        'installs' : "10,000,000+",
        'version' : "12.4.1",
    },
    'com.tier.app' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "09 Jun 2022",
        'App_name' : "TIER Electric scooters",
        'Package_name' : "com.tier.app",
        'installs' : "5,000,000+",
        'version' : "4.0.51",
    },
    'gogolook.callgogolook2' :
    {
        'category' : "Social and Communication",
        'updated' : "07 Jun 2022",
        'App_name' : "Whoscall - Caller ID & Block",
        'Package_name' : "gogolook.callgogolook2",
        'installs' : "50,000,000+",
        'version' : "7.16.1",
    },
    'com.crazylabs.i.can.paint' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "I Can Paint - Art your way",
        'Package_name' : "com.crazylabs.i.can.paint",
        'installs' : "10,000,000+",
        'version' : "1.6.4",
    },
    '1452122930' :
    {
        'category' : "Games",
        'updated' : "2022-03-04T08:11:02Z",
        'App_name' : "\u7345\u5b50\u306e\u5982\u304f\uff5e\u6226\u56fd\u8987\u738b\u6226\u8a18\uff5e",
        'Package_name' : "com.sixwaves.shinsengoku",
        'version' : "1.3.1",
        'minimumOsVersion' : "9.0",
    },
    'com.app.sugarcosmetics' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "SUGAR Cosmetics: Buy Beauty Products Online",
        'Package_name' : "com.app.sugarcosmetics",
        'installs' : "1,000,000+",
        'version' : "3.0.56",
    },
    'com.neptune.domino' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "Higgs Domino Island",
        'Package_name' : "com.neptune.domino",
        'installs' : "50,000,000+",
        'version' : "1.85",
    },
    'com.ludo.king' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Ludo King\u2122",
        'Package_name' : "com.ludo.king",
        'installs' : "500,000,000+",
        'version' : "7.1.0.222",
    },
    'com.xfinite.xinaam' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "Mzaalo - Movies, Web Series",
        'Package_name' : "com.xfinite.xinaam",
        'installs' : "1,000,000+",
        'version' : "1.2.7",
    },
    'jp.co.cygames.umamusume' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "\u30a6\u30de\u5a18 \u30d7\u30ea\u30c6\u30a3\u30fc\u30c0\u30fc\u30d3\u30fc",
        'Package_name' : "jp.co.cygames.umamusume",
        'installs' : "1,000,000+",
        'version' : "1.19.1",
    },
    'com.murka.solitaire.play.klondike' :
    {
        'category' : "Games",
        'updated' : "28 Mar 2022",
        'App_name' : "Solitaire Play - Card Klondike",
        'Package_name' : "com.murka.solitaire.play.klondike",
        'installs' : "1,000,000+",
        'version' : "3.1.9",
    },
    '1247866567' :
    {
        'category' : "Games",
        'updated' : "2022-06-20T01:16:23Z",
        'App_name' : "\u3088\u3046\u3068\u3093\u5834MIX",
        'Package_name' : "jp.j-o-e.NewPig",
        'version' : "12.9",
        'minimumOsVersion' : "9.0",
    },
    '1439772862' :
    {
        'category' : "Games",
        'updated' : "2022-06-16T04:02:05Z",
        'App_name' : "\u30e9\u30b9\u30c8\u30af\u30e9\u30a6\u30c7\u30a3\u30a2",
        'Package_name' : "com.aidis.lastcloudiajpn",
        'version' : "3.2.2",
        'minimumOsVersion' : "9.0",
    },
    'com.inditex.ecommerce.bershka' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "Bershka: Fashion & trends",
        'Package_name' : "com.inditex.ecommerce.bershka",
        'installs' : "10,000,000+",
        'version' : "2.76.2",
    },
    '1535455615' :
    {
        'category' : "Games",
        'updated' : "2022-05-16T16:06:10Z",
        'App_name' : "bwin \u2013 Sportwetten App",
        'Package_name' : "sports.bwin.de2",
        'version' : "22.02.22",
        'minimumOsVersion' : "13.0",
    },
    'com.com2us.wannabe.android.google.global.normal' :
    {
        'category' : "Games",
        'updated' : "04 May 2022",
        'App_name' : "Wannabe Challenge",
        'Package_name' : "com.com2us.wannabe.android.google.global.normal",
        'installs' : "1,000,000+",
        'version' : "1.10.17",
    },
    'com.qoni.guesstheiranswer' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "Guess Their Answer",
        'Package_name' : "com.qoni.guesstheiranswer",
        'installs' : "10,000,000+",
        'version' : "2.27",
    },
    'com.lpg.aom' :
    {
        'category' : "Games",
        'updated' : "08 Feb 2022",
        'App_name' : "Monster Masters",
        'Package_name' : "com.lpg.aom",
        'installs' : "5,000,000+",
        'version' : "13.2.8564",
    },
    'com.dygames.pac2021' :
    {
        'category' : "Games",
        'updated' : "16 May 2022",
        'App_name' : "People and The City",
        'Package_name' : "com.dygames.pac2021",
        'installs' : "100,000+",
        'version' : "1.1.502",
    },
    'com.nhn.yongbi' :
    {
        'category' : "Games",
        'updated' : "21 Apr 2022",
        'App_name' : "\uc6a9\ube44\ubd88\ud328M",
        'Package_name' : "com.nhn.yongbi",
        'installs' : "100,000+",
        'version' : "1.9.0",
    },
    'com.vng.autochess' :
    {
        'category' : "Games",
        'updated' : "22 Apr 2022",
        'App_name' : "Auto Chess",
        'Package_name' : "com.vng.autochess",
        'installs' : "1,000,000+",
        'version' : "2.10.2",
    },
    'com.tempos21.eroskiclub.pro' :
    {
        'category' : "Lifestyle",
        'updated' : "01 Jun 2022",
        'App_name' : "EROSKI - La APP de Eroski Club",
        'Package_name' : "com.tempos21.eroskiclub.pro",
        'installs' : "500,000+",
        'version' : "3.14.0",
    },
    'com.aldgames.jksm' :
    {
        'category' : "Games",
        'updated' : "25 Nov 2021",
        'App_name' : "\u528d\u958b\u5c71\u9580",
        'Package_name' : "com.aldgames.jksm",
        'installs' : "10,000+",
        'version' : "1.7",
    },
    'jp.xlegend.com.at' :
    {
        'category' : "Games",
        'updated' : "01 Jun 2022",
        'App_name' : "Ash Tale-\u98a8\u306e\u5927\u9678-",
        'Package_name' : "jp.xlegend.com.at",
        'installs' : "500,000+",
        'version' : "1.14.28",
    },
    'fr.casino.casinodrive' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "Casino Drive et Livraison",
        'Package_name' : "fr.casino.casinodrive",
        'installs' : "100,000+",
        'version' : "2.22.0",
    },
    'com.rxsjnew4399kr.google' :
    {
        'category' : "Games",
        'updated' : "01 Sep 2021",
        'App_name' : "\ub1cc\uba85\ucc9c\ud5582 \ub9ac\ud134\uc988",
        'Package_name' : "com.rxsjnew4399kr.google",
        'installs' : "100,000+",
        'version' : "1.5.4.004",
    },
    'com.joy.clash.cj' :
    {
        'category' : "Games",
        'updated' : "17 Feb 2022",
        'App_name' : "Clash of Joy",
        'Package_name' : "com.joy.clash.cj",
        'installs' : "100,000+",
        'version' : "2.0.1",
    },
    'com.yunbu.arkcraft.free' :
    {
        'category' : "Games",
        'updated' : "24 Jan 2022",
        'App_name' : "Arkcraft - Idle Adventure",
        'Package_name' : "com.yunbu.arkcraft.free",
        'installs' : "100,000+",
        'version' : "0.0.11",
    },
    'com.shopclues' :
    {
        'category' : "Shopping",
        'updated' : "28 Apr 2022",
        'App_name' : "ShopClues Bazaar: Shopping App",
        'Package_name' : "com.shopclues",
        'installs' : "50,000,000+",
        'version' : "3.6.48",
    },
    '1093826194' :
    {
        'category' : "Education and Books",
        'updated' : "2022-05-21T21:52:54Z",
        'App_name' : "TooFar Media",
        'Package_name' : "com.richshapero.tfma",
        'version' : "1.12.2",
        'minimumOsVersion' : "10.0",
    },
    'com.tw.tycoon.casino' :
    {
        'category' : "Casino",
        'updated' : "02 Jun 2022",
        'App_name' : "Tycoon Casino Vegas Slot Games",
        'Package_name' : "com.tw.tycoon.casino",
        'installs' : "5,000,000+",
        'version' : "2.3.9",
    },
    'com.skypeople.fivestars' :
    {
        'category' : "Games",
        'updated' : "14 Jan 2022",
        'App_name' : "\ud30c\uc774\ube0c\uc2a4\ud0c0\uc988",
        'Package_name' : "com.skypeople.fivestars",
        'installs' : "100,000+",
        'version' : "3.30.1",
    },
    'com.jnj.myacuvue.consumer' :
    {
        'category' : "Lifestyle",
        'updated' : "02 Jun 2022",
        'App_name' : "MyACUVUE\u00ae",
        'Package_name' : "com.jnj.myacuvue.consumer",
        'installs' : "500,000+",
        'version' : "3.6.6",
    },
    'jp.bizreach.candidate' :
    {
        'category' : "Finance",
        'updated' : "07 Jun 2022",
        'App_name' : "\u8ee2\u8077\u306a\u3089\u30d3\u30ba\u30ea\u30fc\u30c1 \u8ee2\u8077\u30a2\u30d7\u30ea",
        'Package_name' : "jp.bizreach.candidate",
        'installs' : "100,000+",
        'version' : "6.4.0",
    },
    'com.photoaffections.freeprints' :
    {
        'category' : "Entertainment",
        'updated' : "09 Jun 2022",
        'App_name' : "FreePrints",
        'Package_name' : "com.photoaffections.freeprints",
        'installs' : "5,000,000+",
        'version' : "3.55.0",
    },
    'com.airasia.mobile' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "13 Jun 2022",
        'App_name' : "airasia Super App",
        'Package_name' : "com.airasia.mobile",
        'installs' : "10,000,000+",
        'version' : "11.30.2",
    },
    'com.ucool.hero' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Heroes Charge",
        'Package_name' : "com.ucool.hero",
        'installs' : "10,000,000+",
        'version' : "2.1.336",
    },
    'id.dana' :
    {
        'category' : "Finance",
        'updated' : "11 Jun 2022",
        'App_name' : "DANA Indonesia Digital Wallet",
        'Package_name' : "id.dana",
        'installs' : "50,000,000+",
        'version' : "2.16.0",
    },
    '1322399438' :
    {
        'category' : "Games",
        'updated' : "2022-06-09T03:10:18Z",
        'App_name' : "\uc5d0\ud53d\uc138\ube10",
        'Package_name' : "com.stove.epic7.ios",
        'version' : "1.0.487",
        'minimumOsVersion' : "10.0",
    },
    '938506958' :
    {
        'category' : "Games",
        'updated' : "2022-06-15T00:29:16Z",
        'App_name' : "\u5b9f\u6cc1\u30d1\u30ef\u30d5\u30eb\u30d7\u30ed\u91ce\u7403",
        'Package_name' : "jp.konami.pawapuroapp",
        'version' : "7.5.0",
        'minimumOsVersion' : "11.0",
    },
    'jp.co.livesense.mb' :
    {
        'category' : "Lifestyle",
        'updated' : "09 Jun 2022",
        'App_name' : "\u30a2\u30eb\u30d0\u30a4\u30c8\u63a2\u3057\u30fb\u30d0\u30a4\u30c8\u30fb\u30d1\u30fc\u30c8\u6c42\u4eba\u60c5\u5831\u306f\u30de\u30c3\u30cf\u30d0\u30a4\u30c8\u30a2\u30d7\u30ea",
        'Package_name' : "jp.co.livesense.mb",
        'installs' : "100,000+",
        'version' : "3.3.0",
    },
    'my.trevo.trevoapp' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "23 May 2022",
        'App_name' : "TREVO - Car Sharing Done Right",
        'Package_name' : "my.trevo.trevoapp",
        'installs' : "1,000,000+",
        'version' : "3.3.7",
    },
    'com.netmarble.bnsmkr' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "\ube14\ub808\uc774\ub4dc&\uc18c\uc6b8 \ub808\ubcfc\ub8e8\uc158",
        'Package_name' : "com.netmarble.bnsmkr",
        'installs' : "1,000,000+",
        'version' : "1.02.382.1",
    },
    'com.coindcx.btc' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "CoinDCX:Bitcoin Investment App",
        'Package_name' : "com.coindcx.btc",
        'installs' : "10,000,000+",
        'version' : "4.11.001",
    },
    'jp.goodsmile.grandsummoners_android' :
    {
        'category' : "Games",
        'updated' : "11 Jun 2022",
        'App_name' : "\u738b\u9053 RPG \u30b0\u30e9\u30f3\u30c9\u30b5\u30de\u30ca\u30fc\u30ba : \u30b0\u30e9\u30b5\u30de",
        'Package_name' : "jp.goodsmile.grandsummoners_android",
        'installs' : "500,000+",
        'version' : "3.53.5",
    },
    'com.chat.stable.forever' :
    {
        'category' : "Entertainment",
        'updated' : "09 Jun 2022",
        'App_name' : "ChaCha",
        'Package_name' : "com.chat.stable.forever",
        'installs' : "500,000+",
        'version' : "1.2.5",
    },
    'com.dokan.twentyfour' :
    {
        'category' : "Shopping",
        'updated' : "26 Apr 2022",
        'App_name' : "24Dokan",
        'Package_name' : "com.dokan.twentyfour",
        'installs' : "10,000+",
        'version' : "3.0",
    },
    'com.faberlic' :
    {
        'category' : "Shopping",
        'updated' : "13 Oct 2021",
        'App_name' : "Faberlic",
        'Package_name' : "com.faberlic",
        'installs' : "1,000,000+",
        'version' : "1.7.3.525",
    },
    'com.yoozoogames.lightofthel' :
    {
        'category' : "Games",
        'updated' : "18 Mar 2022",
        'App_name' : "Light of Thel:Glory of Cepheus",
        'Package_name' : "com.yoozoogames.lightofthel",
        'installs' : "1,000,000+",
        'version' : "0.13.1791",
    },
    'com.aax.exchange' :
    {
        'category' : "Finance",
        'updated' : "23 May 2022",
        'App_name' : "AAX-Trade Crypto, Bitcoin, ETH",
        'Package_name' : "com.aax.exchange",
        'installs' : "1,000,000+",
        'version' : "3.2.10",
    },
    '1528305717' :
    {
        'category' : "Food & Drink",
        'updated' : "2021-10-25T04:17:37Z",
        'App_name' : "KFC Kuwait - Order food Online",
        'Package_name' : "com.kfc.kwt",
        'version' : "5.14.3",
        'minimumOsVersion' : "12.0",
    },
    'com.fbs.ctand' :
    {
        'category' : "Finance",
        'updated' : "25 May 2022",
        'App_name' : "FBS CopyTrade \u2014 Social trading",
        'Package_name' : "com.fbs.ctand",
        'installs' : "5,000,000+",
        'version' : "1.39.0",
    },
    'com.paymaya' :
    {
        'category' : "Finance",
        'updated' : "03 Jun 2022",
        'App_name' : "Maya\u2013Your all-in-one money app",
        'Package_name' : "com.paymaya",
        'installs' : "10,000,000+",
        'version' : "2.74.0",
    },
    'com.aniplex.magireco' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "\u30de\u30ae\u30a2\u30ec\u30b3\u30fc\u30c9 \u9b54\u6cd5\u5c11\u5973\u307e\u3069\u304b\u30de\u30ae\u30ab\u5916\u4f1d",
        'Package_name' : "com.aniplex.magireco",
        'installs' : "1,000,000+",
        'version' : "2.5.2",
    },
    'work.ena.ena' :
    {
        'category' : "Lifestyle",
        'updated' : "02 Nov 2021",
        'App_name' : "ENA",
        'Package_name' : "work.ena.ena",
        'installs' : "50,000+",
        'version' : "1.2.0.0",
    },
    'com.gujaratimatrimony' :
    {
        'category' : "Social and Communication",
        'updated' : "23 May 2022",
        'App_name' : "Gujarati Matrimony\u00ae-Shaadi App",
        'Package_name' : "com.gujaratimatrimony",
        'installs' : "500,000+",
        'version' : "Varies with device",
    },
    'com.Uking.thai.fk' :
    {
        'category' : "Casino",
        'updated' : "06 May 2022",
        'App_name' : "Slots-dummy 2V2 \u0e44\u0e1e\u0e48\u0e41\u0e04\u0e07 \u0e14\u0e31\u0e21\u0e21\u0e35\u0e48",
        'Package_name' : "com.Uking.thai.fk",
        'installs' : "1,000,000+",
        'version' : "2.22.2.0",
    },
    'jp.sride.userapp' :
    {
        'category' : "Utilities",
        'updated' : "27 May 2022",
        'App_name' : "S.RIDE",
        'Package_name' : "jp.sride.userapp",
        'installs' : "100,000+",
        'version' : "7.5.1",
    },
    'com.glu.disneygame' :
    {
        'category' : "Games",
        'updated' : "17 May 2022",
        'App_name' : "Disney Sorcerer's Arena",
        'Package_name' : "com.glu.disneygame",
        'installs' : "1,000,000+",
        'version' : "22.0",
    },
    'jp.co.bandainamcoent.BNEI0242' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "\u30a2\u30a4\u30c9\u30eb\u30de\u30b9\u30bf\u30fc \u30b7\u30f3\u30c7\u30ec\u30e9\u30ac\u30fc\u30eb\u30ba \u30b9\u30bf\u30fc\u30e9\u30a4\u30c8\u30b9\u30c6\u30fc\u30b8",
        'Package_name' : "jp.co.bandainamcoent.BNEI0242",
        'installs' : "1,000,000+",
        'version' : "7.8.0",
    },
    'com.main.gopuff' :
    {
        'category' : "Food & Drink",
        'updated' : "12 Jun 2022",
        'App_name' : "Gopuff\u2014Alcohol & Food Delivery",
        'Package_name' : "com.main.gopuff",
        'installs' : "1,000,000+",
        'version' : "4.17.0",
    },
    'com.fraazo.app' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "FRAAZO - Green Grocery App",
        'Package_name' : "com.fraazo.app",
        'installs' : "1,000,000+",
        'version' : "2.3.75",
    },
    '813597992' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-10T18:23:59Z",
        'App_name' : "Floward Online Flowers & Gifts",
        'Package_name' : "com.q8flowers.q8flowers",
        'version' : "6.14.0",
        'minimumOsVersion' : "13.0",
    },
    'com.godgame.mj.android' :
    {
        'category' : "Casino",
        'updated' : "27 May 2022",
        'App_name' : "\u795e\u4f86\u4e5f\u9ebb\u5c07\uff0d\u9ebb\u5c07\u3001\u9ebb\u96c0",
        'Package_name' : "com.godgame.mj.android",
        'installs' : "5,000,000+",
        'version' : "13.5",
    },
    'kr.co.fitpet' :
    {
        'category' : "Shopping",
        'updated' : "09 Jun 2022",
        'App_name' : "Fitpet:Pet Healthcare Solution",
        'Package_name' : "kr.co.fitpet",
        'installs' : "100,000+",
        'version' : "4.2.02",
    },
    '1531582123' :
    {
        'category' : "Games",
        'updated' : "2022-04-27T20:46:44Z",
        'App_name' : "Who is? Brain Teaser & Riddles",
        'Package_name' : "com.unicostudio.whois",
        'version' : "1.4.2",
        'minimumOsVersion' : "11.0",
    },
    'pl.modivo.modivoapp' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "MODIVO \u2013 WYRA\u017bAJ SIEBIE",
        'Package_name' : "pl.modivo.modivoapp",
        'installs' : "1,000,000+",
        'version' : "2.14.2",
    },
    'com.joycity.gw' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Gunship Battle Total Warfare",
        'Package_name' : "com.joycity.gw",
        'installs' : "10,000,000+",
        'version' : "5.1.10",
    },
    'com.ncsoft.paige' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "PAIGE - Baseball app for KBO",
        'Package_name' : "com.ncsoft.paige",
        'installs' : "100,000+",
        'version' : "4.3.12",
    },
    'jp.nkdn2.ninkyo2' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "\u4efb\u4fa0\u4f1d\u3000\u4e0d\u826f\u9054\u306e\u30ac\u30c1\u30f3\u30b3\u55a7\u5629\u30d0\u30c8\u30ebRPG",
        'Package_name' : "jp.nkdn2.ninkyo2",
        'installs' : "1,000,000+",
        'version' : "1.0.65",
    },
    'com.teachmint.teachmint' :
    {
        'category' : "Education and Books",
        'updated' : "09 Jun 2022",
        'App_name' : "Teachmint - The Classroom App",
        'Package_name' : "com.teachmint.teachmint",
        'installs' : "10,000,000+",
        'version' : "6.0.0",
    },
    'com.bluetakugame.mhakr' :
    {
        'category' : "Games",
        'updated' : "26 Aug 2021",
        'App_name' : "\ub098\uc758 \ud788\uc5b4\ub85c \uc544\uce74\ub370\ubbf8\uc544: \ucd5c\uac15 \ud788\uc5b4\ub85c",
        'Package_name' : "com.bluetakugame.mhakr",
        'installs' : "100,000+",
        'version' : "30012.8.9",
    },
    'com.imobilecode.fanatik' :
    {
        'category' : "Games",
        'updated' : "18 Apr 2022",
        'App_name' : "Fanatik",
        'Package_name' : "com.imobilecode.fanatik",
        'installs' : "500,000+",
        'version' : "4.2.6",
    },
    'com.appg.jelly' :
    {
        'category' : "Lifestyle",
        'updated' : "18 May 2022",
        'App_name' : "\uc824\ub9ac\ud3ab_\uac15\uc544\uc9c0 \uc0b0\ucc45, \uace0\uc591\uc774 \ub180\uc544\uc8fc\uae30",
        'Package_name' : "com.appg.jelly",
        'installs' : "10,000+",
        'version' : "2.0.23",
    },
    'com.unocoin.unocoinwallet' :
    {
        'category' : "Finance",
        'updated' : "12 Jun 2022",
        'App_name' : "Unocoin: Bitcoin & 85+ Cryptos",
        'Package_name' : "com.unocoin.unocoinwallet",
        'installs' : "1,000,000+",
        'version' : "4.2.5",
    },
    'com.kamagames.pokerist' :
    {
        'category' : "Games",
        'updated' : "29 Apr 2022",
        'App_name' : "Texas Hold'em Poker: Pokerist",
        'Package_name' : "com.kamagames.pokerist",
        'installs' : "10,000,000+",
        'version' : "46.3.0",
    },
    'com.YoStarKR.Arknights' :
    {
        'category' : "Games",
        'updated' : "27 Apr 2022",
        'App_name' : "\uba85\uc77c\ubc29\uc8fc",
        'Package_name' : "com.YoStarKR.Arknights",
        'installs' : "500,000+",
        'version' : "9.0.01",
    },
    'in.workindia.nileshdungarwal.workindiaandroid' :
    {
        'category' : "Finance",
        'updated' : "19 May 2022",
        'App_name' : "WorkIndia Job Search App",
        'Package_name' : "in.workindia.nileshdungarwal.workindiaandroid",
        'installs' : "10,000,000+",
        'version' : "7.0.5.3",
    },
    'com.bazarchic.android' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "BazarChic, Vente Priv\u00e9e Mode",
        'Package_name' : "com.bazarchic.android",
        'installs' : "1,000,000+",
        'version' : "5.3.4",
    },
    'com.clearscore.mobile' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "ClearScore - Credit Score",
        'Package_name' : "com.clearscore.mobile",
        'installs' : "1,000,000+",
        'version' : "4.25.0",
    },
    'com.kwai.kuaishou.video.live' :
    {
        'category' : "Entertainment",
        'updated' : "17 Jun 2022",
        'App_name' : "Kwai - Short Video Community",
        'Package_name' : "com.kwai.kuaishou.video.live",
        'installs' : "50,000,000+",
        'version' : "6.5.10.526002",
    },
    'com.lidl.eci.mylidlstore' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2021",
        'App_name' : "My Lidl World",
        'Package_name' : "com.lidl.eci.mylidlstore",
        'installs' : "1,000,000+",
        'version' : "1.7.4",
    },
    '1486550727' :
    {
        'category' : "Social and Communication",
        'updated' : "2021-11-12T22:37:34Z",
        'App_name' : "\u5373\u4f1a\u3044\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea\u3067\u5b8c\u5168\u306b\u51fa\u4f1a\u3044\u52df\u96c6\u30a2\u30d7\u30ea Many",
        'Package_name' : "online.many.app",
        'version' : "1.2.3",
        'minimumOsVersion' : "9.0",
    },
    'com.intertech.mobilemoneytransfer.activity' :
    {
        'category' : "Finance",
        'updated' : "27 Apr 2022",
        'App_name' : "fastPay",
        'Package_name' : "com.intertech.mobilemoneytransfer.activity",
        'installs' : "1,000,000+",
        'version' : "8.5.1",
    },
    'com.sportscheck.mobile' :
    {
        'category' : "Shopping",
        'updated' : "04 Apr 2022",
        'App_name' : "SportScheck \u2013 DEIN SPORT SHOP",
        'Package_name' : "com.sportscheck.mobile",
        'installs' : "100,000+",
        'version' : "2.2.0",
    },
    'vn.speedy.express.delivery' :
    {
        'category' : "Tools",
        'updated' : "16 Jun 2022",
        'App_name' : "MrSpeedy: Reliable Express Delivery App",
        'Package_name' : "vn.speedy.express.delivery",
        'installs' : "100,000+",
        'version' : "1.63.0",
    },
    'com.shortpedianews' :
    {
        'category' : "Education and Books",
        'updated' : "20 Apr 2022",
        'App_name' : "Shortpedia - Hindi News, Latest Short News Summary",
        'Package_name' : "com.shortpedianews",
        'installs' : "500,000+",
        'version' : "4.9.9",
    },
    'com.akabane.uwaminu' :
    {
        'category' : "Games",
        'updated' : "17 Feb 2022",
        'App_name' : "\u6d6e\u6c17\u5f7c\u6c0f\u306e\u898b\u629c\u304d\u65b9 -\u3061\u3087\u3063\u3068\u3048\u3063\u3061\u306a\u6687\u3064\u3076\u3057\u6d6e\u6c17\u63a2\u3057\u30b2\u30fc\u30e0",
        'Package_name' : "com.akabane.uwaminu",
        'installs' : "100,000+",
        'version' : "1.0.3",
    },
    'com.petsbe.android.petsbemall' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "\uc5b4\ubc14\uc6c3\ud3ab (aboutPet)",
        'Package_name' : "com.petsbe.android.petsbemall",
        'installs' : "1,000,000+",
        'version' : "3.8.58",
    },
    'com.binance.cloud.tokocrypto' :
    {
        'category' : "Finance",
        'updated' : "19 Apr 2022",
        'App_name' : "Tokocrypto - Trading Kripto",
        'Package_name' : "com.binance.cloud.tokocrypto",
        'installs' : "1,000,000+",
        'version' : "1.8.0",
    },
    'com.shopee.id' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "Shopee 6.6 Rumah & Hobi",
        'Package_name' : "com.shopee.id",
        'installs' : "100,000,000+",
        'version' : "2.88.41",
    },
    'jp.boi.mitrasphere.app' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "\u30df\u30c8\u30e9\u30b9\u30d5\u30a3\u30a2 -MITRASPHERE-",
        'Package_name' : "jp.boi.mitrasphere.app",
        'installs' : "500,000+",
        'version' : "3.17.1",
    },
    'id.danarupiah.weshare.jiekuan' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "DanaRupiah-Pinjaman Uang Cepat",
        'Package_name' : "id.danarupiah.weshare.jiekuan",
        'installs' : "5,000,000+",
        'version' : "2.4.29",
    },
    'com.amazon.mp3' :
    {
        'category' : "Entertainment",
        'updated' : "04 Jun 2022",
        'App_name' : "Amazon Music: Discover Songs",
        'Package_name' : "com.amazon.mp3",
        'installs' : "100,000,000+",
        'version' : "22.7.1",
    },
    'com.eyougame.radiance' :
    {
        'category' : "Games",
        'updated' : "23 Dec 2021",
        'App_name' : "\uc0e4\uc774\ub2dd\ub77c\uc774\ud2b8",
        'Package_name' : "com.eyougame.radiance",
        'installs' : "500,000+",
        'version' : "1.0.40",
    },
    '646315802' :
    {
        'category' : "Games",
        'updated' : "2022-04-21T00:15:23Z",
        'App_name' : "\ubaa8\ub450\uc758\ub9c8\ube14",
        'Package_name' : "com.cjenm.momak",
        'version' : "1.91.0",
        'minimumOsVersion' : "10.0",
    },
    'grit.storytel.app' :
    {
        'category' : "Education and Books",
        'updated' : "13 Jun 2022",
        'App_name' : "Storytel: Audiobooks & Ebooks",
        'Package_name' : "grit.storytel.app",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.ptfarm.pokerrrr' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "Pokerrrr 2 - Holdem, OFC, Stud",
        'Package_name' : "com.ptfarm.pokerrrr",
        'installs' : "1,000,000+",
        'version' : "4.10.0",
    },
    'com.noon.buyerapp' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "noon shopping",
        'Package_name' : "com.noon.buyerapp",
        'installs' : "10,000,000+",
        'version' : "3.53 (1161)",
    },
    'com.neowizgames.game.browndust.srpg.gamfs' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "\ube0c\ub77c\uc6b4\ub354\uc2a4\ud2b8 - \ud134\uc81c RPG",
        'Package_name' : "com.neowizgames.game.browndust.srpg.gamfs",
        'installs' : "1,000,000+",
        'version' : "2.20.5",
    },
    'com.cleartrip.android' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "23 May 2022",
        'App_name' : "Cleartrip - Travel Booking App",
        'Package_name' : "com.cleartrip.android",
        'installs' : "10,000,000+",
        'version' : "22.5.1",
    },
    'com.denachina.g33002013.android' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "\uc2ac\ub7a8\ub369\ud06c",
        'Package_name' : "com.denachina.g33002013.android",
        'installs' : "500,000+",
        'version' : "1.24",
    },
    '1478705151' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2021-11-17T08:46:49Z",
        'App_name' : "CAOCAO service VTC responsable",
        'Package_name' : "com.caocaoParis.ceeuCarPassenger",
        'version' : "2.11.6",
        'minimumOsVersion' : "11.0",
    },
    'com.ehk.bosslife3d' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Boss Life 3D",
        'Package_name' : "com.ehk.bosslife3d",
        'installs' : "10,000,000+",
        'version' : "1.1.74",
    },
    'com.bigcake.android.bpdaily' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Block Puzzle",
        'Package_name' : "com.bigcake.android.bpdaily",
        'installs' : "5,000,000+",
        'version' : "18.0.29",
    },
    'com.ftt.boxingstar.gl.aos' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "Boxing Star",
        'Package_name' : "com.ftt.boxingstar.gl.aos",
        'installs' : "10,000,000+",
        'version' : "3.7.1",
    },
    'com.indie.jzsd.google.jp' :
    {
        'category' : "Games",
        'updated' : "30 Mar 2021",
        'App_name' : "\u9b54\u795e\u306e\u5b50-\u653e\u7f6e\u306e\u5e7b\u60f3\u5927\u9678-",
        'Package_name' : "com.indie.jzsd.google.jp",
        'installs' : "10,000+",
        'version' : "2.0.4",
    },
    'com.Utes.NaughtyDreams' :
    {
        'category' : "Games",
        'updated' : "28 Sep 2021",
        'App_name' : "Naughty Dreams - Choices Story",
        'Package_name' : "com.Utes.NaughtyDreams",
        'installs' : "10,000+",
        'version' : "1.6.6",
    },
    'com.wgt.android.golf' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "WGT Golf",
        'Package_name' : "com.wgt.android.golf",
        'installs' : "10,000,000+",
        'version' : "1.78.0",
    },
    'com.koinworks.app' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "KoinWorks NEO: Digital Finance",
        'Package_name' : "com.koinworks.app",
        'installs' : "1,000,000+",
        'version' : "2.2.4.6",
    },
    'com.ruangguru.livestudents' :
    {
        'category' : "Education and Books",
        'updated' : "27 May 2022",
        'App_name' : "Ruangguru: Bimbel SD SMP SMA",
        'Package_name' : "com.ruangguru.livestudents",
        'installs' : "10,000,000+",
        'version' : "6.35.2",
    },
    'net.btpro.client.koctas' :
    {
        'category' : "Shopping",
        'updated' : "17 Mar 2022",
        'App_name' : "Ko\u00e7ta\u015f",
        'Package_name' : "net.btpro.client.koctas",
        'installs' : "1,000,000+",
        'version' : "2.4.5",
    },
    'com.linecorp.LGPKPK' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "LINE Pokopoko",
        'Package_name' : "com.linecorp.LGPKPK",
        'installs' : "10,000,000+",
        'version' : "2.3.4",
    },
    'com.fifada.customer' :
    {
        'category' : "Finance",
        'updated' : "12 Apr 2022",
        'App_name' : "FIFADA - Cicilan Online Mudah",
        'Package_name' : "com.fifada.customer",
        'installs' : "500,000+",
        'version' : "2.2.56",
    },
    'com.jp.devil' :
    {
        'category' : "Games",
        'updated' : "21 Oct 2021",
        'App_name' : "\u9b54\u738b\u3068100\u4eba\u306e\u304a\u59eb\u69d8",
        'Package_name' : "com.jp.devil",
        'installs' : "100,000+",
        'version' : "1.4.1",
    },
    '1470893433' :
    {
        'category' : "Games",
        'updated' : "2020-10-20T16:03:23Z",
        'App_name' : "City Bus Inc.",
        'Package_name' : "com.HikmetDuran.CityBusInc",
        'version' : "3.1.0",
        'minimumOsVersion' : "10.0",
    },
    'hk.com.wegames.newdldl.and' :
    {
        'category' : "Games",
        'updated' : "24 Mar 2022",
        'App_name' : "\u65b0\u6597\u7f85\u5927\u9678\uff1a\u767b\u5165\u9001SS\u524d\u4e16\u5510\u4e09",
        'Package_name' : "hk.com.wegames.newdldl.and",
        'installs' : "100,000+",
        'version' : "1.1.7.9",
    },
    'com.mosaicwellness.manmatters' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "Man Matters - Health & Doctors",
        'Package_name' : "com.mosaicwellness.manmatters",
        'installs' : "100,000+",
        'version' : "8.83",
    },
    'com.knights.bikesstunt.motomaster' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Bike Stunt 3d Motorcycle Games",
        'Package_name' : "com.knights.bikesstunt.motomaster",
        'installs' : "100,000,000+",
        'version' : "3.132",
    },
    'com.fanmartapp.shop' :
    {
        'category' : "Shopping",
        'updated' : "21 Apr 2021",
        'App_name' : "FanMart - Fast Online Shopping",
        'Package_name' : "com.fanmartapp.shop",
        'installs' : "1,000,000+",
        'version' : "3.21.0",
    },
    'mobi.abcmouse.academy_goo' :
    {
        'category' : "Education and Books",
        'updated' : "27 May 2022",
        'App_name' : "ABCmouse.com",
        'Package_name' : "mobi.abcmouse.academy_goo",
        'installs' : "10,000,000+",
        'version' : "8.42.0",
    },
    'com.c2info.wellnessforever' :
    {
        'category' : "Health and Fitness",
        'updated' : "17 Mar 2022",
        'App_name' : "WellnessForever",
        'Package_name' : "com.c2info.wellnessforever",
        'installs' : "1,000,000+",
        'version' : "4.7.3",
    },
    'com.localqueen' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Work from Home, Earn Money Online, Start Reselling",
        'Package_name' : "com.localqueen",
        'installs' : "10,000,000+",
        'version' : "3.9.10",
    },
    '884043462' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-13T07:14:03Z",
        'App_name' : "\uc5ec\uae30\uc5b4\ub54c - \ud638\ud154, \ubaa8\ud154, \ud39c\uc158, \ud56d\uacf5\uad8c \ucd5c\uc800\uac00 \uc608\uc57d!",
        'Package_name' : "kr.co.withweb.aboutyeogi",
        'version' : "5.10.0",
        'minimumOsVersion' : "13.0",
    },
    'com.eterno' :
    {
        'category' : "Education and Books",
        'updated' : "21 Apr 2022",
        'App_name' : "Dailyhunt: News, Video,Cricket",
        'Package_name' : "com.eterno",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'ru.yandex.searchplugin' :
    {
        'category' : "Tools",
        'updated' : "10 Jun 2022",
        'App_name' : "Yandex",
        'Package_name' : "ru.yandex.searchplugin",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.indie.jz3d.google.ft' :
    {
        'category' : "Games",
        'updated' : "12 May 2022",
        'App_name' : "\u5c01\u795e\u5929\u5730\u52ab-\u548c\u98a8\u4ed9\u4fe0 \u65b0\u4e16\u754c\u60c5\u7de3\u96d9\u4fee\u990a\u6210 \u89d2\u8272\u626e\u6f14\u52d5\u4f5c\u624b\u904a",
        'Package_name' : "com.indie.jz3d.google.ft",
        'installs' : "50,000+",
        'version' : "2.2.3",
    },
    '1375330360' :
    {
        'category' : "Finance",
        'updated' : "2022-05-24T18:26:51Z",
        'App_name' : "Jassby: Debit Card for Teens",
        'Package_name' : "com.jassby.jassby",
        'version' : "5.5.0",
        'minimumOsVersion' : "10.3",
    },
    'ru.legorussia.android' :
    {
        'category' : "Shopping",
        'updated' : "09 Mar 2022",
        'App_name' : "mir-kubikov.ru - \u0421\u0435\u0442\u044c \u043c\u0430\u0433\u0430\u0437\u0438\u043d\u043e\u0432",
        'Package_name' : "ru.legorussia.android",
        'installs' : "100,000+",
        'version' : "5.57.2",
    },
    'com.gamevilusa.mlbpilive.android.google.global.normal' :
    {
        'category' : "Games",
        'updated' : "04 May 2022",
        'App_name' : "MLB Perfect Inning 2022",
        'Package_name' : "com.gamevilusa.mlbpilive.android.google.global.normal",
        'installs' : "5,000,000+",
        'version' : "2.5.4",
    },
    'com.agate.bpm' :
    {
        'category' : "Games",
        'updated' : "13 Apr 2022",
        'App_name' : "Code Atma : Supernatural RPG",
        'Package_name' : "com.agate.bpm",
        'installs' : "100,000+",
        'version' : "1.1.51",
    },
    'com.vn.vib.mobileapp' :
    {
        'category' : "Finance",
        'updated' : "09 Jun 2022",
        'App_name' : "MyVIB",
        'Package_name' : "com.vn.vib.mobileapp",
        'installs' : "1,000,000+",
        'version' : "8.32.1",
    },
    'com.eyougame.caveshooter' :
    {
        'category' : "Games",
        'updated' : "16 Apr 2022",
        'App_name' : "Cave Shooter-Instant Shooting",
        'Package_name' : "com.eyougame.caveshooter",
        'installs' : "500,000+",
        'version' : "1.1.16",
    },
    'com.app.smytten' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "Smytten: Try Sample Products",
        'Package_name' : "com.app.smytten",
        'installs' : "5,000,000+",
        'version' : "9.2.12",
    },
    'com.gravity.ragnarokorigin.aos' :
    {
        'category' : "Games",
        'updated' : "24 Mar 2022",
        'App_name' : "\ub77c\uadf8\ub098\ub85c\ud06c \uc624\ub9ac\uc9c4",
        'Package_name' : "com.gravity.ragnarokorigin.aos",
        'installs' : "500,000+",
        'version' : "2.14.1",
    },
    'com.vn.sl.google' :
    {
        'category' : "Games",
        'updated' : "12 Nov 2021",
        'App_name' : "Soul Land: \u0110\u1ea5u La \u0110\u1ea1i L\u1ee5c-Funtap",
        'Package_name' : "com.vn.sl.google",
        'installs' : "1,000,000+",
        'version' : "53.0",
    },
    'com.gm99.dzg' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "\u53eb\u6211\u5927\u638c\u6ac3:\u539f\u5275\u6b63\u7248\u53e4\u98a8\u7d93\u71df",
        'Package_name' : "com.gm99.dzg",
        'installs' : "500,000+",
        'version' : "4.7.4",
    },
    'com.zbooni' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "Zbooni",
        'Package_name' : "com.zbooni",
        'installs' : "100,000+",
        'version' : "2.7.4",
    },
    'puzzle.merge.family.mansion' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Merge Matters: House Design",
        'Package_name' : "puzzle.merge.family.mansion",
        'installs' : "1,000,000+",
        'version' : "19.0.07",
    },
    'com.thehomedepot' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "The Home Depot",
        'Package_name' : "com.thehomedepot",
        'installs' : "10,000,000+",
        'version' : "7.6.1",
    },
    'com.followstat' :
    {
        'category' : "Tools",
        'updated' : "20 May 2021",
        'App_name' : "iMetric: Profile Followers Analytics for Instagram",
        'Package_name' : "com.followstat",
        'installs' : "5,000,000+",
        'version' : "5.1.8",
    },
    'gg.wasder.app' :
    {
        'category' : "Social and Communication",
        'updated' : "10 Jun 2022",
        'App_name' : "Wasder",
        'Package_name' : "gg.wasder.app",
        'installs' : "1,000,000+",
        'version' : "1.2.203",
    },
    'com.theotino.bingo.luckybingo.wonderland' :
    {
        'category' : "Casino",
        'updated' : "18 May 2022",
        'App_name' : "Bingo: Play Lucky Bingo Games",
        'Package_name' : "com.theotino.bingo.luckybingo.wonderland",
        'installs' : "1,000,000+",
        'version' : "2.0.0",
    },
    'com.b2w.americanas' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Americanas: Compras Online",
        'Package_name' : "com.b2w.americanas",
        'installs' : "50,000,000+",
        'version' : "3.85.0",
    },
    'com.wallapop' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "Wallapop - Buy & Sell Nearby",
        'Package_name' : "com.wallapop",
        'installs' : "10,000,000+",
        'version' : "1.150.4",
    },
    'com.otipy.otipy' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "OTIPY:Fresh Vegetable & Fruits",
        'Package_name' : "com.otipy.otipy",
        'installs' : "1,000,000+",
        'version' : "1.1.2",
    },
    'com.wealthguide.muffler' :
    {
        'category' : "Finance",
        'updated' : "03 Dec 2021",
        'App_name' : "\ub9c8\uc774\uba38\ud50c\ub7ec - \uc5f0\uae08\uc804\ubb38 \ud1a0\ud0c8\ucf00\uc5b4\uc11c\ube44\uc2a4",
        'Package_name' : "com.wealthguide.muffler",
        'installs' : "10,000+",
        'version' : "2.8.0",
    },
    'com.ruby.rummy' :
    {
        'category' : "Games",
        'updated' : "28 Dec 2020",
        'App_name' : "Ruby Rummy-Indian Online Free Card Game",
        'Package_name' : "com.ruby.rummy",
        'installs' : "1,000,000+",
        'version' : "1.0.8",
    },
    'com.taliber.zaphirelive.android.prod' :
    {
        'category' : "Finance",
        'updated' : "30 Mar 2022",
        'App_name' : "Zaphire: Network & Job Finder",
        'Package_name' : "com.taliber.zaphirelive.android.prod",
        'installs' : "10,000+",
        'version' : "1.5.1",
    },
    'app.mycountrydelight.in.countrydelight' :
    {
        'category' : "Food & Drink",
        'updated' : "14 Jun 2022",
        'App_name' : "Country Delight: Milk Delivery",
        'Package_name' : "app.mycountrydelight.in.countrydelight",
        'installs' : "5,000,000+",
        'version' : "7.2.6",
    },
    'com.indie.zgtx.google.jp' :
    {
        'category' : "Games",
        'updated' : "27 Jan 2021",
        'App_name' : "\u7570\u5883\u4f1d\u8aac-\u9e92\u9e9f\u306e\u7ffc-",
        'Package_name' : "com.indie.zgtx.google.jp",
        'installs' : "1,000+",
        'version' : "1.0",
    },
    'com.youhodler.youhodler' :
    {
        'category' : "Finance",
        'updated' : "06 Jun 2022",
        'App_name' : "YouHodler - Bitcoin Wallet",
        'Package_name' : "com.youhodler.youhodler",
        'installs' : "500,000+",
        'version' : "5.1.0",
    },
    'com.hike.rush' :
    {
        'category' : "Games",
        'updated' : "12 May 2022",
        'App_name' : "Rush: Play Ludo & Carrom Games",
        'Package_name' : "com.hike.rush",
        'installs' : "1,000,000+",
        'version' : "1.0.239",
    },
    'com.adcb.cbgdigi' :
    {
        'category' : "Finance",
        'updated' : "18 May 2022",
        'App_name' : "ADCB Hayyak: Start your banking relationship now!",
        'Package_name' : "com.adcb.cbgdigi",
        'installs' : "500,000+",
        'version' : "1.4.2",
    },
    '379693831' :
    {
        'category' : "Education and Books",
        'updated' : "2022-06-14T16:24:13Z",
        'App_name' : "Audible audiobooks & podcasts",
        'Package_name' : "com.audible.iphone",
        'version' : "3.74",
        'minimumOsVersion' : "15.0",
    },
    'com.com2us.smon.normal.freefull.google.kr.android.common' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Summoners War",
        'Package_name' : "com.com2us.smon.normal.freefull.google.kr.android.common",
        'installs' : "50,000,000+",
        'version' : "6.6.7",
    },
    'com.rxsj.ssjj' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "\u5927\u4fe0\u5ba2\u2014\u6fdf\u516c\u964d\u4e16",
        'Package_name' : "com.rxsj.ssjj",
        'installs' : "500,000+",
        'version' : "1.6.7.003",
    },
    'com.tilemaster.puzzle.block.match' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "Tile Master\u00ae - Classic Match",
        'Package_name' : "com.tilemaster.puzzle.block.match",
        'installs' : "50,000,000+",
        'version' : "2.7.16",
    },
    'com.mycreditchain.goodmorning' :
    {
        'category' : "Lifestyle",
        'updated' : "09 Jun 2022",
        'App_name' : "GoodMorn",
        'Package_name' : "com.mycreditchain.goodmorning",
        'installs' : "100,000+",
        'version' : "2.1.25",
    },
    'com.fireandglory.wonderwar' :
    {
        'category' : "Games",
        'updated' : "21 May 2022",
        'App_name' : "Fire and Glory: Blood War",
        'Package_name' : "com.fireandglory.wonderwar",
        'installs' : "500,000+",
        'version' : "1.0.063",
    },
    'com.missjulie.homedesign' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Miss Julie Home Design",
        'Package_name' : "com.missjulie.homedesign",
        'installs' : "50,000+",
        'version' : "17",
    },
    'br.westwing.android' :
    {
        'category' : "Shopping",
        'updated' : "28 May 2022",
        'App_name' : "Westwing - Casa e Decora\u00e7\u00e3o",
        'Package_name' : "br.westwing.android",
        'installs' : "500,000+",
        'version' : "2.13.0-5806",
    },
    'com.kakaogames.golffriends' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Friends Shot: Golf for All",
        'Package_name' : "com.kakaogames.golffriends",
        'installs' : "1,000,000+",
        'version' : "0.0.51",
    },
    'com.noah.flow' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "stop the flow! - rescue puzzle",
        'Package_name' : "com.noah.flow",
        'installs' : "10,000,000+",
        'version' : "1.1.7",
    },
    'com.nhnent.SK10392' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "Kingdom Story: Brave Legion",
        'Package_name' : "com.nhnent.SK10392",
        'installs' : "1,000,000+",
        'version' : "3.00.0.KG",
    },
    'in.pickily.shopping' :
    {
        'category' : "Shopping",
        'updated' : "17 Jun 2022",
        'App_name' : "pickily - Grocery Delivery",
        'Package_name' : "in.pickily.shopping",
        'installs' : "1,000,000+",
        'version' : "2.3.0",
    },
    'com.smallcase.android' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "smallcase: Investing Made Easy",
        'Package_name' : "com.smallcase.android",
        'installs' : "1,000,000+",
        'version' : "4.18.2",
    },
    'com.kilofasting' :
    {
        'category' : "Health and Fitness",
        'updated' : "13 Jun 2022",
        'App_name' : "DoFasting - Fasting Tracker",
        'Package_name' : "com.kilofasting",
        'installs' : "500,000+",
        'version' : "4.11.2",
    },
    'dating.bump' :
    {
        'category' : "Social and Communication",
        'updated' : "05 Aug 2020",
        'App_name' : "\u51fa\u4f1a\u3044\u306fBUMP\uff08\u30d0\u30f3\u30d7\uff09 - \u8fd1\u6240\u3067\u51fa\u9022\u3044\u63a2\u3057\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea-  \u767b\u9332\u7121\u6599",
        'Package_name' : "dating.bump",
        'installs' : "10,000+",
        'version' : "2.3.3",
    },
    '489472613' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-19T18:07:40Z",
        'App_name' : "Accor All - Reserva hoteles",
        'Package_name' : "fr.accor.push",
        'version' : "12.66.0",
        'minimumOsVersion' : "14.0",
    },
    'com.regz.chientuong' :
    {
        'category' : "Games",
        'updated' : "10 Nov 2021",
        'App_name' : "Chi\u1ebfn T\u01b0\u1edbng Tam Qu\u1ed1c",
        'Package_name' : "com.regz.chientuong",
        'installs' : "100,000+",
        'version' : "1.6.798",
    },
    'com.taoist.gp' :
    {
        'category' : "Games",
        'updated' : "15 Apr 2022",
        'App_name' : "\u9053\u58eb\u51fa\u89c0-\u6797\u6b63\u82f1\u6b63\u7248\u6388\u6b0a",
        'Package_name' : "com.taoist.gp",
        'installs' : "100,000+",
        'version' : "1.0.10",
    },
    'com.theentertainerme.entertainer' :
    {
        'category' : "Lifestyle",
        'updated' : "26 May 2022",
        'App_name' : "the ENTERTAINER",
        'Package_name' : "com.theentertainerme.entertainer",
        'installs' : "1,000,000+",
        'version' : "8.06.02",
    },
    'com.adpdigital.mbs.ayande' :
    {
        'category' : "Finance",
        'updated' : "02 Jun 2022",
        'App_name' : "\u0647\u0645\u0631\u0627\u0647 \u06a9\u0627\u0631\u062a | Hamrah Card",
        'Package_name' : "com.adpdigital.mbs.ayande",
        'installs' : "10,000,000+",
        'version' : "5.9.40",
    },
    'com.yhsh.ssjj' :
    {
        'category' : "Games",
        'updated' : "19 Aug 2021",
        'App_name' : "\u6c38\u6046\u5b88\u8b77-\u6230\u795e\u6b78\u4f86",
        'Package_name' : "com.yhsh.ssjj",
        'installs' : "500,000+",
        'version' : "1.21.0",
    },
    'com.stgl.global' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "War and Magic: Kingdom Reborn",
        'Package_name' : "com.stgl.global",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    '1458697406' :
    {
        'category' : "Games",
        'updated' : "2022-05-06T02:00:35Z",
        'App_name' : "\uc6cc\ub108\ube44\ucc4c\ub9b0\uc9c0",
        'Package_name' : "com.com2us.wannabe.ios.apple.global.normal",
        'version' : "1.10.17",
        'minimumOsVersion' : "10.0",
    },
    'com.moneytap.vn.app' :
    {
        'category' : "Finance",
        'updated' : "15 Sep 2021",
        'App_name' : "MoneyTap - Vay Ti\u1ec1n Tr\u1ea3 G\u00f3p - Powered by FE Credit",
        'Package_name' : "com.moneytap.vn.app",
        'installs' : "500,000+",
        'version' : "1.4.3",
    },
    'tw.com.aiming.caravan' :
    {
        'category' : "Games",
        'updated' : "06 Jun 2022",
        'App_name' : "\u5361\u62c9\u90a6 CARAVAN STORIES",
        'Package_name' : "tw.com.aiming.caravan",
        'installs' : "100,000+",
        'version' : "4.12.3",
    },
    'com.sixwaves.warshiprising' :
    {
        'category' : "Games",
        'updated' : "28 Dec 2021",
        'App_name' : "Warship Rising-10vs10",
        'Package_name' : "com.sixwaves.warshiprising",
        'installs' : "100,000+",
        'version' : "6.8.0",
    },
    'com.card.dda' :
    {
        'category' : "Finance",
        'updated' : "28 Apr 2022",
        'App_name' : "CARD.com Premium Banking",
        'Package_name' : "com.card.dda",
        'installs' : "100,000+",
        'version' : "1.0.34",
    },
    'vn.selly' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "Selly - D\u1ec5 d\u00e0ng b\u00e1n h\u00e0ng",
        'Package_name' : "vn.selly",
        'installs' : "1,000,000+",
        'version' : "2.22.0",
    },
    '1415694682' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-06T16:26:18Z",
        'App_name' : "Dinnerly",
        'Package_name' : "com.dinnerly.app",
        'version' : "3.7.0",
        'minimumOsVersion' : "12.0",
    },
    '531324961' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-05-16T23:17:38Z",
        'App_name' : "Cleartrip - Travel Booking App",
        'Package_name' : "com.cleartrip.iphoneapp",
        'version' : "22.5.0",
        'minimumOsVersion' : "11.0",
    },
    'im.kumu.ph' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "Kumu Livestream Community",
        'Package_name' : "im.kumu.ph",
        'installs' : "10,000,000+",
        'version' : "8.22.0",
    },
    'kr.co.welrixrental' :
    {
        'category' : "Shopping",
        'updated' : "01 Feb 2021",
        'App_name' : "\uc6f0\ub9ad\uc2a4\ub80c\ud0c8\ubab0 - \uc6f0\ubab0, \ub80c\ud0c8 \uc0dd\ud65c\uc774 \ub418\ub2e4",
        'Package_name' : "kr.co.welrixrental",
        'installs' : "10,000+",
        'version' : "1.18",
    },
    'com.digitaldukaan' :
    {
        'category' : "Finance",
        'updated' : "17 Jun 2022",
        'App_name' : "Digital Showroom: Business App",
        'Package_name' : "com.digitaldukaan",
        'installs' : "5,000,000+",
        'version' : "4.7.6",
    },
    'com.drifebeta' :
    {
        'category' : "Utilities",
        'updated' : "15 Jun 2022",
        'App_name' : "Drife - Taxi 3.0",
        'Package_name' : "com.drifebeta",
        'installs' : "10,000+",
        'version' : "105",
    },
    'com.scsoft.boribori' :
    {
        'category' : "Shopping",
        'updated' : "31 May 2022",
        'App_name' : "\ubcf4\ub9ac\ubcf4\ub9ac - \uc5c4\ub9c8\uc758 \ub9c8\uc74c\uc744 \uc785\ud788\ub294 \ucc29\ud55c\uac00\uac8c",
        'Package_name' : "com.scsoft.boribori",
        'installs' : "1,000,000+",
        'version' : "6.1.0",
    },
    '972558973' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-20T03:38:35Z",
        'App_name' : "Azar: Video-Chat Live",
        'Package_name' : "com.hpcnt.azar",
        'version' : "3.12.0",
        'minimumOsVersion' : "13.0",
    },
    'in.india.upi.flexpay' :
    {
        'category' : "Finance",
        'updated' : "13 May 2022",
        'App_name' : "FlexPay Instant Cash Loan App",
        'Package_name' : "in.india.upi.flexpay",
        'installs' : "1,000,000+",
        'version' : "2.9",
    },
    'jp.mfapps.loc.ekimemo' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "\u99c5\u30e1\u30e2\uff01 - \u30b9\u30c6\u30fc\u30b7\u30e7\u30f3\u30e1\u30e2\u30ea\u30fc\u30ba\uff01- \u9244\u9053\u4f4d\u7f6e\u30b2\u30fc\u30e0",
        'Package_name' : "jp.mfapps.loc.ekimemo",
        'installs' : "500,000+",
        'version' : "Varies with device",
    },
    'com.pinteng.lovelywholesale' :
    {
        'category' : "Shopping",
        'updated' : "30 May 2022",
        'App_name' : "LovelyWholesale-Fashion Online",
        'Package_name' : "com.pinteng.lovelywholesale",
        'installs' : "1,000,000+",
        'version' : "4.7.1",
    },
    'com.topgamesinc.zombies' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Plague of Z",
        'Package_name' : "com.topgamesinc.zombies",
        'installs' : "1,000,000+",
        'version' : "1.37.1",
    },
    'com.kakaogames.friendsTown' :
    {
        'category' : "Games",
        'updated' : "18 May 2022",
        'App_name' : "\ud504\ub80c\uc988\ud0c0\uc6b4",
        'Package_name' : "com.kakaogames.friendsTown",
        'installs' : "1,000,000+",
        'version' : "3.4.15",
    },
    'com.itau.empresas' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Ita\u00fa Empresas: Conta PJ e MEI",
        'Package_name' : "com.itau.empresas",
        'installs' : "5,000,000+",
        'version' : "5.89.1",
    },
    'com.graymatrix.did' :
    {
        'category' : "Entertainment",
        'updated' : "13 Jun 2022",
        'App_name' : "ZEE5: Movies, TV Shows, Series",
        'Package_name' : "com.graymatrix.did",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.snibblecorp.snibble' :
    {
        'category' : "Entertainment",
        'updated' : "08 Jun 2022",
        'App_name' : "Snibble",
        'Package_name' : "com.snibblecorp.snibble",
        'installs' : "10,000+",
        'version' : "2.0.32",
    },
    'br.lgfelicio' :
    {
        'category' : "Utilities",
        'updated' : "10 Jun 2022",
        'App_name' : "Fretebras: Baixou, Carregou!",
        'Package_name' : "br.lgfelicio",
        'installs' : "1,000,000+",
        'version' : "11.1.3",
    },
    '1506092240' :
    {
        'category' : "Education and Books",
        'updated' : "2022-06-19T20:26:51Z",
        'App_name' : "Zigazoo: Social Media for Kids",
        'Package_name' : "com.zigazoo.zigazoo",
        'version' : "6.4.0",
        'minimumOsVersion' : "11.4",
    },
    'com.mosl.mobile' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "Share Market Demat MO Investor",
        'Package_name' : "com.mosl.mobile",
        'installs' : "1,000,000+",
        'version' : "5.1.09",
    },
    'com.tokoko.and' :
    {
        'category' : "Finance",
        'updated' : "12 Jun 2022",
        'App_name' : "Tokoko Seller",
        'Package_name' : "com.tokoko.and",
        'installs' : "1,000,000+",
        'version' : "1.0.95",
    },
    'com.indodana.app' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "Indodana: PayLater & Pinjaman",
        'Package_name' : "com.indodana.app",
        'installs' : "5,000,000+",
        'version' : "3.34.6",
    },
    'jp.co.sonymusic.game.hinatosho' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "\u65e5\u5411\u574246\u3068\u3075\u3057\u304e\u306a\u56f3\u66f8\u5ba4",
        'Package_name' : "jp.co.sonymusic.game.hinatosho",
        'installs' : "50,000+",
        'version' : "1.17.1",
    },
    'com.inbox.clean.free.gmail.unsubscribe.smart.email.fresh.mailbox' :
    {
        'category' : "Tools",
        'updated' : "18 Jun 2021",
        'App_name' : "Instaclean - Organise your Inbox",
        'Package_name' : "com.inbox.clean.free.gmail.unsubscribe.smart.email.fresh.mailbox",
        'installs' : "1,000,000+",
        'version' : "2.7.1",
    },
    'com.starbucks.mobilecard' :
    {
        'category' : "Food & Drink",
        'updated' : "14 Jun 2022",
        'App_name' : "Starbucks",
        'Package_name' : "com.starbucks.mobilecard",
        'installs' : "10,000,000+",
        'version' : "6.24",
    },
    'com.pandats.evest' :
    {
        'category' : "Finance",
        'updated' : "06 Jun 2022",
        'App_name' : "Evest: Stocks Market Investing",
        'Package_name' : "com.pandats.evest",
        'installs' : "500,000+",
        'version' : "2.0.5",
    },
    'com.topgamesinc.evony' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Evony: The King's Return",
        'Package_name' : "com.topgamesinc.evony",
        'installs' : "100,000,000+",
        'version' : "4.25.0",
    },
    'com.mfine' :
    {
        'category' : "Health and Fitness",
        'updated' : "16 May 2022",
        'App_name' : "MFine: Your Healthcare App",
        'Package_name' : "com.mfine",
        'installs' : "5,000,000+",
        'version' : "1.7.9",
    },
    'com.global.gloryvn' :
    {
        'category' : "Games",
        'updated' : "19 May 2022",
        'App_name' : "MU: Vinh D\u1ef1\u2014Funtap",
        'Package_name' : "com.global.gloryvn",
        'installs' : "100,000+",
        'version' : "1.10.0",
    },
    'jp.edy.edyapp' :
    {
        'category' : "Shopping",
        'updated' : "04 Apr 2022",
        'App_name' : "\u697d\u5929Edy\u3067\u30ad\u30e3\u30c3\u30b7\u30e5\u30ec\u30b9\uff01\u30dd\u30a4\u30f3\u30c8\u304c\u8caf\u307e\u308b\u4fbf\u5229\u306a\u96fb\u5b50\u30de\u30cd\u30fc",
        'Package_name' : "jp.edy.edyapp",
        'installs' : "5,000,000+",
        'version' : "6.0.6",
    },
    'com.deliveryclub' :
    {
        'category' : "Food & Drink",
        'updated' : "17 Jun 2022",
        'App_name' : "Delivery Club: \u041f\u0440\u043e\u0434\u0443\u043a\u0442\u044b \u043d\u0430 \u0434\u043e\u043c",
        'Package_name' : "com.deliveryclub",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.lmzh.twgp' :
    {
        'category' : "Games",
        'updated' : "17 Mar 2022",
        'App_name' : "\u7375\u9b54\u8005Demon Hunter\uff1a\u89ba\u9192",
        'Package_name' : "com.lmzh.twgp",
        'installs' : "100,000+",
        'version' : "1.6.11.2014",
    },
    'kr.goodchoice.abouthere' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "13 Jun 2022",
        'App_name' : "\uc5ec\uae30\uc5b4\ub54c - \ud638\ud154, \ubaa8\ud154, \ud39c\uc158, \ud56d\uacf5\uad8c \ucd5c\uc800\uac00 \uc608\uc57d!",
        'Package_name' : "kr.goodchoice.abouthere",
        'installs' : "10,000,000+",
        'version' : "5.10.0",
    },
    'com.igg.android.dawnofdynasty' :
    {
        'category' : "Games",
        'updated' : "04 Jan 2022",
        'App_name' : "Dawn of Dynasty",
        'Package_name' : "com.igg.android.dawnofdynasty",
        'installs' : "500,000+",
        'version' : "1.3.1",
    },
    '1575830891' :
    {
        'category' : "Games",
        'updated' : "2022-06-10T02:57:27Z",
        'App_name' : "\u30b3\u30fc\u30c9\u30ae\u30a2\u30b9 \u53cd\u9006\u306e\u30eb\u30eb\u30fc\u30b7\u30e5\u3000\u30ed\u30b9\u30c8\u30b9\u30c8\u30fc\u30ea\u30fc\u30ba",
        'Package_name' : "com.dmm.games.loststories",
        'version' : "1.0.10",
        'minimumOsVersion' : "13.0",
    },
    'com.phrendly' :
    {
        'category' : "Entertainment",
        'updated' : "29 Mar 2022",
        'App_name' : "Phrendly Video Chat with Women",
        'Package_name' : "com.phrendly",
        'installs' : "500,000+",
        'version' : "2.9.6.802",
    },
    '778437357' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-13T08:42:51Z",
        'App_name' : "FlixBus - Voyages en bus",
        'Package_name' : "com.flixbus.ios.Flixbus",
        'version' : "7.43.0",
        'minimumOsVersion' : "12.0",
    },
    'com.stash.stashinvest' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Stash: Invest & Build Wealth",
        'Package_name' : "com.stash.stashinvest",
        'installs' : "10,000,000+",
        'version' : "2.0.40.1",
    },
    'com.happybeing.nsmiles' :
    {
        'category' : "Health and Fitness",
        'updated' : "09 May 2022",
        'App_name' : "Happy Being App",
        'Package_name' : "com.happybeing.nsmiles",
        'installs' : "10,000+",
        'version' : "6.5",
    },
    'com.scopely.startrek' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Star Trek\u2122 Fleet Command",
        'Package_name' : "com.scopely.startrek",
        'installs' : "5,000,000+",
        'version' : "1.000.24986",
    },
    'com.daki' :
    {
        'category' : "Food & Drink",
        'updated' : "04 Jun 2022",
        'App_name' : "Daki | Mercado em 15 minutos",
        'Package_name' : "com.daki",
        'installs' : "500,000+",
        'version' : "1.16.6",
    },
    'com.yjmg.soul.gl' :
    {
        'category' : "Games",
        'updated' : "08 Apr 2021",
        'App_name' : "\uc18c\uc6b8\uc6cc\ucee4 \uc544\uce74\ub370\ubbf8\uc544",
        'Package_name' : "com.yjmg.soul.gl",
        'installs' : "500,000+",
        'version' : "3.118486.14628",
    },
    'com.omada.social' :
    {
        'category' : "Social and Communication",
        'updated' : "01 Jun 2022",
        'App_name' : "Omada - Make Sport Social",
        'Package_name' : "com.omada.social",
        'installs' : "100,000+",
        'version' : "1.1.4",
    },
    'com.glu.stardomkim' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Kim Kardashian: Hollywood",
        'Package_name' : "com.glu.stardomkim",
        'installs' : "10,000,000+",
        'version' : "13.0.0",
    },
    '405049280' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-05-31T07:40:00Z",
        'App_name' : "adopte",
        'Package_name' : "com.adopteunmec.iphonefr",
        'version' : "4.8.1",
        'minimumOsVersion' : "11.0",
    },
    '955746162' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-15T00:09:50Z",
        'App_name' : "\u30e9\u30af\u30b5\u30b9-\u30d0\u30c3\u30b0\u5b9a\u984d\u30ec\u30f3\u30bf\u30eb(\u30d5\u30a1\u30c3\u30b7\u30e7\u30f3\u30d6\u30e9\u30f3\u30c9\u30d0\u30c3\u30b0)",
        'Package_name' : "jp.co.es-c.Laxus",
        'version' : "5.4.7",
        'minimumOsVersion' : "12.0",
    },
    '1128647632' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-08T16:58:18Z",
        'App_name' : "\u30e2\u30a2\u30b3\u30f3\u30bf\u30af\u30c8 - \u30b3\u30f3\u30bf\u30af\u30c8\u30ec\u30f3\u30ba \u901a\u8ca9\u30a2\u30d7\u30ea",
        'Package_name' : "jp.r-up.MoreconApp",
        'version' : "2.5.1",
        'minimumOsVersion' : "13.0",
    },
    'com.byhours.byhours' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "01 Apr 2022",
        'App_name' : "BYHOURS: Hotel Microstays",
        'Package_name' : "com.byhours.byhours",
        'installs' : "100,000+",
        'version' : "3.6.1",
    },
    'com.ponycanyon.game.prismstep' :
    {
        'category' : "Games",
        'updated' : "27 Mar 2022",
        'App_name' : "Re:\u30b9\u30c6\u30fc\u30b8\uff01\u30d7\u30ea\u30ba\u30e0\u30b9\u30c6\u30c3\u30d7",
        'Package_name' : "com.ponycanyon.game.prismstep",
        'installs' : "100,000+",
        'version' : "1.1.76",
    },
    'io.curio' :
    {
        'category' : "Education and Books",
        'updated' : "13 Apr 2022",
        'App_name' : "Curio: hear great journalism",
        'Package_name' : "io.curio",
        'installs' : "100,000+",
        'version' : "6.64.0",
    },
    'loppipoppi.dominoes' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "Dominoes - Classic Domino Game",
        'Package_name' : "loppipoppi.dominoes",
        'installs' : "10,000,000+",
        'version' : "2.5.18",
    },
    'com.omiai_jp' :
    {
        'category' : "Social and Communication",
        'updated' : "08 Jun 2022",
        'App_name' : "Omiai - \u30de\u30c3\u30c1\u30f3\u30b0\u3057\u3066\u51fa\u4f1a\u3044\u898b\u3064\u3051\u3088\u3046",
        'Package_name' : "com.omiai_jp",
        'installs' : "1,000,000+",
        'version' : "13.14.0",
    },
    'com.sixwaves.sengokuhk' :
    {
        'category' : "Games",
        'updated' : "06 May 2020",
        'App_name' : "\u9738\u738b\u4e4b\u91ce\u671b\uff08\u6230\u570b\u9738\u738b\u6230\u8a18\uff09",
        'Package_name' : "com.sixwaves.sengokuhk",
        'installs' : "10,000+",
        'version' : "1.0.3",
    },
    '749133753' :
    {
        'category' : "Utilities",
        'updated' : "2022-06-15T07:33:26Z",
        'App_name' : "Clime: NOAA Weather Radar Live",
        'Package_name' : "com.apalonapps.radarfree",
        'version' : "4.24.0",
        'minimumOsVersion' : "12.4",
    },
    'com.byu.id' :
    {
        'category' : "Lifestyle",
        'updated' : "28 May 2022",
        'App_name' : "by.U-The All-Digital Provider",
        'Package_name' : "com.byu.id",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.delivertech.flashrewards' :
    {
        'category' : "Lifestyle",
        'updated' : "17 Jun 2022",
        'App_name' : "Flash Rewards",
        'Package_name' : "com.delivertech.flashrewards",
        'installs' : "1,000,000+",
        'version' : "1.0.79",
    },
    'com.ninetwolabs.drawshop' :
    {
        'category' : "Games",
        'updated' : "18 May 2022",
        'App_name' : "\ubf51\uae30\ubc29 \ud0b9\ub364",
        'Package_name' : "com.ninetwolabs.drawshop",
        'installs' : "10,000+",
        'version' : "766",
    },
    '1196302015' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-19T06:57:11Z",
        'App_name' : "ToYou - Delivery & More",
        'Package_name' : "com.arammeem.toyou.ios",
        'version' : "1.66",
        'minimumOsVersion' : "12.0",
    },
    'jp.furyu.samurai' :
    {
        'category' : "Games",
        'updated' : "27 Apr 2022",
        'App_name' : "\u604b\u611b\u5e55\u672b\u30ab\u30ec\u30b7-\u65b0\u9078\u7d44\u3084\u5e55\u672b\u5fd7\u58eb\u3068\u306e\u5973\u6027\u5411\u3051\u4e59\u5973\u30fb\u604b\u611b\u30b2\u30fc\u30e0",
        'Package_name' : "jp.furyu.samurai",
        'installs' : "100,000+",
        'version' : "3.0.0",
    },
    'com.zhangyue.read' :
    {
        'category' : "Education and Books",
        'updated' : "14 Jun 2022",
        'App_name' : "iReader-Novels, Romance Story",
        'Package_name' : "com.zhangyue.read",
        'installs' : "1,000,000+",
        'version' : "9.15.1",
    },
    'com.gsretail.android.dalisalda' :
    {
        'category' : "Shopping",
        'updated' : "29 Mar 2022",
        'App_name' : "\ub2ec\ub9ac\uc0b4\ub2e4",
        'Package_name' : "com.gsretail.android.dalisalda",
        'installs' : "100,000+",
        'version' : "1.0.30",
    },
    'com.vng.codmvn' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Call Of Duty: Mobile VN",
        'Package_name' : "com.vng.codmvn",
        'installs' : "1,000,000+",
        'version' : "1.8.33",
    },
    'com.nexon.counterside' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "\uce74\uc6b4\ud130\uc0ac\uc774\ub4dc",
        'Package_name' : "com.nexon.counterside",
        'installs' : "100,000+",
        'version' : "4.8.205137",
    },
    '1605936806' :
    {
        'category' : "Games",
        'updated' : "2022-06-17T08:47:05Z",
        'App_name' : "Fill The Fridge!",
        'Package_name' : "com.GybeGames.FillTheFridgeTest",
        'version' : "2.3.2",
        'minimumOsVersion' : "11.0",
    },
    'com.yoozoo.kr.zwfz' :
    {
        'category' : "Games",
        'updated' : "15 Dec 2020",
        'App_name' : "\uc2dc\ub178\ub2c8\uc2a4",
        'Package_name' : "com.yoozoo.kr.zwfz",
        'installs' : "100,000+",
        'version' : "9.0.1",
    },
    'kr.co.richnco.goodrich' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "\uad7f\ub9ac\uce58 \u2013 \ubcf4\ud5d8\uc758 \ubc14\ub978\uc774\uce58",
        'Package_name' : "kr.co.richnco.goodrich",
        'installs' : "1,000,000+",
        'version' : "3.3.5",
    },
    'jp.co.rakuten.android' :
    {
        'category' : "Shopping",
        'updated' : "02 Jun 2022",
        'App_name' : "\u697d\u5929\u5e02\u5834 \u30b7\u30e7\u30c3\u30d4\u30f3\u30b0\u30a2\u30d7\u30ea",
        'Package_name' : "jp.co.rakuten.android",
        'installs' : "10,000,000+",
        'version' : "10.1.0",
    },
    'com.youtap.indo.merchant' :
    {
        'category' : "Finance",
        'updated' : "19 Apr 2022",
        'App_name' : "Youtap ID - Aplikasi Usaha",
        'Package_name' : "com.youtap.indo.merchant",
        'installs' : "100,000+",
        'version' : "3.8.10-GA2",
    },
    'com.requapp.requ' :
    {
        'category' : "Lifestyle",
        'updated' : "01 Jun 2022",
        'App_name' : "AttaPoll - Paid Surveys",
        'Package_name' : "com.requapp.requ",
        'installs' : "5,000,000+",
        'version' : "3.7.0",
    },
    'ru.mybook' :
    {
        'category' : "Education and Books",
        'updated' : "30 Mar 2022",
        'App_name' : "MyBook: books and audiobooks",
        'Package_name' : "ru.mybook",
        'installs' : "5,000,000+",
        'version' : "4.3.0",
    },
    'de.mypostcard.app' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "07 Jun 2022",
        'App_name' : "MyPostcard Postcard App",
        'Package_name' : "de.mypostcard.app",
        'installs' : "1,000,000+",
        'version' : "1.112.2",
    },
    'com.cygames.cycomi' :
    {
        'category' : "Education and Books",
        'updated' : "27 May 2022",
        'App_name' : "\u30de\u30f3\u30ac\u30fb\u4eba\u6c17\u30b3\u30df\u30c3\u30af\u304c\u8aad\u3081\u308b\u6f2b\u753b\u30a2\u30d7\u30ea - \u30b5\u30a4\u30b3\u30df",
        'Package_name' : "com.cygames.cycomi",
        'installs' : "1,000,000+",
        'version' : "6.6.2",
    },
    'com.pochicrane' :
    {
        'category' : "Games",
        'updated' : "21 Mar 2022",
        'App_name' : "POCHI-CRANE\uff08ONLINE CRANE GAME\uff09",
        'Package_name' : "com.pochicrane",
        'installs' : "100,000+",
        'version' : "3.3.0",
    },
    '1193135691' :
    {
        'category' : "Games",
        'updated' : "2022-06-16T00:07:18Z",
        'App_name' : "\u30ea\u30cd\u30fc\u30b8\u30e52 \u30ec\u30dc\u30ea\u30e5\u30fc\u30b7\u30e7\u30f3",
        'Package_name' : "com.netmarble.revolutionjp",
        'version' : "3.12.04",
        'minimumOsVersion' : "12.0",
    },
    'th.co.the1.the1app' :
    {
        'category' : "Lifestyle",
        'updated' : "08 Jun 2022",
        'App_name' : "The 1: Rewards, Points, Deals",
        'Package_name' : "th.co.the1.the1app",
        'installs' : "1,000,000+",
        'version' : "4.14.1",
    },
    'com.skypeople.fivestarsforklaytn' :
    {
        'category' : "Games",
        'updated' : "14 Jan 2022",
        'App_name' : "Five Stars for Klaytn",
        'Package_name' : "com.skypeople.fivestarsforklaytn",
        'installs' : "100,000+",
        'version' : "3.30.1",
    },
    'com.hily.app' :
    {
        'category' : "Social and Communication",
        'updated' : "15 Jun 2022",
        'App_name' : "Hily - Dating. Make Friends.",
        'Package_name' : "com.hily.app",
        'installs' : "10,000,000+",
        'version' : "3.5.5",
    },
    'com.perblue.disneyheroes' :
    {
        'category' : "Games",
        'updated' : "28 May 2022",
        'App_name' : "Disney Heroes: Battle Mode",
        'Package_name' : "com.perblue.disneyheroes",
        'installs' : "10,000,000+",
        'version' : "4.0.10",
    },
    '1139476429' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-16T15:03:25Z",
        'App_name' : "Picap",
        'Package_name' : "co.picap",
        'version' : "4.9.6",
        'minimumOsVersion' : "11.0",
    },
    '895687962' :
    {
        'category' : "Games",
        'updated' : "2022-06-14T05:06:09Z",
        'App_name' : "\u767d\u732b\u30d7\u30ed\u30b8\u30a7\u30af\u30c8",
        'Package_name' : "jp.colopl.wcat",
        'version' : "4.13.0",
        'minimumOsVersion' : "10.0",
    },
    'com.newchic.client' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "Newchic - Fashion Online",
        'Package_name' : "com.newchic.client",
        'installs' : "5,000,000+",
        'version' : "6.25.4",
    },
    'com.ourpalm.mu3.gp.tw' :
    {
        'category' : "Games",
        'updated' : "06 Apr 2022",
        'App_name' : "\u5947\u8e5fMU\uff1a\u6b63\u5b97\u7e8c\u4f5c",
        'Package_name' : "com.ourpalm.mu3.gp.tw",
        'installs' : "100,000+",
        'version' : "2.0.4",
    },
    'com.webzen.r2m.google' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "R2M",
        'Package_name' : "com.webzen.r2m.google",
        'installs' : "500,000+",
        'version' : "1.2.13",
    },
    '529488079' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-10T11:38:27Z",
        'App_name' : "Migros Money: F\u0131rsat Kampanya",
        'Package_name' : "com.technoface.migros",
        'version' : "12.08",
        'minimumOsVersion' : "10.2",
    },
    '618052941' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-04-18T01:35:05Z",
        'App_name' : "\u30a4\u30af\u30af\u30eb-\u51fa\u4f1a\u3044\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea",
        'Package_name' : "jp.prosgate.194964",
        'version' : "02.00.32",
        'minimumOsVersion' : "11.0",
    },
    'com.interfocusllc.patpat' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "PatPat\u00a0-\u00a0Kids\u00a0&\u00a0Baby\u00a0Clothing",
        'Package_name' : "com.interfocusllc.patpat",
        'installs' : "10,000,000+",
        'version' : "7.7.7",
    },
    'com.kuraki.dore8' :
    {
        'category' : "Games",
        'updated' : "07 Oct 2020",
        'App_name' : "\u30ca\u30be\u30c8\u30ad\u306e\u6642\u9593\uff1a\u8b0e\u89e3\u304d\u00d7\u30a2\u30c9\u30d9\u30f3\u30c1\u30e3\u30fc",
        'Package_name' : "com.kuraki.dore8",
        'installs' : "1,000,000+",
        'version' : "1.2.10",
    },
    'com.smo.deepcleaninc3d' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Deep Clean Inc. 3D",
        'Package_name' : "com.smo.deepcleaninc3d",
        'installs' : "10,000,000+",
        'version' : "1.10.53",
    },
    '330035194' :
    {
        'category' : "Not available",
        'updated' : "2022-06-14T16:31:56Z",
        'App_name' : "Snapfish: Photos Cards & Books",
        'Package_name' : "com.hp.Snapfish",
        'version' : "13.11.0",
        'minimumOsVersion' : "13.0",
    },
    'jp.co.cygames.princessconnectredive' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "\u30d7\u30ea\u30f3\u30bb\u30b9\u30b3\u30cd\u30af\u30c8\uff01Re:Dive",
        'Package_name' : "jp.co.cygames.princessconnectredive",
        'installs' : "1,000,000+",
        'version' : "6.2.0",
    },
    '521117624' :
    {
        'category' : "Finance",
        'updated' : "2022-06-01T12:11:25Z",
        'App_name' : "Garanti BBVA Mobile",
        'Package_name' : "com.garanti.cepsube",
        'version' : "11.3",
        'minimumOsVersion' : "12.0",
    },
    'com.gamehouse.deliciousdinercooking' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "Delicious World - Cooking Game",
        'Package_name' : "com.gamehouse.deliciousdinercooking",
        'installs' : "10,000,000+",
        'version' : "1.47.1",
    },
    'com.betterhalf' :
    {
        'category' : "Social and Communication",
        'updated' : "07 Jun 2022",
        'App_name' : "Betterhalf.ai - Matrimony App",
        'Package_name' : "com.betterhalf",
        'installs' : "1,000,000+",
        'version' : "4.1.5",
    },
    'vn.funtap.linhgioi3d' :
    {
        'category' : "Games",
        'updated' : "03 Mar 2022",
        'App_name' : "Linh Gi\u1edbi 3D - Linh Gioi 3D",
        'Package_name' : "vn.funtap.linhgioi3d",
        'installs' : "100,000+",
        'version' : "1.0.10",
    },
    'com.blackgold.cwto21' :
    {
        'category' : "Games",
        'updated' : "17 Feb 2022",
        'App_name' : "ChipWin To 21:Merge game",
        'Package_name' : "com.blackgold.cwto21",
        'installs' : "5,000,000+",
        'version' : "1.1.5",
    },
    'com.fragileheart.mp3editor' :
    {
        'category' : "Entertainment",
        'updated' : "06 Jun 2022",
        'App_name' : "Music Editor: Ringtone & MP3",
        'Package_name' : "com.fragileheart.mp3editor",
        'installs' : "10,000,000+",
        'version' : "5.6.13",
    },
    '978286449' :
    {
        'category' : "Social and Communication",
        'updated' : "2021-04-27T06:57:48Z",
        'App_name' : "Qeep\u00ae Dating: Chat, Meet, Love",
        'Package_name' : "com.bluelionmobile.qeep",
        'version' : "3.4.7",
        'minimumOsVersion' : "10.0",
    },
    'com.plarium.mechlegion' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "Mech Arena: Robot Showdown",
        'Package_name' : "com.plarium.mechlegion",
        'installs' : "10,000,000+",
        'version' : "2.07.02",
    },
    'com.shanggame.shtm' :
    {
        'category' : "Games",
        'updated' : "24 Feb 2022",
        'App_name' : "D-MEN\uff1aThe Defenders",
        'Package_name' : "com.shanggame.shtm",
        'installs' : "1,000,000+",
        'version' : "2.0.402",
    },
    'com.netmarble.sknightsmmo' :
    {
        'category' : "Games",
        'updated' : "10 May 2022",
        'App_name' : "\uc138\ube10\ub098\uc774\uce202",
        'Package_name' : "com.netmarble.sknightsmmo",
        'installs' : "1,000,000+",
        'version' : "1.36.04",
    },
    'com.fundoshiparade.kobitotown' :
    {
        'category' : "Games",
        'updated' : "07 Dec 2021",
        'App_name' : "Pocket Island - Puzzle Game",
        'Package_name' : "com.fundoshiparade.kobitotown",
        'installs' : "10,000+",
        'version' : "1.6.0",
    },
    'jp.j_o_e.NewPigCnt' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "\u990a\u8c6c\u5834MIX",
        'Package_name' : "jp.j_o_e.NewPigCnt",
        'installs' : "500,000+",
        'version' : "11.1",
    },
    'com.mox.app' :
    {
        'category' : "Finance",
        'updated' : "03 Jun 2022",
        'App_name' : "Mox Bank",
        'Package_name' : "com.mox.app",
        'installs' : "100,000+",
        'version' : "1.6.0",
    },
    'com.darts.hero.ml' :
    {
        'category' : "Games",
        'updated' : "22 Dec 2020",
        'App_name' : "Darts Hero",
        'Package_name' : "com.darts.hero.ml",
        'installs' : "10,000+",
        'version' : "12.0.1",
    },
    'com.sega.KemonoFriends3' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\u3051\u3082\u306e\u30d5\u30ec\u30f3\u30ba\uff13",
        'Package_name' : "com.sega.KemonoFriends3",
        'installs' : "100,000+",
        'version' : "2.6.3",
    },
    'com.h5g.high5casino' :
    {
        'category' : "Casino",
        'updated' : "11 May 2022",
        'App_name' : "High 5 Casino Vegas Slot Games",
        'Package_name' : "com.h5g.high5casino",
        'installs' : "1,000,000+",
        'version' : "4.31.1",
    },
    'cn.danatech.xingseus' :
    {
        'category' : "Education and Books",
        'updated' : "15 Jun 2022",
        'App_name' : "PictureThis - Plant Identifier",
        'Package_name' : "cn.danatech.xingseus",
        'installs' : "10,000,000+",
        'version' : "3.25",
    },
    'com.playmgmcasino.nj' :
    {
        'category' : "Casino",
        'updated' : "19 Apr 2022",
        'App_name' : "BetMGM Online Casino",
        'Package_name' : "com.playmgmcasino.nj",
        'installs' : "100,000+",
        'version' : "22.04.16",
    },
    '434803180' :
    {
        'category' : "Games",
        'updated' : "2020-09-01T01:18:41Z",
        'App_name' : "\u63db\u91d1\u3067\u304d\u308bRPG\u3010DORAKEN\u3011\u304a\u5c0f\u9063\u3044\u7a3c\u304e\uff01\u7a3c\u3052\u308b\u30a2\u30d7\u30ea",
        'Package_name' : "jp.doraken",
        'version' : "15.5.0",
        'minimumOsVersion' : "9.3",
    },
    'com.mbmobile' :
    {
        'category' : "Finance",
        'updated' : "12 Jun 2022",
        'App_name' : "MB Bank",
        'Package_name' : "com.mbmobile",
        'installs' : "10,000,000+",
        'version' : "5.8",
    },
    'com.efun.sishen.ae' :
    {
        'category' : "Games",
        'updated' : "29 Dec 2021",
        'App_name' : "BLEACH:\u00a0Eternal\u00a0Soul",
        'Package_name' : "com.efun.sishen.ae",
        'installs' : "1,000,000+",
        'version' : "1.9.31",
    },
    'com.neowiz.game.koh' :
    {
        'category' : "Games",
        'updated' : "28 Mar 2022",
        'App_name' : "Kingdom of Heroes - RPG",
        'Package_name' : "com.neowiz.game.koh",
        'installs' : "100,000+",
        'version' : "3.05.000",
    },
    'com.taxis99' :
    {
        'category' : "Utilities",
        'updated' : "17 Jun 2022",
        'App_name' : "99 - Private Driver and Taxi",
        'Package_name' : "com.taxis99",
        'installs' : "50,000,000+",
        'version' : "Varies with device",
    },
    'com.cozy.android' :
    {
        'category' : "Shopping",
        'updated' : "31 May 2022",
        'App_name' : "COZY-Fashion shopping",
        'Package_name' : "com.cozy.android",
        'installs' : "100,000+",
        'version' : "1.2.0",
    },
    'com.wegames.gk.jp.and' :
    {
        'category' : "Games",
        'updated' : "14 May 2022",
        'App_name' : "\u5927\u4e09\u56fd\u5fd7",
        'Package_name' : "com.wegames.gk.jp.and",
        'installs' : "100,000+",
        'version' : "3.1.7110",
    },
    'com.play.rogt' :
    {
        'category' : "Games",
        'updated' : "21 Apr 2022",
        'App_name' : "RO\u4ed9\u5883\u50b3\u8aaa\uff1a\u65b0\u4e16\u4ee3\u7684\u8a95\u751f",
        'Package_name' : "com.play.rogt",
        'installs' : "500,000+",
        'version' : "2.4.0.220420.3",
    },
    'com.dhtz.fighting.tz' :
    {
        'category' : "Games",
        'updated' : "13 May 2022",
        'App_name' : "\u9b25\u9b42\u9ab0\u5b50 - \u7121\u76e1\u5854\u9632",
        'Package_name' : "com.dhtz.fighting.tz",
        'installs' : "100,000+",
        'version' : "4.1.1",
    },
    'com.ykhw.sgsmjz' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "\u4e09\u56fd\u5fd7\u540d\u5c06\u4f1d",
        'Package_name' : "com.ykhw.sgsmjz",
        'installs' : "100,000+",
        'version' : "3.0.29",
    },
    '1401500803' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-18T19:26:37Z",
        'App_name' : "\u0421\u0430\u043c\u043e\u043a\u0430\u0442\u30fb\u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u0435\u0434\u044b\u30fb\u043f\u0440\u043e\u0434\u0443\u043a\u0442\u043e\u0432",
        'Package_name' : "ru.sbcs.store",
        'version' : "3.52.0",
        'minimumOsVersion' : "11.0",
    },
    '372648912' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-14T18:55:36Z",
        'App_name' : "MeetMe - Meet, Chat & Go Live",
        'Package_name' : "com.myYearbook.MyYearbook",
        'version' : "14.41.0",
        'minimumOsVersion' : "11.0",
    },
    'com.drop.loyalty.android' :
    {
        'category' : "Shopping",
        'updated' : "04 Jun 2022",
        'App_name' : "Drop: Cash Back Shopping App",
        'Package_name' : "com.drop.loyalty.android",
        'installs' : "1,000,000+",
        'version' : "1.97.0",
    },
    '836071680' :
    {
        'category' : "Social and Communication",
        'updated' : "2021-05-26T16:54:09Z",
        'App_name' : "Wishbone - Compare Anything",
        'Package_name' : "com.Science.Cyclops",
        'version' : "9.3.6",
        'minimumOsVersion' : "13.0",
    },
    'com.hibobi.store' :
    {
        'category' : "Shopping",
        'updated' : "25 May 2022",
        'App_name' : "hibobi-enrich baby's childhood",
        'Package_name' : "com.hibobi.store",
        'installs' : "5,000,000+",
        'version' : "v2.4.0",
    },
    'com.autoflipz.user' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "14 Oct 2021",
        'App_name' : "AutoFlipz : One App For All Your Car Needs",
        'Package_name' : "com.autoflipz.user",
        'installs' : "5,000+",
        'version' : "4.2.4",
    },
    'com.crazylabs.asmr.cut' :
    {
        'category' : "Games",
        'updated' : "12 Apr 2022",
        'App_name' : "ASMR Slicing",
        'Package_name' : "com.crazylabs.asmr.cut",
        'installs' : "50,000,000+",
        'version' : "1.9.9.0",
    },
    'com.trastra.mobile' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "TRASTRA: Crypto Wallet & Card",
        'Package_name' : "com.trastra.mobile",
        'installs' : "50,000+",
        'version' : "2.7.6",
    },
    '1544942129' :
    {
        'category' : "Games",
        'updated' : "2022-02-10T23:38:15Z",
        'App_name' : "Parkour Mania ninja",
        'Package_name' : "com.haitam.Mania",
        'version' : "2.1",
        'minimumOsVersion' : "11.0",
    },
    'com.linecorp.LGCHEF' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "LINE CHEF Sesame Street Tie-Up",
        'Package_name' : "com.linecorp.LGCHEF",
        'installs' : "1,000,000+",
        'version' : "1.19.1.0",
    },
    'com.audible.application' :
    {
        'category' : "Education and Books",
        'updated' : "17 Jun 2022",
        'App_name' : "Audible: audiobooks & podcasts",
        'Package_name' : "com.audible.application",
        'installs' : "100,000,000+",
        'version' : "3.28.0",
    },
    '1054302942' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-05-14T05:37:29Z",
        'App_name' : "bTaskee - Maids and Cleaning",
        'Package_name' : "com.lanterns.btaskee",
        'version' : "3.4.0",
        'minimumOsVersion' : "10.0",
    },
    'com.apollo.patientapp' :
    {
        'category' : "Health and Fitness",
        'updated' : "16 Jun 2022",
        'App_name' : "Apollo 247 - Health & Medicine",
        'Package_name' : "com.apollo.patientapp",
        'installs' : "10,000,000+",
        'version' : "6.5.0",
    },
    'net.btpro.client.karaca' :
    {
        'category' : "Shopping",
        'updated' : "03 Jun 2022",
        'App_name' : "Karaca",
        'Package_name' : "net.btpro.client.karaca",
        'installs' : "1,000,000+",
        'version' : "2.0.28",
    },
    'ru.rabota.app2' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Rabota.ru: Job search app",
        'Package_name' : "ru.rabota.app2",
        'installs' : "10,000,000+",
        'version' : "4.55.2",
    },
    'com.teledoc' :
    {
        'category' : "Health and Fitness",
        'updated' : "25 Oct 2021",
        'App_name' : "Teledoc - b\u00e1c s\u0129 c\u1ee7a b\u1ea1n",
        'Package_name' : "com.teledoc",
        'installs' : "10,000+",
        'version' : "1.2.39",
    },
    'com.flipkart.shopsy' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Shopsy Shopping App - Flipkart",
        'Package_name' : "com.flipkart.shopsy",
        'installs' : "50,000,000+",
        'version' : "7.17",
    },
    'com.nexon.da3' :
    {
        'category' : "Games",
        'updated' : "13 Dec 2021",
        'App_name' : "\ub2e4\ud06c\uc5b4\ubca4\uc8003",
        'Package_name' : "com.nexon.da3",
        'installs' : "1,000,000+",
        'version' : "1.58.2",
    },
    'com.rockbite.deeptown' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Deep Town: Idle Mining Tycoon",
        'Package_name' : "com.rockbite.deeptown",
        'installs' : "10,000,000+",
        'version' : "5.4.7",
    },
    'br.com.meutudo' :
    {
        'category' : "Finance",
        'updated' : "02 Jun 2022",
        'App_name' : "meutudo. Empr\u00e9stimo, INSS FGTS",
        'Package_name' : "br.com.meutudo",
        'installs' : "1,000,000+",
        'version' : "3.3.0",
    },
    'com.migrosmagazam' :
    {
        'category' : "Lifestyle",
        'updated' : "10 Jun 2022",
        'App_name' : "Migros Money: F\u0131rsat Kampanya",
        'Package_name' : "com.migrosmagazam",
        'installs' : "1,000,000+",
        'version' : "4.5.2",
    },
    '1400220482' :
    {
        'category' : "Finance",
        'updated' : "2022-06-13T14:39:12Z",
        'App_name' : "SmartPay-Chuy\u00ean gia thanh to\u00e1n",
        'Package_name' : "vn.com.paysmart",
        'version' : "2.70.1",
        'minimumOsVersion' : "10.0",
    },
    'com.square_enix.android_googleplay.FFBEWW' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "FINAL FANTASY  BRAVE EXVIUS",
        'Package_name' : "com.square_enix.android_googleplay.FFBEWW",
        'installs' : "10,000,000+",
        'version' : "7.3.0",
    },
    '1466736988' :
    {
        'category' : "Games",
        'updated' : "2022-05-11T10:27:24Z",
        'App_name' : "KartRider Rush+",
        'Package_name' : "com.nexon.kart",
        'version' : "1.13.8",
        'minimumOsVersion' : "9.0",
    },
    'com.netease.g104na.gb' :
    {
        'category' : "Games",
        'updated' : "27 Apr 2022",
        'App_name' : "MARVEL Super War",
        'Package_name' : "com.netease.g104na.gb",
        'installs' : "10,000,000+",
        'version' : "3.17.2",
    },
    'com.gamevil.heiroflight.android.google.global.normal' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "HEIR OF LIGHT",
        'Package_name' : "com.gamevil.heiroflight.android.google.global.normal",
        'installs' : "1,000,000+",
        'version' : "6.9.2",
    },
    'net.zenius.mobile' :
    {
        'category' : "Education and Books",
        'updated' : "02 Jun 2022",
        'App_name' : "Zenius - #GantiCaraBelajar",
        'Package_name' : "net.zenius.mobile",
        'installs' : "5,000,000+",
        'version' : "2.6.9",
    },
    'com.twitter.android' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "Twitter",
        'Package_name' : "com.twitter.android",
        'installs' : "1,000,000,000+",
        'version' : "Varies with device",
    },
    'jp.co.ponos.battlecatsen' :
    {
        'category' : "Games",
        'updated' : "17 May 2022",
        'App_name' : "The Battle Cats",
        'Package_name' : "jp.co.ponos.battlecatsen",
        'installs' : "10,000,000+",
        'version' : "11.5.0",
    },
    'com.chqbook.customers' :
    {
        'category' : "Finance",
        'updated' : "06 Jan 2022",
        'App_name' : "Chqbook | #SmallBusinessOwners",
        'Package_name' : "com.chqbook.customers",
        'installs' : "1,000,000+",
        'version' : "2.5.3",
    },
    'com.linecorp.linemanth' :
    {
        'category' : "Food & Drink",
        'updated' : "09 Jun 2022",
        'App_name' : "LINE MAN - Food, Shop, Taxi",
        'Package_name' : "com.linecorp.linemanth",
        'installs' : "10,000,000+",
        'version' : "10.10.1",
    },
    'la.manzana_verde_app' :
    {
        'category' : "Food & Drink",
        'updated' : "16 Jun 2022",
        'App_name' : "Manzana Verde Comida Saludable",
        'Package_name' : "la.manzana_verde_app",
        'installs' : "100,000+",
        'version' : "9.3.1",
    },
    'com.tencent.tmgp.sskkr' :
    {
        'category' : "Games",
        'updated' : "28 Jun 2021",
        'App_name' : "\uc138\uc778\ud2b8 \uc138\uc774\uc57c : \uac01\uc131",
        'Package_name' : "com.tencent.tmgp.sskkr",
        'installs' : "100,000+",
        'version' : "1.6.42.1",
    },
    'com.tangramgames.gourddoll.wordcrush' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Word Crush - Fun Puzzle Game",
        'Package_name' : "com.tangramgames.gourddoll.wordcrush",
        'installs' : "5,000,000+",
        'version' : "3.0.7",
    },
    'com.backmarket' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "Back Market. Buy. Sell. Easy.",
        'Package_name' : "com.backmarket",
        'installs' : "1,000,000+",
        'version' : "3.39",
    },
    'jp.appsta.socialtrade' :
    {
        'category' : "Finance",
        'updated' : "18 Apr 2022",
        'App_name' : "\u30b3\u30df\u30c8\u30ec\u653b\u7565\uff01\u30d5\u30a9\u30ed\u30fc\u3059\u308b\u3060\u3051\u7c21\u5358FX\uff01\u5b89\u5168\u306b\u7a3c\u3052\u308b\u526f\u696d",
        'Package_name' : "jp.appsta.socialtrade",
        'installs' : "5,000+",
        'version' : "2.8.3",
    },
    '332193586' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-05-18T00:35:13Z",
        'App_name' : "HRS Hotel Suche - Top Hotels",
        'Package_name' : "com.hrs.app",
        'version' : "7.10.2",
        'minimumOsVersion' : "11.0",
    },
    'jp.co.plusr.android.love_baby' :
    {
        'category' : "Health and Fitness",
        'updated' : "14 Jun 2022",
        'App_name' : "\u30ef\u30af\u30c1\u30f3\u30ce\u30fc\u30c8\uff5e\u8d64\u3061\u3083\u3093\u306e\u4e88\u9632\u63a5\u7a2e\u30b9\u30b1\u30b8\u30e5\u30fc\u30eb\u3092\u7ba1\u7406\uff5e",
        'Package_name' : "jp.co.plusr.android.love_baby",
        'installs' : "100,000+",
        'version' : "3.8.0",
    },
    'com.winpeople.filebogo' :
    {
        'category' : "Lifestyle",
        'updated' : "18 Nov 2021",
        'App_name' : "\ud30c\uc77c\ubcf4\uace0 - \ucee8\ud150\uce20, \ucd5c\uc2e0\uc601\ud654, \ub4dc\ub77c\ub9c8, \uc608\ub2a5, \uc560\ub2c8 \ub2e4\uc6b4\ub85c\ub4dc \uc571",
        'Package_name' : "com.winpeople.filebogo",
        'installs' : "50,000+",
        'version' : "1.10",
    },
    'com.vtcmobile.longkynguyen' :
    {
        'category' : "Games",
        'updated' : "11 Feb 2022",
        'App_name' : "Long K\u1ef7 Nguy\u00ean",
        'Package_name' : "com.vtcmobile.longkynguyen",
        'installs' : "1,000,000+",
        'version' : "1.0.190",
    },
    'com.keralamatrimony' :
    {
        'category' : "Social and Communication",
        'updated' : "23 May 2022",
        'App_name' : "Kerala Matrimony\u00ae-Marriage App",
        'Package_name' : "com.keralamatrimony",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.q8.flowers.app' :
    {
        'category' : "Shopping",
        'updated' : "31 May 2022",
        'App_name' : "Floward Online Flowers & Gifts",
        'Package_name' : "com.q8.flowers.app",
        'installs' : "1,000,000+",
        'version' : "5.8.4",
    },
    'com.video.meter' :
    {
        'category' : "Entertainment",
        'updated' : "24 May 2022",
        'App_name' : "VTION AudienceMeter",
        'Package_name' : "com.video.meter",
        'installs' : "100,000+",
        'version' : "1.2.1",
    },
    'com.cube.master.game' :
    {
        'category' : "Games",
        'updated' : "03 Sep 2021",
        'App_name' : "Cube Master",
        'Package_name' : "com.cube.master.game",
        'installs' : "100,000+",
        'version' : "Varies with device",
    },
    'com.expertoption' :
    {
        'category' : "Finance",
        'updated' : "17 Jun 2022",
        'App_name' : "ExpertOption - Mobile Trading",
        'Package_name' : "com.expertoption",
        'installs' : "10,000,000+",
        'version' : "4.2.1",
    },
    '1525824433' :
    {
        'category' : "Games",
        'updated' : "2022-06-15T13:11:10Z",
        'App_name' : "S.O.S:\uc2a4\ud14c\uc774\ud2b8 \uc624\ube0c \uc11c\ubc14\uc774\ubc8c",
        'Package_name' : "com.kingsgroup.ss.kr",
        'version' : "1.16.10",
        'minimumOsVersion' : "11.0",
    },
    'com.livwell' :
    {
        'category' : "Health and Fitness",
        'updated' : "09 Jun 2022",
        'App_name' : "LivWell -Move Burn Earn Repeat",
        'Package_name' : "com.livwell",
        'installs' : "50,000+",
        'version' : "7.6.0.0",
    },
    'in.startv.hotstar' :
    {
        'category' : "Entertainment",
        'updated' : "16 Jun 2022",
        'App_name' : "Hotstar",
        'Package_name' : "in.startv.hotstar",
        'installs' : "500,000,000+",
        'version' : "Varies with device",
    },
    '1354260888' :
    {
        'category' : "Games",
        'updated' : "2022-06-18T06:32:05Z",
        'App_name' : "Rise of Kingdoms",
        'Package_name' : "com.lilithgame.roc.ios",
        'version' : "1.0.58.22",
        'minimumOsVersion' : "10.0",
    },
    'com.app.makano' :
    {
        'category' : "Food & Drink",
        'updated' : "14 Jun 2022",
        'App_name' : "Pop Meals - order food",
        'Package_name' : "com.app.makano",
        'installs' : "1,000,000+",
        'version' : "64.4.1",
    },
    '1360223609' :
    {
        'category' : "Games",
        'updated' : "2021-03-22T01:00:51Z",
        'App_name' : "Warfare Strike:Global War",
        'Package_name' : "com.letang.warfarestrikeeng",
        'version' : "2.8.3",
        'minimumOsVersion' : "9.0",
    },
    'com.netmarble.enn' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\uc81c2\uc758 \ub098\ub77c: Cross Worlds",
        'Package_name' : "com.netmarble.enn",
        'installs' : "1,000,000+",
        'version' : "1.11.06",
    },
    'vn.funtap.tuyetthevolam' :
    {
        'category' : "Games",
        'updated' : "27 Jan 2022",
        'App_name' : "Giang H\u1ed3 Chi M\u1ed9ng - Tuyet The Vo Lam",
        'Package_name' : "vn.funtap.tuyetthevolam",
        'installs' : "1,000,000+",
        'version' : "1.1.36624",
    },
    '969229981' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-08T15:50:10Z",
        'App_name' : "SkipTheDishes - Food Delivery",
        'Package_name' : "ca.skipthedishes.customerapp",
        'version' : "4.19.0",
        'minimumOsVersion' : "13.0",
    },
    'ro.superbet.games' :
    {
        'category' : "Casino",
        'updated' : "10 Jun 2022",
        'App_name' : "Superbet Casino - Slots Online",
        'Package_name' : "ro.superbet.games",
        'installs' : "100,000+",
        'version' : "1.27.1",
    },
    'com.risingwings.castlecraft' :
    {
        'category' : "Games",
        'updated' : "30 Dec 2021",
        'App_name' : "Castle Craft - World War",
        'Package_name' : "com.risingwings.castlecraft",
        'installs' : "100,000+",
        'version' : "1.1.21",
    },
    'com.iqoption' :
    {
        'category' : "Finance",
        'updated' : "17 Jun 2022",
        'App_name' : "IQ Option \u2013 Trading Platform",
        'Package_name' : "com.iqoption",
        'installs' : "50,000,000+",
        'version' : "7.40.0",
    },
    'com.intellectokids.academy' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Intellecto Kids Learning Games",
        'Package_name' : "com.intellectokids.academy",
        'installs' : "500,000+",
        'version' : "4.20.0",
    },
    'com.efun.aets.sea' :
    {
        'category' : "Games",
        'updated' : "24 Dec 2021",
        'App_name' : "Master Topia",
        'Package_name' : "com.efun.aets.sea",
        'installs' : "1,000,000+",
        'version' : "1.2.1",
    },
    'cdiscount.mobile' :
    {
        'category' : "Shopping",
        'updated' : "03 Feb 2022",
        'App_name' : "Cdiscount",
        'Package_name' : "cdiscount.mobile",
        'installs' : "10,000,000+",
        'version' : "1.43.0-twa",
    },
    'com.NHNEnt.MSudda' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "\ud55c\uac8c\uc784 \uc12f\ub2e4&\ub9de\uace0",
        'Package_name' : "com.NHNEnt.MSudda",
        'installs' : "1,000,000+",
        'version' : "1.2.26",
    },
    'coinone.co.kr.official' :
    {
        'category' : "Finance",
        'updated' : "26 May 2022",
        'App_name' : "\ucf54\uc778\uc6d0 - Coinone",
        'Package_name' : "coinone.co.kr.official",
        'installs' : "1,000,000+",
        'version' : "2.9.20",
    },
    'com.topps.marvel' :
    {
        'category' : "Games",
        'updated' : "27 Apr 2022",
        'App_name' : "Marvel Collect! by Topps\u00ae",
        'Package_name' : "com.topps.marvel",
        'installs' : "1,000,000+",
        'version' : "18.4.0",
    },
    'com.ringcredible' :
    {
        'category' : "Social and Communication",
        'updated' : "01 Jun 2022",
        'App_name' : "Talk360: International Calls",
        'Package_name' : "com.ringcredible",
        'installs' : "1,000,000+",
        'version' : "7.2.7",
    },
    'com.mytona.seekersnotes.android' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Seekers Notes: Hidden Mystery",
        'Package_name' : "com.mytona.seekersnotes.android",
        'installs' : "10,000,000+",
        'version' : "2.25.0",
    },
    'jp.co.nohana' :
    {
        'category' : "Entertainment",
        'updated' : "07 Jun 2022",
        'App_name' : "\u30ce\u30cf\u30ca \u30d5\u30a9\u30c8\u30d6\u30c3\u30af\u3084\u5199\u771f\u30a2\u30eb\u30d0\u30e0\u306e\u4f5c\u6210\u3001\u30d5\u30a9\u30c8\u30a2\u30eb\u30d0\u30e0\u5370\u5237",
        'Package_name' : "jp.co.nohana",
        'installs' : "1,000,000+",
        'version' : "6.36.0",
    },
    '1314878525' :
    {
        'category' : "Games",
        'updated' : "2022-06-15T08:28:46Z",
        'App_name' : "Superbet Sport",
        'Package_name' : "ro.superbet.sport",
        'version' : "3.31.0",
        'minimumOsVersion' : "13.0",
    },
    'jp.co.recruit.rikunabinext' :
    {
        'category' : "Finance",
        'updated' : "16 May 2022",
        'App_name' : "\u8ee2\u8077\u306f\u30ea\u30af\u30ca\u30d3NEXT\uff0f\u6c42\u4eba \u4ed5\u4e8b\u63a2\u3057 \u8ee2\u8077\u6d3b\u52d5\u3092\u652f\u63f4 \u6b63\u793e\u54e1 \u6c42\u4eba\u3082 \u4ed5\u4e8b\u63a2\u3057 \u8ee2\u8077\u30b5\u30a4\u30c8\u3067\u8ee2\u8077",
        'Package_name' : "jp.co.recruit.rikunabinext",
        'installs' : "1,000,000+",
        'version' : "10.41.0",
    },
    'tutoring.app' :
    {
        'category' : "Education and Books",
        'updated' : "15 Jun 2022",
        'App_name' : "TUTORING | 24/7 Learn English",
        'Package_name' : "tutoring.app",
        'installs' : "1,000,000+",
        'version' : "1.11.12",
    },
    '1455009311' :
    {
        'category' : "Shopping",
        'updated' : "2022-05-26T13:43:21Z",
        'App_name' : "Cartlow",
        'Package_name' : "com.cartlow.customer",
        'version' : "5.7.1",
        'minimumOsVersion' : "12.3",
    },
    'kr.co.millie.millieshelf' :
    {
        'category' : "Education and Books",
        'updated' : "14 Apr 2022",
        'App_name' : "\ubc00\ub9ac\uc758 \uc11c\uc7ac - \ub3c5\uc11c\uc640 \ubb34\uc81c\ud55c \uce5c\ud574\uc9c0\ub9ac",
        'Package_name' : "kr.co.millie.millieshelf",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.c6bank.app' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "C6 Bank",
        'Package_name' : "com.c6bank.app",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.vividgames.realboxing2' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Real Boxing 2",
        'Package_name' : "com.vividgames.realboxing2",
        'installs' : "10,000,000+",
        'version' : "1.18.5",
    },
    '965782383' :
    {
        'category' : "Shopping",
        'updated' : "2022-03-21T06:06:13Z",
        'App_name' : "DealDash - Bid & Save Auctions",
        'Package_name' : "com.DealDash.DealDashMobile",
        'version' : "5.6.0",
        'minimumOsVersion' : "12.0",
    },
    'in.mohalla.video' :
    {
        'category' : "Social and Communication",
        'updated' : "17 Jun 2022",
        'App_name' : "Moj",
        'Package_name' : "in.mohalla.video",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.tg.hdqym.gp' :
    {
        'category' : "Games",
        'updated' : "11 Mar 2022",
        'App_name' : "\u6df7\u6c8c\u8d77\u6e90M",
        'Package_name' : "com.tg.hdqym.gp",
        'installs' : "100,000+",
        'version' : "53.0.0",
    },
    'com.qookkagame.sgzzlb.gp.global' :
    {
        'category' : "Games",
        'updated' : "10 Jan 2022",
        'App_name' : "Three Kingdoms Tactics:Global",
        'Package_name' : "com.qookkagame.sgzzlb.gp.global",
        'installs' : "50,000+",
        'version' : "Varies with device",
    },
    'com.herofincorp.simplycash' :
    {
        'category' : "Finance",
        'updated' : "23 May 2022",
        'App_name' : "Instant Personal Loan App",
        'Package_name' : "com.herofincorp.simplycash",
        'installs' : "1,000,000+",
        'version' : "2.2.11",
    },
    'com.siclo.plus' :
    {
        'category' : "Games",
        'updated' : "01 Apr 2022",
        'App_name' : "S\u00edclo+",
        'Package_name' : "com.siclo.plus",
        'installs' : "10,000+",
        'version' : "2.0.1",
    },
    '1452474937' :
    {
        'category' : "Games",
        'updated' : "2022-06-15T10:36:14Z",
        'App_name' : "State of Survival: Zombie War",
        'Package_name' : "com.kingsgroup.ss",
        'version' : "1.16.10",
        'minimumOsVersion' : "11.0",
    },
    'com.indie.shj.google.jp' :
    {
        'category' : "Games",
        'updated' : "23 Mar 2022",
        'App_name' : "\u5e7b\u7363\u30ec\u30b8\u30a7\u30f3\u30c9 -\u767e\u5996\u5fd7-",
        'Package_name' : "com.indie.shj.google.jp",
        'installs' : "50,000+",
        'version' : "1.1.4",
    },
    '1331893314' :
    {
        'category' : "Social and Communication",
        'updated' : "2020-03-16T02:15:05Z",
        'App_name' : "\u3059\u3050\u4f1a\u3048\u308b\u51fa\u4f1a\u3044KOKURE\u306e\u51fa\u4f1a\u3044\u30a2\u30d7\u30ea",
        'Package_name' : "jp.kokure.chat",
        'version' : "1.16",
        'minimumOsVersion' : "11.0",
    },
    'get.lokal.localnews' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "Lokal App : Local area updates",
        'Package_name' : "get.lokal.localnews",
        'installs' : "10,000,000+",
        'version' : "1.0.228",
    },
    '427242564' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-03T23:53:28Z",
        'App_name' : "\uc815\uc624\uc758\ub370\uc774\ud2b8 \uc18c\uac1c\ud305 - \ub3d9\ub124\uce5c\uad6c\uc640 \ucc44\ud305\ud558\uace0 \uc2f6\uc744 \ub54c",
        'Package_name' : "com.mozzet.fbting",
        'version' : "6.3.6",
        'minimumOsVersion' : "13.4",
    },
    'kr.co.usunshop' :
    {
        'category' : "Shopping",
        'updated' : "13 Jan 2022",
        'App_name' : "\u200b\uc6b0\uc120\uc0f5(USUN#) - \uc6b0\ub9ac\ub4e4\uc758 \uc120\ubb3c \uc140\ub809\uc0f5",
        'Package_name' : "kr.co.usunshop",
        'installs' : "100,000+",
        'version' : "1.3.1",
    },
    'com.ashleymadison.mobile' :
    {
        'category' : "Social and Communication",
        'updated' : "07 Jun 2022",
        'App_name' : "Ashley Madison",
        'Package_name' : "com.ashleymadison.mobile",
        'installs' : "1,000,000+",
        'version' : "4.5.29",
    },
    'com.ininal.wallet' :
    {
        'category' : "Finance",
        'updated' : "13 May 2022",
        'App_name' : "ininal",
        'Package_name' : "com.ininal.wallet",
        'installs' : "5,000,000+",
        'version' : "3.2.2",
    },
    'com.ballytechnologies.quickhitslots' :
    {
        'category' : "Casino",
        'updated' : "31 May 2022",
        'App_name' : "Quick Hit Casino Slot Games",
        'Package_name' : "com.ballytechnologies.quickhitslots",
        'installs' : "10,000,000+",
        'version' : "3.00.27",
    },
    '1486214495' :
    {
        'category' : "Games",
        'updated' : "2022-06-10T16:58:41Z",
        'App_name' : "Brain Test\uff1a\u3072\u3063\u304b\u3051\u30d1\u30ba\u30eb\u30b2\u30fc\u30e0",
        'Package_name' : "com.unicostudio.braintest",
        'version' : "2.730.1",
        'minimumOsVersion' : "11.0",
    },
    'haygot.togyah.app' :
    {
        'category' : "Education and Books",
        'updated' : "05 Jan 2022",
        'App_name' : "Toppr - Learning App for Class 5 - 12",
        'Package_name' : "haygot.togyah.app",
        'installs' : "10,000,000+",
        'version' : "6.5.115",
    },
    '1146359527' :
    {
        'category' : "Games",
        'updated' : "2022-06-16T00:50:22Z",
        'App_name' : "\u604b\u611b\u5e55\u672b\u30ab\u30ec\u30b7 \u5973\u6027\u5411\u3051\u4eba\u6c17\u604b\u611b\u30b2\u30fc\u30e0",
        'Package_name' : "jp.furyu.samurai",
        'version' : "3.0.1",
        'minimumOsVersion' : "10.0",
    },
    'com.gm99.sgby' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "\u4e09\u570b\uff1a\u82f1\u96c4\u7684\u69ae\u5149",
        'Package_name' : "com.gm99.sgby",
        'installs' : "100,000+",
        'version' : "2.13",
    },
    'com.joinroot.root' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Root: Better car insurance",
        'Package_name' : "com.joinroot.root",
        'installs' : "1,000,000+",
        'version' : "258.0.0",
    },
    'com.boticario.mobile' :
    {
        'category' : "Shopping",
        'updated' : "28 Apr 2022",
        'App_name' : "O Botic\u00e1rio",
        'Package_name' : "com.boticario.mobile",
        'installs' : "10,000,000+",
        'version' : "5.9.0",
    },
    'com.r2.zgm.krgoogle' :
    {
        'category' : "Games",
        'updated' : "29 Sep 2021",
        'App_name' : "\u5927\u6226\u56fd\u5fd7",
        'Package_name' : "com.r2.zgm.krgoogle",
        'installs' : "100,000+",
        'version' : "1.0.188",
    },
    'com.tencent.tmgp.sskeus' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "Saint Seiya Awakening: Knights of the Zodiac",
        'Package_name' : "com.tencent.tmgp.sskeus",
        'installs' : "5,000,000+",
        'version' : "1.6.51.1",
    },
    'info.menbo.yotuba' :
    {
        'category' : "Entertainment",
        'updated' : "20 May 2022",
        'App_name' : "\u3088\u3064\u3070",
        'Package_name' : "info.menbo.yotuba",
        'installs' : "100,000+",
        'version' : "1.2.0.0",
    },
    '1484471032' :
    {
        'category' : "Games",
        'updated' : "2022-04-26T06:12:15Z",
        'App_name' : "\u84bc\u85cd\u306e\u8a93\u3044 - \u30d6\u30eb\u30fc\u30aa\u30fc\u30b9",
        'Package_name' : "com.zephyrus.clsy.app",
        'version' : "3.2.4",
        'minimumOsVersion' : "10.1.1",
    },
    'com.kfc.kwt' :
    {
        'category' : "Food & Drink",
        'updated' : "20 Oct 2021",
        'App_name' : "KFC Kuwait - Order Food Online",
        'Package_name' : "com.kfc.kwt",
        'installs' : "500,000+",
        'version' : "5.14.3",
    },
    'com.herogame.gplay.punishing.grayraven.jp' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "\u30d1\u30cb\u30b7\u30f3\u30b0\uff1a\u30b0\u30ec\u30a4\u30ec\u30a4\u30f4\u30f3",
        'Package_name' : "com.herogame.gplay.punishing.grayraven.jp",
        'installs' : "100,000+",
        'version' : "1.22.3",
    },
    'jp.sunny.kanojo' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "\u8d85\u6b21\u5143\u5f7c\u5973: \u795e\u59eb\u653e\u7f6e\u306e\u5e7b\u60f3\u697d\u5712",
        'Package_name' : "jp.sunny.kanojo",
        'installs' : "100,000+",
        'version' : "1.0.813",
    },
    'com.mercadopago.wallet' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "Mercado Pago: cuenta digital",
        'Package_name' : "com.mercadopago.wallet",
        'installs' : "100,000,000+",
        'version' : "2.218.4",
    },
    'com.blancozone.dlssr.gp' :
    {
        'category' : "Games",
        'updated' : "06 Apr 2022",
        'App_name' : "\u6597\u7f85\u5927\u96783D\uff1a\u9b42\u5e2b\u5c0d\u6c7a\u2014\u771f3D\u771f\u6597\u7f85\uff0c\u767e\u5206\u767e\u9084\u539f\u52d5\u756b",
        'Package_name' : "com.blancozone.dlssr.gp",
        'installs' : "500,000+",
        'version' : "2.1.1",
    },
    'com.ecextreme.eccombat' :
    {
        'category' : "Games",
        'updated' : "15 Mar 2021",
        'App_name' : "ExtremE HUB",
        'Package_name' : "com.ecextreme.eccombat",
        'installs' : "100,000+",
        'version' : "1.0.1",
    },
    'org.findmykids.app' :
    {
        'category' : "Health and Fitness",
        'updated' : "17 Jun 2022",
        'App_name' : "Find My Kids: Parental Control",
        'Package_name' : "org.findmykids.app",
        'installs' : "10,000,000+",
        'version' : "2.4.87",
    },
    'com.flutrr.dating' :
    {
        'category' : "Lifestyle",
        'updated' : "13 May 2022",
        'App_name' : "flutrr-Safe Indian Dating App",
        'Package_name' : "com.flutrr.dating",
        'installs' : "100,000+",
        'version' : "1.0.36",
    },
    'jp.yyc.app.official2' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "YYC  \u51fa\u4f1a\u3044\u30fb\u604b\u6d3b\u30fb\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea-\u51fa\u4f1a\u3044\u7cfb",
        'Package_name' : "jp.yyc.app.official2",
        'installs' : "1,000,000+",
        'version' : "11.1.0",
    },
    'com.kakaogames.pcr' :
    {
        'category' : "Games",
        'updated' : "24 May 2022",
        'App_name' : "\ud504\ub9b0\uc138\uc2a4 \ucee4\ub125\ud2b8! Re:Dive",
        'Package_name' : "com.kakaogames.pcr",
        'installs' : "1,000,000+",
        'version' : "5.6.0",
    },
    'com.littlewarrior.google' :
    {
        'category' : "Games",
        'updated' : "07 Feb 2021",
        'App_name' : "\uc544\uc774\ub4e4 \ud0a4\uc6b0\uae30: \ucc10 \ubc29\uce58\ud615 RPG",
        'Package_name' : "com.littlewarrior.google",
        'installs' : "5,000+",
        'version' : "1.0.0.9",
    },
    '507760450' :
    {
        'category' : "Shopping",
        'updated' : "2022-05-31T15:58:53Z",
        'App_name' : "Auto.ru: \u043a\u0443\u043f\u0438\u0442\u044c, \u043f\u0440\u043e\u0434\u0430\u0442\u044c \u0430\u0432\u0442\u043e",
        'Package_name' : "ru.AutoRu",
        'version' : "12.11.0",
        'minimumOsVersion' : "14.0",
    },
    'com.kfc.oman' :
    {
        'category' : "Food & Drink",
        'updated' : "12 Oct 2021",
        'App_name' : "KFC Oman",
        'Package_name' : "com.kfc.oman",
        'installs' : "100,000+",
        'version' : "5.14.6",
    },
    'com.vng.tanomg3q' :
    {
        'category' : "Games",
        'updated' : "13 Apr 2022",
        'App_name' : "T\u00e2n OMG3Q VNG - \u0110\u1ea5u T\u01b0\u1edbng Tam Qu\u1ed1c Th\u1ebf H\u1ec7 M\u1edbi",
        'Package_name' : "com.vng.tanomg3q",
        'installs' : "500,000+",
        'version' : "0.50.75",
    },
    'com.jsone.kokgg' :
    {
        'category' : "Games",
        'updated' : "04 May 2022",
        'App_name' : "VERSUS : SEASON2 with AI",
        'Package_name' : "com.jsone.kokgg",
        'installs' : "500,000+",
        'version' : "1.2.2600",
    },
    'com.GoldenbellGames.GoldenbellBaccarat' :
    {
        'category' : "Casino",
        'updated' : "06 May 2021",
        'App_name' : "\uace8\ub4e0\ubca8\ud3ec\ucee4",
        'Package_name' : "com.GoldenbellGames.GoldenbellBaccarat",
        'installs' : "10,000+",
        'version' : "1.3.103",
    },
    'com.magisto' :
    {
        'category' : "Entertainment",
        'updated' : "12 May 2022",
        'App_name' : "Magisto - Video Editor & Music Slideshow Maker",
        'Package_name' : "com.magisto",
        'installs' : "50,000,000+",
        'version' : "6.23.0.20941",
    },
    'com.ulugame.with2KR.google' :
    {
        'category' : "Games",
        'updated' : "14 Sep 2021",
        'App_name' : "\uc704\ub4dc2:\uc2e0\uc758\uadc0\ud658",
        'Package_name' : "com.ulugame.with2KR.google",
        'installs' : "100,000+",
        'version' : "1.01.055.2",
    },
    'com.tencent.tmgp.sskjp' :
    {
        'category' : "Games",
        'updated' : "17 Sep 2021",
        'App_name' : "\u8056\u95d8\u58eb\u661f\u77e2 \u30e9\u30a4\u30b8\u30f3\u30b0\u30b3\u30b9\u30e2",
        'Package_name' : "com.tencent.tmgp.sskjp",
        'installs' : "100,000+",
        'version' : "1.6.9.1",
    },
    'com.infinitenessio.battle.artist' :
    {
        'category' : "Games",
        'updated' : "19 Apr 2022",
        'App_name' : "Pocket Battles - War Strategy",
        'Package_name' : "com.infinitenessio.battle.artist",
        'installs' : "100,000+",
        'version' : "0.7.3",
    },
    'com.icantw.goc' :
    {
        'category' : "Games",
        'updated' : "21 Mar 2022",
        'App_name' : "\u585e\u723e\u4e4b\u5149\uff1a\u4e00\u8d77\u7528\u53ef\u611b\u62ef\u6551\u4e16\u754c\uff01",
        'Package_name' : "com.icantw.goc",
        'installs' : "100,000+",
        'version' : "Varies with device",
    },
    'com.eterno.shortvideos' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "Josh: Short Videos App",
        'Package_name' : "com.eterno.shortvideos",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.healthsignz.consumer.oliva' :
    {
        'category' : "Health and Fitness",
        'updated' : "19 May 2022",
        'App_name' : "Oliva - Consult Dermatologists",
        'Package_name' : "com.healthsignz.consumer.oliva",
        'installs' : "100,000+",
        'version' : "1.0.20       ",
    },
    'com.shaadi.android' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "Shaadi.com\u00ae - Matrimony App",
        'Package_name' : "com.shaadi.android",
        'installs' : "10,000,000+",
        'version' : "9.17.1",
    },
    'com.Hezarfun.SinavBoard' :
    {
        'category' : "Games",
        'updated' : "17 Jun 2022",
        'App_name' : "S\u0131navBoard",
        'Package_name' : "com.Hezarfun.SinavBoard",
        'installs' : "100,000+",
        'version' : "5.377",
    },
    'il.co.mintapp.buyme' :
    {
        'category' : "Shopping",
        'updated' : "18 May 2022",
        'App_name' : "BUYME - \u05e4\u05e9\u05d5\u05d8 \u05dc\u05ea\u05ea \u05de\u05ea\u05e0\u05d5\u05ea",
        'Package_name' : "il.co.mintapp.buyme",
        'installs' : "500,000+",
        'version' : "4.12.0.186",
    },
    'com.qualson.superfan' :
    {
        'category' : "Education and Books",
        'updated' : "31 May 2022",
        'App_name' : "\uc288\ud37c\ud32c: \ubbf8\ub4dc, \uc601\ud654\ub85c \ubc30\uc6b0\ub294 \uc601\uc5b4",
        'Package_name' : "com.qualson.superfan",
        'installs' : "1,000,000+",
        'version' : "2.8.1",
    },
    '1253796614' :
    {
        'category' : "Games",
        'updated' : "2022-05-19T03:23:24Z",
        'App_name' : "RO\u4ed9\u5883\u50b3\u8aaa\uff1a\u5b88\u8b77\u6c38\u6046\u7684\u611b",
        'Package_name' : "com.gravity.ro.ios",
        'version' : "3.8.4",
        'minimumOsVersion' : "10.0",
    },
    'com.cjenm.sknights' :
    {
        'category' : "Games",
        'updated' : "14 Mar 2022",
        'App_name' : "\uc138\ube10\ub098\uc774\uce20",
        'Package_name' : "com.cjenm.sknights",
        'installs' : "10,000,000+",
        'version' : "5.8.10",
    },
    'com.tap4fun.kissofwar.googleplay.me' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Kiss of War - \u0642\u0628\u0644\u0629 \u0627\u0644\u062d\u0631\u0628",
        'Package_name' : "com.tap4fun.kissofwar.googleplay.me",
        'installs' : "1,000,000+",
        'version' : "1.83.0",
    },
    'de.sec.mobile' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "Lidl - Offers & Leaflets",
        'Package_name' : "de.sec.mobile",
        'installs' : "10,000,000+",
        'version' : "4.29.3(#167)",
    },
    'id.co.hyundaimotor.mmp' :
    {
        'category' : "Lifestyle",
        'updated' : "31 May 2022",
        'App_name' : "myHyundai Indonesia",
        'Package_name' : "id.co.hyundaimotor.mmp",
        'installs' : "10,000+",
        'version' : "1.0.54",
    },
    'com.mico' :
    {
        'category' : "Social and Communication",
        'updated' : "06 Jun 2022",
        'App_name' : "MICO: Go Live streaming & Chat",
        'Package_name' : "com.mico",
        'installs' : "50,000,000+",
        'version' : "7.0.5.3",
    },
    'acropole.garenta' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "11 Apr 2022",
        'App_name' : "Garenta",
        'Package_name' : "acropole.garenta",
        'installs' : "100,000+",
        'version' : "4.1.0",
    },
    'com.ucool.warofgods' :
    {
        'category' : "Games",
        'updated' : "09 Sep 2021",
        'App_name' : "War and Wit: Heroes Match 3",
        'Package_name' : "com.ucool.warofgods",
        'installs' : "500,000+",
        'version' : "0.0.178",
    },
    'com.flipkart.android' :
    {
        'category' : "Shopping",
        'updated' : "17 Jun 2022",
        'App_name' : "Flipkart Online Shopping App",
        'Package_name' : "com.flipkart.android",
        'installs' : "100,000,000+",
        'version' : "7.46",
    },
    '880551258' :
    {
        'category' : "Games",
        'updated' : "2022-06-10T01:00:08Z",
        'App_name' : "\ucef4\ud22c\uc2a4\ud504\ub85c\uc57c\uad6c2022",
        'Package_name' : "com.com2us.probaseball3d.normal.freefull.apple.global.ios.universal",
        'version' : "8.0.5",
        'minimumOsVersion' : "11.0",
    },
    'com.machipopo.media17' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "17LIVE - Live streaming",
        'Package_name' : "com.machipopo.media17",
        'installs' : "10,000,000+",
        'version' : "2.104.1.0",
    },
    'com.edgames.newworld' :
    {
        'category' : "Games",
        'updated' : "29 Dec 2021",
        'App_name' : "\uc0c8 \ucc9c\ud558\ub97c \uc5f4\ub2e4",
        'Package_name' : "com.edgames.newworld",
        'installs' : "100,000+",
        'version' : "31.10",
    },
    'com.touchnote.android' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "TouchNote: Gifts & Cards",
        'Package_name' : "com.touchnote.android",
        'installs' : "5,000,000+",
        'version' : "13.8.0",
    },
    'com.runtastic.android' :
    {
        'category' : "Health and Fitness",
        'updated' : "09 Jun 2022",
        'App_name' : "adidas Running: Sports Tracker",
        'Package_name' : "com.runtastic.android",
        'installs' : "50,000,000+",
        'version' : "Varies with device",
    },
    'com.ticker.android.cryptowire' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "CryptoWire: Price News Courses",
        'Package_name' : "com.ticker.android.cryptowire",
        'installs' : "500,000+",
        'version' : "2.0.30",
    },
    'com.albamon.app' :
    {
        'category' : "Finance",
        'updated' : "25 May 2022",
        'App_name' : "\uc54c\ubc14\ubaac - \uc54c\ubc14 \ucc44\uc6a9 \uc804\ubb38 \ud50c\ub7ab\ud3fc",
        'Package_name' : "com.albamon.app",
        'installs' : "10,000,000+",
        'version' : "4.5.4",
    },
    'com.mecl.la3eb' :
    {
        'category' : "Social and Communication",
        'updated' : "02 Jun 2022",
        'App_name' : "La3eb - \u0644\u0627\u0639\u0628 | Gamers Hub",
        'Package_name' : "com.mecl.la3eb",
        'installs' : "100,000+",
        'version' : "2.0.0",
    },
    'com.jinjinjin777sg.tapirus' :
    {
        'category' : "Casino",
        'updated' : "21 May 2022",
        'App_name' : "JinJinJin Casino",
        'Package_name' : "com.jinjinjin777sg.tapirus",
        'installs' : "50,000+",
        'version' : "4.23.1",
    },
    'com.indiejoy.mergeheroes' :
    {
        'category' : "Games",
        'updated' : "29 Dec 2020",
        'App_name' : "Merge Heroes Frontier: Casual RPG Online",
        'Package_name' : "com.indiejoy.mergeheroes",
        'installs' : "500,000+",
        'version' : "3.3.0",
    },
    'com.bomenas.baximo' :
    {
        'category' : "Finance",
        'updated' : "24 Mar 2022",
        'App_name' : "Trust Trade",
        'Package_name' : "com.bomenas.baximo",
        'installs' : "1,000,000+",
        'version' : "1.1.0",
    },
    'com.furlenco.android' :
    {
        'category' : "Lifestyle",
        'updated' : "06 Jun 2022",
        'App_name' : "Furlenco - Furniture on Rent",
        'Package_name' : "com.furlenco.android",
        'installs' : "1,000,000+",
        'version' : "8.26",
    },
    'com.wemakeprice' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "\uc704\uba54\ud504 \u2013 \uc1fc\ud551\ud504\ub85c \uc704\uba54\ud504\ub85c",
        'Package_name' : "com.wemakeprice",
        'installs' : "10,000,000+",
        'version' : "5.64.0",
    },
    'com.emagroups.ic2' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Tales of Wind",
        'Package_name' : "com.emagroups.ic2",
        'installs' : "5,000,000+",
        'version' : "4.2.1",
    },
    '1473762652' :
    {
        'category' : "Games",
        'updated' : "2022-05-23T02:10:28Z",
        'App_name' : "\ub9ac\uce58\ub9ac\uce58",
        'Package_name' : "com.ulugame.goldenkr.ios",
        'version' : "3.40",
        'minimumOsVersion' : "12.0",
    },
    'com.cocoplay.fashion.style' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Super Stylist Fashion Makeover",
        'Package_name' : "com.cocoplay.fashion.style",
        'installs' : "50,000,000+",
        'version' : "2.6.03",
    },
    '1370797511' :
    {
        'category' : "Not available",
        'updated' : "2022-03-03T13:50:38Z",
        'App_name' : "Youpix, carte postale & timbre",
        'Package_name' : "com.laposte.pemo",
        'version' : "3.0.2",
        'minimumOsVersion' : "12.1",
    },
    '888615473' :
    {
        'category' : "Games",
        'updated' : "2022-05-25T02:00:48Z",
        'App_name' : "LINE \u30dd\u30b3\u30dd\u30b3",
        'Package_name' : "com.linecorp.LGPKPK",
        'version' : "2.3.4",
        'minimumOsVersion' : "10.3",
    },
    'jp.dawn.scanit' :
    {
        'category' : "Games",
        'updated' : "11 May 2021",
        'App_name' : "Scan it!!",
        'Package_name' : "jp.dawn.scanit",
        'installs' : "5,000,000+",
        'version' : "5.3.0",
    },
    'jp.enish.delithe' :
    {
        'category' : "Games",
        'updated' : "20 May 2021",
        'App_name' : "De:Lithe \u672c\u683c\u30aa\u30f3\u30e9\u30a4\u30f3RPG",
        'Package_name' : "jp.enish.delithe",
        'installs' : "100,000+",
        'version' : "1.22.449",
    },
    'br.com.projetopolishop.mobile' :
    {
        'category' : "Shopping",
        'updated' : "16 May 2022",
        'App_name' : "Polishop: Comprar Online",
        'Package_name' : "br.com.projetopolishop.mobile",
        'installs' : "1,000,000+",
        'version' : "8.0.38",
    },
    'com.ak.ta.dainikbhaskar.activity' :
    {
        'category' : "Education and Books",
        'updated' : "17 Jun 2022",
        'App_name' : "Hindi News by Dainik Bhaskar",
        'Package_name' : "com.ak.ta.dainikbhaskar.activity",
        'installs' : "50,000,000+",
        'version' : "8.6.1",
    },
    'com.joytea.citydunk' :
    {
        'category' : "Games",
        'updated' : "29 Jun 2021",
        'App_name' : "\u30b7\u30c6\u30a3\u30c0\u30f3\u30af\uff1a\u30d5\u30ea\u30fc\u30b9\u30bf\u30a4\u30eb",
        'Package_name' : "com.joytea.citydunk",
        'installs' : "100,000+",
        'version' : "6.0.1",
    },
    'com.indie.shj.google.kr' :
    {
        'category' : "Games",
        'updated' : "20 Apr 2022",
        'App_name' : "\ud0dc\uace0\uc2e0\uc774\ub2f4:\uc2e0\uc758\ud55c\uc218",
        'Package_name' : "com.indie.shj.google.kr",
        'installs' : "100,000+",
        'version' : "1.1.1",
    },
    'jp.mbga.a12016007.lite' :
    {
        'category' : "Games",
        'updated' : "08 Mar 2022",
        'App_name' : "\u30b0\u30e9\u30f3\u30d6\u30eb\u30fc\u30d5\u30a1\u30f3\u30bf\u30b8\u30fc",
        'Package_name' : "jp.mbga.a12016007.lite",
        'installs' : "1,000,000+",
        'version' : "1.9.11",
    },
    'com.bandainamcoent.saoars' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "SAO Unleash Blading",
        'Package_name' : "com.bandainamcoent.saoars",
        'installs' : "1,000,000+",
        'version' : "3.4.0",
    },
    'com.lightfoxgames.dungeoneer' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Knight's Edge",
        'Package_name' : "com.lightfoxgames.dungeoneer",
        'installs' : "1,000,000+",
        'version' : "1.17.0",
    },
    'com.maxstacklabs.app.singx' :
    {
        'category' : "Finance",
        'updated' : "06 Jun 2022",
        'App_name' : "SingX\u2013Money Transfer Overseas",
        'Package_name' : "com.maxstacklabs.app.singx",
        'installs' : "10,000+",
        'version' : "3.1.25",
    },
    'jp.co.ncjapan.lineagem' :
    {
        'category' : "Games",
        'updated' : "21 May 2022",
        'App_name' : "Lineage M(\u30ea\u30cd\u30fc\u30b8\u30e5M)",
        'Package_name' : "jp.co.ncjapan.lineagem",
        'installs' : "100,000+",
        'version' : "1.3.0",
    },
    '1575865946' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-07T19:08:03Z",
        'App_name' : "Avis T\u00fcrkiye",
        'Package_name' : "com.otokocotomotiv.avis",
        'version' : "1.0.14",
        'minimumOsVersion' : "13.0",
    },
    '1094591345' :
    {
        'category' : "Games",
        'updated' : "2022-06-09T16:04:55Z",
        'App_name' : "Pok\u00e9mon GO",
        'Package_name' : "com.nianticlabs.pokemongo",
        'version' : "0.205.2",
        'minimumOsVersion' : "13.0",
    },
    'co.triller.droid' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "Triller: Social, Video Editor",
        'Package_name' : "co.triller.droid",
        'installs' : "10,000,000+",
        'version' : "v43.0b159",
    },
    '625257520' :
    {
        'category' : "Games",
        'updated' : "2022-06-01T17:30:32Z",
        'App_name' : "\ud558\uc2a4\uc2a4\ud1a4",
        'Package_name' : "com.blizzard.wtcg.hearthstone",
        'version' : "23.4.139963",
        'minimumOsVersion' : "10.2",
    },
    'com.weedmaps.app.android' :
    {
        'category' : "Health and Fitness",
        'updated' : "14 Jun 2022",
        'App_name' : "Weedmaps: Find Weed & Delivery",
        'Package_name' : "com.weedmaps.app.android",
        'installs' : "5,000,000+",
        'version' : "008.074.000",
    },
    'com.grandegames.slots.dafu.casino' :
    {
        'category' : "Casino",
        'updated' : "14 Jun 2022",
        'App_name' : "Jackpot World\u2122 - Slots Casino",
        'Package_name' : "com.grandegames.slots.dafu.casino",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'br.com.enjoei.app' :
    {
        'category' : "Shopping",
        'updated' : "01 Jun 2022",
        'App_name' : "enjoei: comprar e vender roupa",
        'Package_name' : "br.com.enjoei.app",
        'installs' : "10,000,000+",
        'version' : "7.26.0",
    },
    '1153808931' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-14T13:07:51Z",
        'App_name' : "AMARO: Comprar Moda e Beleza",
        'Package_name' : "com.amaro.app.AMARO",
        'version' : "2.2.3",
        'minimumOsVersion' : "12.0",
    },
    '404844644' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-13T07:14:02Z",
        'App_name' : "OTTO - Shopping und M\u00f6bel",
        'Package_name' : "de.otto.shop",
        'version' : "10.34.0",
        'minimumOsVersion' : "12.1",
    },
    'com.pinatarenterapp' :
    {
        'category' : "Lifestyle",
        'updated' : "09 Jun 2022",
        'App_name' : "Pi\u00f1ata: Make Rent Rewarding",
        'Package_name' : "com.pinatarenterapp",
        'installs' : "50,000+",
        'version' : "1.47.2",
    },
    'jp.stageinc.app.blue' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "EVERY .LIVE\uff08\u30a8\u30d6\u30ea\u30a3\u30e9\u30a4\u30d6\uff09\u30fc\u3000\u30e9\u30a4\u30d6\u914d\u4fe1\u30a2\u30d7\u30ea",
        'Package_name' : "jp.stageinc.app.blue",
        'installs' : "100,000+",
        'version' : "2.1.0",
    },
    'com.neowiz.playstudio.slot.casino' :
    {
        'category' : "Casino",
        'updated' : "19 May 2022",
        'App_name' : "\ud53c\ub9dd \ub274\ubca0\uac00\uc2a4 : \uc2ac\ub86f, \ubc14\uce74\ub77c, \ube14\ub799\uc7ad",
        'Package_name' : "com.neowiz.playstudio.slot.casino",
        'installs' : "500,000+",
        'version' : "3.0.17",
    },
    '1469351696' :
    {
        'category' : "Finance",
        'updated' : "2022-06-06T10:20:56Z",
        'App_name' : "YouHodler - Bitcoin Wallet",
        'Package_name' : "com.youhodler.youhodler",
        'version' : "5.1.0",
        'minimumOsVersion' : "12.0",
    },
    'nl.wehkamp.shop' :
    {
        'category' : "Shopping",
        'updated' : "01 Jun 2022",
        'App_name' : "Wehkamp",
        'Package_name' : "nl.wehkamp.shop",
        'installs' : "1,000,000+",
        'version' : "4.25.0",
    },
    'in.mohalla.sharechat' :
    {
        'category' : "Social and Communication",
        'updated' : "17 Jun 2022",
        'App_name' : "ShareChat - Made in India",
        'Package_name' : "in.mohalla.sharechat",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    '1262148500' :
    {
        'category' : "Finance",
        'updated' : "2022-06-10T03:16:43Z",
        'App_name' : "Crypto.com - Achat de BTC, ETH",
        'Package_name' : "co.mona.Monaco",
        'version' : "3.138",
        'minimumOsVersion' : "13.0",
    },
    'com.tap4fun.ape.gplay' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Age of Apes",
        'Package_name' : "com.tap4fun.ape.gplay",
        'installs' : "1,000,000+",
        'version' : "0.42.4",
    },
    'com.uxbert.sfa' :
    {
        'category' : "Games",
        'updated' : "30 Apr 2022",
        'App_name' : "Saudi Sports for All",
        'Package_name' : "com.uxbert.sfa",
        'installs' : "100,000+",
        'version' : "1.0.33",
    },
    'com.indie.jz3d.google.sea' :
    {
        'category' : "Games",
        'updated' : "25 Feb 2022",
        'App_name' : "\u5c01\u795e\u5f02\u4e16\u5f55\uff08\u56fd\u9645\u7248\uff09",
        'Package_name' : "com.indie.jz3d.google.sea",
        'installs' : "100,000+",
        'version' : "2.1.3",
    },
    'com.lydia' :
    {
        'category' : "Finance",
        'updated' : "11 Jun 2022",
        'App_name' : "Lydia - Current account",
        'Package_name' : "com.lydia",
        'installs' : "1,000,000+",
        'version' : "11.4.1",
    },
    'com.nexon.bnb2' :
    {
        'category' : "Games",
        'updated' : "03 Mar 2022",
        'App_name' : "BnB M",
        'Package_name' : "com.nexon.bnb2",
        'installs' : "5,000,000+",
        'version' : "2.14.0",
    },
    'com.herogame.gplay.punishing.grayraven.kr' :
    {
        'category' : "Games",
        'updated' : "29 Apr 2022",
        'App_name' : "\ud37c\ub2c8\uc2f1:\uadf8\ub808\uc774 \ub808\uc774\ube10",
        'Package_name' : "com.herogame.gplay.punishing.grayraven.kr",
        'installs' : "500,000+",
        'version' : "1.16.4",
    },
    'com.sleet.beautyplastic' :
    {
        'category' : "Social and Communication",
        'updated' : "27 May 2022",
        'App_name' : "\ubc14\ube44\ud1a1 - \uc131\ud615 & \ubdf0\ud2f0 \ucee4\ubba4\ub2c8\ud2f0",
        'Package_name' : "com.sleet.beautyplastic",
        'installs' : "1,000,000+",
        'version' : "5.36.0",
    },
    'ru.utkonos.android.utkonoid' :
    {
        'category' : "Food & Drink",
        'updated' : "16 Jun 2022",
        'App_name' : "Utkonos: grocery delivery",
        'Package_name' : "ru.utkonos.android.utkonoid",
        'installs' : "1,000,000+",
        'version' : "4.48.3(2107_release)",
    },
    'com.alrifaiarabia' :
    {
        'category' : "Finance",
        'updated' : "08 Sep 2021",
        'App_name' : "Al Rifai Arabia",
        'Package_name' : "com.alrifaiarabia",
        'installs' : "10,000+",
        'version' : "1.0.9",
    },
    'com.rapidpaisa.instant.loan.app' :
    {
        'category' : "Finance",
        'updated' : "10 May 2022",
        'App_name' : "RapidPaisa - Instant Loan App",
        'Package_name' : "com.rapidpaisa.instant.loan.app",
        'installs' : "1,000,000+",
        'version' : "1.16.0",
    },
    '901649419' :
    {
        'category' : "Games",
        'updated' : "2022-06-03T04:34:48Z",
        'App_name' : "\u30b9\u30cc\u30fc\u30d4\u30fc \u30c9\u30ed\u30c3\u30d7\u30b9 -\u30d1\u30ba\u30eb\uff1a\u30b9\u30cc\u30fc\u30d4\u30fc \u30d1\u30ba\u30eb",
        'Package_name' : "com.bij.snoopyDrops",
        'version' : "1.9.21",
        'minimumOsVersion' : "11.0",
    },
    'com.cardfeed.video_public' :
    {
        'category' : "Social and Communication",
        'updated' : "11 Jun 2022",
        'App_name' : "Public - Indian Local Videos",
        'Package_name' : "com.cardfeed.video_public",
        'installs' : "100,000,000+",
        'version' : "2.35.7",
    },
    'ai.rtzr.vito' :
    {
        'category' : "Tools",
        'updated' : "16 Jun 2022",
        'App_name' : "\ube44\ud1a0 VITO - \ub208\uc73c\ub85c \ubcf4\ub294 \ud1b5\ud654",
        'Package_name' : "ai.rtzr.vito",
        'installs' : "500,000+",
        'version' : "2.4.4",
    },
    'com.sevensenses.eternal' :
    {
        'category' : "Games",
        'updated' : "16 Mar 2022",
        'App_name' : "Eternal\uff1a\u6c38\u6046\u8056\u7d04",
        'Package_name' : "com.sevensenses.eternal",
        'installs' : "50,000+",
        'version' : "1.5.0",
    },
    'com.gamepub.by.g' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2021",
        'App_name' : "\uc5d8\ud504:\ub4dc\ub798\uace4\uc18c\ud658\uc0ac",
        'Package_name' : "com.gamepub.by.g",
        'installs' : "1,000,000+",
        'version' : "1.5.0",
    },
    'com.kfc.qatar' :
    {
        'category' : "Food & Drink",
        'updated' : "27 Oct 2021",
        'App_name' : "KFC Qatar - Order food online",
        'Package_name' : "com.kfc.qatar",
        'installs' : "500,000+",
        'version' : "5.14.4",
    },
    'com.trademanza' :
    {
        'category' : "Entertainment",
        'updated' : "01 Apr 2022",
        'App_name' : "Trademanza",
        'Package_name' : "com.trademanza",
        'installs' : "50,000+",
        'version' : "6.4",
    },
    '1526086695' :
    {
        'category' : "Entertainment",
        'updated' : "2022-06-05T03:21:58Z",
        'App_name' : "Hiya - Group Voice Chat",
        'Package_name' : "com.global.live.hiya",
        'version' : "3.24.0",
        'minimumOsVersion' : "10.0",
    },
    'vn.mytour.apps.android' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "08 Jun 2022",
        'App_name' : "Mytour: \u0110\u1eb7t Kh\u00e1ch S\u1ea1n, V\u00e9 Bay",
        'Package_name' : "vn.mytour.apps.android",
        'installs' : "500,000+",
        'version' : "4.2.7",
    },
    'com.nintendo.zaga' :
    {
        'category' : "Games",
        'updated' : "25 Mar 2022",
        'App_name' : "Dragalia Lost",
        'Package_name' : "com.nintendo.zaga",
        'installs' : "1,000,000+",
        'version' : "2.18.2",
    },
    'com.mhbl.sastasundar' :
    {
        'category' : "Health and Fitness",
        'updated' : "31 May 2022",
        'App_name' : "Flipkart Health+ (SastaSundar)",
        'Package_name' : "com.mhbl.sastasundar",
        'installs' : "1,000,000+",
        'version' : "4.0.5",
    },
    'com.dmm.games.kamihime' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "\u795e\u59ebPROJECT A \u7f8e\u5c11\u5973\u30ad\u30e3\u30e9x\u30d0\u30c8\u30ebRPG",
        'Package_name' : "com.dmm.games.kamihime",
        'installs' : "500,000+",
        'version' : "2.5.1",
    },
    'com.nextdoor' :
    {
        'category' : "Lifestyle",
        'updated' : "17 Jun 2022",
        'App_name' : "Nextdoor: Your Neighborhood",
        'Package_name' : "com.nextdoor",
        'installs' : "10,000,000+",
        'version' : "3.103.9",
    },
    'com.nttdocomo.dmagazine' :
    {
        'category' : "Education and Books",
        'updated' : "27 May 2022",
        'App_name' : "d\u30de\u30ac\u30b8\u30f3\u3000\u4eba\u6c17\u96d1\u8a8c\u304c\u3044\u3064\u3067\u3082\u3069\u3053\u3067\u3082\u30a2\u30d7\u30ea\u3067\u8aad\u307f\u653e\u984c\uff01",
        'Package_name' : "com.nttdocomo.dmagazine",
        'installs' : "1,000,000+",
        'version' : "3.1.18",
    },
    'ro.sephora.sephoraromania' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "Sephora : Beauty si Parfumuri",
        'Package_name' : "ro.sephora.sephoraromania",
        'installs' : "50,000+",
        'version' : "3.8.70",
    },
    '1020661630' :
    {
        'category' : "Finance",
        'updated' : "2022-06-08T20:14:56Z",
        'App_name' : "Rede: Maquininha de Cart\u00e3o",
        'Package_name' : "br.com.userede.rede",
        'version' : "6.2.0",
        'minimumOsVersion' : "12.4",
    },
    '1030383844' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-04-17T17:11:53Z",
        'App_name' : "Pop Meals - food delivery",
        'Package_name' : "com.fooddelivery.app",
        'version' : "64.2.2",
        'minimumOsVersion' : "11.0",
    },
    'com.hamada.itakesi' :
    {
        'category' : "Games",
        'updated' : "19 Aug 2021",
        'App_name' : "Crazy Eraser",
        'Package_name' : "com.hamada.itakesi",
        'installs' : "500,000+",
        'version' : "1.0.4",
    },
    'com.Blitz.bingo.cash.game.win.lucky.rewards.money' :
    {
        'category' : "Games",
        'updated' : "20 Aug 2021",
        'App_name' : "Bingo Blast-Lucky Fun Game",
        'Package_name' : "com.Blitz.bingo.cash.game.win.lucky.rewards.money",
        'installs' : "100,000+",
        'version' : "1.0.6",
    },
    'com.trafficsource.nodepositbonusescom' :
    {
        'category' : "Casino",
        'updated' : "17 May 2022",
        'App_name' : "No Deposit Welcome Bonuses",
        'Package_name' : "com.trafficsource.nodepositbonusescom",
        'installs' : "1,000,000+",
        'version' : "1.5.1",
    },
    'com.paxel' :
    {
        'category' : "Lifestyle",
        'updated' : "03 Jun 2022",
        'App_name' : "Paxel - Send Package Intercity",
        'Package_name' : "com.paxel",
        'installs' : "1,000,000+",
        'version' : "2.11.0",
    },
    'com.takimi.android' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "Taimi - LGBTQ+ Dating and Chat",
        'Package_name' : "com.takimi.android",
        'installs' : "5,000,000+",
        'version' : "5.1.179",
    },
    'com.mdf.repsol' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Waylet. Pagos con el m\u00f3vil",
        'Package_name' : "com.mdf.repsol",
        'installs' : "1,000,000+",
        'version' : "11.8.1",
    },
    'com.kingsgroup.sos' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "State of Survival: Zombie War",
        'Package_name' : "com.kingsgroup.sos",
        'installs' : "50,000,000+",
        'version' : "1.16.10",
    },
    'net.sejongtelecom.bbric' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "\ube44\ube0c\ub9ad",
        'Package_name' : "net.sejongtelecom.bbric",
        'installs' : "10,000+",
        'version' : "1.2.0",
    },
    'com.btpn.dc' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Jenius",
        'Package_name' : "com.btpn.dc",
        'installs' : "5,000,000+",
        'version' : "3.12.2",
    },
    'com.gzone.yyvi' :
    {
        'category' : "Games",
        'updated' : "28 Apr 2022",
        'App_name' : "V\u01b0\u01a1ng Qu\u1ed1c \u00c1nh S\u00e1ng - Gzone",
        'Package_name' : "com.gzone.yyvi",
        'installs' : "100,000+",
        'version' : "14.0",
    },
    '931337752' :
    {
        'category' : "Games",
        'updated' : "2022-06-14T07:02:42Z",
        'App_name' : "\u5e7b\u7363\u5951\u7d04\u30af\u30ea\u30d7\u30c8\u30e9\u30af\u30c8",
        'Package_name' : "jp.boi.cryptract",
        'version' : "v5.1.3",
        'minimumOsVersion' : "9.0",
    },
    'jp.co.atm.unison' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "\u30e6\u30cb\u30be\u30f3\u30ea\u30fc\u30b0-\u672c\u683cRPG/\u30ed\u30fc\u30eb\u30d7\u30ec\u30a4\u30f3\u30b0\u30b2\u30fc\u30e0-",
        'Package_name' : "jp.co.atm.unison",
        'installs' : "1,000,000+",
        'version' : "2.8.9",
    },
    'com.haulmont.shamrock.android' :
    {
        'category' : "Utilities",
        'updated' : "26 May 2022",
        'App_name' : "Addison Lee: Minicab & Courier",
        'Package_name' : "com.haulmont.shamrock.android",
        'installs' : "500,000+",
        'version' : "8.11.0",
    },
    'com.eyougame.sggh' :
    {
        'category' : "Games",
        'updated' : "25 Aug 2021",
        'App_name' : "\u53eb\u6211\u541b\u4e3b\uff1a\u7d05\u984f\u6c5f\u5c71\u6211\u90fd\u8981",
        'Package_name' : "com.eyougame.sggh",
        'installs' : "50,000+",
        'version' : "3.1.0.00090006",
    },
    'com.gravityga.teraclassic' :
    {
        'category' : "Games",
        'updated' : "21 Feb 2022",
        'App_name' : "\u30c6\u30e9\u30af\u30e9\u30b7\u30c3\u30af\uff08\u30c6\u30e9\u30af\u30e9\uff09",
        'Package_name' : "com.gravityga.teraclassic",
        'installs' : "50,000+",
        'version' : "1.4.0",
    },
    '1352230941' :
    {
        'category' : "Games",
        'updated' : "2022-03-30T06:00:22Z",
        'App_name' : "Dragalia Lost \uff5e\u5931\u843d\u7684\u9f8d\u7d46\uff5e",
        'Package_name' : "com.nintendo.zaga",
        'version' : "2.18.2",
        'minimumOsVersion' : "10.0",
    },
    'com.gittigidiyormobil' :
    {
        'category' : "Shopping",
        'updated' : "30 May 2022",
        'App_name' : "GittiGidiyor",
        'Package_name' : "com.gittigidiyormobil",
        'installs' : "10,000,000+",
        'version' : "3.61.1",
    },
    'com.giottus.giottus_mobile' :
    {
        'category' : "Finance",
        'updated' : "09 May 2022",
        'App_name' : "Giottus: Crypto Investing App",
        'Package_name' : "com.giottus.giottus_mobile",
        'installs' : "500,000+",
        'version' : "2.1.49",
    },
    'com.jp4399.SKF.an' :
    {
        'category' : "Games",
        'updated' : "22 Oct 2021",
        'App_name' : "\u84bc\u7a7a\u30d5\u30a1\u30f3\u30bf\u30b8\u30fc\uff5e\u904b\u547d\u306e\u30f4\u30a1\u30eb\u30ad\u30e5\u30ea\u30a2\uff5e",
        'Package_name' : "com.jp4399.SKF.an",
        'installs' : "50,000+",
        'version' : "1.1.6",
    },
    'com.denizbank.mobildeniz' :
    {
        'category' : "Finance",
        'updated' : "20 May 2022",
        'App_name' : "MobilDeniz",
        'Package_name' : "com.denizbank.mobildeniz",
        'installs' : "5,000,000+",
        'version' : "2.30.1",
    },
    '1245550834' :
    {
        'category' : "Games",
        'updated' : "2022-06-20T04:50:20Z",
        'App_name' : "\u30a2\u30ea\u30b9\u30fb\u30ae\u30a2\u30fb\u30a2\u30a4\u30ae\u30b9",
        'Package_name' : "jp.colopl.alice",
        'version' : "1.54.2",
        'minimumOsVersion' : "9.0",
    },
    'com.simplygood.ct' :
    {
        'category' : "Shopping",
        'updated' : "09 Apr 2022",
        'App_name' : "Canadian Tire: Shop Smarter",
        'Package_name' : "com.simplygood.ct",
        'installs' : "1,000,000+",
        'version' : "8.19.2",
    },
    '1384325538' :
    {
        'category' : "Entertainment",
        'updated' : "2022-06-16T23:57:51Z",
        'App_name' : "HAKUNA\uff08\u30cf\u30af\u30ca\uff09 - \u3086\u308b\u30b3\u30df\u30e5\u30e9\u30a4\u30d6\u914d\u4fe1\u30a2\u30d7\u30ea",
        'Package_name' : "com.movefastcompany.bora",
        'version' : "1.26.0",
        'minimumOsVersion' : "12.0",
    },
    'com.orange.rn.dop' :
    {
        'category' : "Tools",
        'updated' : "07 Jun 2022",
        'App_name' : "Orange Flex",
        'Package_name' : "com.orange.rn.dop",
        'installs' : "1,000,000+",
        'version' : "54.2.0",
    },
    'com.dgames.g23002011.android' :
    {
        'category' : "Games",
        'updated' : "19 Oct 2021",
        'App_name' : "Yu Yu Hakusho\uff1aBANG! Spirit Gun",
        'Package_name' : "com.dgames.g23002011.android",
        'installs' : "100,000+",
        'version' : "5.0.400",
    },
    'mx.com.bancoazteca.bazdigitalmovil' :
    {
        'category' : "Finance",
        'updated' : "11 Jun 2022",
        'App_name' : "Banco Azteca",
        'Package_name' : "mx.com.bancoazteca.bazdigitalmovil",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'net.savetime' :
    {
        'category' : "Food & Drink",
        'updated' : "06 Jul 2021",
        'App_name' : "SaveTime \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u043e\u0432 \u0438 \u0435\u0434\u044b \u043d\u0430 \u0437\u0430\u043a\u0430\u0437 \u043d\u0430 \u0434\u043e\u043c",
        'Package_name' : "net.savetime",
        'installs' : "100,000+",
        'version' : "4.1.1",
    },
    'com.palm.astrology' :
    {
        'category' : "Lifestyle",
        'updated' : "02 Dec 2021",
        'App_name' : "Astrology & Palm Master",
        'Package_name' : "com.palm.astrology",
        'installs' : "100,000+",
        'version' : "2.0.3",
    },
    'com.coindcx' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "CoinDCX Pro:Crypto Trading App",
        'Package_name' : "com.coindcx",
        'installs' : "1,000,000+",
        'version' : "2.03.006",
    },
    'work.anzy.anan' :
    {
        'category' : "Lifestyle",
        'updated' : "03 Mar 2022",
        'App_name' : "\u30d8\u30d6\u30f3",
        'Package_name' : "work.anzy.anan",
        'installs' : "100,000+",
        'version' : "1.3.0.0",
    },
    'com.surveysampling.mobile.quickthoughts' :
    {
        'category' : "Lifestyle",
        'updated' : "02 Jun 2022",
        'App_name' : "QuickThoughts: Take Surveys Earn Gift Card Rewards",
        'Package_name' : "com.surveysampling.mobile.quickthoughts",
        'installs' : "5,000,000+",
        'version' : "2.31.0",
    },
    'com.spar.mobile.app' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "SPAR SAMMEN",
        'Package_name' : "com.spar.mobile.app",
        'installs' : "100,000+",
        'version' : "7.3.1",
    },
    '872476884' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-05-17T09:33:50Z",
        'App_name' : "youbride \u5a5a\u6d3b\u30fb\u518d\u5a5a\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea",
        'Package_name' : "jp.youbride.ios",
        'version' : "4.3.26",
        'minimumOsVersion' : "10.0",
    },
    'com.shopee.my' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Shopee MY: No Shipping Fee",
        'Package_name' : "com.shopee.my",
        'installs' : "10,000,000+",
        'version' : "2.88.41",
    },
    'com.pozitron.hepsiburada' :
    {
        'category' : "Shopping",
        'updated' : "07 Jun 2022",
        'App_name' : "Hepsiburada: Online Shopping",
        'Package_name' : "com.pozitron.hepsiburada",
        'installs' : "10,000,000+",
        'version' : "5.4.5",
    },
    'com.me.starme.gamepoker' :
    {
        'category' : "Games",
        'updated' : "17 Jul 2021",
        'App_name' : "Teen Patti Star-3 Patti Online",
        'Package_name' : "com.me.starme.gamepoker",
        'installs' : "100,000+",
        'version' : "1.0.36",
    },
    'com.higgs.tilemaster3d' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "Tile Master 3D\u00ae - Match 3D",
        'Package_name' : "com.higgs.tilemaster3d",
        'installs' : "50,000,000+",
        'version' : "1.7.5",
    },
    'com.drpanda.town.street' :
    {
        'category' : "Education and Books",
        'updated' : "20 May 2022",
        'App_name' : "Dr. Panda Town - Let's Create!",
        'Package_name' : "com.drpanda.town.street",
        'installs' : "5,000,000+",
        'version' : "22.2.47",
    },
    'com.klab.lovelive.allstars' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "\u30e9\u30d6\u30e9\u30a4\u30d6\uff01\u30b9\u30af\u30fc\u30eb\u30a2\u30a4\u30c9\u30eb\u30d5\u30a7\u30b9\u30c6\u30a3\u30d0\u30ebALL STARS",
        'Package_name' : "com.klab.lovelive.allstars",
        'installs' : "100,000+",
        'version' : "3.2.4",
    },
    'com.square_enix.android_googleplay.RANBUjp' :
    {
        'category' : "Games",
        'updated' : "21 Jun 2021",
        'App_name' : "RANBU \u4e09\u56fd\u5fd7\u4e71\u821e",
        'Package_name' : "com.square_enix.android_googleplay.RANBUjp",
        'installs' : "10,000+",
        'version' : "1.1.91",
    },
    'com.bandainamcoent.opbrjp' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "ONE PIECE \u30d0\u30a6\u30f3\u30c6\u30a3\u30e9\u30c3\u30b7\u30e5 - \u30a2\u30af\u30b7\u30e7\u30f3\u30b2\u30fc\u30e0",
        'Package_name' : "com.bandainamcoent.opbrjp",
        'installs' : "500,000+",
        'version' : "51100",
    },
    'com.ksa.hardeesburger.fastfood' :
    {
        'category' : "Food & Drink",
        'updated' : "31 Mar 2022",
        'App_name' : "Hardee's Saudi Arabia - Burger & Sandwich Meals!",
        'Package_name' : "com.ksa.hardeesburger.fastfood",
        'installs' : "100,000+",
        'version' : "5.7.2",
    },
    '458056343' :
    {
        'category' : "Games",
        'updated' : "2022-06-09T13:30:28Z",
        'App_name' : "LIVE Score - the Fastest Score",
        'Package_name' : "kr.co.psynet.LiveScore",
        'version' : "26.2",
        'minimumOsVersion' : "12.1",
    },
    'ru.cardsmobile.mw3' :
    {
        'category' : "Shopping",
        'updated' : "06 Jun 2022",
        'App_name' : "\u041a\u043e\u0448\u0435\u043b\u0451\u043a: \u0434\u0438\u0441\u043a\u043e\u043d\u0442\u043d\u044b\u0435 \u043a\u0430\u0440\u0442\u044b",
        'Package_name' : "ru.cardsmobile.mw3",
        'installs' : "10,000,000+",
        'version' : "7.64.0",
    },
    '1438535599' :
    {
        'category' : "Utilities",
        'updated' : "2022-02-15T23:17:03Z",
        'App_name' : "Snowthority: Ski, Schnee, Lift",
        'Package_name' : "com.snowthority",
        'version' : "1.66.0",
        'minimumOsVersion' : "11.0",
    },
    'net.gamon.dqmslTW' :
    {
        'category' : "Games",
        'updated' : "24 Jan 2022",
        'App_name' : "\u52c7\u8005\u9b25\u60e1\u9f8d \u602a\u7269\u4ed9\u5883 SUPER LIGHT",
        'Package_name' : "net.gamon.dqmslTW",
        'installs' : "500,000+",
        'version' : "8.2.1",
    },
    'jp.Marvelous.girlcafegun' :
    {
        'category' : "Games",
        'updated' : "13 May 2020",
        'App_name' : "\u30ac\u30fc\u30eb\u30fb\u30ab\u30d5\u30a7\u30fb\u30ac\u30f3",
        'Package_name' : "jp.Marvelous.girlcafegun",
        'installs' : "100,000+",
        'version' : "1.1.4",
    },
    'com.dream11sportsguru' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "FanCode: Live Cricket & Score",
        'Package_name' : "com.dream11sportsguru",
        'installs' : "10,000,000+",
        'version' : "3.68.2",
    },
    'com.aoyun.getreminded' :
    {
        'category' : "Tools",
        'updated' : "10 Mar 2022",
        'App_name' : "GetReminded: Organise and Save",
        'Package_name' : "com.aoyun.getreminded",
        'installs' : "5,000+",
        'version' : "3.0.6",
    },
    'com.kwai.bulldog' :
    {
        'category' : "Entertainment",
        'updated' : "17 Jun 2022",
        'App_name' : "SnackVideo",
        'Package_name' : "com.kwai.bulldog",
        'installs' : "100,000,000+",
        'version' : "6.5.10.526002",
    },
    '1121898899' :
    {
        'category' : "Games",
        'updated' : "2022-06-02T04:01:47Z",
        'App_name' : "\u30b9\u30bf\u30f3\u30c9\u30de\u30a4\u30d2\u30fc\u30ed\u30fc\u30ba",
        'Package_name' : "com.colyinc.standMyHeroes",
        'version' : "4.13.6",
        'minimumOsVersion' : "10.0",
    },
    'com.fibermode.mode.android' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "Mode: Buy, Earn & Grow Bitcoin",
        'Package_name' : "com.fibermode.mode.android",
        'installs' : "50,000+",
        'version' : "2.4.5",
    },
    'tv.sensical.android' :
    {
        'category' : "Entertainment",
        'updated' : "31 May 2022",
        'App_name' : "Sensical",
        'Package_name' : "tv.sensical.android",
        'installs' : "50,000+",
        'version' : "1.0.63",
    },
    'com.funplus.kingofavalon' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "King of Avalon: Dominion",
        'Package_name' : "com.funplus.kingofavalon",
        'installs' : "50,000,000+",
        'version' : "13.7.0",
    },
    'com.kingsgroup.sos.vn' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "State of Survival - Funtap",
        'Package_name' : "com.kingsgroup.sos.vn",
        'installs' : "1,000,000+",
        'version' : "1.16.10",
    },
    'com.zebrainy.skazbuka' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Zebrainy - ABC kids games",
        'Package_name' : "com.zebrainy.skazbuka",
        'installs' : "1,000,000+",
        'version' : "8.4.0",
    },
    'kr.co.captv.pooqV2' :
    {
        'category' : "Entertainment",
        'updated' : "15 Jun 2022",
        'App_name' : "wavve(\uc6e8\uc774\ube0c) - \uc7ac\ubbf8\uc758 \ud30c\ub3c4\ub97c \ud0c0\ub2e4!",
        'Package_name' : "kr.co.captv.pooqV2",
        'installs' : "10,000,000+",
        'version' : "5.6.30",
    },
    'com.natris.Franchise' :
    {
        'category' : "Games",
        'updated' : "22 Jul 2021",
        'App_name' : "\uc544\uc774\ub7ec\ube0c\ucee4\ud53cN",
        'Package_name' : "com.natris.Franchise",
        'installs' : "100,000+",
        'version' : "1.2.7",
    },
    'kr.projectvanilla.production' :
    {
        'category' : "Finance",
        'updated' : "28 Feb 2022",
        'App_name' : "\ubc14\ub2d0\ub77c - \uc8fc\uc2dd, \ub2e8\ub9db\uc5d0 \ub208\ub728\ub2e4",
        'Package_name' : "kr.projectvanilla.production",
        'installs' : "50,000+",
        'version' : "1.1.5",
    },
    'in.playsimple.tripcross' :
    {
        'category' : "Games",
        'updated' : "19 May 2022",
        'App_name' : "Crossword Jam",
        'Package_name' : "in.playsimple.tripcross",
        'installs' : "5,000,000+",
        'version' : "1.396.0",
    },
    'com.d8aspring.panelnow' :
    {
        'category' : "Lifestyle",
        'updated' : "20 May 2022",
        'App_name' : "\ud328\ub110\ub098\uc6b0 - \uc124\ubb38\uc870\uc0ac\ub85c \ub3c8\ubc84\ub294 \uc571",
        'Package_name' : "com.d8aspring.panelnow",
        'installs' : "100,000+",
        'version' : "2.9.0",
    },
    'tv.hooq.android' :
    {
        'category' : "Entertainment",
        'updated' : "27 Apr 2020",
        'App_name' : "HOOQ - Watch Movies, TV Shows, Live Channels, News",
        'Package_name' : "tv.hooq.android",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.stepler' :
    {
        'category' : "Health and Fitness",
        'updated' : "18 Apr 2022",
        'App_name' : "Stepler",
        'Package_name' : "com.stepler",
        'installs' : "1,000,000+",
        'version' : "1.7.9",
    },
    'com.fliper.app' :
    {
        'category' : "Finance",
        'updated' : "31 May 2022",
        'App_name' : "IM+: finan\u00e7as e investimentos",
        'Package_name' : "com.fliper.app",
        'installs' : "500,000+",
        'version' : "5.2.0",
    },
    'com.celcom.yoodo' :
    {
        'category' : "Tools",
        'updated' : "26 Apr 2022",
        'App_name' : "Yoodo",
        'Package_name' : "com.celcom.yoodo",
        'installs' : "500,000+",
        'version' : "4.0.5",
    },
    'com.jztx.mmorpg.unlock.kr' :
    {
        'category' : "Games",
        'updated' : "07 Feb 2020",
        'App_name' : "\uc9c4\uc815\uff1a\uc120\uc5f0\uacfc \ud568\uaed8",
        'Package_name' : "com.jztx.mmorpg.unlock.kr",
        'installs' : "100,000+",
        'version' : "1.0.6",
    },
    'com.playrix.homescapes' :
    {
        'category' : "Games",
        'updated' : "04 Jun 2022",
        'App_name' : "Homescapes",
        'Package_name' : "com.playrix.homescapes",
        'installs' : "100,000,000+",
        'version' : "5.4.3",
    },
    '1264947230' :
    {
        'category' : "Games",
        'updated' : "2022-05-20T08:08:36Z",
        'App_name' : "LINE \u6ce2\u5154\u5c0f\u93ae",
        'Package_name' : "com.linecorp.LGPKV",
        'version' : "5.1.0",
        'minimumOsVersion' : "10.0",
    },
    'com.ulugame.cmlkr.google' :
    {
        'category' : "Games",
        'updated' : "12 Jan 2021",
        'App_name' : "\uc880\ube44\uc2a4\ud31f:\ubbf8\ub140\uc640 \uc880\ube44",
        'Package_name' : "com.ulugame.cmlkr.google",
        'installs' : "50,000+",
        'version' : "1.1.1",
    },
    'com.zzkko' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "SHEIN: SUMMER VACAY",
        'Package_name' : "com.zzkko",
        'installs' : "100,000,000+",
        'version' : "8.2.4",
    },
    'com.branch_international.branch.branch_demo_android' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Branch Personal Loan App",
        'Package_name' : "com.branch_international.branch.branch_demo_android",
        'installs' : "10,000,000+",
        'version' : "4.31.1",
    },
    'com.zynga.pottermatch' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Harry Potter: Puzzles & Spells",
        'Package_name' : "com.zynga.pottermatch",
        'installs' : "10,000,000+",
        'version' : "47.0.847",
    },
    'com.Plus500' :
    {
        'category' : "Finance",
        'updated' : "06 Jun 2022",
        'App_name' : "Plus500 Trading",
        'Package_name' : "com.Plus500",
        'installs' : "10,000,000+",
        'version' : "14.3.0",
    },
    'com.merqueo' :
    {
        'category' : "Shopping",
        'updated' : "09 Jun 2022",
        'App_name' : "Merqueo: Mercado a domicilio",
        'Package_name' : "com.merqueo",
        'installs' : "1,000,000+",
        'version' : "3.9.1",
    },
    '1547937067' :
    {
        'category' : "Games",
        'updated' : "2022-05-06T05:01:15Z",
        'App_name' : "\ub77c\uadf8\ub098\ub85c\ud06c: \ub77c\ube44\ub9b0\uc2a4",
        'Package_name' : "com.gravity.gtlor.ios",
        'version' : "193 (45.2425.0)",
        'minimumOsVersion' : "12.0",
    },
    'com.julofinance.juloapp' :
    {
        'category' : "Finance",
        'updated' : "27 May 2022",
        'App_name' : "JULO Kredit Digital & Pinjaman",
        'Package_name' : "com.julofinance.juloapp",
        'installs' : "5,000,000+",
        'version' : "6.5.1",
    },
    'com.square_enix.android_googleplay.dqwalkj' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "\u30c9\u30e9\u30b4\u30f3\u30af\u30a8\u30b9\u30c8\u30a6\u30a9\u30fc\u30af",
        'Package_name' : "com.square_enix.android_googleplay.dqwalkj",
        'installs' : "1,000,000+",
        'version' : "3.15.0",
    },
    'co.thepom.mobile' :
    {
        'category' : "Social and Communication",
        'updated' : "06 Jun 2022",
        'App_name' : "POM - MUSIC DATING",
        'Package_name' : "co.thepom.mobile",
        'installs' : "50,000+",
        'version' : "1.6.2",
    },
    'com.tmon' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "TMON(ticket monster)",
        'Package_name' : "com.tmon",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.clapp.jobs' :
    {
        'category' : "Finance",
        'updated' : "02 Feb 2022",
        'App_name' : "CornerJob - Job offers, Recruitment, Job Search",
        'Package_name' : "com.clapp.jobs",
        'installs' : "5,000,000+",
        'version' : "22.2.0",
    },
    'com.nexon.v4kr' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "V4",
        'Package_name' : "com.nexon.v4kr",
        'installs' : "1,000,000+",
        'version' : "1.36.443837",
    },
    'com.pubg.imobile' :
    {
        'category' : "Games",
        'updated' : "09 May 2022",
        'App_name' : "Battlegrounds Mobile India",
        'Package_name' : "com.pubg.imobile",
        'installs' : "50,000,000+",
        'version' : "2.0.0",
    },
    'com.breakingnewsbrief.app' :
    {
        'category' : "Education and Books",
        'updated' : "28 May 2022",
        'App_name' : "Breaking News Brief: US News",
        'Package_name' : "com.breakingnewsbrief.app",
        'installs' : "1,000,000+",
        'version' : "2.02.0",
    },
    'us.current.android' :
    {
        'category' : "Lifestyle",
        'updated' : "16 Jun 2022",
        'App_name' : "Make Money & Earn Cash Rewards",
        'Package_name' : "us.current.android",
        'installs' : "10,000,000+",
        'version' : "1.142.1",
    },
    '1523597275' :
    {
        'category' : "Games",
        'updated' : "2021-11-25T03:02:57Z",
        'App_name' : "Age of Chaos: Legends",
        'Package_name' : "com.vqw.aehd",
        'version' : "0.1.67",
        'minimumOsVersion' : "10.0",
    },
    'net.alexplay.nuclearempire' :
    {
        'category' : "Games",
        'updated' : "22 Apr 2022",
        'App_name' : "Nuclear Tycoon: Idle Simulator",
        'Package_name' : "net.alexplay.nuclearempire",
        'installs' : "100,000+",
        'version' : "0.2.5",
    },
    'askiss.game.nouenkonkatsu' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "\u8fb2\u5712\u5a5a\u6d3b\u3000-\u30d0\u30fc\u30c1\u30e3\u30eb\u5a5a\u6d3b|\u304d\u305b\u304b\u3048&\u8fb2\u5712-",
        'Package_name' : "askiss.game.nouenkonkatsu",
        'installs' : "500,000+",
        'version' : "4.2.1",
    },
    'com.linecorp.LGGRTW' :
    {
        'category' : "Games",
        'updated' : "28 Mar 2022",
        'App_name' : "LINE \u65c5\u904a\u5927\u4ea8",
        'Package_name' : "com.linecorp.LGGRTW",
        'installs' : "5,000,000+",
        'version' : "3.6.2",
    },
    'com.luizalabs.mlapp' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "Magalu: Black App",
        'Package_name' : "com.luizalabs.mlapp",
        'installs' : "50,000,000+",
        'version' : "3.71.0",
    },
    'com.socialgood_foundation.sg_app_android' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "SocialGood App:Crypto CashBack",
        'Package_name' : "com.socialgood_foundation.sg_app_android",
        'installs' : "1,000,000+",
        'version' : "1.7.1",
    },
    'com.hammerent.inuyasha' :
    {
        'category' : "Games",
        'updated' : "04 Feb 2021",
        'App_name' : "\u72ac\u591c\u53c9-\u3088\u307f\u304c\u3048\u308b\u7269\u8a9e-",
        'Package_name' : "com.hammerent.inuyasha",
        'installs' : "50,000+",
        'version' : "1.0.0",
    },
    'com.jianyqy.gptw' :
    {
        'category' : "Games",
        'updated' : "06 Sep 2021",
        'App_name' : "\u7375\u6bba\u5973\u795e",
        'Package_name' : "com.jianyqy.gptw",
        'installs' : "100,000+",
        'version' : "6.5.17",
    },
    'com.theplatforms.drbooking' :
    {
        'category' : "Health and Fitness",
        'updated' : "19 May 2022",
        'App_name' : "Dr.Booking",
        'Package_name' : "com.theplatforms.drbooking",
        'installs' : "1,000+",
        'version' : "1.7",
    },
    'com.scbabacus.l2020' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "MoneyThunder: Legal lending",
        'Package_name' : "com.scbabacus.l2020",
        'installs' : "1,000,000+",
        'version' : "2.04.20",
    },
    'kr.co.emart.emartmall' :
    {
        'category' : "Shopping",
        'updated' : "02 Jun 2022",
        'App_name' : "\uc774\ub9c8\ud2b8\ubab0 \u2013 emart mall",
        'Package_name' : "kr.co.emart.emartmall",
        'installs' : "5,000,000+",
        'version' : "3.1.8",
    },
    'com.vincellstudios.hiddenescapelosttemple' :
    {
        'category' : "Games",
        'updated' : "21 Apr 2022",
        'App_name' : "Escape Games - Lost Temple",
        'Package_name' : "com.vincellstudios.hiddenescapelosttemple",
        'installs' : "1,000,000+",
        'version' : "2.4.3",
    },
    'ru.zdravcity.app' :
    {
        'category' : "Health and Fitness",
        'updated' : "29 Apr 2022",
        'App_name' : "\u0417\u0434\u0440\u0430\u0432\u0441\u0438\u0442\u0438 \u2013 \u0410\u043f\u0442\u0435\u043a\u0438 \u0441 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u043e\u0439",
        'Package_name' : "ru.zdravcity.app",
        'installs' : "1,000,000+",
        'version' : "2.8",
    },
    'ae.supermart.app' :
    {
        'category' : "Food & Drink",
        'updated' : "03 Feb 2022",
        'App_name' : "Supermart.ae -Online Grocery Dubai",
        'Package_name' : "ae.supermart.app",
        'installs' : "5,000+",
        'version' : "2.8.6",
    },
    'tw.com.iwplay.pwm' :
    {
        'category' : "Games",
        'updated' : "07 Apr 2022",
        'App_name' : "\u5b8c\u7f8e\u4e16\u754cM",
        'Package_name' : "tw.com.iwplay.pwm",
        'installs' : "500,000+",
        'version' : "1.563.2",
    },
    'com.duellogames.Zero21' :
    {
        'category' : "Games",
        'updated' : "03 Mar 2022",
        'App_name' : "Zero21 Solitaire",
        'Package_name' : "com.duellogames.Zero21",
        'installs' : "5,000,000+",
        'version' : "2.17",
    },
    'jp.co.hangame.sjchocotto' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "\u30c1\u30e7\u30b3\u30c3\u30c8\u30e9\u30f3\u30c9SP",
        'Package_name' : "jp.co.hangame.sjchocotto",
        'installs' : "100,000+",
        'version' : "8.1.2",
    },
    'com.kwt.hardeesburger.fastfood' :
    {
        'category' : "Food & Drink",
        'updated' : "04 Feb 2022",
        'App_name' : "Hardee's Kuwait - Burger & Sandwich Meals!",
        'Package_name' : "com.kwt.hardeesburger.fastfood",
        'installs' : "100,000+",
        'version' : "5.7.3",
    },
    'mx.justo.android' :
    {
        'category' : "Shopping",
        'updated' : "03 Jun 2022",
        'App_name' : "J\u00fcsto: Supermercado en l\u00ednea",
        'Package_name' : "mx.justo.android",
        'installs' : "500,000+",
        'version' : "7.9.1",
    },
    'com.hypd.store' :
    {
        'category' : "Shopping",
        'updated' : "02 May 2022",
        'App_name' : "Hypd Store-Create share & earn",
        'Package_name' : "com.hypd.store",
        'installs' : "100,000+",
        'version' : "11.4.1",
    },
    '1273195340' :
    {
        'category' : "Shopping",
        'updated' : "2022-05-25T16:01:32Z",
        'App_name' : "Casino Max - promos & fid\u00e9lit\u00e9",
        'Package_name' : "com.casino.cmyapp",
        'version' : "13.7.0",
        'minimumOsVersion' : "12.0",
    },
    'com.appcpi.monsterworld' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Park of Monster",
        'Package_name' : "com.appcpi.monsterworld",
        'installs' : "1,000,000+",
        'version' : "2.6.8",
    },
    'com.ykb.android' :
    {
        'category' : "Finance",
        'updated' : "29 May 2022",
        'App_name' : "Yap\u0131 Kredi Mobile",
        'Package_name' : "com.ykb.android",
        'installs' : "10,000,000+",
        'version' : "3.9.14",
    },
    'com.sstl.pxr.google' :
    {
        'category' : "Games",
        'updated' : "07 Dec 2021",
        'App_name' : "\uc74c\uc591\ubb38",
        'Package_name' : "com.sstl.pxr.google",
        'installs' : "50,000+",
        'version' : "1.0.2",
    },
    'com.andromedagames.noblesse' :
    {
        'category' : "Games",
        'updated' : "15 Mar 2022",
        'App_name' : "\ub178\ube14\ub808\uc2a4 : \uc81c\ub85c with NAVER WEBTOON",
        'Package_name' : "com.andromedagames.noblesse",
        'installs' : "100,000+",
        'version' : "1.59.1",
    },
    '523052831' :
    {
        'category' : "Shopping",
        'updated' : "2022-05-30T03:08:09Z",
        'App_name' : "ezbuy - Online Shopping",
        'Package_name' : "com.65daigou",
        'version' : "9.40.0",
        'minimumOsVersion' : "11.0",
    },
    'com.firstmeetgame.txsea.gp' :
    {
        'category' : "Games",
        'updated' : "10 Feb 2022",
        'App_name' : "Starsurge\u00a0Rising",
        'Package_name' : "com.firstmeetgame.txsea.gp",
        'installs' : "50,000+",
        'version' : "4.0.0",
    },
    'co.mona.android' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "Crypto.com - Buy BTC, ETH",
        'Package_name' : "co.mona.android",
        'installs' : "10,000,000+",
        'version' : "3.138.0",
    },
    'com.nexon.bluearchive' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "Blue Archive",
        'Package_name' : "com.nexon.bluearchive",
        'installs' : "1,000,000+",
        'version' : "1.39.146794",
    },
    '1332637659' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-05-13T04:25:14Z",
        'App_name' : "aeru.party",
        'Package_name' : "jp.co.nlcc.AeruParty",
        'version' : "1.4.2",
        'minimumOsVersion' : "11.3",
    },
    'com.ulugame.goldenkr.google' :
    {
        'category' : "Games",
        'updated' : "17 May 2022",
        'App_name' : "\ub9ac\uce58\ub9ac\uce58",
        'Package_name' : "com.ulugame.goldenkr.google",
        'installs' : "1,000,000+",
        'version' : "3.203",
    },
    'in.product.salary' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Employee Attendance & Payroll",
        'Package_name' : "in.product.salary",
        'installs' : "500,000+",
        'version' : "4.10",
    },
    '595291824' :
    {
        'category' : "Games",
        'updated' : "2022-06-09T02:00:12Z",
        'App_name' : "\u3077\u3088\u3077\u3088!!\u30af\u30a8\u30b9\u30c8 -\u7c21\u5358\u64cd\u4f5c\u3067\u5927\u9023\u9396\u3002\u723d\u5feb \u30d1\u30ba\u30eb\uff01",
        'Package_name' : "com.sega.PuyoQuest",
        'version' : "10.3.2",
        'minimumOsVersion' : "10.0",
    },
    'com.thesilverlabs.rumbl' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "Rizzle - Short Videos",
        'Package_name' : "com.thesilverlabs.rumbl",
        'installs' : "50,000,000+",
        'version' : "9.6.9",
    },
    'com.play.rosea' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "Ragnarok X: Next Generation",
        'Package_name' : "com.play.rosea",
        'installs' : "10,000,000+",
        'version' : "2.3.0.220602.7",
    },
    '1463171656' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-20T06:39:32Z",
        'App_name' : "Roraa-don\u2019t download app",
        'Package_name' : "com.roraa.app",
        'version' : "2.4",
        'minimumOsVersion' : "12.1",
    },
    '1096177630' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-16T15:38:07Z",
        'App_name' : "Burger King Arabia",
        'Package_name' : "com.skylinedynamics.bk",
        'version' : "6.0.5",
        'minimumOsVersion' : "13.0",
    },
    'com.bnb.paynearby' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "PayNearby - Aadhaar ATM, DMT",
        'Package_name' : "com.bnb.paynearby",
        'installs' : "5,000,000+",
        'version' : "4.8.0",
    },
    'com.YoStarEN.AzurLane' :
    {
        'category' : "Games",
        'updated' : "07 Apr 2022",
        'App_name' : "Azur Lane",
        'Package_name' : "com.YoStarEN.AzurLane",
        'installs' : "5,000,000+",
        'version' : "6.1.2",
    },
    'com.egls.ytkr.gp' :
    {
        'category' : "Games",
        'updated' : "14 Oct 2020",
        'App_name' : "\ud63c\ub839\uc0ac",
        'Package_name' : "com.egls.ytkr.gp",
        'installs' : "50,000+",
        'version' : "6.0",
    },
    'com.mpokket.app' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "mPokket: Instant Loan App",
        'Package_name' : "com.mpokket.app",
        'installs' : "10,000,000+",
        'version' : "3.6.3",
    },
    'com.nhnpixelcube.mummy' :
    {
        'category' : "Games",
        'updated' : "27 Aug 2020",
        'App_name' : "\u30df\u30a4\u30e9\u306e\u98fc\u3044\u65b9 \u30d1\u30ba\u30eb\u3067\u80b2\u3066\u308b\u4e0d\u601d\u8b70\u306a\u751f\u304d\u7269",
        'Package_name' : "com.nhnpixelcube.mummy",
        'installs' : "100,000+",
        'version' : "1.17.62",
    },
    'com.bankofhodlers.mobile' :
    {
        'category' : "Finance",
        'updated' : "06 May 2022",
        'App_name' : "Vauld - Earn, Lend with Crypto",
        'Package_name' : "com.bankofhodlers.mobile",
        'installs' : "500,000+",
        'version' : "2.6.8",
    },
    'com.userjoy.sinjp' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "sin \u4e03\u3064\u306e\u5927\u7f6a X-TASY",
        'Package_name' : "com.userjoy.sinjp",
        'installs' : "100,000+",
        'version' : "1.0.7",
    },
    'tw.com.nuphoto.nuphoto' :
    {
        'category' : "Entertainment",
        'updated' : "13 Jun 2022",
        'App_name' : "nuPhoto\u62cd\u7acb\u6d17-\u6c96\u6d17\u7167\u7247 \u684c\u66c6/\u7121\u6846\u756b/\u76f8\u7247\u66f8",
        'Package_name' : "tw.com.nuphoto.nuphoto",
        'installs' : "500,000+",
        'version' : "3.0.658",
    },
    'com.bluestonesoft.soularkglobal' :
    {
        'category' : "Games",
        'updated' : "18 Jan 2021",
        'App_name' : "Soul Ark: New World",
        'Package_name' : "com.bluestonesoft.soularkglobal",
        'installs' : "500,000+",
        'version' : "4.8",
    },
    'dano.mydano' :
    {
        'category' : "Health and Fitness",
        'updated' : "13 Jun 2022",
        'App_name' : "\ub2e4\ub178 - \uc9c0\uc18d \uac00\ub2a5\ud55c \ub2e4\uc774\uc5b4\ud2b8, \uc2b5\uad00\uc131\ud615 \uc571",
        'Package_name' : "dano.mydano",
        'installs' : "100,000+",
        'version' : "3.2.46",
    },
    'com.cayenne.tongkeymobile' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "\u9b25\u7403\u5152\u5f48\u5e73M",
        'Package_name' : "com.cayenne.tongkeymobile",
        'installs' : "10,000+",
        'version' : "2.0.36",
    },
    'com.discord' :
    {
        'category' : "Social and Communication",
        'updated' : "16 Jun 2022",
        'App_name' : "Discord: Talk, Chat & Hang Out",
        'Package_name' : "com.discord",
        'installs' : "100,000,000+",
        'version' : "126.15 - Stable",
    },
    'com.ulugame.faithkr.google' :
    {
        'category' : "Games",
        'updated' : "18 Apr 2022",
        'App_name' : "\uc544\ub974\uce74",
        'Package_name' : "com.ulugame.faithkr.google",
        'installs' : "1,000,000+",
        'version' : "13.0.1",
    },
    '1439290223' :
    {
        'category' : "Finance",
        'updated' : "2022-05-31T23:18:19Z",
        'App_name' : "mScanner - PDF Scanner App",
        'Package_name' : "com.madduck.documentscanner",
        'version' : "5.0.5",
        'minimumOsVersion' : "12.0",
    },
    'com.intsig.camscanner' :
    {
        'category' : "Tools",
        'updated' : "14 Jun 2022",
        'App_name' : "CamScanner - PDF Scanner App",
        'Package_name' : "com.intsig.camscanner",
        'installs' : "100,000,000+",
        'version' : "6.17.0.2205230000",
    },
    '1327925104' :
    {
        'category' : "Games",
        'updated' : "2022-05-31T14:06:02Z",
        'App_name' : "Disney Heroes: Battle Mode",
        'Package_name' : "com.perblue.disneyheroes.ios",
        'version' : "4.0.10",
        'minimumOsVersion' : "10.0",
    },
    'com.thesouledstore' :
    {
        'category' : "Shopping",
        'updated' : "07 Jun 2022",
        'App_name' : "The Souled Store: Shopping App",
        'Package_name' : "com.thesouledstore",
        'installs' : "1,000,000+",
        'version' : "2.36",
    },
    'com.gmlive.vins' :
    {
        'category' : "Entertainment",
        'updated' : "18 Oct 2021",
        'App_name' : "Azizi - Group Voice Chat",
        'Package_name' : "com.gmlive.vins",
        'installs' : "1,000,000+",
        'version' : "3.7.11",
    },
    'com.delivery.korea.client' :
    {
        'category' : "Tools",
        'updated' : "26 May 2022",
        'App_name' : "Quickers: Express Delivery App",
        'Package_name' : "com.delivery.korea.client",
        'installs' : "10,000+",
        'version' : "1.62.0",
    },
    '1531855095' :
    {
        'category' : "Shopping",
        'updated' : "2022-04-28T11:49:43Z",
        'App_name' : "FACES Beauty \u2013 \u0641\u064a\u0633\u0632",
        'Package_name' : "com.faces.iosapp",
        'version' : "3.1",
        'minimumOsVersion' : "11.0",
    },
    'mx.com.procesar.aforemovil.sura' :
    {
        'category' : "Finance",
        'updated' : "27 Feb 2021",
        'App_name' : "Afore M\u00f3vil SURA",
        'Package_name' : "mx.com.procesar.aforemovil.sura",
        'installs' : "1,000,000+",
        'version' : "3.4.2.1",
    },
    'com.wondershake.locari' :
    {
        'category' : "Education and Books",
        'updated' : "30 May 2022",
        'App_name' : "LOCARI\uff08\u30ed\u30ab\u30ea\uff09 - \u30aa\u30c8\u30ca\u5973\u5b50\u5411\u3051\u30e9\u30a4\u30d5\u30b9\u30bf\u30a4\u30eb\u60c5\u5831\u30a2\u30d7\u30ea",
        'Package_name' : "com.wondershake.locari",
        'installs' : "1,000,000+",
        'version' : "8.1.0",
    },
    'ru.auto.ara' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "\u0410\u0432\u0442\u043e.\u0440\u0443: \u043a\u0443\u043f\u0438\u0442\u044c \u0438 \u043f\u0440\u043e\u0434\u0430\u0442\u044c \u0430\u0432\u0442\u043e",
        'Package_name' : "ru.auto.ara",
        'installs' : "10,000,000+",
        'version' : "10.13.0",
    },
    '1441964924' :
    {
        'category' : "Games",
        'updated' : "2022-03-14T09:33:49Z",
        'App_name' : "Skratchy",
        'Package_name' : "com.skratchy.appsuccess",
        'version' : "2.8",
        'minimumOsVersion' : "12.0",
    },
    'com.fundoshiparade.bokukoro3jp' :
    {
        'category' : "Games",
        'updated' : "17 May 2022",
        'App_name' : "Just Kill Me 3",
        'Package_name' : "com.fundoshiparade.bokukoro3jp",
        'installs' : "1,000,000+",
        'version' : "12.0",
    },
    'com.extreme.lunam.th' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Luna M",
        'Package_name' : "com.extreme.lunam.th",
        'installs' : "100,000+",
        'version' : "1.0.621",
    },
    'com.poa.kr' :
    {
        'category' : "Games",
        'updated' : "02 Apr 2021",
        'App_name' : "\ud074\ub798\uc2dc \ubd90(Clash Boom)",
        'Package_name' : "com.poa.kr",
        'installs' : "100,000+",
        'version' : "1.7.95",
    },
    'com.agac.Mazadak' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "02 Jun 2022",
        'App_name' : "Mazadak",
        'Package_name' : "com.agac.Mazadak",
        'installs' : "10,000+",
        'version' : "1.23.2",
    },
    'com.vsemayki.app' :
    {
        'category' : "Shopping",
        'updated' : "11 May 2022",
        'App_name' : "Vsemayki: \u041e\u0434\u0435\u0436\u0434\u0430 \u0441 \u043f\u0440\u0438\u043d\u0442\u0430\u043c\u0438",
        'Package_name' : "com.vsemayki.app",
        'installs' : "5,000,000+",
        'version' : "4.37.8",
    },
    'jp.pjbox' :
    {
        'category' : "Games",
        'updated' : "31 Jan 2022",
        'App_name' : "\u306f\u3058\u3081\u306e\u4e00\u6b69 FIGHTING SOULS",
        'Package_name' : "jp.pjbox",
        'installs' : "100,000+",
        'version' : "1.0.13",
    },
    'com.ulugames.chaosglory.google' :
    {
        'category' : "Games",
        'updated' : "13 Jan 2022",
        'App_name' : "\ud2b8\ub85c\uc774",
        'Package_name' : "com.ulugames.chaosglory.google",
        'installs' : "10,000+",
        'version' : "1.0.0",
    },
    'com.goodgamestudios.ageofknights' :
    {
        'category' : "Games",
        'updated' : "13 Oct 2020",
        'App_name' : "Empire: Age of Knights - Fantasy MMO Strategy Game",
        'Package_name' : "com.goodgamestudios.ageofknights",
        'installs' : "1,000,000+",
        'version' : "2.7.8979",
    },
    'com.king.candycrushsaga' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "Candy Crush Saga",
        'Package_name' : "com.king.candycrushsaga",
        'installs' : "1,000,000,000+",
        'version' : "1.229.0.2",
    },
    'ir.divar' :
    {
        'category' : "Shopping",
        'updated' : "12 Jun 2022",
        'App_name' : "Divar",
        'Package_name' : "ir.divar",
        'installs' : "10,000,000+",
        'version' : "11.5.18",
    },
    'com.TimurBesli.Judge3D' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Judge 3D - Court Affairs",
        'Package_name' : "com.TimurBesli.Judge3D",
        'installs' : "1,000,000+",
        'version' : "1.9.2.0",
    },
    'pl.pracuj.android.jobsearcher' :
    {
        'category' : "Finance",
        'updated' : "19 May 2022",
        'App_name' : "Pracuj.pl - Jobs",
        'Package_name' : "pl.pracuj.android.jobsearcher",
        'installs' : "1,000,000+",
        'version' : "4.41.6",
    },
    'com.qjzj4399jp.google' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "\u9b54\u5263\u4f1d\u8aac",
        'Package_name' : "com.qjzj4399jp.google",
        'installs' : "500,000+",
        'version' : "1.17.1",
    },
    'com.neowiz.games.newmatgo' :
    {
        'category' : "Games",
        'updated' : "17 May 2022",
        'App_name' : "\ud53c\ub9dd \ub274\ub9de\uace0: \uace0\uc2a4\ud1b1 \ub300\ud45c \ub9de\uace0 \uac8c\uc784",
        'Package_name' : "com.neowiz.games.newmatgo",
        'installs' : "10,000,000+",
        'version' : "81.0",
    },
    'com.oynakazanapp.android' :
    {
        'category' : "Entertainment",
        'updated' : "13 Jan 2022",
        'App_name' : "Oyna Kazan - Kazand\u0131ran Bilgi Yar\u0131\u015fmas\u0131",
        'Package_name' : "com.oynakazanapp.android",
        'installs' : "1,000,000+",
        'version' : "2.3.9",
    },
    'com.joygame.xxyz.google' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "\ubbf8\ub2c8\uc6a9\uc0ac \ud0a4\uc6b0\uae30",
        'Package_name' : "com.joygame.xxyz.google",
        'installs' : "100,000+",
        'version' : "4.6.1",
    },
    'com.linecorp.LGYDS' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "\u30b8\u30e3\u30f3\u30d7\u30c1 \u30d2\u30fc\u30ed\u30fc\u30ba\u3000\u30b8\u30e3\u30f3\u30d7\u306e\u30d1\u30ba\u30ebRPG",
        'Package_name' : "com.linecorp.LGYDS",
        'installs' : "1,000,000+",
        'version' : "6.1.3",
    },
    'ca.autotrader.userapp' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "31 May 2022",
        'App_name' : "AutoTrader - Shop Car Deals",
        'Package_name' : "ca.autotrader.userapp",
        'installs' : "1,000,000+",
        'version' : "7.46.1",
    },
    'com.fastbanking' :
    {
        'category' : "Finance",
        'updated' : "18 May 2022",
        'App_name' : "Kissht: Instant Line of Credit",
        'Package_name' : "com.fastbanking",
        'installs' : "10,000,000+",
        'version' : "2.2.1",
    },
    'com.tokopedia.tkpd' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "Tokopedia",
        'Package_name' : "com.tokopedia.tkpd",
        'installs' : "100,000,000+",
        'version' : "3.178",
    },
    '1512718675' :
    {
        'category' : "Health and Fitness",
        'updated' : "2022-05-05T08:29:46Z",
        'App_name' : "HealthPass by OCBC",
        'Package_name' : "com.cxagroup.mobile.EmployeePortal.production.cxa",
        'version' : "1.0.750",
        'minimumOsVersion' : "9.0",
    },
    'com.indie.shj.google.dny' :
    {
        'category' : "Games",
        'updated' : "20 Apr 2022",
        'App_name' : "\u5c71\u6d77\u6709\u5996\u517d(\u56fd\u9645\u7248)",
        'Package_name' : "com.indie.shj.google.dny",
        'installs' : "100,000+",
        'version' : "Varies with device",
    },
    'com.toluna.webservice' :
    {
        'category' : "Social and Communication",
        'updated' : "22 May 2022",
        'App_name' : "Toluna Influencers",
        'Package_name' : "com.toluna.webservice",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.glowdayz.glowmee' :
    {
        'category' : "Social and Communication",
        'updated' : "31 May 2022",
        'App_name' : "\uae00\ub85c\uc6b0\ud53d (GLOWPICK)",
        'Package_name' : "com.glowdayz.glowmee",
        'installs' : "1,000,000+",
        'version' : "3.1.5",
    },
    'coloring.color.number.happy.paint.art.drawing.puzzle' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Tap Color Pro: Color By Number",
        'Package_name' : "coloring.color.number.happy.paint.art.drawing.puzzle",
        'installs' : "10,000,000+",
        'version' : "5.8.2",
    },
    'com.amazon.mShop.android.shopping' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Amazon Shopping",
        'Package_name' : "com.amazon.mShop.android.shopping",
        'installs' : "500,000,000+",
        'version' : "24.11.0.100",
    },
    'com.carrefoursa.ecommerce' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "CarrefourSA Online Market",
        'Package_name' : "com.carrefoursa.ecommerce",
        'installs' : "1,000,000+",
        'version' : "2.4.7",
    },
    'com.paltalk.chat.android' :
    {
        'category' : "Social and Communication",
        'updated' : "28 Apr 2022",
        'App_name' : "Paltalk: Chat with Strangers",
        'Package_name' : "com.paltalk.chat.android",
        'installs' : "5,000,000+",
        'version' : "9.5.0.1-RC",
    },
    'air.jp.boi.cryptract' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "\u5e7b\u7363\u5951\u7d04\u30af\u30ea\u30d7\u30c8\u30e9\u30af\u30c8",
        'Package_name' : "air.jp.boi.cryptract",
        'installs' : "1,000,000+",
        'version' : "5.1.3",
    },
    'com.superking.parchisi.star' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "Parchisi STAR Online",
        'Package_name' : "com.superking.parchisi.star",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    '529615515' :
    {
        'category' : "Finance",
        'updated' : "2022-06-13T20:36:50Z",
        'App_name' : "Xoom Money Transfer",
        'Package_name' : "com.xoom.app",
        'version' : "9.5",
        'minimumOsVersion' : "13.0",
    },
    'com.asiainno.uplive' :
    {
        'category' : "Social and Communication",
        'updated' : "08 Jun 2022",
        'App_name' : "Uplive-Live Stream, Go Live",
        'Package_name' : "com.asiainno.uplive",
        'installs' : "50,000,000+",
        'version' : "8.5.0",
    },
    'com.carshering' :
    {
        'category' : "Utilities",
        'updated' : "24 May 2022",
        'App_name' : "Delimobil. Your carsharing",
        'Package_name' : "com.carshering",
        'installs' : "5,000,000+",
        'version' : "8.0.4, build 834295147",
    },
    'com.fanduel.android.self' :
    {
        'category' : "Games",
        'updated' : "11 Jun 2022",
        'App_name' : "FanDuel Fantasy Sports",
        'Package_name' : "com.fanduel.android.self",
        'installs' : "1,000,000+",
        'version' : "3.47",
    },
    'com.beecash.app' :
    {
        'category' : "Finance",
        'updated' : "28 Feb 2022",
        'App_name' : "BukuKas - Catatan Kas Digital",
        'Package_name' : "com.beecash.app",
        'installs' : "5,000,000+",
        'version' : "0.50.1",
    },
    'com.ttby.android' :
    {
        'category' : "Games",
        'updated' : "18 Feb 2022",
        'App_name' : "\u0e40\u0e01\u0e21\u0e15\u0e01\u0e1b\u0e25\u0e32 - Didi",
        'Package_name' : "com.ttby.android",
        'installs' : "100,000+",
        'version' : "1.1.54",
    },
    '1158877342' :
    {
        'category' : "Tools",
        'updated' : "2022-06-08T10:55:14Z",
        'App_name' : "Grammarly - Keyboard & Editor",
        'Package_name' : "com.grammarly.keyboard",
        'version' : "2.2.0",
        'minimumOsVersion' : "12.2",
    },
    'com.payfazz.android' :
    {
        'category' : "Finance",
        'updated' : "17 Jun 2022",
        'App_name' : "PAYFAZZ Agen: Pulsa & Transfer",
        'Package_name' : "com.payfazz.android",
        'installs' : "5,000,000+",
        'version' : "3.8.9",
    },
    'com.tap4fun.brutalage_test' :
    {
        'category' : "Games",
        'updated' : "06 Jun 2022",
        'App_name' : "Brutal Age: Horde Invasion",
        'Package_name' : "com.tap4fun.brutalage_test",
        'installs' : "10,000,000+",
        'version' : "1.0.80",
    },
    'com.ozdilek.ozdilekteyim' :
    {
        'category' : "Shopping",
        'updated' : "05 Apr 2022",
        'App_name' : "\u00d6zdilekteyim:Online Al\u0131\u015fveri\u015f",
        'Package_name' : "com.ozdilek.ozdilekteyim",
        'installs' : "100,000+",
        'version' : "1.1.6",
    },
    'com.liveperson.kasamba.android' :
    {
        'category' : "Lifestyle",
        'updated' : "18 May 2022",
        'App_name' : "Kasamba Live Psychic Reading",
        'Package_name' : "com.liveperson.kasamba.android",
        'installs' : "500,000+",
        'version' : "Varies with device",
    },
    'ru.mobstudio.andgalaxy' :
    {
        'category' : "Social and Communication",
        'updated' : "31 May 2022",
        'App_name' : "Galaxy - Chat Rooms & Dating",
        'Package_name' : "ru.mobstudio.andgalaxy",
        'installs' : "10,000,000+",
        'version' : "9.5.20",
    },
    'com.neowizgames.game.browndust.srpg.global' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "Brave Nine - Tactical RPG",
        'Package_name' : "com.neowizgames.game.browndust.srpg.global",
        'installs' : "1,000,000+",
        'version' : "2.20.5",
    },
    'pbsc.cyclefinder.tembici' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "08 Jun 2022",
        'App_name' : "Bike Ita\u00fa: Bicycle-Sharing",
        'Package_name' : "pbsc.cyclefinder.tembici",
        'installs' : "1,000,000+",
        'version' : "9.1.13",
    },
    '991673877' :
    {
        'category' : "Finance",
        'updated' : "2022-06-03T01:42:06Z",
        'App_name' : "Maya-Your all-in-one money app",
        'Package_name' : "com.paymaya.ios",
        'version' : "2.41.0",
        'minimumOsVersion' : "11.0",
    },
    '926272969' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-13T08:20:45Z",
        'App_name' : "Catawiki - Aste Online",
        'Package_name' : "nl.catawiki.bidder-app",
        'version' : "4.45",
        'minimumOsVersion' : "14.0",
    },
    'com.rappelz.mmorpg.mobile' :
    {
        'category' : "Games",
        'updated' : "25 Feb 2022",
        'App_name' : "Rappelz Online: Fantasy MMORPG",
        'Package_name' : "com.rappelz.mmorpg.mobile",
        'installs' : "50,000+",
        'version' : "1.8400.1100",
    },
    'co.xare.tribe' :
    {
        'category' : "Finance",
        'updated' : "25 May 2022",
        'App_name' : "Xare Card - Share. Pay. Shop",
        'Package_name' : "co.xare.tribe",
        'installs' : "1,000,000+",
        'version' : "1.4.0",
    },
    'com.whaleapp.huntermaster' :
    {
        'category' : "Games",
        'updated' : "16 Oct 2020",
        'App_name' : "Hunter: Master of Arrows",
        'Package_name' : "com.whaleapp.huntermaster",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.UCMobile.intl' :
    {
        'category' : "Social and Communication",
        'updated' : "20 Oct 2021",
        'App_name' : "UC Browser-Safe, Fast, Private",
        'Package_name' : "com.UCMobile.intl",
        'installs' : "1,000,000,000+",
        'version' : "Varies with device",
    },
    '1511058649' :
    {
        'category' : "Entertainment",
        'updated' : "2022-05-20T07:35:48Z",
        'App_name' : "Musixen - Online Canl\u0131 M\u00fczik",
        'Package_name' : "com.musetechs.musixen",
        'version' : "2.0.12",
        'minimumOsVersion' : "12.0",
    },
    'com.pratilipi.mobile.android' :
    {
        'category' : "Education and Books",
        'updated' : "07 Jun 2022",
        'App_name' : "FireNovel - World of Novels",
        'Package_name' : "com.pratilipi.mobile.android",
        'installs' : "10,000,000+",
        'version' : "6.19.1",
    },
    '777645417' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-14T19:23:40Z",
        'App_name' : "Lamoda: \u043e\u0434\u0435\u0436\u0434\u0430 \u0438 \u043e\u0431\u0443\u0432\u044c \u043e\u043d\u043b\u0430\u0439\u043d",
        'Package_name' : "com.lamoda.ios",
        'version' : "4.9",
        'minimumOsVersion' : "12.0",
    },
    'com.kprpg.e12' :
    {
        'category' : "Lifestyle",
        'updated' : "01 Sep 2020",
        'App_name' : "\u304a\u5c0f\u9063\u3044\u7a3c\u304e\u30b2\u30fc\u30e0\uff01\u304a\u91d1\u7a3c\u304e\u30b2\u30fc\u30e0\uff01\u30dd\u30a4\u6d3b\u30b2\u30fc\u30e0\uff01\u30c9\u30e9\u30b1\u30f3",
        'Package_name' : "com.kprpg.e12",
        'installs' : "10,000+",
        'version' : "5.7.7",
    },
    'com.eightelements.godsflame.aurora7' :
    {
        'category' : "Games",
        'updated' : "24 May 2021",
        'App_name' : "Aurora 7 Indonesia",
        'Package_name' : "com.eightelements.godsflame.aurora7",
        'installs' : "10,000+",
        'version' : "1.0.5",
    },
    'com.appfreshcc.ccchat' :
    {
        'category' : "Lifestyle",
        'updated' : "05 Feb 2020",
        'App_name' : "CC\u30c1\u30e3\u30c3\u30c8",
        'Package_name' : "com.appfreshcc.ccchat",
        'installs' : "100,000+",
        'version' : "1.2.0.0",
    },
    'com.NoonDate' :
    {
        'category' : "Social and Communication",
        'updated' : "10 Jun 2022",
        'App_name' : "NoonDate: Ways to Make Friends",
        'Package_name' : "com.NoonDate",
        'installs' : "1,000,000+",
        'version' : "6.7.5",
    },
    'com.fansipan.customer' :
    {
        'category' : "Food & Drink",
        'updated' : "04 Jun 2022",
        'App_name' : "Rino - Fast grocery delivery",
        'Package_name' : "com.fansipan.customer",
        'installs' : "10,000+",
        'version' : "1.3.12",
    },
    'kr.co.april7.eundabang.google' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "\uc740\ud558\uc218\ub2e4\ubc29 - \uc18c\uac1c\ud305,\ub370\uc774\ud305\uc571,\ubbf8\ud63c,\ub3cc\uc2f1,\ub9cc\ub0a8, \ub3d9\ud638\ud68c",
        'Package_name' : "kr.co.april7.eundabang.google",
        'installs' : "500,000+",
        'version' : "2.2.39",
    },
    'com.gmt.tamquoc.lienthanhchien' :
    {
        'category' : "Games",
        'updated' : "17 May 2022",
        'App_name' : "Tam Qu\u1ed1c Li\u1ec7t Truy\u1ec7n-T\u1eb7ngVip10",
        'Package_name' : "com.gmt.tamquoc.lienthanhchien",
        'installs' : "100,000+",
        'version' : "4.13",
    },
    '1086466469' :
    {
        'category' : "Shopping",
        'updated' : "2022-05-23T03:09:41Z",
        'App_name' : "\ud3ab\ud504\ub80c\uc988 - \ubc18\ub824\ub3d9\ubb3c 1\ub4f1 \uc1fc\ud551\ubab0",
        'Package_name' : "com.reverseauction.petfriends",
        'version' : "2.6.9",
        'minimumOsVersion' : "11.0",
    },
    'com.it888.sport.quiz' :
    {
        'category' : "Games",
        'updated' : "16 May 2022",
        'App_name' : "888sportquiz | Pronostica e vinci",
        'Package_name' : "com.it888.sport.quiz",
        'installs' : "10,000+",
        'version' : "1.15.7",
    },
    'com.dena.mirrativ' :
    {
        'category' : "Social and Communication",
        'updated' : "16 Jun 2022",
        'App_name' : "Mirrativ: Live-streaming App",
        'Package_name' : "com.dena.mirrativ",
        'installs' : "1,000,000+",
        'version' : "9.67.1",
    },
    '1506229470' :
    {
        'category' : "Games",
        'updated' : "2022-06-14T07:01:45Z",
        'App_name' : "FanDuel Casino - Real Money",
        'Package_name' : "com.fanduel.casino",
        'version' : "2.30",
        'minimumOsVersion' : "11.0",
    },
    'in.medibuddy' :
    {
        'category' : "Health and Fitness",
        'updated' : "14 Jun 2022",
        'App_name' : "MediBuddy-Online Dr, Lab, Meds",
        'Package_name' : "in.medibuddy",
        'installs' : "1,000,000+",
        'version' : "3.2.07",
    },
    'com.netmarble.survival' :
    {
        'category' : "Games",
        'updated' : "24 May 2022",
        'App_name' : "A3: \uc2a4\ud2f8\uc5bc\ub77c\uc774\ube0c",
        'Package_name' : "com.netmarble.survival",
        'installs' : "1,000,000+",
        'version' : "1.19.41",
    },
    'com.endowus.mobileapp' :
    {
        'category' : "Finance",
        'updated' : "03 Jun 2022",
        'App_name' : "Endowus: Invest Cash, CPF, SRS",
        'Package_name' : "com.endowus.mobileapp",
        'installs' : "50,000+",
        'version' : "1.7.9",
    },
    'com.viu.phone' :
    {
        'category' : "Entertainment",
        'updated' : "15 Jun 2022",
        'App_name' : "Viu : Korean & Asian content",
        'Package_name' : "com.viu.phone",
        'installs' : "10,000,000+",
        'version' : "1.54.0",
    },
    'com.igs.goldentigerslots' :
    {
        'category' : "Casino",
        'updated' : "12 Jun 2022",
        'App_name' : "Golden Tiger Slots",
        'Package_name' : "com.igs.goldentigerslots",
        'installs' : "1,000,000+",
        'version' : "3.0.0",
    },
    'com.astropaycard.android' :
    {
        'category' : "Finance",
        'updated' : "17 Jun 2022",
        'App_name' : "AstroPay - Online Money Wallet",
        'Package_name' : "com.astropaycard.android",
        'installs' : "5,000,000+",
        'version' : "1.102-prod-release",
    },
    '586234461' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-07T10:48:37Z",
        'App_name' : "SPAR SAMMEN",
        'Package_name' : "com.spardk.spar",
        'version' : "7.3.0",
        'minimumOsVersion' : "12.0",
    },
    'com.bradesco' :
    {
        'category' : "Finance",
        'updated' : "04 Jun 2022",
        'App_name' : "Bradesco: Conta, Cart\u00e3o e Pix!",
        'Package_name' : "com.bradesco",
        'installs' : "50,000,000+",
        'version' : "4.28.3",
    },
    'com.eurekoservis.doost' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "08 Apr 2022",
        'App_name' : "Doost - Yol Yard\u0131m",
        'Package_name' : "com.eurekoservis.doost",
        'installs' : "100,000+",
        'version' : "4.0",
    },
    '1125346704' :
    {
        'category' : "Games",
        'updated' : "2022-06-13T03:06:32Z",
        'App_name' : "\u738b\u5b50\u69d8\u306e\u30d7\u30ed\u30dd\u30fc\u30ba Eternal Kiss",
        'Package_name' : "com.voltage.j.ouji2",
        'version' : "7.3",
        'minimumOsVersion' : "10.0",
    },
    'com.thinkreals.couponmoa' :
    {
        'category' : "Shopping",
        'updated' : "09 May 2022",
        'App_name' : "\ucfe0\ud3f0\ubaa8\uc544",
        'Package_name' : "com.thinkreals.couponmoa",
        'installs' : "1,000,000+",
        'version' : "6.33",
    },
    'jp.co.crooz.shoplistapp' :
    {
        'category' : "Shopping",
        'updated' : "11 Jun 2022",
        'App_name' : "\u30d5\u30a1\u30c3\u30b7\u30e7\u30f3\u901a\u8ca9\u30b7\u30e7\u30c3\u30d4\u30f3\u30b0SHOPLIST-\u30b7\u30e7\u30c3\u30d7\u30ea\u30b9\u30c8",
        'Package_name' : "jp.co.crooz.shoplistapp",
        'installs' : "1,000,000+",
        'version' : "22.6.14",
    },
    'com.kakaogames.alicecloset' :
    {
        'category' : "Games",
        'updated' : "29 Apr 2022",
        'App_name' : "\uc568\ub9ac\uc2a4\ud074\ub85c\uc82f",
        'Package_name' : "com.kakaogames.alicecloset",
        'installs' : "100,000+",
        'version' : "2.2.0",
    },
    'com.garanti.cepsubesi' :
    {
        'category' : "Finance",
        'updated' : "03 Jun 2022",
        'App_name' : "Garanti BBVA Mobile",
        'Package_name' : "com.garanti.cepsubesi",
        'installs' : "10,000,000+",
        'version' : "11.0",
    },
    '1423221955' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-14T14:37:07Z",
        'App_name' : "Liv Up \u2013 Comida Saud\u00e1vel",
        'Package_name' : "br.com.livup.ecommerce",
        'version' : "41.1.0",
        'minimumOsVersion' : "11.0",
    },
    'ae.etisalat.smiles' :
    {
        'category' : "Lifestyle",
        'updated' : "23 May 2022",
        'App_name' : "Smiles UAE",
        'Package_name' : "ae.etisalat.smiles",
        'installs' : "1,000,000+",
        'version' : "6.3.3",
    },
    'com.moonvideo.android.resso' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "Resso Music - Songs & Lyrics",
        'Package_name' : "com.moonvideo.android.resso",
        'installs' : "100,000,000+",
        'version' : "1.78.0",
    },
    'com.avatrade.mobile' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "Avatrade: Forex & CFD Trading",
        'Package_name' : "com.avatrade.mobile",
        'installs' : "1,000,000+",
        'version' : "100.7",
    },
    'com.next.innovation.takatak.lite' :
    {
        'category' : "Social and Communication",
        'updated' : "27 Aug 2021",
        'App_name' : "MX Takatak Lite",
        'Package_name' : "com.next.innovation.takatak.lite",
        'installs' : "10,000,000+",
        'version' : "1.3.0",
    },
    'com.ludoogames.jxqy' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "\u6200\u6200\u6e05\u5ead:\u9082\u9005",
        'Package_name' : "com.ludoogames.jxqy",
        'installs' : "500,000+",
        'version' : "2.6.9.0",
    },
    'com.taptodate' :
    {
        'category' : "Social and Communication",
        'updated' : "02 Feb 2022",
        'App_name' : "TapToDate - Chat, Meet, Love",
        'Package_name' : "com.taptodate",
        'installs' : "500,000+",
        'version' : "1.0.25.1",
    },
    'fr.vestiairecollective' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "Vestiaire Collective",
        'Package_name' : "fr.vestiairecollective",
        'installs' : "5,000,000+",
        'version' : "3.68.1",
    },
    'kr.co.grow.app' :
    {
        'category' : "Education and Books",
        'updated' : "03 May 2022",
        'App_name' : "\uadf8\ub85c\uc6b0(grow)-\uc131\uc7a5\uc774 \uc2dc\uc791\ub418\ub294 \uacf3.",
        'Package_name' : "kr.co.grow.app",
        'installs' : "100,000+",
        'version' : "2.0.28",
    },
    'com.bandainamcoent.kidsf' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "\u30ad\u30f3\u30b0\u30c0\u30e0 \u30bb\u30d6\u30f3\u30d5\u30e9\u30c3\u30b0\u30b9",
        'Package_name' : "com.bandainamcoent.kidsf",
        'installs' : "500,000+",
        'version' : "8.6.1",
    },
    '1411754473' :
    {
        'category' : "Games",
        'updated' : "2019-12-25T15:06:07Z",
        'App_name' : "\u30ca\u30be\u30c8\u30ad\u306e\u6642\u9593 - \u8b0e\u89e3\u304d\u3067\u63a8\u7406\u529b\u3092\u8a66\u3059\u9762\u767d\u3044\u30b2\u30fc\u30e0",
        'Package_name' : "com.kuraki.dore8",
        'version' : "1.3.4",
        'minimumOsVersion' : "9.0",
    },
    'org.nanobit.hollywood' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "Hollywood Story\u00ae: Fashion Star",
        'Package_name' : "org.nanobit.hollywood",
        'installs' : "10,000,000+",
        'version' : "11.0.2",
    },
    'app.happyvoice.store' :
    {
        'category' : "Social and Communication",
        'updated' : "11 Jun 2022",
        'App_name' : "\u6b61\u6a02\u8a9e\u97f3-\u53f0\u7063\u6b4c\u53cb\u6b61\u6b4c\u6b61\u5531\u5168\u6c11K\u6b4c,\u5531\u6b4c\u804a\u5929\u4ea4\u53cb\u7684\u624b\u6a5fKTV",
        'Package_name' : "app.happyvoice.store",
        'installs' : "100,000+",
        'version' : "2.5.3",
    },
    'kr.co.kodc.rewview' :
    {
        'category' : "Lifestyle",
        'updated' : "03 Jun 2022",
        'App_name' : "\ubdf0\ud2f0/\ud328\uc158/\ub77c\uc774\ud504 \uccb4\ud5d8\ub2e8 \u2013 \ub974\ubdf0",
        'Package_name' : "kr.co.kodc.rewview",
        'installs' : "100,000+",
        'version' : "3.3.4",
    },
    'com.shopkick.app' :
    {
        'category' : "Shopping",
        'updated' : "03 Jun 2022",
        'App_name' : "Shopping for Rewards: Shopkick",
        'Package_name' : "com.shopkick.app",
        'installs' : "10,000,000+",
        'version' : "6.25.0",
    },
    'ru.kfc.kfc_delivery' :
    {
        'category' : "Food & Drink",
        'updated' : "14 Jun 2022",
        'App_name' : "KFC: Delivery, Food & Coupons",
        'Package_name' : "ru.kfc.kfc_delivery",
        'installs' : "10,000,000+",
        'version' : "8.2.0",
    },
    'com.remedicoapp' :
    {
        'category' : "Health and Fitness",
        'updated' : "07 Jun 2022",
        'App_name' : "Remedico: Online Dermatology Solutions",
        'Package_name' : "com.remedicoapp",
        'installs' : "50,000+",
        'version' : "1.1.1",
    },
    'com.skillacademy.mobile' :
    {
        'category' : "Education and Books",
        'updated' : "24 May 2022",
        'App_name' : "Skill Academy by Ruangguru",
        'Package_name' : "com.skillacademy.mobile",
        'installs' : "1,000,000+",
        'version' : "3.5.7",
    },
    'com.and.riseofthekings' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "Rise of the Kings",
        'Package_name' : "com.and.riseofthekings",
        'installs' : "10,000,000+",
        'version' : "1.9.12",
    },
    'com.dena.pokota' :
    {
        'category' : "Social and Communication",
        'updated' : "05 Jun 2022",
        'App_name' : "Pococha Live - Live Stream & Build Your Community",
        'Package_name' : "com.dena.pokota",
        'installs' : "1,000,000+",
        'version' : "5.15.2",
    },
    'com.bitexen.exchange' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "Bitexen - Bitcoin and Altcoin",
        'Package_name' : "com.bitexen.exchange",
        'installs' : "500,000+",
        'version' : "0.71",
    },
    '575920978' :
    {
        'category' : "Shopping",
        'updated' : "2021-11-09T19:08:22Z",
        'App_name' : "BazarChic, ventes priv\u00e9es mode",
        'Package_name' : "com.bazarchicV2",
        'version' : "6.4.2",
        'minimumOsVersion' : "13.0",
    },
    '875855935' :
    {
        'category' : "Finance",
        'updated' : "2022-02-01T11:13:49Z",
        'App_name' : "WorldRemit - Send Money Abroad",
        'Package_name' : "com.worldremit.ios",
        'version' : "3.115.0",
        'minimumOsVersion' : "13.0",
    },
    'com.exorid.TreeCraftman3D' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "Tree Craftman 3D",
        'Package_name' : "com.exorid.TreeCraftman3D",
        'installs' : "1,000,000+",
        'version' : "0.8.5",
    },
    'com.outfit7.mytalkingangelafree' :
    {
        'category' : "Games",
        'updated' : "06 Jun 2022",
        'App_name' : "My Talking Angela",
        'Package_name' : "com.outfit7.mytalkingangelafree",
        'installs' : "500,000,000+",
        'version' : "6.0.2.3411",
    },
    'com.flo.ayakkabi' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "FLO",
        'Package_name' : "com.flo.ayakkabi",
        'installs' : "10,000,000+",
        'version' : "5.0.20",
    },
    'com.bitoasis' :
    {
        'category' : "Finance",
        'updated' : "11 Jun 2022",
        'App_name' : "BitOasis: BTC, Crypto Exchange",
        'Package_name' : "com.bitoasis",
        'installs' : "100,000+",
        'version' : "2.1.14",
    },
    'com.doconline.medicine' :
    {
        'category' : "Health and Fitness",
        'updated' : "12 Apr 2022",
        'App_name' : "Make an appointment with a doctor online on Doc.ua",
        'Package_name' : "com.doconline.medicine",
        'installs' : "100,000+",
        'version' : "1.4.9",
    },
    'com.ulugame.yz2kr.google' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "\uc815\ub9c8\ub2f4",
        'Package_name' : "com.ulugame.yz2kr.google",
        'installs' : "100,000+",
        'version' : "1.0.29",
    },
    'jp.giftole.giftole' :
    {
        'category' : "Entertainment",
        'updated' : "31 Mar 2022",
        'App_name' : "Giftole\uff08\u30aa\u30f3\u30af\u30ec\uff09-\u30af\u30ec\u30fc\u30f3\u30b2\u30fc\u30e0\u306a\u3089\u30ae\u30d5\u30c8\u30fc\u30ec",
        'Package_name' : "jp.giftole.giftole",
        'installs' : "100,000+",
        'version' : "1.0.24",
    },
    'com.tatasky.binge' :
    {
        'category' : "Entertainment",
        'updated' : "28 Mar 2022",
        'App_name' : "Tata Play Binge",
        'Package_name' : "com.tatasky.binge",
        'installs' : "5,000,000+",
        'version' : "3.0.3",
    },
    'com.sharekhan.androidsharemobile' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "Sharekhan: Demat & Trading App",
        'Package_name' : "com.sharekhan.androidsharemobile",
        'installs' : "5,000,000+",
        'version' : "2.3.3.31",
    },
    'jp.picappinc.teller' :
    {
        'category' : "Education and Books",
        'updated' : "14 Jun 2022",
        'App_name' : "\u30c6\u30e9\u30fc\u30ce\u30d9\u30eb\uff1a\u5c0f\u8aac\u304c\u8aad\u307f\u653e\u984c\uff01\u5c0f\u8aac\u30fb\u5922\u5c0f\u8aac\u3092\u66f8\u304f\u30a2\u30d7\u30ea",
        'Package_name' : "jp.picappinc.teller",
        'installs' : "1,000,000+",
        'version' : "7.15.0",
    },
    'com.tunaikumobile.app' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "Tunaiku- Pinjaman Online Cepat",
        'Package_name' : "com.tunaikumobile.app",
        'installs' : "5,000,000+",
        'version' : "1.80.0",
    },
    'com.phablecare' :
    {
        'category' : "Health and Fitness",
        'updated' : "23 May 2022",
        'App_name' : "Phable: Manage BP & Sugar",
        'Package_name' : "com.phablecare",
        'installs' : "1,000,000+",
        'version' : "7.5",
    },
    'com.eyougame.yye.th' :
    {
        'category' : "Games",
        'updated' : "10 May 2022",
        'App_name' : "Moon\u00a0Forest:RPG\u0e41\u0e19\u0e27\u0e15\u0e31\u0e49\u0e07",
        'Package_name' : "com.eyougame.yye.th",
        'installs' : "100,000+",
        'version' : "13.0",
    },
    'com.oakgames.wordgame' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Word Pearls: Word Games",
        'Package_name' : "com.oakgames.wordgame",
        'installs' : "1,000,000+",
        'version' : "1.6.1",
    },
    'com.tejimandi.android' :
    {
        'category' : "Finance",
        'updated' : "20 May 2022",
        'App_name' : "Teji Mandi:Stock/ETF Portfolio",
        'Package_name' : "com.tejimandi.android",
        'installs' : "100,000+",
        'version' : "1.1.65",
    },
    'com.gamepub.ss.g' :
    {
        'category' : "Games",
        'updated' : "17 Aug 2021",
        'App_name' : "\uc30d\uc0bc\uad6d\uc9c0",
        'Package_name' : "com.gamepub.ss.g",
        'installs' : "1,000,000+",
        'version' : "0.0.22",
    },
    'me.dailymeal.main' :
    {
        'category' : "Food & Drink",
        'updated' : "08 Jun 2022",
        'App_name' : "Dailymealz: Food Subscription",
        'Package_name' : "me.dailymeal.main",
        'installs' : "100,000+",
        'version' : "31.1.0",
    },
    'com.qbi.roboadvisor' :
    {
        'category' : "Finance",
        'updated' : "01 Jul 2021",
        'App_name' : "\ucffc\ud130\ubc31 - \uc2e0\uc911\ud55c \uc0ac\ub78c\ub4e4\uc758 \ud604\uba85\ud55c \ud22c\uc790 \uc2b5\uad00",
        'Package_name' : "com.qbi.roboadvisor",
        'installs' : "10,000+",
        'version' : "1.10.1",
    },
    '898244857' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-13T01:48:21Z",
        'App_name' : "Traveloka Lifestyle Superapp",
        'Package_name' : "com.traveloka.traveloka",
        'version' : "3.55.0",
        'minimumOsVersion' : "13.0",
    },
    'com.google.android.googlequicksearchbox' :
    {
        'category' : "Tools",
        'updated' : "16 Jun 2022",
        'App_name' : "Google",
        'Package_name' : "com.google.android.googlequicksearchbox",
        'installs' : "10,000,000,000+",
        'version' : "Varies with device",
    },
    '1479658070' :
    {
        'category' : "Games",
        'updated' : "2020-06-16T01:57:57Z",
        'App_name' : "\u30d7\u30ed\u30b8\u30a7\u30af\u30c8\u30fb\u30b7\u30eb\u30d0\u30fc\u30a6\u30a4\u30f3\u30b0",
        'Package_name' : "com.leyou.acgnjtwo",
        'version' : "1.1.9",
        'minimumOsVersion' : "9.0",
    },
    '1454236362' :
    {
        'category' : "Games",
        'updated' : "2021-12-02T07:38:34Z",
        'App_name' : "\u3061\u3087\u3044\u3068\u53ec\u559a\uff01\u30e2\u30f3\u30b9\u30bf\u30fc\u30d0\u30b9\u30b1\u30c3\u30c8",
        'Package_name' : "com.i-til.monbas",
        'version' : "1.0.7",
        'minimumOsVersion' : "10.0",
    },
    'com.rb.enfamil' :
    {
        'category' : "Health and Fitness",
        'updated' : "06 Jun 2022",
        'App_name' : "Enfamil Family Beginnings\u00ae",
        'Package_name' : "com.rb.enfamil",
        'installs' : "100,000+",
        'version' : "1.2.3",
    },
    '592978487' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-20T06:27:17Z",
        'App_name' : "Careem - Ride, Delivery, Pay",
        'Package_name' : "com.careem.mobile.app.icma",
        'version' : "12.24.0",
        'minimumOsVersion' : "13.0",
    },
    'bndr.music.app' :
    {
        'category' : "Entertainment",
        'updated' : "07 Apr 2022",
        'App_name' : "stream LIVE - by streamusic",
        'Package_name' : "bndr.music.app",
        'installs' : "10,000+",
        'version' : "3.0.11",
    },
    'com.ceremonial.higoTrade' :
    {
        'category' : "Tools",
        'updated' : "08 Jun 2022",
        'App_name' : "Higo Trade -Easy Trading App",
        'Package_name' : "com.ceremonial.higoTrade",
        'installs' : "1,000,000+",
        'version' : "1.3.8",
    },
    '1135368655' :
    {
        'category' : "Games",
        'updated' : "2022-03-26T10:17:05Z",
        'App_name' : "Dig Out!: Jeu de mine aventure",
        'Package_name' : "com.zimad.digout",
        'version' : "2.32.3",
        'minimumOsVersion' : "11.0",
    },
    'in.healthandglow' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Health & Glow - Online Beauty Shopping App",
        'Package_name' : "in.healthandglow",
        'installs' : "1,000,000+",
        'version' : "3.0.31",
    },
    'com.xinhuanet.xinhua_pt' :
    {
        'category' : "Education and Books",
        'updated' : "01 Sep 2021",
        'App_name' : "NOT\u00cdCIAS DA CHINA",
        'Package_name' : "com.xinhuanet.xinhua_pt",
        'installs' : "50,000+",
        'version' : "1.0.8",
    },
    'com.lemon.lvoverseas' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "CapCut - Video Editor",
        'Package_name' : "com.lemon.lvoverseas",
        'installs' : "100,000,000+",
        'version' : "6.0.0",
    },
    'com.ccfun.jjcs' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "\u91d1\u5c07\u50b3\u8aaaM",
        'Package_name' : "com.ccfun.jjcs",
        'installs' : "100,000+",
        'version' : "1.0.10011",
    },
    'com.linecorp.LGRGS' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "LINE Rangers & Shaman King!",
        'Package_name' : "com.linecorp.LGRGS",
        'installs' : "10,000,000+",
        'version' : "8.1.0",
    },
    '1387897651' :
    {
        'category' : "Games",
        'updated' : "2022-06-03T07:43:08Z",
        'App_name' : "Onnect \u2013 Pair Matching Puzzle",
        'Package_name' : "com.gamebility.onet",
        'version' : "24.0.1",
        'minimumOsVersion' : "10.0",
    },
    'com.eyougame.xjhx.th' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "Tales of gaia- PVP\u0e28\u0e36\u0e01\u0e0a\u0e34\u0e07\u0e08\u0e49\u0e32\u0e27",
        'Package_name' : "com.eyougame.xjhx.th",
        'installs' : "500,000+",
        'version' : "39.0",
    },
    '1068818303' :
    {
        'category' : "Games",
        'updated' : "2022-04-28T06:05:31Z",
        'App_name' : "\u5143\u7956\uff01\u3000\u304a\u305d\u677e\u3055\u3093\u306e\u3078\u305d\u304f\u308a\u30a6\u30a9\u30fc\u30ba\u3000\u301c\u30cb\u30fc\u30c8\u306e\u653b\u9632\u301c",
        'Package_name' : "com.avex.pine.towerdefense",
        'version' : "5.6.1",
        'minimumOsVersion' : "10.0",
    },
    'com.mydigipay.app.android' :
    {
        'category' : "Finance",
        'updated' : "21 Apr 2022",
        'App_name' : "My Digipay",
        'Package_name' : "com.mydigipay.app.android",
        'installs' : "1,000,000+",
        'version' : "2.2.6 - GP",
    },
    'com.lavazza.lavazzaconsumer' :
    {
        'category' : "Food & Drink",
        'updated' : "06 Jun 2022",
        'App_name' : "Piacere Lavazza",
        'Package_name' : "com.lavazza.lavazzaconsumer",
        'installs' : "100,000+",
        'version' : "1.2.19",
    },
    '1575135643' :
    {
        'category' : "Finance",
        'updated' : "2022-06-06T05:52:18Z",
        'App_name' : "Al Hilal Digital",
        'Package_name' : "ae.ahb.digital.prod",
        'version' : "1.2.7",
        'minimumOsVersion' : "13.0",
    },
    'com.sa.maana' :
    {
        'category' : "Lifestyle",
        'updated' : "31 May 2022",
        'App_name' : "Yaqoot  \u064a\u0627\u0642\u0648\u062a",
        'Package_name' : "com.sa.maana",
        'installs' : "1,000,000+",
        'version' : "7.6.0",
    },
    'com.koinal.android' :
    {
        'category' : "Finance",
        'updated' : "06 Jun 2022",
        'App_name' : "Koinal: Buy Bitcoin with credit & debit card",
        'Package_name' : "com.koinal.android",
        'installs' : "100,000+",
        'version' : "1.3.7",
    },
    'com.game.qyzh.an' :
    {
        'category' : "Games",
        'updated' : "14 Jul 2020",
        'App_name' : "\u6211\u8981\u5c01\u795e",
        'Package_name' : "com.game.qyzh.an",
        'installs' : "100,000+",
        'version' : "15.0",
    },
    'holy.bible.biblegame.bibleverse.color.by.number.colorbynumber.paint.pixel.art' :
    {
        'category' : "Games",
        'updated' : "06 May 2022",
        'App_name' : "Bible Coloring Paint By Number",
        'Package_name' : "holy.bible.biblegame.bibleverse.color.by.number.colorbynumber.paint.pixel.art",
        'installs' : "5,000,000+",
        'version' : "2.24.8",
    },
    '1255116464' :
    {
        'category' : "Games",
        'updated' : "2022-03-27T16:15:08Z",
        'App_name' : "Re:\u30b9\u30c6\u30fc\u30b8\uff01\u30d7\u30ea\u30ba\u30e0\u30b9\u30c6\u30c3\u30d7",
        'Package_name' : "com.ponycanyon.game.prismstep",
        'version' : "1.1.76",
        'minimumOsVersion' : "10.0",
    },
    '1524910970' :
    {
        'category' : "Games",
        'updated' : "2021-05-24T06:23:28Z",
        'App_name' : "Master Topia",
        'Package_name' : "com.vqw.aets",
        'version' : "1.3.2",
        'minimumOsVersion' : "8.0",
    },
    'jp.co.gachi.gachi' :
    {
        'category' : "Games",
        'updated' : "19 May 2022",
        'App_name' : "CRANE GAME 'GACHI' ufo catcher",
        'Package_name' : "jp.co.gachi.gachi",
        'installs' : "10,000+",
        'version' : "1.1.0",
    },
    'jp.co.enterbrain.kansen_syoujo' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "\u611f\u67d3\u00d7\u5c11\u5973",
        'Package_name' : "jp.co.enterbrain.kansen_syoujo",
        'installs' : "100,000+",
        'version' : "2.0.31",
    },
    'com.denizbank.firsatlardenizi' :
    {
        'category' : "Finance",
        'updated' : "23 May 2022",
        'App_name' : "DenizKart\u0131m",
        'Package_name' : "com.denizbank.firsatlardenizi",
        'installs' : "1,000,000+",
        'version' : "2.7.6",
    },
    '1503110837' :
    {
        'category' : "Games",
        'updated' : "2022-05-20T06:39:33Z",
        'App_name' : "SINoALICE",
        'Package_name' : "com.global.sinoalice",
        'version' : "31.0.0",
        'minimumOsVersion' : "10.0",
    },
    '1431776088' :
    {
        'category' : "Health and Fitness",
        'updated' : "2022-06-09T07:15:38Z",
        'App_name' : "\u0627\u0644\u0643\u0648\u062a\u0634 - \u0628\u0631\u0627\u0645\u062c \u062a\u0645\u0627\u0631\u064a\u0646 \u0648\u062a\u063a\u0630\u064a\u0629",
        'Package_name' : "com.elcoach.me",
        'version' : "4.1.7",
        'minimumOsVersion' : "13.0",
    },
    'com.kbstar.land' :
    {
        'category' : "House & Home",
        'updated' : "15 Jun 2022",
        'App_name' : "KB\ubd80\ub3d9\uc0b0 - \uc544\ud30c\ud2b8 \ub2e8\uc9c0 \ub9e4\ubb3c \ubd84\uc591 \ube4c\ub77c \uc2dc\uc138",
        'Package_name' : "com.kbstar.land",
        'installs' : "1,000,000+",
        'version' : "1.4.35",
    },
    'com.timeling.android' :
    {
        'category' : "Lifestyle",
        'updated' : "18 May 2022",
        'App_name' : "Timeling: Dates, Meet, Friends",
        'Package_name' : "com.timeling.android",
        'installs' : "10,000+",
        'version' : "1.0.37",
    },
    'de.prosiebensat1digital.seventv' :
    {
        'category' : "Entertainment",
        'updated' : "09 Jun 2022",
        'App_name' : "Joyn | deine Streaming App",
        'Package_name' : "de.prosiebensat1digital.seventv",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'kr.co.burgerkinghybrid' :
    {
        'category' : "Lifestyle",
        'updated' : "07 Apr 2022",
        'App_name' : "(\uacf5\uc2dd) \ubc84\uac70\ud0b9 BURGER KING\u00aeKOREA",
        'Package_name' : "kr.co.burgerkinghybrid",
        'installs' : "5,000,000+",
        'version' : "4.1.01",
    },
    'ph.onlineloans.mobile.android' :
    {
        'category' : "Finance",
        'updated' : "27 May 2022",
        'App_name' : "Online Loans Pilipinas App",
        'Package_name' : "ph.onlineloans.mobile.android",
        'installs' : "1,000,000+",
        'version' : "1.0.496-google",
    },
    '493304166' :
    {
        'category' : "Not available",
        'updated' : "2022-05-19T16:19:50Z",
        'App_name' : "Ouest-France, l'info en direct",
        'Package_name' : "fr.ouestfrance.OuestFrance",
        'version' : "4.10.3",
        'minimumOsVersion' : "11.0",
    },
    'com.zivame.consumer' :
    {
        'category' : "Shopping",
        'updated' : "12 Apr 2022",
        'App_name' : "Zivame - One Stop Lingerie App",
        'Package_name' : "com.zivame.consumer",
        'installs' : "10,000,000+",
        'version' : "3.15.1",
    },
    'com.coc.kr.cocmsl.google' :
    {
        'category' : "Games",
        'updated' : "09 Jul 2021",
        'App_name' : "\uba78\ub9dd\ub85d: 14\uc77c\uac04\uc758 \uc885\ub9d0 MMORPG",
        'Package_name' : "com.coc.kr.cocmsl.google",
        'installs' : "100,000+",
        'version' : "1.0.10",
    },
    'com.photobox.android' :
    {
        'category' : "Entertainment",
        'updated' : "02 Jun 2022",
        'App_name' : "Photobox - Photo Books, Prints",
        'Package_name' : "com.photobox.android",
        'installs' : "1,000,000+",
        'version' : "97",
    },
    '394401915' :
    {
        'category' : "Finance",
        'updated' : "2022-06-14T20:27:32Z",
        'App_name' : "Ita\u00fa: Cart\u00f5es de cr\u00e9dito",
        'Package_name' : "com.itau.itaucard",
        'version' : "6.8.9",
        'minimumOsVersion' : "10.3",
    },
    'com.amanotes.beathopper' :
    {
        'category' : "Games",
        'updated' : "04 Jun 2022",
        'App_name' : "Tiles Hop: EDM Rush!",
        'Package_name' : "com.amanotes.beathopper",
        'installs' : "100,000,000+",
        'version' : "3.9.4",
    },
    'jp.gocro.smartnews.android' :
    {
        'category' : "Education and Books",
        'updated' : "10 Jun 2022",
        'App_name' : "SmartNews: Local Breaking News",
        'Package_name' : "jp.gocro.smartnews.android",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    '1184852109' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-08T08:56:16Z",
        'App_name' : "TUI.com Reisen - Urlaub buchen",
        'Package_name' : "de.tui.tuicom",
        'version' : "14.2.0",
        'minimumOsVersion' : "14.0",
    },
    'show.grip' :
    {
        'category' : "Shopping",
        'updated' : "30 May 2022",
        'App_name' : "\uadf8\ub9bd Grip - \uc804\uad6d\ubbfc \ub77c\uc774\ube0c \u5927\uc7a5\ud130",
        'Package_name' : "show.grip",
        'installs' : "1,000,000+",
        'version' : "3.2.1",
    },
    'co.tslc.cashe.android' :
    {
        'category' : "Finance",
        'updated' : "18 May 2022",
        'App_name' : "CASHe Personal Loan App",
        'Package_name' : "co.tslc.cashe.android",
        'installs' : "5,000,000+",
        'version' : "8.7.5",
    },
    '1098157959' :
    {
        'category' : "Games",
        'updated' : "2022-06-16T22:18:03Z",
        'App_name' : "Evony",
        'Package_name' : "com.topgamesinc.evony",
        'version' : "4.25.1",
        'minimumOsVersion' : "9.0",
    },
    'com.ev.live' :
    {
        'category' : "Lifestyle",
        'updated' : "15 Jun 2022",
        'App_name' : "Guruji - Live Astro, Horoscope",
        'Package_name' : "com.ev.live",
        'installs' : "5,000,000+",
        'version' : "2.46.2",
    },
    'net.townwork.recruit' :
    {
        'category' : "Lifestyle",
        'updated' : "08 Jun 2022",
        'App_name' : "\u30d0\u30a4\u30c8\u63a2\u3057\u306f\u30bf\u30a6\u30f3\u30ef\u30fc\u30af\uff01\u30d0\u30a4\u30c8\u30fb\u30a2\u30eb\u30d0\u30a4\u30c8\u6c42\u4eba\u30a2\u30d7\u30ea",
        'Package_name' : "net.townwork.recruit",
        'installs' : "1,000,000+",
        'version' : "6.66.0",
    },
    'com.games24x7.ultimaterummy.playstore' :
    {
        'category' : "Games",
        'updated' : "26 May 2021",
        'App_name' : "Ultimate Rummy",
        'Package_name' : "com.games24x7.ultimaterummy.playstore",
        'installs' : "10,000,000+",
        'version' : "1.11.33",
    },
    '1029026167' :
    {
        'category' : "Finance",
        'updated' : "2022-06-15T16:26:11Z",
        'App_name' : "ininal C\u00fczdan",
        'Package_name' : "com.ngier.ininalwallet",
        'version' : "3.2.32",
        'minimumOsVersion' : "12.4",
    },
    'com.sanhe.clipclaps' :
    {
        'category' : "Lifestyle",
        'updated' : "08 Jun 2022",
        'App_name' : "ClipClaps - Find your interest",
        'Package_name' : "com.sanhe.clipclaps",
        'installs' : "10,000,000+",
        'version' : "3.9.0",
    },
    'amaro.amaroandroid' :
    {
        'category' : "Shopping",
        'updated' : "06 Jun 2022",
        'App_name' : "AMARO: Comprar Moda e Beleza",
        'Package_name' : "amaro.amaroandroid",
        'installs' : "1,000,000+",
        'version' : "3.11.0",
    },
    'com.comuto' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "10 Jun 2022",
        'App_name' : "BlaBlaCar: Carpooling and Bus",
        'Package_name' : "com.comuto",
        'installs' : "50,000,000+",
        'version' : "5.114.0",
    },
    'com.kyte' :
    {
        'category' : "Finance",
        'updated' : "21 Apr 2022",
        'App_name' : "Catalog and POS System by Kyte",
        'Package_name' : "com.kyte",
        'installs' : "1,000,000+",
        'version' : "1.25.11",
    },
    'com.boloindya.boloindya' :
    {
        'category' : "Lifestyle",
        'updated' : "14 Jun 2022",
        'App_name' : "Bolo Live -Stream & Video Chat",
        'Package_name' : "com.boloindya.boloindya",
        'installs' : "5,000,000+",
        'version' : "6.1.42",
    },
    'com.goodluck777.panther' :
    {
        'category' : "Casino",
        'updated' : "10 Jun 2022",
        'App_name' : "\u91d1\u597d\u904b\u5a1b\u6a02\u57ce - \u67b1\u5b50\u6700\u591a\u958b\u734e\u6700\u760b \u5a01\u9be8\u50b3\u5947\u6355\u9b5a\u6a5f \u8001\u864e\u6a5f",
        'Package_name' : "com.goodluck777.panther",
        'installs' : "500,000+",
        'version' : "1.49.2",
    },
    'com.balaji.alt' :
    {
        'category' : "Entertainment",
        'updated' : "06 Jun 2022",
        'App_name' : "ALTBalaji : Web Series & More",
        'Package_name' : "com.balaji.alt",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.traveloka.android' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "16 Jun 2022",
        'App_name' : "Traveloka Lifestyle Superapp",
        'Package_name' : "com.traveloka.android",
        'installs' : "50,000,000+",
        'version' : "3.55.1",
    },
    'com.boyaa.enginetaiwanqp.main' :
    {
        'category' : "Casino",
        'updated' : "16 May 2022",
        'App_name' : "\u958b\u5fc3\u9b25\u4e00\u756a-\u9b25\u5730\u4e3b \u9ebb\u5c07 \u5341\u4e09\u652f \u5927\u8001\u4e8c \u5fb7\u5dde\u64b2\u514b\u7b49\u5408\u96c6\u904a\u6232",
        'Package_name' : "com.boyaa.enginetaiwanqp.main",
        'installs' : "100,000+",
        'version' : "3.8.3",
    },
    'com.turkiyepetroller.TP' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "14 Jun 2022",
        'App_name' : "T\u00fcrkiye Petrolleri",
        'Package_name' : "com.turkiyepetroller.TP",
        'installs' : "100,000+",
        'version' : "2.0.22",
    },
    'com.Level5.YSG2' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\u5996\u602a\u4e09\u56fd\u5fd7 \u56fd\u76d7\u308a\u30a6\u30a9\u30fc\u30ba",
        'Package_name' : "com.Level5.YSG2",
        'installs' : "500,000+",
        'version' : "9.09.00",
    },
    'jp.paters.android' :
    {
        'category' : "Social and Communication",
        'updated' : "15 Jun 2022",
        'App_name' : "paters: Chat, Date & More",
        'Package_name' : "jp.paters.android",
        'installs' : "100,000+",
        'version' : "4.5.7",
    },
    'com.sonyliv' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "SonyLIV:Entertainment & Sports",
        'Package_name' : "com.sonyliv",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.gunosy.android' :
    {
        'category' : "Education and Books",
        'updated' : "15 Jun 2022",
        'App_name' : "\u30b0\u30ce\u30b7\u30fc - \u91cd\u8981\u30cb\u30e5\u30fc\u30b9\u3092\u9003\u3055\u306a\u3044\u3001\u5b9a\u756a\u30cb\u30e5\u30fc\u30b9\u30a2\u30d7\u30ea",
        'Package_name' : "com.gunosy.android",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.stove.epic7.google' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "Epic Seven",
        'Package_name' : "com.stove.epic7.google",
        'installs' : "5,000,000+",
        'version' : "1.0.487",
    },
    'au.com.talihealth.talidetect.india' :
    {
        'category' : "Education and Books",
        'updated' : "04 Apr 2022",
        'App_name' : "TALi",
        'Package_name' : "au.com.talihealth.talidetect.india",
        'installs' : "50,000+",
        'version' : "1.6.10",
    },
    'com.clarocolombia.miclaro' :
    {
        'category' : "Tools",
        'updated' : "06 Jun 2022",
        'App_name' : "Mi Claro",
        'Package_name' : "com.clarocolombia.miclaro",
        'installs' : "10,000,000+",
        'version' : "13.3.0",
    },
    '1231921402' :
    {
        'category' : "Games",
        'updated' : "2022-05-31T15:21:07Z",
        'App_name' : "\u4efb\u4fa0\u4f1d\u3000\u4e0d\u826f\u9054\u306e\u30ac\u30c1\u30f3\u30b3\u55a7\u5629\u30d0\u30c8\u30eb",
        'Package_name' : "jp.nkdn2",
        'version' : "1.0.98",
        'minimumOsVersion' : "8.0",
    },
    '905953485' :
    {
        'category' : "Utilities",
        'updated' : "2022-05-27T10:56:47Z",
        'App_name' : "NordVPN: VPN Fast & Secure",
        'Package_name' : "com.nordvpn.NordVPN",
        'version' : "7.13.0",
        'minimumOsVersion' : "11.0",
    },
    '610418370' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-07T09:51:30Z",
        'App_name' : "Wehkamp",
        'Package_name' : "nl.wehkamp.wehkamp.nl",
        'version' : "3.128.1",
        'minimumOsVersion' : "12.1",
    },
    'jp.co.peanutsclub.mobacure' :
    {
        'category' : "Games",
        'updated' : "06 May 2022",
        'App_name' : "Mobacure (Online Crane Game)",
        'Package_name' : "jp.co.peanutsclub.mobacure",
        'installs' : "50,000+",
        'version' : "2.3.0",
    },
    '284910350' :
    {
        'category' : "Food & Drink",
        'updated' : "2021-01-11T19:13:39Z",
        'App_name' : "Yelp: Food, Delivery & Reviews",
        'Package_name' : "com.yelp.yelpiphone",
        'version' : "22.24.0",
        'minimumOsVersion' : "14.0",
    },
    'com.gmt.hoathienkiepgamota' :
    {
        'category' : "Games",
        'updated' : "08 Nov 2019",
        'App_name' : "Hoa Thi\u00ean Ki\u1ebfp",
        'Package_name' : "com.gmt.hoathienkiepgamota",
        'installs' : "500,000+",
        'version' : "6.0",
    },
    '1200727466' :
    {
        'category' : "Games",
        'updated' : "2022-05-25T04:02:53Z",
        'App_name' : "\u5929\u58022\uff1a\u9769\u547d",
        'Package_name' : "com.netmarble.revolutionthm",
        'version' : "1.34.12",
        'minimumOsVersion' : "10.0",
    },
    'com.rsg.mhs' :
    {
        'category' : "Games",
        'updated' : "15 Mar 2022",
        'App_name' : "\u6211\u7684\u52c7\u8005",
        'Package_name' : "com.rsg.mhs",
        'installs' : "100,000+",
        'version' : "21.68.0",
    },
    'com.vtcmobile.thanvuongnhatthe' :
    {
        'category' : "Games",
        'updated' : "08 Dec 2021",
        'App_name' : "Th\u1ea7n V\u01b0\u01a1ng Nh\u1ea5t Th\u1ebf - Game C\u00e0y Th\u1ebf H\u1ec7 M\u1edbi",
        'Package_name' : "com.vtcmobile.thanvuongnhatthe",
        'installs' : "100,000+",
        'version' : "2.4.1",
    },
    'net.hi94.UAD.android' :
    {
        'category' : "Games",
        'updated' : "12 Aug 2021",
        'App_name' : "\u4e3b\u516c\u5728\u4e0a\u6211\u5728\u4e0b",
        'Package_name' : "net.hi94.UAD.android",
        'installs' : "10,000+",
        'version' : "1.0.1",
    },
    'com.pig.toro.aos' :
    {
        'category' : "Games",
        'updated' : "29 Jul 2020",
        'App_name' : "Toro and Friends: Onsen Town",
        'Package_name' : "com.pig.toro.aos",
        'installs' : "10,000+",
        'version' : "1.2.1",
    },
    'com.tul.tatacliq' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "Tata CLiQ Shopping App India",
        'Package_name' : "com.tul.tatacliq",
        'installs' : "50,000,000+",
        'version' : "47",
    },
    'com.gameylywl.tw' :
    {
        'category' : "Games",
        'updated' : "25 Dec 2019",
        'App_name' : "\u82f1\u9748\u7570\u805e\u9304",
        'Package_name' : "com.gameylywl.tw",
        'installs' : "100,000+",
        'version' : "1.8.0.19",
    },
    '1456221314' :
    {
        'category' : "Games",
        'updated' : "2022-04-13T01:36:12Z",
        'App_name' : "GUNDAM BREAKER\uff1a\u92fc\u5f48\u5275\u58de\u8005 MOBILE",
        'Package_name' : "jp.co.bandainamcoent.BNEI0352",
        'version' : "3.4.0",
        'minimumOsVersion' : "11.0",
    },
    'com.tatadigital.tcp' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Tata Neu-rewarding experiences",
        'Package_name' : "com.tatadigital.tcp",
        'installs' : "5,000,000+",
        'version' : "2.1.1",
    },
    'kr.co.hiver' :
    {
        'category' : "Shopping",
        'updated' : "None",
        'App_name' : "None",
        'Package_name' : "kr.co.hiver",
        'installs' : "None",
        'version' : "Varies with device",
    },
    '1068378177' :
    {
        'category' : "Games",
        'updated' : "2022-06-20T01:51:09Z",
        'App_name' : "\u904a\u622f\u738b \u30c7\u30e5\u30a8\u30eb\u30ea\u30f3\u30af\u30b9",
        'Package_name' : "jp.konami.yugiohmobile",
        'version' : "6.8.0",
        'minimumOsVersion' : "10.0",
    },
    'com.Level5.YWP' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "\u5996\u602a\u30a6\u30a9\u30c3\u30c1 \u3077\u306b\u3077\u306b",
        'Package_name' : "com.Level5.YWP",
        'installs' : "5,000,000+",
        'version' : "4.72.0",
    },
    'com.lilithgame.hgame.gp.jp' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "AFK \u30a2\u30ea\u30fc\u30ca",
        'Package_name' : "com.lilithgame.hgame.gp.jp",
        'installs' : "500,000+",
        'version' : "1.91.01",
    },
    'com.fordeal.android' :
    {
        'category' : "Shopping",
        'updated' : "17 Jun 2022",
        'App_name' : "Fordeal - \u0641\u0648\u0631\u062f\u064a\u0644 \u0633\u0648\u0642 \u0627\u0644\u0627\u0646\u062a\u0631\u0646\u062a",
        'Package_name' : "com.fordeal.android",
        'installs' : "10,000,000+",
        'version' : "5.2.5",
    },
    '1059665257' :
    {
        'category' : "Games",
        'updated' : "2022-05-27T02:00:10Z",
        'App_name' : "MLB 9\uc774\ub2dd\uc2a4 22",
        'Package_name' : "com.com2us.ninepb3d.normal.freefull.apple.global.ios.universal",
        'version' : "7.0.7",
        'minimumOsVersion' : "11.0",
    },
    'br.com.evino.android' :
    {
        'category' : "Food & Drink",
        'updated' : "11 Jun 2022",
        'App_name' : "Evino: Compre Vinho Online",
        'Package_name' : "br.com.evino.android",
        'installs' : "1,000,000+",
        'version' : "1.16.0",
    },
    'com.lezhin.comics' :
    {
        'category' : "Education and Books",
        'updated' : "31 May 2022",
        'App_name' : "Lezhin Comics - Daily Releases",
        'Package_name' : "com.lezhin.comics",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.galliance.battleclash.goo' :
    {
        'category' : "Games",
        'updated' : "04 May 2022",
        'App_name' : "\ubc30\ud2c0 \ud074\ub798\uc2dc: \ud788\uc5b4\ub85c \uc624\ube0c \ud0b9\ub364\uc988",
        'Package_name' : "com.galliance.battleclash.goo",
        'installs' : "5,000+",
        'version' : "1.0.11",
    },
    '1496742556' :
    {
        'category' : "Games",
        'updated' : "2022-05-24T03:00:32Z",
        'App_name' : "\ub85c\ub4dc \uc624\ube0c \ud788\uc5b4\ub85c\uc988",
        'Package_name' : "com.clovergames.lordofheroes",
        'version' : "1.2.052405",
        'minimumOsVersion' : "10.0",
    },
    'com.carrefour.fid.android' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "Carrefour : drive et livraison",
        'Package_name' : "com.carrefour.fid.android",
        'installs' : "5,000,000+",
        'version' : "14.7.0",
    },
    'jp.youbride.android' :
    {
        'category' : "Social and Communication",
        'updated' : "25 Apr 2022",
        'App_name' : "youbride \u5a5a\u6d3b\u30fb\u518d\u5a5a\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea",
        'Package_name' : "jp.youbride.android",
        'installs' : "500,000+",
        'version' : "4.3.25",
    },
    'com.baishe.mmu' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "B\u1ea1ch X\u00e0 Ti\u00ean Ki\u1ebfp",
        'Package_name' : "com.baishe.mmu",
        'installs' : "100,000+",
        'version' : "1052709",
    },
    'jp.co.cybird.appli.android.sgk.en' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "IkemenSengoku Otome Anime Game",
        'Package_name' : "jp.co.cybird.appli.android.sgk.en",
        'installs' : "1,000,000+",
        'version' : "1.1.4",
    },
    'com.loongcheer.neverlate.wizardlegend.fightmaster' :
    {
        'category' : "Games",
        'updated' : "17 Dec 2021",
        'App_name' : "Wizard Legend: Fighting Master",
        'Package_name' : "com.loongcheer.neverlate.wizardlegend.fightmaster",
        'installs' : "1,000,000+",
        'version' : "2.4.5",
    },
    'com.deriv.app' :
    {
        'category' : "Finance",
        'updated' : "27 Apr 2022",
        'App_name' : "Deriv GO",
        'Package_name' : "com.deriv.app",
        'installs' : "100,000+",
        'version' : "0.8.8",
    },
    'com.dreame.reader' :
    {
        'category' : "Education and Books",
        'updated' : "30 May 2022",
        'App_name' : "Dreame",
        'Package_name' : "com.dreame.reader",
        'installs' : "10,000,000+",
        'version' : "3.5.1",
    },
    'com.NetmedsMarketplace.Netmeds' :
    {
        'category' : "Health and Fitness",
        'updated' : "13 May 2022",
        'App_name' : "Netmeds - India Ki Pharmacy",
        'Package_name' : "com.NetmedsMarketplace.Netmeds",
        'installs' : "10,000,000+",
        'version' : "8.2.34",
    },
    'com.papumba.academyforkids' :
    {
        'category' : "Education and Books",
        'updated' : "24 May 2022",
        'App_name' : "Papumba: Games for Toddler 2+",
        'Package_name' : "com.papumba.academyforkids",
        'installs' : "1,000,000+",
        'version' : "1.74",
    },
    'com.kreyougame.galaxy' :
    {
        'category' : "Games",
        'updated' : "31 Aug 2021",
        'App_name' : "\ub9ac\ubc84\ud2f0\ub808\uae30\uc628",
        'Package_name' : "com.kreyougame.galaxy",
        'installs' : "100,000+",
        'version' : "18.0",
    },
    'com.ninegames.solitaire' :
    {
        'category' : "Games",
        'updated' : "02 Jul 2021",
        'App_name' : "InfinitySolitaire",
        'Package_name' : "com.ninegames.solitaire",
        'installs' : "1,000+",
        'version' : "1.1.3",
    },
    'com.netmarble.lineageII' :
    {
        'category' : "Games",
        'updated' : "26 Apr 2022",
        'App_name' : "\ub9ac\ub2c8\uc9c02 \ub808\ubcfc\ub8e8\uc158",
        'Package_name' : "com.netmarble.lineageII",
        'installs' : "5,000,000+",
        'version' : "0.82.12",
    },
    'com.worldwitchesunitedfront.forwardworks' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "\u30ef\u30fc\u30eb\u30c9\u30a6\u30a3\u30c3\u30c1\u30fc\u30ba UNITED FRONT\uff08\u30e6\u30ca\u30d5\u30ed\uff09",
        'Package_name' : "com.worldwitchesunitedfront.forwardworks",
        'installs' : "100,000+",
        'version' : "3.2.6",
    },
    'jp.feliznet' :
    {
        'category' : "Social and Communication",
        'updated' : "02 Jun 2022",
        'App_name' : "feliz-\u604b\u4eba\u3084\u51fa\u4f1a\u3044\u63a2\u3057\u306e\u5a5a\u6d3b\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea",
        'Package_name' : "jp.feliznet",
        'installs' : "100,000+",
        'version' : "2.4.2",
    },
    'com.bcadigital.blu' :
    {
        'category' : "Finance",
        'updated' : "07 Jun 2022",
        'App_name' : "blu by BCA Digital",
        'Package_name' : "com.bcadigital.blu",
        'installs' : "1,000,000+",
        'version' : "1.20.0",
    },
    'com.okcupid.okcupid' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "OkCupid: Online Dating App",
        'Package_name' : "com.okcupid.okcupid",
        'installs' : "10,000,000+",
        'version' : "66.1.0",
    },
    'com.nosmk.burgerking' :
    {
        'category' : "Food & Drink",
        'updated' : "01 Jun 2022",
        'App_name' : "Burger King\u00ae Mexico",
        'Package_name' : "com.nosmk.burgerking",
        'installs' : "1,000,000+",
        'version' : "4.8.0",
    },
    'com.sevenkingdom.novastar' :
    {
        'category' : "Games",
        'updated' : "16 Dec 2020",
        'App_name' : "\uce60\uad6d\uc9c0",
        'Package_name' : "com.sevenkingdom.novastar",
        'installs' : "10,000+",
        'version' : "1.7",
    },
    'fr.casino.fidelite' :
    {
        'category' : "Shopping",
        'updated' : "30 May 2022",
        'App_name' : "Casino Max - Promos & fid\u00e9lit\u00e9",
        'Package_name' : "fr.casino.fidelite",
        'installs' : "1,000,000+",
        'version' : "13.7.0",
    },
    'ai.wizely.android' :
    {
        'category' : "Finance",
        'updated' : "17 Jun 2022",
        'App_name' : "Start saving with just Rs 10",
        'Package_name' : "ai.wizely.android",
        'installs' : "1,000,000+",
        'version' : "5.0.8-release-508",
    },
    'com.okinc.okex.gp' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "OKX: Buy Bitcoin, ETH, Crypto",
        'Package_name' : "com.okinc.okex.gp",
        'installs' : "1,000,000+",
        'version' : "6.0.42",
    },
    'godticket.mobile' :
    {
        'category' : "Social and Communication",
        'updated' : "30 May 2022",
        'App_name' : "\uc5ec\uc2e0\ud2f0\ucf13 - \ud53c\ubd80, \uc058\ub760, \uc131\ud615, \ub2e4\uc774\uc5b4\ud2b8 \uac00\uaca9\ube44\uad50",
        'Package_name' : "godticket.mobile",
        'installs' : "500,000+",
        'version' : "2.1.10",
    },
    'live.video.shopping.simsim' :
    {
        'category' : "Shopping",
        'updated' : "25 Mar 2022",
        'App_name' : "simsim - Watch Videos & Shop",
        'Package_name' : "live.video.shopping.simsim",
        'installs' : "10,000,000+",
        'version' : "1.0.66",
    },
    'com.nexon.nsc.maplem' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "\uba54\uc774\ud50c\uc2a4\ud1a0\ub9acM",
        'Package_name' : "com.nexon.nsc.maplem",
        'installs' : "5,000,000+",
        'version' : "1.78.3367",
    },
    'com.global.dzgkr.google' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\uac11\ubd80: \uc7a5\uc0ac\uc758 \uc2dc\ub300",
        'Package_name' : "com.global.dzgkr.google",
        'installs' : "500,000+",
        'version' : "4.7.0",
    },
    '1343688545' :
    {
        'category' : "Games",
        'updated' : "2022-05-23T05:29:16Z",
        'App_name' : "ONE PIECE Bounty Rush",
        'Package_name' : "jp.co.bandainamcoent.BNEI0297",
        'version' : "51100",
        'minimumOsVersion' : "10.0",
    },
    'com.aosapp.chatradar' :
    {
        'category' : "Social and Communication",
        'updated' : "07 Jun 2022",
        'App_name' : "\ud074\ub7fd5678 - \uc2e4\uc2dc\uac04 \ucc44\ud305, \uc74c\uc131, \uc601\uc0c1\ub300\ud654 \uc5b4\ud50c",
        'Package_name' : "com.aosapp.chatradar",
        'installs' : "500,000+",
        'version' : "2.6.361",
    },
    'com.pieyel.scrabble' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Scrabble\u00ae GO-Classic Word Game",
        'Package_name' : "com.pieyel.scrabble",
        'installs' : "10,000,000+",
        'version' : "1.47.5",
    },
    'com.chaplin.zxz.google' :
    {
        'category' : "Games",
        'updated' : "11 Mar 2022",
        'App_name' : "\uac00\uc628:\ud328\uad8c\uc758 \uc2dc\ub300",
        'Package_name' : "com.chaplin.zxz.google",
        'installs' : "100,000+",
        'version' : "1.5.8",
    },
    'com.defacto.android' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "DeFacto - Clothing & Shopping",
        'Package_name' : "com.defacto.android",
        'installs' : "5,000,000+",
        'version' : "4.1.2",
    },
    'de.rewe.app.mobile' :
    {
        'category' : "Food & Drink",
        'updated' : "17 Jun 2022",
        'App_name' : "REWE - Angebote & Coupons",
        'Package_name' : "de.rewe.app.mobile",
        'installs' : "1,000,000+",
        'version' : "2.19.3",
    },
    'com.wingloryinternational.mydailycash' :
    {
        'category' : "Lifestyle",
        'updated' : "26 Mar 2022",
        'App_name' : "MyDailyCash",
        'Package_name' : "com.wingloryinternational.mydailycash",
        'installs' : "1,000,000+",
        'version' : "1.0.28",
    },
    'com.subway.mobile.subwayapp03' :
    {
        'category' : "Food & Drink",
        'updated' : "31 May 2022",
        'App_name' : "Subway\u00ae",
        'Package_name' : "com.subway.mobile.subwayapp03",
        'installs' : "10,000,000+",
        'version' : "29.13.0",
    },
    'biz.maca2c' :
    {
        'category' : "Social and Communication",
        'updated' : "16 Jun 2022",
        'App_name' : "\u5e74\u4e0a\u30c1\u30e3\u30c3\u30c8\uff5e\u3054\u8fd1\u6240\u3067\u5927\u4eba\u306e\u5a5a\u6d3b\u604b\u6d3b\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea\uff5e",
        'Package_name' : "biz.maca2c",
        'installs' : "10,000+",
        'version' : "1.6",
    },
    'com.smilegate.magicshop.stove.google' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Magical Atelier",
        'Package_name' : "com.smilegate.magicshop.stove.google",
        'installs' : "500,000+",
        'version' : "2.5.18",
    },
    'com.healthkart.healthkart' :
    {
        'category' : "Health and Fitness",
        'updated' : "03 Jun 2022",
        'App_name' : "HealthKart: Fitness for All",
        'Package_name' : "com.healthkart.healthkart",
        'installs' : "1,000,000+",
        'version' : "18.1.0",
    },
    'com.playrix.township' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Township",
        'Package_name' : "com.playrix.township",
        'installs' : "100,000,000+",
        'version' : "9.2.0",
    },
    'com.hokplay.google.cwsk' :
    {
        'category' : "Games",
        'updated' : "28 Oct 2021",
        'App_name' : "\u932f\u4f4d\u6642\u7a7a",
        'Package_name' : "com.hokplay.google.cwsk",
        'installs' : "5,000+",
        'version' : "1.7.7",
    },
    'my.socar.flex' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "15 Jun 2022",
        'App_name' : "SOCAR",
        'Package_name' : "my.socar.flex",
        'installs' : "100,000+",
        'version' : "2.2.7",
    },
    'social.tsu.android' :
    {
        'category' : "Social and Communication",
        'updated' : "10 Jun 2022",
        'App_name' : "Display Social",
        'Package_name' : "social.tsu.android",
        'installs' : "5,000,000+",
        'version' : "2.4.2.2",
    },
    'com.nebula.mamu.lite' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "4Fun Lite - Live Chat Room",
        'Package_name' : "com.nebula.mamu.lite",
        'installs' : "10,000,000+",
        'version' : "6.08",
    },
    'jp.co.paperboy.minne.app' :
    {
        'category' : "Shopping",
        'updated' : "09 Jun 2022",
        'App_name' : "minne\uff08\u30df\u30f3\u30cd\uff09 - \u30cf\u30f3\u30c9\u30e1\u30a4\u30c9\u30de\u30fc\u30b1\u30c3\u30c8\u30a2\u30d7\u30ea",
        'Package_name' : "jp.co.paperboy.minne.app",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.yoozoo.kr.wddg' :
    {
        'category' : "Games",
        'updated' : "28 May 2021",
        'App_name' : "AFK\uc0bc\uad6d\uc9c0",
        'Package_name' : "com.yoozoo.kr.wddg",
        'installs' : "50,000+",
        'version' : "1.26",
    },
    'com.immortaltaoists.en' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Immortal Taoists - Idle Manga",
        'Package_name' : "com.immortaltaoists.en",
        'installs' : "1,000,000+",
        'version' : "1.6.6",
    },
    'com.musicow.android' :
    {
        'category' : "Finance",
        'updated' : "26 Apr 2022",
        'App_name' : "MUSICOW",
        'Package_name' : "com.musicow.android",
        'installs' : "1,000,000+",
        'version' : "1.3.15",
    },
    '1008719792' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-04-27T13:05:01Z",
        'App_name' : "Evino: Compre Vinho Online",
        'Package_name' : "br.com.evino.app",
        'version' : "1.11.6",
        'minimumOsVersion' : "12.3",
    },
    'com.tva.z5' :
    {
        'category' : "Entertainment",
        'updated' : "02 Jun 2022",
        'App_name' : "Weyyak \u0648\u064a\u0627\u0643",
        'Package_name' : "com.tva.z5",
        'installs' : "10,000,000+",
        'version' : "1.0.209",
    },
    'com.dreamplug.androidapp' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "CRED: Credit Card Bills & More",
        'Package_name' : "com.dreamplug.androidapp",
        'installs' : "10,000,000+",
        'version' : "3.0.8.6",
    },
    'com.sega.chainchronicle' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "\u30c1\u30a7\u30a4\u30f3\u30af\u30ed\u30cb\u30af\u30eb \u30c1\u30a7\u30a4\u30f3\u30b7\u30ca\u30ea\u30aa\u738b\u9053\u30d0\u30c8\u30ebRPG",
        'Package_name' : "com.sega.chainchronicle",
        'installs' : "1,000,000+",
        'version' : "4.3.2",
    },
    'com.rzxy.ssjj' :
    {
        'category' : "Games",
        'updated' : "30 Nov 2020",
        'App_name' : "\u5fcd\u8005\u5951\u7d04",
        'Package_name' : "com.rzxy.ssjj",
        'installs' : "100,000+",
        'version' : "9.1",
    },
    'br.com.bancopan.cartoes' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "Banco PAN: cart\u00e3o sem anuidade",
        'Package_name' : "br.com.bancopan.cartoes",
        'installs' : "10,000,000+",
        'version' : "2.44.8",
    },
    'com.sportsbookbetonsports' :
    {
        'category' : "Games",
        'updated' : "05 Oct 2021",
        'App_name' : "Bet On Sports the Sportsbook Betting Freeplay App",
        'Package_name' : "com.sportsbookbetonsports",
        'installs' : "100,000+",
        'version' : "Varies with device",
    },
    'com.indie.zlby.google.jp' :
    {
        'category' : "Games",
        'updated' : "02 Dec 2020",
        'App_name' : "\u9f8d\u306e\u8987\u696d -\u9032\u6483\u306e\u30d2\u30fc\u30ed\u30fc\u30ba-",
        'Package_name' : "com.indie.zlby.google.jp",
        'installs' : "10,000+",
        'version' : "1.1.0",
    },
    'com.msf.kbank.mobile' :
    {
        'category' : "Finance",
        'updated' : "26 Apr 2022",
        'App_name' : "Kotak - 811 & Mobile Banking",
        'Package_name' : "com.msf.kbank.mobile",
        'installs' : "10,000,000+",
        'version' : "5.3.5",
    },
    'com.icicibank.ibizz' :
    {
        'category' : "Finance",
        'updated' : "23 May 2022",
        'App_name' : "InstaBIZ",
        'Package_name' : "com.icicibank.ibizz",
        'installs' : "1,000,000+",
        'version' : "7.0",
    },
    'com.amazon.avod.thirdpartyclient' :
    {
        'category' : "Entertainment",
        'updated' : "10 Jun 2022",
        'App_name' : "Amazon Prime Video",
        'Package_name' : "com.amazon.avod.thirdpartyclient",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'jp.co.yoozoo.stellaarcana' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "\u30b9\u30c6\u30e9\u30a2\u30eb\u30ab\u30ca \u611b\u306e\u5149\u3068\u904b\u547d\u306e\u7d46",
        'Package_name' : "jp.co.yoozoo.stellaarcana",
        'installs' : "100,000+",
        'version' : "0.13.1700",
    },
    'com.kkii' :
    {
        'category' : "Finance",
        'updated' : "31 May 2022",
        'App_name' : "PinjamYuk - Pinjaman Uang Aman",
        'Package_name' : "com.kkii",
        'installs' : "5,000,000+",
        'version' : "1.9.1",
    },
    '983826477' :
    {
        'category' : "Tools",
        'updated' : "2022-05-01T09:03:52Z",
        'App_name' : "Productive - Habit Tracker",
        'Package_name' : "com.beHappy.Productive",
        'version' : "3.4.1",
        'minimumOsVersion' : "13.5",
    },
    'jp.co.ncjapan.janryumon' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "\u30ea\u30a2\u30eb\u9ebb\u96c0 \u96c0\u9f8d\u9580M [\u9ebb\u96c0\u30b2\u30fc\u30e0]",
        'Package_name' : "jp.co.ncjapan.janryumon",
        'installs' : "100,000+",
        'version' : "5.0.8",
    },
    'com.mpwr.selfcare' :
    {
        'category' : "Lifestyle",
        'updated' : "12 May 2022",
        'App_name' : "MPWR - Digital Telco",
        'Package_name' : "com.mpwr.selfcare",
        'installs' : "1,000,000+",
        'version' : "1.9.35",
    },
    'com.eyougame.xlfz' :
    {
        'category' : "Games",
        'updated' : "07 Dec 2021",
        'App_name' : "\u738b\u570b\u5c0d\u6c7a\uff1a\u7121\u76e1\u7d1b\u722d",
        'Package_name' : "com.eyougame.xlfz",
        'installs' : "1,000+",
        'version' : "1.5",
    },
    'com.indie.jl.kr' :
    {
        'category' : "Games",
        'updated' : "15 Nov 2019",
        'App_name' : "\uac15\ub9bc : \ub9dd\ub839\uc778\ub3c4\uc790",
        'Package_name' : "com.indie.jl.kr",
        'installs' : "500,000+",
        'version' : "3.4.0",
    },
    'com.day1ent.bloodchaos' :
    {
        'category' : "Games",
        'updated' : "22 Apr 2021",
        'App_name' : "\ube14\ub7ec\ub4dc\uce74\uc624\uc2a4 : \uac01\uc131",
        'Package_name' : "com.day1ent.bloodchaos",
        'installs' : "50,000+",
        'version' : "2.5085.22233",
    },
    'com.neobazar.webcomics' :
    {
        'category' : "Education and Books",
        'updated' : "16 Jun 2021",
        'App_name' : "kakaopage - Webtoon Original",
        'Package_name' : "com.neobazar.webcomics",
        'installs' : "5,000,000+",
        'version' : "3.4.8",
    },
    '852801905' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-05-30T04:25:29Z",
        'App_name' : "\u30bf\u30c3\u30d7\u30eb-\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea\u3067\u604b\u6d3b/\u5a5a\u6d3b",
        'Package_name' : "com.matchingagent.tapple",
        'version' : "8.1.13",
        'minimumOsVersion' : "13.0",
    },
    'com.ziptoss.v2' :
    {
        'category' : "House & Home",
        'updated' : "07 Jun 2022",
        'App_name' : "Ziptoss",
        'Package_name' : "com.ziptoss.v2",
        'installs' : "100,000+",
        'version' : "6.40.2",
    },
    'com.grofers.customerapp' :
    {
        'category' : "Food & Drink",
        'updated' : "16 Jun 2022",
        'App_name' : "Blinkit (formerly grofers)",
        'Package_name' : "com.grofers.customerapp",
        'installs' : "10,000,000+",
        'version' : "13.11.2",
    },
    'com.cowon.dragonrajaEX' :
    {
        'category' : "Games",
        'updated' : "29 Mar 2022",
        'App_name' : "\ub4dc\ub798\uace4\ub77c\uc790EX",
        'Package_name' : "com.cowon.dragonrajaEX",
        'installs' : "100,000+",
        'version' : "1.0.26",
    },
    'io.connectcourses.app' :
    {
        'category' : "Education and Books",
        'updated' : "09 Jun 2022",
        'App_name' : "FrontRow: Learn What You Love!",
        'Package_name' : "io.connectcourses.app",
        'installs' : "1,000,000+",
        'version' : "1.14.0",
    },
    'jp.wifishare.townwifi' :
    {
        'category' : "Social and Communication",
        'updated' : "01 Jun 2022",
        'App_name' : "TownWiFi by GMO",
        'Package_name' : "jp.wifishare.townwifi",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.fusionmedia.investing' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Investing.com: Stocks & News",
        'Package_name' : "com.fusionmedia.investing",
        'installs' : "10,000,000+",
        'version' : "6.10.11.1",
    },
    'com.elex.twdsaw.gp' :
    {
        'category' : "Games",
        'updated' : "11 Jun 2022",
        'App_name' : "The Walking Dead: Survivors",
        'Package_name' : "com.elex.twdsaw.gp",
        'installs' : "10,000,000+",
        'version' : "3.4.4",
    },
    'com.datehar.dateharapp' :
    {
        'category' : "Shopping",
        'updated' : "06 Sep 2021",
        'App_name' : "Datehar-Fashion Shopping",
        'Package_name' : "com.datehar.dateharapp",
        'installs' : "50,000+",
        'version' : "1.3.1",
    },
    '297606951' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-09T16:03:53Z",
        'App_name' : "Amazon - Shopping made easy",
        'Package_name' : "com.amazon.Amazon",
        'version' : "19.12.2",
        'minimumOsVersion' : "13.0",
    },
    'com.ariel.sow' :
    {
        'category' : "Games",
        'updated' : "26 Sep 2021",
        'App_name' : "\uc804\uc7c1\uc758 \uc5f0\uac00: \uc804\uc5f0 - \uc5ec\uc2e0\uc758 \ubd80\ud65c",
        'Package_name' : "com.ariel.sow",
        'installs' : "100,000+",
        'version' : "1.2.0.0",
    },
    'ng.com.fairmoney.fairmoney' :
    {
        'category' : "Finance",
        'updated' : "01 Jun 2022",
        'App_name' : "Loans with FairMoney Loan App",
        'Package_name' : "ng.com.fairmoney.fairmoney",
        'installs' : "5,000,000+",
        'version' : "8.79.0",
    },
    'com.DefaultCompany.BlackCrown' :
    {
        'category' : "Games",
        'updated' : "17 Jun 2022",
        'App_name' : "\uac80\uc740\uc655\uad00 \uba54\uae30\uc655\uc758\ubd84\ub178",
        'Package_name' : "com.DefaultCompany.BlackCrown",
        'installs' : "100,000+",
        'version' : "2.1.159",
    },
    'com.destruction.fruitmaster.pp' :
    {
        'category' : "Games",
        'updated' : "29 Oct 2021",
        'App_name' : "Fruit Master - Adventure Spin & Coin Master Saga",
        'Package_name' : "com.destruction.fruitmaster.pp",
        'installs' : "100,000+",
        'version' : "1.2.52",
    },
    'com.goh.daya.ydonline' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "\uac13 \uc624\ube0c \ud558\uc774\uc2a4\ucfe8",
        'Package_name' : "com.goh.daya.ydonline",
        'installs' : "1,000,000+",
        'version' : "5.3.2",
    },
    'com.octazone' :
    {
        'category' : "Health and Fitness",
        'updated' : "03 Jun 2022",
        'App_name' : "Home Workout by OctaZone",
        'Package_name' : "com.octazone",
        'installs' : "500,000+",
        'version' : "1.6.1",
    },
    'com.jungleegames.rummy' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "Rummy Card Game @Junglee Rummy",
        'Package_name' : "com.jungleegames.rummy",
        'installs' : "10,000,000+",
        'version' : "3.0.7",
    },
    'com.hbo.hbonow' :
    {
        'category' : "Entertainment",
        'updated' : "08 Jun 2022",
        'App_name' : "HBO Max: Stream TV & Movies",
        'Package_name' : "com.hbo.hbonow",
        'installs' : "50,000,000+",
        'version' : "Varies with device",
    },
    'com.hanbit_t3.android_googleplay.RANBUkr' :
    {
        'category' : "Games",
        'updated' : "24 Mar 2021",
        'App_name' : "\uc0bc\uad6d\uc9c0\ub09c\ubb34",
        'Package_name' : "com.hanbit_t3.android_googleplay.RANBUkr",
        'installs' : "100,000+",
        'version' : "1.1.41",
    },
    'com.cyou.freestyle2.gp' :
    {
        'category' : "Games",
        'updated' : "25 Mar 2022",
        'App_name' : "\ud504\ub9ac\uc2a4\ud0c0\uc77c2: \ub808\ubcfc\ub8e8\uc158 \ud50c\ub77c\uc789\ub369\ud06c",
        'Package_name' : "com.cyou.freestyle2.gp",
        'installs' : "100,000+",
        'version' : "1.1.1",
    },
    '1454477270' :
    {
        'category' : "Games",
        'updated' : "2022-06-08T21:03:14Z",
        'App_name' : "V4",
        'Package_name' : "com.nexon.v4kr",
        'version' : "1.36.440405",
        'minimumOsVersion' : "12.0",
    },
    'com.vidangel.thechosen' :
    {
        'category' : "Entertainment",
        'updated' : "02 Jun 2022",
        'App_name' : "The Chosen: Stream the Series",
        'Package_name' : "com.vidangel.thechosen",
        'installs' : "10,000,000+",
        'version' : "2.2.77",
    },
    'de.hochbahn.hvvswitch' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "13 Jun 2022",
        'App_name' : "hvv switch - Hamburg mobility",
        'Package_name' : "de.hochbahn.hvvswitch",
        'installs' : "100,000+",
        'version' : "1.9.2",
    },
    'empiricus.com.br.empiricus' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Empiricus Research",
        'Package_name' : "empiricus.com.br.empiricus",
        'installs' : "500,000+",
        'version' : "4.7.8",
    },
    'com.egame.zsxy' :
    {
        'category' : "Games",
        'updated' : "12 Apr 2021",
        'App_name' : "\u8af8\u795e\u5b78\u9662Smash",
        'Package_name' : "com.egame.zsxy",
        'installs' : "100,000+",
        'version' : "1.0.21",
    },
    'ru.handh.petrovich' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "\u041f\u0435\u0442\u0440\u043e\u0432\u0438\u0447\u2014\u0434\u043b\u044f \u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0438 \u0440\u0435\u043c\u043e\u043d\u0442\u0430",
        'Package_name' : "ru.handh.petrovich",
        'installs' : "500,000+",
        'version' : "1.27.2",
    },
    'ru.litres.android' :
    {
        'category' : "Education and Books",
        'updated' : "06 Jun 2022",
        'App_name' : "LitRes: Read and listen",
        'Package_name' : "ru.litres.android",
        'installs' : "10,000,000+",
        'version' : "3.56.0-gp",
    },
    'com.deviwear.byapps' :
    {
        'category' : "Shopping",
        'updated' : "04 Nov 2021",
        'App_name' : "\ub370\ube44\uc6e8\uc5b4",
        'Package_name' : "com.deviwear.byapps",
        'installs' : "5,000+",
        'version' : "1.3",
    },
    'com.vng.thieunien3qvng' :
    {
        'category' : "Games",
        'updated' : "21 Sep 2021",
        'App_name' : "Thi\u1ebfu Ni\u00ean 3Q - VNG: Tam Qu\u1ed1c Chi\u1ebfn Thu\u1eadt",
        'Package_name' : "com.vng.thieunien3qvng",
        'installs' : "500,000+",
        'version' : "1.3.82",
    },
    'com.agoda.mobile.consumer' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "14 Jun 2022",
        'App_name' : "Agoda",
        'Package_name' : "com.agoda.mobile.consumer",
        'installs' : "10,000,000+",
        'version' : "10.23.0",
    },
    'com.communityshaadi.android' :
    {
        'category' : "Social and Communication",
        'updated' : "22 Apr 2022",
        'App_name' : "Sangam.com: Matrimony App",
        'Package_name' : "com.communityshaadi.android",
        'installs' : "1,000,000+",
        'version' : "2.6.0",
    },
    '1485526957' :
    {
        'category' : "Games",
        'updated' : "2022-06-14T05:09:23Z",
        'App_name' : "\u5b88\u671b\u50b3\u8aaa Guardian Tales",
        'Package_name' : "com.kakaogames.gdts",
        'version' : "2.44.0",
        'minimumOsVersion' : "11.0",
    },
    'id.astra.adp.movic' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "08 Jun 2022",
        'App_name' : "Movic - Solusi Rental Mobil",
        'Package_name' : "id.astra.adp.movic",
        'installs' : "1,000,000+",
        'version' : "3.6.0",
    },
    'com.globalegrow.app.dresslily' :
    {
        'category' : "Shopping",
        'updated' : "02 Jun 2022",
        'App_name' : "Dresslily-Fashion Trend",
        'Package_name' : "com.globalegrow.app.dresslily",
        'installs' : "1,000,000+",
        'version' : "7.2.2",
    },
    'blibli.mobile.commerce' :
    {
        'category' : "Shopping",
        'updated' : "12 Apr 2022",
        'App_name' : "Blibli Ramadan Pasti Berkah",
        'Package_name' : "blibli.mobile.commerce",
        'installs' : "10,000,000+",
        'version' : "8.7.0",
    },
    'com.DG.rz3.google' :
    {
        'category' : "Games",
        'updated' : "21 Sep 2020",
        'App_name' : "\ub77c\uc2a4\ud2b8 \uc250\ub3c4\uc6b03 :\uc2e0\uc138\uacc4",
        'Package_name' : "com.DG.rz3.google",
        'installs' : "10,000+",
        'version' : "2.5",
    },
    'com.lulu.luluone' :
    {
        'category' : "Finance",
        'updated' : "17 Jun 2022",
        'App_name' : "Lulu Money- Send Money, Instant Money Transfer",
        'Package_name' : "com.lulu.luluone",
        'installs' : "500,000+",
        'version' : "3.0.43",
    },
    'com.interpark.shop' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "\uc778\ud130\ud30c\ud06c",
        'Package_name' : "com.interpark.shop",
        'installs' : "5,000,000+",
        'version' : "4.8.2",
    },
    'com.icon.pln123' :
    {
        'category' : "Finance",
        'updated' : "03 Jun 2022",
        'App_name' : "PLN Mobile",
        'Package_name' : "com.icon.pln123",
        'installs' : "10,000,000+",
        'version' : "5.2.24",
    },
    'tv.pluto.android' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "Pluto TV - Live TV and Movies",
        'Package_name' : "tv.pluto.android",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.flowe.flowe' :
    {
        'category' : "Finance",
        'updated' : "06 Jun 2022",
        'App_name' : "FLOWE \u2013 Pay better, be better",
        'Package_name' : "com.flowe.flowe",
        'installs' : "1,000,000+",
        'version' : "0.59.220606.1",
    },
    'jp.gamewith.gamewith' :
    {
        'category' : "Entertainment",
        'updated' : "13 May 2022",
        'App_name' : "GameWith \u30b2\u30fc\u30e0\u30a6\u30a3\u30ba",
        'Package_name' : "jp.gamewith.gamewith",
        'installs' : "1,000,000+",
        'version' : "2.9.46",
    },
    'com.myntra.android' :
    {
        'category' : "Shopping",
        'updated' : "11 May 2022",
        'App_name' : "Myntra - Fashion Shopping App",
        'Package_name' : "com.myntra.android",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.korea.qyj.google' :
    {
        'category' : "Games",
        'updated' : "12 Oct 2021",
        'App_name' : "\uce74\ub974\ub9c8M",
        'Package_name' : "com.korea.qyj.google",
        'installs' : "100,000+",
        'version' : "1.0.9",
    },
    'boyaa.slots.en' :
    {
        'category' : "Casino",
        'updated' : "08 Dec 2021",
        'App_name' : "Boyaa Slots",
        'Package_name' : "boyaa.slots.en",
        'installs' : "5,000+",
        'version' : "1.7.0",
    },
    'de.tvsmiles.app' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "TVSMILES",
        'Package_name' : "de.tvsmiles.app",
        'installs' : "1,000,000+",
        'version' : "7.8.1",
    },
    'mop.thglob.glaoebxdwesa' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "Mini Trade - Mobile Trade App",
        'Package_name' : "mop.thglob.glaoebxdwesa",
        'installs' : "500,000+",
        'version' : "1.0.8",
    },
    'com.eyougame.crescent' :
    {
        'category' : "Games",
        'updated' : "14 Aug 2021",
        'App_name' : "\ud06c\ub808\uc13c\ud2b8",
        'Package_name' : "com.eyougame.crescent",
        'installs' : "100,000+",
        'version' : "19.0",
    },
    'kr.co.peaplecar' :
    {
        'category' : "Utilities",
        'updated' : "17 Jun 2022",
        'App_name' : "\ud53c\ud50c\uce74 - \ucc29\ud55c\uac00\uaca9, \ucc29\ud55c\uce74\uc170\uc5b4\ub9c1",
        'Package_name' : "kr.co.peaplecar",
        'installs' : "500,000+",
        'version' : "1.15.15",
    },
    'com.indeed.android.jobsearch' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "Indeed Job Search",
        'Package_name' : "com.indeed.android.jobsearch",
        'installs' : "100,000,000+",
        'version' : "111.0",
    },
    'in.rebase.app' :
    {
        'category' : "Finance",
        'updated' : "02 Jun 2022",
        'App_name' : "SmartCoin - Personal Loan App",
        'Package_name' : "in.rebase.app",
        'installs' : "5,000,000+",
        'version' : "3.0.2",
    },
    'ctrip.english' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "16 Jun 2022",
        'App_name' : "Trip.com: Book Flights, Hotels",
        'Package_name' : "ctrip.english",
        'installs' : "10,000,000+",
        'version' : "7.54.4",
    },
    '1340093871' :
    {
        'category' : "Food & Drink",
        'updated' : "2021-11-29T09:58:12Z",
        'App_name' : "Shawarmer",
        'Package_name' : "com.shawarmer",
        'version' : "2.9",
        'minimumOsVersion' : "12.1",
    },
    'jp.colopl.tennis' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "\u767d\u732b\u30c6\u30cb\u30b9",
        'Package_name' : "jp.colopl.tennis",
        'installs' : "1,000,000+",
        'version' : "2.2.33",
    },
    'com.livebanner.cityscoot' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "08 Jun 2022",
        'App_name' : "Cityscoot",
        'Package_name' : "com.livebanner.cityscoot",
        'installs' : "500,000+",
        'version' : "3.6.20",
    },
    '585661281' :
    {
        'category' : "Games",
        'updated' : "2022-06-03T08:26:31Z",
        'App_name' : "Empire Four Kingdoms - MMO War",
        'Package_name' : "com.goodgamestudios.empirefourkingdoms",
        'version' : "4.35.35",
        'minimumOsVersion' : "10.0",
    },
    'com.edreams.travel' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "16 Jun 2022",
        'App_name' : "eDreams: Book cheap flights",
        'Package_name' : "com.edreams.travel",
        'installs' : "10,000,000+",
        'version' : "4.462.0",
    },
    'jp.cocone.fukyou' :
    {
        'category' : "Social and Communication",
        'updated' : "08 Sep 2021",
        'App_name' : "#\u79c1\u3092\u5e03\u6559\u3057\u3066\u300c\u30e9\u30a4\u30d6\u914d\u4fe1\u300d\u3067\u7e4b\u304c\u308b \u30c8\u30fc\u30af\u30a2\u30d7\u30ea",
        'Package_name' : "jp.cocone.fukyou",
        'installs' : "50,000+",
        'version' : "2.40.11",
    },
    'com.welovesupermom.voices' :
    {
        'category' : "Health and Fitness",
        'updated' : "31 May 2022",
        'App_name' : "Supermom",
        'Package_name' : "com.welovesupermom.voices",
        'installs' : "10,000+",
        'version' : "2.1.2",
    },
    'kr.co.saramin.jumpit' :
    {
        'category' : "Finance",
        'updated' : "09 Jun 2022",
        'App_name' : "\uc810\ud54f - \uac1c\ubc1c\uc790 \ucee4\ub9ac\uc5b4 \uc810\ud504",
        'Package_name' : "kr.co.saramin.jumpit",
        'installs' : "100,000+",
        'version' : "1.1.17",
    },
    'com.fastjobs.asia' :
    {
        'category' : "Finance",
        'updated' : "25 May 2022",
        'App_name' : "FastJobs - Get Jobs Fast",
        'Package_name' : "com.fastjobs.asia",
        'installs' : "1,000,000+",
        'version' : "4.9.0",
    },
    'com.radio.pocketfm' :
    {
        'category' : "Entertainment",
        'updated' : "10 Jun 2022",
        'App_name' : "Pocket FM: Audiobook & Podcast",
        'Package_name' : "com.radio.pocketfm",
        'installs' : "50,000,000+",
        'version' : "5.6.2",
    },
    'app.neolife.ebuzz' :
    {
        'category' : "Entertainment",
        'updated' : "14 Mar 2022",
        'App_name' : "eBuzz - World of entertainment",
        'Package_name' : "app.neolife.ebuzz",
        'installs' : "1,000+",
        'version' : "9.2.1",
    },
    'com.dmm.games.fantasiarebuild' :
    {
        'category' : "Games",
        'updated' : "08 Dec 2021",
        'App_name' : "\u30d5\u30a1\u30f3\u30bf\u30b8\u30a2\u30fb\u30ea\u30d3\u30eb\u30c9",
        'Package_name' : "com.dmm.games.fantasiarebuild",
        'installs' : "50,000+",
        'version' : "1.2.53",
    },
    '1482718980' :
    {
        'category' : "Games",
        'updated' : "2020-10-08T23:01:25Z",
        'App_name' : "\u5b9f\u969b\u306b\u3042\u3063\u305f\u304f\u3060\u3089\u306a\u3044\u4e8b\u4ef6\u7c3f",
        'Package_name' : "com.mask.kudarang",
        'version' : "1.0.4",
        'minimumOsVersion' : "10",
    },
    'com.platb.curi' :
    {
        'category' : "Education and Books",
        'updated' : "04 Feb 2022",
        'App_name' : "CURI \u2013 \uc218\ud559\ubb38\uc81c\ud480\uc774 \uc571",
        'Package_name' : "com.platb.curi",
        'installs' : "100,000+",
        'version' : "1.3.13",
    },
    'jp.gungho.ragnarokm' :
    {
        'category' : "Games",
        'updated' : "16 Mar 2022",
        'App_name' : "\u30e9\u30b0\u30ca\u30ed\u30af \u30de\u30b9\u30bf\u30fc\u30ba (\u30e9\u30b0\u30de\u30b9) -\u672c\u683cMMORPG-",
        'Package_name' : "jp.gungho.ragnarokm",
        'installs' : "100,000+",
        'version' : "1.7.0",
    },
    '1540969222' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-17T05:19:48Z",
        'App_name' : "Azadea: Fashion & more",
        'Package_name' : "com.azadea.azadea",
        'version' : "2.2",
        'minimumOsVersion' : "13.0",
    },
    '1445631112' :
    {
        'category' : "Games",
        'updated' : "2022-05-24T15:02:53Z",
        'App_name' : "Lineage M(\u30ea\u30cd\u30fc\u30b8\u30e5M)",
        'Package_name' : "jp.co.ncjapan.lineagem",
        'version' : "1.3.2",
        'minimumOsVersion' : "9.0",
    },
    'com.upst.hayu' :
    {
        'category' : "Entertainment",
        'updated' : "18 May 2022",
        'App_name' : "Hayu - Watch Reality TV",
        'Package_name' : "com.upst.hayu",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.clickastro.freehoroscope.astrology' :
    {
        'category' : "Lifestyle",
        'updated' : "18 Jun 2022",
        'App_name' : "Clickastro Kundli : Astrology",
        'Package_name' : "com.clickastro.freehoroscope.astrology",
        'installs' : "500,000+",
        'version' : "2.2.8.9",
    },
    'com.medrick.match3' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "\u06af\u0644\u0645\u0631\u0627\u062f - \u0628\u0627\u0632\u06cc \u062c\u0648\u0631\u0686\u06cc\u0646",
        'Package_name' : "com.medrick.match3",
        'installs' : "1,000,000+",
        'version' : "2.00",
    },
    'slots.pcg.casino.games.free.android' :
    {
        'category' : "Casino",
        'updated' : "17 Jun 2022",
        'App_name' : "Cash Frenzy\u2122 - Casino Slots",
        'Package_name' : "slots.pcg.casino.games.free.android",
        'installs' : "10,000,000+",
        'version' : "2.57",
    },
    'br.com.livup.ecommerce' :
    {
        'category' : "Food & Drink",
        'updated' : "14 Jun 2022",
        'App_name' : "Liv Up \u2013 Entrega de Comida Saud\u00e1vel",
        'Package_name' : "br.com.livup.ecommerce",
        'installs' : "1,000,000+",
        'version' : "41.1.0",
    },
    'com.wondergames.warpath.gp' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "Warpath: Ace Shooter",
        'Package_name' : "com.wondergames.warpath.gp",
        'installs' : "10,000,000+",
        'version' : "4.32.10",
    },
    'com.dobai.kis' :
    {
        'category' : "Entertainment",
        'updated' : "10 Jun 2022",
        'App_name' : "Ahlan-Group Voice Chat Room",
        'Package_name' : "com.dobai.kis",
        'installs' : "1,000,000+",
        'version' : "3.1.4",
    },
    'com.showroom.smash' :
    {
        'category' : "Entertainment",
        'updated' : "07 Jun 2022",
        'App_name' : "smash. - Vertical Theater",
        'Package_name' : "com.showroom.smash",
        'installs' : "500,000+",
        'version' : "2.1.1",
    },
    '1543672323' :
    {
        'category' : "Games",
        'updated' : "2022-06-15T04:34:23Z",
        'App_name' : "\u76fe\u306e\u52c7\u8005\u306e\u6210\u308a\u4e0a\u304c\u308a RERISE",
        'Package_name' : "jp.shieldhero-game",
        'version' : "1.8.6",
        'minimumOsVersion' : "12.0",
    },
    'com.showtimeapp' :
    {
        'category' : "Entertainment",
        'updated' : "09 Jun 2022",
        'App_name' : "Loco : Live Game Streaming",
        'Package_name' : "com.showtimeapp",
        'installs' : "10,000,000+",
        'version' : "5.4.17",
    },
    'com.gyantech.pagarbook' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "PagarBook:Attendance & Payroll",
        'Package_name' : "com.gyantech.pagarbook",
        'installs' : "5,000,000+",
        'version' : "1.23.2",
    },
    'ca.blood.giveblood' :
    {
        'category' : "Health and Fitness",
        'updated' : "16 May 2022",
        'App_name' : "GiveBlood",
        'Package_name' : "ca.blood.giveblood",
        'installs' : "100,000+",
        'version' : "3.0.2",
    },
    'com.yatrirailways.yatri' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "10 May 2022",
        'App_name' : "YATRI - Mumbai Local App.",
        'Package_name' : "com.yatrirailways.yatri",
        'installs' : "100,000+",
        'version' : "1.15.2",
    },
    'com.dreamstudio.word.travel.scapes' :
    {
        'category' : "Games",
        'updated' : "18 Feb 2021",
        'App_name' : "Word Relax - Free Word Games & Puzzles",
        'Package_name' : "com.dreamstudio.word.travel.scapes",
        'installs' : "1,000,000+",
        'version' : "1.0.73",
    },
    'com.laposte.pemo' :
    {
        'category' : "Entertainment",
        'updated' : "24 Mar 2022",
        'App_name' : "Youpix \u2013 carte postale et timbre photo",
        'Package_name' : "com.laposte.pemo",
        'installs' : "100,000+",
        'version' : "3.0.3",
    },
    'cn.puffingames.aog' :
    {
        'category' : "Games",
        'updated' : "29 Jul 2021",
        'App_name' : "Age of Myth Genesis",
        'Package_name' : "cn.puffingames.aog",
        'installs' : "1,000,000+",
        'version' : "2.1.30",
    },
    '380974668' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-15T08:05:47Z",
        'App_name' : "Vestiaire Collective",
        'Package_name' : "com.vestiairedecopines.vestiaire",
        'version' : "5.84.2",
        'minimumOsVersion' : "14.0",
    },
    'com.kreyougame.gods' :
    {
        'category' : "Games",
        'updated' : "14 May 2020",
        'App_name' : "\ud2f0\ub974: \uc885\ub9d0\uc758 \uc2dc\uc791",
        'Package_name' : "com.kreyougame.gods",
        'installs' : "10,000+",
        'version' : "18",
    },
    'com.coinmena.coinmenaapp' :
    {
        'category' : "Finance",
        'updated' : "02 Jun 2022",
        'App_name' : "CoinMENA: Buy Bitcoin Now",
        'Package_name' : "com.coinmena.coinmenaapp",
        'installs' : "100,000+",
        'version' : "1.6.0",
    },
    'com.gs2.omg' :
    {
        'category' : "Games",
        'updated' : "12 Oct 2021",
        'App_name' : "OMG 3Q \u2013 \u0110\u1ea5u t\u01b0\u1edbng chi\u1ebfn thu\u1eadt c\u1ef1c m\u1ea1nh",
        'Package_name' : "com.gs2.omg",
        'installs' : "1,000,000+",
        'version' : "1.0.31",
    },
    'com.nexon.mdnfteen' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\ub358\uc804\uc564\ud30c\uc774\ud130 \ubaa8\ubc14\uc77c(12)",
        'Package_name' : "com.nexon.mdnfteen",
        'installs' : "50,000+",
        'version' : "3.5.1",
    },
    'com.bukalapak.mitra' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "Mitra Bukalapak: Pulsa, Grosir",
        'Package_name' : "com.bukalapak.mitra",
        'installs' : "5,000,000+",
        'version' : "1.82.0",
    },
    'com.joyreading.wehear' :
    {
        'category' : "Education and Books",
        'updated' : "14 Jun 2022",
        'App_name' : "Wehear - Audiobooks & Stories",
        'Package_name' : "com.joyreading.wehear",
        'installs' : "500,000+",
        'version' : "1.7.0",
    },
    'com.bitazza.android' :
    {
        'category' : "Finance",
        'updated' : "06 Jun 2022",
        'App_name' : "Bitazza TH: Crypto Exchange",
        'Package_name' : "com.bitazza.android",
        'installs' : "100,000+",
        'version' : "2.3.1",
    },
    'com.aia.rn.th.dp01' :
    {
        'category' : "Health and Fitness",
        'updated' : "03 Jun 2022",
        'App_name' : "ALive Powered by AIA",
        'Package_name' : "com.aia.rn.th.dp01",
        'installs' : "1,000,000+",
        'version' : "1.32.0",
    },
    'com.bharatmatrimony' :
    {
        'category' : "Social and Communication",
        'updated' : "21 May 2022",
        'App_name' : "Bharat Matrimony\u00ae - Shaadi App",
        'Package_name' : "com.bharatmatrimony",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.plarium.raidlegends' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "RAID: Shadow Legends",
        'Package_name' : "com.plarium.raidlegends",
        'installs' : "10,000,000+",
        'version' : "5.60.2",
    },
    'com.once.android' :
    {
        'category' : "Social and Communication",
        'updated' : "20 May 2022",
        'App_name' : "Once - Quality dating for singles",
        'Package_name' : "com.once.android",
        'installs' : "5,000,000+",
        'version' : "3.70",
    },
    'com.bandainamcoent.torays' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "\u30c6\u30a4\u30eb\u30ba \u30aa\u30d6 \u30b6 \u30ec\u30a4\u30ba",
        'Package_name' : "com.bandainamcoent.torays",
        'installs' : "500,000+",
        'version' : "4.13.0",
    },
    'com.nhnent.SKQUEST' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "Crusaders Quest",
        'Package_name' : "com.nhnent.SKQUEST",
        'installs' : "5,000,000+",
        'version' : "6.10.0.KG",
    },
    'com.ahing.korean' :
    {
        'category' : "Social and Communication",
        'updated' : "31 May 2022",
        'App_name' : "Ahing - Meeting Vietnamese",
        'Package_name' : "com.ahing.korean",
        'installs' : "100,000+",
        'version' : "2.1.7",
    },
    'jp.co.beeworks.mushroomWorld' :
    {
        'category' : "Games",
        'updated' : "01 Jun 2022",
        'App_name' : "\u306a\u3081\u3053\u683d\u57f9\u30ad\u30c3\u30c8 \u30b6\u30fb\u30ef\u30fc\u30eb\u30c9",
        'Package_name' : "jp.co.beeworks.mushroomWorld",
        'installs' : "500,000+",
        'version' : "1.49.0",
    },
    'com.melorra' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "Melorra Jewellery Shopping App",
        'Package_name' : "com.melorra",
        'installs' : "1,000,000+",
        'version' : "3.5.6",
    },
    'in.co.sportsapp.sportsapp' :
    {
        'category' : "Social and Communication",
        'updated' : "07 Mar 2022",
        'App_name' : "SportsApp",
        'Package_name' : "in.co.sportsapp.sportsapp",
        'installs' : "100,000+",
        'version' : "2022.2.2-release",
    },
    'com.lamoda.lite' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "Lamoda \u0438\u043d\u0442\u0435\u0440\u043d\u0435\u0442 \u043c\u0430\u0433\u0430\u0437\u0438\u043d \u043e\u0434\u0435\u0436\u0434\u044b",
        'Package_name' : "com.lamoda.lite",
        'installs' : "10,000,000+",
        'version' : "4.8.1",
    },
    'com.saudi.riyadh.seasons' :
    {
        'category' : "Entertainment",
        'updated' : "03 Jun 2022",
        'App_name' : "\u0645\u0648\u0633\u0645 \u062c\u062f\u0629 | Jeddah season",
        'Package_name' : "com.saudi.riyadh.seasons",
        'installs' : "100,000+",
        'version' : "2.0.32",
    },
    'com.brandicorp.brandi3' :
    {
        'category' : "Shopping",
        'updated' : "07 Jun 2022",
        'App_name' : "\ube0c\ub79c\ub514 - \uc5ec\uc131 \ud328\uc158 \uc1fc\ud551\uc571",
        'Package_name' : "com.brandicorp.brandi3",
        'installs' : "5,000,000+",
        'version' : "4.3.0.03",
    },
    '1509010274' :
    {
        'category' : "Games",
        'updated' : "2020-08-28T09:34:15Z",
        'App_name' : "\u4e09\u56fd\u82f1\u96c4\u305f\u3061\u306e\u591c\u660e\u3051",
        'Package_name' : "com.global.kingdom.ios",
        'version' : "1.3",
        'minimumOsVersion' : "10.0",
    },
    'com.demaecan.androidapp' :
    {
        'category' : "Food & Drink",
        'updated' : "06 Jun 2022",
        'App_name' : "\u51fa\u524d\u9928",
        'Package_name' : "com.demaecan.androidapp",
        'installs' : "1,000,000+",
        'version' : "16.0.0",
    },
    'com.prontoitlabs.hunted' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "SONIC JOBS Job Search",
        'Package_name' : "com.prontoitlabs.hunted",
        'installs' : "500,000+",
        'version' : "1.3.63",
    },
    'com.lilithgame.roc.gp' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "Rise of Kingdoms: Lost Crusade",
        'Package_name' : "com.lilithgame.roc.gp",
        'installs' : "50,000,000+",
        'version' : "1.0.58.19",
    },
    '1228254189' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-12T11:49:10Z",
        'App_name' : "justclean",
        'Package_name' : "com.justclean.justclean",
        'version' : "5.5.0",
        'minimumOsVersion' : "13.0",
    },
    'com.enpgames.google.idlefantasy' :
    {
        'category' : "Games",
        'updated' : "25 Jun 2020",
        'App_name' : "Idle Fantasy Merge RPG: Legend of the Stars",
        'Package_name' : "com.enpgames.google.idlefantasy",
        'installs' : "50,000+",
        'version' : "1.3.5",
    },
    '1005442353' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-08T13:18:34Z",
        'App_name' : "\ube0c\ub79c\ub514 - \uc5ec\uc131 \ud328\uc158 \uc1fc\ud551\uc571",
        'Package_name' : "com.brandicorp.bradi",
        'version' : "2.30.3",
        'minimumOsVersion' : "13.0",
    },
    'com.qidian.Int.reader' :
    {
        'category' : "Education and Books",
        'updated' : "09 Jun 2022",
        'App_name' : "Webnovel",
        'Package_name' : "com.qidian.Int.reader",
        'installs' : "10,000,000+",
        'version' : "5.9.6",
    },
    'com.alice.renren.gp.us' :
    {
        'category' : "Games",
        'updated' : "22 Oct 2021",
        'App_name' : "Alice Closet: Anime Dress Up",
        'Package_name' : "com.alice.renren.gp.us",
        'installs' : "100,000+",
        'version' : "1.0.8",
    },
    '1084930849' :
    {
        'category' : "Games",
        'updated' : "2022-06-14T03:53:02Z",
        'App_name' : "\u30ad\u30f3\u30b0\u30fb\u30aa\u30d6\u30fb\u30a2\u30d0\u30ed\u30f3: \u30d0\u30c8\u30eb\u6226\u4e89\u30ad\u30f3\u30b0\u30c0\u30e0\u306eRPG\u5bfe\u6226",
        'Package_name' : "com.diandian.kingofavalon",
        'version' : "13.7.0",
        'minimumOsVersion' : "11.0",
    },
    'com.jingdong.th.app' :
    {
        'category' : "Shopping",
        'updated' : "27 May 2022",
        'App_name' : "JD CENTRAL: SUPER JOY",
        'Package_name' : "com.jingdong.th.app",
        'installs' : "5,000,000+",
        'version' : "2.42.0",
    },
    'com.netmarble.mmatgo' :
    {
        'category' : "Games",
        'updated' : "12 May 2022",
        'App_name' : "\uc708\uc870\uc774 \ub300\ubc15 \ub9de\uace0 : \uc694\uc998 \ub300\uc138 \ub9de\uace0 \uace0\uc2a4\ud1b1",
        'Package_name' : "com.netmarble.mmatgo",
        'installs' : "100,000+",
        'version' : "1.8.17.4dfd72",
    },
    'co.il.strauss.group.consumers' :
    {
        'category' : "Lifestyle",
        'updated' : "30 May 2022",
        'App_name' : "\u05e9\u05d8\u05e8\u05d0\u05d5\u05e1+ \u05d4\u05d5\u05e4\u05db\u05d9\u05dd \u05e7\u05e0\u05d9\u05d5\u05ea \u05dc\u05de\u05ea\u05e0\u05d5\u05ea",
        'Package_name' : "co.il.strauss.group.consumers",
        'installs' : "500,000+",
        'version' : "7.20.0.584",
    },
    'com.bandainamcoent.gb_jp' :
    {
        'category' : "Games",
        'updated' : "13 Apr 2022",
        'App_name' : "\u30ac\u30f3\u30c0\u30e0\u30d6\u30ec\u30a4\u30ab\u30fc\u30e2\u30d0\u30a4\u30eb",
        'Package_name' : "com.bandainamcoent.gb_jp",
        'installs' : "500,000+",
        'version' : "3.04.00",
    },
    'com.gopatients.app' :
    {
        'category' : "Health and Fitness",
        'updated' : "24 Feb 2022",
        'App_name' : "goPatients",
        'Package_name' : "com.gopatients.app",
        'installs' : "500+",
        'version' : "1.1",
    },
    'com.faces.androidapp' :
    {
        'category' : "Shopping",
        'updated' : "27 Apr 2022",
        'App_name' : "FACES Beauty \u2013 \u0641\u064a\u0633\u0632",
        'Package_name' : "com.faces.androidapp",
        'installs' : "10,000+",
        'version' : "2.0",
    },
    'jp.co.ncjapan.atlas' :
    {
        'category' : "Games",
        'updated' : "22 Jun 2020",
        'App_name' : "\u932c\u795e\u306e\u30a2\u30b9\u30c8\u30e9\u30eb",
        'Package_name' : "jp.co.ncjapan.atlas",
        'installs' : "50,000+",
        'version' : "1.4.3",
    },
    'com.efun.krqq.google' :
    {
        'category' : "Games",
        'updated' : "23 Jul 2019",
        'App_name' : "\uc0bc\uad6d\uc9c0\ub77c\uc774\ube0c",
        'Package_name' : "com.efun.krqq.google",
        'installs' : "100,000+",
        'version' : "2.0.0",
    },
    'com.tannico.tannicoexpress' :
    {
        'category' : "Food & Drink",
        'updated' : "05 May 2022",
        'App_name' : "Tannico Express",
        'Package_name' : "com.tannico.tannicoexpress",
        'installs' : "50,000+",
        'version' : "1.3.7",
    },
    'com.tw2021.qyjgp' :
    {
        'category' : "Games",
        'updated' : "01 Jun 2022",
        'App_name' : "\u65b0\u4ed9\u4fe0\uff1a\u8d77\u6e90\u2014\u4e0d\u990a\u87f2\uff0c\u771f\u990a\u9f8d",
        'Package_name' : "com.tw2021.qyjgp",
        'installs' : "500,000+",
        'version' : "1.2.7",
    },
    '341778574' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-17T07:56:49Z",
        'App_name' : "Liligo - Vols et voitures",
        'Package_name' : "com.liligo.iPhoneApp",
        'version' : "5.2.1",
        'minimumOsVersion' : "10.0",
    },
    'com.coinswitch.kuber' :
    {
        'category' : "Finance",
        'updated' : "01 Jun 2022",
        'App_name' : "CoinSwitch: Bitcoin Crypto App",
        'Package_name' : "com.coinswitch.kuber",
        'installs' : "10,000,000+",
        'version' : "3.11.0",
    },
    'br.com.marisa.android' :
    {
        'category' : "Shopping",
        'updated' : "16 Jun 2022",
        'App_name' : "Lojas Marisa: Comprar Roupas",
        'Package_name' : "br.com.marisa.android",
        'installs' : "10,000,000+",
        'version' : "4.4.17",
    },
    'id.maucash.app' :
    {
        'category' : "Finance",
        'updated' : "02 Jun 2022",
        'App_name' : "Maucash - Pinjaman Uang Online",
        'Package_name' : "id.maucash.app",
        'installs' : "1,000,000+",
        'version' : "4.0.8",
    },
    'com.dineout.book' :
    {
        'category' : "Food & Drink",
        'updated' : "17 Jun 2022",
        'App_name' : "Dineout: Restaurant Offers",
        'Package_name' : "com.dineout.book",
        'installs' : "5,000,000+",
        'version' : "13.2.1",
    },
    'com.pickproject.livepick' :
    {
        'category' : "Social and Communication",
        'updated' : "10 Feb 2022",
        'App_name' : "\uc601\uc0c1\ucc44\ud305, \ud654\uc0c1\ucc44\ud305, \uc601\ud1b5, \uc601\uc0c1\ud1b5\ud654, \ud654\uc0c1\ud1b5\ud654 \ucea0\ud1a1 - \ud53d\ubbf8\ucea0 \ub77c\uc774\ube0c",
        'Package_name' : "com.pickproject.livepick",
        'installs' : "50,000+",
        'version' : "1.20",
    },
    'com.manash.purplle' :
    {
        'category' : "Shopping",
        'updated' : "16 Jun 2022",
        'App_name' : "Purplle Online Beauty Shopping",
        'Package_name' : "com.manash.purplle",
        'installs' : "10,000,000+",
        'version' : "2.0.83",
    },
    '1485692907' :
    {
        'category' : "Games",
        'updated' : "2022-03-03T01:24:44Z",
        'App_name' : "3\u5206\u9593\u30df\u30b9\u30c6\u30ea\u30fc - \u6687\u3064\u3076\u3057\u63a8\u7406\u30b2\u30fc\u30e0",
        'Package_name' : "com.mask.3minutes",
        'version' : "1.2.4",
        'minimumOsVersion' : "12.1",
    },
    '1416615258' :
    {
        'category' : "Finance",
        'updated' : "2022-06-13T08:43:37Z",
        'App_name' : "Wavely: Match, Chat, Hired",
        'Package_name' : "com.hpbr.wavely",
        'version' : "4.8",
        'minimumOsVersion' : "13.0",
    },
    'com.global.musvn' :
    {
        'category' : "Games",
        'updated' : "24 Apr 2022",
        'App_name' : "MU: V\u01b0\u1ee3t Th\u1eddi \u0110\u1ea1i - Funtap",
        'Package_name' : "com.global.musvn",
        'installs' : "1,000,000+",
        'version' : "1.39.03",
    },
    '1484034730' :
    {
        'category' : "Games",
        'updated' : "2022-05-31T03:22:58Z",
        'App_name' : "Mystic Mansion - Puzzle Game",
        'Package_name' : "com.fundoshiparade.majinmansion",
        'version' : "3.3.1",
        'minimumOsVersion' : "11.0",
    },
    'com.fetchrewards.fetchrewards.hop' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "Fetch Rewards: Earn Gift Cards",
        'Package_name' : "com.fetchrewards.fetchrewards.hop",
        'installs' : "10,000,000+",
        'version' : "2.66.0",
    },
    'com.r2.jswl.kr' :
    {
        'category' : "Games",
        'updated' : "11 Oct 2021",
        'App_name' : "\ucf8c\ub0a8\ubcf8\uc88c",
        'Package_name' : "com.r2.jswl.kr",
        'installs' : "100,000+",
        'version' : "1.1.34311",
    },
    'com.emagroups.cs' :
    {
        'category' : "Games",
        'updated' : "17 Jun 2022",
        'App_name' : "Guardians of Cloudia",
        'Package_name' : "com.emagroups.cs",
        'installs' : "1,000,000+",
        'version' : "1.5.7",
    },
    'com.interserv.ysbbm' :
    {
        'category' : "Games",
        'updated' : "15 Jan 2020",
        'App_name' : "\u91ce\u7378\u90a6\u90a6\u83bd",
        'Package_name' : "com.interserv.ysbbm",
        'installs' : "1,000+",
        'version' : "1.0.2",
    },
    'com.uniqlo.vn.catalogue' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "UNIQLO VN",
        'Package_name' : "com.uniqlo.vn.catalogue",
        'installs' : "100,000+",
        'version' : "7.2.34",
    },
    'cmn.ngukiem.vn' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2020",
        'App_name' : "Ng\u1ef1 Ki\u1ebfm Mobile - CMN",
        'Package_name' : "cmn.ngukiem.vn",
        'installs' : "10,000+",
        'version' : "1.4.0",
    },
    '808796222' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-10T05:53:10Z",
        'App_name' : "EatSure - Food Delivery",
        'Package_name' : "com.Faasos",
        'version' : "6.5.9",
        'minimumOsVersion' : "11.0",
    },
    'com.an.dungeonrpg.kr' :
    {
        'category' : "Games",
        'updated' : "02 Mar 2021",
        'App_name' : "\ube0c\ub808\uc774\ube0c \ub358\uc804",
        'Package_name' : "com.an.dungeonrpg.kr",
        'installs' : "100,000+",
        'version' : "1.0.2",
    },
    'com.spotify.music' :
    {
        'category' : "Entertainment",
        'updated' : "13 Jun 2022",
        'App_name' : "Spotify: Music and Podcasts",
        'Package_name' : "com.spotify.music",
        'installs' : "1,000,000,000+",
        'version' : "Varies with device",
    },
    'com.inovel.app.yemeksepeti' :
    {
        'category' : "Food & Drink",
        'updated' : "13 Jun 2022",
        'App_name' : "Yemeksepeti - Food & Grocery",
        'Package_name' : "com.inovel.app.yemeksepeti",
        'installs' : "10,000,000+",
        'version' : "22.10.0",
    },
    'com.dating.android' :
    {
        'category' : "Social and Communication",
        'updated' : "10 Jun 2022",
        'App_name' : "Dating.com\u2122: Chat, Meet People",
        'Package_name' : "com.dating.android",
        'installs' : "10,000,000+",
        'version' : "7.71.1",
    },
    'com.anghami' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "Anghami: Play music & Podcasts",
        'Package_name' : "com.anghami",
        'installs' : "50,000,000+",
        'version' : "Varies with device",
    },
    'com.vtcmobile.phongmachien' :
    {
        'category' : "Games",
        'updated' : "01 May 2020",
        'App_name' : "Phong Ma Chi\u1ebfn VTC",
        'Package_name' : "com.vtcmobile.phongmachien",
        'installs' : "100,000+",
        'version' : "1.0.3",
    },
    'com.tatemgames.blockking' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Block King - Woody Puzzle Game",
        'Package_name' : "com.tatemgames.blockking",
        'installs' : "100,000+",
        'version' : "0.2.366",
    },
    'com.o1' :
    {
        'category' : "Finance",
        'updated' : "27 May 2022",
        'App_name' : "Shop101: Resell & Earn Online",
        'Package_name' : "com.o1",
        'installs' : "10,000,000+",
        'version' : "3.33.0",
    },
    'com.mobilemedia.tanuki' :
    {
        'category' : "Food & Drink",
        'updated' : "26 May 2022",
        'App_name' : "TanukiFamily \u2014 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u0435\u0434\u044b",
        'Package_name' : "com.mobilemedia.tanuki",
        'installs' : "1,000,000+",
        'version' : "5.3.6",
    },
    'com.tenweemlive' :
    {
        'category' : "Health and Fitness",
        'updated' : "21 Jun 2021",
        'App_name' : "Tenweem - For Psychotherapy Through Hypnosis",
        'Package_name' : "com.tenweemlive",
        'installs' : "10,000+",
        'version' : "2.0",
    },
    'net.bitburst.cryptobull' :
    {
        'category' : "Lifestyle",
        'updated' : "17 Feb 2022",
        'App_name' : "CryptoBull - Earn Bitcoin",
        'Package_name' : "net.bitburst.cryptobull",
        'installs' : "10,000+",
        'version' : "1.0.1",
    },
    'com.samkokkr.gp' :
    {
        'category' : "Games",
        'updated' : "08 Mar 2021",
        'App_name' : "\uad00\ub3c4:\uc0bc\uad6d\uc9c0",
        'Package_name' : "com.samkokkr.gp",
        'installs' : "100,000+",
        'version' : "1.0.14",
    },
    'com.halanuser' :
    {
        'category' : "Finance",
        'updated' : "06 Jun 2022",
        'App_name' : "Halan: Lending, BNPL, Payments",
        'Package_name' : "com.halanuser",
        'installs' : "1,000,000+",
        'version' : "4.4.95",
    },
    '1169846772' :
    {
        'category' : "Games",
        'updated' : "2021-12-20T04:01:28Z",
        'App_name' : "\u30df\u30e9\u30af\u30eb\u30cb\u30ad-\u7740\u305b\u66ff\u3048\u30b3\u30fc\u30c7RPG",
        'Package_name' : "com.nikki.qjnnjapan",
        'version' : "8.0.1",
        'minimumOsVersion' : "9.0",
    },
    'kr.hatam.halftime' :
    {
        'category' : "Shopping",
        'updated' : "17 May 2022",
        'App_name' : "\ud558\ud0d0 (hatam) \u2013 \uc138\uc0c1\uc5d0\uc11c \uac00\uc7a5 \ube60\ub978 \ub9c8\ucf13",
        'Package_name' : "kr.hatam.halftime",
        'installs' : "10,000+",
        'version' : "0.6.50",
    },
    'com.coke.cokeon' :
    {
        'category' : "Lifestyle",
        'updated' : "02 Jun 2022",
        'App_name' : "Coke ON",
        'Package_name' : "com.coke.cokeon",
        'installs' : "5,000,000+",
        'version' : "5.3.1",
    },
    'ph.robocash.mobile' :
    {
        'category' : "Finance",
        'updated' : "13 Aug 2021",
        'App_name' : "Digido Online Loan Philippines",
        'Package_name' : "ph.robocash.mobile",
        'installs' : "1,000,000+",
        'version' : "0.18.4",
    },
    'air.com.gamania.worldflipper' :
    {
        'category' : "Games",
        'updated' : "24 May 2022",
        'App_name' : "\u5f48\u5c04\u4e16\u754c",
        'Package_name' : "air.com.gamania.worldflipper",
        'installs' : "100,000+",
        'version' : "1.350.29",
    },
    'com.huoys.royalcasinoonline' :
    {
        'category' : "Casino",
        'updated' : "31 Mar 2022",
        'App_name' : "Royal Casino",
        'Package_name' : "com.huoys.royalcasinoonline",
        'installs' : "5,000,000+",
        'version' : "12",
    },
    '1517890717' :
    {
        'category' : "Games",
        'updated' : "2022-06-13T21:47:27Z",
        'App_name' : "Reel Stakes Casino: Win Prizes",
        'Package_name' : "com.gameknight.gkpoker1",
        'version' : "1.4.220613",
        'minimumOsVersion' : "12.0",
    },
    'com.mrjefflab.app' :
    {
        'category' : "Lifestyle",
        'updated' : "15 Jun 2022",
        'App_name' : "Jeff - The super services app",
        'Package_name' : "com.mrjefflab.app",
        'installs' : "1,000,000+",
        'version' : "6.44.1",
    },
    'com.gamepub.pt' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Princess Tale",
        'Package_name' : "com.gamepub.pt",
        'installs' : "500,000+",
        'version' : "2.2.04",
    },
    'com.faballey' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "FabAlley -Women Fashion Online Shopping",
        'Package_name' : "com.faballey",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'app.android.idealz.com' :
    {
        'category' : "Shopping",
        'updated' : "07 Jun 2022",
        'App_name' : "Idealz - Winning made easy!",
        'Package_name' : "app.android.idealz.com",
        'installs' : "100,000+",
        'version' : "2.7.8",
    },
    '1270676408' :
    {
        'category' : "Education and Books",
        'updated' : "2022-06-16T03:43:01Z",
        'App_name' : "\u6570\u5b66\u8a08\u7b97\u3001\u5199\u771f\u691c\u7d22\u3067\u89e3\u6c7a\uff01 \u52c9\u5f37\u30a2\u30d7\u30ea-\u30af\u30a1\u30f3\u30c0 Qanda",
        'Package_name' : "Mathpresso.QandaStudent",
        'version' : "4.1.17",
        'minimumOsVersion' : "13.0",
    },
    'com.nexon.v4gb' :
    {
        'category' : "Games",
        'updated' : "05 Apr 2022",
        'App_name' : "V4",
        'Package_name' : "com.nexon.v4gb",
        'installs' : "1,000,000+",
        'version' : "1.30.422908",
    },
    '785385147' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-17T06:20:18Z",
        'App_name' : "Lazada - Online Shopping App!",
        'Package_name' : "com.LazadaSEA.Lazada",
        'version' : "7.2.0",
        'minimumOsVersion' : "9.0",
    },
    'com.entermate.darkeden' :
    {
        'category' : "Games",
        'updated' : "18 May 2022",
        'App_name' : "\ub2e4\ud06c\uc5d0\ub374M",
        'Package_name' : "com.entermate.darkeden",
        'installs' : "100,000+",
        'version' : "1.29.7",
    },
    'com.nexon.baram' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "\ubc14\ub78c\uc758\ub098\ub77c: \uc5f0",
        'Package_name' : "com.nexon.baram",
        'installs' : "1,000,000+",
        'version' : "1.24.131",
    },
    'com.dsx.ssjj' :
    {
        'category' : "Games",
        'updated' : "16 Apr 2021",
        'App_name' : "\u7f8e\u4eba\u524e-\u56e0\u611b\u5165\u9b54\uff0c\u70ba\u4f60\u57f7\u5ff5",
        'Package_name' : "com.dsx.ssjj",
        'installs' : "100,000+",
        'version' : "0.3.12",
    },
    '1480809171' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-10T22:46:34Z",
        'App_name' : "Mildom(\u30df\u30eb\u30c0\u30e0)\u3000\u30b2\u30fc\u30e0\u5b9f\u6cc1\u52d5\u753b\u30fb\u30e9\u30a4\u30d6\u914d\u4fe1\u30a2\u30d7\u30ea",
        'Package_name' : "com.mildom.douyu",
        'version' : "4.3.1",
        'minimumOsVersion' : "11.0",
    },
    'ru.alfacapital.lk' :
    {
        'category' : "Finance",
        'updated' : "02 Jun 2022",
        'App_name' : "\u0410\u043b\u044c\u0444\u0430-\u041a\u0430\u043f\u0438\u0442\u0430\u043b. \u0418\u043d\u0432\u0435\u0441\u0442\u0438\u0446\u0438\u0438",
        'Package_name' : "ru.alfacapital.lk",
        'installs' : "1,000,000+",
        'version' : "2.61.0",
    },
    'jp.co.liica.tkarena' :
    {
        'category' : "Games",
        'updated' : "13 Oct 2021",
        'App_name' : "\u4e09\u56fd\u5fd7\u30ed\u30ef\u30a4\u30e4\u30eb \u30a2\u30ea\u30fc\u30ca - \u30b5\u30f3\u30a2\u30ea",
        'Package_name' : "jp.co.liica.tkarena",
        'installs' : "10,000+",
        'version' : "1.2.5",
    },
    'com.bandainamcoent.opbrww' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "ONE PIECE Bounty Rush",
        'Package_name' : "com.bandainamcoent.opbrww",
        'installs' : "10,000,000+",
        'version' : "51100",
    },
    'com.firstmeetgame.das.gp' :
    {
        'category' : "Games",
        'updated' : "27 Oct 2021",
        'App_name' : "Dragon and Sword\uff1a\u0e14\u0e32\u0e1a\u0e1e\u0e34\u0e06\u0e32\u0e15\u0e21\u0e31\u0e07\u0e01\u0e23",
        'Package_name' : "com.firstmeetgame.das.gp",
        'installs' : "100,000+",
        'version' : "1.0.100",
    },
    'com.rynatsa.xtrendspeed' :
    {
        'category' : "Finance",
        'updated' : "04 Jun 2022",
        'App_name' : "XTrend Speed Trading App",
        'Package_name' : "com.rynatsa.xtrendspeed",
        'installs' : "1,000,000+",
        'version' : "1.5.1",
    },
    '1332596741' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-15T17:24:20Z",
        'App_name' : "IRL\u200e",
        'Package_name' : "irlapp.com.IRL",
        'version' : "4.30",
        'minimumOsVersion' : "13.0",
    },
    'com.heallo.skinexpert' :
    {
        'category' : "Shopping",
        'updated' : "09 Jun 2022",
        'App_name' : "CureSkin: Skin & Hair Care App",
        'Package_name' : "com.heallo.skinexpert",
        'installs' : "5,000,000+",
        'version' : "2.4.47",
    },
    '1026737684' :
    {
        'category' : "Finance",
        'updated' : "2021-11-08T07:19:47Z",
        'App_name' : "CornerJob - Empleo y trabajo",
        'Package_name' : "com.clapp.Jobs",
        'version' : "21.10.1",
        'minimumOsVersion' : "11.0",
    },
    'vnggames.jxqy.swordsman.sgmy' :
    {
        'category' : "Games",
        'updated' : "04 Jan 2022",
        'App_name' : "\u5251\u4fa0\u60c5\u7f18R",
        'Package_name' : "vnggames.jxqy.swordsman.sgmy",
        'installs' : "10,000+",
        'version' : "1.1.13",
    },
    'com.privalia.mex' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Privalia MX - Outlet de moda",
        'Package_name' : "com.privalia.mex",
        'installs' : "1,000,000+",
        'version' : "5.2.1",
    },
    'com.denachina.g23002013.android' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "\u704c\u7c43\u9ad8\u624b SLAM DUNK",
        'Package_name' : "com.denachina.g23002013.android",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    '1491129341' :
    {
        'category' : "Games",
        'updated' : "2022-04-28T02:35:51Z",
        'App_name' : "A3: STILL ALIVE \u5016\u5b58\u8005",
        'Package_name' : "com.netmarble.survivalgb",
        'version' : "1.6.4",
        'minimumOsVersion' : "10.0",
    },
    'online.cashalarm.app' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Cash Alarm: Games & Rewards",
        'Package_name' : "online.cashalarm.app",
        'installs' : "5,000,000+",
        'version' : "4.3.5-CashAlarm",
    },
    'com.kommadot.casesimulator' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Case Chase: Simulator for CSGO",
        'Package_name' : "com.kommadot.casesimulator",
        'installs' : "1,000,000+",
        'version' : "1.11.1",
    },
    'com.karmamobile' :
    {
        'category' : "Health and Fitness",
        'updated' : "12 May 2022",
        'App_name' : "Qare - Consultez un m\u00e9decin en vid\u00e9o 7j/7",
        'Package_name' : "com.karmamobile",
        'installs' : "500,000+",
        'version' : "1.8.241",
    },
    '1097735195' :
    {
        'category' : "Games",
        'updated' : "2022-06-11T01:10:10Z",
        'App_name' : "\ud0b9\uc2a4\ub808\uc774\ub4dc",
        'Package_name' : "com.vespainteractive.KingsRaid",
        'version' : "4.80.2",
        'minimumOsVersion' : "9.0",
    },
    'com.ocean.shooter.vs' :
    {
        'category' : "Games",
        'updated' : "22 Apr 2022",
        'App_name' : "Ocean Shooter",
        'Package_name' : "com.ocean.shooter.vs",
        'installs' : "500,000+",
        'version' : "1.1.1",
    },
    'com.aeo.mena' :
    {
        'category' : "Shopping",
        'updated' : "24 May 2022",
        'App_name' : "AE + Aerie Middle East",
        'Package_name' : "com.aeo.mena",
        'installs' : "100,000+",
        'version' : "1.23.159.99",
    },
    'com.upjers.zoo2animalpark' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "Zoo 2: Animal Park",
        'Package_name' : "com.upjers.zoo2animalpark",
        'installs' : "10,000,000+",
        'version' : "1.81.2",
    },
    '1315859450' :
    {
        'category' : "Games",
        'updated' : "2022-06-07T07:17:32Z",
        'App_name' : "\u5996\u602a\u4e09\u56fd\u5fd7 \u56fd\u76d7\u308a\u30a6\u30a9\u30fc\u30ba",
        'Package_name' : "com.Level5.YSG2",
        'version' : "9.09.00",
        'minimumOsVersion' : "9.0",
    },
    'br.com.sbf.centauro' :
    {
        'category' : "Shopping",
        'updated' : "02 Jun 2022",
        'App_name' : "Centauro: loja de esportes",
        'Package_name' : "br.com.sbf.centauro",
        'installs' : "10,000,000+",
        'version' : "1.9.65",
    },
    '1473424424' :
    {
        'category' : "Games",
        'updated' : "2022-05-30T00:58:39Z",
        'App_name' : "\ud55c\uac8c\uc784 \uc2e0\ub9de\uace0 : \ub300\ud55c\ubbfc\uad6d \uc6d0\uc870 \uace0\uc2a4\ud1b1",
        'Package_name' : "com.nhn.NDuelgo",
        'version' : "1.8.26",
        'minimumOsVersion' : "11.0",
    },
    'com.more.laozi' :
    {
        'category' : "Casino",
        'updated' : "02 Jun 2022",
        'App_name' : "\u8001\u5b50\u6709\u9322-\u6797\u7f8e\u79c0\u3001\u66fe\u570b\u57ce\u5f37\u529b\u63a8\u85a6",
        'Package_name' : "com.more.laozi",
        'installs' : "1,000,000+",
        'version' : "2.112.1871",
    },
    'com.tap4fun.reignofwar' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Invasion: Modern Empire",
        'Package_name' : "com.tap4fun.reignofwar",
        'installs' : "10,000,000+",
        'version' : "1.47.30",
    },
    'ph.getmeds.app' :
    {
        'category' : "Health and Fitness",
        'updated' : "24 May 2022",
        'App_name' : "Getmeds Philippines",
        'Package_name' : "ph.getmeds.app",
        'installs' : "50,000+",
        'version' : "1.55",
    },
    'jp.gungho.padEN' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "Puzzle & Dragons",
        'Package_name' : "jp.gungho.padEN",
        'installs' : "5,000,000+",
        'version' : "19.9.4",
    },
    'com.akode.tosla' :
    {
        'category' : "Finance",
        'updated' : "24 May 2022",
        'App_name' : "Tosla",
        'Package_name' : "com.akode.tosla",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.nytimes.crossword' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "The New York Times Crossword",
        'Package_name' : "com.nytimes.crossword",
        'installs' : "1,000,000+",
        'version' : "4.32.0",
    },
    'com.joycity.tr' :
    {
        'category' : "Games",
        'updated' : "13 Aug 2021",
        'App_name' : "(End of Service)Endless War",
        'Package_name' : "com.joycity.tr",
        'installs' : "500,000+",
        'version' : "1.2.3.3",
    },
    '682942443' :
    {
        'category' : "Games",
        'updated' : "2022-05-27T01:00:11Z",
        'App_name' : "\ucef4\ud22c\uc2a4\ud504\ub85c\uc57c\uad6c for \ub9e4\ub2c8\uc800 LIVE 2022",
        'Package_name' : "com.com2us.kbomanager.normal.freefull.apple.global.ios.universal",
        'version' : "10.2.0",
        'minimumOsVersion' : "9.0",
    },
    'com.youngplatform.exchange' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "Young Platform",
        'Package_name' : "com.youngplatform.exchange",
        'installs' : "100,000+",
        'version' : "2.3.3",
    },
    '292738169' :
    {
        'category' : "Entertainment",
        'updated' : "2022-06-13T07:40:14Z",
        'App_name' : "Deezer: Musik & H\u00f6rb\u00fccher",
        'Package_name' : "com.deezer.Deezer",
        'version' : "9.15.0",
        'minimumOsVersion' : "14.0",
    },
    'hk.com.theclub.health' :
    {
        'category' : "Health and Fitness",
        'updated' : "29 Oct 2021",
        'App_name' : "Club Wellbeing",
        'Package_name' : "hk.com.theclub.health",
        'installs' : "10,000+",
        'version' : "1.2.7  (70)",
    },
    'com.adblockguru.adsblocker' :
    {
        'category' : "Tools",
        'updated' : "06 May 2021",
        'App_name' : "Adblock - Private Adblocker Browser App",
        'Package_name' : "com.adblockguru.adsblocker",
        'installs' : "10,000+",
        'version' : "1.0.8",
    },
    'za.co.travelstart.flapp' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "13 Jun 2022",
        'App_name' : "Flapp By Travelstart",
        'Package_name' : "za.co.travelstart.flapp",
        'installs' : "1,000,000+",
        'version' : "6.17.1",
    },
    'com.stormgain.mobile' :
    {
        'category' : "Finance",
        'updated' : "26 Feb 2022",
        'App_name' : "StormGain: Bitcoin Wallet App",
        'Package_name' : "com.stormgain.mobile",
        'installs' : "5,000,000+",
        'version' : "1.23.0",
    },
    '966142320' :
    {
        'category' : "Entertainment",
        'updated' : "2022-06-01T01:02:13Z",
        'App_name' : "LINE MUSIC \u97f3\u697d\u306f\u30e9\u30a4\u30f3\u30df\u30e5\u30fc\u30b8\u30c3\u30af",
        'Package_name' : "jp.line.linemusic",
        'version' : "6.1.3",
        'minimumOsVersion' : "13.0",
    },
    'com.crazylabs.amaze.game' :
    {
        'category' : "Games",
        'updated' : "08 Apr 2022",
        'App_name' : "AMAZE!",
        'Package_name' : "com.crazylabs.amaze.game",
        'installs' : "10,000,000+",
        'version' : "3.4.1.0",
    },
    'com.emmental.bznav.mobile' :
    {
        'category' : "Finance",
        'updated' : "29 Apr 2022",
        'App_name' : "\ube44\uc988\ub135 - \uac00\uc7a5 \uc26c\uc6b4 \uc0ac\uc5c5\uc790\uae08 \ud1b5\ud569\uad00\ub9ac \uc194\ub8e8\uc158",
        'Package_name' : "com.emmental.bznav.mobile",
        'installs' : "100,000+",
        'version' : "3.21.0",
    },
    'com.shopee.ph' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Shopee PH: Shop this 6.6-7.7",
        'Package_name' : "com.shopee.ph",
        'installs' : "50,000,000+",
        'version' : "2.88.41",
    },
    'com.linegames.exos' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "Exos Heroes",
        'Package_name' : "com.linegames.exos",
        'installs' : "1,000,000+",
        'version' : "5.6.2",
    },
    'com.aranoah.healthkart.plus' :
    {
        'category' : "Health and Fitness",
        'updated' : "08 Jun 2022",
        'App_name' : "TATA 1mg Online Healthcare App",
        'Package_name' : "com.aranoah.healthkart.plus",
        'installs' : "10,000,000+",
        'version' : "15.4.0",
    },
    'com.gudangada.gudangada' :
    {
        'category' : "Shopping",
        'updated' : "27 May 2022",
        'App_name' : "GudangAda Wholesale Shopping",
        'Package_name' : "com.gudangada.gudangada",
        'installs' : "1,000,000+",
        'version' : "2.24.0",
    },
    'world.social.group.video.share' :
    {
        'category' : "Social and Communication",
        'updated' : "17 Jun 2022",
        'App_name' : "Helo - Humor and Social Trends",
        'Package_name' : "world.social.group.video.share",
        'installs' : "50,000,000+",
        'version' : "3.1.1.02",
    },
    'ua.raketa.app' :
    {
        'category' : "Food & Drink",
        'updated' : "09 Feb 2022",
        'App_name' : "Rocket\uff0dFood Delivery",
        'Package_name' : "ua.raketa.app",
        'installs' : "1,000,000+",
        'version' : "2.62.0",
    },
    'com.rainmanagement.rain' :
    {
        'category' : "Finance",
        'updated' : "31 May 2022",
        'App_name' : "Rain: Buy & Sell Bitcoin",
        'Package_name' : "com.rainmanagement.rain",
        'installs' : "500,000+",
        'version' : "3.0.12",
    },
    'com.jinjinjin777.tapirus' :
    {
        'category' : "Casino",
        'updated' : "21 May 2022",
        'App_name' : "JinJinJin",
        'Package_name' : "com.jinjinjin777.tapirus",
        'installs' : "1,000,000+",
        'version' : "2.23.1",
    },
    'com.xxqy.gs' :
    {
        'category' : "Games",
        'updated' : "01 Jun 2022",
        'App_name' : "T\u00e2n Tru Th\u1ea7n Truy\u1ec7n",
        'Package_name' : "com.xxqy.gs",
        'installs' : "500,000+",
        'version' : "1.3.0",
    },
    '1227411416' :
    {
        'category' : "Entertainment",
        'updated' : "2021-12-09T01:36:58Z",
        'App_name' : "\u5373\u4f1a\u3044\u63a2\u3057\uff01\u51fa\u4f1a\u3044\u7cfb\u30a2\u30d7\u30ea\u30fb\u30ca\u30a6\u30c1\u30e3\u30c3\u30c8",
        'Package_name' : "net.nowchat",
        'version' : "1.12",
        'minimumOsVersion' : "11.0",
    },
    '1001394201' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-15T04:23:15Z",
        'App_name' : "Tokopedia",
        'Package_name' : "com.tokopedia.Tokopedia",
        'version' : "2.184.0",
        'minimumOsVersion' : "12.0",
    },
    'com.boloplay.ddz' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\u958b\u5fc3\u9b25\u5730\u4e3b - \u5341\u4e09\u6c34\u300121\u9ede\u3001\u8001\u864e\u6a5f\u3001\u7dda\u4e0a\u771f\u4eba\u4e00\u79d2\u958b\u5c40",
        'Package_name' : "com.boloplay.ddz",
        'installs' : "100,000+",
        'version' : "1.5.4",
    },
    'com.ilevit.alwayz.android' :
    {
        'category' : "Shopping",
        'updated' : "16 May 2022",
        'App_name' : "\uc62c\uc6e8\uc774\uc988 - \ud300\uad6c\ub9e4\ub85c \ucd08\ud2b9\uac00 \uc1fc\ud551",
        'Package_name' : "com.ilevit.alwayz.android",
        'installs' : "1,000,000+",
        'version' : "1.2.80",
    },
    'in.publicam.thinkrightme' :
    {
        'category' : "Health and Fitness",
        'updated' : "16 Jun 2022",
        'App_name' : "ThinkRight.me: Meditate Daily",
        'Package_name' : "in.publicam.thinkrightme",
        'installs' : "1,000,000+",
        'version' : "3.30",
    },
    '1303445343' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-02T10:04:40Z",
        'App_name' : "\u0420\u0418\u0412 \u0413\u041e\u0428 \u041f\u0430\u0440\u0444\u044e\u043c\u0435\u0440\u0438\u044f \u0438 \u041a\u043e\u0441\u043c\u0435\u0442\u0438\u043a\u0430",
        'Package_name' : "ru.rivegauche.wkapp",
        'version' : "2.16.3",
        'minimumOsVersion' : "12.0",
    },
    'tw.sonet.bxl' :
    {
        'category' : "Games",
        'updated' : "12 Jan 2021",
        'App_name' : "BLADE XLORD \u773e\u528d\u4e4b\u738b",
        'Package_name' : "tw.sonet.bxl",
        'installs' : "50,000+",
        'version' : "1.2.1",
    },
    'com.naviapp' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "Navi: Loans & Health Insurance",
        'Package_name' : "com.naviapp",
        'installs' : "10,000,000+",
        'version' : "2.6.9",
    },
    '1516205019' :
    {
        'category' : "Games",
        'updated' : "2020-10-17T00:45:56Z",
        'App_name' : "Racing Wheels",
        'Package_name' : "com.FrtZrn.RacingWheels",
        'version' : "1.5.6",
        'minimumOsVersion' : "10.0",
    },
    '1164400646' :
    {
        'category' : "Games",
        'updated' : "2022-06-20T04:02:18Z",
        'App_name' : "#\u30b3\u30f3\u30d1\u30b9\u3010\u6226\u95d8\u6442\u7406\u89e3\u6790\u30b7\u30b9\u30c6\u30e0\u3011",
        'Package_name' : "com.nhnpa.cps",
        'version' : "1.136.0",
        'minimumOsVersion' : "10.0",
    },
    'com.cbs.tve' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "CBS",
        'Package_name' : "com.cbs.tve",
        'installs' : "1,000,000+",
        'version' : "8.1.23",
    },
    'jp.co.gu3.orange01' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "\u30af\u30ea\u30b9\u30bf\u30eb \u30aa\u30d6 \u30ea\u30e6\u30cb\u30aa\u30f3\u3010\u738b\u56fd\u30b9\u30c8\u30e9\u30c6\u30b8\u30fcRPG\u3011",
        'Package_name' : "jp.co.gu3.orange01",
        'installs' : "500,000+",
        'version' : "6.0.30",
    },
    'com.canva.editor' :
    {
        'category' : "Not available",
        'updated' : "17 Jun 2022",
        'App_name' : "Canva: Design, Photo & Video",
        'Package_name' : "com.canva.editor",
        'installs' : "100,000,000+",
        'version' : "2.169.0",
    },
    'com.bandainamcoent.gb_asia' :
    {
        'category' : "Games",
        'updated' : "13 Apr 2022",
        'App_name' : "GUNDAM BREAKER : MOBILE",
        'Package_name' : "com.bandainamcoent.gb_asia",
        'installs' : "1,000,000+",
        'version' : "3.04.00",
    },
    'com.eyougame.tamashi.en' :
    {
        'category' : "Games",
        'updated' : "19 Apr 2022",
        'App_name' : "Tamashi : Rise of Yokai",
        'Package_name' : "com.eyougame.tamashi.en",
        'installs' : "100,000+",
        'version' : "13.0",
    },
    'com.n3twork.legendary' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Legendary: Game of Heroes",
        'Package_name' : "com.n3twork.legendary",
        'installs' : "5,000,000+",
        'version' : "3.13.13",
    },
    'com.yoda.and.twsgdh' :
    {
        'category' : "Games",
        'updated' : "12 May 2020",
        'App_name' : "\u4e09\u570b\u5927\u4ea8",
        'Package_name' : "com.yoda.and.twsgdh",
        'installs' : "100,000+",
        'version' : "2.0.0",
    },
    'com.sdpgames.sculptpeople' :
    {
        'category' : "Games",
        'updated' : "06 Jun 2022",
        'App_name' : "Sculpt people",
        'Package_name' : "com.sdpgames.sculptpeople",
        'installs' : "100,000,000+",
        'version' : "1.9.1.0",
    },
    '1326224436' :
    {
        'category' : "Shopping",
        'updated' : "2022-05-19T06:26:40Z",
        'App_name' : "Golden Scent \u0642\u0648\u0644\u062f\u0646 \u0633\u0646\u062a",
        'Package_name' : "com.goldenscent.app",
        'version' : "6.17",
        'minimumOsVersion' : "11.0",
    },
    'com.jllnew4399kr.google' :
    {
        'category' : "Games",
        'updated' : "15 Oct 2020",
        'App_name' : "\ubb34\ucc9c",
        'Package_name' : "com.jllnew4399kr.google",
        'installs' : "50,000+",
        'version' : "1.0.2.9",
    },
    'jp.co.capcom.mh_rjp' :
    {
        'category' : "Games",
        'updated' : "12 Oct 2021",
        'App_name' : "\u30e2\u30f3\u30b9\u30bf\u30fc\u30cf\u30f3\u30bf\u30fc \u30e9\u30a4\u30c0\u30fc\u30ba",
        'Package_name' : "jp.co.capcom.mh_rjp",
        'installs' : "500,000+",
        'version' : "4.02.00",
    },
    'com.wrx.wazirx' :
    {
        'category' : "Finance",
        'updated' : "01 Jun 2022",
        'App_name' : "WazirX : Buy Bitcoin & Crypto",
        'Package_name' : "com.wrx.wazirx",
        'installs' : "10,000,000+",
        'version' : "2.23.1",
    },
    'com.fowgames.projectf' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "\ud0b9\ub364 : \uc804\uc7c1\uc758 \ubd88\uc528",
        'Package_name' : "com.fowgames.projectf",
        'installs' : "100,000+",
        'version' : "1.01.89",
    },
    'com.muzmatch.muzmatchapp' :
    {
        'category' : "Social and Communication",
        'updated' : "16 Jun 2022",
        'App_name' : "Muzz - formerly muzmatch",
        'Package_name' : "com.muzmatch.muzmatchapp",
        'installs' : "1,000,000+",
        'version' : "7.3.0a",
    },
    'com.f4.czqstr.gp' :
    {
        'category' : "Games",
        'updated' : "28 Mar 2022",
        'App_name' : "\u30aa\u30eb\u30bf\u30f3\u30b7\u30a2\u30fb\u30b5\u30fc\u30acR",
        'Package_name' : "com.f4.czqstr.gp",
        'installs' : "50,000+",
        'version' : "1.1.7",
    },
    'com.elmenus.app' :
    {
        'category' : "Food & Drink",
        'updated' : "09 Jun 2022",
        'App_name' : "elmenus Discover & Order food",
        'Package_name' : "com.elmenus.app",
        'installs' : "1,000,000+",
        'version' : "3.95.1",
    },
    'tw.com.iwplay.ws' :
    {
        'category' : "Games",
        'updated' : "30 Aug 2021",
        'App_name' : "\u6230\u795e\u98a8\u66b4",
        'Package_name' : "tw.com.iwplay.ws",
        'installs' : "50,000+",
        'version' : "1.0.12",
    },
    '895761422' :
    {
        'category' : "Games",
        'updated' : "2022-05-27T01:00:40Z",
        'App_name' : "LINE \u30d0\u30d6\u30eb2",
        'Package_name' : "com.linecorp.LGBB2",
        'version' : "3.9.1",
        'minimumOsVersion' : "10.0",
    },
    'com.u8.spaceridersh' :
    {
        'category' : "Games",
        'updated' : "28 Aug 2020",
        'App_name' : "Space Rider: Star Hunt",
        'Package_name' : "com.u8.spaceridersh",
        'installs' : "10,000+",
        'version' : "4.3.0",
    },
    'com.pocketaces.locostudio' :
    {
        'category' : "Entertainment",
        'updated' : "13 Jun 2022",
        'App_name' : "Loco Studio: Start Live Stream",
        'Package_name' : "com.pocketaces.locostudio",
        'installs' : "1,000,000+",
        'version' : "1.0.51",
    },
    'com.epicarena.google' :
    {
        'category' : "Games",
        'updated' : "01 Sep 2021",
        'App_name' : "\uc5d0\ud53d\uc544\ub808\ub098",
        'Package_name' : "com.epicarena.google",
        'installs' : "10,000+",
        'version' : "1.3.1",
    },
    'co.testee.android' :
    {
        'category' : "Lifestyle",
        'updated' : "12 May 2022",
        'App_name' : "\u30dd\u30a4\u30f3\u30c8\u3067\u304a\u5c0f\u9063\u3044\u3092\u7a3c\u3050\u30fb\u8caf\u3081\u308b\u306a\u3089Powl\uff08\u30dd\u30fc\u30eb\uff09",
        'Package_name' : "co.testee.android",
        'installs' : "500,000+",
        'version' : "4.1.2",
    },
    'com.netease.kolite' :
    {
        'category' : "Games",
        'updated' : "19 Apr 2022",
        'App_name' : "\u8352\u91ce\u884c\u52d5-AIR",
        'Package_name' : "com.netease.kolite",
        'installs' : "500,000+",
        'version' : "1.280.479406",
    },
    'vng.games.daula.dailuc.soulland' :
    {
        'category' : "Games",
        'updated' : "21 Apr 2022",
        'App_name' : "\u0110\u1ea5u La VNG: \u0110\u1ea5u Th\u1ea7n T\u00e1i L\u00e2m",
        'Package_name' : "vng.games.daula.dailuc.soulland",
        'installs' : "100,000+",
        'version' : "1.0.5",
    },
    'com.square_enix.ffbejpn' :
    {
        'category' : "Games",
        'updated' : "13 May 2022",
        'App_name' : "FINAL FANTASY BRAVE EXVIUS",
        'Package_name' : "com.square_enix.ffbejpn",
        'installs' : "1,000,000+",
        'version' : "7.7.0",
    },
    'com.finance.oneaset' :
    {
        'category' : "Finance",
        'updated' : "22 May 2022",
        'App_name' : "OneAset",
        'Package_name' : "com.finance.oneaset",
        'installs' : "1,000,000+",
        'version' : "1.1.1",
    },
    'com.brightcapital.app' :
    {
        'category' : "Finance",
        'updated' : "17 May 2022",
        'App_name' : "Bright - Crush Your Card Debt",
        'Package_name' : "com.brightcapital.app",
        'installs' : "100,000+",
        'version' : "1.36",
    },
    'com.raizen.acelera' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Shell Box",
        'Package_name' : "com.raizen.acelera",
        'installs' : "5,000,000+",
        'version' : "9.12.5.0",
    },
    'com.nextgames.android.twd' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "The Walking Dead No Man's Land",
        'Package_name' : "com.nextgames.android.twd",
        'installs' : "10,000,000+",
        'version' : "5.0.3.354",
    },
    'com.sinarmasland.onesmile' :
    {
        'category' : "Lifestyle",
        'updated' : "12 Jun 2022",
        'App_name' : "OneSmile",
        'Package_name' : "com.sinarmasland.onesmile",
        'installs' : "10,000+",
        'version' : "2.220612.169",
    },
    '971265422' :
    {
        'category' : "Entertainment",
        'updated' : "2022-06-02T18:44:18Z",
        'App_name' : "HBO Max: Stream TV & Movies",
        'Package_name' : "com.hbo.hbonow",
        'version' : "52.25.0",
        'minimumOsVersion' : "12.2",
    },
    'com.bluesom.SaverGrow' :
    {
        'category' : "Games",
        'updated' : "15 Sep 2021",
        'App_name' : "\uad6c\uc6d0\uc790\ub97c \ud0a4\uc6b0\ub294 \ubc29\ubc95:\uc57c\uc0dd\uc18c\ub140",
        'Package_name' : "com.bluesom.SaverGrow",
        'installs' : "100,000+",
        'version' : "0.29",
    },
    'com.megaxus.ayodancepuzzles' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "AyoDance Puzzles",
        'Package_name' : "com.megaxus.ayodancepuzzles",
        'installs' : "100,000+",
        'version' : "1.1.6",
    },
    '530621395' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-10T17:51:12Z",
        'App_name' : "Wish - Shopping Made Fun",
        'Package_name' : "com.contextlogic.Wish",
        'version' : "22.11.0",
        'minimumOsVersion' : "12.1",
    },
    'com.com2us.kbomanager.normal2.freefull.google.global.android.common' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "\ucef4\ud22c\uc2a4\ud504\ub85c\uc57c\uad6c\ub9e4\ub2c8\uc800 LIVE 2022",
        'Package_name' : "com.com2us.kbomanager.normal2.freefull.google.global.android.common",
        'installs' : "1,000,000+",
        'version' : "10.2.0",
    },
    'yellowclass.kids.live' :
    {
        'category' : "Education and Books",
        'updated' : "10 Jun 2022",
        'App_name' : "Yellow Class - Kids Learn Live",
        'Package_name' : "yellowclass.kids.live",
        'installs' : "5,000,000+",
        'version' : "3.2.3",
    },
    'com.ifolor.photoservice' :
    {
        'category' : "Entertainment",
        'updated' : "24 May 2022",
        'App_name' : "ifolor: Photo Books, Photos",
        'Package_name' : "com.ifolor.photoservice",
        'installs' : "500,000+",
        'version' : "2.8.4",
    },
    'advenworks.com.snakeblast' :
    {
        'category' : "Games",
        'updated' : "31 May 2021",
        'App_name' : "Snake Blast!",
        'Package_name' : "advenworks.com.snakeblast",
        'installs' : "500,000+",
        'version' : "1.700",
    },
    'biz.org.daysapp' :
    {
        'category' : "Lifestyle",
        'updated' : "30 Dec 2021",
        'App_name' : "days(\u30c7\u30a4\u30ba)  -  \u30c1\u30e3\u30c3\u30c8\u3067\u6bce\u65e5\u304c\u5909\u308f\u308b",
        'Package_name' : "biz.org.daysapp",
        'installs' : "100,000+",
        'version' : "1.2.0.0",
    },
    '988259048' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-16T02:11:43Z",
        'App_name' : "17LIVE - Live Streaming",
        'Package_name' : "com.machipopo.story17",
        'version' : "3.191.1",
        'minimumOsVersion' : "13.0",
    },
    '1031150328' :
    {
        'category' : "Games",
        'updated' : "2022-06-20T02:10:26Z",
        'App_name' : "\u0627\u0646\u062a\u0642\u0627\u0645 \u0627\u0644\u0633\u0644\u0627\u0637\u064a\u0646",
        'Package_name' : "com.onemt.war.ar",
        'version' : "1.16.25",
        'minimumOsVersion' : "9.0",
    },
    'com.medprescricao' :
    {
        'category' : "Health and Fitness",
        'updated' : "02 Jun 2022",
        'App_name' : "Whitebook Medicina: Resid\u00eancia",
        'Package_name' : "com.medprescricao",
        'installs' : "1,000,000+",
        'version' : "11.13.0",
    },
    'com.devsisters.ck' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "Cookie Run: Kingdom",
        'Package_name' : "com.devsisters.ck",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.tencent.mxylzko.android' :
    {
        'category' : "Games",
        'updated' : "30 Jan 2021",
        'App_name' : "\uc601\uc8fc : \ubc31\uc758 \uc5f0\ub300\uae30",
        'Package_name' : "com.tencent.mxylzko.android",
        'installs' : "100,000+",
        'version' : "1.0.313.1",
    },
    'com.shinegames.dynastones' :
    {
        'category' : "Games",
        'updated' : "25 Mar 2021",
        'App_name' : "\ub2e4\uc774\ub108\uc2a4\ud1a4\uc988 (\ud504\ub860\ud2f0\uc5b4 \ud14c\uc2a4\ud2b8)",
        'Package_name' : "com.shinegames.dynastones",
        'installs' : "5,000+",
        'version' : "1.0.4",
    },
    'com.muni.android' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "Muni: \u00a1Ganar dinero extra!",
        'Package_name' : "com.muni.android",
        'installs' : "500,000+",
        'version' : "v22.05.26-6-Live",
    },
    'com.Meditation.app' :
    {
        'category' : "Health and Fitness",
        'updated' : "19 May 2022",
        'App_name' : "Breethe - Meditation & Sleep",
        'Package_name' : "com.Meditation.app",
        'installs' : "500,000+",
        'version' : "5.6.3",
    },
    'com.lucky.goldenegg' :
    {
        'category' : "Games",
        'updated' : "24 Dec 2021",
        'App_name' : "Lucky Eggs - Win Big Rewards",
        'Package_name' : "com.lucky.goldenegg",
        'installs' : "1,000,000+",
        'version' : "1.2.9",
    },
    'com.bandainamcoent.mini4wdgp' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "\u30df\u30cb\u56db\u99c6 \u8d85\u901f\u30b0\u30e9\u30f3\u30d7\u30ea",
        'Package_name' : "com.bandainamcoent.mini4wdgp",
        'installs' : "500,000+",
        'version' : "1.12.3",
    },
    'jp.co.yoozoo.projectyellow' :
    {
        'category' : "Games",
        'updated' : "12 Nov 2021",
        'App_name' : "\u30ec\u30c3\u30c9\uff1a\u30d7\u30e9\u30a4\u30c9\u30aa\u30d6\u30a8\u30c7\u30f3-\u30d7\u30e9\u30a8\u30c7",
        'Package_name' : "jp.co.yoozoo.projectyellow",
        'installs' : "100,000+",
        'version' : "1.0.19",
    },
    'com.done.faasos' :
    {
        'category' : "Food & Drink",
        'updated' : "02 Jun 2022",
        'App_name' : "EatSure - Online Food Delivery",
        'Package_name' : "com.done.faasos",
        'installs' : "5,000,000+",
        'version' : "6.6.1",
    },
    'com.pajk.idpersonaldoc' :
    {
        'category' : "Health and Fitness",
        'updated' : "10 Jun 2022",
        'App_name' : "Good Doctor",
        'Package_name' : "com.pajk.idpersonaldoc",
        'installs' : "1,000,000+",
        'version' : "3.6.1",
    },
    '1088840761' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-06T16:26:02Z",
        'App_name' : "Marley Spoon: Kochbox",
        'Package_name' : "com.marleyspoon.membership.app",
        'version' : "3.7.0",
        'minimumOsVersion' : "12.0",
    },
    'com.jedigames.p20.googleplay' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "Three Kingdoms: Overlord",
        'Package_name' : "com.jedigames.p20.googleplay",
        'installs' : "1,000,000+",
        'version' : "2.14.39",
    },
    'com.gmt.anhhungxadieu' :
    {
        'category' : "Games",
        'updated' : "28 Sep 2021",
        'App_name' : "T\u00e2n Anh H\u00f9ng X\u1ea1 \u0110i\u00eau",
        'Package_name' : "com.gmt.anhhungxadieu",
        'installs' : "1,000,000+",
        'version' : "1.7.51",
    },
    'com.linecorp.LGPTT' :
    {
        'category' : "Games",
        'updated' : "19 May 2022",
        'App_name' : "LINE Puzzle TanTan",
        'Package_name' : "com.linecorp.LGPTT",
        'installs' : "5,000,000+",
        'version' : "4.7.0",
    },
    'com.kannadamatrimony' :
    {
        'category' : "Social and Communication",
        'updated' : "23 May 2022",
        'App_name' : "Kannada Matrimony-Marriage App",
        'Package_name' : "com.kannadamatrimony",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'my.com.myboost' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "Boost eWallet Malaysia",
        'Package_name' : "my.com.myboost",
        'installs' : "5,000,000+",
        'version' : "5.20.625",
    },
    '813494275' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-04-07T00:45:37Z",
        'App_name' : "GS Fresh Mall / \uc2ec\ud50c\ub9ac\ucfe1",
        'Package_name' : "com.gsretail.ios.smapp",
        'version' : "3.0.16",
        'minimumOsVersion' : "11.0",
    },
    'com.aladinfun.islandking.android' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "Island King",
        'Package_name' : "com.aladinfun.islandking.android",
        'installs' : "10,000,000+",
        'version' : "2.39.0",
    },
    'com.netmarble.revolutionjp' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "\u30ea\u30cd\u30fc\u30b8\u30e52 \u30ec\u30dc\u30ea\u30e5\u30fc\u30b7\u30e7\u30f3",
        'Package_name' : "com.netmarble.revolutionjp",
        'installs' : "500,000+",
        'version' : "3.12.04",
    },
    'com.blackgames.rasenganrun' :
    {
        'category' : "Games",
        'updated' : "22 Apr 2022",
        'App_name' : "Manga Run: Rasengan!",
        'Package_name' : "com.blackgames.rasenganrun",
        'installs' : "1,000,000+",
        'version' : "2.24",
    },
    'com.caocaoglobal.ceeu' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "27 Oct 2021",
        'App_name' : "CAOCAO, the responsible Chauffeur service",
        'Package_name' : "com.caocaoglobal.ceeu",
        'installs' : "100,000+",
        'version' : "2.11.5",
    },
    'com.mega.winner.slots.casino.vegas' :
    {
        'category' : "Casino",
        'updated' : "22 Feb 2022",
        'App_name' : "Mega Winner Slots - Hot Vegas Casino Games",
        'Package_name' : "com.mega.winner.slots.casino.vegas",
        'installs' : "100,000+",
        'version' : "1.0.2",
    },
    'com.zephyrus.clsy.gp' :
    {
        'category' : "Games",
        'updated' : "26 Apr 2022",
        'App_name' : "\u84bc\u85cd\u306e\u8a93\u3044 - \u30d6\u30eb\u30fc\u30aa\u30fc\u30b9",
        'Package_name' : "com.zephyrus.clsy.gp",
        'installs' : "100,000+",
        'version' : "1.4.0",
    },
    'vn.funtap.thuthanhorigin' :
    {
        'category' : "Games",
        'updated' : "14 Dec 2020",
        'App_name' : "Th\u1ee7 Th\u00e0nh Origin - Thu Thanh Origin",
        'Package_name' : "vn.funtap.thuthanhorigin",
        'installs' : "100,000+",
        'version' : "1.0.5",
    },
    'com.frubana' :
    {
        'category' : "Shopping",
        'updated' : "30 May 2022",
        'App_name' : "Frubana: todo para tu negocio",
        'Package_name' : "com.frubana",
        'installs' : "500,000+",
        'version' : "4.8.31",
    },
    '1538412237' :
    {
        'category' : "Utilities",
        'updated' : "2022-05-19T17:05:49Z",
        'App_name' : "Call Blocker: Scam Spam Lookup",
        'Package_name' : "com.madduck.callblocker",
        'version' : "2.1.6",
        'minimumOsVersion' : "12.0",
    },
    'app.ridecheck.android' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "08 Jun 2022",
        'App_name' : "Check e-scooter sharing",
        'Package_name' : "app.ridecheck.android",
        'installs' : "100,000+",
        'version' : "1.15.3",
    },
    '1321349856' :
    {
        'category' : "Social and Communication",
        'updated' : "2019-06-02T04:38:15Z",
        'App_name' : "paddy67(\u30d1\u30c7\u30a367)-\u4eca\u3059\u3050\u4f1a\u3048\u308b\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea",
        'Package_name' : "jp.jun.yoshida.paddy",
        'version' : "2.0.6",
        'minimumOsVersion' : "10.0",
    },
    'com.gamehunt.boomzth' :
    {
        'category' : "Games",
        'updated' : "19 Feb 2019",
        'App_name' : "BOOMZ Thailand",
        'Package_name' : "com.gamehunt.boomzth",
        'installs' : "100,000+",
        'version' : "2.5.8.0",
    },
    'com.global.gd.google' :
    {
        'category' : "Games",
        'updated' : "12 May 2022",
        'App_name' : "\ub0b4\uac00\uc655\uc774\ub77c\uba74\uff1a\uace0\uc804 \uc804\ub7b5 \uc2dc\ubbac\ub808\uc774\uc158 \uac8c\uc784",
        'Package_name' : "com.global.gd.google",
        'installs' : "10,000+",
        'version' : "4.0",
    },
    'com.sega.shp' :
    {
        'category' : "Games",
        'updated' : "18 Apr 2022",
        'App_name' : "StarHorsePocket+\u3000\u2013\u7af6\u99ac\u30b2\u30fc\u30e0\u2013",
        'Package_name' : "com.sega.shp",
        'installs' : "100,000+",
        'version' : "6.2.0",
    },
    'com.innofinsolutions.lendenclub.lender' :
    {
        'category' : "Finance",
        'updated' : "20 Jan 2022",
        'App_name' : "LenDenClub Invest Money App",
        'Package_name' : "com.innofinsolutions.lendenclub.lender",
        'installs' : "100,000+",
        'version' : "2.3.9",
    },
    'kr.pepperbank.digital' :
    {
        'category' : "Finance",
        'updated' : "10 May 2022",
        'App_name' : "\ub514\uc9c0\ud138\ud398\ud37c (\ud398\ud37c\uc800\ucd95\uc740\ud589)",
        'Package_name' : "kr.pepperbank.digital",
        'installs' : "50,000+",
        'version' : "1.0.4",
    },
    'jp.co.capcom.basarajp' :
    {
        'category' : "Games",
        'updated' : "26 Jun 2020",
        'App_name' : "\u6226\u56fdBASARA \u30d0\u30c8\u30eb\u30d1\u30fc\u30c6\u30a3\u30fc",
        'Package_name' : "jp.co.capcom.basarajp",
        'installs' : "100,000+",
        'version' : "1.9.1",
    },
    '319557427' :
    {
        'category' : "Not available",
        'updated' : "2022-06-17T00:36:19Z",
        'App_name' : "Le Figaro : Actualit\u00e9s et Info",
        'Package_name' : "com.lefigaro.Figaro.fr",
        'version' : "6.4.0",
        'minimumOsVersion' : "11.0",
    },
    'com.ntrance.dkmobile' :
    {
        'category' : "Games",
        'updated' : "22 Feb 2022",
        'App_name' : "DK\ubaa8\ubc14\uc77c: \uc601\uc6c5\uc758\uadc0\ud658",
        'Package_name' : "com.ntrance.dkmobile",
        'installs' : "100,000+",
        'version' : "3.4.9",
    },
    'com.monawa.ilgum' :
    {
        'category' : "Games",
        'updated' : "10 Jan 2022",
        'App_name' : "\uc77c\uac80\ud654\uc120",
        'Package_name' : "com.monawa.ilgum",
        'installs' : "100,000+",
        'version' : "7.0.0",
    },
    'com.boyaa.enginexgxianggangqp.main' :
    {
        'category' : "Casino",
        'updated' : "16 May 2022",
        'App_name' : "\u958b\u5fc3\u9b25\u4e00\u756a-\u6e2f\u5f0f\u9ebb\u96c0 \u8dd1\u99ac\u4ed4 \u92e4\u5927D\u7b495 IN 1",
        'Package_name' : "com.boyaa.enginexgxianggangqp.main",
        'installs' : "500,000+",
        'version' : "3.8.3",
    },
    'com.fun.happyfruit' :
    {
        'category' : "Games",
        'updated' : "27 Jan 2022",
        'App_name' : "Happy Fruit :Match 3 Puzzle",
        'Package_name' : "com.fun.happyfruit",
        'installs' : "50,000+",
        'version' : "1.0.1",
    },
    'com.dancing.smash.hop.game.tiles.circles.beat.paino' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Smash Colors 3D: Swing & Dash",
        'Package_name' : "com.dancing.smash.hop.game.tiles.circles.beat.paino",
        'installs' : "50,000,000+",
        'version' : "1.0.25",
    },
    '593715088' :
    {
        'category' : "Games",
        'updated' : "2022-05-31T15:21:38Z",
        'App_name' : "Solitaire\u00b7",
        'Package_name' : "com.kathleenOswald.solitaire",
        'version' : "10.0.13",
        'minimumOsVersion' : "11.0",
    },
    'net.gadm.tv' :
    {
        'category' : "Entertainment",
        'updated' : "30 May 2022",
        'App_name' : "stc tv",
        'Package_name' : "net.gadm.tv",
        'installs' : "1,000,000+",
        'version' : "5.2.0",
    },
    'com.skytecgames.cardsheroes' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Card Heroes: TCG/CCG deck Wars",
        'Package_name' : "com.skytecgames.cardsheroes",
        'installs' : "1,000,000+",
        'version' : "2.3.2066",
    },
    'app.trell' :
    {
        'category' : "Lifestyle",
        'updated' : "06 Jun 2022",
        'App_name' : "Trell- Videos and Shopping App",
        'Package_name' : "app.trell",
        'installs' : "10,000,000+",
        'version' : "6.1.74",
    },
    'com.zhiliaoapp.musically' :
    {
        'category' : "Social and Communication",
        'updated' : "16 Jun 2022",
        'App_name' : "TikTok",
        'Package_name' : "com.zhiliaoapp.musically",
        'installs' : "1,000,000,000+",
        'version' : "Varies with device",
    },
    'de.whow.jackpot' :
    {
        'category' : "Casino",
        'updated' : "16 Jun 2022",
        'App_name' : "MyJackpot - Slots & Casino",
        'Package_name' : "de.whow.jackpot",
        'installs' : "1,000,000+",
        'version' : "4.13.38",
    },
    'net.talkmiracle' :
    {
        'category' : "Social and Communication",
        'updated' : "21 Oct 2021",
        'App_name' : "\u30df\u30e9\u30af\u30eb\u30c8\u30fc\u30af",
        'Package_name' : "net.talkmiracle",
        'installs' : "50,000+",
        'version' : "1.0.5",
    },
    'com.xinhuanet.xinhua_ko' :
    {
        'category' : "Education and Books",
        'updated' : "27 May 2022",
        'App_name' : "\ucc28\uc774\ub098 \ub274\uc2a4",
        'Package_name' : "com.xinhuanet.xinhua_ko",
        'installs' : "50,000+",
        'version' : "1.0.8",
    },
    'com.wycx.tw' :
    {
        'category' : "Games",
        'updated' : "24 Aug 2021",
        'App_name' : "\u9053\u53cb\u639b\u6a5f\u55ce",
        'Package_name' : "com.wycx.tw",
        'installs' : "100,000+",
        'version' : "1.0.27",
    },
    '1529842822' :
    {
        'category' : "Games",
        'updated' : "2022-04-19T02:25:26Z",
        'App_name' : "Long Th\u1ea7n Gi\u00e1ng Th\u1ebf",
        'Package_name' : "com.bdtt.vn",
        'version' : "2.3",
        'minimumOsVersion' : "9.0",
    },
    'ibtikar.haseel.user' :
    {
        'category' : "Food & Drink",
        'updated' : "23 May 2022",
        'App_name' : "Haseel - \u062d\u0635\u064a\u0644",
        'Package_name' : "ibtikar.haseel.user",
        'installs' : "100,000+",
        'version' : "1.3.55",
    },
    'com.vogo.android' :
    {
        'category' : "Shopping",
        'updated' : "07 Jun 2022",
        'App_name' : "\ucd08\ud2b9\uac00 \ub77c\uc774\ube0c \uc1fc\ud551 VOGO",
        'Package_name' : "com.vogo.android",
        'installs' : "500,000+",
        'version' : "3.0.85",
    },
    'com.Zonmob.ShadowofDeath.FightingGames' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "Shadow of Death: Offline Games",
        'Package_name' : "com.Zonmob.ShadowofDeath.FightingGames",
        'installs' : "10,000,000+",
        'version' : "1.101.2.8",
    },
    '1235655086' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-04T16:04:18Z",
        'App_name' : "Yboo - Video Chat Live Stream",
        'Package_name' : "ru.ifiwereaboy.taboo",
        'version' : "1.6.2",
        'minimumOsVersion' : "10.0",
    },
    'com.gamevil.arcanatactics.android.google.global.normal' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "Arcana Tactics",
        'Package_name' : "com.gamevil.arcanatactics.android.google.global.normal",
        'installs' : "500,000+",
        'version' : "1.7.0",
    },
    'com.mind.quiz.brain.out' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Brain Out: Can you pass it?",
        'Package_name' : "com.mind.quiz.brain.out",
        'installs' : "100,000,000+",
        'version' : "2.1.20",
    },
    'com.vangraaf.card' :
    {
        'category' : "Shopping",
        'updated' : "16 Jun 2022",
        'App_name' : "VAN GRAAF \u2013 Moda i styl \u017cycia",
        'Package_name' : "com.vangraaf.card",
        'installs' : "100,000+",
        'version' : "4.3.1",
    },
    '907394059' :
    {
        'category' : "Shopping",
        'updated' : "2022-05-12T15:09:27Z",
        'App_name' : "Myntra - Fashion Shopping App",
        'Package_name' : "com.myntra.Myntra",
        'version' : "4.2204.2",
        'minimumOsVersion' : "11.0",
    },
    'com.linecorp.LGGRTHN' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "LINE Let's Get Rich",
        'Package_name' : "com.linecorp.LGGRTHN",
        'installs' : "50,000,000+",
        'version' : "4.0.0",
    },
    'ru.instamart' :
    {
        'category' : "Food & Drink",
        'updated' : "14 Jun 2022",
        'App_name' : "\u0421\u0431\u0435\u0440\u041c\u0430\u0440\u043a\u0435\u0442: \u0414\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u043e\u0432",
        'Package_name' : "ru.instamart",
        'installs' : "10,000,000+",
        'version' : "6.25.6 (13375615)",
    },
    'com.alibaba.aliexpresshd' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "AliExpress",
        'Package_name' : "com.alibaba.aliexpresshd",
        'installs' : "500,000,000+",
        'version' : "8.49.0",
    },
    'com.sega.HokutoRevive' :
    {
        'category' : "Games",
        'updated' : "18 May 2022",
        'App_name' : "\u5317\u6597\u306e\u62f3 LEGENDS ReVIVE \u539f\u4f5c\u8ffd\u4f53\u9a13RPG\uff01",
        'Package_name' : "com.sega.HokutoRevive",
        'installs' : "100,000+",
        'version' : "3.9.0",
    },
    'com.intermiles.app' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "16 May 2022",
        'App_name' : "InterMiles: Get rewarded daily",
        'Package_name' : "com.intermiles.app",
        'installs' : "1,000,000+",
        'version' : "4.9.0",
    },
    'kr.carenation.protector' :
    {
        'category' : "Health and Fitness",
        'updated' : "09 Jun 2022",
        'App_name' : "\ucf00\uc5b4\ub124\uc774\uc158 - \uac04\ubcd1\uc778,\ubcd1\uc6d0 \ucc3e\uae30\ubd80\ud130 \ube44\ub300\uba74 \uacb0\uc81c\uae4c\uc9c0!",
        'Package_name' : "kr.carenation.protector",
        'installs' : "100,000+",
        'version' : "3.0.2",
    },
    'kr.co.dreamshopping.mcapp' :
    {
        'category' : "Shopping",
        'updated' : "24 May 2022",
        'App_name' : "\uc2e0\uc138\uacc4TV\uc1fc\ud551 \u2013 \uc2e0\uaddc\uac00\uc785\uace0\uac1d \ucd94\uac00\ud61c\ud0dd!",
        'Package_name' : "kr.co.dreamshopping.mcapp",
        'installs' : "5,000,000+",
        'version' : "2.2.41",
    },
    'jp.colopl.alice' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "\u30a2\u30ea\u30b9\u30fb\u30ae\u30a2\u30fb\u30a2\u30a4\u30ae\u30b9",
        'Package_name' : "jp.colopl.alice",
        'installs' : "100,000+",
        'version' : "1.54.1",
    },
    '385724144' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-14T06:04:55Z",
        'App_name' : "\u30db\u30c3\u30c8\u30da\u30c3\u30d1\u30fc\u30d3\u30e5\u30fc\u30c6\u30a3\u30fc/\u30b5\u30ed\u30f3\u4e88\u7d04",
        'Package_name' : "jp.co.recruit.mtl.beauty.salon",
        'version' : "6.51.0",
        'minimumOsVersion' : "13.0",
    },
    'net.pluservice.kintogo' :
    {
        'category' : "Utilities",
        'updated' : "22 Apr 2022",
        'App_name' : "KINTO Go - parking, trains, subway, buses, TAXIs",
        'Package_name' : "net.pluservice.kintogo",
        'installs' : "100,000+",
        'version' : "9.12.0",
    },
    '1531713996' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-08T20:15:35Z",
        'App_name' : "HOLODILNIK.RU: \u041c\u0435\u0433\u0430\u041c\u0430\u0440\u043a\u0435\u0442",
        'Package_name' : "ru.holodilnik.ios",
        'version' : "5.62.2",
        'minimumOsVersion' : "12.0",
    },
    '1145312849' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-19T09:32:06Z",
        'App_name' : "MeMe\u76f4\u64ad",
        'Package_name' : "chat.meme.inke",
        'version' : "4.6.5",
        'minimumOsVersion' : "12.0",
    },
    'jp.co.beeworks.nameko.dxkiwami' :
    {
        'category' : "Games",
        'updated' : "28 Mar 2022",
        'App_name' : "\u306a\u3081\u3053\u683d\u57f9\u30ad\u30c3\u30c8Deluxe \u6975 - \u7652\u3057\u306e\u653e\u7f6e\uff06\u80b2\u6210\u30b2\u30fc\u30e0",
        'Package_name' : "jp.co.beeworks.nameko.dxkiwami",
        'installs' : "500,000+",
        'version' : "1.3.13",
    },
    'com.dunzo.user' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Dunzo: Grocery Shopping & More",
        'Package_name' : "com.dunzo.user",
        'installs' : "10,000,000+",
        'version' : "3.45.0.4",
    },
    'jp.co.onetapbuy.jpstock' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "PayPay\u8a3c\u5238 1,000\u5186\u3067\u5927\u4f01\u696d\u306e\u682a\u4e3b\u306b",
        'Package_name' : "jp.co.onetapbuy.jpstock",
        'installs' : "100,000+",
        'version' : "2.2.7",
    },
    'com.tpb.mb.gprsandroid' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "TPBank Mobile",
        'Package_name' : "com.tpb.mb.gprsandroid",
        'installs' : "1,000,000+",
        'version' : "10.10.90",
    },
    'rs.pwn.zombielike' :
    {
        'category' : "Games",
        'updated' : "09 Nov 2021",
        'App_name' : "Diableros: Zombie RPG Shooter",
        'Package_name' : "rs.pwn.zombielike",
        'installs' : "1,000,000+",
        'version' : "0.8.163",
    },
    'com.mcdonalds.mobileapp' :
    {
        'category' : "Food & Drink",
        'updated' : "08 Jun 2022",
        'App_name' : "McDonald's",
        'Package_name' : "com.mcdonalds.mobileapp",
        'installs' : "50,000,000+",
        'version' : "2.43.0",
    },
    '568839295' :
    {
        'category' : "Education and Books",
        'updated' : "2022-06-13T15:12:01Z",
        'App_name' : "Blinkist: Big Ideas in 15 Min",
        'Package_name' : "com.blinkslabs.Blinkist",
        'version' : "7.0.0",
        'minimumOsVersion' : "13.0",
    },
    'com.btckorea.bithumb' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "Bithumb",
        'Package_name' : "com.btckorea.bithumb",
        'installs' : "5,000,000+",
        'version' : "2.5.1",
    },
    'com.saramart.android' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "SaraMart Shopping",
        'Package_name' : "com.saramart.android",
        'installs' : "10,000,000+",
        'version' : "3.1.2",
    },
    'com.hogangnono.hogangnono' :
    {
        'category' : "House & Home",
        'updated' : "08 Jun 2022",
        'App_name' : "\ud638\uac31\ub178\ub178 - \uc544\ud30c\ud2b8 \uc2e4\uac70\ub798\uac00 \uc870\ud68c \ubd80\ub3d9\uc0b0\uc571",
        'Package_name' : "com.hogangnono.hogangnono",
        'installs' : "1,000,000+",
        'version' : "1.9.5",
    },
    'com.landmarkgroup.centrepointstores' :
    {
        'category' : "Shopping",
        'updated' : "03 Jun 2022",
        'App_name' : "Centrepoint Online - \u0633\u0646\u062a\u0631\u0628\u0648\u064a\u0646\u062a",
        'Package_name' : "com.landmarkgroup.centrepointstores",
        'installs' : "5,000,000+",
        'version' : "7.45",
    },
    'jp.gungho.bm' :
    {
        'category' : "Games",
        'updated' : "10 May 2022",
        'App_name' : "\u30b5\u30e2\u30f3\u30ba\u30dc\u30fc\u30c9",
        'Package_name' : "jp.gungho.bm",
        'installs' : "1,000,000+",
        'version' : "8.4.4",
    },
    'com.igp.android' :
    {
        'category' : "Shopping",
        'updated' : "06 Jun 2022",
        'App_name' : "IGP: INDIA ki GIFTING SITE",
        'Package_name' : "com.igp.android",
        'installs' : "500,000+",
        'version' : "2.11",
    },
    'de.sheego.mobile' :
    {
        'category' : "Shopping",
        'updated' : "23 Mar 2022",
        'App_name' : "sheego - Mode in gro\u00dfen Gr\u00f6\u00dfen",
        'Package_name' : "de.sheego.mobile",
        'installs' : "500,000+",
        'version' : "1.20.0-signed",
    },
    '1121625572' :
    {
        'category' : "Social and Communication",
        'updated' : "2020-03-04T03:57:24Z",
        'App_name' : "\u51fa\u4f1a\u3044\u306f3\u5206 \u300c\u30a4\u30f3\u30b9\u30bf\u30f3\u30c8\u300dSNS\u30a2\u30d7\u30ea\u3067\u53cb\u9054\u4f5c\u308a",
        'Package_name' : "jp.appinstant.instant",
        'version' : "2.4.8",
        'minimumOsVersion' : "10.0",
    },
    'com.bplus.vtpay' :
    {
        'category' : "Finance",
        'updated' : "31 May 2022",
        'App_name' : "Viettel Money",
        'Package_name' : "com.bplus.vtpay",
        'installs' : "10,000,000+",
        'version' : "5.1.1",
    },
    'br.com.wine.app' :
    {
        'category' : "Food & Drink",
        'updated' : "08 Jun 2022",
        'App_name' : "Wine: Loja e Clube de Vinhos",
        'Package_name' : "br.com.wine.app",
        'installs' : "500,000+",
        'version' : "1.32.0",
    },
    'kr.co.ssg' :
    {
        'category' : "Shopping",
        'updated' : "02 Jun 2022",
        'App_name' : "SSG.COM",
        'Package_name' : "kr.co.ssg",
        'installs' : "10,000,000+",
        'version' : "3.1.8",
    },
    'com.squats.fittr' :
    {
        'category' : "Health and Fitness",
        'updated' : "09 Jun 2022",
        'App_name' : "FITTR: Fitness & Weight Loss",
        'Package_name' : "com.squats.fittr",
        'installs' : "1,000,000+",
        'version' : "8.2.3",
    },
    'com.vedantu.app' :
    {
        'category' : "Education and Books",
        'updated' : "02 Jun 2022",
        'App_name' : "Vedantu LIVE Learning App",
        'Package_name' : "com.vedantu.app",
        'installs' : "10,000,000+",
        'version' : "2.0.7",
    },
    'com.jeevansathi.android' :
    {
        'category' : "Social and Communication",
        'updated' : "15 Jun 2022",
        'App_name' : "Jeevansathi - Matrimonial App",
        'Package_name' : "com.jeevansathi.android",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'sg.bigo.live' :
    {
        'category' : "Social and Communication",
        'updated' : "17 Jun 2022",
        'App_name' : "Bigo Live - Live Streaming App",
        'Package_name' : "sg.bigo.live",
        'installs' : "100,000,000+",
        'version' : "5.24.3",
    },
    'in.burgerking.android' :
    {
        'category' : "Food & Drink",
        'updated' : "21 Apr 2022",
        'App_name' : "Burger King India",
        'Package_name' : "in.burgerking.android",
        'installs' : "1,000,000+",
        'version' : "3.5",
    },
    'com.pahina.reader' :
    {
        'category' : "Education and Books",
        'updated' : "27 Apr 2022",
        'App_name' : "Allnovel",
        'Package_name' : "com.pahina.reader",
        'installs' : "5,000,000+",
        'version' : "2.1.1",
    },
    'com.whitesquare.animaltransform' :
    {
        'category' : "Games",
        'updated' : "16 Jan 2022",
        'App_name' : "Animal Transform: Epic Race 3D",
        'Package_name' : "com.whitesquare.animaltransform",
        'installs' : "50,000,000+",
        'version' : "1.7",
    },
    'com.huiyu.mxcsgja1.android' :
    {
        'category' : "Games",
        'updated' : "17 Feb 2022",
        'App_name' : "\u96c4\u624d\u4e09\u56fd\uff5e\u96c4\u624d\u3067\u4e89\u3046\u6fc0\u52d5\u306e\u5929\u4e0b\uff5e",
        'Package_name' : "com.huiyu.mxcsgja1.android",
        'installs' : "10,000+",
        'version' : "48688.16",
    },
    'com.enuri.android' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "ENURI",
        'Package_name' : "com.enuri.android",
        'installs' : "1,000,000+",
        'version' : "4.0.8",
    },
    'com.hb.hungryhub' :
    {
        'category' : "Food & Drink",
        'updated' : "01 Jun 2022",
        'App_name' : "Hungry Hub - Dining Offer App",
        'Package_name' : "com.hb.hungryhub",
        'installs' : "100,000+",
        'version' : "5.15.0",
    },
    'com.rufilo.user' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "Small Business Loan: Rufilo",
        'Package_name' : "com.rufilo.user",
        'installs' : "5,000,000+",
        'version' : "2.2.0",
    },
    'com.notissimus.allinstruments.android' :
    {
        'category' : "Shopping",
        'updated' : "03 Jun 2022",
        'App_name' : "\u0412\u0441\u0435\u0418\u043d\u0441\u0442\u0440\u0443\u043c\u0435\u043d\u0442\u044b.\u0440\u0443",
        'Package_name' : "com.notissimus.allinstruments.android",
        'installs' : "1,000,000+",
        'version' : "2.35.1",
    },
    'com.akinon.fashfed' :
    {
        'category' : "Shopping",
        'updated' : "03 Jun 2022",
        'App_name' : "FashFed - Online Al\u0131\u015fveri\u015f",
        'Package_name' : "com.akinon.fashfed",
        'installs' : "100,000+",
        'version' : "1.5.1",
    },
    'com.truebill' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "Truebill Budget & Bill Tracker",
        'Package_name' : "com.truebill",
        'installs' : "1,000,000+",
        'version' : "5.2.7",
    },
    'com.cabs' :
    {
        'category' : "Utilities",
        'updated' : "08 Jun 2022",
        'App_name' : "13cabs",
        'Package_name' : "com.cabs",
        'installs' : "1,000,000+",
        'version' : "7.7.0",
    },
    'com.alien.shooter.galaxy.attack' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Galaxy Attack: Alien Shooting",
        'Package_name' : "com.alien.shooter.galaxy.attack",
        'installs' : "100,000,000+",
        'version' : "38.5",
    },
    'com.global.ztmslgkr' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "\ud37c\uc990 \uc624\ube0c Z",
        'Package_name' : "com.global.ztmslgkr",
        'installs' : "1,000,000+",
        'version' : "7.0.72",
    },
    'com.servicemagic.consumer' :
    {
        'category' : "House & Home",
        'updated' : "15 Jun 2022",
        'App_name' : "Angi: Hire Home Service Pros",
        'Package_name' : "com.servicemagic.consumer",
        'installs' : "5,000,000+",
        'version' : "22.23.0",
    },
    'jp.cocone.p2' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "\u30dd\u30b1\u30b3\u30ed\u30c4\u30a4\u30f3\u3000\u3075\u305f\u3054\u3092\u304b\u308f\u3044\u3044\u30a2\u30a4\u30c6\u30e0\u3067\u7740\u305b\u66ff\u3048\u3088\u3046\uff01",
        'Package_name' : "jp.cocone.p2",
        'installs' : "500,000+",
        'version' : "1.67.2",
    },
    'br.com.zeropaper.app' :
    {
        'category' : "Finance",
        'updated' : "24 Sep 2020",
        'App_name' : "QuickBooks ZeroPaper: Finan\u00e7as para empresas",
        'Package_name' : "br.com.zeropaper.app",
        'installs' : "1,000,000+",
        'version' : "3.18.5",
    },
    'com.lssj.ssjj' :
    {
        'category' : "Games",
        'updated' : "21 May 2021",
        'App_name' : "\u5929\u59ec\u7269\u8a9e",
        'Package_name' : "com.lssj.ssjj",
        'installs' : "500,000+",
        'version' : "0.25.0",
    },
    'com.koo.app' :
    {
        'category' : "Education and Books",
        'updated' : "17 Jun 2022",
        'App_name' : "Koo: Know what's happening!",
        'Package_name' : "com.koo.app",
        'installs' : "10,000,000+",
        'version' : "0.98.4",
    },
    'link.merge.puzzle.onnect.number' :
    {
        'category' : "Games",
        'updated' : "01 Jun 2022",
        'App_name' : "Tile Connect - Classic Match",
        'Package_name' : "link.merge.puzzle.onnect.number",
        'installs' : "50,000,000+",
        'version' : "1.16.1",
    },
    'de.myposter.myposterapp' :
    {
        'category' : "Entertainment",
        'updated' : "10 Jun 2022",
        'App_name' : "myposter - Photo Printing",
        'Package_name' : "de.myposter.myposterapp",
        'installs' : "100,000+",
        'version' : "6.0.2",
    },
    'com.onepunchman.ggplay.sea' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "ONE PUNCH MAN: The Strongest",
        'Package_name' : "com.onepunchman.ggplay.sea",
        'installs' : "5,000,000+",
        'version' : "1.3.9",
    },
    '889723827' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-05-09T18:39:18Z",
        'App_name' : "ClickBus - Passagens de \u00d4nibus",
        'Package_name' : "com.clickbus.mobile",
        'version' : "2.47.0",
        'minimumOsVersion' : "11.0",
    },
    'com.netmarble.nanatsunotaizai' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\u4e03\u3064\u306e\u5927\u7f6a \u5149\u3068\u95c7\u306e\u4ea4\u6226 : \u30b0\u30e9\u30af\u30ed",
        'Package_name' : "com.netmarble.nanatsunotaizai",
        'installs' : "1,000,000+",
        'version' : "2.4.3",
    },
    'com.unicostudio.whois' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "Who is? Brain Teaser & Riddles",
        'Package_name' : "com.unicostudio.whois",
        'installs' : "50,000,000+",
        'version' : "1.5.2",
    },
    'jp.co.stockpoint.connectsp' :
    {
        'category' : "Finance",
        'updated' : "15 Apr 2022",
        'App_name' : "\u30dd\u30a4\u30f3\u30c8\u904b\u7528\u30fbStockPoint for CONNECT",
        'Package_name' : "jp.co.stockpoint.connectsp",
        'installs' : "100,000+",
        'version' : "1.1.28",
    },
    'com.fbs.tpand' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "FBS Trader \u2014 Trading Platform",
        'Package_name' : "com.fbs.tpand",
        'installs' : "5,000,000+",
        'version' : "1.49.0",
    },
    '993529737' :
    {
        'category' : "Games",
        'updated' : "2022-06-14T02:00:42Z",
        'App_name' : "\u30af\u30ea\u30b9\u30bf\u30eb \u30aa\u30d6 \u30ea\u30e6\u30cb\u30aa\u30f3",
        'Package_name' : "jp.co.gu3.orange01",
        'version' : "6.0.30",
        'minimumOsVersion' : "11.0",
    },
    'com.marleyspoon' :
    {
        'category' : "Food & Drink",
        'updated' : "12 Jun 2022",
        'App_name' : "Martha & Marley Spoon",
        'Package_name' : "com.marleyspoon",
        'installs' : "100,000+",
        'version' : "3.5.0",
    },
    'aptip.app' :
    {
        'category' : "Lifestyle",
        'updated' : "09 Jun 2022",
        'App_name' : "\uc544\ud30c\ud2b8\uc544\uc774",
        'Package_name' : "aptip.app",
        'installs' : "1,000,000+",
        'version' : "2.1.8",
    },
    'com.canadiantire.triangle' :
    {
        'category' : "Shopping",
        'updated' : "01 Jun 2022",
        'App_name' : "Triangle",
        'Package_name' : "com.canadiantire.triangle",
        'installs' : "1,000,000+",
        'version' : "3.4.8",
    },
    'com.gamania.twwod' :
    {
        'category' : "Games",
        'updated' : "09 Oct 2021",
        'App_name' : "\u9f8d\u4e4b\u8c37\uff1a\u65b0\u4e16\u754c",
        'Package_name' : "com.gamania.twwod",
        'installs' : "100,000+",
        'version' : "2.2.0",
    },
    'com.nativecamp.nativecamp' :
    {
        'category' : "Education and Books",
        'updated' : "24 May 2022",
        'App_name' : "NativeCamp. - English Online",
        'Package_name' : "com.nativecamp.nativecamp",
        'installs' : "100,000+",
        'version' : "Varies with device",
    },
    'com.infinitylearn.learn' :
    {
        'category' : "Education and Books",
        'updated' : "05 Jun 2022",
        'App_name' : "Infinity Learn - Learning App",
        'Package_name' : "com.infinitylearn.learn",
        'installs' : "100,000+",
        'version' : "1.0.14",
    },
    'com.aswat.carrefouruae' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "MAF Carrefour Online Shopping",
        'Package_name' : "com.aswat.carrefouruae",
        'installs' : "10,000,000+",
        'version' : "16.2.1",
    },
    'com.datemyage' :
    {
        'category' : "Social and Communication",
        'updated' : "24 May 2022",
        'App_name' : "DateMyAge\u2122 - Mature Dating 40+",
        'Package_name' : "com.datemyage",
        'installs' : "1,000,000+",
        'version' : "8.70.100",
    },
    'ru.mts.mtstv' :
    {
        'category' : "Entertainment",
        'updated' : "08 Jun 2022",
        'App_name' : "KION \u2013 \u0444\u0438\u043b\u044c\u043c\u044b, \u0441\u0435\u0440\u0438\u0430\u043b\u044b \u0438 \u0442\u0432",
        'Package_name' : "ru.mts.mtstv",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.popsworldwide.popsapp' :
    {
        'category' : "Entertainment",
        'updated' : "08 Jun 2022",
        'App_name' : "POPS - Films, Anime, Comics",
        'Package_name' : "com.popsworldwide.popsapp",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.transsnet.news.more.ngblog' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "MORE: trends, meme & entertainment for Nigerian",
        'Package_name' : "com.transsnet.news.more.ngblog",
        'installs' : "5,000,000+",
        'version' : "4.4.6",
    },
    'com.bandainamcoent.seiyazbjp' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "\u8056\u95d8\u58eb\u661f\u77e2 \u30be\u30c7\u30a3\u30a2\u30c3\u30af \u30d6\u30ec\u30a4\u30d6",
        'Package_name' : "com.bandainamcoent.seiyazbjp",
        'installs' : "100,000+",
        'version' : "2.27",
    },
    'taxi.android.client' :
    {
        'category' : "Utilities",
        'updated' : "15 Jun 2022",
        'App_name' : "FREE NOW",
        'Package_name' : "taxi.android.client",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.arammeem.android.apps.toyou' :
    {
        'category' : "Food & Drink",
        'updated' : "10 Apr 2022",
        'App_name' : "ToYou - Delivery & More",
        'Package_name' : "com.arammeem.android.apps.toyou",
        'installs' : "1,000,000+",
        'version' : "1.65.4",
    },
    'com.centraldepartment.app' :
    {
        'category' : "Shopping",
        'updated' : "09 Jun 2022",
        'App_name' : "Central App - Shopping Online",
        'Package_name' : "com.centraldepartment.app",
        'installs' : "1,000,000+",
        'version' : "4.26.1",
    },
    'com.ipixelcorp.howfitapp' :
    {
        'category' : "Health and Fitness",
        'updated' : "23 Mar 2022",
        'App_name' : "\ud558\uc6b0\ud54f - \uc6b4\ub3d9, \ub2e4\uc774\uc5b4\ud2b8\ub97c \uc704\ud55c AI \ud648\ud2b8\uc571",
        'Package_name' : "com.ipixelcorp.howfitapp",
        'installs' : "100,000+",
        'version' : "1.0.6",
    },
    'com.myzl.alice' :
    {
        'category' : "Games",
        'updated' : "08 Apr 2021",
        'App_name' : "\u547d\u904b\u4e4b\u8f2a",
        'Package_name' : "com.myzl.alice",
        'installs' : "100,000+",
        'version' : "1.1.8",
    },
    'com.sjsg4399kr.google' :
    {
        'category' : "Games",
        'updated' : "24 Apr 2022",
        'App_name' : "\uc0bc\uad6d\uc9c0Global",
        'Package_name' : "com.sjsg4399kr.google",
        'installs' : "100,000+",
        'version' : "1.18.6",
    },
    'com.yoo.jpand.sgdh' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2020",
        'App_name' : "\u4e09\u56fd\u935b\u51b6\u7269\u8a9e\uff5e\u6700\u9ad8\u306e\u5546\u4f1a\u3092\u76ee\u6307\u305b\uff5e~",
        'Package_name' : "com.yoo.jpand.sgdh",
        'installs' : "10,000+",
        'version' : "2.0.0",
    },
    'com.olacabs.customer' :
    {
        'category' : "Utilities",
        'updated' : "13 Jun 2022",
        'App_name' : "Ola, Safe and affordable rides",
        'Package_name' : "com.olacabs.customer",
        'installs' : "100,000,000+",
        'version' : "5.6.9",
    },
    'com.indie.zll2.google.ft' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "\u4e5d\u5929\u8a23\u2014\u99ac\u4e0a\u4f86\u6230",
        'Package_name' : "com.indie.zll2.google.ft",
        'installs' : "50,000+",
        'version' : "42.0.0",
    },
    'com.iherb' :
    {
        'category' : "Shopping",
        'updated' : "07 Jun 2022",
        'App_name' : "iHerb - Shop and Save",
        'Package_name' : "com.iherb",
        'installs' : "10,000,000+",
        'version' : "8.5.0606",
    },
    '1201717043' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-17T11:44:28Z",
        'App_name' : "Ounass Luxury Shopping \u0627\u064f\u0646\u0627\u0633",
        'Package_name' : "com.at.ounass",
        'version' : "2.81.0",
        'minimumOsVersion' : "13.0",
    },
    '1439776386' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-09T07:01:58Z",
        'App_name' : "Jogo de Pintar por Numero",
        'Package_name' : "com.veraxen.colorbynumber.oilpainting",
        'version' : "1.22.0",
        'minimumOsVersion' : "10.0",
    },
    'com.lightricks.pixaloop' :
    {
        'category' : "Entertainment",
        'updated' : "22 May 2022",
        'App_name' : "Motionleap by Lightricks",
        'Package_name' : "com.lightricks.pixaloop",
        'installs' : "50,000,000+",
        'version' : "1.3.11",
    },
    'com.linkfun.sgzs.kor' :
    {
        'category' : "Games",
        'updated' : "17 May 2020",
        'App_name' : "3\ubd84\ucc9c\ud558",
        'Package_name' : "com.linkfun.sgzs.kor",
        'installs' : "5,000+",
        'version' : "1.0.0",
    },
    'com.beek' :
    {
        'category' : "Education and Books",
        'updated' : "17 Feb 2022",
        'App_name' : "Beek: Audiolibros y Podcasts",
        'Package_name' : "com.beek",
        'installs' : "1,000,000+",
        'version' : "7.0.58",
    },
    'dk.meny' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "MENYkortet",
        'Package_name' : "dk.meny",
        'installs' : "100,000+",
        'version' : "7.3.1",
    },
    'com.freshtohome' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Fresh To Home - Meat Delivery",
        'Package_name' : "com.freshtohome",
        'installs' : "5,000,000+",
        'version' : "4.2.2",
    },
    'com.mobigame.cuongma2' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "Giang H\u1ed3 Ng\u0169 Tuy\u1ec7t",
        'Package_name' : "com.mobigame.cuongma2",
        'installs' : "1,000,000+",
        'version' : "1.0.29",
    },
    'com.evogames.slavsforvk' :
    {
        'category' : "Games",
        'updated' : "04 Jun 2022",
        'App_name' : "Harvest Land",
        'Package_name' : "com.evogames.slavsforvk",
        'installs' : "10,000,000+",
        'version' : "1.12.5",
    },
    'com.hungama.myplay.activity' :
    {
        'category' : "Entertainment",
        'updated' : "07 Jun 2022",
        'App_name' : "Hungama Music - Stream & Download MP3 Songs",
        'Package_name' : "com.hungama.myplay.activity",
        'installs' : "10,000,000+",
        'version' : "5.2.35",
    },
    'com.zain.pay' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "Tamam Finance - \u062a\u0645\u0627\u0645 \u0644\u0644\u062a\u0645\u0648\u064a\u0644",
        'Package_name' : "com.zain.pay",
        'installs' : "100,000+",
        'version' : "2.1.0",
    },
    'com.ctcmediagroup.videomore' :
    {
        'category' : "Entertainment",
        'updated' : "15 Jun 2022",
        'App_name' : "more.tv - \u0422\u0412, \u0444\u0438\u043b\u044c\u043c\u044b \u0438 \u0441\u0435\u0440\u0438\u0430\u043b\u044b",
        'Package_name' : "com.ctcmediagroup.videomore",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.mandicmagic.android' :
    {
        'category' : "Social and Communication",
        'updated' : "15 Feb 2022",
        'App_name' : "WiFi Magic by Mandic Passwords",
        'Package_name' : "com.mandicmagic.android",
        'installs' : "10,000,000+",
        'version' : "4.7.1",
    },
    'com.royal.cut.rrcutbank' :
    {
        'category' : "Games",
        'updated' : "14 Dec 2021",
        'App_name' : "Royal Cut Money",
        'Package_name' : "com.royal.cut.rrcutbank",
        'installs' : "1,000,000+",
        'version' : "1.0.2",
    },
    'com.paysend.app' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Money Transfer App Paysend",
        'Package_name' : "com.paysend.app",
        'installs' : "5,000,000+",
        'version' : "4.1.18",
    },
    'com.Tonwa.Cash7' :
    {
        'category' : "Casino",
        'updated' : "30 May 2022",
        'App_name' : "\u9322\u8857Online-\u8001\u864e\u6a5f\u3001\u6355\u9b5a\u3001\u767e\u5bb6\u6a02\u3001\u9ab0\u5bf6\u3001\u8cfd\u99ac\u3001\u67cf\u9752\u65af\u6d1b",
        'Package_name' : "com.Tonwa.Cash7",
        'installs' : "500,000+",
        'version' : "1.1.87",
    },
    'cl.bci.sismo.mach' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "MACH - Tarjeta Prepago.",
        'Package_name' : "cl.bci.sismo.mach",
        'installs' : "1,000,000+",
        'version' : "3.39.2",
    },
    'gg.squishy.ouch.asmr' :
    {
        'category' : "Games",
        'updated' : "14 May 2022",
        'App_name' : "Squishy Ouch: Squeeze Them!",
        'Package_name' : "gg.squishy.ouch.asmr",
        'installs' : "1,000,000+",
        'version' : "1.9.7",
    },
    'com.bilibilijp.horcruxcollege' :
    {
        'category' : "Games",
        'updated' : "26 Oct 2021",
        'App_name' : "\u9b42\u5668\u5b66\u9662-HorcruxCollege",
        'Package_name' : "com.bilibilijp.horcruxcollege",
        'installs' : "50,000+",
        'version' : "1.6.3",
    },
    'com.pang.jd2.google' :
    {
        'category' : "Games",
        'updated' : "02 Apr 2020",
        'App_name' : "\ud3ed\uad70",
        'Package_name' : "com.pang.jd2.google",
        'installs' : "10,000+",
        'version' : "1.0.4",
    },
    '734946693' :
    {
        'category' : "Games",
        'updated' : "2022-06-14T05:33:15Z",
        'App_name' : "\u30e1\u30eb\u30af\u30b9\u30c8\u30fc\u30ea\u30a2 - \u7652\u8853\u58eb\u3068\u9418\u306e\u97f3\u8272 -",
        'Package_name' : "jp.co.happyelements.toto",
        'version' : "3.18.0",
        'minimumOsVersion' : "10.0",
    },
    'com.arumgames.fantasytown' :
    {
        'category' : "Games",
        'updated' : "10 Mar 2022",
        'App_name' : "FantasyTown",
        'Package_name' : "com.arumgames.fantasytown",
        'installs' : "500,000+",
        'version' : "1.5.3",
    },
    'com.shopee.vn' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Shopee 6.6 Mua S\u1eafm Gi\u1eefa N\u0103m",
        'Package_name' : "com.shopee.vn",
        'installs' : "50,000,000+",
        'version' : "2.88.41",
    },
    'me.com.easytaxi' :
    {
        'category' : "Utilities",
        'updated' : "05 Jun 2022",
        'App_name' : "Jeeny - \u062c\u064a\u0646\u064a",
        'Package_name' : "me.com.easytaxi",
        'installs' : "1,000,000+",
        'version' : "19.16.2",
    },
    'com.nttdocomo.android.dmenunews' :
    {
        'category' : "Education and Books",
        'updated' : "13 Jun 2022",
        'App_name' : "dmenu\u30cb\u30e5\u30fc\u30b9\u3000\u6700\u65b0\u30cb\u30e5\u30fc\u30b9\u3084\u5730\u57df\u3001\u9632\u707d\u60c5\u5831\u3082\uff01",
        'Package_name' : "com.nttdocomo.android.dmenunews",
        'installs' : "5,000,000+",
        'version' : "3.20.0",
    },
    'com.fugo.wow' :
    {
        'category' : "Games",
        'updated' : "06 Jun 2022",
        'App_name' : "Words of Wonders: Crossword",
        'Package_name' : "com.fugo.wow",
        'installs' : "100,000,000+",
        'version' : "3.7.1",
    },
    'com.webzen.muonlineh5.google' :
    {
        'category' : "Games",
        'updated' : "18 Dec 2019",
        'App_name' : "\ubba4\uc628\ub77c\uc778H5",
        'Package_name' : "com.webzen.muonlineh5.google",
        'installs' : "100,000+",
        'version' : "1.6",
    },
    '1497273572' :
    {
        'category' : "Games",
        'updated' : "2020-07-16T02:30:49Z",
        'App_name' : "\u30c4\u30c3\u30b3\u30df\u738b\uff01\u5909\u306a\u753b\u50cf\u306b\u306a\u3093\u3067\u3084\u306d\u3093\uff01",
        'Package_name' : "com.mask.nandeyanen",
        'version' : "1.0.6",
        'minimumOsVersion' : "10.0",
    },
    '1269540854' :
    {
        'category' : "Games",
        'updated' : "2022-05-23T05:25:18Z",
        'App_name' : "Crossword Jam - Fun Word Games",
        'Package_name' : "in.playsimple.tripcross",
        'version' : "1.254.0",
        'minimumOsVersion' : "10.0",
    },
    '1521412306' :
    {
        'category' : "Games",
        'updated' : "2022-05-25T02:02:55Z",
        'App_name' : "\uc544\ub974\uce74\ub098 \ud0dd\ud2f1\uc2a4",
        'Package_name' : "com.gamevil.arcanatactics.ios.apple.global.normal",
        'version' : "1.7.0",
        'minimumOsVersion' : "10.0",
    },
    'com.area.winin' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "Winin App",
        'Package_name' : "com.area.winin",
        'installs' : "100,000+",
        'version' : "3.4.0",
    },
    'jp.cocone.pocketcolony' :
    {
        'category' : "Social and Communication",
        'updated' : "16 Jun 2022",
        'App_name' : "\u30dd\u30b1\u30b3\u30ed \u304b\u308f\u3044\u3044\u30a2\u30d0\u30bf\u30fc\u3067\u697d\u3057\u3080\u304d\u305b\u304b\u3048\u30b2\u30fc\u30e0",
        'Package_name' : "jp.cocone.pocketcolony",
        'installs' : "1,000,000+",
        'version' : "9.15.0",
    },
    'com.bengalimatrimony' :
    {
        'category' : "Social and Communication",
        'updated' : "30 May 2022",
        'App_name' : "Bengali Matrimony\u00ae -Shaadi App",
        'Package_name' : "com.bengalimatrimony",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'de.friendscout24.android.messaging' :
    {
        'category' : "Social and Communication",
        'updated' : "08 Jun 2022",
        'App_name' : "LoveScout24: Flirten & Chatten",
        'Package_name' : "de.friendscout24.android.messaging",
        'installs' : "1,000,000+",
        'version' : "5.74.0",
    },
    'id.moxa' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Moxa - My Financial Companion",
        'Package_name' : "id.moxa",
        'installs' : "5,000,000+",
        'version' : "3.0.3",
    },
    'com.funtomic.matchmasters' :
    {
        'category' : "Games",
        'updated' : "04 Jun 2022",
        'App_name' : "Match Masters",
        'Package_name' : "com.funtomic.matchmasters",
        'installs' : "10,000,000+",
        'version' : "4.022",
    },
    'br.project.pine' :
    {
        'category' : "Shopping",
        'updated' : "02 Jun 2022",
        'App_name' : "Chama: Botij\u00e3o de G\u00e1s Online",
        'Package_name' : "br.project.pine",
        'installs' : "5,000,000+",
        'version' : "32.18.342",
    },
    'com.endressdreamky.loveting' :
    {
        'category' : "Social and Communication",
        'updated' : "06 May 2022",
        'App_name' : "\ub108\ub791\ub098\ub791 \uc18c\uac1c\ud305 - \ub9e4\uc77c16\uba85 \uc18c\uac1c\ud305,\uc5f0\uc560,\ub77c\uc774\ube0c\ucc44\ud305",
        'Package_name' : "com.endressdreamky.loveting",
        'installs' : "1,000,000+",
        'version' : "7.1.5",
    },
    'com.simutronics.b17' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "SIEGE: World War II",
        'Package_name' : "com.simutronics.b17",
        'installs' : "1,000,000+",
        'version' : "2.0.43",
    },
    'com.vostic.android' :
    {
        'category' : "Entertainment",
        'updated' : "09 Jun 2022",
        'App_name' : "Lemo: Live, Chat, Party Online!",
        'Package_name' : "com.vostic.android",
        'installs' : "5,000,000+",
        'version' : "1.63.1",
    },
    '958763157' :
    {
        'category' : "Games",
        'updated' : "2022-05-17T19:13:49Z",
        'App_name' : "War Dragons",
        'Package_name' : "com.pocketgems.dragon",
        'version' : "7.00",
        'minimumOsVersion' : "8.0",
    },
    '1080263104' :
    {
        'category' : "Games",
        'updated' : "2022-03-21T23:39:05Z",
        'App_name' : "\u30af\u30ec\u30fc\u30f3\u30b2\u30fc\u30e0\u300c\u307d\u3061\u304f\u308c\u300d\u30aa\u30f3\u30e9\u30a4\u30f3\u30af\u30ec\u30fc\u30f3\u30b2\u30fc\u30e0\u30fb\u30aa\u30f3\u30af\u30ec",
        'Package_name' : "com.pochicrane",
        'version' : "3.3.0",
        'minimumOsVersion' : "12.0",
    },
    'com.fg.ssaos' :
    {
        'category' : "Games",
        'updated' : "19 May 2022",
        'App_name' : "Super String",
        'Package_name' : "com.fg.ssaos",
        'installs' : "100,000+",
        'version' : "1.0.32",
    },
    'com.nestle.tr.nescafe.nescafe3in1loyaltyapp' :
    {
        'category' : "Food & Drink",
        'updated' : "03 Jun 2022",
        'App_name' : "Nescaf\u00e9 3\u00fc1 Arada NE'APP",
        'Package_name' : "com.nestle.tr.nescafe.nescafe3in1loyaltyapp",
        'installs' : "500,000+",
        'version' : "1.0.22",
    },
    'com.ochkarikrn' :
    {
        'category' : "Shopping",
        'updated' : "04 Apr 2022",
        'App_name' : "\u041e\u0447\u043a\u0430\u0440\u0438\u043a - \u043b\u0438\u043d\u0437\u044b \u0438 \u043e\u0447\u043a\u0438",
        'Package_name' : "com.ochkarikrn",
        'installs' : "50,000+",
        'version' : "4.0.6",
    },
    'com.gelopie.app' :
    {
        'category' : "Food & Drink",
        'updated' : "04 May 2022",
        'App_name' : "Gelopie - Torte gelato on demand",
        'Package_name' : "com.gelopie.app",
        'installs' : "5,000+",
        'version' : "3.2.5",
    },
    '1445068497' :
    {
        'category' : "Games",
        'updated' : "2022-05-17T01:30:27Z",
        'App_name' : "Disney Sorcerer's Arena",
        'Package_name' : "com.glu.disneygame",
        'version' : "22.0",
        'minimumOsVersion' : "11.0",
    },
    'biz.kickass.kenmago' :
    {
        'category' : "Games",
        'updated' : "27 Apr 2020",
        'App_name' : "\u8ce2\u8005\u306e\u5b6b\u301c\u7a76\u6975\u9b54\u6cd5\u4f1d\u8aac\u301c[\u30ec\u30b8\u30de\u30b8]",
        'Package_name' : "biz.kickass.kenmago",
        'installs' : "50,000+",
        'version' : "1.0.12",
    },
    'jp.co.koeitecmo.Victory' :
    {
        'category' : "Games",
        'updated' : "25 Mar 2021",
        'App_name' : "\u5927\u822a\u6d77\u6642\u4ee3\uff16\uff1a\u30a6\u30df\u30ed\u30af",
        'Package_name' : "jp.co.koeitecmo.Victory",
        'installs' : "50,000+",
        'version' : "1.27.1",
    },
    'com.noonEdu.k12App' :
    {
        'category' : "Education and Books",
        'updated' : "10 Jun 2022",
        'App_name' : "Noon Academy \u2013 Student Learning App",
        'Package_name' : "com.noonEdu.k12App",
        'installs' : "10,000,000+",
        'version' : "4.6.37",
    },
    '1500534843' :
    {
        'category' : "Social and Communication",
        'updated' : "2021-12-29T08:46:46Z",
        'App_name' : "\u51fa\u4f1a\u3044\u63a2\u3057\u306f \u3042\u3063\u3061\u3083\u304a\u53cb\u9054\u30de\u30c3\u30c1\u30f3\u30b0",
        'Package_name' : "com.funtimetalk.app.free",
        'version' : "2.4",
        'minimumOsVersion' : "11.0",
    },
    '1114214294' :
    {
        'category' : "Games",
        'updated' : "2022-05-05T17:08:30Z",
        'App_name' : "Solitaire Cube - Win Real Cash",
        'Package_name' : "com.tether.solitaire.klondike",
        'version' : "1.47",
        'minimumOsVersion' : "12.0",
    },
    'com.gameone.tbm' :
    {
        'category' : "Games",
        'updated' : "14 Apr 2022",
        'App_name' : "\u53e4\u60d1\u4ed4M\uff1a\u53f0\u7063\u9ed1\u5e6b\u6559\u7236\u9285\u947c\u7063\u63d2\u65d7",
        'Package_name' : "com.gameone.tbm",
        'installs' : "50,000+",
        'version' : "2.0.9.4",
    },
    'com.ludumart.billiardspoolarena' :
    {
        'category' : "Games",
        'updated' : "24 May 2022",
        'App_name' : "Billiards Pool Arena",
        'Package_name' : "com.ludumart.billiardspoolarena",
        'installs' : "100,000+",
        'version' : "2.3.2",
    },
    'com.pozitron.pegasus' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "16 Jun 2022",
        'App_name' : "Book Flight Tickets by Pegasus",
        'Package_name' : "com.pozitron.pegasus",
        'installs' : "5,000,000+",
        'version' : "2.24.0",
    },
    'com.getbux.android.stocks' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "BUX Zero: Stock Investing",
        'Package_name' : "com.getbux.android.stocks",
        'installs' : "500,000+",
        'version' : "5.11",
    },
    'com.sideline.phone.number' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Sideline - 2nd Line for Work",
        'Package_name' : "com.sideline.phone.number",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'jp.co.thirtysevend.dive' :
    {
        'category' : "Social and Communication",
        'updated' : "25 Jan 2022",
        'App_name' : "\u30da\u30a2\u30d5\u30eb - \u81ea\u5206\u3089\u3057\u3055\u3067\u3064\u306a\u304c\u308b\u51fa\u4f1a\u3044\u30fb\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea",
        'Package_name' : "jp.co.thirtysevend.dive",
        'installs' : "100,000+",
        'version' : "1.4.5",
    },
    '1300508857' :
    {
        'category' : "Games",
        'updated' : "2022-06-15T06:44:50Z",
        'App_name' : "\u30ef\u30fc\u30eb\u30c9\u30d5\u30ea\u30c3\u30d1\u30fc(WORLD FLIPPER)",
        'Package_name' : "air.jp.co.cygames.worldflipper",
        'version' : "1.522.1",
        'minimumOsVersion' : "12.0",
    },
    'com.kubi.kucoin' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "KuCoin: BTC, Crypto Exchange",
        'Package_name' : "com.kubi.kucoin",
        'installs' : "5,000,000+",
        'version' : "3.59.0",
    },
    'jp.gungho.teppen' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "TEPPEN",
        'Package_name' : "jp.gungho.teppen",
        'installs' : "1,000,000+",
        'version' : "4.2.5",
    },
    'com.flexiloan' :
    {
        'category' : "Finance",
        'updated' : "23 Mar 2022",
        'App_name' : "FlexiLoans: Instant Small Business Loans in India",
        'Package_name' : "com.flexiloan",
        'installs' : "500,000+",
        'version' : "82.4.8",
    },
    'com.kai_membership' :
    {
        'category' : "Entertainment",
        'updated' : "14 May 2022",
        'App_name' : "Dorepoc",
        'Package_name' : "com.kai_membership",
        'installs' : "100,000+",
        'version' : "2.4",
    },
    'de.tui.tuicom' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "08 Jun 2022",
        'App_name' : "TUI.com Reisen - Urlaub, Hotels & Fl\u00fcge buchen",
        'Package_name' : "de.tui.tuicom",
        'installs' : "100,000+",
        'version' : "14.2.53",
    },
    'com.tiltingpoint.spongebob' :
    {
        'category' : "Games",
        'updated' : "28 May 2022",
        'App_name' : "SpongeBob: Krusty Cook-Off",
        'Package_name' : "com.tiltingpoint.spongebob",
        'installs' : "10,000,000+",
        'version' : "4.5.7",
    },
    'com.bankbazaar.app' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "CreditScore, Loans, CreditCard",
        'Package_name' : "com.bankbazaar.app",
        'installs' : "5,000,000+",
        'version' : "2.3.22",
    },
    'com.feels' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "Feels - dating & friends",
        'Package_name' : "com.feels",
        'installs' : "100,000+",
        'version' : "3.17.0",
    },
    'com.Danbient.Lobelia' :
    {
        'category' : "Games",
        'updated' : "30 Apr 2022",
        'App_name' : "LOBELIA \u2013 Collective RPG",
        'Package_name' : "com.Danbient.Lobelia",
        'installs' : "50,000+",
        'version' : "1.8.2",
    },
    '723134859' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-09T14:20:42Z",
        'App_name' : "Rakuten: Cash Back & deals",
        'Package_name' : "com.ebates.EbatesMobile",
        'version' : "9.32.0",
        'minimumOsVersion' : "13.0",
    },
    'com.aman.milov' :
    {
        'category' : "Social and Communication",
        'updated' : "20 May 2022",
        'App_name' : "Milov",
        'Package_name' : "com.aman.milov",
        'installs' : "50,000+",
        'version' : "1.0.30",
    },
    'com.yoinsapp' :
    {
        'category' : "Shopping",
        'updated' : "21 Jan 2022",
        'App_name' : "YOINS-fashion clothing-your wardrobe",
        'Package_name' : "com.yoinsapp",
        'installs' : "1,000,000+",
        'version' : "6.20.2",
    },
    'jp.naver.linemanga.android' :
    {
        'category' : "Education and Books",
        'updated' : "02 Jun 2022",
        'App_name' : "LINE\u30de\u30f3\u30ac",
        'Package_name' : "jp.naver.linemanga.android",
        'installs' : "10,000,000+",
        'version' : "6.13.1",
    },
    'net.nomocca' :
    {
        'category' : "Food & Drink",
        'updated' : "19 Mar 2020",
        'App_name' : "nomocca - (\u306e\u3082\u3063\u304b) \u304a\u5f97\u306a\u5c45\u9152\u5c4b\u30a2\u30d7\u30ea",
        'Package_name' : "net.nomocca",
        'installs' : "10,000+",
        'version' : "2.1.0",
    },
    'com.playspare.watersort3d' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Color Water Sort Puzzle Games",
        'Package_name' : "com.playspare.watersort3d",
        'installs' : "10,000,000+",
        'version' : "1.0.4",
    },
    'jp.gungho.padKO' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "\ud37c\uc990&\ub4dc\ub798\uace4\uc988(Puzzle & Dragons)",
        'Package_name' : "jp.gungho.padKO",
        'installs' : "1,000,000+",
        'version' : "19.9.4",
    },
    'com.crowdstar.covetfashion' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Covet Fashion - Dress Up Game",
        'Package_name' : "com.crowdstar.covetfashion",
        'installs' : "10,000,000+",
        'version' : "22.07.43",
    },
    'vn.thuongkhungchikiem.funtap' :
    {
        'category' : "Games",
        'updated' : "16 Apr 2021",
        'App_name' : "Th\u01b0\u01a1ng Khung Chi Ki\u1ebfm - Thuong Khung Chi Kiem",
        'Package_name' : "vn.thuongkhungchikiem.funtap",
        'installs' : "100,000+",
        'version' : "1.0.15",
    },
    'com.dsmart.blu.android' :
    {
        'category' : "Entertainment",
        'updated' : "01 Jun 2022",
        'App_name' : "BluTV",
        'Package_name' : "com.dsmart.blu.android",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.kr.angels.google' :
    {
        'category' : "Games",
        'updated' : "01 Oct 2021",
        'App_name' : "\uadf8\ub791\uc5d4\uc824",
        'Package_name' : "com.kr.angels.google",
        'installs' : "100,000+",
        'version' : "1.0.24",
    },
    '1446028015' :
    {
        'category' : "Games",
        'updated' : "2022-05-17T14:27:57Z",
        'App_name' : "Betsperts",
        'Package_name' : "com.betsperts",
        'version' : "2.8.10",
        'minimumOsVersion' : "15.1",
    },
    'com.longtukorea.yulgang' :
    {
        'category' : "Games",
        'updated' : "31 Aug 2021",
        'App_name' : "\uc5f4\ud608\uac15\ud638",
        'Package_name' : "com.longtukorea.yulgang",
        'installs' : "1,000,000+",
        'version' : "1.0.42",
    },
    'com.utransfer.utransferview' :
    {
        'category' : "Finance",
        'updated' : "24 Nov 2021",
        'App_name' : "UTRANSFER",
        'Package_name' : "com.utransfer.utransferview",
        'installs' : "10,000+",
        'version' : "2.4",
    },
    'com.nextbillion.groww' :
    {
        'category' : "Finance",
        'updated' : "11 Jun 2022",
        'App_name' : "Groww: Stocks & Mutual Fund",
        'Package_name' : "com.nextbillion.groww",
        'installs' : "10,000,000+",
        'version' : "15.17",
    },
    'com.wood.block.puzzle.winner.game' :
    {
        'category' : "Games",
        'updated' : "04 Nov 2021",
        'App_name' : "Block Puzzle: Wood Winner",
        'Package_name' : "com.wood.block.puzzle.winner.game",
        'installs' : "1,000,000+",
        'version' : "1.3.1",
    },
    'ajaib.co.id' :
    {
        'category' : "Finance",
        'updated' : "17 Jun 2022",
        'App_name' : "Ajaib: Investment SuperApp",
        'Package_name' : "ajaib.co.id",
        'installs' : "1,000,000+",
        'version' : "1.0.6",
    },
    'com.dukaan.app' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Dukaan - Start Selling Online",
        'Package_name' : "com.dukaan.app",
        'installs' : "5,000,000+",
        'version' : "3.0.5",
    },
    'com.gamepub.ft2.g' :
    {
        'category' : "Games",
        'updated' : "25 Aug 2021",
        'App_name' : "\ud30c\uc774\ub110\uc0bc\uad6d\uc9c02",
        'Package_name' : "com.gamepub.ft2.g",
        'installs' : "500,000+",
        'version' : "1.0.2",
    },
    'com.topps.disney' :
    {
        'category' : "Entertainment",
        'updated' : "26 May 2022",
        'App_name' : "Disney Collect! by Topps",
        'Package_name' : "com.topps.disney",
        'installs' : "1,000,000+",
        'version' : "19.0.0",
    },
    'kr.co.galaxiacoms.cashg' :
    {
        'category' : "Finance",
        'updated' : "04 May 2022",
        'App_name' : "\uba38\ub2c8\ud2b8\ub9ac-\uc11c\uc6b8\uc0ac\ub791\uc0c1\ud488\uad8c,\uc11c\uc6b8Pay+,\uc11c\uc6b8\ud398\uc774\ud50c\ub7ec\uc2a4",
        'Package_name' : "kr.co.galaxiacoms.cashg",
        'installs' : "1,000,000+",
        'version' : "4.9.7",
    },
    '1434278660' :
    {
        'category' : "Finance",
        'updated' : "2022-05-23T16:26:35Z",
        'App_name' : "AAX-\u6578\u5b57\u8cc7\u7522\u4ea4\u6613\u5e73\u53f0",
        'Package_name' : "com.aax.exchange",
        'version' : "3.2.10",
        'minimumOsVersion' : "10.3",
    },
    'app.meditasyon' :
    {
        'category' : "Health and Fitness",
        'updated' : "17 May 2022",
        'App_name' : "Meditopia: Sleep, Meditation",
        'Package_name' : "app.meditasyon",
        'installs' : "10,000,000+",
        'version' : "3.24.0",
    },
    'com.klab.lovelive.allstars.global' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Love Live! All Stars",
        'Package_name' : "com.klab.lovelive.allstars.global",
        'installs' : "500,000+",
        'version' : "3.2.4",
    },
    '398474140' :
    {
        'category' : "Shopping",
        'updated' : "2022-05-19T09:15:21Z",
        'App_name' : "Lidl - Onlineshop",
        'Package_name' : "de.sec.lidl",
        'version' : "8.19.0",
        'minimumOsVersion' : "14.0",
    },
    'com.gameon.picottown' :
    {
        'category' : "Games",
        'updated' : "11 Mar 2022",
        'App_name' : "\u30d4\u30b3\u30c3\u30c8\u30bf\u30a6\u30f3",
        'Package_name' : "com.gameon.picottown",
        'installs' : "50,000+",
        'version' : "1.3.4",
    },
    'premom.eh.com.ehpremomapp' :
    {
        'category' : "Health and Fitness",
        'updated' : "10 Jun 2022",
        'App_name' : "Premom Ovulation Tracker",
        'Package_name' : "premom.eh.com.ehpremomapp",
        'installs' : "1,000,000+",
        'version' : "1.13.29",
    },
    'kr.co.teamblind.bleet' :
    {
        'category' : "Social and Communication",
        'updated' : "09 Jun 2022",
        'App_name' : "\ube14\ub9bf - \uc9c1\uc7a5\uc778 \ub370\uc774\ud305",
        'Package_name' : "kr.co.teamblind.bleet",
        'installs' : "50,000+",
        'version' : "1.4.5",
    },
    'com.adopteunmec.androidfr' :
    {
        'category' : "Lifestyle",
        'updated' : "17 Jun 2022",
        'App_name' : "adopte - app de rencontre",
        'Package_name' : "com.adopteunmec.androidfr",
        'installs' : "1,000,000+",
        'version' : "4.8.9",
    },
    'co.plano' :
    {
        'category' : "Health and Fitness",
        'updated' : "02 May 2022",
        'App_name' : "Parental Control App - Plano",
        'Package_name' : "co.plano",
        'installs' : "500,000+",
        'version' : "5.3.2",
    },
    'com.perkwizmobile.prod' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "PerkWiz - Find Freelancers & Earn Rewards",
        'Package_name' : "com.perkwizmobile.prod",
        'installs' : "100,000+",
        'version' : "2.0.0",
    },
    'jp.j_o_e.NewPig' :
    {
        'category' : "Games",
        'updated' : "17 May 2022",
        'App_name' : "\u3088\u3046\u3068\u3093\u5834MIX",
        'Package_name' : "jp.j_o_e.NewPig",
        'installs' : "500,000+",
        'version' : "14.3",
    },
    'ru.mvm.eldo' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "\u042d\u043b\u044c\u0434\u043e\u0440\u0430\u0434\u043e - \u0438\u043d\u0442\u0435\u0440\u043d\u0435\u0442 \u043c\u0430\u0433\u0430\u0437\u0438\u043d",
        'Package_name' : "ru.mvm.eldo",
        'installs' : "1,000,000+",
        'version' : "1.15.0",
    },
    'com.pg.pampers.primakulubu' :
    {
        'category' : "Health and Fitness",
        'updated' : "13 Jun 2022",
        'App_name' : "Prima Kul\u00fcb\u00fc: Bebek Geli\u015fimi",
        'Package_name' : "com.pg.pampers.primakulubu",
        'installs' : "500,000+",
        'version' : "1.8.5",
    },
    'com.youmusic.magictiles' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Magic Tiles 3",
        'Package_name' : "com.youmusic.magictiles",
        'installs' : "500,000,000+",
        'version' : "9.058.004",
    },
    'vn.com.paysmart' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "SmartPay Chuy\u00ean gia thanh to\u00e1n",
        'Package_name' : "vn.com.paysmart",
        'installs' : "1,000,000+",
        'version' : "2.70.1",
    },
    'com.sundaytoz.line.joy' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "Disney POP TOWN",
        'Package_name' : "com.sundaytoz.line.joy",
        'installs' : "1,000,000+",
        'version' : "1.2.5",
    },
    'com.clickbus.mobile' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "03 May 2022",
        'App_name' : "ClickBus - Bus Tickets",
        'Package_name' : "com.clickbus.mobile",
        'installs' : "5,000,000+",
        'version' : "3.18.37",
    },
    '1540050192' :
    {
        'category' : "Games",
        'updated' : "2022-03-30T02:46:19Z",
        'App_name' : "\u30aa\u30eb\u30bf\u30f3\u30b7\u30a2\u30fb\u30b5\u30fc\u30acR",
        'Package_name' : "com.f4.czqstr.appstore",
        'version' : "1.1.7",
        'minimumOsVersion' : "10.0",
    },
    'www.undostres.com.mx' :
    {
        'category' : "Shopping",
        'updated' : "12 Jun 2022",
        'App_name' : "UnDosTres - Recargas y Pagos",
        'Package_name' : "www.undostres.com.mx",
        'installs' : "1,000,000+",
        'version' : "4.3.5",
    },
    'com.doradogames.conflictnations.worldwar3' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Conflict of Nations: WW3 Game",
        'Package_name' : "com.doradogames.conflictnations.worldwar3",
        'installs' : "1,000,000+",
        'version' : "0.136",
    },
    'com.tencent.baiyeint' :
    {
        'category' : "Games",
        'updated' : "01 Jun 2022",
        'App_name' : "Alchemy Stars",
        'Package_name' : "com.tencent.baiyeint",
        'installs' : "1,000,000+",
        'version' : "1.9.3",
    },
    'com.lge.promota' :
    {
        'category' : "Tools",
        'updated' : "20 May 2022",
        'App_name' : "\uba38\uc2a4\ud0c0\ub4dc (mustad) - 1\ubd84 \uc644\uc131 \ub9db\uae54\ub098\ub294 \ub514\uc790\uc778",
        'Package_name' : "com.lge.promota",
        'installs' : "500,000+",
        'version' : "3.4.0",
    },
    'com.sgzs.alice' :
    {
        'category' : "Games",
        'updated' : "25 Apr 2022",
        'App_name' : "\u7834\u6575\u00b7\u4e09\u570b\u5fd7",
        'Package_name' : "com.sgzs.alice",
        'installs' : "50,000+",
        'version' : "1.18.6",
    },
    'com.metromart.metromart' :
    {
        'category' : "Food & Drink",
        'updated' : "15 Jun 2022",
        'App_name' : "MetroMart - Grocery Delivery",
        'Package_name' : "com.metromart.metromart",
        'installs' : "500,000+",
        'version' : "10.11.0",
    },
    'com.percent.royaldice' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "Random Dice: Defense",
        'Package_name' : "com.percent.royaldice",
        'installs' : "5,000,000+",
        'version' : "7.6.0",
    },
    'com.ncsoft.bns219' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "\ube14\ub808\uc774\ub4dc&\uc18c\uc6b82",
        'Package_name' : "com.ncsoft.bns219",
        'installs' : "500,000+",
        'version' : "0.75.2",
    },
    'sa.com.stcpay' :
    {
        'category' : "Finance",
        'updated' : "25 May 2022",
        'App_name' : "stc pay",
        'Package_name' : "sa.com.stcpay",
        'installs' : "5,000,000+",
        'version' : "1.10.10",
    },
    '1514005355' :
    {
        'category' : "Utilities",
        'updated' : "2022-06-11T10:55:09Z",
        'App_name' : "TextKiller - Spam Text Blocker",
        'Package_name' : "co.teltech.TextKiller",
        'version' : "1.5.1",
        'minimumOsVersion' : "13.0",
    },
    'com.actify' :
    {
        'category' : "Health and Fitness",
        'updated' : "02 Jun 2022",
        'App_name' : "Actify - Vitaliteitscoach",
        'Package_name' : "com.actify",
        'installs' : "100,000+",
        'version' : "1.11.2",
    },
    '1189255812' :
    {
        'category' : "Finance",
        'updated' : "2022-06-02T13:32:28Z",
        'App_name' : "Empiricus Research",
        'Package_name' : "br.com.empiricus.Empiricus",
        'version' : "4.8.7",
        'minimumOsVersion' : "13.0",
    },
    'com.kr.mir2nb19.google' :
    {
        'category' : "Games",
        'updated' : "16 Jul 2021",
        'App_name' : "\ubbf8\ub974\uc758 \uc804\uc1242 \uc5b4\uac8c\uc778: \ub2e4\uc2dc \uc2dc\uc791\ub418\ub294 \uc804\uc124",
        'Package_name' : "com.kr.mir2nb19.google",
        'installs' : "100,000+",
        'version' : "3.7",
    },
    'com.joytea.wakuwaku.google' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "\u308f\u304f\u308f\u304f\u30d5\u30a1\u30f3\u30bf\u30b8\u30fc\uff5e\u306f\u308b\u304b\u306a\u4e16\u754c\u306e\u7269\u8a9e\uff5e",
        'Package_name' : "com.joytea.wakuwaku.google",
        'installs' : "100,000+",
        'version' : "2.1.5",
    },
    'com.grabtaxi.passenger' :
    {
        'category' : "Food & Drink",
        'updated' : "13 Jun 2022",
        'App_name' : "Grab Superapp",
        'Package_name' : "com.grabtaxi.passenger",
        'installs' : "100,000,000+",
        'version' : "5.205.0",
    },
    'com.cron.cleaner' :
    {
        'category' : "Tools",
        'updated' : "10 Jul 2020",
        'App_name' : "Phone Booster, Ram Cleaner - Croncleaner",
        'Package_name' : "com.cron.cleaner",
        'installs' : "50,000+",
        'version' : "1.5.2",
    },
    'genesis.nebula' :
    {
        'category' : "Lifestyle",
        'updated' : "14 Jun 2022",
        'App_name' : "Nebula: Horoscope & Astrology",
        'Package_name' : "genesis.nebula",
        'installs' : "1,000,000+",
        'version' : "4.7.27",
    },
    '1085470991' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-07T17:35:25Z",
        'App_name' : "The Luxury Closet - Buy & Sell",
        'Package_name' : "com.lykan.LuxuryCloset",
        'version' : "2.0.70",
        'minimumOsVersion' : "12.0",
    },
    '1554981586' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-17T02:28:57Z",
        'App_name' : "Selly - D\u1ec5 d\u00e0ng b\u00e1n h\u00e0ng",
        'Package_name' : "vn.selly",
        'version' : "2.23",
        'minimumOsVersion' : "10.0",
    },
    'cab.snapp.passenger.play' :
    {
        'category' : "Utilities",
        'updated' : "27 Feb 2022",
        'App_name' : "Snapp",
        'Package_name' : "cab.snapp.passenger.play",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.tubitv' :
    {
        'category' : "Entertainment",
        'updated' : "26 May 2022",
        'App_name' : "Tubi - Movies & TV Shows",
        'Package_name' : "com.tubitv",
        'installs' : "100,000,000+",
        'version' : "4.24.0",
    },
    'com.netmarble.stonemmogb' :
    {
        'category' : "Games",
        'updated' : "17 Mar 2022",
        'App_name' : "StoneAge World",
        'Package_name' : "com.netmarble.stonemmogb",
        'installs' : "1,000,000+",
        'version' : "13.1.0.0.0",
    },
    '826510222' :
    {
        'category' : "Entertainment",
        'updated' : "2022-05-30T08:23:04Z",
        'App_name' : "Joyn | deine Streaming App",
        'Package_name' : "de.prosiebensat1digital.seventv",
        'version' : "2022.11.0",
        'minimumOsVersion' : "12.0",
    },
    'com.didiglobal.passenger' :
    {
        'category' : "Utilities",
        'updated' : "16 Jun 2022",
        'App_name' : "DiDi Rider: Affordable rides",
        'Package_name' : "com.didiglobal.passenger",
        'installs' : "50,000,000+",
        'version' : "Varies with device",
    },
    'com.yy.hagolite' :
    {
        'category' : "Games",
        'updated' : "10 Sep 2020",
        'App_name' : "HAGO Lite",
        'Package_name' : "com.yy.hagolite",
        'installs' : "5,000,000+",
        'version' : "1.2.5",
    },
    '1512353649' :
    {
        'category' : "Games",
        'updated' : "2022-03-17T07:01:01Z",
        'App_name' : "\u6211\u7684\u52c7\u8005",
        'Package_name' : "com.rsg.myheroestc",
        'version' : "2.6.0",
        'minimumOsVersion' : "10.0",
    },
    'com.maxidigital.paragonder' :
    {
        'category' : "Finance",
        'updated' : "06 Oct 2021",
        'App_name' : "ParaG\u00f6nder",
        'Package_name' : "com.maxidigital.paragonder",
        'installs' : "10,000+",
        'version' : "Varies with device",
    },
    'com.netmarble.mpoker' :
    {
        'category' : "Casino",
        'updated' : "03 Jun 2022",
        'App_name' : "\uc708\uc870\uc774 \ud3ec\ucee4 - \ube14\ub799\uc7ad,\ubc14\uce74\ub77c,\uc2ac\ub86f,\ub85c\uc6b0\ubc14\ub451\uc774\ub97c \ub3d9\uc2dc\uc5d0",
        'Package_name' : "com.netmarble.mpoker",
        'installs' : "1,000,000+",
        'version' : "7.4.13",
    },
    'com.EurekaStudio.skipschool' :
    {
        'category' : "Games",
        'updated' : "26 Apr 2022",
        'App_name' : "Skip school !\u3000-escape game",
        'Package_name' : "com.EurekaStudio.skipschool",
        'installs' : "10,000,000+",
        'version' : "3.4.4",
    },
    'com.knockknock.live' :
    {
        'category' : "Social and Communication",
        'updated' : "31 May 2022",
        'App_name' : "Knock Knock Live",
        'Package_name' : "com.knockknock.live",
        'installs' : "100,000+",
        'version' : "3.3.0",
    },
    'jp.co.taito.magic' :
    {
        'category' : "Games",
        'updated' : "01 Jun 2022",
        'App_name' : "\u30c7\u30a3\u30ba\u30cb\u30fc \u30df\u30e5\u30fc\u30b8\u30c3\u30af\u30d1\u30ec\u30fc\u30c9",
        'Package_name' : "jp.co.taito.magic",
        'installs' : "100,000+",
        'version' : "2.2.1",
    },
    'jp.jun.yoshida.paddy' :
    {
        'category' : "Social and Communication",
        'updated' : "10 Apr 2019",
        'App_name' : "\u51fa\u4f1a\u3044\u306a\u3089paddy67(\u30d1\u30c7\u30a367)\u604b\u6d3b\u30fb\u5a5a\u6d3b\u30fb\u3067\u3042\u3044",
        'Package_name' : "jp.jun.yoshida.paddy",
        'installs' : "100,000+",
        'version' : "2.1.3",
    },
    'com.mobisoft.morhipo' :
    {
        'category' : "Shopping",
        'updated' : "25 Mar 2022",
        'App_name' : "Morhipo - Online Al\u0131\u015fveri\u015f",
        'Package_name' : "com.mobisoft.morhipo",
        'installs' : "5,000,000+",
        'version' : "7.4.1",
    },
    'vn.fimplus.app.and' :
    {
        'category' : "Entertainment",
        'updated' : "07 Jun 2022",
        'App_name' : "Galaxy Play-Phim m\u1edbi m\u1ed7i ng\u00e0y",
        'Package_name' : "vn.fimplus.app.and",
        'installs' : "1,000,000+",
        'version' : "3.9.11",
    },
    '492075888' :
    {
        'category' : "Games",
        'updated' : "2022-05-19T01:49:24Z",
        'App_name' : "\u66b4\u8d70\u5217\u4f1d \u5358\u8eca\u306e\u864e",
        'Package_name' : "com.donuts.tantora",
        'version' : "1.2.20",
        'minimumOsVersion' : "11.0",
    },
    'com.realore.FarmUp' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Jane's Farm: Farming Game",
        'Package_name' : "com.realore.FarmUp",
        'installs' : "1,000,000+",
        'version' : "9.9.8",
    },
    'com.monawa.giwon' :
    {
        'category' : "Games",
        'updated' : "06 May 2022",
        'App_name' : "\ub354\ud504\ub808\uc774 MMORPG",
        'Package_name' : "com.monawa.giwon",
        'installs' : "100,000+",
        'version' : "1.2.17",
    },
    '1500725639' :
    {
        'category' : "Games",
        'updated' : "2021-02-04T06:36:05Z",
        'App_name' : "\uc804\uc7c1\uc758\uc5f0\uac00:\uc804\uc5f0",
        'Package_name' : "com.ariel.sow",
        'version' : "1.0.26.0",
        'minimumOsVersion' : "9.3",
    },
    'com.com2us.soccerspirits.normal2.freefull.google.global.android.common' :
    {
        'category' : "Games",
        'updated' : "12 Nov 2021",
        'App_name' : "Soccer Spirits",
        'Package_name' : "com.com2us.soccerspirits.normal2.freefull.google.global.android.common",
        'installs' : "1,000,000+",
        'version' : "2.5.5",
    },
    'ru.start.androidmobile' :
    {
        'category' : "Entertainment",
        'updated' : "15 Jun 2022",
        'App_name' : "START: \u0424\u0438\u043b\u044c\u043c\u044b \u0438 \u0421\u0435\u0440\u0438\u0430\u043b\u044b \u0432 HD",
        'Package_name' : "ru.start.androidmobile",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    '295079411' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-14T12:38:30Z",
        'App_name' : "SIXT Mietwagen Carsharing Taxi",
        'Package_name' : "com.sixt.isixt",
        'version' : "9.76.0",
        'minimumOsVersion' : "14.0.0",
    },
    'com.jar.app' :
    {
        'category' : "Finance",
        'updated' : "06 Jun 2022",
        'App_name' : "Gullak to Save Daily in Gold",
        'Package_name' : "com.jar.app",
        'installs' : "5,000,000+",
        'version' : "4.5.2",
    },
    'jp.dgeg.dragonegg2' :
    {
        'category' : "Games",
        'updated' : "14 Feb 2022",
        'App_name' : "\u30c9\u30e9\u30b4\u30f3\u30a8\u30c3\u30b0 \u4ef2\u9593\u3068\u306e\u51fa\u4f1a\u3044 \u53cb\u9054\u5bfe\u6226RPG",
        'Package_name' : "jp.dgeg.dragonegg2",
        'installs' : "1,000,000+",
        'version' : "1.0.106",
    },
    'com.gm99.dldl' :
    {
        'category' : "Games",
        'updated' : "21 May 2021",
        'App_name' : "\u6597\u7f85\u5927\u9678",
        'Package_name' : "com.gm99.dldl",
        'installs' : "1,000,000+",
        'version' : "4.4",
    },
    'es.prestamer.mobileapp' :
    {
        'category' : "Finance",
        'updated' : "02 Jun 2022",
        'App_name' : "Pr\u00e9stamer Prestamos Inmediatos",
        'Package_name' : "es.prestamer.mobileapp",
        'installs' : "100,000+",
        'version' : "0.5.3",
    },
    'com.privalia.privalia' :
    {
        'category' : "Shopping",
        'updated' : "31 May 2022",
        'App_name' : "Privalia Shopping",
        'Package_name' : "com.privalia.privalia",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    '1544914266' :
    {
        'category' : "Games",
        'updated' : "2022-01-10T06:31:49Z",
        'App_name' : "BLEACH:\u00a0Eternal\u00a0Soul",
        'Package_name' : "com.vqw.aess",
        'version' : "1.9.43",
        'minimumOsVersion' : "10.0",
    },
    'com.cmcm.uangme' :
    {
        'category' : "Finance",
        'updated' : "07 Jun 2022",
        'App_name' : "UangMe - Online Cash Loan",
        'Package_name' : "com.cmcm.uangme",
        'installs' : "5,000,000+",
        'version' : "2.6.9",
    },
    'com.kms.free' :
    {
        'category' : "Tools",
        'updated' : "23 May 2022",
        'App_name' : "Kaspersky Antivirus & VPN",
        'Package_name' : "com.kms.free",
        'installs' : "50,000,000+",
        'version' : "Varies with device",
    },
    'com.fabhotels.guests' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "08 Mar 2022",
        'App_name' : "FabHotels: Hotel Booking App",
        'Package_name' : "com.fabhotels.guests",
        'installs' : "1,000,000+",
        'version' : "6.4.4",
    },
    'com.voicefive.mms' :
    {
        'category' : "Tools",
        'updated' : "16 Mar 2021",
        'App_name' : "My Mobile Secure - Fast, Reliable, Unlimited VPN",
        'Package_name' : "com.voicefive.mms",
        'installs' : "5,000,000+",
        'version' : "1.1.42",
    },
    'br.com.intermedium' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "Inter: Pix, Cart\u00e3o e Conta",
        'Package_name' : "br.com.intermedium",
        'installs' : "10,000,000+",
        'version' : "11.2.2",
    },
    'com.ciceksepeti.ciceksepeti' :
    {
        'category' : "Shopping",
        'updated' : "17 Jun 2022",
        'App_name' : "\u00c7i\u00e7ekSepeti - Online Al\u0131\u015fveri\u015f",
        'Package_name' : "com.ciceksepeti.ciceksepeti",
        'installs' : "10,000,000+",
        'version' : "5.9.1",
    },
    '723715907' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-02T12:07:37Z",
        'App_name' : "\u00c7i\u00e7ekSepeti - Online Al\u0131\u015fveri\u015f",
        'Package_name' : "com.ciceksepeti.mobil",
        'version' : "6.1.7",
        'minimumOsVersion' : "11.0",
    },
    'co.jp.karat.puzzlegirl' :
    {
        'category' : "Games",
        'updated' : "12 Jun 2022",
        'App_name' : "Puzzle & Girls",
        'Package_name' : "co.jp.karat.puzzlegirl",
        'installs' : "5,000+",
        'version' : "1.0.47",
    },
    'com.webzen.muorigin3.google' :
    {
        'category' : "Games",
        'updated' : "12 May 2022",
        'App_name' : "\ubba4\uc624\ub9ac\uc9c43",
        'Package_name' : "com.webzen.muorigin3.google",
        'installs' : "100,000+",
        'version' : "2.0.0",
    },
    'com.efun.xiaoao.sm' :
    {
        'category' : "Games",
        'updated' : "13 May 2022",
        'App_name' : "\u7b11\u50b2\u6c5f\u6e56\u65b0\u9a6c\u7248\uff08Swordsman\uff09",
        'Package_name' : "com.efun.xiaoao.sm",
        'installs' : "100,000+",
        'version' : "1.0.9",
    },
    'com.dc.gok.google' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "Game of Khans",
        'Package_name' : "com.dc.gok.google",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    '1161312751' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-03T07:14:34Z",
        'App_name' : "Max Fashion - \u0645\u0627\u0643\u0633 \u0641\u0627\u0634\u0648\u0646",
        'Package_name' : "com.landmarkgroup.max",
        'version' : "9.22.2",
        'minimumOsVersion' : "10.0",
    },
    '640023408' :
    {
        'category' : "Games",
        'updated' : "2022-05-26T00:40:05Z",
        'App_name' : "\u9ebb\u96c0\u683c\u95d8\u5036\u697d\u90e8Sp |\u5165\u9580\u306b\u304a\u3059\u3059\u3081! \u9ebb\u96c0 \u30b2\u30fc\u30e0",
        'Package_name' : "jp.konami.mfcsp",
        'version' : "2.1.0",
        'minimumOsVersion' : "10.0",
    },
    'com.yoozoo.kr.ss0' :
    {
        'category' : "Games",
        'updated' : "18 Jun 2021",
        'App_name' : "\uc0bc\uad6d\uc9c0\ud63c",
        'Package_name' : "com.yoozoo.kr.ss0",
        'installs' : "100,000+",
        'version' : "1.0.10008",
    },
    'com.cogaming.comeoncasino.dga' :
    {
        'category' : "Casino",
        'updated' : "20 May 2022",
        'App_name' : "ComeOn! Online Casino & Sport",
        'Package_name' : "com.cogaming.comeoncasino.dga",
        'installs' : "1,000+",
        'version' : "1.2.0-play",
    },
    'com.rummytime.practice' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "Rummytime - Play Rummy Online",
        'Package_name' : "com.rummytime.practice",
        'installs' : "1,000,000+",
        'version' : "3.7",
    },
    'ru.tinkoff.investing' :
    {
        'category' : "Finance",
        'updated' : "31 May 2022",
        'App_name' : "Tinkoff Investments",
        'Package_name' : "ru.tinkoff.investing",
        'installs' : "5,000,000+",
        'version' : "5.7.0",
    },
    'com.samsungcard.mls' :
    {
        'category' : "Lifestyle",
        'updated' : "21 Aug 2021",
        'App_name' : "\uc778\uc0dd\ub77d\uc11c - \ub0b4 \uc774\uc57c\uae30\ub85c \ub9cc\ub098\ub294 \uc138\uc0c1",
        'Package_name' : "com.samsungcard.mls",
        'installs' : "100,000+",
        'version' : "1.4.803",
    },
    '1492850010' :
    {
        'category' : "Games",
        'updated' : "2022-01-05T01:22:04Z",
        'App_name' : "RO\u4ed9\u5883\u50b3\u8aaa\uff1a\u6211\u7684\u6230\u8853",
        'Package_name' : "com.gravity.rod.ios",
        'version' : "5.6.0",
        'minimumOsVersion' : "10.0",
    },
    '1459864739' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-01-19T19:36:08Z",
        'App_name' : "Supermart.ae - Online Grocery",
        'Package_name' : "ae.supermart.app",
        'version' : "2.8.6",
        'minimumOsVersion' : "14.1",
    },
    'me.whitebeard.fatafeat' :
    {
        'category' : "Food & Drink",
        'updated' : "16 Nov 2021",
        'App_name' : "Fatafeat",
        'Package_name' : "me.whitebeard.fatafeat",
        'installs' : "100,000+",
        'version' : "2.7.1",
    },
    'com.app.tgtg' :
    {
        'category' : "Food & Drink",
        'updated' : "14 Jun 2022",
        'App_name' : "Too Good To Go: End Food Waste",
        'Package_name' : "com.app.tgtg",
        'installs' : "10,000,000+",
        'version' : "22.5.10",
    },
    '882003733' :
    {
        'category' : "Games",
        'updated' : "2019-10-07T18:45:09Z",
        'App_name' : "Golden Nugget Online Casino",
        'Package_name' : "com.goldennugget.GoldenNuggetCasino",
        'version' : "5.2.1",
        'minimumOsVersion' : "8.0",
    },
    '1463160775' :
    {
        'category' : "Finance",
        'updated' : "2022-04-29T11:07:32Z",
        'App_name' : "Tosla",
        'Package_name' : "com.akode.tosla.prod",
        'version' : "1.15.0",
        'minimumOsVersion' : "12.1",
    },
    'com.eyougame.sgyxg' :
    {
        'category' : "Games",
        'updated' : "12 Apr 2021",
        'App_name' : "\u6b66\u5c07\u50b3-\u5168\u65b0\u6b66\u5c07\u99ac\u8d85\u964d\u81e8",
        'Package_name' : "com.eyougame.sgyxg",
        'installs' : "50,000+",
        'version' : "1.2.12",
    },
    'com.lilithgames.rok.gp.jp' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "Rise of Kingdoms \u2015\u4e07\u56fd\u899a\u9192\u2015",
        'Package_name' : "com.lilithgames.rok.gp.jp",
        'installs' : "500,000+",
        'version' : "1.0.58.19",
    },
    'com.justeat.app.it' :
    {
        'category' : "Food & Drink",
        'updated' : "13 Jun 2022",
        'App_name' : "Just Eat ITA Cibo a Domicilio",
        'Package_name' : "com.justeat.app.it",
        'installs' : "5,000,000+",
        'version' : "9.51.0.61597171",
    },
    'com.topps.force' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Star Wars Card Trader by Topps",
        'Package_name' : "com.topps.force",
        'installs' : "1,000,000+",
        'version' : "19.0.0",
    },
    'com.ekdp.samdasoo' :
    {
        'category' : "Lifestyle",
        'updated' : "21 Jun 2021",
        'App_name' : "\uc81c\uc8fc\uc0bc\ub2e4\uc218",
        'Package_name' : "com.ekdp.samdasoo",
        'installs' : "100,000+",
        'version' : "1.34",
    },
    'jp.konami.prospia' :
    {
        'category' : "Games",
        'updated' : "17 May 2022",
        'App_name' : "\u30d7\u30ed\u91ce\u7403\u30b9\u30d4\u30ea\u30c3\u30c4A",
        'Package_name' : "jp.konami.prospia",
        'installs' : "1,000,000+",
        'version' : "14.2.0",
    },
    'com.global.musjp' :
    {
        'category' : "Games",
        'updated' : "24 Mar 2022",
        'App_name' : "MU\uff1a\u30a2\u30fc\u30af\u30a8\u30f3\u30b8\u30a7\u30eb",
        'Package_name' : "com.global.musjp",
        'installs' : "100,000+",
        'version' : "1.27.02",
    },
    'com.jp.beauty' :
    {
        'category' : "Games",
        'updated' : "06 Jun 2022",
        'App_name' : "\u65e5\u66ff\u308f\u308a\u5185\u5ba4",
        'Package_name' : "com.jp.beauty",
        'installs' : "1,000,000+",
        'version' : "4.2",
    },
    'com.frograms.wplay' :
    {
        'category' : "Entertainment",
        'updated' : "16 Jun 2022",
        'App_name' : "WATCHA",
        'Package_name' : "com.frograms.wplay",
        'installs' : "5,000,000+",
        'version' : "1.10.41",
    },
    'com.franprix.catalogue' :
    {
        'category' : "Food & Drink",
        'updated' : "15 Apr 2022",
        'App_name' : "franprix: Livraison de courses",
        'Package_name' : "com.franprix.catalogue",
        'installs' : "500,000+",
        'version' : "5.9.2",
    },
    'kr.co.emotion.dosapersonal' :
    {
        'category' : "Lifestyle",
        'updated' : "16 Jun 2022",
        'App_name' : "\ucd9c\uc7a5\ub3c4\uc0ac - 1:1\uc2e4\uc2dc\uac04 \uc601\uc0c1/\uc74c\uc131 \uc0c1\ub2f4 \ubaa8\ubc14\uc77c \ud574\uc6b0\uc18c",
        'Package_name' : "kr.co.emotion.dosapersonal",
        'installs' : "100,000+",
        'version' : "1.2.24",
    },
    'com.vidyodan' :
    {
        'category' : "Shopping",
        'updated' : "06 Jun 2022",
        'App_name' : "Vidyodan",
        'Package_name' : "com.vidyodan",
        'installs' : "100,000+",
        'version' : "1.150.1",
    },
    '1460593315' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-12T22:09:37Z",
        'App_name' : "CarrefourSA: Online Market",
        'Package_name' : "com.carrefoursa.ecommerce",
        'version' : "2.5.3",
        'minimumOsVersion' : "12.0",
    },
    'jp.prosgate.app194' :
    {
        'category' : "Social and Communication",
        'updated' : "03 Jun 2022",
        'App_name' : "\u30a4\u30af\u30af\u30eb-\u51fa\u4f1a\u3044\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea",
        'Package_name' : "jp.prosgate.app194",
        'installs' : "500,000+",
        'version' : "1.31",
    },
    'com.wsy.google.wansuiye.kr' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "\ud669\uc81c\ub77c \uce6d\ud558\ub77c",
        'Package_name' : "com.wsy.google.wansuiye.kr",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.newgo.coincola' :
    {
        'category' : "Finance",
        'updated' : "12 May 2022",
        'App_name' : "CoinCola - Buy Bitcoin & more",
        'Package_name' : "com.newgo.coincola",
        'installs' : "100,000+",
        'version' : "4.10.1",
    },
    'com.itemmania.imiapp' :
    {
        'category' : "Entertainment",
        'updated' : "25 May 2022",
        'App_name' : "\uc544\uc774\ud15c\ub9e4\ub2c8\uc544 - \uac8c\uc784 \uc544\uc774\ud15c \uac70\ub798\ub294 No1 \uc544\uc774\ud15c\ub9e4\ub2c8\uc544",
        'Package_name' : "com.itemmania.imiapp",
        'installs' : "1,000,000+",
        'version' : "1.3.52",
    },
    'ru.noone.app' :
    {
        'category' : "Shopping",
        'updated' : "11 Jun 2022",
        'App_name' : "NO ONE: \u041e\u0431\u0443\u0432\u044c, \u0430\u043a\u0441\u0435\u0441\u0441\u0443\u0430\u0440\u044b,\u043c\u043e\u0434\u0430",
        'Package_name' : "ru.noone.app",
        'installs' : "10,000+",
        'version' : "0.9.81",
    },
    '1074266177' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-14T17:21:10Z",
        'App_name' : "KFC: \u0414\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u0435\u0434\u044b, \u0440\u0435\u0441\u0442\u043e\u0440\u0430\u043d\u044b",
        'Package_name' : "ru.yum.KFC-Russia",
        'version' : "8.2.0",
        'minimumOsVersion' : "10.0",
    },
    'com.westernunion.moneytransferr3app.eu2' :
    {
        'category' : "Finance",
        'updated' : "12 May 2022",
        'App_name' : "Western Union App: Send Money",
        'Package_name' : "com.westernunion.moneytransferr3app.eu2",
        'installs' : "100,000+",
        'version' : "3.7",
    },
    'com.pwrd.dbbpm' :
    {
        'category' : "Games",
        'updated' : "28 Feb 2022",
        'App_name' : "\ub3d9\ubc29\ubd88\ud328 \ubaa8\ubc14\uc77c",
        'Package_name' : "com.pwrd.dbbpm",
        'installs' : "100,000+",
        'version' : "1.0.135",
    },
    '359917414' :
    {
        'category' : "Games",
        'updated' : "2022-04-25T16:22:27Z",
        'App_name' : "Solit\u00e4r !",
        'Package_name' : "com.mobilityware.SolitaireFree",
        'version' : "7.1.0",
        'minimumOsVersion' : "11.0",
    },
    'com.zempot.wplpoker' :
    {
        'category' : "Casino",
        'updated' : "13 May 2022",
        'App_name' : "WPL:Texas Hold'em, MTT, Sit&Go",
        'Package_name' : "com.zempot.wplpoker",
        'installs' : "100,000+",
        'version' : "2.5.3",
    },
    'com.ncsoft.lineage2m19' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "\ub9ac\ub2c8\uc9c02M",
        'Package_name' : "com.ncsoft.lineage2m19",
        'installs' : "1,000,000+",
        'version' : "1.0.90",
    },
    'com.chachastation.app' :
    {
        'category' : "Lifestyle",
        'updated' : "06 Jun 2022",
        'App_name' : "ChargeSPOT",
        'Package_name' : "com.chachastation.app",
        'installs' : "500,000+",
        'version' : "1.9.46",
    },
    '605305737' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-03T13:10:31Z",
        'App_name' : "Intermarch\u00e9 - Magasin & Drive",
        'Package_name' : "com.mousquetaires.intermarche",
        'version' : "8.2.0",
        'minimumOsVersion' : "14.0",
    },
    'jp.co.plusr.android.stepbabyfood' :
    {
        'category' : "Health and Fitness",
        'updated' : "09 Jun 2022",
        'App_name' : "\u30b9\u30c6\u30c3\u30d7\u96e2\u4e73\u98df - \u6804\u990a\u58eb\u76e3\u4fee\u306e\u6642\u671f\u306b\u3042\u3063\u305f\u98df\u6750\u3068\u98df\u3079\u3055\u305b\u65b9",
        'Package_name' : "jp.co.plusr.android.stepbabyfood",
        'installs' : "100,000+",
        'version' : "5.25.0",
    },
    'leaf.green.midori' :
    {
        'category' : "Lifestyle",
        'updated' : "31 May 2022",
        'App_name' : "\u7fe0 -midori-",
        'Package_name' : "leaf.green.midori",
        'installs' : "100,000+",
        'version' : "1.1.0.0",
    },
    '1234494259' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-07T21:14:08Z",
        'App_name' : "Cepte \u015eok",
        'Package_name' : "com.positive.ceptesok",
        'version' : "2.0.28",
        'minimumOsVersion' : "12.0",
    },
    'com.mtkth.vn' :
    {
        'category' : "Games",
        'updated' : "26 Nov 2021",
        'App_name' : "M\u1ed9ng T\u00ecnh Ki\u1ebfm",
        'Package_name' : "com.mtkth.vn",
        'installs' : "100,000+",
        'version' : "1.2.1",
    },
    '646692973' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-14T18:11:38Z",
        'App_name' : "Opodo: Book cheap flights",
        'Package_name' : "com.opodo.flights",
        'version' : "4.272",
        'minimumOsVersion' : "14.0",
    },
    '1462379373' :
    {
        'category' : "Games",
        'updated' : "2021-02-04T04:55:34Z",
        'App_name' : "\u72ac\u591c\u53c9\u3000\uff0d\u3088\u307f\u304c\u3048\u308b\u7269\u8a9e\uff0d",
        'Package_name' : "com.hammerent.inuyasha",
        'version' : "1.0.46",
        'minimumOsVersion' : "9.0",
    },
    '284862083' :
    {
        'category' : "Not available",
        'updated' : "2022-06-14T15:35:44Z",
        'App_name' : "The New York Times",
        'Package_name' : "com.nytimes.NYTimes",
        'version' : "9.78.0",
        'minimumOsVersion' : "14.5",
    },
    'com.goldspoon' :
    {
        'category' : "Lifestyle",
        'updated' : "02 Jun 2022",
        'App_name' : "\uace8\ub4dc\uc2a4\ud47c : \uad6d\ub0b4 \ucd5c\ub300 \uc778\uc99d\ud615 \uc18c\uac1c\ud305\uc571",
        'Package_name' : "com.goldspoon",
        'installs' : "100,000+",
        'version' : "3.5.19",
    },
    'com.cmge.sdxm.gp' :
    {
        'category' : "Games",
        'updated' : "13 Apr 2022",
        'App_name' : "\u65b0\u5c04\u96d5\u7fa4\u4fa0\u4f20\u4e4b\u94c1\u8840\u4e39\u5fc3",
        'Package_name' : "com.cmge.sdxm.gp",
        'installs' : "100,000+",
        'version' : "1.3.2",
    },
    '963073142' :
    {
        'category' : "Games",
        'updated' : "2022-06-10T03:00:09Z",
        'App_name' : "\u3042\u3093\u3055\u3093\u3076\u308b\u30b9\u30bf\u30fc\u30ba\uff01\uff01Basic",
        'Package_name' : "jp.co.happyelements.boys",
        'version' : "3.0.51",
        'minimumOsVersion' : "11.0",
    },
    'com.eyougame.skzm.th' :
    {
        'category' : "Games",
        'updated' : "27 May 2021",
        'App_name' : "Eternal Land\uff1aMMORPG",
        'Package_name' : "com.eyougame.skzm.th",
        'installs' : "100,000+",
        'version' : "2005.6.0",
    },
    'com.cyfirma.defnce' :
    {
        'category' : "Tools",
        'updated' : "31 May 2022",
        'App_name' : "DeFNCE",
        'Package_name' : "com.cyfirma.defnce",
        'installs' : "10,000+",
        'version' : "1.0.1",
    },
    '1293646758' :
    {
        'category' : "Entertainment",
        'updated' : "2022-06-09T17:43:22Z",
        'App_name' : "UNICORN - Jogos de pintar",
        'Package_name' : "com.appcraft.pixelart",
        'version' : "5.3.0",
        'minimumOsVersion' : "13.0",
    },
    '784389203' :
    {
        'category' : "Entertainment",
        'updated' : "2022-06-20T06:57:02Z",
        'App_name' : "LOTTO 6aus49 & Eurojackpot",
        'Package_name' : "de.lotto24.lottokiosk.app",
        'version' : "6.26.2",
        'minimumOsVersion' : "14.0",
    },
    'com.marscatgames.camelotproject' :
    {
        'category' : "Games",
        'updated' : "10 Mar 2021",
        'App_name' : "\u738b\u9818\u9a0e\u58eb - \u81f3\u9ad8\u8056\u5802",
        'Package_name' : "com.marscatgames.camelotproject",
        'installs' : "100,000+",
        'version' : "4.2.0",
    },
    'net.cjcj.cjolivekitchen' :
    {
        'category' : "Food & Drink",
        'updated' : "29 Oct 2021",
        'App_name' : "CJ\ucfe1\ud0b7",
        'Package_name' : "net.cjcj.cjolivekitchen",
        'installs' : "100,000+",
        'version' : "1.2.1",
    },
    'vn.anfin.app' :
    {
        'category' : "Finance",
        'updated' : "12 Jun 2022",
        'App_name' : "Anfin - \u0110\u1ea7u t\u01b0 ch\u1ee9ng kho\u00e1n",
        'Package_name' : "vn.anfin.app",
        'installs' : "100,000+",
        'version' : "1.5.8",
    },
    'com.igs.mjstar31' :
    {
        'category' : "Casino",
        'updated' : "27 May 2022",
        'App_name' : "\u9ebb\u5c07 \u660e\u661f3\u7f3a1-16\u5f35Mahjong\u3001Slot\u3001Poker",
        'Package_name' : "com.igs.mjstar31",
        'installs' : "5,000,000+",
        'version' : "6.9.104",
    },
    'com.yunbu.nonogram.puzzle' :
    {
        'category' : "Games",
        'updated' : "17 May 2022",
        'App_name' : "Nonogram - Jigsaw Puzzle Game",
        'Package_name' : "com.yunbu.nonogram.puzzle",
        'installs' : "1,000,000+",
        'version' : "4.5.2",
    },
    'jp.co.yoozoo.sgsblast' :
    {
        'category' : "Games",
        'updated' : "24 Apr 2022",
        'App_name' : "\u4e09\u56fd\u5fd7\u30d6\u30e9\u30b9\u30c8-\u5c11\u5e74\u30d2\u30fc\u30ed\u30fc\u30ba",
        'Package_name' : "jp.co.yoozoo.sgsblast",
        'installs' : "500,000+",
        'version' : "1.18.54",
    },
    'live.hatch' :
    {
        'category' : "Games",
        'updated' : "30 Nov 2020",
        'App_name' : "Hatch: Play great games on demand",
        'Package_name' : "live.hatch",
        'installs' : "100,000+",
        'version' : "1.25.0",
    },
    'com.gamexx.qyj.vn' :
    {
        'category' : "Games",
        'updated' : "07 May 2022",
        'App_name' : "THI\u00caN H\u1ea0 V\u00d4 SONG",
        'Package_name' : "com.gamexx.qyj.vn",
        'installs' : "100,000+",
        'version' : "1.1.6",
    },
    'com.zimad.magicwordconnect' :
    {
        'category' : "Games",
        'updated' : "15 Jul 2021",
        'App_name' : "Magic Word Search from Letters",
        'Package_name' : "com.zimad.magicwordconnect",
        'installs' : "100,000+",
        'version' : "1.13.3",
    },
    'net.lightcon.godofpoker' :
    {
        'category' : "Casino",
        'updated' : "21 Jan 2022",
        'App_name' : "\ud3ec\ucee4\uc758 \uc2e0 : \uce74\uce74\uc624 \uacf5\uc2dd \uce74\uc9c0\ub178",
        'Package_name' : "net.lightcon.godofpoker",
        'installs' : "50,000+",
        'version' : "1.08.00",
    },
    'com.gm99.kd' :
    {
        'category' : "Games",
        'updated' : "22 Oct 2021",
        'App_name' : "\u7834\u5c40\uff1a\u4e09\u570b\u7d42\u7ae0",
        'Package_name' : "com.gm99.kd",
        'installs' : "100,000+",
        'version' : "2.0",
    },
    'com.cartlow.android' :
    {
        'category' : "Shopping",
        'updated' : "25 May 2022",
        'App_name' : "Cartlow",
        'Package_name' : "com.cartlow.android",
        'installs' : "1,000,000+",
        'version' : "6.1.3",
    },
    'com.cjenm.monster' :
    {
        'category' : "Games",
        'updated' : "19 May 2022",
        'App_name' : "\ubaac\uc2a4\ud130 \uae38\ub4e4\uc774\uae30",
        'Package_name' : "com.cjenm.monster",
        'installs' : "10,000,000+",
        'version' : "10.090",
    },
    'com.juhaoliao.vochat' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "Falla-Group Voice Chat Rooms",
        'Package_name' : "com.juhaoliao.vochat",
        'installs' : "500,000+",
        'version' : "V4.9.9",
    },
    'com.mobile.pomelo' :
    {
        'category' : "Shopping",
        'updated' : "07 Jun 2022",
        'App_name' : "Pomelo Fashion",
        'Package_name' : "com.mobile.pomelo",
        'installs' : "1,000,000+",
        'version' : "2.190.2",
    },
    'com.hanssem.mall' :
    {
        'category' : "House & Home",
        'updated' : "29 Apr 2022",
        'App_name' : "\ud55c\uc0d8\ubab0 - \ud1a0\ud0c8 \ud648 \uc778\ud14c\ub9ac\uc5b4 \uc1fc\ud551\ubab0",
        'Package_name' : "com.hanssem.mall",
        'installs' : "1,000,000+",
        'version' : "4.4.3",
    },
    '1175969205' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-06T04:27:00Z",
        'App_name' : "Pococha Live",
        'Package_name' : "com.dena.pokota",
        'version' : "5.15.2",
        'minimumOsVersion' : "14.0",
    },
    'com.mini.joy.lite' :
    {
        'category' : "Games",
        'updated' : "14 Jan 2021",
        'App_name' : "MiniJoy Lite - P2E",
        'Package_name' : "com.mini.joy.lite",
        'installs' : "10,000,000+",
        'version' : "5.0.0",
    },
    'com.veraxen.jigsawpuzzlescollectionhd' :
    {
        'category' : "Games",
        'updated' : "25 Apr 2022",
        'App_name' : "Jigsaw Puzzles Collection HD",
        'Package_name' : "com.veraxen.jigsawpuzzlescollectionhd",
        'installs' : "10,000,000+",
        'version' : "1.4.11",
    },
    'com.netease.lotr' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "The Lord of the Rings: War",
        'Package_name' : "com.netease.lotr",
        'installs' : "5,000,000+",
        'version' : "1.0.189088",
    },
    'com.ss.android.ugc.trill' :
    {
        'category' : "Entertainment",
        'updated' : "16 Jun 2022",
        'App_name' : "TikTok",
        'Package_name' : "com.ss.android.ugc.trill",
        'installs' : "500,000,000+",
        'version' : "Varies with device",
    },
    'com.netmarble.kofkr' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "\ud0b9 \uc624\ube0c \ud30c\uc774\ud130 \uc62c\uc2a4\ud0c0",
        'Package_name' : "com.netmarble.kofkr",
        'installs' : "1,000,000+",
        'version' : "1.11.3",
    },
    'com.cbs.app' :
    {
        'category' : "Entertainment",
        'updated' : "09 Jun 2022",
        'App_name' : "Paramount+ | Peak Streaming",
        'Package_name' : "com.cbs.app",
        'installs' : "10,000,000+",
        'version' : "8.1.23",
    },
    'com.dreamstudio.word.connect' :
    {
        'category' : "Games",
        'updated' : "29 Dec 2020",
        'App_name' : "Word Connect - Lucky Puzzle Game to Big Win",
        'Package_name' : "com.dreamstudio.word.connect",
        'installs' : "1,000,000+",
        'version' : "1.0.27",
    },
    '1250946975' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-16T10:37:11Z",
        'App_name' : "Hily App: Encontros.Streaming",
        'Package_name' : "com.hily.ios",
        'version' : "5.53.0",
        'minimumOsVersion' : "13.0",
    },
    'br.com.vivo.vivoeasy' :
    {
        'category' : "Lifestyle",
        'updated' : "16 Jun 2022",
        'App_name' : "Vivo Easy: Plano Celular",
        'Package_name' : "br.com.vivo.vivoeasy",
        'installs' : "5,000,000+",
        'version' : "4.2.22",
    },
    'com.sesisoft.vampireromance.and' :
    {
        'category' : "Games",
        'updated' : "22 Feb 2022",
        'App_name' : "\ubc40\ud30c\uc774\uc5b4 \ub85c\ub9e8\uc2a4",
        'Package_name' : "com.sesisoft.vampireromance.and",
        'installs' : "10,000+",
        'version' : "1.0.61",
    },
    '1477775889' :
    {
        'category' : "Games",
        'updated' : "2022-06-12T03:21:38Z",
        'App_name' : "Holdem or Foldem: Texas Poker",
        'Package_name' : "com.forevernine.texaspoker.ios",
        'version' : "V 1.5.10",
        'minimumOsVersion' : "10.0",
    },
    '1169754011' :
    {
        'category' : "Games",
        'updated' : "2022-02-16T22:57:58Z",
        'App_name' : "Superb IQ Test - Brain Quiz",
        'Package_name' : "com.sharplylabs.q",
        'version' : "3.5.6",
        'minimumOsVersion' : "12.0",
    },
    'com.lang.lang' :
    {
        'category' : "Social and Communication",
        'updated' : "17 Jun 2022",
        'App_name' : "LANG LIVE - LIVE Music Shows",
        'Package_name' : "com.lang.lang",
        'installs' : "1,000,000+",
        'version' : "6.0.5.4",
    },
    'video.like' :
    {
        'category' : "Social and Communication",
        'updated' : "17 Jun 2022",
        'App_name' : "Likee - Short Video Community",
        'Package_name' : "video.like",
        'installs' : "500,000,000+",
        'version' : "3.90.1",
    },
    'hk.com.wegames.god.and' :
    {
        'category' : "Games",
        'updated' : "28 Apr 2020",
        'App_name' : "\u795e\u7684\u6230\u722dGOD-\u767b\u5165\u9001SSR\u82f1\u96c4\uff01",
        'Package_name' : "hk.com.wegames.god.and",
        'installs' : "10,000+",
        'version' : "1.0",
    },
    'com.efunkr.krlh.google' :
    {
        'category' : "Games",
        'updated' : "24 Jun 2020",
        'App_name' : "\uc774\ud130:\ub358\uc804\uc758 \ud3ec\uc2dd\uc790",
        'Package_name' : "com.efunkr.krlh.google",
        'installs' : "100,000+",
        'version' : "1.9.0",
    },
    'com.elcoach.me' :
    {
        'category' : "Health and Fitness",
        'updated' : "09 Jun 2022",
        'App_name' : "\u0627\u0644\u0643\u0648\u062a\u0634 - \u062a\u0645\u0627\u0631\u064a\u0646 \u0648\u062a\u063a\u0630\u064a\u0629 \u0648\u062a\u062e\u0633\u064a\u0633",
        'Package_name' : "com.elcoach.me",
        'installs' : "1,000,000+",
        'version' : "4.1.7",
    },
    'com.ocb.omniextra' :
    {
        'category' : "Finance",
        'updated' : "17 May 2022",
        'App_name' : "OCB OMNI - Digital Bank",
        'Package_name' : "com.ocb.omniextra",
        'installs' : "500,000+",
        'version' : "2.19.0",
    },
    'jp.co.sfidante.yearcard' :
    {
        'category' : "Entertainment",
        'updated' : "24 May 2022",
        'App_name' : "\u5e74\u8cc0\u72b6 2022 \u30b9\u30de\u30db\u3067\u5199\u771f\u5e74\u8cc0\u72b6\u3000\u30a2\u30d7\u30ea\u3067\u7c21\u5358\u304a\u3057\u3083\u308c",
        'Package_name' : "jp.co.sfidante.yearcard",
        'installs' : "500,000+",
        'version' : "10.0.13",
    },
    'com.jp.rlwj' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2020",
        'App_name' : "\u738b\u5ba4\u59eb\u871c-\u591c\u306b\u8822\u304f\u304a\u59eb\u69d8\u305f\u3061\u306e\u7981\u65ad\u5927\u5965",
        'Package_name' : "com.jp.rlwj",
        'installs' : "100,000+",
        'version' : "1.14.0",
    },
    'com.i_til.monbas.android' :
    {
        'category' : "Games",
        'updated' : "16 Apr 2022",
        'App_name' : "\u3061\u3087\u3044\u3068\u53ec\u559a\uff01\u30e2\u30f3\u30b9\u30bf\u30fc\u30d0\u30b9\u30b1\u30c3\u30c8",
        'Package_name' : "com.i_til.monbas.android",
        'installs' : "100,000+",
        'version' : "1.0.6",
    },
    'com.indie.jz3d.google.kr' :
    {
        'category' : "Games",
        'updated' : "17 Jun 2022",
        'App_name' : "\uc2e0\uba852:\uc544\uc218\ub77c",
        'Package_name' : "com.indie.jz3d.google.kr",
        'installs' : "100,000+",
        'version' : "2.2.7",
    },
    'com.shopee.th' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Shopee TH: Online shopping app",
        'Package_name' : "com.shopee.th",
        'installs' : "50,000,000+",
        'version' : "2.88.41",
    },
    'com.asobimo.toramonline' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "RPG Toram Online - MMORPG",
        'Package_name' : "com.asobimo.toramonline",
        'installs' : "10,000,000+",
        'version' : "3.5.22",
    },
    '749083919' :
    {
        'category' : "Utilities",
        'updated' : "2022-06-06T14:13:01Z",
        'App_name' : "\u6c17\u8c61\u30e9\u30a4\u30d6 - \u5730\u57df\u306e\u5929\u6c17\u4e88\u5831",
        'Package_name' : "com.apalonapps.wlf",
        'version' : "7.5.1",
        'minimumOsVersion' : "12.4",
    },
    'com.endlesswar.google' :
    {
        'category' : "Games",
        'updated' : "31 Mar 2021",
        'App_name' : "\uc368\ud074 Re: \ud64d\uc6d4\uce68\uc2dd",
        'Package_name' : "com.endlesswar.google",
        'installs' : "10,000+",
        'version' : "1.2.0",
    },
    '517914548' :
    {
        'category' : "Tools",
        'updated' : "2022-06-20T06:56:29Z",
        'App_name' : "Dashlane \u2013 Password Manager",
        'Package_name' : "com.dashlane.dashlanephonefinal",
        'version' : "6.2224.0",
        'minimumOsVersion' : "15.0",
    },
    'com.bandainamcoent.dblegends_ww' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "DRAGON BALL LEGENDS",
        'Package_name' : "com.bandainamcoent.dblegends_ww",
        'installs' : "10,000,000+",
        'version' : "4.5.0",
    },
    'com.hound13.hundredsoul' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Hundred Soul",
        'Package_name' : "com.hound13.hundredsoul",
        'installs' : "1,000,000+",
        'version' : "4.30.0",
    },
    '1185445287' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-09T09:10:53Z",
        'App_name' : "Go2Joy - Hourly Booking App",
        'Package_name' : "com.appromobile.Hotel",
        'version' : "14.10.1",
        'minimumOsVersion' : "12.0",
    },
    'com.rekoo.startrigger' :
    {
        'category' : "Games",
        'updated' : "17 May 2021",
        'App_name' : "\u30b9\u30bf\u30fc\u30c8\u30ea\u30ac\u30fc\u3010\u723d\u5feb\u30ac\u30f3\u30b7\u30e5\u30fc\u30c6\u30a3\u30f3\u30b0\u30d0\u30c8\u30eb\u3011",
        'Package_name' : "com.rekoo.startrigger",
        'installs' : "500,000+",
        'version' : "18.0.340",
    },
    'com.videomonster.videomonster' :
    {
        'category' : "Entertainment",
        'updated' : "16 Jun 2022",
        'App_name' : "VideoMonster - Make/Edit Video",
        'Package_name' : "com.videomonster.videomonster",
        'installs' : "500,000+",
        'version' : "1.210",
    },
    'th.in.njoy.tkex' :
    {
        'category' : "Games",
        'updated' : "01 Dec 2020",
        'App_name' : "3 Kingdoms Extreme",
        'Package_name' : "th.in.njoy.tkex",
        'installs' : "10,000+",
        'version' : "1.1.0",
    },
    '1477841973' :
    {
        'category' : "Games",
        'updated' : "2022-06-10T02:28:28Z",
        'App_name' : "Brain Out -Tricky riddle games",
        'Package_name' : "com.mind.quiz.brain.out",
        'version' : "1.2.11",
        'minimumOsVersion' : "11.0",
    },
    '1096493180' :
    {
        'category' : "Entertainment",
        'updated' : "2022-06-20T01:15:06Z",
        'App_name' : "WATCHA",
        'Package_name' : "com.frograms.WATCHAPLAY",
        'version' : "1.18.8",
        'minimumOsVersion' : "13.0",
    },
    'com.borrowell.strangelove' :
    {
        'category' : "Finance",
        'updated' : "06 Jun 2022",
        'App_name' : "Borrowell",
        'Package_name' : "com.borrowell.strangelove",
        'installs' : "100,000+",
        'version' : "8.1.0",
    },
    'com.kreditpintar' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "Kredit Pintar-Pinjaman Online",
        'Package_name' : "com.kreditpintar",
        'installs' : "10,000,000+",
        'version' : "1.9.85",
    },
    '473908163' :
    {
        'category' : "Games",
        'updated' : "2022-06-17T05:09:14Z",
        'App_name' : "\u30c1\u30e7\u30b3\u30c3\u30c8\u30e9\u30f3\u30c9SP",
        'Package_name' : "jp.nhncorp.Chocotto",
        'version' : "8.1.3",
        'minimumOsVersion' : "10.0",
    },
    'com.sixwaves.shinsengoku' :
    {
        'category' : "Games",
        'updated' : "31 Mar 2022",
        'App_name' : "\u7345\u5b50\u306e\u5982\u304f\uff5e\u6226\u56fd\u8987\u738b\u6226\u8a18\uff5e",
        'Package_name' : "com.sixwaves.shinsengoku",
        'installs' : "100,000+",
        'version' : "1.3.1",
    },
    'com.byjus.aakash.thelearningapp' :
    {
        'category' : "Education and Books",
        'updated' : "16 Jun 2022",
        'App_name' : "Aakash App for JEE & NEET",
        'Package_name' : "com.byjus.aakash.thelearningapp",
        'installs' : "10,000,000+",
        'version' : "2.12.2.13562",
    },
    'com.celebfie' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Apr 2022",
        'App_name' : "Celebfie - Creating Unforgettable Moments",
        'Package_name' : "com.celebfie",
        'installs' : "100,000+",
        'version' : "1.8.5",
    },
    'com.eyougame.ylhj' :
    {
        'category' : "Games",
        'updated' : "17 Jan 2022",
        'App_name' : "\u5fa1\u7075\u7ed8\u5377",
        'Package_name' : "com.eyougame.ylhj",
        'installs' : "100,000+",
        'version' : "32.0",
    },
    '1057945425' :
    {
        'category' : "Finance",
        'updated' : "2022-04-18T06:47:13Z",
        'App_name' : "ZUB\u0130ZU \u2013 Markalarda Avantajlar",
        'Package_name' : "com.dogusms.zubizu",
        'version' : "1.9.6",
        'minimumOsVersion' : "11.0",
    },
    'com.photobook.android' :
    {
        'category' : "Entertainment",
        'updated' : "03 Jun 2022",
        'App_name' : "Photobook: Albums, Gifts and Prints",
        'Package_name' : "com.photobook.android",
        'installs' : "1,000,000+",
        'version' : "2.86.1",
    },
    'com.williamsinteractive.jackpotparty' :
    {
        'category' : "Casino",
        'updated' : "16 Jun 2022",
        'App_name' : "Jackpot Party Casino Slots",
        'Package_name' : "com.williamsinteractive.jackpotparty",
        'installs' : "10,000,000+",
        'version' : "5032.01",
    },
    'com.bp.mobile.bpme.us' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "19 May 2022",
        'App_name' : "BPme: BP & Amoco Gas Rewards",
        'Package_name' : "com.bp.mobile.bpme.us",
        'installs' : "1,000,000+",
        'version' : "5.3.3",
    },
    'com.positive.ceptesok' :
    {
        'category' : "Shopping",
        'updated' : "07 Jun 2022",
        'App_name' : "Cepte \u015eok",
        'Package_name' : "com.positive.ceptesok",
        'installs' : "1,000,000+",
        'version' : "2.2.3",
    },
    'br.com.icarros.androidapp' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "17 Jun 2022",
        'App_name' : "iCarros- Comprar e Vender Carros",
        'Package_name' : "br.com.icarros.androidapp",
        'installs' : "5,000,000+",
        'version' : "4.25.7",
    },
    'co.taxibeat.driver' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "Beat Driver",
        'Package_name' : "co.taxibeat.driver",
        'installs' : "5,000,000+",
        'version' : "12.76",
    },
    'com.ataexpress.tiklagelsin' :
    {
        'category' : "Food & Drink",
        'updated' : "14 Jun 2022",
        'App_name' : "T\u0131kla Gelsin\u00ae",
        'Package_name' : "com.ataexpress.tiklagelsin",
        'installs' : "1,000,000+",
        'version' : "2.3.1",
    },
    '1486135528' :
    {
        'category' : "Shopping",
        'updated' : "2021-11-29T05:53:12Z",
        'App_name' : "Tryano Luxury Destination",
        'Package_name' : "com.tryano",
        'version' : "3.0",
        'minimumOsVersion' : "11.0",
    },
    'com.eyougame.genyo' :
    {
        'category' : "Games",
        'updated' : "15 Apr 2022",
        'App_name' : "\u5e7b\u5996\u7269\u8a9e-\u5341\u516d\u591c\u306e\u8f2a\u5efb",
        'Package_name' : "com.eyougame.genyo",
        'installs' : "100,000+",
        'version' : "41.1",
    },
    'com.excellentplay.ms' :
    {
        'category' : "Games",
        'updated' : "21 Mar 2022",
        'App_name' : "\u964c\u4e0a\u82b1\u958b",
        'Package_name' : "com.excellentplay.ms",
        'installs' : "50,000+",
        'version' : "10002",
    },
    'com.wowfaces' :
    {
        'category' : "Entertainment",
        'updated' : "01 May 2021",
        'App_name' : "Faces: funny face changer",
        'Package_name' : "com.wowfaces",
        'installs' : "1,000,000+",
        'version' : "3.5.6",
    },
    'com.bjx.rpg.jp.mobile' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2021",
        'App_name' : "\u9006\u547d\u4f7f\u3044\u306e\u5c11\u5e74-\u65ac\u6ec5\u306e\u5203-\u672c\u683c\u7684\u306aRPG",
        'Package_name' : "com.bjx.rpg.jp.mobile",
        'installs' : "10,000+",
        'version' : "1.0.77.0",
    },
    'com.peakx.stickfight.and.inter' :
    {
        'category' : "Games",
        'updated' : "12 Nov 2020",
        'App_name' : "\u6211\u529f\u592b\u8cca\u6e9c",
        'Package_name' : "com.peakx.stickfight.and.inter",
        'installs' : "1,000,000+",
        'version' : "0.7.2",
    },
    'com.sisal.sisalpay' :
    {
        'category' : "Finance",
        'updated' : "24 May 2022",
        'App_name' : "Mooney App: pagamenti digitali",
        'Package_name' : "com.sisal.sisalpay",
        'installs' : "1,000,000+",
        'version' : "5.4.1",
    },
    'io.moia.neptune' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "15 Jun 2022",
        'App_name' : "MOIA - In Hamburg & Hanover",
        'Package_name' : "io.moia.neptune",
        'installs' : "100,000+",
        'version' : "22.24.100",
    },
    'com.mj.blg.google' :
    {
        'category' : "Games",
        'updated' : "24 Dec 2018",
        'App_name' : "\ube44\ub8e1\uc7a0\ud638",
        'Package_name' : "com.mj.blg.google",
        'installs' : "10,000+",
        'version' : "1.1.21.0",
    },
    'com.amazon.kindle' :
    {
        'category' : "Education and Books",
        'updated' : "16 Jun 2022",
        'App_name' : "Amazon Kindle",
        'Package_name' : "com.amazon.kindle",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'th.co.homepro.mobileapp' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "HomePro | Home Shopping",
        'Package_name' : "th.co.homepro.mobileapp",
        'installs' : "500,000+",
        'version' : "1.4.7",
    },
    'com.ictech.mono' :
    {
        'category' : "Lifestyle",
        'updated' : "16 Jun 2022",
        'App_name' : "Mono",
        'Package_name' : "com.ictech.mono",
        'installs' : "500,000+",
        'version' : "2.1.1",
    },
    'id.anteraja.aca' :
    {
        'category' : "Finance",
        'updated' : "17 May 2022",
        'App_name' : "Anteraja",
        'Package_name' : "id.anteraja.aca",
        'installs' : "1,000,000+",
        'version' : "1.7.25",
    },
    'com.siu.koluman' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "04 Jun 2022",
        'App_name' : "Koluman",
        'Package_name' : "com.siu.koluman",
        'installs' : "1,000+",
        'version' : "1.2.3",
    },
    '1163776422' :
    {
        'category' : "Finance",
        'updated' : "2022-06-10T10:02:39Z",
        'App_name' : "Taxfix \u2013 Easy tax declaration",
        'Package_name' : "de.taxfix.Taxfix",
        'version' : "1.182.0",
        'minimumOsVersion' : "11.0",
    },
    '940320341' :
    {
        'category' : "Games",
        'updated' : "2022-05-23T00:37:00Z",
        'App_name' : "\u30d7\u30ed\u91ce\u7403\u30b9\u30d4\u30ea\u30c3\u30c4\uff21",
        'Package_name' : "jp.konami.prospia",
        'version' : "14.2.0",
        'minimumOsVersion' : "8.0",
    },
    'kr.co.itforone.lover2' :
    {
        'category' : "Social and Communication",
        'updated' : "28 Oct 2021",
        'App_name' : "\uc5f0\uc778 - \ub9e4\ub2c8\uc800\uac00 \ud574\uc8fc\ub294 \uc2e4\uc2dc\uac04 \uc18c\uac1c\ud305,\ub370\uc774\ud2b8",
        'Package_name' : "kr.co.itforone.lover2",
        'installs' : "10,000+",
        'version' : "1.25",
    },
    'com.tokopedia.kelontongapp' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Mitra Tokopedia: PPOB & Grosir",
        'Package_name' : "com.tokopedia.kelontongapp",
        'installs' : "1,000,000+",
        'version' : "1.13.5",
    },
    'com.ripio.android' :
    {
        'category' : "Finance",
        'updated' : "11 May 2022",
        'App_name' : "Ripio Bitcoin Wallet",
        'Package_name' : "com.ripio.android",
        'installs' : "1,000,000+",
        'version' : "5.36.8",
    },
    'com.superbinogo.jungleboyadventure' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Super Bino Go:Adventure Jungle",
        'Package_name' : "com.superbinogo.jungleboyadventure",
        'installs' : "100,000,000+",
        'version' : "2.0.6",
    },
    'com.neowiz.games.poker' :
    {
        'category' : "Casino",
        'updated' : "19 May 2022",
        'App_name' : "Pmang Poker : Casino Royal",
        'Package_name' : "com.neowiz.games.poker",
        'installs' : "5,000,000+",
        'version' : "80.0",
    },
    'com.crazylabs.diamond.paint.by.number' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Diamond Painting ASMR Coloring",
        'Package_name' : "com.crazylabs.diamond.paint.by.number",
        'installs' : "10,000,000+",
        'version' : "0.3.2.1",
    },
    'com.akpublish.zombie' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "Zombeast: Zombie Shooter",
        'Package_name' : "com.akpublish.zombie",
        'installs' : "5,000,000+",
        'version' : "0.29.2",
    },
    'com.zvooq.openplay' :
    {
        'category' : "Entertainment",
        'updated' : "10 Jun 2022",
        'App_name' : "Zvuk: Music and podcasts",
        'Package_name' : "com.zvooq.openplay",
        'installs' : "10,000,000+",
        'version' : "4.13",
    },
    'com.varomoney.varo' :
    {
        'category' : "Finance",
        'updated' : "14 Feb 2021",
        'App_name' : "Varo Money - Existing Users Only",
        'Package_name' : "com.varomoney.varo",
        'installs' : "1,000,000+",
        'version' : "1.7.4",
    },
    'com.bosco.boscoApp' :
    {
        'category' : "Health and Fitness",
        'updated' : "30 May 2022",
        'App_name' : "Bosco: Safety for Kids",
        'Package_name' : "com.bosco.boscoApp",
        'installs' : "100,000+",
        'version' : "2022.05.5",
    },
    'com.andromedagames.samkarma' :
    {
        'category' : "Games",
        'updated' : "13 Aug 2021",
        'App_name' : "\uc0bc\uad6d\uc9c0 \uce74\ub974\ub9c8 - \ubc29\uce58\ud615RPG",
        'Package_name' : "com.andromedagames.samkarma",
        'installs' : "10,000+",
        'version' : "1.13.0",
    },
    'id.codigo.klikdokter' :
    {
        'category' : "Health and Fitness",
        'updated' : "28 Apr 2022",
        'App_name' : "KlikDokter: Healthy Solutions",
        'Package_name' : "id.codigo.klikdokter",
        'installs' : "1,000,000+",
        'version' : "1.27.12",
    },
    'za.co.takectrl.app' :
    {
        'category' : "Finance",
        'updated' : "17 May 2022",
        'App_name' : "Ctrl - Insurance assistant",
        'Package_name' : "za.co.takectrl.app",
        'installs' : "10,000+",
        'version' : "3.4.2",
    },
    'com.accor.appli.hybrid' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "17 Jun 2022",
        'App_name' : "Accor All - Hotel booking",
        'Package_name' : "com.accor.appli.hybrid",
        'installs' : "1,000,000+",
        'version' : "9.65.3",
    },
    'com.superplaystudios.dicedreams' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "Dice Dreams\u2122\ufe0f",
        'Package_name' : "com.superplaystudios.dicedreams",
        'installs' : "10,000,000+",
        'version' : "1.45.0.7768",
    },
    'com.higgs.cubemaster3d' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "Cube Master 3D - Match Puzzle",
        'Package_name' : "com.higgs.cubemaster3d",
        'installs' : "10,000,000+",
        'version' : "1.6.1",
    },
    'com.beenverified.android' :
    {
        'category' : "Tools",
        'updated' : "08 May 2022",
        'App_name' : "BeenVerified: People Search",
        'Package_name' : "com.beenverified.android",
        'installs' : "1,000,000+",
        'version' : "6.03.33",
    },
    'com.dealdash' :
    {
        'category' : "Shopping",
        'updated' : "31 Mar 2022",
        'App_name' : "DealDash - Bid & Save Auctions",
        'Package_name' : "com.dealdash",
        'installs' : "5,000,000+",
        'version' : "5.6.1",
    },
    'com.eyougame.yjzy' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "\u6708\u898b\u4e4b\u87a2",
        'Package_name' : "com.eyougame.yjzy",
        'installs' : "100,000+",
        'version' : "23.0",
    },
    'com.innogames.elvenar' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Elvenar - Fantasy Kingdom",
        'Package_name' : "com.innogames.elvenar",
        'installs' : "5,000,000+",
        'version' : "1.154.2",
    },
    'com.ncsoft.lineagem19' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\ub9ac\ub2c8\uc9c0M",
        'Package_name' : "com.ncsoft.lineagem19",
        'installs' : "1,000,000+",
        'version' : "1.7.53a",
    },
    'com.rupeeredee.app' :
    {
        'category' : "Finance",
        'updated' : "25 May 2022",
        'App_name' : "RupeeRedee - Personal Loan App",
        'Package_name' : "com.rupeeredee.app",
        'installs' : "1,000,000+",
        'version' : "2.2.35",
    },
    '1492783870' :
    {
        'category' : "Not available",
        'updated' : "2022-06-08T04:25:35Z",
        'App_name' : "My Wow Memories: Event Videos",
        'Package_name' : "com.mywowmemories",
        'version' : "1.1.8",
        'minimumOsVersion' : "14.1",
    },
    'com.reddit.frontpage' :
    {
        'category' : "Social and Communication",
        'updated' : "15 Jun 2022",
        'App_name' : "Reddit",
        'Package_name' : "com.reddit.frontpage",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.sebbia.delivery' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Dostavista \u2014 \u0440\u0430\u0431\u043e\u0442\u0430 \u043a\u0443\u0440\u044c\u0435\u0440\u043e\u043c",
        'Package_name' : "com.sebbia.delivery",
        'installs' : "1,000,000+",
        'version' : "2.77.0",
    },
    'com.linecorp.LGFARM' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "LINE BROWN FARM",
        'Package_name' : "com.linecorp.LGFARM",
        'installs' : "10,000,000+",
        'version' : "3.3.6",
    },
    'com.zipdoc.zipdoc' :
    {
        'category' : "House & Home",
        'updated' : "11 Mar 2022",
        'App_name' : "\uc9d1\ub2e5 - \uac04\ud3b8\uc548\uc2ec \uc778\ud14c\ub9ac\uc5b4",
        'Package_name' : "com.zipdoc.zipdoc",
        'installs' : "500,000+",
        'version' : "5.3.2",
    },
    'com.psafe.msuite' :
    {
        'category' : "Tools",
        'updated' : "18 Jun 2022",
        'App_name' : "dfndr security: antivirus",
        'Package_name' : "com.psafe.msuite",
        'installs' : "100,000,000+",
        'version' : "8.3.12",
    },
    'com.bitbnspay' :
    {
        'category' : "Finance",
        'updated' : "22 Dec 2021",
        'App_name' : "Bitbns Pay - Crypto trading, 0 fee payments",
        'Package_name' : "com.bitbnspay",
        'installs' : "1,000,000+",
        'version' : "1.8.7",
    },
    '1081463841' :
    {
        'category' : "Games",
        'updated' : "2022-06-02T08:26:53Z",
        'App_name' : "PlaySugarHouse Casino NJ",
        'Package_name' : "com.rushstreetinteractive.sugarhouse.nj.real.casino",
        'version' : "2022.6",
        'minimumOsVersion' : "13.0",
    },
    'com.devsisters.gb' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "Cookie Run: OvenBreak",
        'Package_name' : "com.devsisters.gb",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'de.yellostrom.incontrol' :
    {
        'category' : "Tools",
        'updated' : "14 Jun 2022",
        'App_name' : "kWhapp \u2013 Strom & Gas Check",
        'Package_name' : "de.yellostrom.incontrol",
        'installs' : "500,000+",
        'version' : "3.8.2_284#d0b9",
    },
    'jp.co.beeworks.nameko.hkt' :
    {
        'category' : "Games",
        'updated' : "13 May 2022",
        'App_name' : "\u306a\u3081\u3053\u767a\u6398\u30ad\u30c3\u30c8",
        'Package_name' : "jp.co.beeworks.nameko.hkt",
        'installs' : "100,000+",
        'version' : "1.8.3",
    },
    '804641004' :
    {
        'category' : "Tools",
        'updated' : "2022-05-23T13:58:32Z",
        'App_name' : "Speak & Translate - Translator",
        'Package_name' : "com.appicfun.translatorfree",
        'version' : "5.0.20",
        'minimumOsVersion' : "10.3",
    },
    '1160056295' :
    {
        'category' : "Games",
        'updated' : "2022-06-01T12:19:51Z",
        'App_name' : "Mobile Legends: Bang Bang",
        'Package_name' : "com.mobile.legends",
        'version' : "1.6.86.748.3",
        'minimumOsVersion' : "9.0",
    },
    'com.northerly.bumpr' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "24 Mar 2022",
        'App_name' : "GoBumpr - Car & Bike Service",
        'Package_name' : "com.northerly.bumpr",
        'installs' : "500,000+",
        'version' : "2.3.7",
    },
    '1491206418' :
    {
        'category' : "Games",
        'updated' : "2022-03-23T16:12:47Z",
        'App_name' : "Wood Shop",
        'Package_name' : "com.HeroGames.WoodShop",
        'version' : "3.0.0",
        'minimumOsVersion' : "10.0",
    },
    'mobi.abcmouse.japan.google' :
    {
        'category' : "Education and Books",
        'updated' : "02 Jun 2022",
        'App_name' : "ABCmouse English-\u5e7c\u5150\u5411\u3051\u82f1\u8a9e\u5b66\u7fd2\u30a2\u30d7\u30ea-",
        'Package_name' : "mobi.abcmouse.japan.google",
        'installs' : "50,000+",
        'version' : "9.19.202206010926",
    },
    'com.and.wareternal' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "War Eternal-Divine Battlefield",
        'Package_name' : "com.and.wareternal",
        'installs' : "1,000,000+",
        'version' : "1.0.106",
    },
    'com.apegroup.mcdonaldsrussia' :
    {
        'category' : "Food & Drink",
        'updated' : "02 Jun 2022",
        'App_name' : "\u041c\u043e\u0439 \u0411\u0443\u0440\u0433\u0435\u0440",
        'Package_name' : "com.apegroup.mcdonaldsrussia",
        'installs' : "10,000,000+",
        'version' : "8.10.2",
    },
    '1250331401' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-07T09:34:43Z",
        'App_name' : "franprix: Livraison de courses",
        'Package_name' : "com.franprix.franprix",
        'version' : "6.0",
        'minimumOsVersion' : "13.0",
    },
    'com.ming.event.jap.and' :
    {
        'category' : "Games",
        'updated' : "29 Apr 2022",
        'App_name' : "\u6975\u60aa\u90fd\u5e02\uff5e\u30bc\u30ed\u304b\u3089\u306e\u4efb\u4fa0\u9053\uff5e",
        'Package_name' : "com.ming.event.jap.and",
        'installs' : "10,000+",
        'version' : "1.0.6",
    },
    'com.krosskomics' :
    {
        'category' : "Education and Books",
        'updated' : "21 Mar 2022",
        'App_name' : "Kross Komics \u2013 Various Comics and Rewards!",
        'Package_name' : "com.krosskomics",
        'installs' : "1,000,000+",
        'version' : "1.0.9",
    },
    '1540922030' :
    {
        'category' : "Games",
        'updated' : "2022-05-18T06:52:47Z",
        'App_name' : "Rush - Play Games & Win Cash",
        'Package_name' : "com.hike.mazuma",
        'version' : "1.0.370",
        'minimumOsVersion' : "13.0",
    },
    'com.price_locq.mobile' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "PriceLOCQ - Save BIG on fuel",
        'Package_name' : "com.price_locq.mobile",
        'installs' : "100,000+",
        'version' : "1.17.0",
    },
    'com.indy' :
    {
        'category' : "Lifestyle",
        'updated' : "22 Nov 2021",
        'App_name' : "Swyp",
        'Package_name' : "com.indy",
        'installs' : "500,000+",
        'version' : "6.7",
    },
    'com.topface.topface' :
    {
        'category' : "Social and Communication",
        'updated' : "08 Jun 2022",
        'App_name' : "Topface - Dating Meeting Chat",
        'Package_name' : "com.topface.topface",
        'installs' : "10,000,000+",
        'version' : "4.5",
    },
    'com.pubg.krmobile' :
    {
        'category' : "Games",
        'updated' : "09 May 2022",
        'App_name' : "PUBG Mobile",
        'Package_name' : "com.pubg.krmobile",
        'installs' : "10,000,000+",
        'version' : "2.0.0",
    },
    '1438474507' :
    {
        'category' : "Games",
        'updated' : "2022-06-02T05:33:56Z",
        'App_name' : "\u30c7\u30a3\u30ba\u30cb\u30fc \u30df\u30e5\u30fc\u30b8\u30c3\u30af\u30d1\u30ec\u30fc\u30c9",
        'Package_name' : "jp.co.taito.magic",
        'version' : "2.2.1",
        'minimumOsVersion' : "9.0",
    },
    'trendyol.com' :
    {
        'category' : "Shopping",
        'updated' : "17 Jun 2022",
        'App_name' : "Trendyol - Online Shopping",
        'Package_name' : "trendyol.com",
        'installs' : "50,000,000+",
        'version' : "Varies with device",
    },
    '1491590039' :
    {
        'category' : "Social and Communication",
        'updated' : "2020-09-30T22:57:41Z",
        'App_name' : "\u4eba\u6c17\u306e\u51fa\u4f1a\u3044\u63a2\u3057\u306f\u30bb\u30ec\u30d6\u30ea\u30c6\u30a3-\u30a2\u30d7\u30ea\u3067\u8fd1\u6240\u306e\u53cb\u9054\u3092\u63a2\u3059",
        'Package_name' : "net.app-celeb.celebrity",
        'version' : "1.3",
        'minimumOsVersion' : "11.0",
    },
    'com.spcomes.esabyss' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "Abyss : Rebirth Phantom",
        'Package_name' : "com.spcomes.esabyss",
        'installs' : "100,000+",
        'version' : "1.78.1",
    },
    'com.srllimited.srl' :
    {
        'category' : "Health and Fitness",
        'updated' : "15 Feb 2022",
        'App_name' : "SRL Diagnostics",
        'Package_name' : "com.srllimited.srl",
        'installs' : "1,000,000+",
        'version' : "8.3.8",
    },
    '414461255' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-17T10:21:48Z",
        'App_name' : "Vivino: Buy the Right Wine",
        'Package_name' : "com.vivino",
        'version' : "2022.23.0",
        'minimumOsVersion' : "13.0",
    },
    'com.theinnerhour.b2b' :
    {
        'category' : "Health and Fitness",
        'updated' : "07 Jun 2022",
        'App_name' : "InnerHour: anxiety self-care",
        'Package_name' : "com.theinnerhour.b2b",
        'installs' : "1,000,000+",
        'version' : "3.64.2",
    },
    'com.dpc.carpro2' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "07 Jun 2022",
        'App_name' : "\uce74\uc218\ub9ac - \ucd9c\uc7a5\uc5d4\uc9c4\uc624\uc77c, \ubc30\ud130\ub9ac, \ucc28\ub7c9\uc815\ube44\uc810\uac80 by\uce74\ub791",
        'Package_name' : "com.dpc.carpro2",
        'installs' : "100,000+",
        'version' : "4.0.0",
    },
    'vn.funtap.mukytich' :
    {
        'category' : "Games",
        'updated' : "24 Feb 2022",
        'App_name' : "MU K\u1ef3 T\u00edch - Funtap",
        'Package_name' : "vn.funtap.mukytich",
        'installs' : "1,000,000+",
        'version' : "14.0.4",
    },
    '1176070496' :
    {
        'category' : "Games",
        'updated' : "2022-06-20T04:14:53Z",
        'App_name' : "\u541b\u306e\u76ee\u7684\u306f\u30dc\u30af\u3092\u6bba\u3059\u3053\u3068\uff13",
        'Package_name' : "com.fundoshiparade.bokukoro3jp",
        'version' : "12.1",
        'minimumOsVersion' : "10.0",
    },
    'com.gacraze.xmqy' :
    {
        'category' : "Games",
        'updated' : "16 Dec 2021",
        'App_name' : "\u5929\u6daf\u5e7b\u5922",
        'Package_name' : "com.gacraze.xmqy",
        'installs' : "100,000+",
        'version' : "2.1.0",
    },
    'kr.nacommunity.anyman' :
    {
        'category' : "Lifestyle",
        'updated' : "01 Apr 2022",
        'App_name' : "\uc560\ub2c8\ub9e8 - \uc2e4\uc2dc\uac04 \ub3c4\uc6c0 \uc694\uccad \uc571",
        'Package_name' : "kr.nacommunity.anyman",
        'installs' : "500,000+",
        'version' : "2.2.1",
    },
    'com.applications.lifestyle' :
    {
        'category' : "Shopping",
        'updated' : "04 May 2022",
        'App_name' : "Lifestyle - Online Shopping For Fashion & Clothing",
        'Package_name' : "com.applications.lifestyle",
        'installs' : "10,000,000+",
        'version' : "6.51",
    },
    'com.global.wfwd.google' :
    {
        'category' : "Games",
        'updated' : "12 May 2022",
        'App_name' : "\ud669\uc81c\uc758 \uafc8",
        'Package_name' : "com.global.wfwd.google",
        'installs' : "500,000+",
        'version' : "4.5.1",
    },
    'com.itaucard.activity' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Ita\u00fa: Cart\u00f5es de cr\u00e9dito",
        'Package_name' : "com.itaucard.activity",
        'installs' : "10,000,000+",
        'version' : "6.7.0",
    },
    'com.fineappstudio.android.petfriends' :
    {
        'category' : "Shopping",
        'updated' : "17 May 2022",
        'App_name' : "\ud3ab\ud504\ub80c\uc988 - \ubc18\ub824\ub3d9\ubb3c \ub300\ud45c \uc1fc\ud551\ubab0",
        'Package_name' : "com.fineappstudio.android.petfriends",
        'installs' : "500,000+",
        'version' : "2.7.0",
    },
    '949344041' :
    {
        'category' : "Games",
        'updated' : "2022-06-03T05:48:13Z",
        'App_name' : "LINE BROWN FARM",
        'Package_name' : "com.linecorp.LGFARM",
        'version' : "3.3.6",
        'minimumOsVersion' : "10.0",
    },
    'kr.game.infovine.zone4M.production' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "\uc81c4\uad6c\uc5ed : MMORPG",
        'Package_name' : "kr.game.infovine.zone4M.production",
        'installs' : "10,000+",
        'version' : "2.0.30",
    },
    'com.yeon.google' :
    {
        'category' : "Games",
        'updated' : "20 Oct 2020",
        'App_name' : "\ubbf8\ud638: \ucc9c\ub144\uc758 \uc0ac\ub791",
        'Package_name' : "com.yeon.google",
        'installs' : "100,000+",
        'version' : "1.0.1",
    },
    'com.glu.baseball18' :
    {
        'category' : "Games",
        'updated' : "11 Dec 2018",
        'App_name' : "MLB TAP SPORTS BASEBALL 2018",
        'Package_name' : "com.glu.baseball18",
        'installs' : "1,000,000+",
        'version' : "2.2.1",
    },
    '998409134' :
    {
        'category' : "Games",
        'updated' : "2022-06-14T02:03:04Z",
        'App_name' : "\u767e\u9b42\u6230\u8a18",
        'Package_name' : "com.hound13.HundredSouls",
        'version' : "4.30.0",
        'minimumOsVersion' : "10.3",
    },
    'com.mytona.cookingdiary.android' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Cooking Diary\u00ae Restaurant Game",
        'Package_name' : "com.mytona.cookingdiary.android",
        'installs' : "10,000,000+",
        'version' : "2.1.1",
    },
    '1471600958' :
    {
        'category' : "Games",
        'updated' : "2020-09-09T02:38:25Z",
        'App_name' : "\u6230\u9748M\uff1a\u9b54\u795e\u52ab",
        'Package_name' : "com.zhanhunios.chm",
        'version' : "1.0.24.0",
        'minimumOsVersion' : "9.3",
    },
    '1059697491' :
    {
        'category' : "Games",
        'updated' : "2022-06-15T05:42:42Z",
        'App_name' : "\u30e9\u30b9\u30c8\u30d4\u30ea\u30aa\u30c9 - \u5de1\u308a\u3042\u3046\u87ba\u65cb\u306e\u7269\u8a9e-",
        'Package_name' : "jp.co.happyelements.mirror",
        'version' : "2.10.7",
        'minimumOsVersion' : "10.0",
    },
    'ht.nct' :
    {
        'category' : "Entertainment",
        'updated' : "17 Jun 2022",
        'App_name' : "NCT - NhacCuaTui Nghe MP3",
        'Package_name' : "ht.nct",
        'installs' : "10,000,000+",
        'version' : "6.3.6",
    },
    'co.gradeup.android' :
    {
        'category' : "Education and Books",
        'updated' : "11 Jun 2022",
        'App_name' : "Exam Preparation: Live Classes",
        'Package_name' : "co.gradeup.android",
        'installs' : "10,000,000+",
        'version' : "11.21",
    },
    'com.kakaoent.kakaowebtoon' :
    {
        'category' : "Education and Books",
        'updated' : "20 May 2022",
        'App_name' : "KAKAO WEBTOON",
        'Package_name' : "com.kakaoent.kakaowebtoon",
        'installs' : "1,000,000+",
        'version' : "3.7.0",
    },
    '309465525' :
    {
        'category' : "Not available",
        'updated' : "2022-06-15T13:31:38Z",
        'App_name' : "Shutterfly: Prints Cards Gifts",
        'Package_name' : "com.shutterfly.ShutterflyUploader",
        'version' : "12.11.1",
        'minimumOsVersion' : "13.0",
    },
    'com.hnhsoft.dnslot' :
    {
        'category' : "Casino",
        'updated' : "29 Mar 2022",
        'App_name' : "\uc62c\uc2a4\ud0c0\uc2ac\ub86f",
        'Package_name' : "com.hnhsoft.dnslot",
        'installs' : "10,000+",
        'version' : "1.3.1.1",
    },
    'jp.co.plusr.android.jintsu' :
    {
        'category' : "Health and Fitness",
        'updated' : "27 May 2022",
        'App_name' : "\u9663\u75db\u304d\u305f\u304b\u3082 - \u51fa\u7523\u5f53\u65e5\u3067\u3082\u7126\u3089\u305a\u4f7f\u3048\u308b\u9663\u75db\u30bf\u30a4\u30de\u30fc\u30a2\u30d7\u30ea",
        'Package_name' : "jp.co.plusr.android.jintsu",
        'installs' : "500,000+",
        'version' : "4.10.0",
    },
    '1484524707' :
    {
        'category' : "Games",
        'updated' : "2022-03-04T09:14:23Z",
        'App_name' : "Flipper Dunk",
        'Package_name' : "com.copilotgames.FlipperDunk",
        'version' : "1.41",
        'minimumOsVersion' : "11.0",
    },
    'com.linecorp.LGTM2' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "Tsum Tsum Stadium",
        'Package_name' : "com.linecorp.LGTM2",
        'installs' : "1,000,000+",
        'version' : "1.19.1",
    },
    'com.nexon.fmk' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "\ud53c\ud30c\ubaa8\ubc14\uc77c",
        'Package_name' : "com.nexon.fmk",
        'installs' : "1,000,000+",
        'version' : "9.1.02",
    },
    'com.picnic.android' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Picnic Online Supermarket",
        'Package_name' : "com.picnic.android",
        'installs' : "1,000,000+",
        'version' : "1.15.141",
    },
    'com.uncledelivery.user' :
    {
        'category' : "Utilities",
        'updated' : "27 May 2022",
        'App_name' : "Uncle Delivery - Fast Delivery",
        'Package_name' : "com.uncledelivery.user",
        'installs' : "100,000+",
        'version' : "315.9.0",
    },
    '1044503337' :
    {
        'category' : "Games",
        'updated' : "2022-05-25T05:04:57Z",
        'App_name' : "LINE: \u92fc\u5f48\u5927\u4e82\u9b25",
        'Package_name' : "com.linecorp.LGSDG",
        'version' : "8.5.0",
        'minimumOsVersion' : "10.0",
    },
    'com.netmarble.survivalgb' :
    {
        'category' : "Games",
        'updated' : "25 Apr 2022",
        'App_name' : "A3: Still Alive",
        'Package_name' : "com.netmarble.survivalgb",
        'installs' : "1,000,000+",
        'version' : "1.6.4",
    },
    'com.kobobooks.android.fnac' :
    {
        'category' : "Education and Books",
        'updated' : "01 Jun 2022",
        'App_name' : "Kobo by Fnac - eBooks et Livres audio",
        'Package_name' : "com.kobobooks.android.fnac",
        'installs' : "500,000+",
        'version' : "Varies with device",
    },
    'com.busuu.android.enc' :
    {
        'category' : "Education and Books",
        'updated' : "14 Jun 2022",
        'App_name' : "Busuu: Learn Languages",
        'Package_name' : "com.busuu.android.enc",
        'installs' : "10,000,000+",
        'version' : "23.5.0.748",
    },
    'com.talkspace.talkspaceapp' :
    {
        'category' : "Health and Fitness",
        'updated' : "25 May 2022",
        'App_name' : "Talkspace Counseling & Therapy",
        'Package_name' : "com.talkspace.talkspaceapp",
        'installs' : "500,000+",
        'version' : "3.32.7",
    },
    'com.innogames.foeandroid' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Forge of Empires: Build a City",
        'Package_name' : "com.innogames.foeandroid",
        'installs' : "10,000,000+",
        'version' : "1.232.16",
    },
    'pl.interia.okazjum' :
    {
        'category' : "Shopping",
        'updated' : "17 May 2022",
        'App_name' : "Okazjum",
        'Package_name' : "pl.interia.okazjum",
        'installs' : "100,000+",
        'version' : "1.10.27",
    },
    'com.mathpresso.qanda' :
    {
        'category' : "Education and Books",
        'updated' : "16 Jun 2022",
        'App_name' : "QANDA: Instant Math Helper",
        'Package_name' : "com.mathpresso.qanda",
        'installs' : "50,000,000+",
        'version' : "5.1.31",
    },
    'com.bukuwarung' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "BukuWarung Apps for MSMEs",
        'Package_name' : "com.bukuwarung",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.arkeapps.fantasylife' :
    {
        'category' : "Games",
        'updated' : "04 Jun 2022",
        'App_name' : "Fantasy Life: Alerts + Advice",
        'Package_name' : "com.arkeapps.fantasylife",
        'installs' : "100,000+",
        'version' : "3.6.2",
    },
    '664973122' :
    {
        'category' : "Education and Books",
        'updated' : "2022-05-17T00:17:49Z",
        'App_name' : "Lezhin Comics-Premium Webtoons",
        'Package_name' : "com.lezhin.comics",
        'version' : "2022.5.0",
        'minimumOsVersion' : "12.0",
    },
    'de.deutschlandcard.app' :
    {
        'category' : "Lifestyle",
        'updated' : "27 May 2022",
        'App_name' : "DeutschlandCard",
        'Package_name' : "de.deutschlandcard.app",
        'installs' : "1,000,000+",
        'version' : "2.91.0",
    },
    'com.lab465.SmoreApp' :
    {
        'category' : "Lifestyle",
        'updated' : "10 Jun 2022",
        'App_name' : "Make Money & Earn Gift Cards",
        'Package_name' : "com.lab465.SmoreApp",
        'installs' : "1,000,000+",
        'version' : "2.0.14",
    },
    'com.upgrad.student' :
    {
        'category' : "Education and Books",
        'updated' : "15 Jun 2022",
        'App_name' : "upGrad - Online Learning Courses",
        'Package_name' : "com.upgrad.student",
        'installs' : "1,000,000+",
        'version' : "5.4.6",
    },
    'com.canopy.tubenight' :
    {
        'category' : "Entertainment",
        'updated' : "31 Jan 2022",
        'App_name' : "Tube Night - Romantic Movies and Uncut Scenes",
        'Package_name' : "com.canopy.tubenight",
        'installs' : "100,000+",
        'version' : "1.72",
    },
    'com.voltage.sus.ropng' :
    {
        'category' : "Games",
        'updated' : "06 Jun 2022",
        'App_name' : "\u516d\u672c\u6728\u30b5\u30c7\u30a3\u30b9\u30c6\u30a3\u30c3\u30af\u30ca\u30a4\u30c8",
        'Package_name' : "com.voltage.sus.ropng",
        'installs' : "500,000+",
        'version' : "7.6.0",
    },
    'dk.dsg.bifrostCarlsJrAppP' :
    {
        'category' : "Food & Drink",
        'updated' : "30 May 2022",
        'App_name' : "Carl's Jr. DK",
        'Package_name' : "dk.dsg.bifrostCarlsJrAppP",
        'installs' : "50,000+",
        'version' : "1.80",
    },
    'com.linekong.wxggkr' :
    {
        'category' : "Games",
        'updated' : "04 Sep 2020",
        'App_name' : "\uac80\uc740\ub2ec:\uafc8\uc744 \uc774\ub8e8\ub294 \uc790",
        'Package_name' : "com.linekong.wxggkr",
        'installs' : "100,000+",
        'version' : "36.0",
    },
    'com.pronetis.ironball2' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Going Balls",
        'Package_name' : "com.pronetis.ironball2",
        'installs' : "100,000,000+",
        'version' : "1.30",
    },
    'com.wetter.androidclient' :
    {
        'category' : "Utilities",
        'updated' : "17 May 2022",
        'App_name' : "wetter.com - Weather and Radar",
        'Package_name' : "com.wetter.androidclient",
        'installs' : "10,000,000+",
        'version' : "2.50.3",
    },
    'com.behrouzbiryani' :
    {
        'category' : "Food & Drink",
        'updated' : "08 Jun 2022",
        'App_name' : "Behrouz Biryani - Order Online",
        'Package_name' : "com.behrouzbiryani",
        'installs' : "1,000,000+",
        'version' : "2.28",
    },
    'app.snoop' :
    {
        'category' : "Finance",
        'updated' : "25 May 2022",
        'App_name' : "Smart Spending l Snoop Finance",
        'Package_name' : "app.snoop",
        'installs' : "100,000+",
        'version' : "5.2.1",
    },
    'com.gravity.rot.aos' :
    {
        'category' : "Games",
        'updated' : "31 Dec 2021",
        'App_name' : "\ub77c\uadf8\ub098\ub85c\ud06c \ud0dd\ud2f1\uc2a4",
        'Package_name' : "com.gravity.rot.aos",
        'installs' : "100,000+",
        'version' : "5.6.1",
    },
    'de.telekom.mail' :
    {
        'category' : "Social and Communication",
        'updated' : "03 Mar 2022",
        'App_name' : "Telekom Mail - E-Mail-Programm",
        'Package_name' : "de.telekom.mail",
        'installs' : "5,000,000+",
        'version' : "2.2.11",
    },
    'com.wemade.mir4' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "\ubbf8\ub9744",
        'Package_name' : "com.wemade.mir4",
        'installs' : "500,000+",
        'version' : "0.320703",
    },
    'com.gshopper.gapp' :
    {
        'category' : "Shopping",
        'updated' : "01 Jun 2022",
        'App_name' : "Gshopper",
        'Package_name' : "com.gshopper.gapp",
        'installs' : "10,000+",
        'version' : "3.9.8",
    },
    '553663691' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-16T02:41:18Z",
        'App_name' : "99: Viaje com Economia",
        'Package_name' : "com.99taxis",
        'version' : "6.24.2",
        'minimumOsVersion' : "11.0",
    },
    'com.richshapero.tfma' :
    {
        'category' : "Education and Books",
        'updated' : "21 May 2022",
        'App_name' : "TooFar Media",
        'Package_name' : "com.richshapero.tfma",
        'installs' : "1,000,000+",
        'version' : "1.12.2 (#260-7485-ccaba96a5)",
    },
    '1107815836' :
    {
        'category' : "Games",
        'updated' : "2022-04-20T03:04:02Z",
        'App_name' : "GetLive!\u3000\u30af\u30ec\u30fc\u30f3\u30b2\u30fc\u30e0\u300e\u30b2\u30c3\u30c8\u30e9\u30a4\u30d6\u300f\u30fb\u30aa\u30f3\u30af\u30ec",
        'Package_name' : "jp.co.peanutsclub.GetLive",
        'version' : "4.2.1",
        'minimumOsVersion' : "12.0",
    },
    'com.linegames.ie' :
    {
        'category' : "Games",
        'updated' : "08 Apr 2022",
        'App_name' : "\uc774\uce74\ub8e8\uc2a4 \uc774\ud130\ub110",
        'Package_name' : "com.linegames.ie",
        'installs' : "100,000+",
        'version' : "1.0.34",
    },
    'com.netway.phone.advice' :
    {
        'category' : "Lifestyle",
        'updated' : "13 Jun 2022",
        'App_name' : "Astroyogi: Online Astrology",
        'Package_name' : "com.netway.phone.advice",
        'installs' : "1,000,000+",
        'version' : "12.7",
    },
    'com.unacademyapp' :
    {
        'category' : "Education and Books",
        'updated' : "16 Jun 2022",
        'App_name' : "Unacademy Learner App",
        'Package_name' : "com.unacademyapp",
        'installs' : "50,000,000+",
        'version' : "Varies with device",
    },
    'com.exa.nanamarket' :
    {
        'category' : "Food & Drink",
        'updated' : "08 Jun 2022",
        'App_name' : "NANA",
        'Package_name' : "com.exa.nanamarket",
        'installs' : "1,000,000+",
        'version' : "12.41.0",
    },
    'com.ada.astrapay' :
    {
        'category' : "Finance",
        'updated' : "26 May 2022",
        'App_name' : "AstraPay",
        'Package_name' : "com.ada.astrapay",
        'installs' : "5,000,000+",
        'version' : "2.20.0",
    },
    'com.mangot5.gh' :
    {
        'category' : "Games",
        'updated' : "28 Apr 2020",
        'App_name' : "\u9ad8\u6821\u4e4b\u795e",
        'Package_name' : "com.mangot5.gh",
        'installs' : "5,000+",
        'version' : "2.2.352",
    },
    'com.prodege.swagbucksmobile' :
    {
        'category' : "Lifestyle",
        'updated' : "26 May 2022",
        'App_name' : "Swagbucks",
        'Package_name' : "com.prodege.swagbucksmobile",
        'installs' : "5,000,000+",
        'version' : "5.12",
    },
    'jp.co.greenhouse.asken' :
    {
        'category' : "Health and Fitness",
        'updated' : "06 Jun 2022",
        'App_name' : "\u30c0\u30a4\u30a8\u30c3\u30c8\u30a2\u30d7\u30ea \u3042\u3059\u3051\u3093 \u30ab\u30ed\u30ea\u30fc\u8a08\u7b97\u30fb\u98df\u4e8b\u8a18\u9332\u30fb\u4f53\u91cd\u7ba1\u7406",
        'Package_name' : "jp.co.greenhouse.asken",
        'installs' : "1,000,000+",
        'version' : "5.0",
    },
    'com.ketchapp.samuraislash' :
    {
        'category' : "Games",
        'updated' : "11 Feb 2021",
        'App_name' : "Samurai Slash - Run & Slice",
        'Package_name' : "com.ketchapp.samuraislash",
        'installs' : "1,000,000+",
        'version' : "1.1.0",
    },
    'com.onemainstream.smithsonia.android' :
    {
        'category' : "Entertainment",
        'updated' : "12 May 2022",
        'App_name' : "Smithsonian Channel",
        'Package_name' : "com.onemainstream.smithsonia.android",
        'installs' : "100,000+",
        'version' : "107.104.0",
    },
    'jp.naver.lineplay.android' :
    {
        'category' : "Social and Communication",
        'updated' : "31 May 2022",
        'App_name' : "LINE PLAY - Our Avatar World",
        'Package_name' : "jp.naver.lineplay.android",
        'installs' : "10,000,000+",
        'version' : "8.8.1.0",
    },
    '808687272' :
    {
        'category' : "Games",
        'updated' : "2022-04-08T06:01:43Z",
        'App_name' : "\u30b9\u30af\u30fc\u30eb\u30ac\u30fc\u30eb\u30b9\u30c8\u30e9\u30a4\u30ab\u30fc\u30ba2",
        'Package_name' : "com.square-enix.schoolgirlstrikersjpn",
        'version' : "1.11.0",
        'minimumOsVersion' : "7.0",
    },
    'com.global.sgxd' :
    {
        'category' : "Games",
        'updated' : "01 Jun 2020",
        'App_name' : "Symphogear XD UNLIMITED",
        'Package_name' : "com.global.sgxd",
        'installs' : "100,000+",
        'version' : "1.2.0",
    },
    'com.gamemorefun.sgztw' :
    {
        'category' : "Games",
        'updated' : "21 Apr 2022",
        'App_name' : "\u65b0\u4e09\u570b \u6f22\u5ba4\u5fa9\u8208",
        'Package_name' : "com.gamemorefun.sgztw",
        'installs' : "500,000+",
        'version' : "2.5.2",
    },
    '1495014289' :
    {
        'category' : "Social and Communication",
        'updated' : "2021-06-15T08:30:08Z",
        'App_name' : "nicee \u2013 \u5b85\u5bb6\u793e\u4ea4\u5fc5\u5099\u795e\u5668",
        'Package_name' : "com.funplus.teamup",
        'version' : "1.49.2",
        'minimumOsVersion' : "10.0",
    },
    '1516629235' :
    {
        'category' : "Games",
        'updated' : "2022-04-16T13:14:08Z",
        'App_name' : "Aggretsuko :Sanrio Puzzle Game",
        'Package_name' : "com.hive.aggretsuko",
        'version' : "1.19.6",
        'minimumOsVersion' : "10.0",
    },
    'th.co.crie.tron2.android' :
    {
        'category' : "Lifestyle",
        'updated' : "09 Jun 2022",
        'App_name' : "dtac",
        'Package_name' : "th.co.crie.tron2.android",
        'installs' : "10,000,000+",
        'version' : "10.3.0",
    },
    'pl.adhdinteractive.app' :
    {
        'category' : "Lifestyle",
        'updated' : "16 May 2022",
        'App_name' : "Twoje ACUVUE\u00ae",
        'Package_name' : "pl.adhdinteractive.app",
        'installs' : "100,000+",
        'version' : "300.7.0",
    },
    'com.bilibilijp.fairysphere' :
    {
        'category' : "Games",
        'updated' : "20 Nov 2021",
        'App_name' : "\u30d5\u30a7\u30a2\u30ea\u30fc\u30b9\u30d5\u30a3\u30a2",
        'Package_name' : "com.bilibilijp.fairysphere",
        'installs' : "50,000+",
        'version' : "1.16.3",
    },
    'com.biligamekr.figuregp' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "\ubbf8\ub2c8\uc5b4\uc2a4",
        'Package_name' : "com.biligamekr.figuregp",
        'installs' : "500,000+",
        'version' : "3.87.1110",
    },
    'com.com2us.mlbgm.normal.freefull.google.global.android.common' :
    {
        'category' : "Games",
        'updated' : "18 May 2022",
        'App_name' : "MLB 9 Innings GM",
        'Package_name' : "com.com2us.mlbgm.normal.freefull.google.global.android.common",
        'installs' : "1,000,000+",
        'version' : "6.2.0",
    },
    'com.lazada.android' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Lazada \u2013 Online Shopping App!",
        'Package_name' : "com.lazada.android",
        'installs' : "100,000,000+",
        'version' : "7.1.1",
    },
    'com.global.angels' :
    {
        'category' : "Games",
        'updated' : "20 Apr 2022",
        'App_name' : "\u30a2\u30a4\u30c9\u30eb\u30a8\u30f3\u30b8\u30a7\u30eb\u30b9\uff1aAegis of Fate",
        'Package_name' : "com.global.angels",
        'installs' : "50,000+",
        'version' : "3.23.1.041501",
    },
    '1444055257' :
    {
        'category' : "Finance",
        'updated' : "2022-06-10T04:28:27Z",
        'App_name' : "Shiprocket: Courier Delivery",
        'Package_name' : "app.shiprocket",
        'version' : "2.2.2",
        'minimumOsVersion' : "12.0",
    },
    'vn.tpbank.savy' :
    {
        'category' : "Finance",
        'updated' : "17 Apr 2022",
        'App_name' : "TPBank Savy",
        'Package_name' : "vn.tpbank.savy",
        'installs' : "500,000+",
        'version' : "2.7.0",
    },
    'com.mobile.legends' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Mobile Legends: Bang Bang",
        'Package_name' : "com.mobile.legends",
        'installs' : "500,000,000+",
        'version' : "Varies with device",
    },
    'com.theepochtimes.news' :
    {
        'category' : "Education and Books",
        'updated' : "13 May 2022",
        'App_name' : "The Epoch Times: Breaking News",
        'Package_name' : "com.theepochtimes.news",
        'installs' : "1,000,000+",
        'version' : "2.29.21",
    },
    '416973595' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-08T12:16:13Z",
        'App_name' : "Meetic - Rencontre et Amour",
        'Package_name' : "com.meetic.iphone",
        'version' : "8.75.0",
        'minimumOsVersion' : "13.0",
    },
    'com.bandainamcoent.shinycolors' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "\u30a2\u30a4\u30c9\u30eb\u30de\u30b9\u30bf\u30fc \u30b7\u30e3\u30a4\u30cb\u30fc\u30ab\u30e9\u30fc\u30ba",
        'Package_name' : "com.bandainamcoent.shinycolors",
        'installs' : "100,000+",
        'version' : "1.0.58",
    },
    'com.cygames.Shadowverse' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Shadowverse CCG",
        'Package_name' : "com.cygames.Shadowverse",
        'installs' : "1,000,000+",
        'version' : "3.7.30",
    },
    'jp.gungho.yww' :
    {
        'category' : "Games",
        'updated' : "22 Feb 2022",
        'App_name' : "\u5996\u602a\u30a6\u30a9\u30c3\u30c1 \u30ef\u30fc\u30eb\u30c9",
        'Package_name' : "jp.gungho.yww",
        'installs' : "1,000,000+",
        'version' : "3.4.0",
    },
    '1485826005' :
    {
        'category' : "Games",
        'updated' : "2021-07-14T21:41:44Z",
        'App_name' : "Stack Up !!!",
        'Package_name' : "jp.bap.game.stackup",
        'version' : "3.6.1",
        'minimumOsVersion' : "10.0",
    },
    'com.samsungcard.baby' :
    {
        'category' : "Health and Fitness",
        'updated' : "11 Mar 2022",
        'App_name' : "\ubca0\uc774\ube44\uc2a4\ud1a0\ub9ac - \uc5c4\ub9c8, \uc544\ube60\uc758 \ud544\uc218 \ucd9c\uc0b0\uc721\uc544\uc571",
        'Package_name' : "com.samsungcard.baby",
        'installs' : "500,000+",
        'version' : "2.6.2",
    },
    'com.tencent.tmgp.kr.codm' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "\ucf5c \uc624\ube0c \ub4c0\ud2f0: \ubaa8\ubc14\uc77c",
        'Package_name' : "com.tencent.tmgp.kr.codm",
        'installs' : "1,000,000+",
        'version' : "1.7.33",
    },
    'com.unicostudio.braintest' :
    {
        'category' : "Games",
        'updated' : "04 Jun 2022",
        'App_name' : "Brain Test: Tricky Puzzles",
        'Package_name' : "com.unicostudio.braintest",
        'installs' : "100,000,000+",
        'version' : "2.730.1",
    },
    'com.watcho' :
    {
        'category' : "Entertainment",
        'updated' : "19 May 2022",
        'App_name' : "Watcho - Shows Live TV & More",
        'Package_name' : "com.watcho",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.contentsfirst.tappytoon' :
    {
        'category' : "Education and Books",
        'updated' : "07 Jun 2022",
        'App_name' : "Tappytoon Comics & Novels",
        'Package_name' : "com.contentsfirst.tappytoon",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.kfc.me' :
    {
        'category' : "Food & Drink",
        'updated' : "13 May 2022",
        'App_name' : "KFC UAE (United Arab Emirates)",
        'Package_name' : "com.kfc.me",
        'installs' : "1,000,000+",
        'version' : "6.4.1",
    },
    'com.yandex.mobile.realty' :
    {
        'category' : "House & Home",
        'updated' : "10 Jun 2022",
        'App_name' : "Yandex.Realty",
        'Package_name' : "com.yandex.mobile.realty",
        'installs' : "5,000,000+",
        'version' : "5.25.0",
    },
    'com.app.danube' :
    {
        'category' : "Shopping",
        'updated' : "11 Apr 2022",
        'App_name' : "Danube",
        'Package_name' : "com.app.danube",
        'installs' : "1,000,000+",
        'version' : "8.4.5",
    },
    'com.mobillium.btcturk' :
    {
        'category' : "Finance",
        'updated' : "01 Jun 2022",
        'App_name' : "BtcTurk | Bitcoin Buy Sell",
        'Package_name' : "com.mobillium.btcturk",
        'installs' : "500,000+",
        'version' : "1.17.1",
    },
    'com.tnkfactory.villainmaster.google' :
    {
        'category' : "Games",
        'updated' : "01 Apr 2022",
        'App_name' : "\ube4c\ub7f0\ub9c8\uc2a4\ud130 : DEAD OR ALIVE",
        'Package_name' : "com.tnkfactory.villainmaster.google",
        'installs' : "100,000+",
        'version' : "1.7.8",
    },
    'net.snowpipe.tongkey_mobile' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "\ud53c\uad6c\uc655\ud1b5\ud0a4M",
        'Package_name' : "net.snowpipe.tongkey_mobile",
        'installs' : "100,000+",
        'version' : "1.9.49",
    },
    'com.newsdistill.mobile' :
    {
        'category' : "Social and Communication",
        'updated' : "31 May 2022",
        'App_name' : "PublicVibe: Local Area Videos",
        'Package_name' : "com.newsdistill.mobile",
        'installs' : "10,000,000+",
        'version' : "3.0.47",
    },
    'com.yoozoo.kr.ik' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "\uc778\ud53c\ub2c8\ud2f0 \ud0b9\ub364",
        'Package_name' : "com.yoozoo.kr.ik",
        'installs' : "100,000+",
        'version' : "2.1.1",
    },
    'com.meesho.supply' :
    {
        'category' : "Shopping",
        'updated' : "09 Jun 2022",
        'App_name' : "Meesho: Online Shopping App",
        'Package_name' : "com.meesho.supply",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'oky.smartla.net' :
    {
        'category' : "Lifestyle",
        'updated' : "21 May 2022",
        'App_name' : "OKY Vouchers to Latin America",
        'Package_name' : "oky.smartla.net",
        'installs' : "50,000+",
        'version' : "V.8.0.1.5",
    },
    'com.avail.easyloans.android' :
    {
        'category' : "Finance",
        'updated' : "07 Jun 2022",
        'App_name' : "Avail Finance: Credit Loan App",
        'Package_name' : "com.avail.easyloans.android",
        'installs' : "5,000,000+",
        'version' : "22.06.02.6",
    },
    'tw.com.cayenneark.ns' :
    {
        'category' : "Games",
        'updated' : "24 May 2022",
        'App_name' : "\u51cd\u4eacNECRO \u81ea\u6bba\u4efb\u52d9",
        'Package_name' : "tw.com.cayenneark.ns",
        'installs' : "10,000+",
        'version' : "2.0.109",
    },
    '1093108529' :
    {
        'category' : "Entertainment",
        'updated' : "2022-05-17T12:56:47Z",
        'App_name' : "Coloring Book for Me",
        'Package_name' : "com.apalonapps.clrbook",
        'version' : "4.47.1",
        'minimumOsVersion' : "13.0",
    },
    'net.bitburst.pollpay' :
    {
        'category' : "Lifestyle",
        'updated' : "13 Jun 2022",
        'App_name' : "Poll Pay: Surveys for Money",
        'Package_name' : "net.bitburst.pollpay",
        'installs' : "5,000,000+",
        'version' : "6.0.22",
    },
    'xyz.be.customer' :
    {
        'category' : "Utilities",
        'updated' : "08 Jun 2022",
        'App_name' : "be - Vietnamese ride-hailing app",
        'Package_name' : "xyz.be.customer",
        'installs' : "1,000,000+",
        'version' : "2.6.15",
    },
    'com.iplus.tops' :
    {
        'category' : "Shopping",
        'updated' : "09 Jun 2022",
        'App_name' : "Tops Online - Food & Grocery",
        'Package_name' : "com.iplus.tops",
        'installs' : "1,000,000+",
        'version' : "3.29.0",
    },
    'com.neowiz.games.smg' :
    {
        'category' : "Games",
        'updated' : "24 Jan 2022",
        'App_name' : "GoStop : Card-playing game",
        'Package_name' : "com.neowiz.games.smg",
        'installs' : "50,000+",
        'version' : "2.04.9",
    },
    '1054790721' :
    {
        'category' : "Finance",
        'updated' : "2022-06-10T15:51:50Z",
        'App_name' : "CLARK - Versicherungsmanager",
        'Package_name' : "de.clark.ios.iphone.clark",
        'version' : "4.0.0",
        'minimumOsVersion' : "11.0",
    },
    'com.candy.kaboom.gp' :
    {
        'category' : "Games",
        'updated' : "19 May 2022",
        'App_name' : "candy kaboom",
        'Package_name' : "com.candy.kaboom.gp",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.mrd.food' :
    {
        'category' : "Food & Drink",
        'updated' : "09 May 2022",
        'App_name' : "Mr D Food-delivery & takeaway",
        'Package_name' : "com.mrd.food",
        'installs' : "5,000,000+",
        'version' : "5.0.6",
    },
    'com.NHNEnt.NDuelgo' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "\ud55c\uac8c\uc784 \uc2e0\ub9de\uace0 : \ub300\ud55c\ubbfc\uad6d \uc6d0\uc870 \uace0\uc2a4\ud1b1",
        'Package_name' : "com.NHNEnt.NDuelgo",
        'installs' : "1,000,000+",
        'version' : "1.8.26",
    },
    'com.gamehunt.ddt' :
    {
        'category' : "Games",
        'updated' : "07 Dec 2021",
        'App_name' : "\u5f39\u5f39\u4e16\u754c",
        'Package_name' : "com.gamehunt.ddt",
        'installs' : "50,000+",
        'version' : "4.3.1.0",
    },
    'ru.simple.wine' :
    {
        'category' : "Food & Drink",
        'updated' : "27 May 2022",
        'App_name' : "SimpleWine \u2014 \u043d\u0435 \u043f\u0440\u043e\u0441\u0442\u043e \u0432\u0438\u043d\u043e",
        'Package_name' : "ru.simple.wine",
        'installs' : "100,000+",
        'version' : "1.1.167",
    },
    'com.starmakerinteractive.starmaker' :
    {
        'category' : "Entertainment",
        'updated' : "09 Jun 2022",
        'App_name' : "StarMaker: Sing Karaoke Songs",
        'Package_name' : "com.starmakerinteractive.starmaker",
        'installs' : "100,000,000+",
        'version' : "8.11.1",
    },
    'com.global.ztmslg' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "Puzzles & Survival",
        'Package_name' : "com.global.ztmslg",
        'installs' : "10,000,000+",
        'version' : "7.0.72",
    },
    'pl.carrefour.carrefourmobile' :
    {
        'category' : "Shopping",
        'updated' : "16 May 2022",
        'App_name' : "M\u00f3j Carrefour",
        'Package_name' : "pl.carrefour.carrefourmobile",
        'installs' : "1,000,000+",
        'version' : "3.7.188",
    },
    'com.im.candychallenge3d' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Candy Challenge 3D",
        'Package_name' : "com.im.candychallenge3d",
        'installs' : "10,000,000+",
        'version' : "1.1.41",
    },
    'com.metrobikes.app' :
    {
        'category' : "Utilities",
        'updated' : "12 Jun 2022",
        'App_name' : "Bounce Electric Scooter Rental",
        'Package_name' : "com.metrobikes.app",
        'installs' : "5,000,000+",
        'version' : "6.1.9",
    },
    '1510001708' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-02T06:34:01Z",
        'App_name' : "Barakat Fresh Grocery Delivery",
        'Package_name' : "com.barakat.fresh",
        'version' : "1.9.0",
        'minimumOsVersion' : "11.0",
    },
    'de.motap.idleking' :
    {
        'category' : "Games",
        'updated' : "24 Feb 2022",
        'App_name' : "Idle King Clicker Tycoon Games",
        'Package_name' : "de.motap.idleking",
        'installs' : "100,000+",
        'version' : "2.0.9",
    },
    'co.go.fynd' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Fynd Online Shopping App",
        'Package_name' : "co.go.fynd",
        'installs' : "10,000,000+",
        'version' : "3.0.27",
    },
    'com.myairtelapp' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "Airtel Thanks \u2013 Recharge & UPI",
        'Package_name' : "com.myairtelapp",
        'installs' : "100,000,000+",
        'version' : "4.47.7",
    },
    '904413305' :
    {
        'category' : "Food & Drink",
        'updated' : "2021-05-28T12:47:27Z",
        'App_name' : "Little Caesars T\u00fcrkiye",
        'Package_name' : "com.eight.littlecaesars",
        'version' : "5.0.5",
        'minimumOsVersion' : "10.0",
    },
    'ro.superbet.sport' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Superbet - Pariuri Sportive",
        'Package_name' : "ro.superbet.sport",
        'installs' : "500,000+",
        'version' : "3.30.0",
    },
    'com.adidas.app' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "adidas",
        'Package_name' : "com.adidas.app",
        'installs' : "10,000,000+",
        'version' : "5.7.2",
    },
    'com.gaea.sdg.shelter' :
    {
        'category' : "Games",
        'updated' : "15 Sep 2021",
        'App_name' : "Fallout Shelter Online",
        'Package_name' : "com.gaea.sdg.shelter",
        'installs' : "500,000+",
        'version' : "3.9.1",
    },
    'com.dmm.games.flower' :
    {
        'category' : "Games",
        'updated' : "01 Jun 2022",
        'App_name' : "\u30d5\u30e9\u30ef\u30fc\u30ca\u30a4\u30c8\u30ac\u30fc\u30eb -\u7f8e\u5c11\u5973\u30b2\u30fc\u30e0\u30a2\u30d7\u30ea",
        'Package_name' : "com.dmm.games.flower",
        'installs' : "100,000+",
        'version' : "1.5.5",
    },
    'com.globo.globotv' :
    {
        'category' : "Entertainment",
        'updated' : "11 Jun 2022",
        'App_name' : "Globoplay: S\u00e9ries brasileiras!",
        'Package_name' : "com.globo.globotv",
        'installs' : "50,000,000+",
        'version' : "Varies with device",
    },
    '1019437222' :
    {
        'category' : "Games",
        'updated' : "2022-05-26T02:52:17Z",
        'App_name' : "\u30a2\u30a4\u30c9\u30ea\u30c3\u30b7\u30e5\u30bb\u30d6\u30f3",
        'Package_name' : "jp.co.bandainamcoonline.idolish7",
        'version' : "6.10.1",
        'minimumOsVersion' : "11.0",
    },
    'com.planetart.fpfr' :
    {
        'category' : "Entertainment",
        'updated' : "16 Jun 2022",
        'App_name' : "FreePrints",
        'Package_name' : "com.planetart.fpfr",
        'installs' : "1,000,000+",
        'version' : "3.55.2",
    },
    'pl.eobuwie.eobuwieapp' :
    {
        'category' : "Shopping",
        'updated' : "07 Jun 2022",
        'App_name' : "efootwear.eu",
        'Package_name' : "pl.eobuwie.eobuwieapp",
        'installs' : "5,000,000+",
        'version' : "1.38.1",
    },
    '1469869761' :
    {
        'category' : "Games",
        'updated' : "2022-06-07T23:30:37Z",
        'App_name' : "\ub2ec\ube5b\uc870\uac01\uc0ac",
        'Package_name' : "com.kakaogames.moonlight",
        'version' : "1.0.469",
        'minimumOsVersion' : "11.0",
    },
    'com.gameskraft.rummyculturelite' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Rummy Game | Play Rummy Online",
        'Package_name' : "com.gameskraft.rummyculturelite",
        'installs' : "5,000,000+",
        'version' : "28.00",
    },
    'com.landmarkgroup.maxstores' :
    {
        'category' : "Shopping",
        'updated' : "03 Jun 2022",
        'App_name' : "Max Fashion - \u0645\u0627\u0643\u0633 \u0641\u0627\u0634\u0648\u0646",
        'Package_name' : "com.landmarkgroup.maxstores",
        'installs' : "5,000,000+",
        'version' : "7.45",
    },
    'com.gm99.ylcs' :
    {
        'category' : "Games",
        'updated' : "22 Apr 2021",
        'App_name' : "\u9b3c\u8a9e\u8ff7\u57ce",
        'Package_name' : "com.gm99.ylcs",
        'installs' : "500,000+",
        'version' : "0.18.23.114.0",
    },
    'in.workindia.hireindia' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "WorkIndia Recruiter App",
        'Package_name' : "in.workindia.hireindia",
        'installs' : "1,000,000+",
        'version' : "1.9.10",
    },
    'com.gamebility.onet' :
    {
        'category' : "Games",
        'updated' : "01 Jun 2022",
        'App_name' : "Onnect - Pair Matching Puzzle",
        'Package_name' : "com.gamebility.onet",
        'installs' : "10,000,000+",
        'version' : "24.0.0",
    },
    'game.xin.hd.g' :
    {
        'category' : "Casino",
        'updated' : "15 Jun 2022",
        'App_name' : "\u661f\u57ceOnline",
        'Package_name' : "game.xin.hd.g",
        'installs' : "1,000,000+",
        'version' : "4.33",
    },
    'com.yocore.app' :
    {
        'category' : "Social and Communication",
        'updated' : "21 May 2022",
        'App_name' : "YO MOBILE",
        'Package_name' : "com.yocore.app",
        'installs' : "1,000,000+",
        'version' : "2.4.7",
    },
    '1183268040' :
    {
        'category' : "Games",
        'updated' : "2022-06-13T01:00:55Z",
        'App_name' : "\u30b8\u30e3\u30f3\u30d7\u30c1 \u30d2\u30fc\u30ed\u30fc\u30ba\u3000\u30b8\u30e3\u30f3\u30d7\u306e\u30d1\u30ba\u30ebRPG",
        'Package_name' : "com.linecorp.LGYDS",
        'version' : "6.1.3",
        'minimumOsVersion' : "8.0",
    },
    'com.drorapp' :
    {
        'category' : "Lifestyle",
        'updated' : "20 Apr 2022",
        'App_name' : "Lythouse- Report & Win Rewards",
        'Package_name' : "com.drorapp",
        'installs' : "100,000+",
        'version' : "4.1.2",
    },
    'com.pwrd.pwmru' :
    {
        'category' : "Games",
        'updated' : "16 May 2022",
        'App_name' : "Perfect World Mobile: RU",
        'Package_name' : "com.pwrd.pwmru",
        'installs' : "1,000,000+",
        'version' : "1.400.1",
    },
    'skplanet.musicmate' :
    {
        'category' : "Entertainment",
        'updated' : "04 May 2022",
        'App_name' : "FLO \u2013 \ud50c\ub85c",
        'Package_name' : "skplanet.musicmate",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'kr.co.april7.buddy' :
    {
        'category' : "Social and Communication",
        'updated' : "23 Mar 2022",
        'App_name' : "DaTalk - Random Chat Room",
        'Package_name' : "kr.co.april7.buddy",
        'installs' : "500,000+",
        'version' : "1.16.9",
    },
    'de.penny.app' :
    {
        'category' : "Food & Drink",
        'updated' : "29 May 2022",
        'App_name' : "PENNY Angebote & Coupons",
        'Package_name' : "de.penny.app",
        'installs' : "1,000,000+",
        'version' : "1.19.3-42360",
    },
    'jp.co.koeitecmo.Nobu201X' :
    {
        'category' : "Games",
        'updated' : "19 May 2022",
        'App_name' : "\u4fe1\u9577\u306e\u91ce\u671b 20XX",
        'Package_name' : "jp.co.koeitecmo.Nobu201X",
        'installs' : "100,000+",
        'version' : "2.014.000",
    },
    'com.readynine.bigpot999' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "BIGPOT 999",
        'Package_name' : "com.readynine.bigpot999",
        'installs' : "100,000+",
        'version' : "1.1.38",
    },
    'com.lilithgame.hgame.gp' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "AFK Arena",
        'Package_name' : "com.lilithgame.hgame.gp",
        'installs' : "10,000,000+",
        'version' : "1.91.01",
    },
    'com.sumaluna' :
    {
        'category' : "Health and Fitness",
        'updated' : "08 Jun 2022",
        'App_name' : "\u30b9\u30de\u30eb\u30ca-\u81ea\u5b85\u304b\u3089\u30b9\u30de\u30db\u3067\u3001\u30d4\u30eb\u306e\u76f8\u8ac7\u30fb\u8a3a\u5bdf\u30fb\u51e6\u65b9\u307e\u3067\u3002",
        'Package_name' : "com.sumaluna",
        'installs' : "100,000+",
        'version' : "2.19.0",
    },
    '1558452905' :
    {
        'category' : "Education and Books",
        'updated' : "2022-06-10T20:40:18Z",
        'App_name' : "Studybay",
        'Package_name' : "com.experthouse.us.customer",
        'version' : "2.4.0",
        'minimumOsVersion' : "11.0",
    },
    'com.ticno.olymptrade' :
    {
        'category' : "Finance",
        'updated' : "03 Jun 2022",
        'App_name' : "Olymp Trade - trading online",
        'Package_name' : "com.ticno.olymptrade",
        'installs' : "50,000,000+",
        'version' : "8.16.25679",
    },
    'jp.summervacation.cocorosekai' :
    {
        'category' : "Games",
        'updated' : "22 Feb 2021",
        'App_name' : "\u82f1\u8a9e\u3068\u30af\u30a4\u30ba\u306e\u30b3\u30b3\u30ed\u30bb\u30ab\u30a4",
        'Package_name' : "jp.summervacation.cocorosekai",
        'installs' : "100,000+",
        'version' : "1.7.5",
    },
    'com.ulugame.swordkingdoms.google' :
    {
        'category' : "Games",
        'updated' : "14 Dec 2021",
        'App_name' : "\ucc9c\ub839",
        'Package_name' : "com.ulugame.swordkingdoms.google",
        'installs' : "100,000+",
        'version' : "1.01.079",
    },
    'in.shopx.consumerapp' :
    {
        'category' : "Shopping",
        'updated' : "11 Feb 2022",
        'App_name' : "ShopX: Cashback & Local Deals",
        'Package_name' : "in.shopx.consumerapp",
        'installs' : "500,000+",
        'version' : "1.0.30",
    },
    'com.shutterfly' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "Shutterfly: Prints Cards Gifts",
        'Package_name' : "com.shutterfly",
        'installs' : "10,000,000+",
        'version' : "9.11.0",
    },
    'com.vui.taptapandroid' :
    {
        'category' : "Lifestyle",
        'updated' : "25 May 2022",
        'App_name' : "TAPTAP - T\u00edch \u0111i\u1ec3m, \u0111\u1ed5i \u01b0u \u0111\u00e3i",
        'Package_name' : "com.vui.taptapandroid",
        'installs' : "1,000,000+",
        'version' : "3.1.0",
    },
    'com.sunborn.girlsfrontline.jp' :
    {
        'category' : "Games",
        'updated' : "18 Feb 2022",
        'App_name' : "\u30c9\u30fc\u30eb\u30ba\u30d5\u30ed\u30f3\u30c8\u30e9\u30a4\u30f3",
        'Package_name' : "com.sunborn.girlsfrontline.jp",
        'installs' : "500,000+",
        'version' : "2.0800_220",
    },
    'com.medlife.customer' :
    {
        'category' : "Health and Fitness",
        'updated' : "06 May 2021",
        'App_name' : "Medlife Is Now PharmEasy",
        'Package_name' : "com.medlife.customer",
        'installs' : "10,000,000+",
        'version' : "5.0.61",
    },
    'com.gamania.lineagem' :
    {
        'category' : "Games",
        'updated' : "29 Apr 2022",
        'App_name' : "\u5929\u5802M",
        'Package_name' : "com.gamania.lineagem",
        'installs' : "1,000,000+",
        'version' : "1.6.27",
    },
    'com.atlasvpn.free.android.proxy.secure' :
    {
        'category' : "Tools",
        'updated' : "20 May 2022",
        'App_name' : "AtlasVPN: for speed & security",
        'Package_name' : "com.atlasvpn.free.android.proxy.secure",
        'installs' : "5,000,000+",
        'version' : "3.12.2",
    },
    'com.dogusms.zubizu' :
    {
        'category' : "Finance",
        'updated' : "08 Apr 2022",
        'App_name' : "ZUB\u0130ZU \u2013 Advantages in Brands",
        'Package_name' : "com.dogusms.zubizu",
        'installs' : "1,000,000+",
        'version' : "1.9.93",
    },
    'org.imperiaonline.android.v6' :
    {
        'category' : "Games",
        'updated' : "18 May 2022",
        'App_name' : "Imperia Online - Medieval empire war strategy MMO",
        'Package_name' : "org.imperiaonline.android.v6",
        'installs' : "1,000,000+",
        'version' : "8.0.35",
    },
    'com.ghg.komorilife' :
    {
        'category' : "Games",
        'updated' : "31 Dec 2020",
        'App_name' : "\u5c0f\u68ee\u751f\u6d3b",
        'Package_name' : "com.ghg.komorilife",
        'installs' : "100,000+",
        'version' : "1.9.1",
    },
    'com.coc.kr.cocmj.google' :
    {
        'category' : "Games",
        'updated' : "04 Nov 2020",
        'App_name' : "\uac80\uc740\ubbf8\uad81",
        'Package_name' : "com.coc.kr.cocmj.google",
        'installs' : "10,000+",
        'version' : "1.1.0",
    },
    'com.ak.ta.divya.bhaskar.activity' :
    {
        'category' : "Education and Books",
        'updated' : "17 Jun 2022",
        'App_name' : "Gujarati News by Divya Bhaskar",
        'Package_name' : "com.ak.ta.divya.bhaskar.activity",
        'installs' : "10,000,000+",
        'version' : "8.6.0",
    },
    'co.brainly' :
    {
        'category' : "Education and Books",
        'updated' : "15 Jun 2022",
        'App_name' : "Brainly \u2013 Get Homework Answers",
        'Package_name' : "co.brainly",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.guud.casamia' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "\uad73\ub2f7\ucef4 - Guud.com",
        'Package_name' : "com.guud.casamia",
        'installs' : "500,000+",
        'version' : "1.0.27",
    },
    'com.com2us.probaseball3d.normal.freefull.google.global.android.common' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "\ucef4\ud22c\uc2a4\ud504\ub85c\uc57c\uad6c2022",
        'Package_name' : "com.com2us.probaseball3d.normal.freefull.google.global.android.common",
        'installs' : "5,000,000+",
        'version' : "8.0.5",
    },
    'com.bigbluebubble.singingmonsters.full' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "My Singing Monsters",
        'Package_name' : "com.bigbluebubble.singingmonsters.full",
        'installs' : "10,000,000+",
        'version' : "3.5.0",
    },
    'com.haotianfu.jojomahjong' :
    {
        'category' : "Casino",
        'updated' : "09 Jun 2022",
        'App_name' : "\u91d1\u8c6a\u723a\u5a1b\u6a02\u57ce",
        'Package_name' : "com.haotianfu.jojomahjong",
        'installs' : "10,000+",
        'version' : "3.5.2",
    },
    'com.medrick.blast' :
    {
        'category' : "Games",
        'updated' : "23 Jan 2022",
        'App_name' : "\u062f\u0631\u0628\u0627\u0631\u0647 \u06af\u0644\u06cc",
        'Package_name' : "com.medrick.blast",
        'installs' : "100,000+",
        'version' : "1.37.0",
    },
    'com.monawa.moonmyung' :
    {
        'category' : "Games",
        'updated' : "22 Oct 2021",
        'App_name' : "\uc885\ub9d0\ub3c4\uc2dc: MMORPG",
        'Package_name' : "com.monawa.moonmyung",
        'installs' : "100,000+",
        'version' : "1.0.2",
    },
    'com.hipercard.app' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Cart\u00e3o de cr\u00e9dito Hipercard",
        'Package_name' : "com.hipercard.app",
        'installs' : "5,000,000+",
        'version' : "6.6.9",
    },
    '1462317543' :
    {
        'category' : "Games",
        'updated' : "2022-03-07T10:40:57Z",
        'App_name' : "Picker 3D",
        'Package_name' : "com.ponyom.collect",
        'version' : "6.5",
        'minimumOsVersion' : "11.0",
    },
    'com.goplayplay.game.gods.android' :
    {
        'category' : "Games",
        'updated' : "22 Jan 2020",
        'App_name' : "\u897f\u6e38\u5916\u4f20-\u5c01\u795e\u699c Legends of Gods RPG",
        'Package_name' : "com.goplayplay.game.gods.android",
        'installs' : "50,000+",
        'version' : "1.9.1",
    },
    '882198456' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-04-03T07:40:43Z",
        'App_name' : "Etstur Otel & Tatil F\u0131rsatlar\u0131",
        'Package_name' : "com.etstur.etstur",
        'version' : "v3.1.4",
        'minimumOsVersion' : "10.0",
    },
    'com.square_enix.android_googleplay.WOTVffbeww' :
    {
        'category' : "Games",
        'updated' : "06 Jun 2022",
        'App_name' : "FFBE WAR OF THE VISIONS",
        'Package_name' : "com.square_enix.android_googleplay.WOTVffbeww",
        'installs' : "1,000,000+",
        'version' : "5.3.0",
    },
    '1021268294' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-19T06:28:47Z",
        'App_name' : "Boutiqaat \u0628\u0648\u062a\u064a\u0643\u0627\u062a",
        'Package_name' : "com.leza.Boutiqaat",
        'version' : "6.8.2",
        'minimumOsVersion' : "10.0",
    },
    'kr.co.lgfashion.lgfashionshop.v28' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "LFmall - \ud504\ub9ac\ubbf8\uc5c4 \ub77c\uc774\ud504\uc2a4\ud0c0\uc77c\ubab0",
        'Package_name' : "kr.co.lgfashion.lgfashionshop.v28",
        'installs' : "5,000,000+",
        'version' : "4.1.48",
    },
    'net.singular.deviceassist' :
    {
        'category' : "Finance",
        'updated' : "27 Jul 2020",
        'App_name' : "Singular Device Assist",
        'Package_name' : "net.singular.deviceassist",
        'installs' : "5,000+",
        'version' : "1.6",
    },
    'com.wave.waveradio' :
    {
        'category' : "Social and Communication",
        'updated' : "10 Jun 2022",
        'App_name' : "Wave - Audio Live Streaming",
        'Package_name' : "com.wave.waveradio",
        'installs' : "500,000+",
        'version' : "5.19.1",
    },
    'com.b2w.submarino' :
    {
        'category' : "Shopping",
        'updated' : "03 May 2022",
        'App_name' : "Submarino: Compras Online",
        'Package_name' : "com.b2w.submarino",
        'installs' : "10,000,000+",
        'version' : "3.84.2",
    },
    'com.metrodeal.metrodealandroid' :
    {
        'category' : "Shopping",
        'updated' : "01 Jun 2022",
        'App_name' : "MetroDeal - Voucher | Coupon",
        'Package_name' : "com.metrodeal.metrodealandroid",
        'installs' : "1,000,000+",
        'version' : "4.9.6",
    },
    'com.p1.mobile.putong' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "Tantan",
        'Package_name' : "com.p1.mobile.putong",
        'installs' : "50,000,000+",
        'version' : "5.2.8.1",
    },
    'com.origingame.zlws.gp' :
    {
        'category' : "Games",
        'updated' : "18 Apr 2022",
        'App_name' : "\u65ac\u9f8d\u7121\u96d9",
        'Package_name' : "com.origingame.zlws.gp",
        'installs' : "50,000+",
        'version' : "1.1.96",
    },
    '1178701124' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-19T08:58:17Z",
        'App_name' : "Jeeny - \u062c\u064a\u0646\u064a",
        'Package_name' : "sa.com.easytaxi.Easy",
        'version' : "7.7.1",
        'minimumOsVersion' : "11.0",
    },
    'in.amazon.mShop.android.shopping' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Amazon India - Shop & Pay",
        'Package_name' : "in.amazon.mShop.android.shopping",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.intellihealth.truemeds' :
    {
        'category' : "Health and Fitness",
        'updated' : "16 Jun 2022",
        'App_name' : "Truemeds - Online Medicine App",
        'Package_name' : "com.intellihealth.truemeds",
        'installs' : "1,000,000+",
        'version' : "4.2.1",
    },
    '880047117' :
    {
        'category' : "Games",
        'updated' : "2022-05-31T09:58:52Z",
        'App_name' : "\uc575\uadf8\ub9ac\ubc84\ub4dc 2 (Angry Birds 2)",
        'Package_name' : "com.rovio.baba",
        'version' : "3.0.0",
        'minimumOsVersion' : "13.0",
    },
    'net.igapi.android.istegelsin' :
    {
        'category' : "Shopping",
        'updated' : "30 May 2022",
        'App_name' : "istegelsin",
        'Package_name' : "net.igapi.android.istegelsin",
        'installs' : "1,000,000+",
        'version' : "1370",
    },
    'com.activaweb.matchendirect' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "Match en Direct - Live Score",
        'Package_name' : "com.activaweb.matchendirect",
        'installs' : "10,000,000+",
        'version' : "6.3.3",
    },
    '405489166' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-07T05:03:59Z",
        'App_name' : "\u51fa\u524d\u9928-\u30d5\u30fc\u30c9\u30c7\u30ea\u30d0\u30ea\u30fc",
        'Package_name' : "com.demae-can.iphone",
        'version' : "16.0.0",
        'minimumOsVersion' : "11.0",
    },
    'com.gamesunion.motorcycle.googleplay' :
    {
        'category' : "Games",
        'updated' : "23 Mar 2022",
        'App_name' : "Racing Smash 3D",
        'Package_name' : "com.gamesunion.motorcycle.googleplay",
        'installs' : "10,000,000+",
        'version' : "1.0.47",
    },
    'com.kth.kshop' :
    {
        'category' : "Shopping",
        'updated' : "26 May 2022",
        'App_name' : "KT\uc54c\ud30c \uc1fc\ud551 - \uc77c\uc0c1\uc774 \uc54c\ud30c\uac00 \ub418\ub294 \uc1fc\ud551",
        'Package_name' : "com.kth.kshop",
        'installs' : "1,000,000+",
        'version' : "2.7.0",
    },
    'com.lp.nlw.tw.android' :
    {
        'category' : "Games",
        'updated' : "29 Jan 2021",
        'App_name' : "\u9f8d\u6b66MOBILE-\u8afe\u8a00",
        'Package_name' : "com.lp.nlw.tw.android",
        'installs' : "100,000+",
        'version' : "1.6.3467",
    },
    '1522609776' :
    {
        'category' : "Games",
        'updated' : "2022-06-10T04:11:46Z",
        'App_name' : "\u30ef\u30fc\u30eb\u30c9\u30a6\u30a3\u30c3\u30c1\u30fc\u30ba UNITED FRONT\uff08\u30e6\u30ca\u30d5\u30ed\uff09",
        'Package_name' : "com.worldwitchesunitedfront.forwardworks",
        'version' : "3.2.6",
        'minimumOsVersion' : "11.0",
    },
    'com.zipcarapp' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "05 Jun 2022",
        'App_name' : "Zipcar T\u00fcrkiye",
        'Package_name' : "com.zipcarapp",
        'installs' : "100,000+",
        'version' : "3.6",
    },
    'com.woowahan.vn.baemin' :
    {
        'category' : "Food & Drink",
        'updated' : "16 Jun 2022",
        'App_name' : "BAEMIN - Food delivery app",
        'Package_name' : "com.woowahan.vn.baemin",
        'installs' : "1,000,000+",
        'version' : "1.12.4",
    },
    'com.uu100.jszh.gp' :
    {
        'category' : "Games",
        'updated' : "29 Jul 2020",
        'App_name' : "Sword & Soul",
        'Package_name' : "com.uu100.jszh.gp",
        'installs' : "100,000+",
        'version' : "1.0.14.2",
    },
    'com.cjenm.ModooMarbleKakao' :
    {
        'category' : "Games",
        'updated' : "13 Apr 2022",
        'App_name' : "\ubaa8\ub450\uc758\ub9c8\ube14",
        'Package_name' : "com.cjenm.ModooMarbleKakao",
        'installs' : "10,000,000+",
        'version' : "9.1.00",
    },
    'com.muslim.android' :
    {
        'category' : "Lifestyle",
        'updated' : "29 Mar 2022",
        'App_name' : "umma: Quran Majeed Prayer time",
        'Package_name' : "com.muslim.android",
        'installs' : "10,000,000+",
        'version' : "3.0.5",
    },
    'kr.co.psynet' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "LIVE Score, Real-Time Score",
        'Package_name' : "kr.co.psynet",
        'installs' : "1,000,000+",
        'version' : "40.7.0",
    },
    'com.licious' :
    {
        'category' : "Food & Drink",
        'updated' : "27 May 2022",
        'App_name' : "Licious - Chicken, Fish & Meat",
        'Package_name' : "com.licious",
        'installs' : "5,000,000+",
        'version' : "4.0.0",
    },
    '1439768881' :
    {
        'category' : "Games",
        'updated' : "2022-05-30T01:12:39Z",
        'App_name' : "\ud0b9 \uc624\ube0c \ud30c\uc774\ud130 \uc62c\uc2a4\ud0c0",
        'Package_name' : "com.netmarble.kofkr",
        'version' : "1.11.3",
        'minimumOsVersion' : "10.0",
    },
    'jp.co.benesse.clacal' :
    {
        'category' : "Education and Books",
        'updated' : "14 Feb 2022",
        'App_name' : "ClaCal\uff08\u30af\u30e9\u30ab\u30eb\uff09-\u30af\u30e9\u30b9\u307f\u3093\u306a\u3067\u5b66\u7fd2\u7ba1\u7406",
        'Package_name' : "jp.co.benesse.clacal",
        'installs' : "10,000+",
        'version' : "1.3.6",
    },
    'jp.shieldhero.game' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "\u76fe\u306e\u52c7\u8005\u306e\u6210\u308a\u4e0a\u304c\u308a RERISE",
        'Package_name' : "jp.shieldhero.game",
        'installs' : "50,000+",
        'version' : "1.8.6",
    },
    'com.testbook.tbapp' :
    {
        'category' : "Education and Books",
        'updated' : "13 Jun 2022",
        'App_name' : "Testbook: Exam Preparation App",
        'Package_name' : "com.testbook.tbapp",
        'installs' : "10,000,000+",
        'version' : "6.10.7",
    },
    'com.contextlogic.wish' :
    {
        'category' : "Shopping",
        'updated' : "16 Jun 2022",
        'App_name' : "Wish - Shopping Made Fun",
        'Package_name' : "com.contextlogic.wish",
        'installs' : "500,000,000+",
        'version' : "22.10.0",
    },
    '680595680' :
    {
        'category' : "Entertainment",
        'updated' : "2022-06-08T15:28:53Z",
        'App_name' : "Telemundo: Series y TV en vivo",
        'Package_name' : "com.nbcuni.telemundo.tve",
        'version' : "7.35",
        'minimumOsVersion' : "13.0",
    },
    'com.starcube.Maruay99' :
    {
        'category' : "Casino",
        'updated' : "15 Jun 2022",
        'App_name' : "Fishing Maruay99 Slots Casino",
        'Package_name' : "com.starcube.Maruay99",
        'installs' : "1,000,000+",
        'version' : "1.0.59",
    },
    'com.fastrunkitchen' :
    {
        'category' : "Food & Drink",
        'updated' : "31 May 2022",
        'App_name' : "Local Kitchen",
        'Package_name' : "com.fastrunkitchen",
        'installs' : "500,000+",
        'version' : "7.2.0",
    },
    'com.igs.TMD' :
    {
        'category' : "Casino",
        'updated' : "30 May 2022",
        'App_name' : "ManganDahen Casino",
        'Package_name' : "com.igs.TMD",
        'installs' : "1,000,000+",
        'version' : "1.1.143",
    },
    'com.enflick.android.TextNow' :
    {
        'category' : "Social and Communication",
        'updated' : "11 Jun 2022",
        'App_name' : "TextNow: Call + Text Unlimited",
        'Package_name' : "com.enflick.android.TextNow",
        'installs' : "50,000,000+",
        'version' : "22.22.1.0",
    },
    '1434292068' :
    {
        'category' : "Games",
        'updated' : "2022-04-08T12:03:56Z",
        'App_name' : "Monster Masters EX",
        'Package_name' : "com.lpg.aom",
        'version' : "13.2.8762",
        'minimumOsVersion' : "11.0",
    },
    'com.global.dzgjp' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "\u5546\u4eba\u653e\u6d6a\u8a18-\u3042\u304d\u3093\u3069\u306e\u6210\u308a\u4e0a\u304c\u308a\u9053",
        'Package_name' : "com.global.dzgjp",
        'installs' : "100,000+",
        'version' : "4.7.2",
    },
    'com.wannaplay.circle.match.relax' :
    {
        'category' : "Games",
        'updated' : "05 May 2022",
        'App_name' : "Circle Relax: Daily Art Puzzle",
        'Package_name' : "com.wannaplay.circle.match.relax",
        'installs' : "1,000,000+",
        'version' : "1.7.4",
    },
    'com.cuelearn.cuemathapp' :
    {
        'category' : "Education and Books",
        'updated' : "16 Mar 2022",
        'App_name' : "Cuemath: Math Games & Classes",
        'Package_name' : "com.cuelearn.cuemathapp",
        'installs' : "1,000,000+",
        'version' : "2.9.0",
    },
    'com.gmclub.cupgame' :
    {
        'category' : "Games",
        'updated' : "17 Aug 2020",
        'App_name' : "Gaming Club",
        'Package_name' : "com.gmclub.cupgame",
        'installs' : "10,000+",
        'version' : "1.0",
    },
    'com.kilogroup.ketocycle' :
    {
        'category' : "Health and Fitness",
        'updated' : "02 Jun 2022",
        'App_name' : "Keto Cycle: Keto Diet Tracker",
        'Package_name' : "com.kilogroup.ketocycle",
        'installs' : "500,000+",
        'version' : "1.24.0",
    },
    'jp.dgsm' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "\u30c9\u30e9\u30b4\u30f3\u30b9\u30de\u30c3\u30b7\u30e5",
        'Package_name' : "jp.dgsm",
        'installs' : "100,000+",
        'version' : "4.19.2",
    },
    '373034841' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-14T12:04:26Z",
        'App_name' : "Yemeksepeti: Food & Grocery",
        'Package_name' : "yemeksepeti.com.valensas",
        'version' : "22.12.0",
        'minimumOsVersion' : "11.0",
    },
    'bwin.de.sports' :
    {
        'category' : "Games",
        'updated' : "25 Feb 2022",
        'App_name' : "bwin Sportwetten App",
        'Package_name' : "bwin.de.sports",
        'installs' : "100,000+",
        'version' : "22.02.23",
    },
    'com.online.real.indian.cards.palacerummy' :
    {
        'category' : "Games",
        'updated' : "03 Feb 2021",
        'App_name' : "Rummy Palace- Play Rummy Online | Indian Card Game",
        'Package_name' : "com.online.real.indian.cards.palacerummy",
        'installs' : "1,000,000+",
        'version' : "1.63",
    },
    'jp.co.ponos.battlecatskr' :
    {
        'category' : "Games",
        'updated' : "17 May 2022",
        'App_name' : "\ub0e5\ucf54 \ub300\uc804\uc7c1",
        'Package_name' : "jp.co.ponos.battlecatskr",
        'installs' : "5,000,000+",
        'version' : "11.5.0",
    },
    'ru.rivegauche.app' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "\u0420\u0418\u0412 \u0413\u041e\u0428 \u041f\u0430\u0440\u0444\u044e\u043c\u0435\u0440\u0438\u044f \u0438 \u041a\u043e\u0441\u043c\u0435\u0442\u0438\u043a\u0430",
        'Package_name' : "ru.rivegauche.app",
        'installs' : "1,000,000+",
        'version' : "2.16.2-gms",
    },
    'com.gmmstream.plern' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "Plern",
        'Package_name' : "com.gmmstream.plern",
        'installs' : "1,000,000+",
        'version' : "1.2.4",
    },
    '1521313666' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-03T23:33:02Z",
        'App_name' : "Yajny - \u0643\u0648\u0628\u0648\u0646\u0627\u062a \u0648\u0643\u0627\u0634 \u0628\u0627\u0643 \u064a\u062c\u0646\u064a",
        'Package_name' : "com.yajny.cashback",
        'version' : "2.1.40",
        'minimumOsVersion' : "11.0",
    },
    'com.metacube.sindorim.aos' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "\uc2e0\ub3c4\ub9bc: \ud53c\ub3c4\uc548\ub9c8\ub978\ub140\uc11d\ub4e4 \uc561\uc158RPG",
        'Package_name' : "com.metacube.sindorim.aos",
        'installs' : "50,000+",
        'version' : "0.9.63",
    },
    'com.NextFloor.DestinyChild' :
    {
        'category' : "Games",
        'updated' : "08 Dec 2021",
        'App_name' : "\ub370\uc2a4\ud2f0\ub2c8 \ucc28\uc77c\ub4dc",
        'Package_name' : "com.NextFloor.DestinyChild",
        'installs' : "1,000,000+",
        'version' : "2.9.0",
    },
    'com.landmarkgroup.splashfashions' :
    {
        'category' : "Shopping",
        'updated' : "03 Jun 2022",
        'App_name' : "Splash Online - \u0633\u0628\u0644\u0627\u0634 \u0627\u0648\u0646 \u0644\u0627\u064a\u0646",
        'Package_name' : "com.landmarkgroup.splashfashions",
        'installs' : "1,000,000+",
        'version' : "7.45",
    },
    '1215668882' :
    {
        'category' : "Games",
        'updated' : "2022-06-16T08:37:57Z",
        'App_name' : "Big Farm: Mobile Harvest",
        'Package_name' : "com.goodgamestudios.bigfarmmobileharvest",
        'version' : "10.8.27254",
        'minimumOsVersion' : "11.0",
    },
    'com.hkgame.plantswar' :
    {
        'category' : "Games",
        'updated' : "21 May 2021",
        'App_name' : "Plants War",
        'Package_name' : "com.hkgame.plantswar",
        'installs' : "1,000+",
        'version' : "1.0.19",
    },
    'com.gamepub.sbmir.g' :
    {
        'category' : "Games",
        'updated' : "12 May 2021",
        'App_name' : "\ubbf8\ub974\uc758\uc804\uc1242 \uc0ac\ubd81\uc7c1\ud0c8",
        'Package_name' : "com.gamepub.sbmir.g",
        'installs' : "100,000+",
        'version' : "1.0.5.0",
    },
    'com.fstudio.kream' :
    {
        'category' : "Shopping",
        'updated' : "18 Jun 2022",
        'App_name' : "KREAM (\ud06c\ub9bc)",
        'Package_name' : "com.fstudio.kream",
        'installs' : "1,000,000+",
        'version' : "3.9.8",
    },
    'com.sgzjxz.gp' :
    {
        'category' : "Games",
        'updated' : "03 Nov 2021",
        'App_name' : "\uc0bc\uad6d\uc9c0\uad70\uc6c5\uc804",
        'Package_name' : "com.sgzjxz.gp",
        'installs' : "100,000+",
        'version' : "1.0.22",
    },
    'kr.tripstore.tripstore' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "08 Jun 2022",
        'App_name' : "\ud2b8\ub9bd\uc2a4\ud1a0\uc5b4 - \uc990\uac70\uc6b4 \uc5ec\ud589\uc744 \uc27d\uac8c",
        'Package_name' : "kr.tripstore.tripstore",
        'installs' : "1,000,000+",
        'version' : "22.06.08",
    },
    'de.axelspringer.yana' :
    {
        'category' : "Education and Books",
        'updated' : "13 Jun 2022",
        'App_name' : "upday - Big news in short time",
        'Package_name' : "de.axelspringer.yana",
        'installs' : "1,000,000+",
        'version' : "3.1.14655",
    },
    'vn.funtap.dailuc.vinhdieu' :
    {
        'category' : "Games",
        'updated' : "03 Mar 2022",
        'App_name' : "\u0110\u1ea1i L\u1ee5c Vinh Di\u1ec7u - Dai Luc Vinh Dieu",
        'Package_name' : "vn.funtap.dailuc.vinhdieu",
        'installs' : "100,000+",
        'version' : "1.0.7",
    },
    'jp.co.fablic.fril' :
    {
        'category' : "Shopping",
        'updated' : "27 May 2022",
        'App_name' : "\u697d\u5929\u30e9\u30af\u30de-\u30d5\u30ea\u30de\u30a2\u30d7\u30ea",
        'Package_name' : "jp.co.fablic.fril",
        'installs' : "5,000,000+",
        'version' : "8.2.0",
    },
    'com.socialswag.app' :
    {
        'category' : "Social and Communication",
        'updated' : "20 May 2022",
        'App_name' : "SocialSwag",
        'Package_name' : "com.socialswag.app",
        'installs' : "50,000+",
        'version' : "0.1.35-alpha",
    },
    'com.estgames.cabalmkor' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "\uce74\ubc1c \ubaa8\ubc14\uc77c (CABAL Mobile)",
        'Package_name' : "com.estgames.cabalmkor",
        'installs' : "100,000+",
        'version' : "1.1.98",
    },
    'com.tjbnew4399kr.google' :
    {
        'category' : "Games",
        'updated' : "20 Nov 2020",
        'App_name' : "\uc2a4\ud14c\ub9ac\ud14c\uc77c",
        'Package_name' : "com.tjbnew4399kr.google",
        'installs' : "100,000+",
        'version' : "0.16.20",
    },
    'com.bdswissnative' :
    {
        'category' : "Finance",
        'updated' : "03 Jun 2022",
        'App_name' : "BDSwiss Online Trading",
        'Package_name' : "com.bdswissnative",
        'installs' : "100,000+",
        'version' : "4.6.3.4",
    },
    'lili.co' :
    {
        'category' : "Finance",
        'updated' : "02 Jun 2022",
        'App_name' : "Lili Banking for Your Business",
        'Package_name' : "lili.co",
        'installs' : "500,000+",
        'version' : "22.6.1",
    },
    'com.ggwatertv.android.air' :
    {
        'category' : "Entertainment",
        'updated' : "10 May 2022",
        'App_name' : "\uafc0\ubb3c\ud2f0\ube44 - S\uae09 \uc5ec\ucea0BJ 19\uac1c\uc778\ubc29\uc1a1 \ud31d\ucf58\ud2f0\ube44 \uc5f0\ub3d9",
        'Package_name' : "com.ggwatertv.android.air",
        'installs' : "100,000+",
        'version' : "4.6.00",
    },
    'com.truefan.user.live' :
    {
        'category' : "Entertainment",
        'updated' : "09 Jun 2022",
        'App_name' : "TrueFan - Get Video Messages",
        'Package_name' : "com.truefan.user.live",
        'installs' : "1,000,000+",
        'version' : "2.4.3",
    },
    'com.lenskart.app' :
    {
        'category' : "Shopping",
        'updated' : "17 Jun 2022",
        'App_name' : "Lenskart : Eyeglasses & More",
        'Package_name' : "com.lenskart.app",
        'installs' : "10,000,000+",
        'version' : "3.5.7",
    },
    'com.ftband.mono' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "monobank \u2014 \u0431\u0430\u043d\u043a \u0432 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0456",
        'Package_name' : "com.ftband.mono",
        'installs' : "5,000,000+",
        'version' : "1.40.8",
    },
    'com.Garawell.BridgeRace' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Bridge Race",
        'Package_name' : "com.Garawell.BridgeRace",
        'installs' : "100,000,000+",
        'version' : "3.4.0",
    },
    '1057314145' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-14T22:24:36Z",
        'App_name' : "Claro shop",
        'Package_name' : "com.claro.americamovil.claroshop",
        'version' : "6.15.1",
        'minimumOsVersion' : "13.0",
    },
    'com.efun.sn' :
    {
        'category' : "Games",
        'updated' : "25 Sep 2020",
        'App_name' : "\u30bf\u30a4\u30e0\u30ea\u30d5\u30ec\u30a4\u30f3",
        'Package_name' : "com.efun.sn",
        'installs' : "50,000+",
        'version' : "1.0.1",
    },
    'com.ncp.ncprecap' :
    {
        'category' : "Shopping",
        'updated' : "05 Dec 2019",
        'App_name' : "NCP ReCap: Shopping Rewards",
        'Package_name' : "com.ncp.ncprecap",
        'installs' : "100,000+",
        'version' : "1.0.22",
    },
    '1350350945' :
    {
        'category' : "Games",
        'updated' : "2021-02-24T05:00:30Z",
        'App_name' : "\u82f1\u8a9e\u3068\u30af\u30a4\u30ba\u306e\u30b3\u30b3\u30ed\u30bb\u30ab\u30a4",
        'Package_name' : "jp.summervacation.cocorosekai",
        'version' : "1.7.5",
        'minimumOsVersion' : "9.0",
    },
    '451684733' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-16T02:09:45Z",
        'App_name' : "Pokecolo",
        'Package_name' : "jp.pocketcolony.PocketColony",
        'version' : "9.16.0",
        'minimumOsVersion' : "11.0",
    },
    'com.gu_global.osharemaker.kr' :
    {
        'category' : "Shopping",
        'updated' : "07 Aug 2019",
        'App_name' : "GU Korea",
        'Package_name' : "com.gu_global.osharemaker.kr",
        'installs' : "100,000+",
        'version' : "1.1.4",
    },
    'org.fxclub.libertex' :
    {
        'category' : "Finance",
        'updated' : "16 Feb 2022",
        'App_name' : "Libertex: Trade Stocks & Forex",
        'Package_name' : "org.fxclub.libertex",
        'installs' : "10,000,000+",
        'version' : "2.30.0",
    },
    'com.app.bluemena' :
    {
        'category' : "Food & Drink",
        'updated' : "10 Jun 2022",
        'App_name' : "\u0645\u064a\u0627\u0647 \u0646\u0648\u06a4\u0627 - Nova Water",
        'Package_name' : "com.app.bluemena",
        'installs' : "100,000+",
        'version' : "2.0.370",
    },
    'com.topps.ice' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Topps\u00ae NHL SKATE\u2122: Hockey Card Trader",
        'Package_name' : "com.topps.ice",
        'installs' : "100,000+",
        'version' : "19.0.0",
    },
    'com.navi.insurance' :
    {
        'category' : "Finance",
        'updated' : "20 Nov 2021",
        'App_name' : "Navi Health Insurance - Pay using monthly premium",
        'Package_name' : "com.navi.insurance",
        'installs' : "1,000,000+",
        'version' : "1.1.3",
    },
    'com.netease.project.x2.kr.google' :
    {
        'category' : "Games",
        'updated' : "12 Jan 2022",
        'App_name' : "X2: \uc774\ud074\ub9bd\uc2a4",
        'Package_name' : "com.netease.project.x2.kr.google",
        'installs' : "100,000+",
        'version' : "1.0",
    },
    'com.alodokter.android' :
    {
        'category' : "Health and Fitness",
        'updated' : "05 Jun 2022",
        'App_name' : "Alodokter \u2014Chat Bersama Dokter",
        'Package_name' : "com.alodokter.android",
        'installs' : "5,000,000+",
        'version' : "4.4.0",
    },
    'com.bleu122.knowminut' :
    {
        'category' : "Games",
        'updated' : "20 Apr 2022",
        'App_name' : "Know Minut Podcasts",
        'Package_name' : "com.bleu122.knowminut",
        'installs' : "10,000+",
        'version' : "5.1.0",
    },
    'in.siply.ionic' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Siply: Gold Savings & Stocks",
        'Package_name' : "in.siply.ionic",
        'installs' : "1,000,000+",
        'version' : "2.3.4-RC-01",
    },
    'com.leyou.acgn.fx' :
    {
        'category' : "Games",
        'updated' : "18 May 2021",
        'App_name' : "\u30d7\u30ed\u30b8\u30a7\u30af\u30c8\u30fb\u30b7\u30eb\u30d0\u30fc\u30a6\u30a4\u30f3\u30b0",
        'Package_name' : "com.leyou.acgn.fx",
        'installs' : "10,000+",
        'version' : "1.1.9",
    },
    'com.veseystudios.veseyrewards' :
    {
        'category' : "Lifestyle",
        'updated' : "19 Jan 2021",
        'App_name' : "Boodle: Earn Rewards Discovering New Apps & Games",
        'Package_name' : "com.veseystudios.veseyrewards",
        'installs' : "500,000+",
        'version' : "1.21.0",
    },
    'com.square_enix.android_googleplay.SB69Fes' :
    {
        'category' : "Games",
        'updated' : "05 Jun 2022",
        'App_name' : "SHOW BY ROCK!! Fes A Live",
        'Package_name' : "com.square_enix.android_googleplay.SB69Fes",
        'installs' : "100,000+",
        'version' : "1.52.0",
    },
    'in.playsimple.wordtrip' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Word Trip",
        'Package_name' : "in.playsimple.wordtrip",
        'installs' : "10,000,000+",
        'version' : "1.450.0",
    },
    'com.t3.luna' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "\ub8e8\ub098 \ubaa8\ubc14\uc77c(12)",
        'Package_name' : "com.t3.luna",
        'installs' : "100,000+",
        'version' : "1.0.621",
    },
    'com.global.kingdom' :
    {
        'category' : "Games",
        'updated' : "26 Oct 2021",
        'App_name' : "\u4e09\u56fd\u82f1\u96c4\u305f\u3061\u306e\u591c\u660e\u3051",
        'Package_name' : "com.global.kingdom",
        'installs' : "100,000+",
        'version' : "2.0",
    },
    '1534477610' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-14T07:28:39Z",
        'App_name' : "T\u0131kla Gelsin\u00ae",
        'Package_name' : "com.ataexpress.tiklagelsin",
        'version' : "2.3.1",
        'minimumOsVersion' : "11.0",
    },
    'vn.funtap.tinhkiem.mobile3d' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "T\u00ecnh Ki\u1ebfm 3D-Tam Ni\u00ean V\u1ea1n Ph\u00fac",
        'Package_name' : "vn.funtap.tinhkiem.mobile3d",
        'installs' : "1,000,000+",
        'version' : "1.0.64",
    },
    'com.omnitel.android.lottewebview' :
    {
        'category' : "Shopping",
        'updated' : "25 May 2022",
        'App_name' : "\ub86f\ub370\ud648\uc1fc\ud551",
        'Package_name' : "com.omnitel.android.lottewebview",
        'installs' : "10,000,000+",
        'version' : "3.6.1",
    },
    'com.dhgate.buyermob' :
    {
        'category' : "Shopping",
        'updated' : "16 Jun 2022",
        'App_name' : "DHgate-online wholesale stores",
        'Package_name' : "com.dhgate.buyermob",
        'installs' : "10,000,000+",
        'version' : "5.8.4",
    },
    'jp.konami.jlccs' :
    {
        'category' : "Games",
        'updated' : "06 Jun 2022",
        'App_name' : "\uff2a\u30ea\u30fc\u30b0\u30af\u30e9\u30d6\u30c1\u30e3\u30f3\u30d4\u30aa\u30f3\u30b7\u30c3\u30d7",
        'Package_name' : "jp.konami.jlccs",
        'installs' : "100,000+",
        'version' : "4.2.1",
    },
    'com.matchlatam.parperfeitoapp' :
    {
        'category' : "Social and Communication",
        'updated' : "25 May 2022",
        'App_name' : "Par Perfeito: Encontros/Namoro",
        'Package_name' : "com.matchlatam.parperfeitoapp",
        'installs' : "10,000,000+",
        'version' : "21.11.02",
    },
    'com.hypercarrot.giantrush' :
    {
        'category' : "Games",
        'updated' : "11 May 2022",
        'App_name' : "Giant Rush!",
        'Package_name' : "com.hypercarrot.giantrush",
        'installs' : "50,000,000+",
        'version' : "1.6.0",
    },
    'com.westernunion.moneytransferr3app.sa' :
    {
        'category' : "Finance",
        'updated' : "17 Jun 2022",
        'App_name' : "WesternUnion SA Money Transfer",
        'Package_name' : "com.westernunion.moneytransferr3app.sa",
        'installs' : "500,000+",
        'version' : "1.228.0",
    },
    'com.cars24.seller' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "10 Jun 2022",
        'App_name' : "CARS24\u00ae: Buy Used Cars & Sell",
        'Package_name' : "com.cars24.seller",
        'installs' : "10,000,000+",
        'version' : "10.7.1",
    },
    '1220720235' :
    {
        'category' : "Games",
        'updated' : "2022-06-10T02:11:01Z",
        'App_name' : "\ube14\ub808\uc774\ub4dc&\uc18c\uc6b8 \ub808\ubcfc\ub8e8\uc158",
        'Package_name' : "com.netmarble.bnsmkr",
        'version' : "1.1.66",
        'minimumOsVersion' : "9.0",
    },
    'jp.co.koeitecmo.Might' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "\u4e09\u570b\u5fd7 \u8987\u9053",
        'Package_name' : "jp.co.koeitecmo.Might",
        'installs' : "100,000+",
        'version' : "1.07.00",
    },
    'com.unicell.pangoandroid' :
    {
        'category' : "Utilities",
        'updated' : "30 May 2022",
        'App_name' : "\u05e4\u05e0\u05d2\u05d5 - Pango",
        'Package_name' : "com.unicell.pangoandroid",
        'installs' : "1,000,000+",
        'version' : "10.0011",
    },
    'com.leflair.android' :
    {
        'category' : "Shopping",
        'updated' : "21 Apr 2022",
        'App_name' : "Leflair - H\u00e0ng hi\u1ec7u ch\u00ednh h\u00e3ng",
        'Package_name' : "com.leflair.android",
        'installs' : "10,000+",
        'version' : "1.6.4",
    },
    'jp.co.cygames.Shadowverse' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "\u30b7\u30e3\u30c9\u30a6\u30d0\u30fc\u30b9 (Shadowverse)",
        'Package_name' : "jp.co.cygames.Shadowverse",
        'installs' : "1,000,000+",
        'version' : "3.7.30",
    },
    'vn.funtap.phongkhoitruongan' :
    {
        'category' : "Games",
        'updated' : "08 Mar 2022",
        'App_name' : "Phong Kh\u1edfi Tr\u01b0\u1eddng An",
        'Package_name' : "vn.funtap.phongkhoitruongan",
        'installs' : "500,000+",
        'version' : "1.31",
    },
    'br.com.fogas.service' :
    {
        'category' : "Shopping",
        'updated' : "02 Jun 2022",
        'App_name' : "Fog\u00e1s: Pre\u00e7o do g\u00e1s de cozinha",
        'Package_name' : "br.com.fogas.service",
        'installs' : "1,000,000+",
        'version' : "4.8.7",
    },
    'com.funteng.cfslots' :
    {
        'category' : "Casino",
        'updated' : "25 Jan 2022",
        'App_name' : "Crazyfun Slots",
        'Package_name' : "com.funteng.cfslots",
        'installs' : "100,000+",
        'version' : "1.0.9",
    },
    'com.exness.android.pa' :
    {
        'category' : "Finance",
        'updated' : "09 Jun 2022",
        'App_name' : "Exness Trade: Online Trading",
        'Package_name' : "com.exness.android.pa",
        'installs' : "5,000,000+",
        'version' : "2.158.2-real-release",
    },
    'com.techbins.niki' :
    {
        'category' : "Shopping",
        'updated' : "22 Apr 2021",
        'App_name' : "Niki: Ration, Online Recharge & Bill Payment",
        'Package_name' : "com.techbins.niki",
        'installs' : "1,000,000+",
        'version' : "3.14",
    },
    'com.namcobandaigames.spmoja010E' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "ONE PIECE TREASURE CRUISE",
        'Package_name' : "com.namcobandaigames.spmoja010E",
        'installs' : "10,000,000+",
        'version' : "12.0.2",
    },
    'com.gofody.food.android' :
    {
        'category' : "Food & Drink",
        'updated' : "13 Jun 2022",
        'App_name' : "Gofody Yemek",
        'Package_name' : "com.gofody.food.android",
        'installs' : "100,000+",
        'version' : "1.7.0",
    },
    'com.topps.slam' :
    {
        'category' : "Entertainment",
        'updated' : "18 Mar 2022",
        'App_name' : "Topps\u00ae WWE SLAM: Card Trader",
        'Package_name' : "com.topps.slam",
        'installs' : "1,000,000+",
        'version' : "18.2.0",
    },
    '1040047095' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-07T01:20:54Z",
        'App_name' : "Spoon: Livestream Talk & music",
        'Package_name' : "Mycoon.SpoonMe",
        'version' : "7.7.1",
        'minimumOsVersion' : "12.0",
    },
    'com.fun.blowup' :
    {
        'category' : "Games",
        'updated' : "08 Feb 2022",
        'App_name' : "Blow Them Up",
        'Package_name' : "com.fun.blowup",
        'installs' : "5,000,000+",
        'version' : "1.4.2.1",
    },
    '1017261655' :
    {
        'category' : "Finance",
        'updated' : "2022-06-17T10:51:35Z",
        'App_name' : "Scan Hero: PDF Scanner",
        'Package_name' : "com.apalonapps.pdffree",
        'version' : "3.13.0",
        'minimumOsVersion' : "13.0",
    },
    'com.elparking.elparking' :
    {
        'category' : "Utilities",
        'updated' : "02 Jun 2022",
        'App_name' : "ElParking - Book your parking spot",
        'Package_name' : "com.elparking.elparking",
        'installs' : "1,000,000+",
        'version' : "9.28",
    },
    'kr.tl.girls' :
    {
        'category' : "Games",
        'updated' : "24 Jun 2021",
        'App_name' : "\ucd08\ucc28\uc6d0\uc5ec\uce5c: \uc5ec\uc2e0\uc758 \ud658\uc0c1\ub099\uc6d0",
        'Package_name' : "kr.tl.girls",
        'installs' : "50,000+",
        'version' : "1.0.60",
    },
    'com.kwai.video' :
    {
        'category' : "Entertainment",
        'updated' : "17 Jun 2022",
        'App_name' : "Kwai - Watch cool&funny videos",
        'Package_name' : "com.kwai.video",
        'installs' : "100,000,000+",
        'version' : "6.5.10.526002",
    },
    'com.CouponChart' :
    {
        'category' : "Shopping",
        'updated' : "16 May 2022",
        'App_name' : "\ucfe0\ucc28",
        'Package_name' : "com.CouponChart",
        'installs' : "10,000,000+",
        'version' : "5.66",
    },
    '600535436' :
    {
        'category' : "Games",
        'updated' : "2022-06-06T21:01:16Z",
        'App_name' : "Tuttur - iddaa & Canl\u0131 Bahis",
        'Package_name' : "com.tuttur.app",
        'version' : "7.2.1",
        'minimumOsVersion' : "12.0",
    },
    'com.ixigo' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "31 May 2022",
        'App_name' : "Flight Booking & Cheap Flights",
        'Package_name' : "com.ixigo",
        'installs' : "10,000,000+",
        'version' : "4.5.3",
    },
    'com.mobilion.total.v2' :
    {
        'category' : "Finance",
        'updated' : "01 Apr 2022",
        'App_name' : "Total Oil T\u00fcrkiye",
        'Package_name' : "com.mobilion.total.v2",
        'installs' : "100,000+",
        'version' : "1.6.4",
    },
    '757934825' :
    {
        'category' : "Health and Fitness",
        'updated' : "2022-05-30T02:31:51Z",
        'App_name' : "\ubc14\ube44\ud1a1 - Babitalk",
        'Package_name' : "com.healthing.babitalk",
        'version' : "5.36.0",
        'minimumOsVersion' : "11.0",
    },
    '600674056' :
    {
        'category' : "Games",
        'updated' : "2022-05-20T07:47:39Z",
        'App_name' : "Pictoword: Fun Word Quiz Games",
        'Package_name' : "com.kooapps.pictoword",
        'version' : "2.6.35",
        'minimumOsVersion' : "9.0",
    },
    'com.doctor247.patient' :
    {
        'category' : "Health and Fitness",
        'updated' : "07 Apr 2022",
        'App_name' : "Doctor 24x7 - Consult Doctors Online On Call/Video",
        'Package_name' : "com.doctor247.patient",
        'installs' : "100,000+",
        'version' : "8.0",
    },
    'com.hkt.nightingale' :
    {
        'category' : "Health and Fitness",
        'updated' : "23 May 2022",
        'App_name' : "DrGo - Video consultation on the go",
        'Package_name' : "com.hkt.nightingale",
        'installs' : "100,000+",
        'version' : "1.25.2",
    },
    'com.topps.bunt' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "Topps\u00ae BUNT\u00ae MLB Card Trader",
        'Package_name' : "com.topps.bunt",
        'installs' : "1,000,000+",
        'version' : "19.0.0",
    },
    '931370010' :
    {
        'category' : "Games",
        'updated' : "2022-05-23T01:01:53Z",
        'App_name' : "\u5929\u4e0b\u7d71\u4e00\u604b\u306e\u4e71\u3000Love Ballad",
        'Package_name' : "com.voltage.j.tenka",
        'version' : "8.5",
        'minimumOsVersion' : "9.0",
    },
    '1094194042' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-19T20:52:56Z",
        'App_name' : "Mumzworld",
        'Package_name' : "com.mumzworldfzllc.mumzworld",
        'version' : "3.8",
        'minimumOsVersion' : "11.0",
    },
    'com.nomadeducation.nomadeducation' :
    {
        'category' : "Education and Books",
        'updated' : "08 Jun 2022",
        'App_name' : "Brevet Bac Licence Sup 2022",
        'Package_name' : "com.nomadeducation.nomadeducation",
        'installs' : "1,000,000+",
        'version' : "5.11.4",
    },
    'com.siwar.app' :
    {
        'category' : "Food & Drink",
        'updated' : "21 Apr 2022",
        'App_name' : "Siwar - \u0633\u0648\u0627\u0631",
        'Package_name' : "com.siwar.app",
        'installs' : "1,000+",
        'version' : "3.2.0",
    },
    'com.panteon.homesweethome' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Home Restoration",
        'Package_name' : "com.panteon.homesweethome",
        'installs' : "10,000,000+",
        'version' : "2.10",
    },
    'fit.cure.android' :
    {
        'category' : "Health and Fitness",
        'updated' : "16 Jun 2022",
        'App_name' : "cult.fit Health Fitness & Gyms",
        'Package_name' : "fit.cure.android",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    '1516465507' :
    {
        'category' : "Games",
        'updated' : "2022-06-15T11:39:23Z",
        'App_name' : "\u5168\u9762\u5c4d\u63a7",
        'Package_name' : "com.kingsgroup.ss.tw",
        'version' : "1.16.10",
        'minimumOsVersion' : "11.0",
    },
    'air.jp.co.cygames.worldflipper' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "\u30ef\u30fc\u30eb\u30c9\u30d5\u30ea\u30c3\u30d1\u30fc",
        'Package_name' : "air.jp.co.cygames.worldflipper",
        'installs' : "100,000+",
        'version' : "1.522.1",
    },
    'com.csjsnetfunkr.google' :
    {
        'category' : "Games",
        'updated' : "22 Jun 2021",
        'App_name' : "\uc6d0\ub354\uc544\ub808\ub098",
        'Package_name' : "com.csjsnetfunkr.google",
        'installs' : "10,000+",
        'version' : "1.1.4",
    },
    'com.nhnent.pokerclassic' :
    {
        'category' : "Casino",
        'updated' : "27 May 2022",
        'App_name' : "\ud55c\uac8c\uc784\ud3ec\ucee4 \ud074\ub798\uc2dd with PC",
        'Package_name' : "com.nhnent.pokerclassic",
        'installs' : "100,000+",
        'version' : "1.3.44",
    },
    'com.sebbia.delivery.client' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Dostavista \u2014 Delivery Service",
        'Package_name' : "com.sebbia.delivery.client",
        'installs' : "1,000,000+",
        'version' : "1.63.0",
    },
    'com.hrs.b2c.android' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "21 Feb 2022",
        'App_name' : "Hotel Search HRS (New)",
        'Package_name' : "com.hrs.b2c.android",
        'installs' : "1,000,000+",
        'version' : "8.27.0",
    },
    'com.wt.birdtornado' :
    {
        'category' : "Games",
        'updated' : "17 Dec 2021",
        'App_name' : "BirdTornado on WEMIX",
        'Package_name' : "com.wt.birdtornado",
        'installs' : "500,000+",
        'version' : "1.11.0",
    },
    '870365423' :
    {
        'category' : "Games",
        'updated' : "2022-06-15T22:11:49Z",
        'App_name' : "LINE Let's Get Rich",
        'Package_name' : "com.linecorp.LGGRTH",
        'version' : "4.0.0",
        'minimumOsVersion' : "10.0",
    },
    '1467291840' :
    {
        'category' : "Games",
        'updated' : "2022-01-29T08:44:38Z",
        'App_name' : "Hashiriya Drifter Car Racing",
        'Package_name' : "com.crazydevjj.HashiriyaDrifteriOS",
        'version' : "2.3.3",
        'minimumOsVersion' : "11.0",
    },
    'jp.co.rakuten.slide' :
    {
        'category' : "Lifestyle",
        'updated' : "09 May 2022",
        'App_name' : "Super Point Screen - Rewards",
        'Package_name' : "jp.co.rakuten.slide",
        'installs' : "1,000,000+",
        'version' : "7.3.6",
    },
    'com.idevspro.author.customers' :
    {
        'category' : "Education and Books",
        'updated' : "12 May 2022",
        'App_name' : "Author24 - Study Experts Marketplace",
        'Package_name' : "com.idevspro.author.customers",
        'installs' : "500,000+",
        'version' : "4.21.0",
    },
    'air.com.bitrhymes.bingo' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Bingo Bash: Fun Bingo Games",
        'Package_name' : "air.com.bitrhymes.bingo",
        'installs' : "10,000,000+",
        'version' : "1.187.1",
    },
    'com.muna.lively' :
    {
        'category' : "Health and Fitness",
        'updated' : "01 Jun 2022",
        'App_name' : "Fita. Sehat Makin Nikmat.",
        'Package_name' : "com.muna.lively",
        'installs' : "1,000,000+",
        'version' : "1.10.0+188",
    },
    'ae.ahb.digital' :
    {
        'category' : "Finance",
        'updated' : "04 Jun 2022",
        'App_name' : "Al Hilal Digital",
        'Package_name' : "ae.ahb.digital",
        'installs' : "100,000+",
        'version' : "1.2.7(2.9.7-11040)",
    },
    'com.yoozoogames.carromfriendsboardgames' :
    {
        'category' : "Games",
        'updated' : "24 Dec 2021",
        'App_name' : "Carrom Friends : Carrom Board Game",
        'Package_name' : "com.yoozoogames.carromfriendsboardgames",
        'installs' : "10,000,000+",
        'version' : "1.0.35",
    },
    'com.crowdstar.covetHome' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "Design Home: Real Home Decor",
        'Package_name' : "com.crowdstar.covetHome",
        'installs' : "50,000,000+",
        'version' : "1.85.097",
    },
    'com.shgame.sg293' :
    {
        'category' : "Games",
        'updated' : "12 Jan 2022",
        'App_name' : "T\u00e2n Minh Ch\u1ee7 - SohaGame",
        'Package_name' : "com.shgame.sg293",
        'installs' : "100,000+",
        'version' : "8.1",
    },
    'com.transfergo.android' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "TransferGo: Money Transfer",
        'Package_name' : "com.transfergo.android",
        'installs' : "1,000,000+",
        'version' : "4.39.0",
    },
    'com.crazylabs.acrylic.nails' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "Acrylic Nails!",
        'Package_name' : "com.crazylabs.acrylic.nails",
        'installs' : "50,000,000+",
        'version' : "1.3.0.0",
    },
    'ru.ivi.client' :
    {
        'category' : "Entertainment",
        'updated' : "16 Jun 2022",
        'App_name' : "IVI: \u0441\u0435\u0440\u0438\u0430\u043b\u044b, \u0444\u0438\u043b\u044c\u043c\u044b, \u043c\u0443\u043b\u044c\u0442\u0438\u043a\u0438",
        'Package_name' : "ru.ivi.client",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.eyougame.astral' :
    {
        'category' : "Games",
        'updated' : "15 Feb 2021",
        'App_name' : "\uc584\uc2a4\ubaa8\ud5d8\ub2e8",
        'Package_name' : "com.eyougame.astral",
        'installs' : "10,000+",
        'version' : "2003.9.0",
    },
    'zebpay.Application' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "ZebPay Crypto Exchange",
        'Package_name' : "zebpay.Application",
        'installs' : "5,000,000+",
        'version' : "3.18.01",
    },
    '1457173515' :
    {
        'category' : "Games",
        'updated' : "2022-06-10T11:14:48Z",
        'App_name' : "King's Throne",
        'Package_name' : "com.goat.ksrgl",
        'version' : "1.3.146",
        'minimumOsVersion' : "10.0",
    },
    'com.dts.freefireth' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "Garena Free Fire: Rampage",
        'Package_name' : "com.dts.freefireth",
        'installs' : "1,000,000,000+",
        'version' : "1.90.1",
    },
    '1507999406' :
    {
        'category' : "Games",
        'updated' : "2022-04-27T01:33:28Z",
        'App_name' : "Icarus M: Riders of Icarus",
        'Package_name' : "com.valofe.icarusm.na",
        'version' : "1.0.28",
        'minimumOsVersion' : "11.0",
    },
    'com.blancozone.xxzyd.gp' :
    {
        'category' : "Games",
        'updated' : "23 Feb 2021",
        'App_name' : "\u96f2\u7aef\u4fee\u884c\u624b\u8a18",
        'Package_name' : "com.blancozone.xxzyd.gp",
        'installs' : "100,000+",
        'version' : "2.2.1",
    },
    'com.glu.dashtown' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "Diner DASH Adventures",
        'Package_name' : "com.glu.dashtown",
        'installs' : "5,000,000+",
        'version' : "1.35.3",
    },
    'jp.konami.pawasaka' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "\u5b9f\u6cc1\u30d1\u30ef\u30d5\u30eb\u30b5\u30c3\u30ab\u30fc",
        'Package_name' : "jp.konami.pawasaka",
        'installs' : "1,000,000+",
        'version' : "7.0.61",
    },
    'com.viacom18.vootkids' :
    {
        'category' : "Education and Books",
        'updated' : "24 May 2022",
        'App_name' : "Voot Kids",
        'Package_name' : "com.viacom18.vootkids",
        'installs' : "10,000,000+",
        'version' : "1.28.4",
    },
    'com.yoozoo.kr.snsgz2' :
    {
        'category' : "Games",
        'updated' : "18 May 2022",
        'App_name' : "\uadf8\ub791\uc0bc\uad6d",
        'Package_name' : "com.yoozoo.kr.snsgz2",
        'installs' : "500,000+",
        'version' : "1.7.85",
    },
    'com.kienguru.livestudents' :
    {
        'category' : "Education and Books",
        'updated' : "31 May 2022",
        'App_name' : "Ki\u1ebfn Guru - H\u1ecdc vui h\u1ecdc ch\u1ea5t",
        'Package_name' : "com.kienguru.livestudents",
        'installs' : "1,000,000+",
        'version' : "6.32.0",
    },
    'com.akinon.occasion' :
    {
        'category' : "Shopping",
        'updated' : "01 Jun 2022",
        'App_name' : "Occasion",
        'Package_name' : "com.akinon.occasion",
        'installs' : "100,000+",
        'version' : "4.4.9",
    },
    'com.yajny.cashback' :
    {
        'category' : "Shopping",
        'updated' : "03 Jun 2022",
        'App_name' : "Yajny - Shop & Save Cash",
        'Package_name' : "com.yajny.cashback",
        'installs' : "100,000+",
        'version' : "2.1.40",
    },
    'com.the29cm.app29cm' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "29CM - \uac10\ub3c4 \uae4a\uc740 \ucde8\ud5a5 \uc140\ub809\ud2b8\uc0f5",
        'Package_name' : "com.the29cm.app29cm",
        'installs' : "1,000,000+",
        'version' : "4.50.0",
    },
    'com.marktguru.mg2.de' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "marktguru - leaflets & offers",
        'Package_name' : "com.marktguru.mg2.de",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.playtimekarma.app' :
    {
        'category' : "Entertainment",
        'updated' : "21 Apr 2022",
        'App_name' : "PlayKarma Rewards: Gift Cards & Scratch Cards",
        'Package_name' : "com.playtimekarma.app",
        'installs' : "100,000+",
        'version' : "1.1.10",
    },
    'com.proxgy.app' :
    {
        'category' : "Lifestyle",
        'updated' : "12 Oct 2021",
        'App_name' : "Proxgy: Live Visit Places, Stores & Showrooms",
        'Package_name' : "com.proxgy.app",
        'installs' : "10,000+",
        'version' : "1.2.11",
    },
    'com.sgiggle.production' :
    {
        'category' : "Social and Communication",
        'updated' : "17 Jun 2022",
        'App_name' : "Tango-Live Stream & Video Chat",
        'Package_name' : "com.sgiggle.production",
        'installs' : "100,000,000+",
        'version' : "7.32.1655277044",
    },
    'connectiq.miles.app' :
    {
        'category' : "Lifestyle",
        'updated' : "15 Jun 2022",
        'App_name' : "Miles - Rewards For All Travel",
        'Package_name' : "connectiq.miles.app",
        'installs' : "1,000,000+",
        'version' : "2.1.139(1)",
    },
    'com.emkanapp' :
    {
        'category' : "Finance",
        'updated' : "09 Jun 2022",
        'App_name' : "Emkan Finance",
        'Package_name' : "com.emkanapp",
        'installs' : "500,000+",
        'version' : "2.0.29",
    },
    'pro.huobi' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Huobi: Trade Crypto & Bitcoin",
        'Package_name' : "pro.huobi",
        'installs' : "5,000,000+",
        'version' : "7.0.5",
    },
    '1445028663' :
    {
        'category' : "Games",
        'updated' : "2020-11-06T01:54:29Z",
        'App_name' : "Snake Blast!",
        'Package_name' : "advenworks.com.snakeblast",
        'version' : "1.660",
        'minimumOsVersion' : "9.0",
    },
    'br.com.ubook.ubookapp' :
    {
        'category' : "Education and Books",
        'updated' : "21 May 2022",
        'App_name' : "Ubook",
        'Package_name' : "br.com.ubook.ubookapp",
        'installs' : "1,000,000+",
        'version' : "10.0.1",
    },
    'com.affinity.rewarded_play' :
    {
        'category' : "Entertainment",
        'updated' : "11 May 2022",
        'App_name' : "Rewarded Play: Earn Gift Cards",
        'Package_name' : "com.affinity.rewarded_play",
        'installs' : "5,000,000+",
        'version' : "9.2.1",
    },
    '1322397590' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-08T07:12:37Z",
        'App_name' : "Casino Drive et Livraison",
        'Package_name' : "fr.casino.casinodrive",
        'version' : "2.22.0",
        'minimumOsVersion' : "11.0",
    },
    'com.cocacola.app.cee' :
    {
        'category' : "Lifestyle",
        'updated' : "30 May 2022",
        'App_name' : "Coca-Cola",
        'Package_name' : "com.cocacola.app.cee",
        'installs' : "5,000,000+",
        'version' : "2.98.4",
    },
    'com.mashreq.NeoApp' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Mashreq Neo - Bank easy",
        'Package_name' : "com.mashreq.NeoApp",
        'installs' : "1,000,000+",
        'version' : "3.3.31",
    },
    'com.dd.doordash' :
    {
        'category' : "Food & Drink",
        'updated' : "13 Jun 2022",
        'App_name' : "DoorDash - Food Delivery",
        'Package_name' : "com.dd.doordash",
        'installs' : "10,000,000+",
        'version' : "15.58.13",
    },
    'com.Gameberry.ColorWall' :
    {
        'category' : "Games",
        'updated' : "01 Dec 2020",
        'App_name' : "Color Wall",
        'Package_name' : "com.Gameberry.ColorWall",
        'installs' : "1,000+",
        'version' : "2.3",
    },
    'com.cakecodes.bitmaker' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "StormX: Shop and Earn Crypto",
        'Package_name' : "com.cakecodes.bitmaker",
        'installs' : "1,000,000+",
        'version' : "8.15.3",
    },
    'com.star.union.planetant' :
    {
        'category' : "Games",
        'updated' : "06 Jun 2022",
        'App_name' : "The Ants: Underground Kingdom",
        'Package_name' : "com.star.union.planetant",
        'installs' : "10,000,000+",
        'version' : "1.24.0",
    },
    'br.com.zeenow.zeenow' :
    {
        'category' : "Shopping",
        'updated' : "11 Apr 2022",
        'App_name' : "Zee.Now - Pet Shop Delivery",
        'Package_name' : "br.com.zeenow.zeenow",
        'installs' : "100,000+",
        'version' : "1.37.0",
    },
    'com.nhnent.AFTERLIFE' :
    {
        'category' : "Games",
        'updated' : "24 Aug 2020",
        'App_name' : "AFTER L!FE: The Sacred Kaleidoscope",
        'Package_name' : "com.nhnent.AFTERLIFE",
        'installs' : "500,000+",
        'version' : "2.2.1",
    },
    'com.letyshops' :
    {
        'category' : "Shopping",
        'updated' : "16 Jun 2022",
        'App_name' : "LetyShops cashback service",
        'Package_name' : "com.letyshops",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    '1558331986' :
    {
        'category' : "Games",
        'updated' : "2022-05-26T22:15:17Z",
        'App_name' : "stc play",
        'Package_name' : "com.stc.xplay",
        'version' : "2.0.46",
        'minimumOsVersion' : "12.0",
    },
    'com.bukalapak.bukumitra' :
    {
        'category' : "Finance",
        'updated' : "18 Mar 2022",
        'App_name' : "BukuMitra: Catat Keuangan & Promosi Usaha",
        'Package_name' : "com.bukalapak.bukumitra",
        'installs' : "100,000+",
        'version' : "1.3.4",
    },
    '1367232195' :
    {
        'category' : "Games",
        'updated' : "2022-01-26T16:43:37Z",
        'App_name' : "Triple Sweet Puzzle: Match 3",
        'Package_name' : "net.leadid.TripleSweetPuzzle",
        'version' : "16.19.7",
        'minimumOsVersion' : "10.0",
    },
    '742186886' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-14T00:59:36Z",
        'App_name' : "\ub370\uc77c\ub9ac\ud638\ud154",
        'Package_name' : "kr.co.dailyhotel",
        'version' : "2.17.0",
        'minimumOsVersion' : "12.0",
    },
    'com.mylivn.android.app' :
    {
        'category' : "Social and Communication",
        'updated' : "04 Apr 2022",
        'App_name' : "mylivn",
        'Package_name' : "com.mylivn.android.app",
        'installs' : "100,000+",
        'version' : "4.0.6",
    },
    '1019478181' :
    {
        'category' : "Utilities",
        'updated' : "2022-06-14T07:13:13Z",
        'App_name' : "kWhapp \u2013 Strom & Gas Check",
        'Package_name' : "de.yellostrom.incontrol",
        'version' : "207.3.0",
        'minimumOsVersion' : "11.0",
    },
    'com.sivillage.du' :
    {
        'category' : "Shopping",
        'updated' : "23 May 2022",
        'App_name' : "DU - \ub514\uc790\uc778\uc744 \uc5f0\uacb0\ud558\ub2e4",
        'Package_name' : "com.sivillage.du",
        'installs' : "100,000+",
        'version' : "2.2",
    },
    'tw.sonet.princessconnect' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "\u8d85\u7570\u57df\u516c\u4e3b\u9023\u7d50\uff01Re:Dive",
        'Package_name' : "tw.sonet.princessconnect",
        'installs' : "500,000+",
        'version' : "3.3.0",
    },
    'com.nianticlabs.pokemongo' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Pok\u00e9mon GO",
        'Package_name' : "com.nianticlabs.pokemongo",
        'installs' : "100,000,000+",
        'version' : "0.239.2",
    },
    'com.locon.housing' :
    {
        'category' : "House & Home",
        'updated' : "10 Jun 2022",
        'App_name' : "Housing App: Buy, Rent, Sell Property & Pay Rent",
        'Package_name' : "com.locon.housing",
        'installs' : "10,000,000+",
        'version' : "13.2.2",
    },
    'jp.ravit.and' :
    {
        'category' : "Social and Communication",
        'updated' : "28 May 2022",
        'App_name' : "Ravit(\u30e9\u30d3\u30c3\u30c8)\u604b\u6d3b\u30fb\u5a5a\u6d3b\u30fb\u51fa\u4f1a\u3044\u63a2\u3057\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea",
        'Package_name' : "jp.ravit.and",
        'installs' : "100,000+",
        'version' : "2.3.6",
    },
    'kr.co.monoplatform.whistle' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "10 Jun 2022",
        'App_name' : "\ud1b5\ud569\uc8fc\uc815\ucc28\ub2e8\uc18d\uc54c\ub9bc - \ud718\uc2ac",
        'Package_name' : "kr.co.monoplatform.whistle",
        'installs' : "1,000,000+",
        'version' : "2.10.2",
    },
    'com.kayac.koshien_pocket' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\u307c\u304f\u3089\u306e\u7532\u5b50\u5712\uff01\u30dd\u30b1\u30c3\u30c8\u3000\u9ad8\u6821\u91ce\u7403\u30b2\u30fc\u30e0",
        'Package_name' : "com.kayac.koshien_pocket",
        'installs' : "1,000,000+",
        'version' : "8.11.0",
    },
    'com.etstur' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "01 Apr 2022",
        'App_name' : "Etstur - Otel Ara, Rezervasyon Yap",
        'Package_name' : "com.etstur",
        'installs' : "1,000,000+",
        'version' : "3.1.4",
    },
    'de.flixbus.app' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "04 Jun 2022",
        'App_name' : "FlixBus: Book Cheap Bus Tickets",
        'Package_name' : "de.flixbus.app",
        'installs' : "10,000,000+",
        'version' : "6.34.0",
    },
    'com.netease.ko' :
    {
        'category' : "Games",
        'updated' : "19 Apr 2022",
        'App_name' : "Knives Out",
        'Package_name' : "com.netease.ko",
        'installs' : "10,000,000+",
        'version' : "1.280.479406",
    },
    'com.rastargames.maplestory' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "\u6953\u4e4b\u8c37R\uff1a\u7d93\u5178\u65b0\u5b9a\u7fa9",
        'Package_name' : "com.rastargames.maplestory",
        'installs' : "100,000+",
        'version' : "1.0.25",
    },
    'com.ulugame.yongzhekr.google' :
    {
        'category' : "Games",
        'updated' : "20 Jan 2021",
        'App_name' : "\uc5d8\ub9ac\uba3c\ud2b89",
        'Package_name' : "com.ulugame.yongzhekr.google",
        'installs' : "50,000+",
        'version' : "1.0.6",
    },
    'com.pocketgems.chef' :
    {
        'category' : "Games",
        'updated' : "24 Nov 2021",
        'App_name' : "Adventure Chef: Merge Explorer",
        'Package_name' : "com.pocketgems.chef",
        'installs' : "10,000+",
        'version' : "2.22",
    },
    'com.osn.go' :
    {
        'category' : "Entertainment",
        'updated' : "31 May 2022",
        'App_name' : "OSN+ Streaming App",
        'Package_name' : "com.osn.go",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.ziplab.innocentwarrior' :
    {
        'category' : "Games",
        'updated' : "12 Jun 2020",
        'App_name' : "Innocent Warrior",
        'Package_name' : "com.ziplab.innocentwarrior",
        'installs' : "50,000+",
        'version' : "1.0.59",
    },
    '938095479' :
    {
        'category' : "Games",
        'updated' : "2022-06-09T05:49:21Z",
        'App_name' : "\u5922\u738b\u56fd\u3068\u7720\u308c\u308b100\u4eba\u306e\u738b\u5b50\u69d8",
        'Package_name' : "com.gcrest.prod.yume100",
        'version' : "5.19.0",
        'minimumOsVersion' : "9.0",
    },
    'com.skybet.app.itv7' :
    {
        'category' : "Games",
        'updated' : "01 Mar 2019",
        'App_name' : "ITV 7",
        'Package_name' : "com.skybet.app.itv7",
        'installs' : "100,000+",
        'version' : "1.1",
    },
    'com.GMA.Ball.Sort.Puzzle' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "Ball Sort - Color Puzzle Game",
        'Package_name' : "com.GMA.Ball.Sort.Puzzle",
        'installs' : "50,000,000+",
        'version' : "11.0.0",
    },
    'com.tianxi.vn.game' :
    {
        'category' : "Games",
        'updated' : "15 Feb 2022",
        'App_name' : "Th\u1ebf Gi\u1edbi Ma Hi\u1ec7p",
        'Package_name' : "com.tianxi.vn.game",
        'installs' : "10,000+",
        'version' : "1.0.1",
    },
    'com.next.innovation.takatak' :
    {
        'category' : "Social and Communication",
        'updated' : "06 Jun 2022",
        'App_name' : "MX TakaTak Short Video App",
        'Package_name' : "com.next.innovation.takatak",
        'installs' : "100,000,000+",
        'version' : "2.1.3",
    },
    '1494078394' :
    {
        'category' : "Finance",
        'updated' : "2022-06-20T00:26:21Z",
        'App_name' : "PriceLOCQ",
        'Package_name' : "com.pricelocq.mobile",
        'version' : "1.17.1",
        'minimumOsVersion' : "11.0",
    },
    'com.kitabisa.android' :
    {
        'category' : "Lifestyle",
        'updated' : "08 Jun 2022",
        'App_name' : "Kitabisa: Donasi, Zakat, Wakaf",
        'Package_name' : "com.kitabisa.android",
        'installs' : "1,000,000+",
        'version' : "4.39.1",
    },
    'jp.aocca.app' :
    {
        'category' : "Social and Communication",
        'updated' : "10 Jun 2022",
        'App_name' : "\u30a2\u30f3\u30b8\u30e5-\u5927\u4eba\u306e\u604b\u6d3b\u30fb\u5a5a\u6d3b\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea",
        'Package_name' : "jp.aocca.app",
        'installs' : "100,000+",
        'version' : "4.2.0",
    },
    'com.wsy.google.wansuiye.jp' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "\u30a2\u30a4\u30a2\u30e0\u7687\u5e1d",
        'Package_name' : "com.wsy.google.wansuiye.jp",
        'installs' : "500,000+",
        'version' : "3.8.0",
    },
    'com.carolgames.moemoegirls' :
    {
        'category' : "Games",
        'updated' : "05 Jan 2022",
        'App_name' : "Girls X Battle 2",
        'Package_name' : "com.carolgames.moemoegirls",
        'installs' : "10,000,000+",
        'version' : "1.5.248",
    },
    'com.ncsoft.tricksterm19' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "\ud2b8\ub9ad\uc2a4\ud130M",
        'Package_name' : "com.ncsoft.tricksterm19",
        'installs' : "500,000+",
        'version' : "1.0.67",
    },
    'com.redcell.goldandgoblins' :
    {
        'category' : "Games",
        'updated' : "21 May 2022",
        'App_name' : "Gold & Goblins: Idle Merger",
        'Package_name' : "com.redcell.goldandgoblins",
        'installs' : "10,000,000+",
        'version' : "1.18.2",
    },
    'com.picturerock.rock' :
    {
        'category' : "Lifestyle",
        'updated' : "10 Jun 2022",
        'App_name' : "Rock Identifier: Stone ID",
        'Package_name' : "com.picturerock.rock",
        'installs' : "1,000,000+",
        'version' : "2.3.0",
    },
    '928866584' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-20T07:02:23Z",
        'App_name' : "Almosafer: Travel & leisure",
        'Package_name' : "com.almosafer.travel",
        'version' : "7.42.0",
        'minimumOsVersion' : "14.0",
    },
    'com.tw.king' :
    {
        'category' : "Games",
        'updated' : "14 Oct 2021",
        'App_name' : "\u65e5\u7406\u842c\u59ec",
        'Package_name' : "com.tw.king",
        'installs' : "1,000,000+",
        'version' : "1.0.20",
    },
    'com.jdgames.p20n.googleplay' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "Sengoku Fubu",
        'Package_name' : "com.jdgames.p20n.googleplay",
        'installs' : "1,000,000+",
        'version' : "1.6.7102",
    },
    'com.webzen.muorigin2.google' :
    {
        'category' : "Games",
        'updated' : "24 May 2022",
        'App_name' : "\ubba4\uc624\ub9ac\uc9c42",
        'Package_name' : "com.webzen.muorigin2.google",
        'installs' : "500,000+",
        'version' : "10.1.0",
    },
    '988141624' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-04-13T08:12:09Z",
        'App_name' : "OYO: Search & Book Hotel Rooms",
        'Package_name' : "com.oyo.oyo-ios",
        'version' : "6.33",
        'minimumOsVersion' : "12.0",
    },
    '1488423517' :
    {
        'category' : "Games",
        'updated' : "2022-06-09T01:07:11Z",
        'App_name' : "\u4e8c\u30ce\u56fd\uff1aCross Worlds",
        'Package_name' : "com.netmarble.enn",
        'version' : "1.11.06",
        'minimumOsVersion' : "11.0",
    },
    'com.spaia_keiba' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "SPAIA\u7af6\u99ac\uff5eAI\u4e88\u60f3\u3068\u30c7\u30fc\u30bf\u89e3\u6790\u306b\u3088\u308b\u6b21\u4e16\u4ee3\u306e\u7af6\u99ac\u30a2\u30d7\u30ea",
        'Package_name' : "com.spaia_keiba",
        'installs' : "5,000+",
        'version' : "1.12.2",
    },
    'com.garena.game.ftmsm' :
    {
        'category' : "Games",
        'updated' : "13 May 2021",
        'App_name' : "FAIRY TAIL: Forces Unite!",
        'Package_name' : "com.garena.game.ftmsm",
        'installs' : "100,000+",
        'version' : "8.11.106",
    },
    'com.igg.android.lordsmobile' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "Lords Mobile: Tower Defense",
        'Package_name' : "com.igg.android.lordsmobile",
        'installs' : "100,000,000+",
        'version' : "2.82",
    },
    'com.parsifal.starz' :
    {
        'category' : "Entertainment",
        'updated' : "26 May 2022",
        'App_name' : "\u0633\u062a\u0627\u0631\u0632\u0628\u0644\u0627\u064a STARZPLAY",
        'Package_name' : "com.parsifal.starz",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.klab.captain283.global' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "Captain Tsubasa: Dream Team",
        'Package_name' : "com.klab.captain283.global",
        'installs' : "10,000,000+",
        'version' : "6.2.1",
    },
    'com.bp.oheadline' :
    {
        'category' : "Education and Books",
        'updated' : "09 Jun 2022",
        'App_name' : "\ud5e4\ub4dc\ub77c\uc787 - \uae54\ub054\ud558\uac8c \ub274\uc2a4\ub97c \ucb49\ucb49 \ubcf4\uc138\uc694",
        'Package_name' : "com.bp.oheadline",
        'installs' : "100,000+",
        'version' : "1.4.6",
    },
    'com.ulugames.sangoku.google' :
    {
        'category' : "Games",
        'updated' : "26 Mar 2020",
        'App_name' : "\u8c6a\u708e\u4e09\u56fd\u5fd7\uff5e\u8987\u738b\u306e\u7121\u53cc\u4e71\u6226\uff5e",
        'Package_name' : "com.ulugames.sangoku.google",
        'installs' : "10,000+",
        'version' : "1.0.5",
    },
    '1364197140' :
    {
        'category' : "Finance",
        'updated' : "2022-05-25T21:01:03Z",
        'App_name' : "stc pay",
        'Package_name' : "sa.com.stcpay",
        'version' : "1.10.10",
        'minimumOsVersion' : "11.0",
    },
    '1457692483' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-13T18:34:43Z",
        'App_name' : "nupal",
        'Package_name' : "com.companion-it.NuPal",
        'version' : "1.19",
        'minimumOsVersion' : "11.0",
    },
    'com.lottemart.shopping' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "\ub86f\ub370\ub9c8\ud2b8\ubab0 - \ub86f\ub370ON \ud558\ub098\uba74 \ub41c\ub2e4",
        'Package_name' : "com.lottemart.shopping",
        'installs' : "1,000,000+",
        'version' : "12.1.0",
    },
    'com.solotalk' :
    {
        'category' : "Entertainment",
        'updated' : "06 Jun 2022",
        'App_name' : "\u30d3\u30c7\u30aa\u901a\u8a71\u30fb\u30d3\u30c7\u30aa\u30c1\u30e3\u30c3\u30c8 moments\uff08\u30e2\u30fc\u30e1\u30f3\u30c4\uff09",
        'Package_name' : "com.solotalk",
        'installs' : "10,000+",
        'version' : "1.2.56",
    },
    'th.co.truemoney.wallet' :
    {
        'category' : "Finance",
        'updated' : "27 May 2022",
        'App_name' : "TrueMoney Wallet",
        'Package_name' : "th.co.truemoney.wallet",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.rapidnaira.fastloan' :
    {
        'category' : "Finance",
        'updated' : "28 Apr 2022",
        'App_name' : "RapidNaira",
        'Package_name' : "com.rapidnaira.fastloan",
        'installs' : "100,000+",
        'version' : "1.0.7",
    },
    'ru.litres.android.audio' :
    {
        'category' : "Education and Books",
        'updated' : "07 Jun 2022",
        'App_name' : "Listen",
        'Package_name' : "ru.litres.android.audio",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.coconala.android.portal' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "\u30b3\u30b3\u30ca\u30e9(coconala)\u30b9\u30ad\u30eb\u30de\u30fc\u30b1\u30c3\u30c8\u3067\u5f97\u610f\u3092\u58f2\u308a\u8cb7\u3044",
        'Package_name' : "com.coconala.android.portal",
        'installs' : "500,000+",
        'version' : "5.21.2",
    },
    'com.global.sl' :
    {
        'category' : "Games",
        'updated' : "24 Jun 2021",
        'App_name' : "Soul Land: Awaken Warsoul",
        'Package_name' : "com.global.sl",
        'installs' : "1,000,000+",
        'version' : "40.0",
    },
    'com.sivillage.beauty' :
    {
        'category' : "Shopping",
        'updated' : "26 May 2022",
        'App_name' : "S.I.BEAUTY - \ud65c\ub3d9\uc774 \ud61c\ud0dd\uc774 \ub418\ub294 \ub7ed\uc154\ub9ac \ubdf0\ud2f0",
        'Package_name' : "com.sivillage.beauty",
        'installs' : "50,000+",
        'version' : "2.7",
    },
    'com.global.ztmslgtw' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "\u672b\u65e5\u55a7\u56c2",
        'Package_name' : "com.global.ztmslgtw",
        'installs' : "500,000+",
        'version' : "7.0.73",
    },
    'com.HeroGames.WoodShop' :
    {
        'category' : "Games",
        'updated' : "18 Mar 2022",
        'App_name' : "Wood Shop",
        'Package_name' : "com.HeroGames.WoodShop",
        'installs' : "10,000,000+",
        'version' : "3.0.0",
    },
    'com.eyecon.global' :
    {
        'category' : "Social and Communication",
        'updated' : "08 May 2022",
        'App_name' : "Eyecon Caller ID & Spam Block",
        'Package_name' : "com.eyecon.global",
        'installs' : "50,000,000+",
        'version' : "3.0.407",
    },
    'tv.every.delishkitchen' :
    {
        'category' : "Food & Drink",
        'updated' : "14 Jun 2022",
        'App_name' : "DELISH KITCHEN-\u30ec\u30b7\u30d4\u52d5\u753b\u3067\u6599\u7406\u3092\u697d\u3057\u304f\u7c21\u5358\u306b",
        'Package_name' : "tv.every.delishkitchen",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.snkrdunk.android' :
    {
        'category' : "Shopping",
        'updated' : "31 May 2022",
        'App_name' : "\u30b9\u30cb\u30fc\u30ab\u30fc\u30c0\u30f3\u30af \u30b9\u30cb\u30fc\u30ab\u30fc&\u30cf\u30a4\u30d6\u30e9\u30f3\u30c9\u30d5\u30ea\u30de\u30a2\u30d7\u30ea",
        'Package_name' : "com.snkrdunk.android",
        'installs' : "100,000+",
        'version' : "3.1.11",
    },
    'com.doubtnutapp' :
    {
        'category' : "Education and Books",
        'updated' : "17 Jun 2022",
        'App_name' : "Doubtnut: NCERT, IIT JEE, NEET",
        'Package_name' : "com.doubtnutapp",
        'installs' : "50,000,000+",
        'version' : "7.9.74",
    },
    'com.netease.lagrange' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "Infinite Lagrange",
        'Package_name' : "com.netease.lagrange",
        'installs' : "1,000,000+",
        'version' : "1.1.179416",
    },
    'com.espritgames.rpg.eternal.sword.m' :
    {
        'category' : "Games",
        'updated' : "23 Dec 2021",
        'App_name' : "Eternal Sword",
        'Package_name' : "com.espritgames.rpg.eternal.sword.m",
        'installs' : "100,000+",
        'version' : "1.9.3",
    },
    'jp.boikone' :
    {
        'category' : "Entertainment",
        'updated' : "04 May 2022",
        'App_name' : "\u30dc\u30a4\u30b3\u30cd - \u58f0\u5287\u30e9\u30a4\u30d6\u914d\u4fe1\u30a2\u30d7\u30ea",
        'Package_name' : "jp.boikone",
        'installs' : "100,000+",
        'version' : "2.15.0",
    },
    'com.careem.acma' :
    {
        'category' : "Utilities",
        'updated' : "16 Jun 2022",
        'App_name' : "Careem",
        'Package_name' : "com.careem.acma",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    '1288441708' :
    {
        'category' : "Games",
        'updated' : "2022-05-05T21:14:00Z",
        'App_name' : "PointsBet - Online Betting",
        'Package_name' : "com.pointsbet.app",
        'version' : "3.0.0",
        'minimumOsVersion' : "10.3",
    },
    '1263365153' :
    {
        'category' : "Games",
        'updated' : "2022-05-23T01:37:00Z",
        'App_name' : "\u30a8\u30d0\u30fc\u30c6\u30a4\u30eb",
        'Package_name' : "com.zigzagame.evertale",
        'version' : "2.0.63",
        'minimumOsVersion' : "11.0",
    },
    'de.nivea.nivea' :
    {
        'category' : "Social and Communication",
        'updated' : "03 Mar 2022",
        'App_name' : "NIVEA App",
        'Package_name' : "de.nivea.nivea",
        'installs' : "100,000+",
        'version' : "3.7.0",
    },
    'com.com2us.slroh.normal.freefull.google.global.android.common' :
    {
        'category' : "Games",
        'updated' : "17 Nov 2021",
        'App_name' : "Skylanders\u2122 Ring of Heroes",
        'Package_name' : "com.com2us.slroh.normal.freefull.google.global.android.common",
        'installs' : "1,000,000+",
        'version' : "2.0.12",
    },
    'com.npixel.GranSaga' :
    {
        'category' : "Games",
        'updated' : "17 May 2022",
        'App_name' : "\uadf8\ub791\uc0ac\uac00",
        'Package_name' : "com.npixel.GranSaga",
        'installs' : "1,000,000+",
        'version' : "2.8.1",
    },
    'com.mask.netburn' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2021",
        'App_name' : "\u30cd\u30c3\u30c8\u708e\u4e0a\u307f\u3063\u3051\uff01 \u3000 \u6687\u3064\u3076\u3057\u30b2\u30fc\u30e0\u30a2\u30d7\u30ea",
        'Package_name' : "com.mask.netburn",
        'installs' : "100,000+",
        'version' : "1.0.7",
    },
    '1503482896' :
    {
        'category' : "Finance",
        'updated' : "2022-06-07T06:52:18Z",
        'App_name' : "BtcTurk | Bitcoin Al\u0131m Sat\u0131m\u0131",
        'Package_name' : "com.mobillium.btcturk",
        'version' : "1.18.1",
        'minimumOsVersion' : "11.0",
    },
    'com.survjun' :
    {
        'category' : "Lifestyle",
        'updated' : "13 Jun 2022",
        'App_name' : "Survey Junkie",
        'Package_name' : "com.survjun",
        'installs' : "1,000,000+",
        'version' : "1.18.5",
    },
    'in.bajajfinservmarkets.app' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "Bajaj MARKETS: Loan, Card, UPI",
        'Package_name' : "in.bajajfinservmarkets.app",
        'installs' : "5,000,000+",
        'version' : "5.0.17",
    },
    '1164785360' :
    {
        'category' : "Games",
        'updated' : "2022-05-26T02:44:16Z",
        'App_name' : "\u30de\u30ae\u30a2\u30ec\u30b3\u30fc\u30c9 \u9b54\u6cd5\u5c11\u5973\u307e\u3069\u304b\u30de\u30ae\u30ab\u5916\u4f1d",
        'Package_name' : "com.aniplex.magireco",
        'version' : "2.5.1",
        'minimumOsVersion' : "9.0",
    },
    '545599256' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-16T20:17:19Z",
        'App_name' : "Instacart: Grocery delivery",
        'Package_name' : "com.instacart",
        'version' : "7.129.2",
        'minimumOsVersion' : "13.0",
    },
    '1471506070' :
    {
        'category' : "Finance",
        'updated' : "2022-02-26T05:49:07Z",
        'App_name' : "StormGain: Bitcoin Wallet App",
        'Package_name' : "com.stormgain.mobile",
        'version' : "1.23.0",
        'minimumOsVersion' : "10.0",
    },
    'com.allstarunion.ta.jp' :
    {
        'category' : "Games",
        'updated' : "06 Jun 2022",
        'App_name' : "\u30b6\u30fb\u30a2\u30f3\u30c4\uff1a\u30a2\u30f3\u30c0\u30fc\u30b0\u30e9\u30a6\u30f3\u30c9 \u30ad\u30f3\u30b0\u30c0\u30e0",
        'Package_name' : "com.allstarunion.ta.jp",
        'installs' : "500,000+",
        'version' : "1.24.0",
    },
    '1535907379' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-05T16:10:38Z",
        'App_name' : "Zipcar T\u00fcrkiye",
        'Package_name' : "com.zipcar.tr",
        'version' : "3.6",
        'minimumOsVersion' : "10.0",
    },
    'kr.com.efun.kok.google' :
    {
        'category' : "Games",
        'updated' : "22 Dec 2020",
        'App_name' : "\uc624\ud06c: \uc885\uc871\uc758 \uacc4\uc2b9\uc790",
        'Package_name' : "kr.com.efun.kok.google",
        'installs' : "500,000+",
        'version' : "3.4.1",
    },
    'co.picap.passenger' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "15 Jun 2022",
        'App_name' : "Picap",
        'Package_name' : "co.picap.passenger",
        'installs' : "5,000,000+",
        'version' : "4.9.6",
    },
    'au.com.bws' :
    {
        'category' : "Food & Drink",
        'updated' : "01 Apr 2022",
        'App_name' : "BWS on tAPP",
        'Package_name' : "au.com.bws",
        'installs' : "500,000+",
        'version' : "3.6",
    },
    'com.snapfish.mobile' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "Snapfish: Prints + Photo Books",
        'Package_name' : "com.snapfish.mobile",
        'installs' : "5,000,000+",
        'version' : "13.10.0",
    },
    'com.loco_partners.relux' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "30 May 2022",
        'App_name' : "Relux",
        'Package_name' : "com.loco_partners.relux",
        'installs' : "500,000+",
        'version' : "3.49.0",
    },
    'tw.com.iwplay.xxa' :
    {
        'category' : "Games",
        'updated' : "22 Mar 2022",
        'App_name' : "\u65b0\u7b11\u50b2\u6c5f\u6e56M",
        'Package_name' : "tw.com.iwplay.xxa",
        'installs' : "100,000+",
        'version' : "1.0.141",
    },
    'com.eskyfun.kingdoms' :
    {
        'category' : "Games",
        'updated' : "25 Jan 2022",
        'App_name' : "\u795e\u9b54\u4e09\u570b\u5fd7",
        'Package_name' : "com.eskyfun.kingdoms",
        'installs' : "100,000+",
        'version' : "1.1.9",
    },
    'tv.abema' :
    {
        'category' : "Entertainment",
        'updated' : "15 Jun 2022",
        'App_name' : "ABEMA\uff08\u30a2\u30d9\u30de\uff09\u65b0\u3057\u3044\u672a\u6765\u306e\u30c6\u30ec\u30d3",
        'Package_name' : "tv.abema",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.nexon.kart' :
    {
        'category' : "Games",
        'updated' : "04 May 2022",
        'App_name' : "KartRider Rush+",
        'Package_name' : "com.nexon.kart",
        'installs' : "10,000,000+",
        'version' : "1.13.8",
    },
    '979091969' :
    {
        'category' : "Games",
        'updated' : "2022-05-16T02:00:13Z",
        'App_name' : "\u30a4\u30b1\u30e1\u30f3\u6226\u56fd \u6642\u3092\u304b\u3051\u308b\u604b \u5973\u6027\u5411\u3051\u306e\u604b\u611b\u30b2\u30fc\u30e0\u30fb\u4e59\u5973\u30b2\u30fc\u30e0",
        'Package_name' : "jp.co.cybird.appli.sgk",
        'version' : "1.7.4",
        'minimumOsVersion' : "12.3",
    },
    '1166835724' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-01-17T03:44:20Z",
        'App_name' : "\uce74\ud50c\ub7ab - \uc9d1 \uc55e\uc5d0\uc11c \ud0c0\ub294 \ub80c\ud2b8\uce74(\ub80c\ud130\uce74)",
        'Package_name' : "kr.co.plat.carplat",
        'version' : "6.1.14",
        'minimumOsVersion' : "13.0",
    },
    '1434519042' :
    {
        'category' : "Games",
        'updated' : "2022-06-08T22:47:26Z",
        'App_name' : "\u30e1\u30a4\u30d7\u30eb\u30b9\u30c8\u30fc\u30ea\u30fc\uff2d",
        'Package_name' : "com.nexon.maplem.japan",
        'version' : "1.780.3125",
        'minimumOsVersion' : "10.0",
    },
    'com.fondeadora.bank' :
    {
        'category' : "Finance",
        'updated' : "14 May 2022",
        'App_name' : "Fondeadora: Cuenta & Tarjeta",
        'Package_name' : "com.fondeadora.bank",
        'installs' : "1,000,000+",
        'version' : "3.5.4",
    },
    'ua.com.rozetka.shop' :
    {
        'category' : "Shopping",
        'updated' : "03 Jun 2022",
        'App_name' : "ROZETKA \u2014 Online marketplace",
        'Package_name' : "ua.com.rozetka.shop",
        'installs' : "5,000,000+",
        'version' : "5.30.1",
    },
    'com.mobile.justmop' :
    {
        'category' : "House & Home",
        'updated' : "14 Jun 2022",
        'App_name' : "Justlife (Justmop)",
        'Package_name' : "com.mobile.justmop",
        'installs' : "100,000+",
        'version' : "6.27.0",
    },
    'com.yaari' :
    {
        'category' : "Shopping",
        'updated' : "22 Jan 2022",
        'App_name' : "Yaari: Online Shopping App",
        'Package_name' : "com.yaari",
        'installs' : "5,000,000+",
        'version' : "1.2.4",
    },
    'com.linecorp.LGSHOP' :
    {
        'category' : "Games",
        'updated' : "15 Jul 2021",
        'App_name' : "LINE: Pixar Tower",
        'Package_name' : "com.linecorp.LGSHOP",
        'installs' : "1,000,000+",
        'version' : "1.7.0",
    },
    'com.aaptiv.android' :
    {
        'category' : "Health and Fitness",
        'updated' : "24 May 2022",
        'App_name' : "Aaptiv: Fitness for Everyone",
        'Package_name' : "com.aaptiv.android",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.nolbal.nolbal' :
    {
        'category' : "Health and Fitness",
        'updated' : "13 Jun 2022",
        'App_name' : "\ub180\uc774\uc758\ubc1c\uacac - 110\ub9cc \uc5c4\ub9c8\uc544\ube60\uc758 \ud0a4\uc988\ub180\uc774\uc571",
        'Package_name' : "com.nolbal.nolbal",
        'installs' : "1,000,000+",
        'version' : "2.21.4",
    },
    'com.netmarble.lin2ws' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "Lineage 2: Revolution",
        'Package_name' : "com.netmarble.lin2ws",
        'installs' : "5,000,000+",
        'version' : "1.34.12",
    },
    'com.indentcorp.spray' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "Spray - Swipe, Buy & Review",
        'Package_name' : "com.indentcorp.spray",
        'installs' : "1,000+",
        'version' : "1.12.0",
    },
    'com.lanterns.btaskee' :
    {
        'category' : "Lifestyle",
        'updated' : "12 May 2022",
        'App_name' : "btaskee - Cleaning Services",
        'Package_name' : "com.lanterns.btaskee",
        'installs' : "100,000+",
        'version' : "3.4.0",
    },
    'com.bilibilijp.finalgear' :
    {
        'category' : "Games",
        'updated' : "22 Mar 2022",
        'App_name' : "\u30d5\u30a1\u30a4\u30ca\u30eb\u30ae\u30a2-\u91cd\u88c5\u6226\u59eb-",
        'Package_name' : "com.bilibilijp.finalgear",
        'installs' : "100,000+",
        'version' : "1.31.0",
    },
    'vn.moneycat' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "MoneyCat.vn - 0% l\u00e3i su\u1ea5t",
        'Package_name' : "vn.moneycat",
        'installs' : "100,000+",
        'version' : "2.1.4",
    },
    'com.kakaogames.gdts' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Guardian Tales",
        'Package_name' : "com.kakaogames.gdts",
        'installs' : "5,000,000+",
        'version' : "2.44.0",
    },
    'com.mulherespositivas.app' :
    {
        'category' : "Lifestyle",
        'updated' : "15 Jun 2022",
        'App_name' : "Mulheres Positivas",
        'Package_name' : "com.mulherespositivas.app",
        'installs' : "100,000+",
        'version' : "4.2.0",
    },
    'com.ggt.kingc.aos' :
    {
        'category' : "Games",
        'updated' : "17 Jan 2022",
        'App_name' : "THE LORD",
        'Package_name' : "com.ggt.kingc.aos",
        'installs' : "100,000+",
        'version' : "1.0.1",
    },
    'com.positive_apps.buzz_car.prod' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "04 Apr 2022",
        'App_name' : "Share - Israel Car Sharing",
        'Package_name' : "com.positive_apps.buzz_car.prod",
        'installs' : "100,000+",
        'version' : "6.0.0",
    },
    'il.co.yellow.app' :
    {
        'category' : "Finance",
        'updated' : "18 May 2022",
        'App_name' : "\u05d4\u05d0\u05e8\u05e0\u05e7 \u05d4\u05d3\u05d9\u05d2\u05d9\u05d8\u05dc\u05d9 \u05e9\u05dc \u05e4\u05d6 yellow",
        'Package_name' : "il.co.yellow.app",
        'installs' : "500,000+",
        'version' : "4.9.0.516",
    },
    '1456569943' :
    {
        'category' : "Games",
        'updated' : "2022-05-27T09:08:44Z",
        'App_name' : "LINE \u30b7\u30a7\u30d5",
        'Package_name' : "com.linecorp.LGCHEF",
        'version' : "1.19.0",
        'minimumOsVersion' : "10.0",
    },
    'it.subito' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "Subito: compra e vendi usato",
        'Package_name' : "it.subito",
        'installs' : "10,000,000+",
        'version' : "6.21.1",
    },
    'com.viacom.betplus' :
    {
        'category' : "Entertainment",
        'updated' : "09 Jun 2022",
        'App_name' : "BET+",
        'Package_name' : "com.viacom.betplus",
        'installs' : "1,000,000+",
        'version' : "109.102.0",
    },
    'com.fiverr.fiverr' :
    {
        'category' : "Finance",
        'updated' : "01 Jun 2022",
        'App_name' : "Fiverr - Freelance Service",
        'Package_name' : "com.fiverr.fiverr",
        'installs' : "10,000,000+",
        'version' : "3.5.6.2",
    },
    'com.gurutrade7.app' :
    {
        'category' : "Finance",
        'updated' : "17 Jun 2022",
        'App_name' : "Guru Trade7 Pro-Online trading",
        'Package_name' : "com.gurutrade7.app",
        'installs' : "5,000,000+",
        'version' : "1.9.0.1",
    },
    'com.privatesportshop.app.prod' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "Private Sport Shop",
        'Package_name' : "com.privatesportshop.app.prod",
        'installs' : "1,000,000+",
        'version' : "2.8.20",
    },
    'com.lionsgateplay.videoapp' :
    {
        'category' : "Entertainment",
        'updated' : "08 Jun 2022",
        'App_name' : "Lionsgate Play: Watch Movies, TV Shows, Web Series",
        'Package_name' : "com.lionsgateplay.videoapp",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.planetart.fpit' :
    {
        'category' : "Entertainment",
        'updated' : "11 Jun 2022",
        'App_name' : "FreePrints",
        'Package_name' : "com.planetart.fpit",
        'installs' : "1,000,000+",
        'version' : "3.55.0",
    },
    'com.mioto.mioto' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "17 Jun 2022",
        'App_name' : "MIOTO - Car rental app",
        'Package_name' : "com.mioto.mioto",
        'installs' : "100,000+",
        'version' : "3.2.3",
    },
    '916869395' :
    {
        'category' : "Games",
        'updated' : "2022-06-13T15:21:55Z",
        'App_name' : "Wizard of Oz Slots Games",
        'Package_name' : "com.zynga.wizardofoz",
        'version' : "185.0.452",
        'minimumOsVersion' : "10.0",
    },
    'com.ebay.kleinanzeigen' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "eBay Kleinanzeigen Marketplace",
        'Package_name' : "com.ebay.kleinanzeigen",
        'installs' : "10,000,000+",
        'version' : "13.19.0",
    },
    'com.planetart.fpin' :
    {
        'category' : "Entertainment",
        'updated' : "19 May 2020",
        'App_name' : "FreePrints \u2013 Free Photos Delivered",
        'Package_name' : "com.planetart.fpin",
        'installs' : "1,000,000+",
        'version' : "2.1.15",
    },
    'com.cct24.cc' :
    {
        'category' : "Social and Communication",
        'updated' : "16 Oct 2019",
        'App_name' : "Cherry Chat Pro \u2013 Flirt and more",
        'Package_name' : "com.cct24.cc",
        'installs' : "100,000+",
        'version' : "2.3.1.3",
    },
    'com.shg.sg301' :
    {
        'category' : "Games",
        'updated' : "01 Jun 2021",
        'App_name' : "Blood Chaos M - H\u1ed7n Huy\u1ebft Mobile",
        'Package_name' : "com.shg.sg301",
        'installs' : "50,000+",
        'version' : "4.7030.22273",
    },
    'com.lgbt_cg' :
    {
        'category' : "Social and Communication",
        'updated' : "10 Aug 2021",
        'App_name' : "LGBT\u30ab\u30e0 -SIDE GIRLS-",
        'Package_name' : "com.lgbt_cg",
        'installs' : "1,000+",
        'version' : "1.0.4",
    },
    'appcent.mobi.waterboyandroid' :
    {
        'category' : "Food & Drink",
        'updated' : "15 Jun 2022",
        'App_name' : "BiSU - Mobile water ordering",
        'Package_name' : "appcent.mobi.waterboyandroid",
        'installs' : "1,000,000+",
        'version' : "8.0.3",
    },
    'com.digging.qtk.kr.gp' :
    {
        'category' : "Games",
        'updated' : "18 Nov 2021",
        'App_name' : "\ubc30\ud2c0 \uc0bc\uad6d\uc9c0",
        'Package_name' : "com.digging.qtk.kr.gp",
        'installs' : "10,000+",
        'version' : "1.2.1",
    },
    'com.arcadegame.games.bingo.holiday.free.slots.bash' :
    {
        'category' : "Casino",
        'updated' : "24 May 2022",
        'App_name' : "Bingo Holiday: Bingo Games",
        'Package_name' : "com.arcadegame.games.bingo.holiday.free.slots.bash",
        'installs' : "5,000,000+",
        'version' : "1.9.50",
    },
    '1214223596' :
    {
        'category' : "Games",
        'updated' : "2022-06-15T22:23:46Z",
        'App_name' : "Word Trip - Word Puzzles Games",
        'Package_name' : "com.littleengine.wordtreat",
        'version' : "1.330.0",
        'minimumOsVersion' : "10.0",
    },
    'com.policybazaar' :
    {
        'category' : "Finance",
        'updated' : "11 May 2022",
        'App_name' : "Compare & Buy Insurance \u2013 Policybazaar",
        'Package_name' : "com.policybazaar",
        'installs' : "10,000,000+",
        'version' : "4.1.8",
    },
    'com.inbea' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Dec 2021",
        'App_name' : "Inbea",
        'Package_name' : "com.inbea",
        'installs' : "50,000+",
        'version' : "1.12.24",
    },
    'com.gotgl.jp' :
    {
        'category' : "Games",
        'updated' : "05 Dec 2021",
        'App_name' : "\u30b2\u30fc\u30e0\u30fb\u30aa\u30d6\u30fb\u30b9\u30ed\u30fc\u30f3\u30ba-\u51ac\u6765\u305f\u308b",
        'Package_name' : "com.gotgl.jp",
        'installs' : "50,000+",
        'version' : "2.6.12011102",
    },
    '693137280' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-10T08:03:52Z",
        'App_name' : "Heetch - Ride-hailing app",
        'Package_name' : "com.heetch.Heetch",
        'version' : "5.53.0",
        'minimumOsVersion' : "12.1",
    },
    '1372625336' :
    {
        'category' : "Games",
        'updated' : "2022-05-27T06:09:51Z",
        'App_name' : "War Eternal",
        'Package_name' : "com.onemt.wareternal",
        'version' : "1.0.106",
        'minimumOsVersion' : "9.0",
    },
    'com.kr.krsd.google' :
    {
        'category' : "Games",
        'updated' : "28 Oct 2021",
        'App_name' : "\ubb34\ud611\uc9c0\uc874: \uc601\uc6c5\ubb38",
        'Package_name' : "com.kr.krsd.google",
        'installs' : "100,000+",
        'version' : "1.1.3",
    },
    'com.app99.driver' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "99 Motorista: Renda Extra",
        'Package_name' : "com.app99.driver",
        'installs' : "10,000,000+",
        'version' : "7.6.54",
    },
    'jp.wowma.app' :
    {
        'category' : "Shopping",
        'updated' : "24 May 2022",
        'App_name' : "au PAY \u30de\u30fc\u30b1\u30c3\u30c8\u3000\u30dd\u30a4\u30f3\u30c8\u304c\u305f\u307e\u308b\u30b7\u30e7\u30c3\u30d4\u30f3\u30b0\u30a2\u30d7\u30ea",
        'Package_name' : "jp.wowma.app",
        'installs' : "5,000,000+",
        'version' : "5.58.1",
    },
    '1483222663' :
    {
        'category' : "Games",
        'updated' : "2022-05-28T07:40:52Z",
        'App_name' : "Words of Wonders: Search",
        'Package_name' : "com.fugo.wowsearch",
        'version' : "2.5.8",
        'minimumOsVersion' : "10.0",
    },
    'com.myglamm.ecommerce' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "MyGlamm: Makeup Shopping App",
        'Package_name' : "com.myglamm.ecommerce",
        'installs' : "5,000,000+",
        'version' : "2.33.3",
    },
    'com.biu.bardaat' :
    {
        'category' : "Education and Books",
        'updated' : "03 Mar 2022",
        'App_name' : "\u05d1\u05e8-\u05d3\u05e2\u05ea: \u05d4\u05e4\u05d5\u05d3\u05e7\u05d0\u05e1\u05d8\u05d9\u05dd \u05e9\u05dc \u05d0\u05d5\u05e0\u05d9\u05d1\u05e8\u05e1\u05d9\u05d8\u05ea \u05d1\u05e8-\u05d0\u05d9\u05dc\u05df",
        'Package_name' : "com.biu.bardaat",
        'installs' : "50,000+",
        'version' : "2.1.0",
    },
    'com.b2w.shoptime' :
    {
        'category' : "Shopping",
        'updated' : "03 May 2022",
        'App_name' : "Shoptime: Compras online",
        'Package_name' : "com.b2w.shoptime",
        'installs' : "10,000,000+",
        'version' : "3.84.2",
    },
    'com.rentomojo' :
    {
        'category' : "Lifestyle",
        'updated' : "28 May 2022",
        'App_name' : "RentoMojo: Products on Rent",
        'Package_name' : "com.rentomojo",
        'installs' : "1,000,000+",
        'version' : "3.7.6",
    },
    '647214190' :
    {
        'category' : "Entertainment",
        'updated' : "2022-06-07T14:56:44Z",
        'App_name' : "BluTV",
        'Package_name' : "com.dsmart.D-Smart-BLU-iPhone",
        'version' : "6.9.4",
        'minimumOsVersion' : "11.2",
    },
    'com.miHoYo.bh3korea' :
    {
        'category' : "Games",
        'updated' : "13 May 2022",
        'App_name' : "\ubd95\uad343rd",
        'Package_name' : "com.miHoYo.bh3korea",
        'installs' : "1,000,000+",
        'version' : "5.7.0",
    },
    'io.gonative.android.zpxkli.debug' :
    {
        'category' : "Shopping",
        'updated' : "18 May 2022",
        'App_name' : "VogaCloset",
        'Package_name' : "io.gonative.android.zpxkli.debug",
        'installs' : "1,000,000+",
        'version' : "3.1.0",
    },
    'com.nsmobilehub' :
    {
        'category' : "Shopping",
        'updated' : "07 Jun 2022",
        'App_name' : "NS\ud648\uc1fc\ud551",
        'Package_name' : "com.nsmobilehub",
        'installs' : "10,000,000+",
        'version' : "3.4.4",
    },
    'com.watsons.id.android' :
    {
        'category' : "Shopping",
        'updated' : "16 Jun 2022",
        'App_name' : "Watsons ID",
        'Package_name' : "com.watsons.id.android",
        'installs' : "1,000,000+",
        'version' : "10.640.1",
    },
    'it.wescount.cwallet' :
    {
        'category' : "Shopping",
        'updated' : "01 Feb 2022",
        'App_name' : "WeScount: sconti e rimborsi su grandi marche",
        'Package_name' : "it.wescount.cwallet",
        'installs' : "100,000+",
        'version' : "4.0.9",
    },
    'com.ssk.android' :
    {
        'category' : "Health and Fitness",
        'updated' : "02 Mar 2022",
        'App_name' : "Simple Soulful: Yoga & Fitness",
        'Package_name' : "com.ssk.android",
        'installs' : "1,000,000+",
        'version' : "1.5.30",
    },
    'com.hamc.android.pine' :
    {
        'category' : "Finance",
        'updated' : "28 Apr 2022",
        'App_name' : "PINE \u2013 \ub098\uc758 \ud22c\uc790 \uba54\uc774\ud2b8",
        'Package_name' : "com.hamc.android.pine",
        'installs' : "100,000+",
        'version' : "1.1.16",
    },
    'com.badambiz.saubaloot' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "Tarbi3ah Baloot \u2013 Arabic game",
        'Package_name' : "com.badambiz.saubaloot",
        'installs' : "1,000,000+",
        'version' : "1.176.0",
    },
    '1198503948' :
    {
        'category' : "Utilities",
        'updated' : "2022-06-01T11:04:12Z",
        'App_name' : "Virgin Mobile UAE",
        'Package_name' : "ae.virginmobile.virginmobileuae",
        'version' : "2.48.1",
        'minimumOsVersion' : "14.0",
    },
    'jp.co.atm.smile' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "\u5c11\u5973 \u6b4c\u5287 \u30ec\u30f4\u30e5\u30fc\u30b9\u30bf\u30a1\u30e9\u30a4\u30c8 -Re LIVE-",
        'Package_name' : "jp.co.atm.smile",
        'installs' : "100,000+",
        'version' : "1.0.50",
    },
    'com.uncosoft.highheels' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "High Heels!",
        'Package_name' : "com.uncosoft.highheels",
        'installs' : "100,000,000+",
        'version' : "3.4.9",
    },
    'de.clark.app' :
    {
        'category' : "Finance",
        'updated' : "21 Jan 2022",
        'App_name' : "CLARK - Versicherungen managen",
        'Package_name' : "de.clark.app",
        'installs' : "1,000,000+",
        'version' : "3.5.10",
    },
    'com.toretane' :
    {
        'category' : "Games",
        'updated' : "27 May 2021",
        'App_name' : "toretane-crane\uff08ONLINE CRANE GAME\uff09",
        'Package_name' : "com.toretane",
        'installs' : "100,000+",
        'version' : "3.1.1",
    },
    '1417738826' :
    {
        'category' : "Games",
        'updated' : "2022-05-26T01:11:52Z",
        'App_name' : "A3: \uc2a4\ud2f8\uc5bc\ub77c\uc774\ube0c",
        'Package_name' : "com.netmarble.survival",
        'version' : "1.19.41",
        'minimumOsVersion' : "10.0",
    },
    'com.boloid.dominos' :
    {
        'category' : "Food & Drink",
        'updated' : "31 May 2022",
        'App_name' : "Domino\u2019s -35% \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u043f\u0438\u0446\u0446\u044b",
        'Package_name' : "com.boloid.dominos",
        'installs' : "1,000,000+",
        'version' : "6.1.1",
    },
    'com.amazon.sellermobile.android' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Amazon Seller",
        'Package_name' : "com.amazon.sellermobile.android",
        'installs' : "10,000,000+",
        'version' : "7.12.1",
    },
    'cm.confide.android' :
    {
        'category' : "Finance",
        'updated' : "16 Feb 2022",
        'App_name' : "Confide - secure messenger",
        'Package_name' : "cm.confide.android",
        'installs' : "1,000,000+",
        'version' : "7.1.0",
    },
    'com.mkbanana.gp' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Meta Apes",
        'Package_name' : "com.mkbanana.gp",
        'installs' : "100,000+",
        'version' : "0.66.0",
    },
    'com.wajual' :
    {
        'category' : "Shopping",
        'updated' : "17 Jun 2022",
        'App_name' : "KitaBeli - Belanja Kebutuhan & Produk Segar Online",
        'Package_name' : "com.wajual",
        'installs' : "500,000+",
        'version' : "14.8.1",
    },
    'com.YoStarEN.Arknights' :
    {
        'category' : "Games",
        'updated' : "24 Apr 2022",
        'App_name' : "Arknights",
        'Package_name' : "com.YoStarEN.Arknights",
        'installs' : "1,000,000+",
        'version' : "9.0.01",
    },
    'it.q8.app.clubq8' :
    {
        'category' : "Lifestyle",
        'updated' : "13 Jun 2022",
        'App_name' : "Club Q8: A New way to refuel",
        'Package_name' : "it.q8.app.clubq8",
        'installs' : "500,000+",
        'version' : "1.25.0",
    },
    '1282966364' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-15T09:26:56Z",
        'App_name' : "Taimi: LGBTQ+ Dating und Chat",
        'Package_name' : "com.taimi.ios",
        'version' : "5.1.175",
        'minimumOsVersion' : "12.0",
    },
    'bubbleshooter.orig' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "Bubble Shooter",
        'Package_name' : "bubbleshooter.orig",
        'installs' : "100,000,000+",
        'version' : "14.1.7",
    },
    '1490054892' :
    {
        'category' : "Games",
        'updated' : "2022-01-17T02:18:24Z",
        'App_name' : "\ubba4 \uc544\ud06c\uc5d4\uc824",
        'Package_name' : "com.webzen.mua.ios",
        'version' : "1.39.01",
        'minimumOsVersion' : "9.0",
    },
    'vng.game.gunny.mobi.classic.original' :
    {
        'category' : "Games",
        'updated' : "03 Apr 2022",
        'App_name' : "Gunny Origin",
        'Package_name' : "vng.game.gunny.mobi.classic.original",
        'installs' : "500,000+",
        'version' : "1.1.19",
    },
    'com.neowiz.games.sudda' :
    {
        'category' : "Games",
        'updated' : "19 May 2022",
        'App_name' : "\ud53c\ub9dd \uc12f\ub2e4",
        'Package_name' : "com.neowiz.games.sudda",
        'installs' : "1,000,000+",
        'version' : "64.0",
    },
    'com.cgs.MemorialRecord.jp' :
    {
        'category' : "Games",
        'updated' : "12 Feb 2020",
        'App_name' : "\u30e1\u30e2\u30ea\u30a2\u30eb\u30ec\u30b3\u30fc\u30c9",
        'Package_name' : "com.cgs.MemorialRecord.jp",
        'installs' : "10,000+",
        'version' : "1.095",
    },
    '574794938' :
    {
        'category' : "Health and Fitness",
        'updated' : "2022-06-08T20:54:57Z",
        'App_name' : "Tecnonutri: Encontre sua dieta",
        'Package_name' : "br.com.tecnonutri.app",
        'version' : "4.7.80",
        'minimumOsVersion' : "11.0",
    },
    '1511037528' :
    {
        'category' : "Education and Books",
        'updated' : "2022-06-08T16:39:58Z",
        'App_name' : "Vimbo\u00ae Hikaye Okuma Arkada\u015f\u0131",
        'Package_name' : "com.vimbo.app",
        'version' : "1.0.16",
        'minimumOsVersion' : "12.0",
    },
    'com.square_enix.WOTVffbejp' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "FFBE\u5e7b\u5f71\u6226\u4e89 WAR OF THE VISIONS",
        'Package_name' : "com.square_enix.WOTVffbejp",
        'installs' : "500,000+",
        'version' : "6.1.0",
    },
    'com.kakaogames.tera' :
    {
        'category' : "Games",
        'updated' : "15 Mar 2021",
        'App_name' : "\ud14c\ub77c \ud074\ub798\uc2dd",
        'Package_name' : "com.kakaogames.tera",
        'installs' : "500,000+",
        'version' : "1.600.2",
    },
    'com.onepunchman.ggplay.tw' :
    {
        'category' : "Games",
        'updated' : "30 Apr 2022",
        'App_name' : "\u4e00\u62f3\u8d85\u4eba\uff1a\u6700\u5f37\u4e4b\u7537\uff08\u5b98\u65b9\u6b63\u7248\u6388\u6b0a\uff09",
        'Package_name' : "com.onepunchman.ggplay.tw",
        'installs' : "1,000,000+",
        'version' : "1.4.2",
    },
    'kr.co.lottorich.SLandroid' :
    {
        'category' : "Entertainment",
        'updated' : "21 Feb 2022",
        'App_name' : "\ub85c\ub610\ub9ac\uce58 - \ub85c\ub6101\ub4f1 \ub2f9\ucca8\uc790 127\uba85 \ubc30\ucd9c, \ud55c\uad6d\uae30\ub85d\uc6d0 \uacf5\uc2dd\uc778\uc99d",
        'Package_name' : "kr.co.lottorich.SLandroid",
        'installs' : "1,000,000+",
        'version' : "1.9.9",
    },
    'io.sidelines.propshop1' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "PropShop: Sports Betting",
        'Package_name' : "io.sidelines.propshop1",
        'installs' : "10,000+",
        'version' : "7.1.4",
    },
    'com.unioncoop.ucs' :
    {
        'category' : "Shopping",
        'updated' : "17 May 2022",
        'App_name' : "Union Coop",
        'Package_name' : "com.unioncoop.ucs",
        'installs' : "100,000+",
        'version' : "5.3.2",
    },
    'com.monawa.gswordgg' :
    {
        'category' : "Games",
        'updated' : "14 Apr 2020",
        'App_name' : "\ub300\uac80",
        'Package_name' : "com.monawa.gswordgg",
        'installs' : "100,000+",
        'version' : "1.0.1",
    },
    'com.rapido.rider' :
    {
        'category' : "Utilities",
        'updated' : "14 Jun 2022",
        'App_name' : "Rapido Captain- Bike Taxi|Auto",
        'Package_name' : "com.rapido.rider",
        'installs' : "5,000,000+",
        'version' : "4.5.78",
    },
    'com.kunpo.virus' :
    {
        'category' : "Games",
        'updated' : "13 Aug 2021",
        'App_name' : "Virus War - Space Shooting Game",
        'Package_name' : "com.kunpo.virus",
        'installs' : "5,000,000+",
        'version' : "1.8.8",
    },
    'com.kudabank.app' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "The Bank of the Free - Kuda",
        'Package_name' : "com.kudabank.app",
        'installs' : "1,000,000+",
        'version' : "0.9.583",
    },
    'ae.firstcry.shopping.parenting' :
    {
        'category' : "Shopping",
        'updated' : "16 May 2022",
        'App_name' : "FirstCry Arabia: Baby & Kids",
        'Package_name' : "ae.firstcry.shopping.parenting",
        'installs' : "500,000+",
        'version' : "0.0.26",
    },
    'com.mobime.ecooltra' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "17 Jun 2022",
        'App_name' : "Cooltra Motosharing Scooter",
        'Package_name' : "com.mobime.ecooltra",
        'installs' : "1,000,000+",
        'version' : "4.6.5.2",
    },
    'com.apalon.scanner.app' :
    {
        'category' : "Finance",
        'updated' : "23 Mar 2022",
        'App_name' : "Scan Hero: Document Scanner",
        'Package_name' : "com.apalon.scanner.app",
        'installs' : "10,000,000+",
        'version' : "1.65.0",
    },
    'ru.ritmmedia.yappy' :
    {
        'category' : "Social and Communication",
        'updated' : "08 Jun 2022",
        'App_name' : "YAPPY: \u0432\u0438\u0434\u0435\u043e\u043f\u043b\u0430\u0442\u0444\u043e\u0440\u043c\u0430",
        'Package_name' : "ru.ritmmedia.yappy",
        'installs' : "5,000,000+",
        'version' : "0.12.1",
    },
    'com.bandainamcoent.srwdd_jp' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "\u30b9\u30fc\u30d1\u30fc\u30ed\u30dc\u30c3\u30c8\u5927\u6226DD",
        'Package_name' : "com.bandainamcoent.srwdd_jp",
        'installs' : "100,000+",
        'version' : "3.6.0",
    },
    'com.srpc.MangaArabia' :
    {
        'category' : "Education and Books",
        'updated' : "13 Jun 2022",
        'App_name' : "\u0645\u0627\u0646\u062c\u0627 \u0644\u0644\u0635\u063a\u0627\u0631",
        'Package_name' : "com.srpc.MangaArabia",
        'installs' : "100,000+",
        'version' : "1.0.3",
    },
    'com.flexiroamx' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "12 May 2022",
        'App_name' : "Flexiroam X: eSIM & Data Plans",
        'Package_name' : "com.flexiroamx",
        'installs' : "500,000+",
        'version' : "4.0.2",
    },
    'com.nexon.sinoalice' :
    {
        'category' : "Games",
        'updated' : "13 May 2022",
        'App_name' : "SINoALICE",
        'Package_name' : "com.nexon.sinoalice",
        'installs' : "1,000,000+",
        'version' : "31.0.0",
    },
    'vn.funtap.thankiem.mobile' :
    {
        'category' : "Games",
        'updated' : "25 Feb 2022",
        'App_name' : "Th\u1ea7n Ki\u1ebfm Mobile - Funtap",
        'Package_name' : "vn.funtap.thankiem.mobile",
        'installs' : "500,000+",
        'version' : "1.5.1",
    },
    'fr.snapp.fidme' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "FidMe Loyalty Cards & Cashback",
        'Package_name' : "fr.snapp.fidme",
        'installs' : "1,000,000+",
        'version' : "8.0.0",
    },
    'fr.sephora.sephorafrance' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "Sephora - Maquillage & Parfum",
        'Package_name' : "fr.sephora.sephorafrance",
        'installs' : "1,000,000+",
        'version' : "3.8.70",
    },
    'ph.kreditpintar' :
    {
        'category' : "Finance",
        'updated' : "16 Mar 2020",
        'App_name' : "Atome PH",
        'Package_name' : "ph.kreditpintar",
        'installs' : "1,000,000+",
        'version' : "1.5.0",
    },
    'com.audiobooks.androidapp' :
    {
        'category' : "Education and Books",
        'updated' : "11 Jun 2022",
        'App_name' : "Audiobooks.com: Books & More",
        'Package_name' : "com.audiobooks.androidapp",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.fiton.android' :
    {
        'category' : "Health and Fitness",
        'updated' : "26 May 2022",
        'App_name' : "FitOn Workouts & Fitness Plans",
        'Package_name' : "com.fiton.android",
        'installs' : "5,000,000+",
        'version' : "Varies with device",
    },
    'com.outfit7.mytalkingtom2' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "My Talking Tom 2",
        'Package_name' : "com.outfit7.mytalkingtom2",
        'installs' : "500,000,000+",
        'version' : "3.3.2.2780",
    },
    'com.centromgames.samuraihero' :
    {
        'category' : "Games",
        'updated' : "10 Feb 2022",
        'App_name' : "Samurai Adventure : Endless Fight",
        'Package_name' : "com.centromgames.samuraihero",
        'installs' : "100+",
        'version' : "8.0",
    },
    'com.neowiz.game.golflegends' :
    {
        'category' : "Games",
        'updated' : "26 Nov 2021",
        'App_name' : "Golf Impact - World Tour",
        'Package_name' : "com.neowiz.game.golflegends",
        'installs' : "1,000,000+",
        'version' : "1.10.00",
    },
    'com.tv.v18.viola' :
    {
        'category' : "Entertainment",
        'updated' : "17 Jun 2022",
        'App_name' : "Voot, Bigg Boss, Colors TV",
        'Package_name' : "com.tv.v18.viola",
        'installs' : "100,000,000+",
        'version' : "4.2.10",
    },
    '718312937' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-05-26T12:48:38Z",
        'App_name' : "Addison Lee: Minicab & Courier",
        'Package_name' : "co.uk.addisonlee.priority",
        'version' : "8.11.0",
        'minimumOsVersion' : "13.0",
    },
    'com.aymore.sim' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Empr\u00e9stimo Pessoal SIM",
        'Package_name' : "com.aymore.sim",
        'installs' : "1,000,000+",
        'version' : "1.1.79.1",
    },
    'com.wolffun.thetanarena' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "Thetan Arena: MOBA Survival",
        'Package_name' : "com.wolffun.thetanarena",
        'installs' : "10,000,000+",
        'version' : "335",
    },
    '1463605579' :
    {
        'category' : "Shopping",
        'updated' : "2021-11-24T09:43:40Z",
        'App_name' : "eyewa - Eyewear Shopping App",
        'Package_name' : "com.eyewa.app",
        'version' : "2.0.17",
        'minimumOsVersion' : "12.0",
    },
    'com.heetch' :
    {
        'category' : "Utilities",
        'updated' : "13 Jun 2022",
        'App_name' : "Heetch - Ride-hailing app",
        'Package_name' : "com.heetch",
        'installs' : "5,000,000+",
        'version' : "5.52.2",
    },
    'com.bandainamcoent.gb_en' :
    {
        'category' : "Games",
        'updated' : "13 Apr 2022",
        'App_name' : "GUNDAM BREAKER MOBILE",
        'Package_name' : "com.bandainamcoent.gb_en",
        'installs' : "500,000+",
        'version' : "3.04.00",
    },
    'it.tonki.tonki' :
    {
        'category' : "Lifestyle",
        'updated' : "19 Nov 2021",
        'App_name' : "Tonki - Print Your Photos on Cardboard",
        'Package_name' : "it.tonki.tonki",
        'installs' : "50,000+",
        'version' : "3.1.12",
    },
    'com.lucktastic.scratch' :
    {
        'category' : "Lifestyle",
        'updated' : "23 Feb 2022",
        'App_name' : "Lucktastic: Win Prizes, Real Rewards, & Gift Cards",
        'Package_name' : "com.lucktastic.scratch",
        'installs' : "10,000,000+",
        'version' : "2.33.2",
    },
    'com.crosstower.india' :
    {
        'category' : "Finance",
        'updated' : "30 May 2022",
        'App_name' : "CrossTower : Trade In Crypto",
        'Package_name' : "com.crosstower.india",
        'installs' : "100,000+",
        'version' : "3.8",
    },
    'com.egames.sgdtskr.google' :
    {
        'category' : "Games",
        'updated' : "27 Jan 2022",
        'App_name' : "2X",
        'Package_name' : "com.egames.sgdtskr.google",
        'installs' : "500,000+",
        'version' : "2.0.0",
    },
    'com.capermint.ludoempire' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Ludo Empire",
        'Package_name' : "com.capermint.ludoempire",
        'installs' : "1,000,000+",
        'version' : "10.1",
    },
    'jp.co.inte.miidas' :
    {
        'category' : "Finance",
        'updated' : "28 May 2022",
        'App_name' : "\u30df\u30a4\u30c0\u30b9 - \u3042\u306a\u305f\u306e\u672c\u5f53\u306e\u5e02\u5834\u4fa1\u5024\u3092\u898b\u3044\u3060\u3059\u8ee2\u8077\u30a2\u30d7\u30ea",
        'Package_name' : "jp.co.inte.miidas",
        'installs' : "100,000+",
        'version' : "5.0.8",
    },
    'ph.cashwagon.mobile.android' :
    {
        'category' : "Finance",
        'updated' : "14 Aug 2020",
        'App_name' : "Cashwagon \u2013 Cash loan app Philippines",
        'Package_name' : "ph.cashwagon.mobile.android",
        'installs' : "1,000,000+",
        'version' : "1.69.0",
    },
    'com.diandian.gog' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "Guns of Glory: The Iron Mask",
        'Package_name' : "com.diandian.gog",
        'installs' : "50,000,000+",
        'version' : "7.19.0",
    },
    '480599198' :
    {
        'category' : "Not available",
        'updated' : "2022-06-01T11:50:07Z",
        'App_name' : "ifolor: Photo Books, Photos",
        'Package_name' : "com.ifolor.photo-service",
        'version' : "4.7.6",
        'minimumOsVersion' : "13.2",
    },
    'com.gosub60.sol5spdr2' :
    {
        'category' : "Games",
        'updated' : "02 May 2022",
        'App_name' : "Spider Solitaire Deluxe\u00ae 2",
        'Package_name' : "com.gosub60.sol5spdr2",
        'installs' : "1,000,000+",
        'version' : "4.37.1",
    },
    'de.baur.shop' :
    {
        'category' : "Shopping",
        'updated' : "17 May 2022",
        'App_name' : "BAUR \u2013 Mode & Wohnen",
        'Package_name' : "de.baur.shop",
        'installs' : "1,000,000+",
        'version' : "2.5.1",
    },
    'jp.konami.mfcsp' :
    {
        'category' : "Casino",
        'updated' : "26 May 2022",
        'App_name' : "MAH-JONG FIGHT CLUB Sp",
        'Package_name' : "jp.konami.mfcsp",
        'installs' : "1,000,000+",
        'version' : "2.1.0",
    },
    'com.ebates' :
    {
        'category' : "Shopping",
        'updated' : "09 Jun 2022",
        'App_name' : "Rakuten: Cash Back & more",
        'Package_name' : "com.ebates",
        'installs' : "10,000,000+",
        'version' : "9.32.0",
    },
    'apms.apro.rush' :
    {
        'category' : "Finance",
        'updated' : "14 Apr 2022",
        'App_name' : "\ubb34\uce74\ub4dc",
        'Package_name' : "apms.apro.rush",
        'installs' : "500,000+",
        'version' : "7.0.52",
    },
    'com.linecorp.LGFARMKR' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "BROWN FARM",
        'Package_name' : "com.linecorp.LGFARMKR",
        'installs' : "500,000+",
        'version' : "1.2.5",
    },
    'com.gotgl.kr' :
    {
        'category' : "Games",
        'updated' : "02 Dec 2021",
        'App_name' : "\uc655\uc88c\uc758\uac8c\uc784:\uc708\ud130\uc774\uc988\ucee4\ubc0d",
        'Package_name' : "com.gotgl.kr",
        'installs' : "100,000+",
        'version' : "2.6.11232113",
    },
    'com.tamilmatrimony' :
    {
        'category' : "Social and Communication",
        'updated' : "21 May 2022",
        'App_name' : "Tamil Matrimony\u00ae- Marriage App",
        'Package_name' : "com.tamilmatrimony",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.Level5.BS' :
    {
        'category' : "Games",
        'updated' : "27 Nov 2020",
        'App_name' : "\u30a4\u30ca\u30ba\u30de\u30a4\u30ec\u30d6\u30f3 SD",
        'Package_name' : "com.Level5.BS",
        'installs' : "100,000+",
        'version' : "1.16.2",
    },
    'com.gmt.phonglang' :
    {
        'category' : "Games",
        'updated' : "27 Sep 2021",
        'App_name' : "Phong L\u0103ng Thi\u00ean H\u1ea1 - Ki\u1ebfm Hi\u1ec7p Ch\u00ed T\u00f4n",
        'Package_name' : "com.gmt.phonglang",
        'installs' : "100,000+",
        'version' : "1.2.3",
    },
    'com.spiintl.filmbox' :
    {
        'category' : "Entertainment",
        'updated' : "07 Apr 2022",
        'App_name' : "FilmBox+",
        'Package_name' : "com.spiintl.filmbox",
        'installs' : "500,000+",
        'version' : "0.4.1",
    },
    'com.bsl.spencers.activity' :
    {
        'category' : "Shopping",
        'updated' : "11 Apr 2022",
        'App_name' : "Spencer's - Online Grocery Shopping App",
        'Package_name' : "com.bsl.spencers.activity",
        'installs' : "1,000,000+",
        'version' : "4.6.6",
    },
    'com.winwaygame.txj' :
    {
        'category' : "Games",
        'updated' : "18 Apr 2022",
        'App_name' : "\u5929\u661f\u8a23",
        'Package_name' : "com.winwaygame.txj",
        'installs' : "10,000+",
        'version' : "2.0",
    },
    'kr.co.mustit' :
    {
        'category' : "Shopping",
        'updated' : "02 May 2022",
        'App_name' : "\uba38\uc2a4\ud2b8\uc787(MUST IT) - \uc628\ub77c\uc778 \uba85\ud488 \ud50c\ub7ab\ud3fc",
        'Package_name' : "kr.co.mustit",
        'installs' : "1,000,000+",
        'version' : "4.4.4",
    },
    'fr.airweb.izneo' :
    {
        'category' : "Education and Books",
        'updated' : "24 Mar 2022",
        'App_name' : "izneo: read Manga, Webtoon, BD",
        'Package_name' : "fr.airweb.izneo",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'ru.hoff.app' :
    {
        'category' : "Shopping",
        'updated' : "28 May 2022",
        'App_name' : "Hoff: \u043c\u0435\u0431\u0435\u043b\u044c \u0438 \u0442\u043e\u0432\u0430\u0440\u044b \u0434\u043b\u044f \u0434\u043e\u043c\u0430",
        'Package_name' : "ru.hoff.app",
        'installs' : "1,000,000+",
        'version' : "8.61.0",
    },
    'com.games.topgames' :
    {
        'category' : "Games",
        'updated' : "29 May 2020",
        'App_name' : "Tgames",
        'Package_name' : "com.games.topgames",
        'installs' : "100+",
        'version' : "0.0.1",
    },
    'sg.cocofun' :
    {
        'category' : "Entertainment",
        'updated' : "09 Jun 2022",
        'App_name' : "CocoFun - Video lucu & Meme",
        'Package_name' : "sg.cocofun",
        'installs' : "10,000,000+",
        'version' : "2.22.0",
    },
    'com.gravity.probb.aos' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "CPBL\u8077\u696d\u68d2\u74032022",
        'Package_name' : "com.gravity.probb.aos",
        'installs' : "100,000+",
        'version' : "3.6",
    },
    'com.paribu.app' :
    {
        'category' : "Finance",
        'updated' : "10 Feb 2022",
        'App_name' : "Paribu | Bitcoin-Kripto Para Al\u0131m Sat\u0131m",
        'Package_name' : "com.paribu.app",
        'installs' : "1,000,000+",
        'version' : "3.4.2",
    },
    'com.sega.PuyoQuest' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "\u3077\u3088\u3077\u3088!!\u30af\u30a8\u30b9\u30c8 -\u7c21\u5358\u64cd\u4f5c\u3067\u5927\u9023\u9396\u3002\u723d\u5feb \u30d1\u30ba\u30eb\uff01",
        'Package_name' : "com.sega.PuyoQuest",
        'installs' : "5,000,000+",
        'version' : "10.3.2",
    },
    'jp.co.acearcher' :
    {
        'category' : "Games",
        'updated' : "29 Apr 2021",
        'App_name' : "\u30a8\u30fc\u30b9\u30a2\u30fc\u30c1\u30e3\u30fc",
        'Package_name' : "jp.co.acearcher",
        'installs' : "100,000+",
        'version' : "1.0.14",
    },
    'com.whaleapp.hiddenresort' :
    {
        'category' : "Games",
        'updated' : "01 Sep 2021",
        'App_name' : "Hidden Resort: Adventure Bay",
        'Package_name' : "com.whaleapp.hiddenresort",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.virginmobile.uae' :
    {
        'category' : "Tools",
        'updated' : "01 Jun 2022",
        'App_name' : "Virgin Mobile UAE",
        'Package_name' : "com.virginmobile.uae",
        'installs' : "1,000,000+",
        'version' : "2.48.1",
    },
    '1485355428' :
    {
        'category' : "Games",
        'updated' : "2022-03-02T01:22:33Z",
        'App_name' : "\u767a\u898b\uff01\u5c0f\u5b66\u751f\u3042\u308b\u3042\u308b",
        'Package_name' : "com.mask.shogaku",
        'version' : "1.2.2",
        'minimumOsVersion' : "12.0",
    },
    '905869418' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-15T22:38:49Z",
        'App_name' : "Armaz\u00e9ns Dhgate-Online",
        'Package_name' : "com.dhgate.DHgateBuyer",
        'version' : "5.8.5",
        'minimumOsVersion' : "10.0",
    },
    'com.newnormalgames.phonecasediy' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "Phone Case DIY",
        'Package_name' : "com.newnormalgames.phonecasediy",
        'installs' : "100,000,000+",
        'version' : "2.6.6.0",
    },
    'com.joymax.starwarsstarfighter' :
    {
        'category' : "Games",
        'updated' : "13 Apr 2021",
        'App_name' : "StarWars\u2122: StarfighterMissions",
        'Package_name' : "com.joymax.starwarsstarfighter",
        'installs' : "100,000+",
        'version' : "1.23",
    },
    'com.indie.shj.google.ft' :
    {
        'category' : "Games",
        'updated' : "21 Apr 2022",
        'App_name' : "\u5c71\u6d77\u6709\u5996\u7378",
        'Package_name' : "com.indie.shj.google.ft",
        'installs' : "100,000+",
        'version' : "1.5.6",
    },
    'com.handmark.sportcaster' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "CBS Sports App Scores & News",
        'Package_name' : "com.handmark.sportcaster",
        'installs' : "10,000,000+",
        'version' : "10.31",
    },
    'com.biligamekr.girlcafegungp' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "\uac78\uce74\ud398\uac74",
        'Package_name' : "com.biligamekr.girlcafegungp",
        'installs' : "100,000+",
        'version' : "1.0.15",
    },
    '524362642' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-16T06:17:48Z",
        'App_name' : "Trendyol - Online Shopping",
        'Package_name' : "trendyol.com.trendyol",
        'version' : "6.2.14",
        'minimumOsVersion' : "12.0",
    },
    'com.nordvpn.android' :
    {
        'category' : "Tools",
        'updated' : "17 Jun 2022",
        'App_name' : "NordVPN \u2013 fast VPN for privacy",
        'Package_name' : "com.nordvpn.android",
        'installs' : "50,000,000+",
        'version' : "Varies with device",
    },
    'com.latto.tv.dogtv' :
    {
        'category' : "Entertainment",
        'updated' : "16 Jun 2022",
        'App_name' : "DOGTV: Television for dogs",
        'Package_name' : "com.latto.tv.dogtv",
        'installs' : "100,000+",
        'version' : "7.602.1",
    },
    'com.yoozoogames.ss2game' :
    {
        'category' : "Games",
        'updated' : "24 Mar 2022",
        'App_name' : "\u5c11\u5e74\u4e09\u56fd\u5fd72\u65b0\u9a6c\u7248",
        'Package_name' : "com.yoozoogames.ss2game",
        'installs' : "100,000+",
        'version' : "1.3.70",
    },
    'com.gamezy.lite' :
    {
        'category' : "Games",
        'updated' : "27 Apr 2022",
        'App_name' : "Gamezy: Play Fantasy Cricket, Rummy, Ludo",
        'Package_name' : "com.gamezy.lite",
        'installs' : "1,000,000+",
        'version' : "9.0.2022042717",
    },
    'com.bingo.day.free.game.puzzle' :
    {
        'category' : "Games",
        'updated' : "27 May 2022",
        'App_name' : "Bingo Day: Lucky to Win",
        'Package_name' : "com.bingo.day.free.game.puzzle",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.sendo' :
    {
        'category' : "Shopping",
        'updated' : "17 Jun 2022",
        'App_name' : "Sendo: Ch\u1ee3 C\u1ee7a Ng\u01b0\u1eddi Vi\u1ec7t",
        'Package_name' : "com.sendo",
        'installs' : "10,000,000+",
        'version' : "4.0.46",
    },
    'com.netease.dhhzlggkr' :
    {
        'category' : "Games",
        'updated' : "24 Nov 2021",
        'App_name' : "\ub300\ud56d\ud574\uc758\uae38",
        'Package_name' : "com.netease.dhhzlggkr",
        'installs' : "1,000,000+",
        'version' : "'62'",
    },
    'com.crictec.cricket' :
    {
        'category' : "Games",
        'updated' : "28 Apr 2022",
        'App_name' : "Cricket.com - Live Score&News",
        'Package_name' : "com.crictec.cricket",
        'installs' : "10,000,000+",
        'version' : "3.0.0",
    },
    'com.zap.surveys' :
    {
        'category' : "Tools",
        'updated' : "14 Jun 2022",
        'App_name' : "Zap Surveys - Get Paid Cash",
        'Package_name' : "com.zap.surveys",
        'installs' : "1,000,000+",
        'version' : "3.09",
    },
    'com.topgamesinc.hs' :
    {
        'category' : "Games",
        'updated' : "07 Jun 2022",
        'App_name' : "Kings Legion",
        'Package_name' : "com.topgamesinc.hs",
        'installs' : "1,000,000+",
        'version' : "1.0.391",
    },
    'com.littlecaesars' :
    {
        'category' : "Food & Drink",
        'updated' : "09 Jun 2022",
        'App_name' : "Little Caesars",
        'Package_name' : "com.littlecaesars",
        'installs' : "10,000,000+",
        'version' : "9.7.0",
    },
    'com.icocofun.us.maga' :
    {
        'category' : "Entertainment",
        'updated' : "13 Jun 2022",
        'App_name' : "CocoFun - Funny Videos & Memes",
        'Package_name' : "com.icocofun.us.maga",
        'installs' : "500,000+",
        'version' : "1.43.1",
    },
    'com.hungrigames.godofgod' :
    {
        'category' : "Games",
        'updated' : "05 Sep 2021",
        'App_name' : "God of Gods: Age of Mythology - Strategy RPG game",
        'Package_name' : "com.hungrigames.godofgod",
        'installs' : "10,000+",
        'version' : "1.1.9",
    },
    '1438599112' :
    {
        'category' : "Games",
        'updated' : "2019-12-25T15:06:09Z",
        'App_name' : "\u6d6e\u6c17\u30b5\u30ec\u5973-\u5973\u5b50\u306b\u4eba\u6c17\u306e\u604b\u611b\u63a8\u7406\u30b2\u30fc\u30e0",
        'Package_name' : "com.abiru.uwakiBest",
        'version' : "1.1.3",
        'minimumOsVersion' : "10.0",
    },
    'com.pahamify.android' :
    {
        'category' : "Education and Books",
        'updated' : "18 May 2022",
        'App_name' : "Pahamify - Taklukkan UTBK",
        'Package_name' : "com.pahamify.android",
        'installs' : "1,000,000+",
        'version' : "2.7.1",
    },
    'com.eyougame.mong' :
    {
        'category' : "Games",
        'updated' : "27 Apr 2022",
        'App_name' : "\ucc9c\uc0c1\ub098\ub974\uc0e4",
        'Package_name' : "com.eyougame.mong",
        'installs' : "100,000+",
        'version' : "3.1.1",
    },
    'com.rp.smartpush' :
    {
        'category' : "Shopping",
        'updated' : "28 Feb 2022",
        'App_name' : "\ud560\uc778\ud0c0\uc784 - \uad6d\ub0b4 \ucd5c\ub300\ud560\uc778 \uc1fc\ud551\ubab0",
        'Package_name' : "com.rp.smartpush",
        'installs' : "1,000,000+",
        'version' : "3.50",
    },
    'ahaflix.tv' :
    {
        'category' : "Entertainment",
        'updated' : "15 Jun 2022",
        'App_name' : "aha - 100% Local Entertainment",
        'Package_name' : "ahaflix.tv",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.wishabi.flipp' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Flipp - Weekly Shopping",
        'Package_name' : "com.wishabi.flipp",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.eloelo' :
    {
        'category' : "Lifestyle",
        'updated' : "14 Jun 2022",
        'App_name' : "Live Video Chatrooms & Games",
        'Package_name' : "com.eloelo",
        'installs' : "1,000,000+",
        'version' : "3.4.8",
    },
    'com.anmedia.wowcher.ui' :
    {
        'category' : "Shopping",
        'updated' : "06 Jun 2022",
        'App_name' : "Wowcher \u2013 Deals & Vouchers",
        'Package_name' : "com.anmedia.wowcher.ui",
        'installs' : "5,000,000+",
        'version' : "11.5",
    },
    'com.netease.heatup' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "Heat Up - Chat & Make friends",
        'Package_name' : "com.netease.heatup",
        'installs' : "1,000,000+",
        'version' : "1.24.0",
    },
    'com.gravity.ro.and' :
    {
        'category' : "Games",
        'updated' : "13 Apr 2022",
        'App_name' : "RO\u4ed9\u5883\u50b3\u8aaa\uff1a\u5b88\u8b77\u6c38\u6046\u7684\u611b",
        'Package_name' : "com.gravity.ro.and",
        'installs' : "1,000,000+",
        'version' : "1.8.1",
    },
    '1585419012' :
    {
        'category' : "Games",
        'updated' : "2022-05-30T00:33:13Z",
        'App_name' : "Life Choices",
        'Package_name' : "com.unicostudio.lifechoices",
        'version' : "1.1.2",
        'minimumOsVersion' : "11.0",
    },
    'com.getir' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "getir: groceries in minutes",
        'Package_name' : "com.getir",
        'installs' : "10,000,000+",
        'version' : "2.13.3",
    },
    'com.phonegap.dominos' :
    {
        'category' : "Food & Drink",
        'updated' : "30 May 2022",
        'App_name' : "Domino's Pizza Indonesia - Home Delivery Expert",
        'Package_name' : "com.phonegap.dominos",
        'installs' : "1,000,000+",
        'version' : "6.0.7",
    },
    'com.bandainamcoent.srwdd_ww' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "\u8d85\u7d1a\u6a5f\u5668\u4eba\u5927\u6230DD",
        'Package_name' : "com.bandainamcoent.srwdd_ww",
        'installs' : "100,000+",
        'version' : "3.6.0",
    },
    'com.phonegap.rxpal' :
    {
        'category' : "Health and Fitness",
        'updated' : "11 Jun 2022",
        'App_name' : "PharmEasy - Healthcare App",
        'Package_name' : "com.phonegap.rxpal",
        'installs' : "10,000,000+",
        'version' : "5.2.1",
    },
    'com.lidl.eci.lidlplus' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "Lidl Plus",
        'Package_name' : "com.lidl.eci.lidlplus",
        'installs' : "50,000,000+",
        'version' : "15.4.3",
    },
    'kr.co.reigntalk.amasia' :
    {
        'category' : "Social and Communication",
        'updated' : "07 Jun 2022",
        'App_name' : "Amasia - Love is borderless",
        'Package_name' : "kr.co.reigntalk.amasia",
        'installs' : "1,000,000+",
        'version' : "2.1.7",
    },
    'com.me2on.fulpotgenius' :
    {
        'category' : "Casino",
        'updated' : "13 May 2022",
        'App_name' : "Fulpot Poker : Texas Holdem",
        'Package_name' : "com.me2on.fulpotgenius",
        'installs' : "100,000+",
        'version' : "2.0.78",
    },
    '1081873524' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-02T18:46:29Z",
        'App_name' : "MOOV - Car Hire",
        'Package_name' : "tr.celik",
        'version' : "5.1.5",
        'minimumOsVersion' : "11.0",
    },
    '728374868' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-05-31T23:33:21Z",
        'App_name' : "\u5e74\u8cc0\u72b6 2023 - \u5e74\u8cc0\u72b6\u306f\u300c\u30b9\u30de\u30db\u3067\u5e74\u8cc0\u72b6\u300d\u5e74\u8cc0\u72b6\u30a2\u30d7\u30ea",
        'Package_name' : "jp.co.yahoo.ios.ynenga",
        'version' : "8.6.0",
        'minimumOsVersion' : "13.0",
    },
    'com.voocle.now' :
    {
        'category' : "Games",
        'updated' : "05 Aug 2021",
        'App_name' : "\ub098\uc6b0(NOW) : REMEET",
        'Package_name' : "com.voocle.now",
        'installs' : "50,000+",
        'version' : "1.2.21.584",
    },
    'com.pegipegi.android' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "20 May 2022",
        'App_name' : "Pegipegi - Flights, Hotel, Bus",
        'Package_name' : "com.pegipegi.android",
        'installs' : "5,000,000+",
        'version' : "5.4.0",
    },
    'tw.sonet.wb.google' :
    {
        'category' : "Games",
        'updated' : "14 Apr 2022",
        'App_name' : "WildBorn \u91ce\u5883\u91cd\u751f",
        'Package_name' : "tw.sonet.wb.google",
        'installs' : "50,000+",
        'version' : "0.1.5",
    },
    'in.publicam.cricsquad' :
    {
        'category' : "Games",
        'updated' : "24 Nov 2021",
        'App_name' : "Fantasy Cricket, Live Score, News, Videos: 100MB",
        'Package_name' : "in.publicam.cricsquad",
        'installs' : "1,000,000+",
        'version' : "7.2",
    },
    'com.perblue.disneyheroescjk' :
    {
        'category' : "Games",
        'updated' : "28 May 2022",
        'App_name' : "Disney Heroes: Battle Mode",
        'Package_name' : "com.perblue.disneyheroescjk",
        'installs' : "500,000+",
        'version' : "4.0.10",
    },
    'com.iventory.adv' :
    {
        'category' : "Games",
        'updated' : "24 Sep 2021",
        'App_name' : "\uc2b9\ubd80\uc0ac\uc628\ub77c\uc778",
        'Package_name' : "com.iventory.adv",
        'installs' : "50,000+",
        'version' : "1.0.0.151",
    },
    'com.move.realtor' :
    {
        'category' : "House & Home",
        'updated' : "16 Jun 2022",
        'App_name' : "Realtor.com Real Estate",
        'Package_name' : "com.move.realtor",
        'installs' : "10,000,000+",
        'version' : "10.53.0",
    },
    'com.looket.wconcept' :
    {
        'category' : "Shopping",
        'updated' : "16 Jun 2022",
        'App_name' : "W\ucee8\uc149 - \uc628\ub77c\uc778 \ud328\uc158 \ud50c\ub7ab\ud3fc",
        'Package_name' : "com.looket.wconcept",
        'installs' : "1,000,000+",
        'version' : "5.1.1",
    },
    'sg.bigo.hellotalk' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "Hello Yo - Group Chat Rooms",
        'Package_name' : "sg.bigo.hellotalk",
        'installs' : "10,000,000+",
        'version' : "5.21.3",
    },
    'com.scenestealer.wzjh.google' :
    {
        'category' : "Games",
        'updated' : "16 Apr 2020",
        'App_name' : "\uc2e0\uac15\ud638",
        'Package_name' : "com.scenestealer.wzjh.google",
        'installs' : "1,000,000+",
        'version' : "1.3.4",
    },
    'com.miniclip.eightballpool' :
    {
        'category' : "Games",
        'updated' : "30 May 2022",
        'App_name' : "8 Ball Pool",
        'Package_name' : "com.miniclip.eightballpool",
        'installs' : "500,000,000+",
        'version' : "5.8.0",
    },
    'com.mamaearthapp' :
    {
        'category' : "Shopping",
        'updated' : "06 May 2022",
        'App_name' : "Mamaearth \u2013 Natural Beauty & Baby Products Store",
        'Package_name' : "com.mamaearthapp",
        'installs' : "5,000,000+",
        'version' : "18.2.22",
    },
    '584632814' :
    {
        'category' : "Shopping",
        'updated' : "2022-01-18T17:47:01Z",
        'App_name' : "Slickdeals: Deals & Discounts",
        'Package_name' : "com.slickdeals.mobile",
        'version' : "5.37.4",
        'minimumOsVersion' : "13.0",
    },
    'jp.co.mixi.monsterstrike' :
    {
        'category' : "Games",
        'updated' : "19 May 2022",
        'App_name' : "\u30e2\u30f3\u30b9\u30bf\u30fc\u30b9\u30c8\u30e9\u30a4\u30af",
        'Package_name' : "jp.co.mixi.monsterstrike",
        'installs' : "10,000,000+",
        'version' : "23.2.0",
    },
    'com.marketly.trading' :
    {
        'category' : "Finance",
        'updated' : "03 Jun 2022",
        'App_name' : "Binomo - Mobile Trading Online",
        'Package_name' : "com.marketly.trading",
        'installs' : "10,000,000+",
        'version' : "4.14.11",
    },
    'com.justclean.justclean' :
    {
        'category' : "Lifestyle",
        'updated' : "12 Jun 2022",
        'App_name' : "justclean",
        'Package_name' : "com.justclean.justclean",
        'installs' : "100,000+",
        'version' : "5.04.4",
    },
    'com.eurekastudio.hidemytest' :
    {
        'category' : "Games",
        'updated' : "24 Mar 2022",
        'App_name' : "Hide My Test! - escape game",
        'Package_name' : "com.eurekastudio.hidemytest",
        'installs' : "10,000,000+",
        'version' : "1.6.5",
    },
    'kr.todayis.you' :
    {
        'category' : "Social and Communication",
        'updated' : "31 Mar 2022",
        'App_name' : "Stranger Video Chat",
        'Package_name' : "kr.todayis.you",
        'installs' : "100,000+",
        'version' : "1.3.4",
    },
    'com.lilithgames.wgame.android.jp' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "WARPATH-\u6b66\u88c5\u90fd\u5e02-",
        'Package_name' : "com.lilithgames.wgame.android.jp",
        'installs' : "50,000+",
        'version' : "4.32.10",
    },
    'kr.co.alba.webappalba.m' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "\uc54c\ubc14\ucc9c\uad6d- \uc54c\ubc14 \uad6c\uc778\uad6c\uc9c1 \ud3ec\ud138\uc11c\ube44\uc2a4",
        'Package_name' : "kr.co.alba.webappalba.m",
        'installs' : "10,000,000+",
        'version' : "5.3.53",
    },
    '288087905' :
    {
        'category' : "Not available",
        'updated' : "2022-05-31T16:35:14Z",
        'App_name' : "Stitcher for Podcasts",
        'Package_name' : "com.stitcher.player",
        'version' : "10.29",
        'minimumOsVersion' : "11.0",
    },
    'com.mventus.selfcare.activity' :
    {
        'category' : "Tools",
        'updated' : "02 Jun 2022",
        'App_name' : "Vi App - Recharges & Music",
        'Package_name' : "com.mventus.selfcare.activity",
        'installs' : "100,000,000+",
        'version' : "9.7.5",
    },
    '1448666290' :
    {
        'category' : "Games",
        'updated' : "2022-05-10T07:02:49Z",
        'App_name' : "\u30a2\u30a4\u30c9\u30eb\u30de\u30b9\u30bf\u30fc \u30b7\u30e3\u30a4\u30cb\u30fc\u30ab\u30e9\u30fc\u30ba",
        'Package_name' : "jp.co.bandainamcoent.BNEI0354",
        'version' : "1.0.39",
        'minimumOsVersion' : "11.0",
    },
    'com.tapeacall.com' :
    {
        'category' : "Finance",
        'updated' : "29 Jul 2021",
        'App_name' : "TapeACall: Phone Call Recorder",
        'Package_name' : "com.tapeacall.com",
        'installs' : "1,000,000+",
        'version' : "4.0",
    },
    'com.playpopo.lomvpn' :
    {
        'category' : "Tools",
        'updated' : "10 Jun 2022",
        'App_name' : "LomVPN",
        'Package_name' : "com.playpopo.lomvpn",
        'installs' : "10,000,000+",
        'version' : "1.6.300",
    },
    'shoppersstop.shoppersstop' :
    {
        'category' : "Shopping",
        'updated' : "14 Jun 2022",
        'App_name' : "Shoppers Stop Fashion Shopping",
        'Package_name' : "shoppersstop.shoppersstop",
        'installs' : "5,000,000+",
        'version' : "8.5.3",
    },
    '1455492336' :
    {
        'category' : "Games",
        'updated' : "2022-05-23T02:02:39Z",
        'App_name' : "\u30a4\u30b1\u30e1\u30f3\u6e90\u6c0f\u4f1d \u3042\u3084\u304b\u3057\u604b\u3048\u306b\u3057\u3000\u4e59\u5973 \u604b\u611b \u30b2\u30fc\u30e0",
        'Package_name' : "jp.co.cybird.appli.gen",
        'version' : "3.6.0",
        'minimumOsVersion' : "12.0",
    },
    'com.vlv.aravali' :
    {
        'category' : "Entertainment",
        'updated' : "16 Jun 2022",
        'App_name' : "Kuku FM - Audiobooks & Stories",
        'Package_name' : "com.vlv.aravali",
        'installs' : "10,000,000+",
        'version' : "2.8.8",
    },
    'pl.goodie.prd' :
    {
        'category' : "Shopping",
        'updated' : "25 May 2022",
        'App_name' : "goodie \u2013 cashback, gazetki",
        'Package_name' : "pl.goodie.prd",
        'installs' : "1,000,000+",
        'version' : "3.12.0.45",
    },
    'a7nights.com.a7nights.dev' :
    {
        'category' : "Lifestyle",
        'updated' : "21 Apr 2022",
        'App_name' : "7NIGHTS",
        'Package_name' : "a7nights.com.a7nights.dev",
        'installs' : "10,000+",
        'version' : "1.99.2",
    },
    'ru.magnit.express.android' :
    {
        'category' : "Food & Drink",
        'updated' : "08 Jun 2022",
        'App_name' : "\u041c\u0430\u0433\u043d\u0438\u0442: \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0430 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u043e\u0432",
        'Package_name' : "ru.magnit.express.android",
        'installs' : "1,000,000+",
        'version' : "2.5.0",
    },
    'jp.gmolg.realive' :
    {
        'category' : "Games",
        'updated' : "03 Apr 2022",
        'App_name' : "REALIVE!\uff5e\u5e1d\u90fd\u795e\u697d\u821e\u968a\uff5e",
        'Package_name' : "jp.gmolg.realive",
        'installs' : "10,000+",
        'version' : "1.2.30",
    },
    'jp.hotpepper.android.beauty.hair' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Jun 2022",
        'App_name' : "\u30d8\u30a2&\u30d3\u30e5\u30fc\u30c6\u30a3\u30fc\u30b5\u30ed\u30f3\u691c\u7d22/\u30db\u30c3\u30c8\u30da\u30c3\u30d1\u30fc\u30d3\u30e5\u30fc\u30c6\u30a3\u30fc",
        'Package_name' : "jp.hotpepper.android.beauty.hair",
        'installs' : "10,000,000+",
        'version' : "6.51.0",
    },
    'jp.co.i_bec.suteki_happy' :
    {
        'category' : "Social and Communication",
        'updated' : "25 May 2022",
        'App_name' : "\u30cf\u30c3\u30d4\u30fc\u30e1\u30fc\u30eb - \u604b\u6d3b\u30fb\u51fa\u4f1a\u3044\u30fb\u63a2\u3057\u30fb\u5a5a\u6d3b\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea",
        'Package_name' : "jp.co.i_bec.suteki_happy",
        'installs' : "1,000,000+",
        'version' : "9.16.1",
    },
    'com.mailpix.onehourphoto.walgreens.android' :
    {
        'category' : "Entertainment",
        'updated' : "09 Jun 2022",
        'App_name' : "1 Hour Photo: CVS Photo Prints",
        'Package_name' : "com.mailpix.onehourphoto.walgreens.android",
        'installs' : "100,000+",
        'version' : "1.8.2",
    },
    'com.innofinsolutions.instamoney' :
    {
        'category' : "Finance",
        'updated' : "04 May 2022",
        'App_name' : "InstaMoney Personal Loan App",
        'Package_name' : "com.innofinsolutions.instamoney",
        'installs' : "1,000,000+",
        'version' : "4.4.1",
    },
    'com.mktv.steelkiwi.muslimkidstv' :
    {
        'category' : "Education and Books",
        'updated' : "18 Mar 2022",
        'App_name' : "Muslim Kids TV",
        'Package_name' : "com.mktv.steelkiwi.muslimkidstv",
        'installs' : "500,000+",
        'version' : "Varies with device",
    },
    'com.garena.game.ftmid' :
    {
        'category' : "Games",
        'updated' : "26 May 2021",
        'App_name' : "FAIRY TAIL: Forces Unite!",
        'Package_name' : "com.garena.game.ftmid",
        'installs' : "100,000+",
        'version' : "6.11.102",
    },
    'com.startdee.livestudents' :
    {
        'category' : "Education and Books",
        'updated' : "10 Jun 2022",
        'App_name' : "StartDee: \u0e40\u0e02\u0e49\u0e32\u0e43\u0e08\u0e1a\u0e17\u0e40\u0e23\u0e35\u0e22\u0e19\u0e17\u0e38\u0e01\u0e27\u0e34\u0e0a\u0e32",
        'Package_name' : "com.startdee.livestudents",
        'installs' : "1,000,000+",
        'version' : "6.33.0",
    },
    'com.ck.health' :
    {
        'category' : "Health and Fitness",
        'updated' : "02 Jun 2022",
        'App_name' : "Tune H",
        'Package_name' : "com.ck.health",
        'installs' : "100,000+",
        'version' : "1.2.11",
    },
    'com.apnatime' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "apna: Job Search, Alerts India",
        'Package_name' : "com.apnatime",
        'installs' : "10,000,000+",
        'version' : "2022.06.15",
    },
    'com.fanduel.sportsbook' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "FanDuel Sportsbook & Casino",
        'Package_name' : "com.fanduel.sportsbook",
        'installs' : "1,000,000+",
        'version' : "1.50.0",
    },
    'com.mason.wooplus' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "WooPlus - Dating App for Curvy",
        'Package_name' : "com.mason.wooplus",
        'installs' : "1,000,000+",
        'version' : "7.0.7",
    },
    'com.mobisoft.beymen' :
    {
        'category' : "Shopping",
        'updated' : "06 Jun 2022",
        'App_name' : "Beymen",
        'Package_name' : "com.mobisoft.beymen",
        'installs' : "1,000,000+",
        'version' : "3.4.2",
    },
    'com.lottehomeshopping.vc' :
    {
        'category' : "Shopping",
        'updated' : "11 May 2022",
        'App_name' : "wyd(\uc640\uc774\ub4dc) - Play wyd, Live wide",
        'Package_name' : "com.lottehomeshopping.vc",
        'installs' : "500,000+",
        'version' : "1.2.21",
    },
    '1238611143' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-09T19:59:31Z",
        'App_name' : "Lidl Plus",
        'Package_name' : "com.lidl.eci.lidl.plus",
        'version' : "15.4.7",
        'minimumOsVersion' : "12.0",
    },
    'ru.filit.mvideo.b2c' :
    {
        'category' : "Shopping",
        'updated' : "26 May 2022",
        'App_name' : "\u041c.\u0412\u0438\u0434\u0435\u043e: \u044d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u0438\u043a\u0430 \u0438 \u0442\u0435\u0445\u043d\u0438\u043a\u0430",
        'Package_name' : "ru.filit.mvideo.b2c",
        'installs' : "5,000,000+",
        'version' : "2.45.1-r0-b4",
    },
    'com.gonggames.kbo3.aos.google.kr' :
    {
        'category' : "Games",
        'updated' : "29 Apr 2022",
        'App_name' : "\uc774\uc0ac\ub9cc\ub8e822",
        'Package_name' : "com.gonggames.kbo3.aos.google.kr",
        'installs' : "500,000+",
        'version' : "3.0.2",
    },
    'com.eyougame.krninja' :
    {
        'category' : "Games",
        'updated' : "12 Apr 2021",
        'App_name' : "\ubd09\uc778\uc758\uc11c",
        'Package_name' : "com.eyougame.krninja",
        'installs' : "100,000+",
        'version' : "19.2.8",
    },
    '1497699883' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-02-21T16:08:59Z",
        'App_name' : "Joi - Live Video Chat",
        'Package_name' : "video.chat.joi",
        'version' : "1.8.0",
        'minimumOsVersion' : "11.0",
    },
    'com.pulsz' :
    {
        'category' : "Casino",
        'updated' : "27 May 2022",
        'App_name' : "Pulsz: Fun Slots & Casino",
        'Package_name' : "com.pulsz",
        'installs' : "100,000+",
        'version' : "1.49",
    },
    'com.kakao.beauty.hairshop' :
    {
        'category' : "Social and Communication",
        'updated' : "30 May 2022",
        'App_name' : "\uce74\uce74\uc624\ud5e4\uc5b4\uc0f5",
        'Package_name' : "com.kakao.beauty.hairshop",
        'installs' : "1,000,000+",
        'version' : "4.7.5",
    },
    'com.creativeinnovations.mea' :
    {
        'category' : "Entertainment",
        'updated' : "10 Jun 2022",
        'App_name' : "360VUZ: Watch 360\u00b0 Live Stream & VR Video 3D Views",
        'Package_name' : "com.creativeinnovations.mea",
        'installs' : "1,000,000+",
        'version' : "4.17.6.5",
    },
    'com.zenohealth.android' :
    {
        'category' : "Health and Fitness",
        'updated' : "10 Jun 2022",
        'App_name' : "Zeno Health - Generic Pharmacy",
        'Package_name' : "com.zenohealth.android",
        'installs' : "500,000+",
        'version' : "2.1.4",
    },
    'com.zonsapp.taiwanmajong' :
    {
        'category' : "Casino",
        'updated' : "10 May 2022",
        'App_name' : "\u9177\u6e38\u53f0\u7063\u9ebb\u5c07\u201416\u5f35\u9ebb\u5c07\u3001\u8001\u864e\u6a5f\u3001\u62c9\u9738\u3001\u591a\u6b3eSLOT\u5408\u4e00",
        'Package_name' : "com.zonsapp.taiwanmajong",
        'installs' : "100,000+",
        'version' : "1.119",
    },
    'com.zoneyet.gagahi' :
    {
        'category' : "Social and Communication",
        'updated' : "20 May 2022",
        'App_name' : "GagaHi -Global social platform",
        'Package_name' : "com.zoneyet.gagahi",
        'installs' : "1,000,000+",
        'version' : "3.0.8.1",
    },
    'site.tt2.kuuta' :
    {
        'category' : "Lifestyle",
        'updated' : "22 Dec 2021",
        'App_name' : "\u30b8\u30e3\u30f3\u30b0\u30eb",
        'Package_name' : "site.tt2.kuuta",
        'installs' : "100,000+",
        'version' : "2.4.6.0",
    },
    '1211526840' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-05-25T04:02:57Z",
        'App_name' : "\u30de\u30ea\u30c3\u30b7\u30e5(marrish) \u5a5a\u6d3b\u30fb\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea",
        'Package_name' : "com.marrish.app",
        'version' : "v5.4.0",
        'minimumOsVersion' : "13.0",
    },
    'com.fta.rctitv' :
    {
        'category' : "Entertainment",
        'updated' : "04 Apr 2022",
        'App_name' : "RCTI+ Superapp",
        'Package_name' : "com.fta.rctitv",
        'installs' : "10,000,000+",
        'version' : "2.16.2",
    },
    'com.binggo.domino' :
    {
        'category' : "Games",
        'updated' : "13 May 2022",
        'App_name' : "Domino Qiu Qiu Online: 99\uff08QQ\uff09",
        'Package_name' : "com.binggo.domino",
        'installs' : "1,000,000+",
        'version' : "2.22.2.0",
    },
    'com.ta.dcdw.gl' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Defense War",
        'Package_name' : "com.ta.dcdw.gl",
        'installs' : "1,000,000+",
        'version' : "1.21.24",
    },
    'com.perblue.portalquest' :
    {
        'category' : "Games",
        'updated' : "04 Jun 2022",
        'App_name' : "Portal Quest",
        'Package_name' : "com.perblue.portalquest",
        'installs' : "5,000,000+",
        'version' : "5.16",
    },
    'com.sega.sakatsukurtw' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "\u30d7\u30ed\u30b5\u30c3\u30ab\u30fc\u30af\u30e9\u30d6\u3092\u3064\u304f\u308d\u3046\uff01\u30ed\u30fc\u30c9\u30fb\u30c8\u30a5\u30fb\u30ef\u30fc\u30eb\u30c9",
        'Package_name' : "com.sega.sakatsukurtw",
        'installs' : "100,000+",
        'version' : "5.0.1",
    },
    'com.forevernine.ikpure.android' :
    {
        'category' : "Games",
        'updated' : "12 May 2022",
        'App_name' : "Island King Pro",
        'Package_name' : "com.forevernine.ikpure.android",
        'installs' : "10,000,000+",
        'version' : "3.12.0",
    },
    'com.pizzahutuk.orderingApp' :
    {
        'category' : "Food & Drink",
        'updated' : "23 May 2022",
        'App_name' : "Pizza Hut Delivery & Takeaway",
        'Package_name' : "com.pizzahutuk.orderingApp",
        'installs' : "5,000,000+",
        'version' : "3.2.12",
    },
    'in.okcredit.merchant' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "OkCredit : SMB Credit Ledger",
        'Package_name' : "in.okcredit.merchant",
        'installs' : "10,000,000+",
        'version' : "2.62.3",
    },
    'in.swiggy.android' :
    {
        'category' : "Food & Drink",
        'updated' : "15 Jun 2022",
        'App_name' : "Swiggy : Food Delivery & More",
        'Package_name' : "in.swiggy.android",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'co.busha.android' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "Busha: Buy & Sell BTC, ETH",
        'Package_name' : "co.busha.android",
        'installs' : "100,000+",
        'version' : "3.4.1",
    },
    'com.nexon.maplem.japan' :
    {
        'category' : "Games",
        'updated' : "03 Jun 2022",
        'App_name' : "\u30e1\u30a4\u30d7\u30eb\u30b9\u30c8\u30fc\u30ea\u30fcM \u5354\u529b\u30de\u30eb\u30c1\u30d7\u30ec\u30a4/MMORPG",
        'Package_name' : "com.nexon.maplem.japan",
        'installs' : "100,000+",
        'version' : "1.780.3125",
    },
    '1475221397' :
    {
        'category' : "Social and Communication",
        'updated' : "2022-06-14T06:03:49Z",
        'App_name' : "\u30dd\u30b1\u30b3\u30ed\u30c4\u30a4\u30f3\u3000\u304b\u308f\u3044\u3044\u306b\u65b0\u6642\u4ee3\uff01\u30a2\u30d0\u30bf\u30fc\u304d\u305b\u304b\u3048\u30a2\u30d7\u30ea",
        'Package_name' : "jp.cocone.p2",
        'version' : "1.67.2",
        'minimumOsVersion' : "11.0",
    },
    '783409492' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-01T14:36:44Z",
        'App_name' : "enjoei: comprar e vender roupa",
        'Package_name' : "com.enjoei.enjoei",
        'version' : "9.26.0",
        'minimumOsVersion' : "12.0",
    },
    'jp.goodsmile.grandsummonersglobal_android' :
    {
        'category' : "Games",
        'updated' : "24 May 2022",
        'App_name' : "Grand Summoners - Anime RPG",
        'Package_name' : "jp.goodsmile.grandsummonersglobal_android",
        'installs' : "1,000,000+",
        'version' : "3.24.0",
    },
    'com.surfshark.vpnclient.android' :
    {
        'category' : "Tools",
        'updated' : "31 May 2022",
        'App_name' : "Surfshark VPN - Private & Safe",
        'Package_name' : "com.surfshark.vpnclient.android",
        'installs' : "5,000,000+",
        'version' : "2.7.9.9",
    },
    'com.zhanhun.chm' :
    {
        'category' : "Games",
        'updated' : "22 Sep 2021",
        'App_name' : "\u6230\u9748M\uff1a\u9b54\u795e\u52ab",
        'Package_name' : "com.zhanhun.chm",
        'installs' : "100,000+",
        'version' : "1.4.0.0",
    },
    'io.branch.branchster' :
    {
        'category' : "Entertainment",
        'updated' : "01 Jun 2022",
        'App_name' : "Branch Monster Factory",
        'Package_name' : "io.branch.branchster",
        'installs' : "10,000+",
        'version' : "1.9.3",
    },
    '1487453649' :
    {
        'category' : "Education and Books",
        'updated' : "2022-06-20T06:39:11Z",
        'App_name' : "Blossom - Plant Identification",
        'Package_name' : "com.apalonapps.plants",
        'version' : "2.26.0",
        'minimumOsVersion' : "13.0",
    },
    'com.nhn.android.nbooks' :
    {
        'category' : "Education and Books",
        'updated' : "23 May 2022",
        'App_name' : "SERIES",
        'Package_name' : "com.nhn.android.nbooks",
        'installs' : "10,000,000+",
        'version' : "3.26.3",
    },
    'com.grammarly.android.keyboard' :
    {
        'category' : "Tools",
        'updated' : "09 Jun 2022",
        'App_name' : "Grammarly - Grammar Keyboard",
        'Package_name' : "com.grammarly.android.keyboard",
        'installs' : "10,000,000+",
        'version' : "1.9.24.1",
    },
    'com.audible.universal' :
    {
        'category' : "Entertainment",
        'updated' : "13 Apr 2021",
        'App_name' : "Audible Suno",
        'Package_name' : "com.audible.universal",
        'installs' : "5,000,000+",
        'version' : "2.66.1U",
    },
    'com.taskbucks.taskbucks' :
    {
        'category' : "Finance",
        'updated' : "07 Jun 2022",
        'App_name' : "Taskbucks - Earn Rewards",
        'Package_name' : "com.taskbucks.taskbucks",
        'installs' : "10,000,000+",
        'version' : "43.4",
    },
    '1449552940' :
    {
        'category' : "Games",
        'updated' : "2022-06-09T01:13:14Z",
        'App_name' : "\uc77c\uacf1 \uac1c\uc758 \ub300\uc8c4: GRAND CROSS",
        'Package_name' : "com.netmarble.nanakr",
        'version' : "8.4.1",
        'minimumOsVersion' : "11.0",
    },
    '879383901' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-06T20:40:54Z",
        'App_name' : "Dil Mil - South Asian Dating",
        'Package_name' : "co.DilMile.DilMile",
        'version' : "8.5.1",
        'minimumOsVersion' : "13.0",
    },
    'com.abiru.uwakiBest' :
    {
        'category' : "Games",
        'updated' : "02 Oct 2020",
        'App_name' : "\u6d6e\u6c17\u30b5\u30ec\u5973\uff1a\u6d6e\u6c17\uff06\u4e0d\u502b\u306e\u8a3c\u62e0\u63a2\u3057",
        'Package_name' : "com.abiru.uwakiBest",
        'installs' : "500,000+",
        'version' : "1.1.4",
    },
    'com.gmt.trutien' :
    {
        'category' : "Games",
        'updated' : "24 May 2022",
        'App_name' : "Tru Ti\u00ean 3D - Thanh V\u00e2n Ch\u00ed",
        'Package_name' : "com.gmt.trutien",
        'installs' : "1,000,000+",
        'version' : "2.210.0",
    },
    'com.paisabazaar' :
    {
        'category' : "Finance",
        'updated' : "12 Jun 2022",
        'App_name' : "CreditScore, CreditCard, Loans",
        'Package_name' : "com.paisabazaar",
        'installs' : "10,000,000+",
        'version' : "5.16.3",
    },
    'com.ncsoft.lineagew' :
    {
        'category' : "Games",
        'updated' : "11 Jun 2022",
        'App_name' : "Lineage W",
        'Package_name' : "com.ncsoft.lineagew",
        'installs' : "1,000,000+",
        'version' : "1.1.166",
    },
    'com.xhhd.xianyugame.korea' :
    {
        'category' : "Games",
        'updated' : "16 Sep 2021",
        'App_name' : "\uaf43\ud53c\ub294 \ub2ec\ube5b",
        'Package_name' : "com.xhhd.xianyugame.korea",
        'installs' : "500,000+",
        'version' : "1.0",
    },
    'leyi.westgame' :
    {
        'category' : "Games",
        'updated' : "02 Jun 2022",
        'App_name' : "West Game",
        'Package_name' : "leyi.westgame",
        'installs' : "10,000,000+",
        'version' : "4.3.0",
    },
    'com.opodo.reisen' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "16 Jun 2022",
        'App_name' : "Opodo: Book cheap flights",
        'Package_name' : "com.opodo.reisen",
        'installs' : "1,000,000+",
        'version' : "4.462.0",
    },
    'io.btrader.gali' :
    {
        'category' : "Finance",
        'updated' : "14 May 2022",
        'App_name' : "Galileo Inversiones",
        'Package_name' : "io.btrader.gali",
        'installs' : "10,000+",
        'version' : "1.0.53",
    },
    'com.minorfoodgroup.one1112.one' :
    {
        'category' : "Food & Drink",
        'updated' : "18 May 2022",
        'App_name' : "1112 Delivery",
        'Package_name' : "com.minorfoodgroup.one1112.one",
        'installs' : "1,000,000+",
        'version' : "10.3.0.3475",
    },
    'com.monstyr' :
    {
        'category' : "Shopping",
        'updated' : "23 May 2022",
        'App_name' : "MONSTYR: Your 1-stop for Deals",
        'Package_name' : "com.monstyr",
        'installs' : "10,000+",
        'version' : "1.2.8",
    },
    'com.vektor.moov' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "02 Jun 2022",
        'App_name' : "MOOV",
        'Package_name' : "com.vektor.moov",
        'installs' : "1,000,000+",
        'version' : "5.1.5",
    },
    'com.cashkarma.app' :
    {
        'category' : "Entertainment",
        'updated' : "21 Apr 2022",
        'App_name' : "CashKarma Rewards: Gift Cards & Scratch Cards",
        'Package_name' : "com.cashkarma.app",
        'installs' : "1,000,000+",
        'version' : "2.0.18",
    },
    'com.pg.banabak' :
    {
        'category' : "Lifestyle",
        'updated' : "28 Feb 2022",
        'App_name' : "BanaBak \u2013 Kazan",
        'Package_name' : "com.pg.banabak",
        'installs' : "1,000,000+",
        'version' : "1.8.4",
    },
    'com.AlexandrJanashvili.MultiMaze3D' :
    {
        'category' : "Games",
        'updated' : "06 Jun 2022",
        'App_name' : "Multi Maze 3D",
        'Package_name' : "com.AlexandrJanashvili.MultiMaze3D",
        'installs' : "10,000,000+",
        'version' : "1.2.5.1",
    },
    'jp.co.istyle.atcosme' :
    {
        'category' : "Social and Communication",
        'updated' : "13 Jun 2022",
        'App_name' : "@cosme \u5316\u7ca7\u54c1\u30fb\u30b3\u30b9\u30e1\u306e\u30af\u30c1\u30b3\u30df\u30e9\u30f3\u30ad\u30f3\u30b0&\u304a\u8cb7\u7269",
        'Package_name' : "jp.co.istyle.atcosme",
        'installs' : "500,000+",
        'version' : "4.79.0",
    },
    'com.convert.app' :
    {
        'category' : "Lifestyle",
        'updated' : "08 Jun 2022",
        'App_name' : "neobank | Payment Extension",
        'Package_name' : "com.convert.app",
        'installs' : "50,000+",
        'version' : "2.0.6",
    },
    'com.confirmtkt.lite' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "16 Jun 2022",
        'App_name' : "ConfirmTkt - Train Booking",
        'Package_name' : "com.confirmtkt.lite",
        'installs' : "10,000,000+",
        'version' : "7.4.16",
    },
    'com.ulugames.honors.google' :
    {
        'category' : "Games",
        'updated' : "15 Dec 2021",
        'App_name' : "\uae00\ub85c\ub9ac",
        'Package_name' : "com.ulugames.honors.google",
        'installs' : "1,000,000+",
        'version' : "1.0.146",
    },
    'com.crazylabs.hair.dye.challenge' :
    {
        'category' : "Games",
        'updated' : "01 Jun 2022",
        'App_name' : "Hair Dye",
        'Package_name' : "com.crazylabs.hair.dye.challenge",
        'installs' : "50,000,000+",
        'version' : "1.5.6.0",
    },
    'jp.deai.pm2.spapp' :
    {
        'category' : "Social and Communication",
        'updated' : "26 May 2022",
        'App_name' : "\u51fa\u4f1a\u3044\u306ePCMAX-\u30de\u30c3\u30c1\u30f3\u30b0\u30a2\u30d7\u30ea\u3067\u5a5a\u6d3b\u3001\u604b\u6d3b\u306b\u53cb\u9054\u4f5c\u308a",
        'Package_name' : "jp.deai.pm2.spapp",
        'installs' : "100,000+",
        'version' : "5.4",
    },
    'com.linkdokter.halodoc.android' :
    {
        'category' : "Health and Fitness",
        'updated' : "13 Jun 2022",
        'App_name' : "Halodoc: Doctors & Medicines",
        'Package_name' : "com.linkdokter.halodoc.android",
        'installs' : "10,000,000+",
        'version' : "13.200",
    },
    'com.lilithgames.wgame.android.kr' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "\uc6cc\ud328\uc2a4",
        'Package_name' : "com.lilithgames.wgame.android.kr",
        'installs' : "100,000+",
        'version' : "4.32.10",
    },
    'com.gpn.azs' :
    {
        'category' : "Utilities",
        'updated' : "08 Jun 2022",
        'App_name' : "\u0410\u0417\u0421 \u0413\u0430\u0437\u043f\u0440\u043e\u043c\u043d\u0435\u0444\u0442\u044c",
        'Package_name' : "com.gpn.azs",
        'installs' : "1,000,000+",
        'version' : "3.7.4",
    },
    'com.blancozone.xwm.gp' :
    {
        'category' : "Games",
        'updated' : "28 Apr 2021",
        'App_name' : "\u65b0\u5e0c\u671b",
        'Package_name' : "com.blancozone.xwm.gp",
        'installs' : "100,000+",
        'version' : "1.3.25",
    },
    '1369521645' :
    {
        'category' : "Games",
        'updated' : "2022-06-07T17:10:50Z",
        'App_name' : "Words Of Wonders: Crossword",
        'Package_name' : "com.fugo.wow",
        'version' : "3.7.3",
        'minimumOsVersion' : "12.0",
    },
    'de.merkur24.universalapp' :
    {
        'category' : "Casino",
        'updated' : "10 Jun 2022",
        'App_name' : "Merkur24 \u2013 Slots & Casino",
        'Package_name' : "de.merkur24.universalapp",
        'installs' : "1,000,000+",
        'version' : "4.13.36",
    },
    'com.hnsmall' :
    {
        'category' : "Shopping",
        'updated' : "16 Jun 2022",
        'App_name' : "\ud648\uc564\uc1fc\ud551 -\uc1fc\ud551\uc5d0 \uc9c4\uc2ec\uc744 \ub2e4\ud558\ub2e4, \ud61c\ud0dd\uc744 \ub354\ud558\ub2e4.",
        'Package_name' : "com.hnsmall",
        'installs' : "10,000,000+",
        'version' : "3.8.3",
    },
    'pesocash.loan.cash.prestamo.online' :
    {
        'category' : "Finance",
        'updated' : "23 Mar 2022",
        'App_name' : "PesoCash-Pr\u00e9stamo de cr\u00e9dito",
        'Package_name' : "pesocash.loan.cash.prestamo.online",
        'installs' : "100,000+",
        'version' : "1.4.4",
    },
    'com.rdx.play' :
    {
        'category' : "Social and Communication",
        'updated' : "14 Apr 2022",
        'App_name' : "RDX Play | Short Video App",
        'Package_name' : "com.rdx.play",
        'installs' : "100,000+",
        'version' : "1.1.3.27",
    },
    'com.sixt.reservation' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "13 Jun 2022",
        'App_name' : "SIXT rent. share. ride. plus.",
        'Package_name' : "com.sixt.reservation",
        'installs' : "1,000,000+",
        'version' : "9.75.1-11566",
    },
    'com.splash.esportsgames' :
    {
        'category' : "Games",
        'updated' : "14 Dec 2021",
        'App_name' : "Esports Games Trivia App",
        'Package_name' : "com.splash.esportsgames",
        'installs' : "100,000+",
        'version' : "1.11.0",
    },
    '990229433' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-15T14:10:35Z",
        'App_name' : "Qustodio Kids App",
        'Package_name' : "com.qustodio.mdm.app.family.pro",
        'version' : "181.12.0",
        'minimumOsVersion' : "12.1",
    },
    'com.zynga.livepoker' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "Zynga Poker- Texas Holdem Game",
        'Package_name' : "com.zynga.livepoker",
        'installs' : "50,000,000+",
        'version' : "22.38.2314",
    },
    'com.azuragame.oop' :
    {
        'category' : "Games",
        'updated' : "31 May 2022",
        'App_name' : "\u96f2\u5c71\u4e4b\u7d04",
        'Package_name' : "com.azuragame.oop",
        'installs' : "100,000+",
        'version' : "1.311.6360",
    },
    'video.reface.app' :
    {
        'category' : "Entertainment",
        'updated' : "16 Jun 2022",
        'App_name' : "Reface: Funny face swap videos",
        'Package_name' : "video.reface.app",
        'installs' : "100,000,000+",
        'version' : "3.0.1",
    },
    '432483400' :
    {
        'category' : "Food & Drink",
        'updated' : "2022-06-07T06:40:23Z",
        'App_name' : "Domino's Pizza T\u00fcrkiye",
        'Package_name' : "com.magiclick.DominosAciktik",
        'version' : "6.2.1",
        'minimumOsVersion' : "10.0",
    },
    '1523226013' :
    {
        'category' : "Games",
        'updated' : "2021-05-13T09:41:47Z",
        'App_name' : "Smooth Wheel",
        'Package_name' : "com.superspider.smoothwheel",
        'version' : "1.1.6",
        'minimumOsVersion' : "10.0",
    },
    'com.gaana' :
    {
        'category' : "Entertainment",
        'updated' : "15 Jun 2022",
        'App_name' : "Gaana Hindi Song Music App",
        'Package_name' : "com.gaana",
        'installs' : "100,000,000+",
        'version' : "8.35.6",
    },
    'com.miHoYo.bh3rdJP' :
    {
        'category' : "Games",
        'updated' : "13 May 2022",
        'App_name' : "\u5d29\u58ca3rd",
        'Package_name' : "com.miHoYo.bh3rdJP",
        'installs' : "1,000,000+",
        'version' : "5.7.0",
    },
    'de.autodoc.gmbh' :
    {
        'category' : "Shopping",
        'updated' : "06 Jun 2022",
        'App_name' : "AUTODOC: buy car parts online",
        'Package_name' : "de.autodoc.gmbh",
        'installs' : "10,000,000+",
        'version' : "1.9.9",
    },
    'com.crazylabs.tie.dye.art' :
    {
        'category' : "Games",
        'updated' : "06 May 2022",
        'App_name' : "Tie Dye",
        'Package_name' : "com.crazylabs.tie.dye.art",
        'installs' : "100,000,000+",
        'version' : "3.7.4.0",
    },
    'com.func.colorpieceout' :
    {
        'category' : "Games",
        'updated' : "22 Apr 2022",
        'App_name' : "\u30ab\u30e9\u30fc\u30d4\u30fc\u30bd\u30a6\u30c8-\u8b0e\u89e3\u304d\u00d7\u30de\u30c3\u30c13\u30d1\u30ba\u30eb\u30b2\u30fc\u30e0",
        'Package_name' : "com.func.colorpieceout",
        'installs' : "100,000+",
        'version' : "1.11.10",
    },
    'jp.gungho.pad' :
    {
        'category' : "Games",
        'updated' : "25 May 2022",
        'App_name' : "\u30d1\u30ba\u30eb\uff06\u30c9\u30e9\u30b4\u30f3\u30ba(Puzzle & Dragons)",
        'Package_name' : "jp.gungho.pad",
        'installs' : "10,000,000+",
        'version' : "20.0.0",
    },
    'com.ciceksepeti.lolaflora' :
    {
        'category' : "Shopping",
        'updated' : "02 Jun 2022",
        'App_name' : "Mizu - Gift & Flower Delivery",
        'Package_name' : "com.ciceksepeti.lolaflora",
        'installs' : "500,000+",
        'version' : "2.5.3",
    },
    'com.shghn.sg310' :
    {
        'category' : "Games",
        'updated' : "24 May 2021",
        'App_name' : "Tam Qu\u1ed1c Li\u00ean Minh SohaGame",
        'Package_name' : "com.shghn.sg310",
        'installs' : "50,000+",
        'version' : "10.6.0.0",
    },
    'com.creditkarma.mobile' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "Credit Karma",
        'Package_name' : "com.creditkarma.mobile",
        'installs' : "10,000,000+",
        'version' : "Varies with device",
    },
    'com.smallgiantgames.empires' :
    {
        'category' : "Games",
        'updated' : "15 Jun 2022",
        'App_name' : "Empires & Puzzles: Match-3 RPG",
        'Package_name' : "com.smallgiantgames.empires",
        'installs' : "50,000,000+",
        'version' : "49.0.1",
    },
    'com.humart.trost2' :
    {
        'category' : "Health and Fitness",
        'updated' : "03 Jun 2022",
        'App_name' : "\ud2b8\ub85c\uc2a4\ud2b8 - \uc81c\uc2dc\uc758 \uba58\ud0c8\ucf00\uc5b4 \uc571",
        'Package_name' : "com.humart.trost2",
        'installs' : "100,000+",
        'version' : "4.38.0",
    },
    'com.byjus.thelearningapp' :
    {
        'category' : "Education and Books",
        'updated' : "06 Jun 2022",
        'App_name' : "BYJU'S \u2013 The Learning App",
        'Package_name' : "com.byjus.thelearningapp",
        'installs' : "100,000,000+",
        'version' : "10.4.0.13552",
    },
    'com.netmarble.revolutionthm' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "Lineage2 Revolution",
        'Package_name' : "com.netmarble.revolutionthm",
        'installs' : "5,000,000+",
        'version' : "1.34.12",
    },
    '1263814273' :
    {
        'category' : "Games",
        'updated' : "2022-06-08T04:02:52Z",
        'App_name' : "Neds - Sport & Race Betting",
        'Package_name' : "au.com.nedsintl.neds",
        'version' : "8.36.0",
        'minimumOsVersion' : "12.0",
    },
    'com.nft.maker' :
    {
        'category' : "Entertainment",
        'updated' : "15 Mar 2022",
        'App_name' : "NFT Maker: Create a NFT!",
        'Package_name' : "com.nft.maker",
        'installs' : "50,000+",
        'version' : "1.38",
    },
    'com.muy.app' :
    {
        'category' : "Food & Drink",
        'updated' : "10 May 2022",
        'App_name' : "Muy App",
        'Package_name' : "com.muy.app",
        'installs' : "100,000+",
        'version' : "2.1.8",
    },
    'id.salestock.mobile' :
    {
        'category' : "Shopping",
        'updated' : "06 Apr 2020",
        'App_name' : "Sorabel - Belanja Baju, Coba Dulu Baru Bayar",
        'Package_name' : "id.salestock.mobile",
        'installs' : "10,000,000+",
        'version' : "0.6.4",
    },
    'com.priceminister.buyerapp' :
    {
        'category' : "Shopping",
        'updated' : "15 Jun 2022",
        'App_name' : "Rakuten Achat & Vente en ligne",
        'Package_name' : "com.priceminister.buyerapp",
        'installs' : "1,000,000+",
        'version' : "8.10.1",
    },
    'hk.com.joybomb.bbm3' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "\u5168\u6c11\u6253\u68d2\u7403 Pro",
        'Package_name' : "hk.com.joybomb.bbm3",
        'installs' : "100,000+",
        'version' : "2.1.0",
    },
    'com.hpbr.wavely' :
    {
        'category' : "Finance",
        'updated' : "13 Jun 2022",
        'App_name' : "Wavely: Match, Chat, Hired",
        'Package_name' : "com.hpbr.wavely",
        'installs' : "100,000+",
        'version' : "4.8",
    },
    'zenjob.android' :
    {
        'category' : "Finance",
        'updated' : "08 Jun 2022",
        'App_name' : "Zenjob - Flexible Nebenjobs",
        'Package_name' : "zenjob.android",
        'installs' : "100,000+",
        'version' : "2022.10.2",
    },
    'com.kingnetgame.tszbgp.kr' :
    {
        'category' : "Games",
        'updated' : "15 Oct 2021",
        'App_name' : "\ucc9c\uc0c1\uc758 \ud788\uc5b4\ub85c:AFK RPG",
        'Package_name' : "com.kingnetgame.tszbgp.kr",
        'installs' : "50,000+",
        'version' : "1.0.247",
    },
    'io.wylr.joko' :
    {
        'category' : "Shopping",
        'updated' : "11 Jun 2022",
        'App_name' : "Joko",
        'Package_name' : "io.wylr.joko",
        'installs' : "500,000+",
        'version' : "4.4.0",
    },
    'com.EmasDigi' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "Pluang - Safe Online Investing",
        'Package_name' : "com.EmasDigi",
        'installs' : "1,000,000+",
        'version' : "4.9.9",
    },
    'com.jago.digitalBanking' :
    {
        'category' : "Finance",
        'updated' : "06 Jun 2022",
        'App_name' : "Jago",
        'Package_name' : "com.jago.digitalBanking",
        'installs' : "1,000,000+",
        'version' : "7.5.1",
    },
    'com.tavour.app' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "Tavour",
        'Package_name' : "com.tavour.app",
        'installs' : "100,000+",
        'version' : "6.50.0",
    },
    'com.bluesom.wildGirls' :
    {
        'category' : "Games",
        'updated' : "25 Jun 2021",
        'App_name' : "\uc57c\uc0dd\uc18c\ub140",
        'Package_name' : "com.bluesom.wildGirls",
        'installs' : "50,000+",
        'version' : "1.116",
    },
    'com.eyewa.app' :
    {
        'category' : "Shopping",
        'updated' : "11 Oct 2021",
        'App_name' : "eyewa - Contact lenses, Sunglasses & Eyeglasses.",
        'Package_name' : "com.eyewa.app",
        'installs' : "100,000+",
        'version' : "3.6.0",
    },
    'com.booking' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "15 Jun 2022",
        'App_name' : "Booking.com: Hotels and more",
        'Package_name' : "com.booking",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.app.rehlat' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "13 Jun 2022",
        'App_name' : "Rehlat - Flights & Hotels",
        'Package_name' : "com.app.rehlat",
        'installs' : "500,000+",
        'version' : "8.8.7",
    },
    'com.vtcmobile.nhkt' :
    {
        'category' : "Games",
        'updated' : "26 Jan 2022",
        'App_name' : "Ngh\u1ecbch Thi\u00ean Ki\u1ebfm Th\u1ebf VTC",
        'Package_name' : "com.vtcmobile.nhkt",
        'installs' : "500,000+",
        'version' : "Varies with device",
    },
    'bingo.town.free.wild.journey' :
    {
        'category' : "Games",
        'updated' : "16 May 2022",
        'App_name' : "Bingo Town-Online Bingo Games",
        'Package_name' : "bingo.town.free.wild.journey",
        'installs' : "100,000+",
        'version' : "1.8.8.2580",
    },
    'com.eurekastudio.drawbridge' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "Draw Bridge",
        'Package_name' : "com.eurekastudio.drawbridge",
        'installs' : "10,000,000+",
        'version' : "1.3.4",
    },
    'io.cleanfox.android' :
    {
        'category' : "Tools",
        'updated' : "02 May 2022",
        'App_name' : "Cleanfox - Mail & Spam Cleaner",
        'Package_name' : "io.cleanfox.android",
        'installs' : "1,000,000+",
        'version' : "3.24.3-1000",
    },
    'jp.co.liica.machibure' :
    {
        'category' : "Games",
        'updated' : "02 Jul 2020",
        'App_name' : "\u30de\u30c1\u30ac\u30a4\u30d6\u30ec\u30a4\u30ab\u30fc Re:Quest(\u30ea\u30af\u30a8\u30b9\u30c8)",
        'Package_name' : "jp.co.liica.machibure",
        'installs' : "10,000+",
        'version' : "2.0.20",
    },
    'com.eskyfun.sgsmjzgp' :
    {
        'category' : "Games",
        'updated' : "25 Jan 2022",
        'App_name' : "\u4e09\u570b\u6bba\u540d\u5c07\u50b3",
        'Package_name' : "com.eskyfun.sgsmjzgp",
        'installs' : "500,000+",
        'version' : "Varies with device",
    },
    'com.treebo.starscream' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "11 Jun 2022",
        'App_name' : "Treebo: Hotel Booking App",
        'Package_name' : "com.treebo.starscream",
        'installs' : "1,000,000+",
        'version' : "6.6.31",
    },
    '1087952357' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-06-13T11:27:50Z",
        'App_name' : "NIVEA App",
        'Package_name' : "de.nivea.nivea",
        'version' : "3.7.0",
        'minimumOsVersion' : "10.0",
    },
    'ru.dublgis.dgismobile' :
    {
        'category' : "Utilities",
        'updated' : "24 May 2022",
        'App_name' : "2GIS: Offline map & Navigation",
        'Package_name' : "ru.dublgis.dgismobile",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'br.com.drogasil' :
    {
        'category' : "Health and Fitness",
        'updated' : "15 Jun 2022",
        'App_name' : "Drogasil: Drogaria Online 24h",
        'Package_name' : "br.com.drogasil",
        'installs' : "5,000,000+",
        'version' : "4.45.05",
    },
    'com.camelgames.aoz' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "Age of Origins",
        'Package_name' : "com.camelgames.aoz",
        'installs' : "10,000,000+",
        'version' : "1.2.152",
    },
    'com.stitcher.app' :
    {
        'category' : "Entertainment",
        'updated' : "15 Jun 2022",
        'App_name' : "Stitcher - Podcast Player",
        'Package_name' : "com.stitcher.app",
        'installs' : "5,000,000+",
        'version' : "10.32.778",
    },
    'com.spikedominion.chef' :
    {
        'category' : "Games",
        'updated' : "17 Aug 2021",
        'App_name' : "Merge Chef Adventure",
        'Package_name' : "com.spikedominion.chef",
        'installs' : "50,000+",
        'version' : "2.17.1",
    },
    'com.mentormate.android.inboxdollars' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "InboxDollars",
        'Package_name' : "com.mentormate.android.inboxdollars",
        'installs' : "5,000,000+",
        'version' : "3.11.1",
    },
    'com.vtcmobile.ctkn' :
    {
        'category' : "Games",
        'updated' : "07 Mar 2022",
        'App_name' : "Chi\u1ebfn Th\u1ea7n K\u1ef7 Nguy\u00ean: Nh\u1eadp Ma",
        'Package_name' : "com.vtcmobile.ctkn",
        'installs' : "500,000+",
        'version' : "21030416(313756.322178)",
    },
    '829587759' :
    {
        'category' : "Education and Books",
        'updated' : "2022-06-08T12:34:54Z",
        'App_name' : "Babbel \u2013 Aprender ingl\u00e9s y m\u00e1s",
        'Package_name' : "com.babbel.babbelMulti",
        'version' : "21.0.0",
        'minimumOsVersion' : "12.0",
    },
    'com.mistplay.mistplay' :
    {
        'category' : "Entertainment",
        'updated' : "16 Jun 2022",
        'App_name' : "MISTPLAY: Play to earn rewards",
        'Package_name' : "com.mistplay.mistplay",
        'installs' : "10,000,000+",
        'version' : "5.35",
    },
    'com.varomoney.bank' :
    {
        'category' : "Finance",
        'updated' : "15 Jun 2022",
        'App_name' : "Varo Bank: Mobile Banking",
        'Package_name' : "com.varomoney.bank",
        'installs' : "1,000,000+",
        'version' : "2.34.0",
    },
    '397585390' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-11T14:34:36Z",
        'App_name' : "Migros: Sanal Market - Hemen",
        'Package_name' : "tr.com.sanalmarket.MigrosSanalMarketMobil",
        'version' : "10.0.1",
        'minimumOsVersion' : "11.0",
    },
    'co.irl.android' :
    {
        'category' : "Social and Communication",
        'updated' : "17 Jun 2022",
        'App_name' : "IRL - Social Messenger",
        'Package_name' : "co.irl.android",
        'installs' : "5,000,000+",
        'version' : "4.25.1",
    },
    'com.gigagames.dragonwar.android' :
    {
        'category' : "Games",
        'updated' : "12 Dec 2020",
        'App_name' : "GIGA Dragon War",
        'Package_name' : "com.gigagames.dragonwar.android",
        'installs' : "50,000+",
        'version' : "1.0.3",
    },
    '579581125' :
    {
        'category' : "Not available",
        'updated' : "2022-06-20T01:29:19Z",
        'App_name' : "\u30b9\u30de\u30fc\u30c8\u30cb\u30e5\u30fc\u30b9\u3000\u30cb\u30e5\u30fc\u30b9\u3084\u5929\u6c17\u30fb\u5929\u6c17\u4e88\u5831\u3001\u96e8\u96f2\u30ec\u30fc\u30c0\u30fc\u3082",
        'Package_name' : "jp.gocro.SmartNews",
        'version' : "22.6.30",
        'minimumOsVersion' : "13.0",
    },
    'com.eyougame.xjhx.en' :
    {
        'category' : "Games",
        'updated' : "13 Jun 2022",
        'App_name' : "Gaia Odyssey",
        'Package_name' : "com.eyougame.xjhx.en",
        'installs' : "1,000,000+",
        'version' : "35.0",
    },
    'com.lifeomic.lifeextend' :
    {
        'category' : "Health and Fitness",
        'updated' : "16 Jun 2022",
        'App_name' : "Wellness Tracker | LIFE Extend",
        'Package_name' : "com.lifeomic.lifeextend",
        'installs' : "100,000+",
        'version' : "5.7.14",
    },
    'kr.co.sincetimes.gxhgnew' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "\uc18c\ub140X\ud5cc\ud130",
        'Package_name' : "kr.co.sincetimes.gxhgnew",
        'installs' : "100,000+",
        'version' : "1.0.12",
    },
    'kr.co.station3.dabang' :
    {
        'category' : "House & Home",
        'updated' : "13 Jun 2022",
        'App_name' : "\ub2e4\ubc29 \u2013 \ub300\ud55c\ubbfc\uad6d \ub300\ud45c \ubd80\ub3d9\uc0b0 \uc571",
        'Package_name' : "kr.co.station3.dabang",
        'installs' : "10,000,000+",
        'version' : "4.10.0",
    },
    'jp.co.rakuten.fashionAndroid' :
    {
        'category' : "Shopping",
        'updated' : "19 May 2022",
        'App_name' : "Rakuten Fashion \u697d\u5929\u306e\u30d5\u30a1\u30c3\u30b7\u30e7\u30f3\u901a\u8ca9\u30a2\u30d7\u30ea",
        'Package_name' : "jp.co.rakuten.fashionAndroid",
        'installs' : "1,000,000+",
        'version' : "2.3.0",
    },
    'com.org.app2wintrade' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "2WinTrade-Platform for Trader",
        'Package_name' : "com.org.app2wintrade",
        'installs' : "1,000,000+",
        'version' : "1.8.0",
    },
    'com.oyo.consumer' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "03 Jun 2022",
        'App_name' : "OYO: Hotel Booking App",
        'Package_name' : "com.oyo.consumer",
        'installs' : "50,000,000+",
        'version' : "5.10.5",
    },
    'com.mintq.gocash' :
    {
        'category' : "Finance",
        'updated' : "24 May 2022",
        'App_name' : "UKU - Pinjaman Uang Online",
        'Package_name' : "com.mintq.gocash",
        'installs' : "5,000,000+",
        'version' : "5.0.9",
    },
    '1214763610' :
    {
        'category' : "Games",
        'updated' : "2022-05-26T21:53:18Z",
        'App_name' : "Cooking Diary\u00ae Koch simolator",
        'Package_name' : "com.mytonallc.cookingdiary",
        'version' : "2.1.1",
        'minimumOsVersion' : "10.0",
    },
    'com.mobisoft.boyner' :
    {
        'category' : "Shopping",
        'updated' : "07 Jun 2022",
        'App_name' : "Boyner",
        'Package_name' : "com.mobisoft.boyner",
        'installs' : "5,000,000+",
        'version' : "6.0.5",
    },
    'com.linecorp.LGBB3' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "LINE HELLO BT21 Season 2",
        'Package_name' : "com.linecorp.LGBB3",
        'installs' : "1,000,000+",
        'version' : "2.5.0",
    },
    'com.toogud.android' :
    {
        'category' : "Shopping",
        'updated' : "04 Mar 2021",
        'App_name' : "2GUD - Naye Jaisa, Naye se Sasta",
        'Package_name' : "com.toogud.android",
        'installs' : "5,000,000+",
        'version' : "3.3",
    },
    'com.haraj.app' :
    {
        'category' : "Shopping",
        'updated' : "01 Jun 2022",
        'App_name' : "\u062d\u0631\u0627\u062c - Haraj",
        'Package_name' : "com.haraj.app",
        'installs' : "5,000,000+",
        'version' : "4.6.13",
    },
    'com.hobby2.talk' :
    {
        'category' : "Social and Communication",
        'updated' : "31 May 2022",
        'App_name' : "Hobbytalk - Making friends",
        'Package_name' : "com.hobby2.talk",
        'installs' : "50,000+",
        'version' : "2.1.7",
    },
    'com.igs.kovtw' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "\u4e09\u570b\u6230\u7d00 - \u7d93\u5178\u8857\u6a5f\u7121\u96d9\u7af6\u6280",
        'Package_name' : "com.igs.kovtw",
        'installs' : "100,000+",
        'version' : "2.16.0.0",
    },
    'net.kilimall.shop' :
    {
        'category' : "Shopping",
        'updated' : "31 May 2022",
        'App_name' : "Kilimall - Affordable Shopping",
        'Package_name' : "net.kilimall.shop",
        'installs' : "1,000,000+",
        'version' : "4.6.1",
    },
    '741949324' :
    {
        'category' : "Games",
        'updated' : "2022-03-16T22:59:11Z",
        'App_name' : "\uc138\ube10\ub098\uc774\uce20",
        'Package_name' : "com.cjenm.sknights",
        'version' : "5.8.10",
        'minimumOsVersion' : "9.0",
    },
    'jp.ne.donuts.tanshanotora' :
    {
        'category' : "Games",
        'updated' : "10 May 2022",
        'App_name' : "\u66b4\u8d70\u5217\u4f1d \u5358\u8eca\u306e\u864e \u30e4\u30f3\u30ad\u30fc\uff06\u4e0d\u826f\u306e\u30ac\u30c1\u30f3\u30b3\u55a7\u5629\u30d0\u30c8\u30eb\u30b2\u30fc\u30e0",
        'Package_name' : "jp.ne.donuts.tanshanotora",
        'installs' : "1,000,000+",
        'version' : "2.4.2",
    },
    '530488359' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "2022-06-20T04:39:31Z",
        'App_name' : "MakeMyTrip Hotels, Flight, Bus",
        'Package_name' : "com.Iphone.MMT",
        'version' : "8.7.1",
        'minimumOsVersion' : "13.0",
    },
    'com.icatchonline' :
    {
        'category' : "Games",
        'updated' : "03 Mar 2022",
        'App_name' : "iCatchONLINE-Online Crane Game",
        'Package_name' : "com.icatchonline",
        'installs' : "100,000+",
        'version' : "3.2.0",
    },
    'com.taling' :
    {
        'category' : "Education and Books",
        'updated' : "08 Jun 2022",
        'App_name' : "\ud0c8\uc789 - \ubc30\uc6c0\uc744 \uc7ac\ubc0c\uac8c",
        'Package_name' : "com.taling",
        'installs' : "500,000+",
        'version' : "3.6.6",
    },
    'snapp.snappfid.monoprix' :
    {
        'category' : "Shopping",
        'updated' : "23 May 2022",
        'App_name' : "M' Monoprix",
        'Package_name' : "snapp.snappfid.monoprix",
        'installs' : "500,000+",
        'version' : "5.0.10",
    },
    'com.lilithgame.hgame.gp.tw' :
    {
        'category' : "Games",
        'updated' : "09 Jun 2022",
        'App_name' : "\u528d\u8207\u9060\u5f81",
        'Package_name' : "com.lilithgame.hgame.gp.tw",
        'installs' : "500,000+",
        'version' : "1.91.01",
    },
    'br.com.martinlabs.apptite.android' :
    {
        'category' : "Shopping",
        'updated' : "10 Jun 2022",
        'App_name' : "Apptite - Comida Artesanal",
        'Package_name' : "br.com.martinlabs.apptite.android",
        'installs' : "100,000+",
        'version' : "a5.0.4",
    },
    'com.se.zsj' :
    {
        'category' : "Games",
        'updated' : "28 May 2021",
        'App_name' : "Armored God",
        'Package_name' : "com.se.zsj",
        'installs' : "1,000,000+",
        'version' : "1.0.9",
    },
    '1159292704' :
    {
        'category' : "Games",
        'updated' : "2022-06-14T01:03:21Z",
        'App_name' : "Kingdom Story: \u3054\u3063\u3064\u4e09\u56fd\u95a2\u897f\u6226\u8a18",
        'Package_name' : "com.nhnent.SK10392",
        'version' : "3.00.0",
        'minimumOsVersion' : "9.0",
    },
    'com.westernunion.moneytransferr3app.kw' :
    {
        'category' : "Finance",
        'updated' : "09 Jun 2022",
        'App_name' : "WesternUnion KW Money Transfer",
        'Package_name' : "com.westernunion.moneytransferr3app.kw",
        'installs' : "100,000+",
        'version' : "1.228.0",
    },
    '1471935300' :
    {
        'category' : "Games",
        'updated' : "2021-05-18T09:10:47Z",
        'App_name' : "\uc774\ud130:\ub358\uc804\uc758 \ud3ec\uc2dd\uc790",
        'Package_name' : "com.vqw.krlh",
        'version' : "3.1.4",
        'minimumOsVersion' : "9.0",
    },
    'com.voltage.joshige.tenka' :
    {
        'category' : "Games",
        'updated' : "09 May 2022",
        'App_name' : "\u5929\u4e0b\u7d71\u4e00\u604b\u306e\u4e71\u3000Love Ballad\u3000\u6226\u56fd\u6b66\u5c06\u3068\u604b\u3057\u3066",
        'Package_name' : "com.voltage.joshige.tenka",
        'installs' : "500,000+",
        'version' : "8.7.0",
    },
    'com.yum.pizzahut' :
    {
        'category' : "Food & Drink",
        'updated' : "06 May 2022",
        'App_name' : "Pizza Hut - Food Delivery & Takeout",
        'Package_name' : "com.yum.pizzahut",
        'installs' : "10,000,000+",
        'version' : "5.26.0",
    },
    'net.ap.connect.android' :
    {
        'category' : "Social and Communication",
        'updated' : "10 Jun 2022",
        'App_name' : "\u30d3\u30c7\u30aa\u901a\u8a71\u30a2\u30d7\u30eaConnect-\u533f\u540d\u30d3\u30c7\u30aa\u30c1\u30e3\u30c3\u30c8\u3067\u6687\u3064\u3076\u3057",
        'Package_name' : "net.ap.connect.android",
        'installs' : "500,000+",
        'version' : "13.9",
    },
    'de.taxfix' :
    {
        'category' : "Finance",
        'updated' : "09 Jun 2022",
        'App_name' : "Taxfix German tax declaration",
        'Package_name' : "de.taxfix",
        'installs' : "1,000,000+",
        'version' : "1.182.0",
    },
    'com.savorly' :
    {
        'category' : "Food & Drink",
        'updated' : "13 Jun 2022",
        'App_name' : "Savorly: Home Cooked Meals",
        'Package_name' : "com.savorly",
        'installs' : "50,000+",
        'version' : "4.0.76",
    },
    'com.kanziapp' :
    {
        'category' : "Shopping",
        'updated' : "15 Dec 2021",
        'App_name' : "KANZI",
        'Package_name' : "com.kanziapp",
        'installs' : "10,000+",
        'version' : "14.2.0",
    },
    'com.igg.android.dressuptimeprincess' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Time Princess: Story Traveler",
        'Package_name' : "com.igg.android.dressuptimeprincess",
        'installs' : "10,000,000+",
        'version' : "1.16.1",
    },
    'com.xp101.mtonline' :
    {
        'category' : "Games",
        'updated' : "25 Feb 2021",
        'App_name' : "Era of Legends - Magic MMORPG",
        'Package_name' : "com.xp101.mtonline",
        'installs' : "1,000,000+",
        'version' : "9.0.0.0",
    },
    'tr.com.instreet' :
    {
        'category' : "Shopping",
        'updated' : "25 May 2022",
        'App_name' : "Instreet",
        'Package_name' : "tr.com.instreet",
        'installs' : "1,000,000+",
        'version' : "3.0.5",
    },
    'com.global.live' :
    {
        'category' : "Entertainment",
        'updated' : "15 Jun 2022",
        'App_name' : "Hiya - Group Voice Chat",
        'Package_name' : "com.global.live",
        'installs' : "5,000,000+",
        'version' : "3.26.1",
    },
    '658511662' :
    {
        'category' : "Games",
        'updated' : "2022-05-25T15:02:19Z",
        'App_name' : "\u30e2\u30f3\u30b9\u30bf\u30fc\u30b9\u30c8\u30e9\u30a4\u30af",
        'Package_name' : "jp.co.mixi.monsterstrike",
        'version' : "23.2.0",
        'minimumOsVersion' : "10.3",
    },
    'com.globe.gcash.android' :
    {
        'category' : "Finance",
        'updated' : "30 May 2022",
        'App_name' : "GCash - Buy Load, Pay Bills, Send Money",
        'Package_name' : "com.globe.gcash.android",
        'installs' : "50,000,000+",
        'version' : "5.53.1",
    },
    'com.unicostudio.braintest2new' :
    {
        'category' : "Games",
        'updated' : "29 Apr 2022",
        'App_name' : "Brain Test 2: Tricky Stories",
        'Package_name' : "com.unicostudio.braintest2new",
        'installs' : "50,000,000+",
        'version' : "1.9.0",
    },
    'com.meteoapplication.meteoapp' :
    {
        'category' : "Utilities",
        'updated' : "20 Oct 2021",
        'App_name' : "Cute Weather",
        'Package_name' : "com.meteoapplication.meteoapp",
        'installs' : "10,000+",
        'version' : "1.2.1",
    },
    'jp.jig.product.whowatch.viewer' :
    {
        'category' : "Entertainment",
        'updated' : "02 Jun 2022",
        'App_name' : "WhoWatch - Live Video Chat",
        'Package_name' : "jp.jig.product.whowatch.viewer",
        'installs' : "1,000,000+",
        'version' : "7.17.6",
    },
    'com.fineapp.yogiyo' :
    {
        'category' : "Food & Drink",
        'updated' : "10 Jun 2022",
        'App_name' : "\ubc30\ub2ec\uc694\uae30\uc694 - \uae30\ub2e4\ub9bc \uc5c6\ub294 \ub9db\uc9d1 \ubc30\ub2ec\uc571",
        'Package_name' : "com.fineapp.yogiyo",
        'installs' : "10,000,000+",
        'version' : "6.60.0",
    },
    'com.efun.hdqy.sea' :
    {
        'category' : "Games",
        'updated' : "15 Oct 2021",
        'App_name' : "Age of Chaos: Legends",
        'Package_name' : "com.efun.hdqy.sea",
        'installs' : "500,000+",
        'version' : "3.9",
    },
    'jp.linecorp.linemusic.android' :
    {
        'category' : "Entertainment",
        'updated' : "16 May 2022",
        'App_name' : "LINE MUSIC \u97f3\u697d\u306f\u30e9\u30a4\u30f3\u30df\u30e5\u30fc\u30b8\u30c3\u30af",
        'Package_name' : "jp.linecorp.linemusic.android",
        'installs' : "10,000,000+",
        'version' : "6.1.1",
    },
    'com.holosfind.showroom' :
    {
        'category' : "Shopping",
        'updated' : "17 Jun 2022",
        'App_name' : "Showroompriv\u00e9 - Ventes priv\u00e9es de grandes marques.",
        'Package_name' : "com.holosfind.showroom",
        'installs' : "10,000,000+",
        'version' : "13.24.0",
    },
    'br.com.decathlon' :
    {
        'category' : "Shopping",
        'updated' : "08 Jun 2022",
        'App_name' : "Decathlon: Loja de Esportes",
        'Package_name' : "br.com.decathlon",
        'installs' : "1,000,000+",
        'version' : "1.5.5",
    },
    'com.vimbo.app' :
    {
        'category' : "Education and Books",
        'updated' : "08 Jun 2022",
        'App_name' : "Vimbo \u00c7ocuk Kitap & Videolar\u0131",
        'Package_name' : "com.vimbo.app",
        'installs' : "10,000+",
        'version' : "1.0.20",
    },
    'com.qjzj4399kr.google' :
    {
        'category' : "Games",
        'updated' : "12 May 2022",
        'App_name' : "\uae30\uc801\uc758 \uac80",
        'Package_name' : "com.qjzj4399kr.google",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.bestringtonesapps.freeringtonesforandroid' :
    {
        'category' : "Entertainment",
        'updated' : "21 May 2022",
        'App_name' : "Ringtones for Android\u2122",
        'Package_name' : "com.bestringtonesapps.freeringtonesforandroid",
        'installs' : "50,000,000+",
        'version' : "Varies with device",
    },
    '645704840' :
    {
        'category' : "Games",
        'updated' : "2022-05-02T20:55:07Z",
        'App_name' : "Solitaire Deluxe\u00ae 2: Card Game",
        'Package_name' : "com.gosub60.sol5",
        'version' : "4.37.1",
        'minimumOsVersion' : "10.0",
    },
    '1498302242' :
    {
        'category' : "Finance",
        'updated' : "2022-06-19T14:56:02Z",
        'App_name' : "YAP \u2013 Your Digital Banking App",
        'Package_name' : "com.yap.banking",
        'version' : "1.7.5",
        'minimumOsVersion' : "13.0",
    },
    'bajajfinserv.in.hrx.app' :
    {
        'category' : "Health and Fitness",
        'updated' : "11 Jun 2022",
        'App_name' : "Bajaj Health : Doctor near me",
        'Package_name' : "bajajfinserv.in.hrx.app",
        'installs' : "1,000,000+",
        'version' : "5.0",
    },
    'kr.co.beautynet.missha' :
    {
        'category' : "Shopping",
        'updated' : "13 May 2022",
        'App_name' : "\ub219\ud06c (nunc)",
        'Package_name' : "kr.co.beautynet.missha",
        'installs' : "1,000,000+",
        'version' : "9.3",
    },
    'com.belongtail.belong' :
    {
        'category' : "Health and Fitness",
        'updated' : "13 Jun 2022",
        'App_name' : "BELONG Beating Cancer Together",
        'Package_name' : "com.belongtail.belong",
        'installs' : "100,000+",
        'version' : "5.4.7",
    },
    '1532737959' :
    {
        'category' : "Finance",
        'updated' : "2022-06-11T15:24:48Z",
        'App_name' : "Pokus",
        'Package_name' : "tr.com.turktelekom.pokus",
        'version' : "1.18.1",
        'minimumOsVersion' : "10.0",
    },
    'com.groboot.yala' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "24 May 2022",
        'App_name' : "yala \u2013 \u05de\u05d1\u05e6\u05e2\u05d9 \u05d4\u05e8\u05d2\u05e2 \u05d4\u05d0\u05d7\u05e8\u05d5\u05df \u05e4\u05ea\u05d0\u05dc",
        'Package_name' : "com.groboot.yala",
        'installs' : "500,000+",
        'version' : "4.8.0",
    },
    'adventure.rpg.anime.game.vng.ys6' :
    {
        'category' : "Games",
        'updated' : "01 Jun 2022",
        'App_name' : "Ys 6 Mobile VNG",
        'Package_name' : "adventure.rpg.anime.game.vng.ys6",
        'installs' : "500,000+",
        'version' : "1.1.1",
    },
    'net.mbc.shahid' :
    {
        'category' : "Entertainment",
        'updated' : "10 Jun 2022",
        'App_name' : "\ufeb7\ufe8e\ufeeb\ufeaa - Shahid",
        'Package_name' : "net.mbc.shahid",
        'installs' : "50,000,000+",
        'version' : "7.11.1",
    },
    'com.toppan.shufoo.android' :
    {
        'category' : "Lifestyle",
        'updated' : "26 May 2022",
        'App_name' : "\u30b7\u30e5\u30d5\u30fc \u30c1\u30e9\u30b7\u30a2\u30d7\u30ea\u3067\u8fd1\u304f\u306e\u5e97\u8217\u306e\u4fbf\u5229\u3067\u304a\u5f97\u306a\u30c1\u30e9\u30b7\u898b\u653e\u984c",
        'Package_name' : "com.toppan.shufoo.android",
        'installs' : "5,000,000+",
        'version' : "9.11.0",
    },
    'com.perfect.smdlkr' :
    {
        'category' : "Games",
        'updated' : "11 May 2021",
        'App_name' : "R5",
        'Package_name' : "com.perfect.smdlkr",
        'installs' : "500,000+",
        'version' : "2.6.0",
    },
    'com.stc.xplay' :
    {
        'category' : "Social and Communication",
        'updated' : "26 May 2022",
        'App_name' : "stc play",
        'Package_name' : "com.stc.xplay",
        'installs' : "100,000+",
        'version' : "2.0.46",
    },
    'com.tencent.ig' :
    {
        'category' : "Games",
        'updated' : "11 May 2022",
        'App_name' : "PUBG MOBILE",
        'Package_name' : "com.tencent.ig",
        'installs' : "500,000,000+",
        'version' : "2.0.0",
    },
    'com.trgain.mikrogain' :
    {
        'category' : "Entertainment",
        'updated' : "26 May 2022",
        'App_name' : "GA\u0130N",
        'Package_name' : "com.trgain.mikrogain",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    'com.goodgamestudios.bigfarmmobileharvest' :
    {
        'category' : "Games",
        'updated' : "14 Jun 2022",
        'App_name' : "Big Farm: Mobile Harvest",
        'Package_name' : "com.goodgamestudios.bigfarmmobileharvest",
        'installs' : "10,000,000+",
        'version' : "10.8.27254",
    },
    'com.gamepanda.st2' :
    {
        'category' : "Games",
        'updated' : "23 May 2022",
        'App_name' : "\u84bc\u5929\u82f1\u96c4\u8a8c2",
        'Package_name' : "com.gamepanda.st2",
        'installs' : "100,000+",
        'version' : "0.64.1",
    },
    'com.bitgroup.bloomme' :
    {
        'category' : "Lifestyle",
        'updated' : "09 Mar 2020",
        'App_name' : "BloomMe - Spa & Salon Booking App",
        'Package_name' : "com.bitgroup.bloomme",
        'installs' : "50,000+",
        'version' : "3.23",
    },
    'com.ahamove.user' :
    {
        'category' : "Lifestyle",
        'updated' : "10 Jun 2022",
        'App_name' : "Ahamove",
        'Package_name' : "com.ahamove.user",
        'installs' : "1,000,000+",
        'version' : "8.5.4447",
    },
    'com.wizwid' :
    {
        'category' : "Shopping",
        'updated' : "03 May 2022",
        'App_name' : "\uc704\uc988\uc704\ub4dc, WIZWID",
        'Package_name' : "com.wizwid",
        'installs' : "500,000+",
        'version' : "1.90",
    },
    'com.hocmai' :
    {
        'category' : "Education and Books",
        'updated' : "13 May 2022",
        'App_name' : "HOCMAI: H\u1ecdc online t\u1eeb l\u1edbp 1-12",
        'Package_name' : "com.hocmai",
        'installs' : "1,000,000+",
        'version' : "3.1.4",
    },
    'io.swvl.customer' :
    {
        'category' : "Utilities",
        'updated' : "08 Jun 2022",
        'App_name' : "Swvl - Daily Bus Rides",
        'Package_name' : "io.swvl.customer",
        'installs' : "10,000,000+",
        'version' : "8.15.2",
    },
    'fr.stime.mcommerce' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "Intermarch\u00e9, Magasin & Services (Drive, Livraison)",
        'Package_name' : "fr.stime.mcommerce",
        'installs' : "1,000,000+",
        'version' : "8.2.0",
    },
    'com.planetart.fpnl' :
    {
        'category' : "Entertainment",
        'updated' : "11 Jun 2022",
        'App_name' : "FreePrints",
        'Package_name' : "com.planetart.fpnl",
        'installs' : "500,000+",
        'version' : "3.55.0",
    },
    'com.yoox' :
    {
        'category' : "Shopping",
        'updated' : "31 May 2022",
        'App_name' : "YOOX - Fashion, Design and Art",
        'Package_name' : "com.yoox",
        'installs' : "5,000,000+",
        'version' : "6.4.0",
    },
    'homiedev.com.homie' :
    {
        'category' : "House & Home",
        'updated' : "09 Jun 2022",
        'App_name' : "Homie Real Estate Search",
        'Package_name' : "homiedev.com.homie",
        'installs' : "50,000+",
        'version' : "3.6.5",
    },
    'ru.sportmaster.app' :
    {
        'category' : "Shopping",
        'updated' : "17 Jun 2022",
        'App_name' : "\u0421\u043f\u043e\u0440\u0442\u043c\u0430\u0441\u0442\u0435\u0440: \u0438\u043d\u0442\u0435\u0440\u043d\u0435\u0442-\u043c\u0430\u0433\u0430\u0437\u0438\u043d",
        'Package_name' : "ru.sportmaster.app",
        'installs' : "10,000,000+",
        'version' : "4.7.1",
    },
    'jp.co.benesse.studycast' :
    {
        'category' : "Education and Books",
        'updated' : "27 May 2022",
        'App_name' : "StudyCast\uff08\u30b9\u30bf\u30c7\u30a3\u30ad\u30e3\u30b9\u30c8\uff0f\u30b9\u30bf\u30ad\u30e3\u30b9\uff09",
        'Package_name' : "jp.co.benesse.studycast",
        'installs' : "100,000+",
        'version' : "2.2.0",
    },
    'com.jokr.delivery' :
    {
        'category' : "Food & Drink",
        'updated' : "30 May 2022",
        'App_name' : "JOKR - Fast Grocery Delivery",
        'Package_name' : "com.jokr.delivery",
        'installs' : "1,000,000+",
        'version' : "2.4.0",
    },
    '1124822470' :
    {
        'category' : "Games",
        'updated' : "2022-06-15T05:05:02Z",
        'App_name' : "\u738b\u9053RPG \u30b0\u30e9\u30f3\u30c9\u30b5\u30de\u30ca\u30fc\u30ba",
        'Package_name' : "jp.goodsmile.grandsummoners",
        'version' : "3.53.5",
        'minimumOsVersion' : "9.0",
    },
    'com.appromobile.hotel' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "10 Jun 2022",
        'App_name' : "Go2Joy - Hourly Booking App",
        'Package_name' : "com.appromobile.hotel",
        'installs' : "100,000+",
        'version' : "15.10.2",
    },
    'and.onemt.boe.tr' :
    {
        'category' : "Games",
        'updated' : "12 May 2022",
        'App_name' : "Days of Empire - Heroes Never Die!",
        'Package_name' : "and.onemt.boe.tr",
        'installs' : "5,000,000+",
        'version' : "2.43.009",
    },
    'com.creditsesame' :
    {
        'category' : "Finance",
        'updated' : "11 Jun 2022",
        'App_name' : "Credit Sesame",
        'Package_name' : "com.creditsesame",
        'installs' : "1,000,000+",
        'version' : "5.49.1",
    },
    'com.mspy.lite' :
    {
        'category' : "Lifestyle",
        'updated' : "09 Jun 2022",
        'App_name' : "mLite - GPS Family Tracker",
        'Package_name' : "com.mspy.lite",
        'installs' : "5,000,000+",
        'version' : "3.0.7",
    },
    'com.nyouinc.duelsummonersaos' :
    {
        'category' : "Games",
        'updated' : "16 May 2022",
        'App_name' : "Duel Summoners",
        'Package_name' : "com.nyouinc.duelsummonersaos",
        'installs' : "100,000+",
        'version' : "1.0.2.8",
    },
    'com.kleen.app' :
    {
        'category' : "Shopping",
        'updated' : "09 Jun 2022",
        'App_name' : "Wash par TotalEnergies. Lavage voiture et moto",
        'Package_name' : "com.kleen.app",
        'installs' : "100,000+",
        'version' : "1.34.1",
    },
    'net.one97.paytm' :
    {
        'category' : "Finance",
        'updated' : "30 May 2022",
        'App_name' : "Paytm: Secure UPI Payments",
        'Package_name' : "net.one97.paytm",
        'installs' : "100,000,000+",
        'version' : "Varies with device",
    },
    'com.ada.moovinparis.prod' :
    {
        'category' : "Vehicles and Travels",
        'updated' : "01 Jun 2022",
        'App_name' : "Ada Paris - libre-service 24/7",
        'Package_name' : "com.ada.moovinparis.prod",
        'installs' : "10,000+",
        'version' : "12.1.0",
    },
    'com.apalon.alarmclock.smart' :
    {
        'category' : "Health and Fitness",
        'updated' : "24 Mar 2022",
        'App_name' : "Sleepzy: Sleep Cycle Tracker",
        'Package_name' : "com.apalon.alarmclock.smart",
        'installs' : "1,000,000+",
        'version' : "3.20.0",
    },
    '886445756' :
    {
        'category' : "Entertainment",
        'updated' : "2022-06-09T05:41:28Z",
        'App_name' : "Tubi - Watch Movies & TV Shows",
        'Package_name' : "com.adrise.tubitv",
        'version' : "6.5.0",
        'minimumOsVersion' : "14.0",
    },
    'com.epic.docubay' :
    {
        'category' : "Entertainment",
        'updated' : "14 Jun 2022",
        'App_name' : "DocuBay - Streaming Documentaries",
        'Package_name' : "com.epic.docubay",
        'installs' : "500,000+",
        'version' : "Varies with device",
    },
    'kr.co.plat.carplat' :
    {
        'category' : "Utilities",
        'updated' : "13 Jan 2022",
        'App_name' : "\uce74\ud50c\ub7ab - \uc9d1 \uc55e\uc5d0\uc11c \ud0c0\ub294 \ub80c\ud2b8\uce74(\ub80c\ud130\uce74)",
        'Package_name' : "kr.co.plat.carplat",
        'installs' : "500,000+",
        'version' : "6.1.13",
    },
    'com.app.balaan.balaanboutique' :
    {
        'category' : "Shopping",
        'updated' : "18 May 2022",
        'App_name' : "\ubc1c\ub780 - \ucc38 \uc26c\uc6b4 \ub7ed\uc154\ub9ac \uc1fc\ud551 \uc571",
        'Package_name' : "com.app.balaan.balaanboutique",
        'installs' : "1,000,000+",
        'version' : "1.2.94",
    },
    '1516560926' :
    {
        'category' : "Games",
        'updated' : "2022-04-01T03:47:18Z",
        'App_name' : "Giftole\uff08\u30ae\u30d5\u30c8\u30fc\u30ec\uff09\u30af\u30ec\u30fc\u30f3\u30b2\u30fc\u30e0\u65b0\u4f5c\u30a2\u30d7\u30ea",
        'Package_name' : "jp.giftole",
        'version' : "1.0.17",
        'minimumOsVersion' : "12.2",
    },
    'com.longtukorea.rsg' :
    {
        'category' : "Games",
        'updated' : "30 Sep 2021",
        'App_name' : "\ubcf4\uc2a4\ub808\uc774\ube0c",
        'Package_name' : "com.longtukorea.rsg",
        'installs' : "500,000+",
        'version' : "5.1.7",
    },
    'com.fbs.pa' :
    {
        'category' : "Finance",
        'updated' : "14 Jun 2022",
        'App_name' : "FBS - Stocks & Forex Trading",
        'Package_name' : "com.fbs.pa",
        'installs' : "5,000,000+",
        'version' : "1.71.0",
    },
    '862390640' :
    {
        'category' : "Entertainment",
        'updated' : "2022-05-23T07:59:15Z",
        'App_name' : "OSN+ Streaming App",
        'Package_name' : "com.osn.go",
        'version' : "6.77.0",
        'minimumOsVersion' : "11.4",
    },
    'com.simpl.android' :
    {
        'category' : "Finance",
        'updated' : "10 Jun 2022",
        'App_name' : "Simpl Pay Later",
        'Package_name' : "com.simpl.android",
        'installs' : "1,000,000+",
        'version' : "4.3.5",
    },
    'com.kudu.androidapp' :
    {
        'category' : "Food & Drink",
        'updated' : "08 Jun 2022",
        'App_name' : "Kudu Saudi Arabia",
        'Package_name' : "com.kudu.androidapp",
        'installs' : "100,000+",
        'version' : "6.1.6",
    },
    'com.special.warshiplegend.idle.asia' :
    {
        'category' : "Games",
        'updated' : "21 Apr 2022",
        'App_name' : "Warship Legend: Idle RPG",
        'Package_name' : "com.special.warshiplegend.idle.asia",
        'installs' : "1,000,000+",
        'version' : "2.5.1",
    },
    'com.honey.yeobo' :
    {
        'category' : "Social and Communication",
        'updated' : "31 May 2022",
        'App_name' : "HoneyBaby - Meeting Korean",
        'Package_name' : "com.honey.yeobo",
        'installs' : "100,000+",
        'version' : "2.1.7",
    },
    'com.GybeGames.FillTheFridgeTest' :
    {
        'category' : "Games",
        'updated' : "16 Jun 2022",
        'App_name' : "Fill The Fridge",
        'Package_name' : "com.GybeGames.FillTheFridgeTest",
        'installs' : "10,000,000+",
        'version' : "2.3.1",
    },
    'com.finatext.karufx' :
    {
        'category' : "Finance",
        'updated' : "26 May 2022",
        'App_name' : "FX\u30c7\u30e2\u30c8\u30ec\u30fc\u30c9\u3067\u30b2\u30fc\u30e0\u611f\u899a\u3067\u5b66\u3079\u308b\u6295\u8cc7\u30a2\u30d7\u30ea\uff01\u304b\u308bFX",
        'Package_name' : "com.finatext.karufx",
        'installs' : "100,000+",
        'version' : "2.1.18",
    },
    'com.pg.orkid' :
    {
        'category' : "Health and Fitness",
        'updated' : "19 May 2022",
        'App_name' : "PeriOsfer by Orkid",
        'Package_name' : "com.pg.orkid",
        'installs' : "1,000,000+",
        'version' : "1.0.43",
    },
    '1225034537' :
    {
        'category' : "Lifestyle",
        'updated' : "2022-05-27T10:51:46Z",
        'App_name' : "Smiles UAE",
        'Package_name' : "Etisalat.House",
        'version' : "6.6.4",
        'minimumOsVersion' : "12.0",
    },
    '798519009' :
    {
        'category' : "Games",
        'updated' : "2022-05-19T05:00:50Z",
        'App_name' : "LINE \u30d1\u30ba\u30eb \u30bf\u30f3\u30bf\u30f3",
        'Package_name' : "com.linecorp.LGPSH",
        'version' : "4.7.0",
        'minimumOsVersion' : "10.0",
    },
    'kr.co.questgames.druwadungeon' :
    {
        'category' : "Games",
        'updated' : "20 May 2022",
        'App_name' : "\ub4dc\ub8e8\uc640 \ub358\uc804 - \ubc29\uce58\ud615 RPG",
        'Package_name' : "kr.co.questgames.druwadungeon",
        'installs' : "500,000+",
        'version' : "5.27.1",
    },
    'com.expedition.twgp' :
    {
        'category' : "Games",
        'updated' : "04 Mar 2022",
        'App_name' : "\u738b\u8005\u9060\u5f81",
        'Package_name' : "com.expedition.twgp",
        'installs' : "100,000+",
        'version' : "1.0.17",
    },
    '850057092' :
    {
        'category' : "Games",
        'updated' : "2022-06-20T01:33:01Z",
        'App_name' : "The Battle Cats",
        'Package_name' : "jp.co.ponos.battlecatsen",
        'version' : "11.6.0",
        'minimumOsVersion' : "11.0",
    },
    'com.delivery.india.client' :
    {
        'category' : "Finance",
        'updated' : "16 Jun 2022",
        'App_name' : "Wefast \u2014 Courier Delivery Service",
        'Package_name' : "com.delivery.india.client",
        'installs' : "1,000,000+",
        'version' : "1.63.0",
    },
    'th.co.wegames.rod.and' :
    {
        'category' : "Games",
        'updated' : "19 Jan 2020",
        'App_name' : "Reign of Dragon",
        'Package_name' : "th.co.wegames.rod.and",
        'installs' : "100,000+",
        'version' : "1.13",
    },
    'com.ocito.laredoute' :
    {
        'category' : "Shopping",
        'updated' : "13 Jun 2022",
        'App_name' : "La Redoute - Fashion & Home",
        'Package_name' : "com.ocito.laredoute",
        'installs' : "5,000,000+",
        'version' : "9.16.3",
    },
    'com.dietbox.android' :
    {
        'category' : "Health and Fitness",
        'updated' : "19 May 2022",
        'App_name' : "\u30c0\u30a4\u30a8\u30c3\u30c8BOX",
        'Package_name' : "com.dietbox.android",
        'installs' : "50,000+",
        'version' : "2.1.13",
    },
    'br.com.mobile2you.appgas.costumer' :
    {
        'category' : "Shopping",
        'updated' : "01 Jun 2022",
        'App_name' : "AppGas - Aplicativo de g\u00e1s",
        'Package_name' : "br.com.mobile2you.appgas.costumer",
        'installs' : "100,000+",
        'version' : "12.3.2",
    },
    '1464872022' :
    {
        'category' : "Games",
        'updated' : "2022-04-28T11:12:44Z",
        'App_name' : "Arknights",
        'Package_name' : "com.YoStarEN.Arknights",
        'version' : "9.0.01",
        'minimumOsVersion' : "9.0",
    },
    'com.xlegend.grandfantasia.tw' :
    {
        'category' : "Games",
        'updated' : "26 May 2022",
        'App_name' : "\u7cbe\u9748\u6a02\u7ae0-\u9019\u6a23\u7684\u5925\u4f34\u6c92\u554f\u984c\u55ce",
        'Package_name' : "com.xlegend.grandfantasia.tw",
        'installs' : "100,000+",
        'version' : "9.1.1004",
    },
    '586614529' :
    {
        'category' : "Shopping",
        'updated' : "2022-06-07T23:20:29Z",
        'App_name' : "GittiGidiyor: Al\u0131\u015fveri\u015f Sitesi",
        'Package_name' : "com.gittigidiyor.GittiGidiyor",
        'version' : "3.34.6",
        'minimumOsVersion' : "11.0",
    },
    'tw.com.taomee.huam' :
    {
        'category' : "Games",
        'updated' : "04 Jan 2022",
        'App_name' : "\u5c0f\u82b1\u4ed9M",
        'Package_name' : "tw.com.taomee.huam",
        'installs' : "50,000+",
        'version' : "4.1.4",
    },
    'hellomobile.hello' :
    {
        'category' : "Social and Communication",
        'updated' : "11 Jun 2022",
        'App_name' : "True - Private Group Sharing",
        'Package_name' : "hellomobile.hello",
        'installs' : "1,000,000+",
        'version' : "4.0.0",
    },
    'com.reebee.reebee' :
    {
        'category' : "Shopping",
        'updated' : "31 May 2022",
        'App_name' : "Reebee: Weekly Flyers & Deals",
        'Package_name' : "com.reebee.reebee",
        'installs' : "1,000,000+",
        'version' : "Varies with device",
    },
    '1332577202' :
    {
        'category' : "Social and Communication",
        'updated' : "2021-12-14T02:33:23Z",
        'App_name' : "\uc778\uc0dd\ub77d\uc11c - \ub0b4 \uc774\uc57c\uae30\ub85c \ub9cc\ub098\ub294 \uc138\uc0c1",
        'Package_name' : "com.samsungcard.mls",
        'version' : "1.2.8",
        'minimumOsVersion' : "9.0",
    },
    'com.wizards.mtga' :
    {
        'category' : "Games",
        'updated' : "08 Jun 2022",
        'App_name' : "Magic: The Gathering Arena",
        'Package_name' : "com.wizards.mtga",
        'installs' : "1,000,000+",
        'version' : "2022.16.10.1396",
    },
    'com.purpleteal.tweakandeat' :
    {
        'category' : "Health and Fitness",
        'updated' : "16 Jun 2022",
        'App_name' : "Tweak & Eat. Eat Healthy.",
        'Package_name' : "com.purpleteal.tweakandeat",
        'installs' : "1,000,000+",
        'version' : "4.22",
    },
    '1437231181' :
    {
        'category' : "Games",
        'updated' : "2022-05-30T05:10:46Z",
        'App_name' : "Star Chef 2: Restaurant Game",
        'Package_name' : "com.99games.starchef2",
        'version' : "1.4.8",
        'minimumOsVersion' : "11.0",
    },
    'com.aidis.lastcloudiajpn' :
    {
        'category' : "Games",
        'updated' : "10 Jun 2022",
        'App_name' : "\u30e9\u30b9\u30c8\u30af\u30e9\u30a6\u30c7\u30a3\u30a2",
        'Package_name' : "com.aidis.lastcloudiajpn",
        'installs' : "100,000+",
        'version' : "3.2.2",
    },
}



# app = App_data.keys()
# for i in app:
#     key = App_data.get(i).get('category')
#     value = i
#     with open('script.txt', 'a') as convert_file:
#         convert_file.write('''UPDATE `campaigns` SET `Category` = '%s'  WHERE `app_id` =  '%s';\n''' % (key, value))


