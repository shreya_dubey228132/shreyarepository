def install(app_data, device_data):
    if not app_data.get('app_start_time'):
        app_data['app_start_time'] =time.time()
    if not app_data.get('times'):
        # print "Please wait installing..."
        installtimenew.main(app_data, device_data, app_size=campaign_data.get('app_size'), os='android')
        app_data['installed_at'] = datetime.fromtimestamp(app_data.get('times').get('install_complete_time')).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z' +device_data.get('timezone')



    if not app_data.get('idfa'):
        app_data['idfa'] = str(uuid.uuid4()).upper()
        app_data['idfv'] = str(uuid.uuid4()).upper()
        app_data['ios_uuid'] = str(uuid.uuid4())
        # app_data['uid'] = util.get_random_string('decimal', 9)

    if not app_data.get('fb_anon_id'):
        app_data['fb_anon_id'] = str(uuid.uuid4()).upper()

    app_updated_at = '2020-05-29T01:38:48.000Z'
    app_updated_at =datetime.strptime(app_updated_at,'%Y-%m-%dT%H:%M:%S.%fZ')
    app_updated_at = app_updated_at-timedelta(hours=5, minutes=30)

    tz = device_data.get('timezone')
    if tz[0] == '+':
        app_data['app_updated_at'] = (app_updated_at +timedelta(hours=int(tz[1:3]), minutes= int(tz[3:5]))).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] +'Z'+tz
    else:
        app_data['app_updated_at'] = (app_updated_at -timedelta(hours=int(tz[1:3]), minutes= int(tz[3:5]))).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] +'Z'+tz

    if not app_data.get('callback_params'):
        app_data['callback_params'] = '{"eid":"ADBA033E-71E4-4191-B047-B9C5E347B09C"}'


    # print 'track___'
    req = adjust_session(app_data, device_data)
    util.execute_request(**req)
    time.sleep(random.randint(1,5))


    # print 'sdk-click'
    req = adjust_sdk_click(app_data, device_data)
    util.execute_request(**req)
    time.sleep(random.randint(10, 30))


    # print 'attribution'
    req = adjust_attribution(app_data, device_data)
    util.execute_request(**req)
    time.sleep(random.randint(30, 60))

# ==========================================================================
#  for manually  update comment whole for loop
# ===========================================================================
	for i in adjust_data.get('event_name'):
		# print i
		if adjust_data.get('call_back'):
			calback = adjust_data.get('call_back').get(i)
		else:
			calback  = None
		if adjust_data.get('partner'):
			patner = adjust_data.get('partner').get(i)
		else:
			patner =None
		req = adjust_events(app_data,device_data,event_token=i,callback=calback,patner=patner)
		util.execute_request(**req)
		time.sleep(random.randint(1,5))



# ===========================================================================================
# ===========================================================================================

#  For update in events pls write from her....................................................

	# req = adjust_events(app_data,device_data,event_token='',)
	# util.execute_request(**req)
	# time.sleep(random.randint(1,5))




	return {'status':True}

#==================================================================================================
#          Adjust
#========================================================================================
def adjust_session(app_data, device_data):
    url = 'https://app.adjust.com/session'
    httpmethod = 'post'
    headers ={
        'Client-SDK': campaign_data.get('adjust').get('sdk'),
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Accept-Language': '{0}-{1}'.format(device_data.get('locale').get('language'), device_data.get('locale').get('country').lower()),
        'Accept-Encoding': 'gzip, deflate, br',
        'User-Agent'        : '{0}/{1} CFNetwork/{2} Darwin/{3}'.format(campaign_data.get('app_name'), campaign_data.get('app_version_code'), device_data.get('cfnetwork').split('.')[0]+'.'+device_data.get('cfnetwork').split('.')[1], device_data.get('cfnetwork').split('_')[1]),
    }
    if not app_data.get('session_count'):
        app_data['session_count'] = 1
        app_data['subsession_count'] =1
    else:
        app_data['session_count'] +=1
        app_data['subsession_count'] =1

    app_data['session_start']  = int(time.time())
    params = None
    data ={
        'app_token': campaign_data.get('adjust').get('app_token'),
        'app_updated_at': app_data.get('app_updated_at'),
        'app_version':	campaign_data.get('app_version_code'),
        'app_version_short': campaign_data.get('app_version_name'),
        'attribution_deeplink':	1,
        'bundle_id':	campaign_data.get('package_name'),
        'connectivity_type':	2,
        'country': device_data.get('locale').get('country'),
        'cpu_type':	device_data.get('cpu_type'),
        'created_at':	(datetime.now()- timedelta(seconds=random.randint(5,15))).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] +'Z'+ device_data.get('timezone'),
        'device_name':	device_data.get('device_platform'),
        'device_type':	'iPhone',
        'environment':	'production',
        'event_buffering_enabled':	0,
        'm':'656E4E37316D41500D07000000000000000142007CEB0043310000485FE42162',
        'fb_anon_id':	app_data.get('fb_anon_id'),
        'hardware_name':	device_data.get('hardware'),
        'idfa': app_data.get('idfa'),
        'idfv': app_data.get('idfv'),
        'install_receipt':	apple_receipt(app_data),
        'installed_at': app_data.get('installed_at'),
        'language':	device_data.get('locale').get('language'),
        'needs_response_details':	1,
        'os_build':	device_data.get('build'),
        'os_name':	'ios',
        'os_version':	device_data.get('os_version'),
        'persistent_ios_uuid': app_data.get('ios_uuid'),
        'sent_at':	datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] +'Z'+ device_data.get('timezone'),
        'session_count': app_data.get('session_count'),
        'tracking_enabled':	1
    }
	if session_data.get('vm_isa'):
		data['vm_isa'] = device_data.get('cpu')[:-4]
	if session_data.get('app_secret'):
		data['app_secret'] = session_data.get('app_secret')
	
	if session_data.get('ui_mode'):
		data['ui_mode'] = 1
	
	if session_data.get('fb_id'):
		data['fb_id'] = app_data.get('fb_id')
    	if app_data.get('callback_params'):
        	data['callback_params'] = app_data.get('callback_params')

	if app_data.get('primary_dedupe_token'):
		data['primary_dedupe_token']=app_data.get('ios_uuid')

       if campaign_data.get('adjust').get('app_secret'):	
    		headers['Authorization'] = get_authorization(campaign_data.get('adjust').get('app_secret'), 'session', data.get('idfa'), data.get('created_at'))
    
       return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':data}

#=================================================================
def adjust_sdk_click(app_data, device_data):
    
    url = 'https://app.adjust.com/sdk_click'
    httpmethod = 'post'
    headers ={
        'Client-Sdk': campaign_data.get('adjust').get('sdk'),
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Accept-Language': '{0}-{1}'.format(device_data.get('locale').get('language'), device_data.get('locale').get('country').lower()),
        'Accept-Encoding': 'gzip, deflate, br',
        'User-Agent'        : '{0}/{1} CFNetwork/{2} Darwin/{3}'.format(campaign_data.get('app_name'), campaign_data.get('app_version_code'), device_data.get('cfnetwork').split('.')[0]+'.'+device_data.get('cfnetwork').split('.')[1], device_data.get('cfnetwork').split('_')[1]),

    }
    params = None
    if not app_data.get('subsession_count'):
        app_data['subsession_count'] =1
    elif random.randint(1,5) ==random.randint(1,5):
        app_data['subsession_count'] +=1
    data = {
        'app_token': campaign_data.get('adjust').get('app_token'),
        'app_updated_at': app_data.get('app_updated_at'),
        'app_version':	campaign_data.get('app_version_code'),
        'app_version_short': campaign_data.get('app_version_name'),
        'attribution_deeplink':	1,
        'bundle_id':	campaign_data.get('package_name'),
        'connectivity_type':	2,
        'country': device_data.get('locale').get('country'),
        'cpu_type':	device_data.get('cpu_type'),
        'created_at':	(datetime.now()- timedelta(seconds=random.randint(5,15))).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] +'Z'+ device_data.get('timezone'),
        'details':	'{"Version3.1":{"iad-attribution":"false"}}',
        'device_name':	device_data.get('device_platform'),
        'device_type':	'iPhone',
        'environment':	'production',
        'event_buffering_enabled':	0,
        'hardware_name': device_data.get('hardware'),
        'idfa': app_data.get('idfa'),
        'idfv': app_data.get('idfv'),
        'install_receipt':	apple_receipt(app_data),
        'installed_at': app_data.get('installed_at'),
        'language':	device_data.get('locale').get('language'),
        'last_interval': int(time.time()) - app_data.get('last_interval') if app_data.get('last_interval') else '1',
        'mnc':	device_data.get('mnc'),
        'mcc':	device_data.get('mcc'),
        'needs_response_details':	1,
        'network_type':	device_data.get('network'),
        'os_build':	device_data.get('build'),
        'os_name':	'ios',
        'os_version':	device_data.get('os_version'),
        'persistent_ios_uuid': app_data.get('ios_uuid'),
        'push_token':app_data.get('push_token'),
        'sent_at':	datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] +'Z'+ device_data.get('timezone'),
        'session_count': app_data.get('session_count'),
        'session_length': int(time.time())-app_data.get('session_start'),
        'source':	'iad3',
        'subsession_count': app_data.get('subsession_count'),
        'time_spent': int(time.time()) - app_data.get('session_start'),
        'tracking_enabled':	1,
    }

    app_data['last_interval'] = int(time.time())
    headers['Authorization'] =get_authorization(campaign_data.get('adjust').get('app_secret'), 'click', data.get('idfa'), data.get('created_at'), source=data.get('source'))
    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':data}
#=======================================================================
#======================================================================
def adjust_attribution(app_data, device_data):
    url = 'https://app.adjust.com/attribution'
    httpmethod = 'get'
    headers = {
        'Client-Sdk': campaign_data.get('adjust').get('sdk'),
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Accept-Language': '{0}-{1}'.format(device_data.get('locale').get('language'), device_data.get('locale').get('country').lower()),
        'Accept-Encoding': 'gzip, deflate, br',
        'User-Agent'        : '{0}/{1} CFNetwork/{2} Darwin/{3}'.format(campaign_data.get('app_name'), campaign_data.get('app_version_code'), device_data.get('cfnetwork').split('.')[0]+'.'+device_data.get('cfnetwork').split('.')[1], device_data.get('cfnetwork').split('_')[1]),

    }
    params ={
        'event_buffering_enabled':	0,
        'needs_response_details':	1,
        'bundle_id':campaign_data.get('package_name'),
        'os_name':	'ios',
        'idfa': app_data.get('idfa'),
        'app_version': campaign_data.get('app_version_code'),
        'created_at':	(datetime.now()- timedelta(seconds=random.randint(5,15))).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] +'Z'+ device_data.get('timezone'),
        'initiated_by':	'backend',
        'persistent_ios_uuid':	app_data.get('ios_uuid'),
        'app_version_short':	campaign_data.get('app_version_name'),
        'device_type': device_data.get('device_type'),
        'attribution_deeplink':	1,
        'device_name':	device_data.get('device_platform'),
        'os_build':	device_data.get('build'),
        'idfv':	app_data.get('idfv'),
        'app_token':	campaign_data.get('adjust').get('app_token'),
        'os_version': device_data.get('os_version'),
        'environment':	'production',
        'sent_at':	datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] +'Z'+ device_data.get('timezone'),
    }
    data =None
    headers['Authorization'] = get_authorization(campaign_data.get('adjust').get('app_secret'), 'attribution', params.get('idfa'), params.get('created_at'))
    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':data}
#--------------------------------------------------------------------
def adjust_event(app_data, device_data, event_token):
    url = 'https://app.adjust.com/event'
    httpmethod = 'post'
    headers = {
        'Client-Sdk': campaign_data.get('adjust').get('sdk'),
        'Accept': '*/*',
        'Accept-Language': device_data.get('locale').get('language') + '-'+device_data.get('locale').get('country').lower(),
        'Accept-Encoding': 'br, gzip, deflate',
        'User-Agent': '{0}/{1} CFNetwork/{2} Darwin/{3}'.format(campaign_data.get('app_name'), campaign_data.get('app_version_code'), device_data.get('cfnetwork').split('_')[0], device_data.get('cfnetwork').split('_')[1])
    }
    params = None
    if not app_data.get('event_count'):
        app_data['event_count'] = 1
    else:
        app_data['event_count']+=1
    if random.randint(1, 5) == random.randint(1,5):
        app_data['subsession_count'] +=1
    data ={
        'app_token':	campaign_data.get('adjust').get('app_token'),
        'app_version':	campaign_data.get('app_version_code'),
        'app_version_short':	campaign_data.get('app_version_name'),
        'attribution_deeplink':	'1',
        'bundle_id':	campaign_data.get('package_name'),
        'connectivity_type':	2,
        'country':	device_data.get('locale').get('country'),
        'cpu_type':	device_data.get('cpu_type'),
        'created_at':	(datetime.now()- timedelta(seconds=random.randint(5,15))).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] +'Z'+ device_data.get('timezone'),
        'device_name':	device_data.get('device_platform'),
        'device_type':	'iPhone',
        'environment':	'production',
        'event_buffering_enabled':	0,
        'event_count':	app_data.get('event_count'),
        'event_token':	event_token,
        'hardware_name':	device_data.get('hardware'),
        'idfa':	app_data.get('idfa'),
        'idfv':	app_data.get('idfv'),
        'install_receipt':apple_receipt(app_data),
        'language':	device_data.get('locale').get('language'),
        'mnc':	device_data.get('mnc'),
        'mcc':	device_data.get('mcc'),
        'needs_response_details':	1,
        'network_type':	device_data.get('network'),
        'os_build':	device_data.get('build'),
        'os_name':	'ios',
        'os_version':	device_data.get('os_version'),
        'persistent_ios_uuid':	app_data.get('ios_uuid'),
        'push_token':app_data.get('push_token'),
        'sent_at':	datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] +'Z'+ device_data.get('timezone'),
        'session_count': app_data.get('session_count'),
        'session_length': int(time.time())-app_data.get('session_start'),
        'subsession_count':	app_data.get('subsession_count'),
        'time_spent':	int(time.time())-app_data.get('session_start'),
        'tracking_enabled':	1,
    }

    if app_data.get('callback_params'):
        data['callback_params'] = app_data.get('callback_params')
    headers['Authorization'] = get_authorization(campaign_data.get('adjust').get('app_secret'), 'event', data.get('idfa'), data.get('created_at'))
    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':data}
    
    


def adjust_sdk_info(app_data, device_data, source ='push'):
    url = 'https://app.adjust.com/sdk_info'
    httpmethod = 'post'
    headers = {
        'Client-Sdk': campaign_data.get('adjust').get('sdk'),
        'Accept': '*/*',
        'Accept-Language': device_data.get('locale').get('language') + '-'+device_data.get('locale').get('country').lower(),
        'Accept-Encoding': 'br, gzip, deflate',
        'User-Agent': '{0}/{1} CFNetwork/{2} Darwin/{3}'.format(campaign_data.get('app_name'), campaign_data.get('app_version_code'), device_data.get('cfnetwork').split('_')[0], device_data.get('cfnetwork').split('_')[1])
    }
    params = None

    if not app_data.get('push_token'):
        app_data['push_token'] ='dY75OuUUQEM:APA91b' + ''.join(random.choice(string.digits + string.ascii_letters + '-_') for _ in range(134))
    
    data ={
        
        'app_token': campaign_data.get('adjust').get('app_token'),
        'attribution_deeplink':	'1',
        'created_at':	(datetime.now()- timedelta(seconds=random.randint(20,30))).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'+device_data.get('timezone'),
        'environment':	'production',
        'event_buffering_enabled':	0,
        'idfa':	app_data.get('idfa'),
        'idfv':	app_data.get('idfv'),
        'needs_response_details':	1,
        'persistent_ios_uuid':	app_data.get('ios_uuid'),
        'push_token': app_data.get('push_token'),
        'sent_at':	datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'+device_data.get('timezone'),
        'source':	source,
        
    }

    
    headers['Authorization'] = get_authorization(campaign_data.get('adjust').get('app_secret'), 'event', data.get('idfa'), data.get('created_at'))
    return {'url':url, 'httpmethod':httpmethod, 'headers':headers, 'params':params, 'data':data}
#------------------------------------------
#=================================================================================================
def get_authorization(app_secret, activity_kind, idfa, created_at, source =''):
    sign_string = '{0}{1}{2}{4}{3}'.format(app_secret, activity_kind, idfa, created_at, source)
    sign = hashlib.sha256(sign_string).hexdigest()
    if source:
        return 'Signature secret_id="1",signature="{0}",algorithm="sha256",headers="app_secret activity_kind idfa source created_at"'.format(sign)
    return 'Signature secret_id="1",signature="{0}",algorithm="sha256",headers="app_secret activity_kind idfa created_at"'.format(sign)


#======================================================
def apple_receipt(app_data):
    if not app_data.get('apple_receipt'):
        app_data['apple_receipt'] = 'MIIS'+''.join(random.choice(string.digits + string.ascii_letters+ '+/' ) for _ in range(random.randint(6000,7000)))+'='
    return app_data.get('apple_receipt')

def click(device_data=None, camp_type='market', camp_plat = 'ios'):
    serial = device_data.get('serial')
    agent_id = Config.AGENTID
    random_number = random.randint(1,10)
    source_id = ""
    if random_number < 5:
        source_id = "728"
    elif random_number < 8:
        source_id = "517"
    elif random_number < 9:
        source_id = "604"
    else:
        source_id = "386"

    st = str(int(time.time()*1000))

    link = campaign_data['link']
    link = link.format(dp2=st,agent=agent_id,source=source_id,device_serial=serial,gaid=device_data.get('adid'))
    return clicker.click(link, device_data, camp_type, camp_plat, 'url=')

def get_country():
    weighted_choices = campaign_data['country'] if campaign_data.get('country') else ('USA', 4)
    country_list = [val for val, cnt in weighted_choices for i in range(cnt)]
    return random.choice(country_list)

def get_retention(day):
    return campaign_data['retention'][day] if campaign_data['retention'].get(day) else 0
